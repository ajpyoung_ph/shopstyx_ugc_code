var sortby = function (prop,sortVal){
	if(typeof(sortVal)=='undefined' || sortVal.toLowerCase()=='asc')
	{
		return function(a,b){
	      if( a[prop] > b[prop]){
	          return 1;
	      }else if( a[prop] < b[prop] ){
	          return -1;
	      }
	      return 0;
	   }
	}else{
		return function(b,a){
	      if( a[prop] > b[prop]){
	          return 1;
	      }else if( a[prop] < b[prop] ){
	          return -1;
	      }
	      return 0;
	   }
	}
}

module.exports = {
	sortby:sortby
}