var nodemailer = require('nodemailer');
//var transporter = nodemailer.createTransport();
var smtpTransport = require('nodemailer-smtp-transport');
var transporter = nodemailer.createTransport(smtpTransport({
    host: 'smtp.mailgun.org',
    port: 587,
    auth: {
        user: 'postmaster@shopstyx.com',
        pass: '0fkpo68eh3t4'
    }
}));
var mongojs = require('mongojs');
var uuid = require('node-uuid');
var uriTools = require('../helpers/uriTools');
var	util = require('util');
var mytime = require('../mysql_connects/mysql_convertions');
var mysql = require('mysql');

var processData = function()
{
	try{
		//check unprocessed data and reprocess them
		//find unprocessed emails
		findProgressMailOptions();
		//find unprocessed batch requests
		findProcessLevel();
		cleanOldRecords();
	}catch(err){
		msg = {
			status:"Error",
			message:"[processData] Error Processing Data",
			description: err
		};
		setImmediate(GLOBAL.sendEmail,msg);
	}
}
function findProgressMailOptions()
{
	var query = {
		"progress":0
	};
	GLOBAL.emailData.find(query,function(err,docs){
		if(err==null)
		{
			for(var x = 0;x < docs.length;x++)
			{
				recoverSendingEmail(docs[x].mailOptions,docs[x].session_id,docs[x].batch_id,docs[x].target_user_id);
			}
		}else{
			msg = {
				status:"Error",
				message:"[findProgressMailOptions] Error Finding Data",
				description: err
			};
			setImmediate(GLOBAL.sendEmail,msg);
		}
	});
}
//TO BE CREATED

function cleanOldRecords()
{
	if(process.env.NODE_ENV!='development' && process.env.NODE_ENV!='localdev')
    {
        var now = new time.Date();
        now.setTimezone("America/Los_Angeles");
        var deleteDate = new time.Date();
        now.setTimezone("America/Los_Angeles");
    }else{
        var now = new Date();
        var deleteDate = new Date();
    }
    deleteDate.setDate(now.getDate() - 7);//7 days before
    var deleteDate_unix = Math.floor(deleteDate.getTime() / 1000);

    GLOBAL.emailData.remove({"newUnixDate":{$lte:deleteDate_unix}});

    setTimeout(cleanOldRecords, 86400000);
}

function findProcessLevel()
{
	var query = {
		process_level:0
	};
	GLOBAL.emailRecords.find(query,function(err,docs){
		if(err==null)
		{
			for(var x = 0;x < docs.length;x++)
			{
				single_processData(docs[x]);
			}
		}else{
			msg = {
				status:"Error",
				message:"[findProcessLevel] Error Finding Data",
				description: err
			};
			setImmediate(GLOBAL.sendEmail,msg);
		}
	});
}
var single_processData = function(sdoc)
{
	try{
		//process single data sdoc (saved information from the API)
		// {
		// 	process_level:0,
		// 	action_owner:parseInt(dataStruct.user_id),
		// 	data:{
			//    "verb":<type of verb>,
			//    "email_html":"string of xml/html data with pre-defined string replacements"
			// }
		// }
		if(typeof(sdoc.action_owner)=='undefined' || isNaN(parseInt(sdoc.action_owner))==true || sdoc.action_owner==null)
		{
			console.log("Marking Job as done due to NaN user_id");
			updateProgress(sdoc,1,batch_id);
		}else{
			var batch_id = uuid.v1(); 
			//var choice = verbAnalyzer(sdoc);
			sdoc.emailCodeType = insertType(sdoc);
			// switch(choice)
			// {
			// 	case 1://get profile followers
			// 		getFollowers(sdoc,batch_id);
			// 		break;
			// 	case 1.1:
			// 		//getFollowers(sdoc,batch_id);
			// 		getProfileCollectionFollowers(sdoc,batch_id);
			// 		break;
			// 	case 2://get shop followers
			// 		getShopFollowers(sdoc,batch_id);
			// 		break;
			// 	case 2.1:
			// 		//getShopFollowers(sdoc,batch_id);
			// 		getShopCollectionFollowers(sdoc,batch_id);
			// 		break;
			// 	default://single email to action_owner
			// 		email_single(sdoc);
			// 		break;
			// }


			//get action_ownerInfo
			var query = "SELECT * FROM cs_users WHERE user_id="+sdoc.action_owner+";";
			GLOBAL.db_cs_common.query(query,function(err,rows, fields){
				if(err==null)
				{
					setImmediate(paramAnalyser,sdoc,rows[0],batch_id);
				}else{
					msg = {
						status:"Error",
						message:"[getNames] Can't find owner id",
						description: err,
						query:query
					};
					setImmediate(GLOBAL.sendEmail,msg);
				}
			});
			
		}
	}catch(err){
		//console.log("captured error");
		msg = {
			status:"Error",
			message:"[single_processData] Error Processing Data",
			description: err
		};
		setImmediate(GLOBAL.sendEmail,msg);
	}
}

/* New Method Start */

function paramAnalyser(sdoc,owner_info,batch_id)
{
	/*
		process_level: 0,
		date: 1440559740,
		date_simple: '2015-08-26 11:29:00',
		action_owner: 0,
		data:{
		   "user_id":int, (owner of the action or owner of the store)
		   "verb":<type of verb>,
		   "email_action":[<array of strings>],("UF","UCF","SF","SCF")
		   "email_subject":string only,
		   "email_html":"string of xml/html data with pre-defined string replacements", (should be PHP base64encoded value)(Collection Followers Content)
		=======optional==============
		   "email_subject2":string only,(Shop or User Followers Content)
		   "email_html2":"string of xml/html data with pre-defined string replacements", (should be PHP base64encoded value)(Shop or User Followers Content)
		   "collection_id":int, (will email collection followers for user_id),
		   "store_id":int, (will need this if this is a follower of a specific collection_id in a store)  
		}
	*/
	var storeQuery = true;
	// UF = user profile followers = cs_users_followers table; user_id = int; no collection_id; no store_id

	// UCF = user profile collection followers = cs_users_collections_followers table; user_id = int; collection_id = int; store_id = 0

	// SF = shop follower = cs_users_collections_followers table; user_id = int; collection_id = 0 ; store_id = int

	// SCF = shop collection follower  = cs_users_collections_followers table; user_id = int; collection_id = int; store_id = int

	if(sdoc.data.length>1)
	{
		if(sdoc.data.email_action.toUpperCase().indexOf("UCF") === -1)
		{
			//console.log("triggering EmailBothCollectionAndProfileFollowers");
			EmailBothCollectionAndProfileFollowers(sdoc,owner_info,batch_id);
		}
		if(sdoc.data.email_action.toUpperCase().indexOf("SCF") === -1)
		{
			//console.log("triggering EmailBothShopCollectionAndShopFollowers");
			EmailBothShopCollectionAndShopFollowers(sdoc,owner_info,batch_id);
		}
	}else{
		switch(sdoc.data.email_action[0].toUpperCase())
		{
			case "UF":
				//console.log("triggering EmailProfileFollowers");
				EmailProfileFollowers(sdoc,owner_info,batch_id);
				break;
			case "UCF":
			case "SF":
			case "SCF":
				//console.log("triggering EmailShopFollowers");
				EmailShopFollowers(sdoc,owner_info,batch_id)
				break;
			default:
				console.log("NO EMAIL ACTION UNDERSTOOD");
				break;
		}
	}
}
function createStreamConnection()
{
	if(process.env.NODE_ENV=='development'){ //development or local machine
		var db_init = {
			host     : 'localhost',
			user     : 'root',
			password : '',
			database : ''
		};
	}else if(process.env.NODE_ENV=='localdev'){
		var db_init = {
			host     : 'localhost',
			user     : 'root',
			password : '3##@#u3!',
			database : ''
		};
	}else if(process.env.NODE_ENV=='staging'){ 
		var db_init = {
			host     : 'localhost',
			user     : 'mymugzy',
			password : 'mymugzy2009',
			database : ''
		};
	}else{ //production
		var db_init = {
			host     : '104.239.220.252',
			user     : 'mymugzy',
			password : 'mymugzy!2009',
			database : ''
		};
	}
	var temp_stream_connection;
	try{
		temp_stream_connection.end(function(err){
			console.log("error terminating stream connection");
			console.log(err);
			temp_stream_connection.destroy();
		});
		console.log("ending STREAM MySQL connection");
	}catch(err){
		console.log("No MySQL STREAM Connection to Close");
	}
	console.log("creating new mysql stream connection");
	console.log(mytime.MySQLDateTimeNOWDisplay());
	db_init.database = 'cs_common';
	temp_stream_connection = mysql.createConnection(db_init);

	temp_stream_connection.connect(function(err) {              // The server is either down
    if(err) {                                     // or restarting (takes a while sometimes).
	      console.log('error when connecting to STREAM db:', err);
	      setTimeout(createStreamConnection, 2000); // We introduce a delay before attempting to reconnect,
	    }                                     // to avoid a hot loop, and to allow our node script to
	  });                                     // process asynchronous requests in the meantime.
	                                          // If you're also serving http, display a 503 error.
	temp_stream_connection.on('error', function(err) {
		console.log('db STREAM error', err);
		if(err.code === 'PROTOCOL_CONNECTION_LOST') { // Connection to the MySQL server is usually
			createStreamConnection();                         // lost due to either server restart, or a
		}  else if(err.code === 'PROTOCOL_ENQUEUE_AFTER_FATAL_ERROR'){
			createStreamConnection();
		} else {                                      // connnection idle timeout (the wait_timeout
		  throw err;                                  // server variable configures this)
		}
	});
	return temp_stream_connection;
}
function EmailBothShopCollectionAndShopFollowers(sdoc,owner_info,batch_id)
{
	try{
		//cs_users_followers
		var query = "SELECT * FROM cs_users_collections_followers WHERE (user_id="+parseInt(sdoc.action_owner)+" AND store_id="+parseInt(sdoc.data.store_id)+" AND collection_id="+parseInt(sdoc.data.collection_id)+" ) OR (user_id="+parseInt(sdoc.action_owner)+" AND store_id="+parseInt(sdoc.data.store_id)+" AND collection_id=0 ) GROUP BY follower_id ORDER BY follower_id ASC;";
		//console.log("getShopFollowers Query =================");
		//console.log(query);
		
		var connection = createStreamConnection();
		var queryHandler = connection.query(query);
		queryHandler
			.on('error', function(err) {
				// Handle error, an 'end' event will be emitted after this as well
				// do nothing since the updateProgress value is still 0 so this will be processed back in the future
				// when the server restarts
				msg = {
					status:"Error",
					message:"[EmailBothShopCollectionAndShopFollowers] Error query DB",
					description: err,
					query:query
				};
				setImmediate(GLOBAL.sendEmail,msg);
			})
			// .on('fields', function(fields) {
			// 	// the field packets for the rows to follow
			// })
			.on('result', function(row) {
				// Pausing the connnection is useful if your processing involves I/O
				connection.pause();
				//console.log("triggering checkIfShopFollower");
				checkIfShopFollower(row,owner_info,sdoc,batch_id,connection);
				//getNameStream(row,owner_info,sdoc,batch_id,connection);
				//connection.resume();
			})
			.on('end', function() {
				// all rows have been received
				updateProgress(sdoc,1,batch_id);
				//console.log("End Connection");
				connection.end(function(err){
					console.log("error disconnecting");
					console.log(err);
					connection.destroy();
				});
			});
	}catch(err){
		msg = {
			status:"Error",
			message:"[EmailBothShopCollectionAndShopFollowers] Error Processing Data",
			description: err
		};
		setImmediate(GLOBAL.sendEmail,msg);
	}
}
function EmailBothCollectionAndProfileFollowers(sdoc,owner_info,batch_id)
{
	try{
		//cs_users_followers
		var query = "SELECT * FROM cs_users_collections_followers WHERE (user_id="+parseInt(sdoc.action_owner)+" AND store_id=0 AND collection_id="+parseInt(sdoc.data.collection_id)+" ) GROUP BY follower_id ORDER BY follower_id ASC;";
		//console.log("EmailBothCollectionAndProfileFollowers Query =================");
		//console.log(query);
		
		var connection = createStreamConnection();
		var queryHandler = connection.query(query);
		queryHandler
			.on('error', function(err) {
				// Handle error, an 'end' event will be emitted after this as well
				// do nothing since the updateProgress value is still 0 so this will be processed back in the future
				// when the server restarts
				msg = {
					status:"Error",
					message:"[EmailBothCollectionAndProfileFollowers] Error query DB",
					description: err,
					query:query
				};
				setImmediate(GLOBAL.sendEmail,msg);
			})
			// .on('fields', function(fields) {
			// 	// the field packets for the rows to follow
			// })
			.on('result', function(row) {
				// Pausing the connnection is useful if your processing involves I/O
				//console.log("triggering checkIfUserProfileFollower");
				connection.pause();
				checkIfUserProfileFollower(row,owner_info,sdoc,batch_id,connection);
				//getNameStream(row,owner_info,sdoc,batch_id,connection);
				//connection.resume();
			})
			.on('end', function() {
				// all rows have been received
				//console.log("ending Connection");
				connection.end(function(err){
					console.log("error disconnecting");
					console.log(err);
					connection.destroy();
				});
				EmailProfileFollowersAfterCollection(sdoc,owner_info,batch_id);//OR (user_id="+parseInt(sdoc.action_owner)+" AND store_id=0 AND collection_id=0 )
			});
	}catch(err){
		msg = {
			status:"Error",
			message:"[EmailBothCollectionAndProfileFollowers] Error Processing Data",
			description: err
		};
		setImmediate(GLOBAL.sendEmail,msg);
	}
}
function EmailProfileFollowersAfterCollection(sdoc,owner_info,batch_id)//OR (user_id="+parseInt(sdoc.action_owner)+" AND store_id=0 AND collection_id=0 )
{
	try{
		//cs_users_followers
		var query = "SELECT * FROM cs_users_followers WHERE user_id="+parseInt(sdoc.action_owner)+" GROUP BY follower_id ORDER BY follower_id ASC;";
		//console.log("EmailProfileFollowersAfterCollection Query =================");
		//console.log(query);
		
		var connection = createStreamConnection();
		var queryHandler = connection.query(query);
		queryHandler
			.on('error', function(err) {
				// Handle error, an 'end' event will be emitted after this as well
				// do nothing since the updateProgress value is still 0 so this will be processed back in the future
				// when the server restarts
				msg = {
					status:"Error",
					message:"[EmailProfileFollowersAfterCollection] Error query DB",
					description: err,
					query:query
				};
				setImmediate(GLOBAL.sendEmail,msg);
			})
			// .on('fields', function(fields) {
			// 	// the field packets for the rows to follow
			// })
			.on('result', function(row) {
				// Pausing the connnection is useful if your processing involves I/O
				//console.log("triggering checkIfUserEmailed");
				connection.pause();
				checkIfUserEmailed(row,owner_info,sdoc,batch_id,connection);
				//getNameStream(row,owner_info,sdoc,batch_id,connection);
				//connection.resume();
			})
			.on('end', function() {
				// all rows have been received
				//console.log("end Connection");
				updateProgress(sdoc,1,batch_id);
				connection.end(function(err){
					console.log("error disconnecting");
					console.log(err);
					connection.destroy();
				});
			});
	}catch(err){
		msg = {
			status:"Error",
			message:"[EmailProfileFollowersAfterCollection] Error Processing Data",
			description: err
		};
		setImmediate(GLOBAL.sendEmail,msg);
	}
}
function EmailShopFollowers(sdoc,owner_info,batch_id)
{
	try{
		//cs_users_followers
		var query = "SELECT * FROM cs_users_collections_followers WHERE user_id="+parseInt(sdoc.action_owner)+" AND store_id="+parseInt(sdoc.data.store_id)+" AND collection_id="+parseInt(sdoc.data.collection_id)+" ORDER BY follower_id ASC;";
		//console.log("EmailShopFollowers Query =================");
		//console.log(query);
		// GLOBAL.db_cs_common.query(query,function(err,rows, fields){
		// 	if(err==null)
		// 	{
		// 		getNames(rows,sdoc,batch_id);
		// 		updateProgress(sdoc,1,batch_id);
		// 	}else{
		// 		msg = {
		// 			status:"Error",
		// 			message:"[getShopFollowers] Error query DB",
		// 			description: err,
		// 			query:query
		// 		};
		// 		setImmediate(GLOBAL.sendEmail,msg);
		// 	}
		// });
		var connection = createStreamConnection();
		var queryHandler = connection.query(query);
		queryHandler
			.on('error', function(err) {
				// Handle error, an 'end' event will be emitted after this as well
				// do nothing since the updateProgress value is still 0 so this will be processed back in the future
				// when the server restarts
				msg = {
					status:"Error",
					message:"[EmailShopFollowers] Error query DB",
					description: err,
					query:query
				};
				setImmediate(GLOBAL.sendEmail,msg);
			})
			// .on('fields', function(fields) {
			// 	// the field packets for the rows to follow
			// })
			.on('result', function(row) {
				// Pausing the connnection is useful if your processing involves I/O
				//console.log("triggering getNameStream");
				connection.pause();
				getNameStream(row,owner_info,sdoc,batch_id,connection);
				//connection.resume();
			})
			.on('end', function() {
				// all rows have been received
				//console.log("ending connection");
				updateProgress(sdoc,1,batch_id);
				connection.end(function(err){
					console.log("error disconnecting");
					console.log(err);
					connection.destroy();
				});
			});
	}catch(err){
		msg = {
			status:"Error",
			message:"[EmailShopFollowers] Error Processing Data",
			description: err
		};
		setImmediate(GLOBAL.sendEmail,msg);
	}
}

function EmailProfileFollowers(sdoc,owner_info,batch_id)
{
	try{
		//cs_users_followers
		var query = "SELECT * FROM cs_users_followers WHERE user_id="+parseInt(sdoc.action_owner)+" ORDER BY follower_id ASC;";
		//console.log("EmailProfileFollowers Query =================");
		//console.log(query);
		// GLOBAL.db_cs_common.query(query,function(err,rows, fields){
		// 	if(err==null)
		// 	{
		// 		getNames(rows,sdoc,batch_id);
		// 		updateProgress(sdoc,1,batch_id);
		// 	}else{
		// 		msg = {
		// 			status:"Error",
		// 			message:"[getFollowers] Error query DB",
		// 			description: err,
		// 			query:query
		// 		};
		// 		setImmediate(GLOBAL.sendEmail,msg);
		// 	}
		// });
		var connection = createStreamConnection();
		var queryHandler = connection.query(query);
		queryHandler
			.on('error', function(err) {
				// Handle error, an 'end' event will be emitted after this as well
				// do nothing since the updateProgress value is still 0 so this will be processed back in the future
				// when the server restarts
				msg = {
					status:"Error",
					message:"[EmailProfileFollowers] Error query DB",
					description: err,
					query:query
				};
				setImmediate(GLOBAL.sendEmail,msg);
			})
			// .on('fields', function(fields) {
			// 	// the field packets for the rows to follow
			// })
			.on('result', function(row) {
				// Pausing the connnection is useful if your processing involves I/O
				//console.log("triggering getNameStream");
				connection.pause();
				getNameStream(row,owner_info,sdoc,batch_id,connection);
				//connection.resume();
			})
			.on('end', function() {
				// all rows have been received
				//console.log("ending connection");
				updateProgress(sdoc,1,batch_id);
				connection.end(function(err){
					console.log("error disconnecting");
					console.log(err);
					connection.destroy();
				});
			});
	}catch(err){
		msg = {
			status:"Error",
			message:"[EmailProfileFollowers] Error Processing Data",
			description: err
		};
		setImmediate(GLOBAL.sendEmail,msg);
	}
}

function checkIfUserEmailed(row,owner_info,sdoc,batch_id,connection)
{
	
	/*
	GLOBAL.emailData

	var setVal = {
			"session_id":session_id,
			"batch_id":batch_id,
			"target_user_id":target_user_id,
			"newSimpleDate":'',
			"newUnixDate":'',
			"timezone":"America/Los_Angeles",
			"progress":parseInt(progress),
			"mailOptions":mailOptions
		};
	*/
	//check if target_user_id has already been emailed for this batch_id
	var query = {
		"target_user_id":parseInt(cs_users_row.follower_id),
		"batch_id":batch_id
	};

	GLOBAL.emailData.findOne(query,function(err,doc){
		var copyOfsdoc = JSON.stringify(sdoc);
	
		copyOfsdoc.data.email_subject=copyOfsdoc.data.email_subject2;
		copyOfsdoc.data.email_html=copyOfsdoc.data.email_html2;

		if(err==null)
		{
			if(doc==null)
			{
				getNameStream(row,owner_info,copyOfsdoc,batch_id,connection);
			}else{
				//found record
				console.log("skipped target_user_id: "+cs_users_row.follower_id);
				connection.resume();
			}
		}else{
			getNameStream(row,owner_info,copyOfsdoc,batch_id,connection);
		}
	});
}
function checkIfShopFollower(row,owner_info,sdoc,batch_id,connection)
{
	var query = "SELECT * FROM cs_users_collections_followers WHERE user_id="+parseInt(sdoc.action_owner)+" AND store_id="+parseInt(sdoc.data.store_id)+" AND collection_id=0 AND follower_id="+parseInt(row.follower_id)+" ORDER BY follower_id ASC;";
	GLOBAL.db_cs_common.query(query,function(err,rows, fields){
		if(err==null)
		{
			var copyOfsdoc = JSON.stringify(sdoc);
			if(rows.length>0)//zero cause I'm looking at the cs_users_followers
			{
				//use email_subject2
				//use email_html2
				copyOfsdoc.data.email_subject=copyOfsdoc.data.email_subject2;
				copyOfsdoc.data.email_html=copyOfsdoc.data.email_html2;
			}else{
				//do nothing (use email_subject and email_html)
			}
			getNameStream(row,owner_info,copyOfsdoc,batch_id,connection)
		}else{
			msg = {
				status:"Error",
				message:"[checkIfUserProfileFollower] Error query DB",
				description: err,
				query:query
			};
			setImmediate(GLOBAL.sendEmail,msg);
			connection.resume();
		}
	});
}

function checkIfUserProfileFollower(row,owner_info,sdoc,batch_id,connection)
{
	var query = "SELECT * FROM cs_users_followers WHERE user_id="+parseInt(sdoc.action_owner)+" AND follower_id="+parseInt(row.follower_id)+" ORDER BY follower_id ASC;";
	GLOBAL.db_cs_common.query(query,function(err,rows, fields){
		if(err==null)
		{
			var copyOfsdoc = JSON.stringify(sdoc);
			if(rows.length>0)//zero cause I'm looking at the cs_users_followers
			{
				//use email_subject2
				//use email_html2
				copyOfsdoc.data.email_subject=copyOfsdoc.data.email_subject2;
				copyOfsdoc.data.email_html=copyOfsdoc.data.email_html2;
			}else{
				//do nothing (use email_subject and email_html)
			}
			getNameStream(row,owner_info,copyOfsdoc,batch_id,connection)
		}else{
			msg = {
				status:"Error",
				message:"[checkIfUserProfileFollower] Error query DB",
				description: err,
				query:query
			};
			setImmediate(GLOBAL.sendEmail,msg);
			connection.resume();
		}
	});
}

function getNameStream(row,owner_info,sdoc,batch_id,connection)
{
	try
	{
		var orig_sdoc = JSON.stringify(sdoc);
		orig_sdoc = eval("("+orig_sdoc+")");
	}catch(err){
		console.log("Error occurred in converting to JSON");
		console.log(err);
	}
	
	//console.log("triggering getUserEmail");
	getUserEmail(row,owner_info,orig_sdoc,sdoc,batch_id,connection);
}


function getUserEmail(cs_users_row,owner_info,orig_sdoc,sdoc,batch_id,connection)
{
	var query = "SELECT * FROM cs_users WHERE user_id="+cs_users_row.follower_id+";";
	//console.log("getUserEmail Query =================");
	//console.log(query);
	GLOBAL.db_cs_common.query(query,function(err,rows, fields){
		if(err==null)
		{
			if(rows.length>0)
			{
				console.log("Emailing Followers: "+rows[0].email);
				emailFollowers(rows[0],owner_info,orig_sdoc,sdoc,batch_id,connection);
			}else{
				console.log("No Data Found for user_id: "+cs_users_row.follower_id);
				connection.resume();
			}
		}else{
			msg = {
				status:"Error",
				message:"[getUserEmail] Error query DB",
				description: err,
				query:query
			};
			//console.log("ERROR in getUserEmail");
			setImmediate(GLOBAL.sendEmail,msg);
			connection.resume();
		}
	});
}

function emailFollowers(cs_users_row,owner_info,orig_sdoc,sdoc,batch_id,connection)
{
	/*
	cs_users_row
	{ collection_id: 0,
    user_id: 2,
    store_id: 1,
    follower_id: 2,
    date_followed: Thu Apr 23 2015 10:07:27 GMT-0700 (PDT) }
	*/
	//check if this user has been emailed by this batch
	console.log('emailing :'+util.inspect(owner_info,{showHidden: false, depth: null}));
	var query = {
		"batch_id":batch_id,
		"mailOptions.to":cs_users_row.email
	};
	GLOBAL.emailData.find(query,function(err,docs){
		try{
			if(docs.length>0)
			{
				//do not email
				console.log("email cancelled");
				connection.resume();
			}else{
				//email
				console.log("sending email");
				//getUserEmail(cs_users_row,owner_info,orig_sdoc,sdoc,batch_id);
				replaceAndEmail(cs_users_row,owner_info,orig_sdoc,sdoc,batch_id,connection);
			}
		}catch(err){
			replaceAndEmail(cs_users_row,owner_info,orig_sdoc,sdoc,batch_id,connection);
		}
	});
}

function replaceAndEmail(cs_users_row,owner_info,orig_sdoc,sdoc,batch_id,connection)
{
	console.log("replacing words");
	//url decode orig_sdoc.data.email_html
	orig_sdoc.data.email_html = uriTools.base64_decode(orig_sdoc.data.email_html);
	//end
	// console.log("original_sdoc:");
	// console.log(sdoc);
	// console.log("copy sdoc:");
	// console.log(orig_sdoc);
	orig_sdoc.data.email_html = replaceME(orig_sdoc.data.email_html,"{{first name follower}}",cs_users_row.firstname);
	orig_sdoc.data.email_html = replaceME(orig_sdoc.data.email_html,"{{last name follower}}",cs_users_row.lastname);
	if(typeof(owner_info)!='undefined')
	{
		if(typeof(owner_info.firstname)!='undefined')
		{
			orig_sdoc.data.email_html = replaceME(orig_sdoc.data.email_html,"{{first name owner}}",owner_info.firstname);
			orig_sdoc.data.email_html = replaceME(orig_sdoc.data.email_html,"{{last name owner}}",owner_info.lastname);
		}
	}

	orig_sdoc.data.email_html = replaceME(orig_sdoc.data.email_html,"{{follower email}}",cs_users_row.email);
	orig_sdoc.data.email_html = replaceME(orig_sdoc.data.email_html,"{{to email}}",cs_users_row.email);
	orig_sdoc.data.email_html = replaceME(orig_sdoc.data.email_html,"{{to username}}",cs_users_row.username);


	orig_sdoc.data.email_subject = replaceME(orig_sdoc.data.email_subject,"{{first name follower}}",cs_users_row.firstname);
	orig_sdoc.data.email_subject = replaceME(orig_sdoc.data.email_subject,"{{last name follower}}",cs_users_row.lastname);

	if(typeof(owner_info)!='undefined')
	{
		if(typeof(owner_info.firstname)!='undefined')
		{
			orig_sdoc.data.email_subject = replaceME(orig_sdoc.data.email_subject,"{{first name owner}}",owner_info.firstname);
			orig_sdoc.data.email_subject = replaceME(orig_sdoc.data.email_subject,"{{last name owner}}",owner_info.lastname);
		}
	}
	
	var session_id = uuid.v1();
	console.log("=========== Emailing : "+cs_users_row.email+"===========");
	sendEmailNow(orig_sdoc,cs_users_row.email, session_id,batch_id,cs_users_row,connection);
}

function replaceME(body,searchMask,value)
{
	// console.log("body info:");
	// console.log(body);
	var regEx = new RegExp(searchMask, "ig");
	var replaceMask = value;
	try{
		return body.replace(regEx, replaceMask);
	}catch(err){
		console.log(err);
		return body;
	}
				
}

function sendEmailNow(sdoc,targetEmail,session_id,batch_id,cs_users_row,connection)
{
	var mailOptions = {
	    from: 'ShopStyx <noreply@shopstyx.com>', // sender address
	    to: targetEmail, // list of receivers
	    subject: sdoc.data.email_subject, // Subject line
	    //text: message // plaintext body
	    html: sdoc.data.email_html // html body
	};
	//console.log(mailOptions);
	//save emailData with progress 0
	updateMailOptions(session_id,mailOptions,0,batch_id,parseInt(cs_users_row.user_id));
	
	//check if setting allows sending of email
	var query={
		"user_id":parseInt(cs_users_row.user_id)
	};
	GLOBAL.accountSetting.findOne(query,function(err,doc){
		if(err==null)
		{
			var returnVal = -1;
			if(doc!=null)
			{
				var notificationBlock = doc.notificationBlock.slice();
				
				if(typeof(sdoc.emailCodeType)=="string")
				{
					returnVal = notificationBlock.indexOf(sdoc.emailCodeType);
				}
				if(typeof(sdoc.emailCodeType)=="object")
				{
					var found =false;
					for(var x=0;x<sdoc.emailCodeType.length;x++)
					{
						if(notificationBlock.indexOf(sdoc.emailCodeType[x])!=-1)
						{
							found = true;
						}
					}
					if(found==true)
					{
						returnVal=0;
					}
				}
				
			}

			if(returnVal==-1)
			{
				// send mail with defined transport object
				transporter.sendMail(mailOptions, function(error, info){
				    if(error){
				        console.log(error);
				        //try to resend 30 minutes after
				        if(error!='"Recipients" addresses missing' && error!='Message sent: undefined')
				        {
				        	if(typeof(error.code)!='undefined')
				        	{
				        		if(error.code=='EENVELOPE')
				        		{
				        			console.log("Terminating Resend due to EENVELOPE");
				        			updateMailOptions(session_id,mailOptions,1,batch_id,parseInt(cs_users_row.user_id));
				        		}else{
				        			console.log('setting mail for resend');//sdoc,targetEmail,session_id,batch_id,cs_users_row,connection
				        			setTimeout(sendEmailNow, 30 * 60 * 1000,sdoc,targetEmail,session_id,batch_id,cs_users_row,null);
				        		}
				        	}else{
				        		console.log('setting mail for resend');
				        		setTimeout(sendEmailNow, 30 * 60 * 1000,sdoc,targetEmail,session_id,batch_id,cs_users_row,null);
				        	}
				        	if(connection!=null)
				        	{
				        		connection.resume();
				        	}
				        	
				        }else{
				        	console.log("Terminating Resend");
				        	updateMailOptions(session_id,mailOptions,1,batch_id,parseInt(cs_users_row.user_id));
				        	if(connection!=null)
				        	{
				        		connection.resume();
				        	}
				        }
				    }else{
				        console.log('Message sent: ' + info.response);
				        updateMailOptions(session_id,mailOptions,1,batch_id,parseInt(cs_users_row.user_id));
				        if(connection!=null)
			        	{
			        		connection.resume();
			        	}
				    }
				});
			}else{
				console.log("Terminating Sending Email due to settings BLOCK");
				updateMailOptions(session_id,mailOptions,1,batch_id,parseInt(cs_users_row.user_id));
				if(connection!=null)
	        	{
	        		connection.resume();
	        	}
			}
		}else{
			msg = {
				status:"Error",
				message:"[sendEmailNow] Error Processing Data",
				description: err
			};
			setImmediate(GLOBAL.sendEmail,msg);
			if(connection!=null)
        	{
        		connection.resume();
        	}
		}
	});
}
						  //session_id,mailOptions,1,batch_id
function updateMailOptions(session_id,mailOptions,progress,batch_id,target_user_id)
{
	console.log("logging mailtransport");
	var query = {
		"session_id":session_id
	};
	if(batch_id!=null)
	{
		var setVal = {
			"session_id":session_id,
			"batch_id":batch_id,
			"target_user_id":target_user_id,
			"newSimpleDate":'',
			"newUnixDate":'',
			"timezone":"America/Los_Angeles",
			"progress":parseInt(progress),
			"mailOptions":mailOptions
		};
	}else{
		var setVal = {
			"session_id":session_id,
			"batch_id":batch_id,
			"target_user_id":target_user_id,
			"progress":parseInt(progress),
			"newSimpleDate":'',
			"newUnixDate":'',
			"timezone":"America/Los_Angeles",
			"mailOptions":mailOptions
		};
	}
	//console.log(query);
	//console.log(setVal);
	var newSimpleDate = mytime.MySQLDateTimeNOW(setVal);
	setVal.newSimpleDate = newSimpleDate;
	GLOBAL.emailData.update(query,{"$set":setVal},{"upsert":true});
}
function updateProgress(sdoc,progressLevel,batch_id)
{
	// var mydata = {
	// 		process_level:0,
	// 		action_owner:parseInt(dataStruct.user_id),
	// 		data:dataJSON
	// 	};
	console.log("marking mother job as done");
	var targetID = mongojs.ObjectId((sdoc._id).toString());
	var query = {
		"_id":targetID
	};
	var setVal = {
		"process_level":parseInt(progressLevel)
	};
	//console.log(query);
	//console.log(setVal);
	GLOBAL.emailRecords.update(query,{"$set":setVal});
}

function insertType(sdoc)
{
	var returnVal = "NOBLOCKER";
	switch(sdoc.data.verb.toLowerCase())
	{
		case 'add-product-external': //profile	
			returnVal = ["PA_NP","SA_SANP"];
			break;	
		case 'removed-product-external':
			break;
		case 'removed-collection-external':
			break;
		case 'add-product-to-collection':
			break;
		case 'remove-product-from-collection':
			break;
		case 'modified-product-external':
			break;
		case 'modified-collection-external':
			break;
		case 'added-new-collection-external':
			returnVal = ["PA_NC","SA_SANC"];
			break;
		case 'opened-new-store':
			returnVal = "SA_NSUF";
			break;
		case 'claimed-store':
			returnVal = "SA_SCUF";			
			break;
		case 'add-product-internal': //shop
			returnVal = ["PA_NP","SA_SANP"];
			break;	
		case 'removed-product-internal':
			break;
		case 'removed-collection-internal':
			break;
		case 'removed-store-internal':		
			break;
		case 'modified-product-internal':
			break;
		case 'modified-collection-internal':
			break;
		case 'added-new-collection-internal':
			returnVal = ["PA_NC","SA_SANC"];
			break;
		case 'modified-store':			
			break;
		case 'profile-new-collection-and-product-fromshoptyx':// - (UF, UCF) email collection_followers or profile followers for newly added product on a newly added collection
			returnVal = ["PA_NC","PA_NP"];
			break;
		case 'profile-new-collection-and-product-fromweb':// - (UF, UCF) email collection_followers or profile followers for newly added product on a newly added collection
			returnVal = ["PA_NC","PA_NP"];
			break;
		case 'profile-new-product-fromweb-oncollection':// - (UF, UCF) email collection_followers or profile followers for the newly added product on a certain collection
			returnVal = "PA_NP";
			break;
		case 'profile-new-product-fromshoptyx-oncollection':// - (UF, UCF) email collection_followers or profile followers for the newly added product on a certain collection
			returnVal = "PA_NP";
			break;
		case 'shop-new-collection-and-product':// - (SF, SCF) email collection_followers or shop followers for newly added product on a newly added collection
			returnVal = ["SA_SANP","SA_SANC"]
			break;
		case 'shop-new-product-on-collection':// - (SF, SCF) email collection_followers or shop followers for the newly added product on a certain collection
			returnVal = "SA_SANP";
			break;
		default:
			break;
	}
	return returnVal;
}

function recoverSendingEmail(mailOptions,session_id,batch_id,target_user_id)
{
	// send mail with defined transport object
	transporter.sendMail(mailOptions, function(error, info){
	    if(error){
	        console.log(error);
	        //try to resend 30 minutes after
	        if(error!='"Recipients" addresses missing' && error!='Message sent: undefined')
	        {
	        	if(typeof(error.code)!='undefined')
	        	{
	        		if(error.code=='EENVELOPE')
	        		{
	        			console.log("Terminating Resend due to EENVELOPE");
	        			updateMailOptions(session_id,mailOptions,1,batch_id,target_user_id);//session_id,mailOptions,progress,batch_id)
	        		}else{
	        			console.log('setting mail for resend');
	        			setTimeout(recoverSendingEmail, 30 * 60 * 1000,mailOptions,session_id,batch_id,target_user_id);
	        		}
	        	}else{
	        		console.log('setting mail for resend');
	        		setTimeout(recoverSendingEmail, 30 * 60 * 1000,mailOptions,session_id,batch_id,target_user_id);
	        	}
	        	
	        }else{
	        	console.log("Terminating Resend");
	        	updateMailOptions(session_id,mailOptions,1,batch_id,target_user_id);
	        }
	    }else{
	        console.log('Message sent: ' + info.response);
	        updateMailOptions(session_id,mailOptions,1,batch_id,target_user_id);
	    }
	});
}
/*New Method End*/

module.exports = {
	processData:processData,
	single_processData:single_processData
}