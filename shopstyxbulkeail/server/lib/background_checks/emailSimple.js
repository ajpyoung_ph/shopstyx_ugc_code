var nodemailer = require('nodemailer');
//var transporter = nodemailer.createTransport();
var smtpTransport = require('nodemailer-smtp-transport');
var transporter = nodemailer.createTransport(smtpTransport({
    host: 'smtp.mailgun.org',
    port: 587,
    auth: {
        user: 'postmaster@shopstyx.com',
        pass: '0fkpo68eh3t4'
    }
}));
var mongojs = require('mongojs');
var uuid = require('node-uuid');
var uriTools = require('../helpers/uriTools');

/*
	<source URL>/<token>/<user_id>/send/generic/email/
	METHOD: POST
	ACTION:CREATE/INSERT data
	DATA: JSON data
	{
	   "to":String,(email)
	   "subj":String,(subject)
	   "message":String,
	   "type":String,("html","text")
	}

*/
var collect_unsentGenericEmail=function()
{
	var query = {
		process_level:0
	};
	GLOBAL.genericEmail.find(query,function(err,docs){
		if(docs.length>0)
		{
			for(var x=0;x<docs.length;x++)
			{
				emailStart(docs[x]);
			}
		}
	});
}

var emailStart = function(mydata)
{
	////"_id":mongojs.ObjectId((doc._id).toString())
	var string_id = (mydata._id).toString();
	/*
	var mydata = {
			session_id:uuid.v1(),
			process_level:0,
			data:dataJSON
		};
	*/
	var mailOptions = {
	    from: 'ShopStyx <noreply@shopstyx.com>', // sender address
	    to: mydata.data.to, // list of receivers
	    subject: mydata.data.subj, // Subject line
	    //text: message // plaintext body
	    //html: sdoc.data.email_html // html body
	};
	switch(mydata.data.type.toLowerCase())
	{
		case 'html':
			mailOptions.html = [];
			mailOptions.html = uriTools.base64_decode(mydata.data.message);
			break;
		case 'text':
			mailOptions.text = [];
			mailOptions.text = mydata.data.message;
			break;
	}
	if(typeof(mydata.data.cc)!='undefined')
	{
		mailOptions.cc = [];
		mailOptions.cc = mydata.data.cc;
	}
	if(typeof(mydata.data.bcc)!='undefined')
	{
		mailOptions.bcc = [];
		mailOptions.bcc = mydata.data.bcc;
	}
	sendEmailNow(mailOptions,string_id)
}

function sendEmailNow(mailOptions,string_id)
{
	// send mail with defined transport object
	transporter.sendMail(mailOptions, function(error, info){
	    if(error){
	        console.log(error);
	        //try to resend 30 minutes after
	        if(error!='"Recipients" addresses missing' && error!='Message sent: undefined')
	        {
	        	//{ [Error: No recipients defined] code: 'EENVELOPE' }
	        	if(typeof(error.code)!='undefined')
	        	{
	        		if(error.code=='EENVELOPE')
	        		{
	        			console.log("Terminating Resend due to EENVELOPE");
	        			updateMailOptions(string_id);
	        		}else{
	        			console.log('setting mail for resend');
	        			setTimeout(sendEmailNow, 30 * 60 * 1000,mailOptions,string_id);
	        		}
	        	}else{
	        		console.log('setting mail for resend');
	        		setTimeout(sendEmailNow, 30 * 60 * 1000,mailOptions,string_id);
	        	}
	        	
	        }else{
	        	console.log("Terminating Resend");
	        	updateMailOptions(string_id);
	        }
	    }else{
	        console.log('Message sent: ' + info.response);
	        updateMailOptions(string_id);
	    }
	});
}

function updateMailOptions(string_id,mailOptions,progress,batch_id)
{
	console.log("logging mailtransport");
	var query = {
		"_id":mongojs.ObjectId(string_id)
	};
	var setVal = {
		"process_level":1
	};
	//console.log(query);
	//console.log(setVal);
	GLOBAL.genericEmail.update(query,{"$set":setVal});
}

module.exports={
	emailStart:emailStart,
	collect_unsentGenericEmail:collect_unsentGenericEmail
};