var mySQLStuff = require('../../mysql_connects/mysql_convertions');
var data_integrity = require('../../background_checks/processData');
var mongojs = require('mongojs');
var util = require('util');

//{ [Error: No recipients defined] code: 'EENVELOPE' }
var saveEmailRequest = function(dataStruct)
{
	try{
		var res = dataStruct.res;
		var req = dataStruct.req;
		var dataJSON = req.body.data;

		if(typeof(dataJSON)=="string")
		{
			dataJSON = eval("("+dataJSON+")");
		}

		if(typeof(dataJSON.user_id)=='undefined' || isNaN(dataJSON.user_id)==true || dataJSON.user_id==null)
		{
			msg = {
				status:"Error",
				message:"[saveEmailRequest] No User ID sent",
				description: err
			};
			GLOBAL.sendError(msg,dataStruct.res);
		}else{
			var mydata = {
				process_level:0,
				date:dataStruct.newUnixDate,
				date_simple:dataStruct.newSimpleDate,
				action_owner:parseInt(dataJSON.user_id),
				data:dataJSON
			};
			GLOBAL.emailRecords.insert(mydata,function(err,doc){
				if(err==null)
				{
					msg = {
						status:"Success",
						message:"[saveEmailRequest] Saved Data, queued for Emailing",
						description: mydata
					};
					GLOBAL.sendError(msg,dataStruct.res);
					data_integrity.single_processData(doc);
				}else{
					msg = {
						status:"Error",
						message:"[saveEmailRequest] Error Saving Data",
						description: err
					};
					GLOBAL.sendError(msg,dataStruct.res);
				}
			});
		}
		
	}catch(err){
		msg = {
			status:"Error",
			message:"[saveEmailRequest] Error Processing Data",
			description: err
		};
		GLOBAL.sendError(msg,dataStruct.res);
	}
}
module.exports = {
	saveEmailRequest:saveEmailRequest
}