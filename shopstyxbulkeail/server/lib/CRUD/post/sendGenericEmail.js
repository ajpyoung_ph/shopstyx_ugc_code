var mySQLStuff = require('../../mysql_connects/mysql_convertions');
var data_integrity = require('../../background_checks/emailSimple');
var mongojs = require('mongojs');
var util = require('util');

var start = function(dataStruct)
{
	var res = dataStruct.res;
		var req = dataStruct.req;
		var dataJSON = req.body.data;

		if(typeof(dataJSON)=="string")
		{
			dataJSON = eval("("+dataJSON+")");
		}

		var mydata = {
			process_level:0,
			date:dataStruct.newUnixDate,
			date_simple:dataStruct.newSimpleDate,
			data:dataJSON
		};
		GLOBAL.genericEmail.insert(mydata,function(err,doc){
			if(err==null)
			{
				msg = {
					status:"Success",
					message:"[start sendGenericEmail] Saved Data, queued for Emailing",
					description: mydata
				};
				GLOBAL.sendError(msg,dataStruct.res);
				data_integrity.emailStart(doc);
			}else{
				msg = {
					status:"Error",
					message:"[start sendGenericEmail] Error Saving Data",
					description: err
				};
				GLOBAL.sendError(msg,dataStruct.res);
			}
		})
};



module.exports={
	start:start
};