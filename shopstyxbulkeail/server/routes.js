var saveRecord = require('./lib/CRUD/post/saveRecord');
var genericEmail = require('./lib/CRUD/post/sendGenericEmail')
var mySQLStuff = require('./lib/mysql_connects/mysql_convertions');
var verifyAccess = require('./lib/securityChecks');

var pmx = require('pmx');
var probe = pmx.probe();
 
var meter = probe.meter({
  name    : 'req/min',
  seconds : 60
});

module.exports = function(router){

	router.route('/:token/:user_id/bulk/email/')
		.post(function(req,res){
			validate(req,res,saveRecord.saveEmailRequest);
		});

	router.route('/:token/:user_id/send/generic/email/')
		.post(function(req,res){
			validate(req,res,genericEmail.start);
		});
	
	router.route('*')
		.get(function(req,res){
			msg = {
				status:"Error",
				message:"400 page not found",
				description: ""
			};
			GLOBAL.sendError(msg,res);
		})
		.post(function(req,res){
			msg = {
				status:"Error",
				message:"400 page not found",
				description: ""
			};
			GLOBAL.sendError(msg,res);
		})
		.put(function(req,res){
			msg = {
				status:"Error",
				message:"400 page not found",
				description: ""
			};
			GLOBAL.sendError(msg,res);
		})
		.delete(function(req,res){
			msg = {
				status:"Error",
				message:"400 page not found",
				description: ""
			};
			GLOBAL.sendError(msg,res);
		});
}

function validate(req,res,callback)
{
	var dataStruct = {
		"user_id":parseInt(req.params.user_id),
		"token":req.params.token,
		"req": req,
		"res": res,
		"newSimpleDate":'',
		"newUnixDate":'',
		"timezone":"America/Los_Angeles"
	};
	var newSimpleDate = mySQLStuff.MySQLDateTimeNOW(dataStruct);
	dataStruct.newSimpleDate = newSimpleDate;
	meter.mark();
	//currently we have store_id and user_id fixed
	if(process.env.NODE_ENV=='development')// && parseInt(req.params.token) == 0)
	{
		callback(dataStruct);
	}else{
		verifyAccess.checkToken(dataStruct,callback);
	}
}