// inital setup
var pmx = require('pmx');
//Global Exception Logging
pmx.init();
var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var logger = require('morgan'); //logger
//var events = require('events');
var	util = require('util');
var nodemailer = require('nodemailer');
//var verbHandler = require('./server/lib/eventHandler/verbHandler');
var data_integrity = require('./server/lib/background_checks/processData');
var data_integrity2 = require('./server/lib/background_checks/emailSimple');
// create reusable transporter object using SMTP transport
// var transporter = nodemailer.createTransport({
//     service: 'Gmail',
//     auth: {
//         user: 'andrewjohnyoung@gmail.com',
//         pass: 'k!#g0fk!#g$'
//     }
// });
//var transporter = nodemailer.createTransport();
var smtpTransport = require('nodemailer-smtp-transport');
var transporter = nodemailer.createTransport(smtpTransport({
    host: 'smtp.mailgun.org',
    port: 587,
    auth: {
        user: 'postmaster@shopstyx.com',
        pass: '0fkpo68eh3t4'
    }
}));
var probe = pmx.probe();
 
var shutdown_counter = probe.counter({
  name : 'bulk email shutdown'
});
	
var startAPI = function()
{
	//var minify = require('express-minify');
	//var multer = require('multer'); //handes multi-part forms
	var mySQLStuff = require('./server/lib/mysql_connects/mysql_convertions');
	var sqlConnect = require(__dirname + '/server/lib/mysql_connects/db_conf');
	//var compress = require('compression');
	GLOBAL.router = express.Router();
	var port = process.env.PORT || 8768;
	//GLOBAL.db_location = 'mapDB';
	GLOBAL.http = "http://localhost:"+port;
	GLOBAL.saved_images_location = '/images';//"/images/:shop_id/:user_id/:filename"
	// configuration ===============================================================
	var mongojs = require('mongojs');
	process.env.NODE_ENV=process.env.NODE_ENV.trim();
	var trailingArg='';
	if(process.env.NODE_ENV=='development'){ //development or local machine
		console.log("setting up development");
		GLOBAL.locationString = "localhost";
		
		//app.use(compress({threshold: 256}));
		//app.use(minify({cache: __dirname + '/cache'}));
		app.use(express.static(__dirname + '/public')); 		// set the static files location /public/img will be /img for users
		app.use(logger('combined')); 						// log every request to the console
		app.use(bodyParser.json()); 							// parse application/json
		app.use(bodyParser.urlencoded({extended:true}));		// parse application/x-www-form-urlencoded
		// app.use(multer({ dest: './uploads/',
		// 	rename: function (fieldname, filename) {
		// 		return filename.replace(/\W+/g, '');//.toLowerCase() + Date.now()
		// 	} 
	 //  	}));				// parse multipart/form-data
		var db_init = {
			host     : 'localhost',
			user     : 'root',
			password : '',
			database : ''
		};
	}else if(process.env.NODE_ENV=='localdev'){
		console.log("setting up localdev");
		GLOBAL.locationString = "localhost";
		

		//app.use(compress({threshold: 256}));
		//app.use(minify({cache: __dirname + '/cache'}));
		app.use(express.static(__dirname + '/public')); 		// set the static files location /public/img will be /img for users
		app.use(logger('combined')); 						// log every request to the console
		app.use(bodyParser.json()); 							// parse application/json
		app.use(bodyParser.urlencoded({extended:true}));		// parse application/x-www-form-urlencoded
		// app.use(multer({ dest: './uploads/',
		// 	rename: function (fieldname, filename) {
		// 		return filename.replace(/\W+/g, '');//.toLowerCase() + Date.now()
		// 	}
		//  	}));				// parse multipart/form-data
		var db_init = {
			host     : 'localhost',
			user     : 'root',
			password : '3##@#u3!',
			database : ''
		};
	}else if(process.env.NODE_ENV=='staging'){ 
		console.log("setting up staging");
		//app.use(compress({threshold: 256}));
		GLOBAL.locationString = "localhost";	
		
		//app.use(minify({cache: __dirname + '/cache'}));
		app.use(express.static(__dirname + '/public')); 		// set the static files location /public/img will be /img for users
		app.use(logger('combined')); 								// log every request to the console - default settings
		app.use(bodyParser.json()); 							// parse application/json
		app.use(bodyParser.urlencoded({extended:true}));		// parse application/x-www-form-urlencoded
		// app.use(multer({ dest: './uploads/',
		// 	rename: function (fieldname, filename) {
		// 		//return filename.replace(/\W+/g, '-').toLowerCase() + Date.now()
		// 		return filename.replace(/\W+/g, '');//.toLowerCase() + Date.now()
		// 	} 
	 //  	}));				// parse multipart/form-data
		var db_init = {
			host     : 'localhost',
			user     : 'mymugzy',
			password : 'mymugzy2009',
			database : ''
		};
	}else{ //production
		console.log("setting up production");
		GLOBAL.locationString = "104.130.206.240:27017,104.130.162.34:27017,104.239.135.106:27017";
		trailingArg = '?replicaSet=styx01';
		//GLOBAL.http = "http://localhost:"+port;
		//app.use(minify({cache: __dirname + '/cache'}));
		app.use(express.static(__dirname + '/public')); 		// set the static files location /public/img will be /img for users
		app.use(logger('combined')); 						// log every request to the console
		app.use(bodyParser.json()); 							// parse application/json
		app.use(bodyParser.urlencoded({extended:true}));		// parse application/x-www-form-urlencoded
		// app.use(multer({ dest: './uploads/',
		// 	rename: function (fieldname, filename) {
		// 		//return filename.replace(/\W+/g, '-').toLowerCase() + Date.now()
		// 		return filename.replace(/\W+/g, '');//.toLowerCase() + Date.now()
		// 	} 
	 //  	}));				// parse multipart/form-data
		// var db_init = {
		// 	host     : '44fe862ed36373986601eac2094f12ad2d6c3c8c.rackspaceclouddb.com',
		// 	user     : 'mymugzy',
		// 	password : 'mymugzy!2009',
		// 	database : ''
		// };
		var db_init = {
			host     : '104.239.220.252',
			user     : 'mymugzy',
			password : 'mymugzy!2009',
			database : ''
		};
	}
	
	//DB connection
	GLOBAL.http = "http://"+GLOBAL.locationString+":"+port;
	GLOBAL.mongodb = mongojs('bulkEmailUser:s#0pst!cks@'+GLOBAL.locationString+'/shopstyx_bulkEmail'+trailingArg,['emailRecords','emailData','genericEmail'], {authMechanism: 'ScramSHA1'});
	GLOBAL.emailRecords = GLOBAL.mongodb.collection('emailRecords');
	GLOBAL.emailData = GLOBAL.mongodb.collection('emailData');
	GLOBAL.genericEmail = GLOBAL.mongodb.collection('genericEmail');
	// GLOBAL.trackEmailSent = GLOBAL.mongodb.collection('sentBulkEmail');

	GLOBAL.mongodb2 = mongojs('notificationUser:s#0pst!cks@'+GLOBAL.locationString+'/shopstyx_notification'+trailingArg,['accountSetting'], {authMechanism: 'ScramSHA1'}); 
	GLOBAL.accountSetting = GLOBAL.mongodb2.collection('accountSetting');

	console.log(GLOBAL.http);

	GLOBAL.sendEmail = function(msg)
	{
		var message = msg;
		console.log(util.inspect(msg,{showHidden: false, depth: null}));
		var dataStruct = {
			"newSimpleDate":'',
			"newUnixDate":''
		};
		dataStruct.newSimpleDate = mySQLStuff.MySQLDateTimeNOW(dataStruct);
		if(typeof(msg)=='object')
		{
			msg['date']=[];
			msg['date_simple']=[];
			msg.date=dataStruct.newUnixDate;
			msg.date_simple=dataStruct.newSimpleDate;
			var err = new Error(JSON.stringify(msg));
   			pmx.notify(err);
			message = JSON.stringify(msg);
		}
		var mailOptions = {
		    from: 'nodeservices@shopstyx.com', // sender address
		    to: 'andrewjohnyoung@gmail.com', // list of receivers
		    subject: 'Status Error', // Subject line
		    text: message // plaintext body
		    //html: '<b>Hello world ✔</b>' // html body
		};

		// send mail with defined transport object
		transporter.sendMail(mailOptions, function(error, info){
		    if(error){
		        console.log(error);
		    }else{
		        console.log('Message sent: ' + info.response);
		    }
		});
	};
	GLOBAL.sendError = function(msg,res)
	{
		if(msg.status=="Error" && msg.message!="400 page not found")
		{
			setImmediate(GLOBAL.sendEmail,msg);
		}else{
			console.log(util.inspect(msg,{showHidden: false, depth: null}));
		}
		res.json(msg);
	};
	sqlConnect.mysqlConnect(db_init);
	//set the template engine added something
	//app.engine('.html',require('ejs').__express);
	//set the template engine's template folder
	//app.set('views',__dirname+'/public');
	//automatically set the extensions for all server side res.renders
	//app.set('view engine','html');

	//load the routes
	require('./server/routes')(GLOBAL.router);


	//CORs Registration
	//configure CORs
	app.all('*', function(req, res, next) {
		// res.header("Access-Control-Allow-Origin", "*");
		// res.header("Access-Control-Allow-Headers", "X-Requested-With");
		// next();
		res.header('Access-Control-Allow-Origin', '*');
	    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
	    res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');

	    // intercept OPTIONS method
	    if ('OPTIONS' == req.method) {
	      res.send(200);
	    }
	    else {
	      next();
	    }
	});
	//graceful reload for PM2 manager
	function close_all_connections(){
		GLOBAL.mongodb.close();
		GLOBAL.db_cs_common.end();
	}
	app.on('message', function(msg) {  
	  if (msg === 'shutdown') {
	  	shutdown_counter.inc();
	    close_all_connections();
	    //delete_logs();
	    GLOBAL.server._connections=0;
	    GLOBAL.server.close(function(){
	    	console.log("closing server");
	    });
	    process.exit(0);
	  }
	});
	// REGISTER OUR ROUTES -------------------------------
	// all of our routes will be from root /
	if(process.env.NODE_ENV.toLowerCase()=='development')
	{
		app.use('/', GLOBAL.router);
	}else{
		app.use('/api/bulkemail/', GLOBAL.router);
	}
	// Add the error middleware at the end (after route declaration)
    app.use(pmx.expressErrorHandler());
	// app.use(function(err, req, res, next){
	// 	msg = {
	// 		status:"Error",
	// 		message:"400 page not found",
	// 		description: err.stack
	// 	};
	// 	GLOBAL.sendError(msg,res);
	//   // console.error(err.stack);
	//   // res.status(500).json('Something broke!');
	// });
	//start our server
	GLOBAL.server = app.listen(port);
	console.log('starting server at port '+port);
	console.log(process.env.NODE_ENV+' mode running');
	data_integrity.processData();
	data_integrity2.collect_unsentGenericEmail();
};

module.exports = {
	startAPI:startAPI
};