var videoStyxCreate = function(dataStruct)
{
	console.log('VideoStyxStart');
	var showme = false;
	try{
		if(typeof(dataStruct.req.params.show)!='undefined')
		{
			showme = dataStruct.req.params.show;
		}
	}catch(err){
		//do nothing
	}
	
	var payload = {
		GlobalHost:dataStruct.req.protocol+"://"+dataStruct.req.get('host'),//GLOBAL.http,
		user_id:dataStruct.user_id,
		store_id:dataStruct.req.params.store_id,
		NODE_ENV:process.env.NODE_ENV,
		token:dataStruct.req.params.token,
		show:showme,
		video:true
	};
	console.log("payload");
	console.log(payload);
	dataStruct.res.render('photoStyxStartPage',payload);
};

var photoStyxCreate = function(dataStruct)
{
	console.log('PhotoStyxStart');
	var showme = false;
	try{
		if(typeof(dataStruct.req.params.show)!='undefined')
		{
			showme = dataStruct.req.params.show;
		}
	}catch(err){
		//do nothing
	}
	
	var payload = {
		GlobalHost:dataStruct.req.protocol+"://"+dataStruct.req.get('host'),//GLOBAL.http,
		user_id:dataStruct.user_id,
		store_id:dataStruct.req.params.store_id,
		NODE_ENV:process.env.NODE_ENV,
		token:dataStruct.req.params.token,
		show:showme,
		video:false
	};
	console.log("payload");
	console.log(payload);
	dataStruct.res.render('photoStyxStartPage',payload);
};
module.exports = {
	videoStyxCreate:videoStyxCreate,
	photoStyxCreate:photoStyxCreate
};