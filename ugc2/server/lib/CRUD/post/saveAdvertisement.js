var cf = require('../../helpers/fileHandling');

var savePhotoStyx = function(dataStruct)
{
	try{
		var res = dataStruct.res;
		var req = dataStruct.req;
		var dataJSON = dataStruct.req.body.model;

		if(typeof(dataJSON)=="string")
		{
			dataJSON = eval("("+dataJSON+")");
		}
		/*
		{
			currentTags:[],
			photoStyxColors:[],
			advertInfo:[]
		}
		*/
		console.log(dataJSON);
		// console.log(dataStruct.req.files);

		var uploadDirectory = __dirname ;
		//get the root based on server folder
		var uploadDirArr = uploadDirectory.split('server');
		uploadDirectory = uploadDirArr[0];
		
		dataStruct.moveFile={
			src : uploadDirectory+'uploads/'+dataJSON.filename+"."+dataJSON.filename_ext,
			dest : {
				filename: dataJSON.filename+"."+dataJSON.filename_ext,
				path: uploadDirectory+"cdn/uploads/advertisement/photostyx/"+dataStruct.user_id+"/"+parseInt(req.params.store_id),
				location:"cdn/uploads/advertisement/photostyx/"
			},
			filename: dataJSON.filename+"."+dataJSON.filename_ext
		};
		dataStruct.saveData = JSON.parse(JSON.stringify(dataJSON));
		//dataStruct.filename=filename;
		//insertToDB(dataStruct);
		cf.moveFile(dataStruct,insertToDB);
	}catch(err){
		var msg = {
			status:"Error",
			message:"[savePhotoStyx]Error Processing Data",
			desc:err
		};
		GLOBAL.sendError(msg,dataStruct.res);
	}
};

function insertToDB(dataStruct)
{
	var query = {
		"advertInfo.name":dataStruct.saveData.advertInfo.name,
		"user.user_id":dataStruct.saveData.user.user_id,
		"user.store_id":dataStruct.saveData.user.store_id
	};
	GLOBAL.mongodb.ugcPhotostyx.update(query,dataStruct.saveData,{upsert:true},function(err,docs){
		if(err==null)
		{
			GLOBAL.mongodb.ugcPhotostyx.find(query,function(err,docs){
				var msg = {
					status:"Success",
					message:"[insertToDB]Success Saving data",
					docs:docs
				};
				GLOBAL.sendError(msg,dataStruct.res);
			});
		}else{
			var msg = {
				status:"Error",
				message:"[insertToDB]Error Saving data",
				err:err
			};
			GLOBAL.sendError(msg,dataStruct.res);
		}
	});
}

module.exports = {
	savePhotoStyx:savePhotoStyx
};


