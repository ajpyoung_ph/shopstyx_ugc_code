
var checkToken = function(dataStruct,callback)
{
	var query = "SELECT * FROM cs_tokens WHERE user_id="+dataStruct.user_id+" AND token='"+dataStruct.token+"';";
	console.log(query);
	GLOBAL.db_cs_common.query(query,function(err,rows, fields){
		var msg={};
		if(err==null)
		{
			if(rows.length>0)
			{
				callback(dataStruct);
			}else{
				msg = {
					status:"Error",
					message:"You are not permitted to use this service",
					description:"Credentials are not valid"
				};
				try{
					dataStruct.res.json(msg);
					console.log(msg);
				}catch(err){
					console.log("Error Sending Server response");
					console.log(err);
					console.log(dataStruct.res);
					//dataStruct.res.json(msg);
				}
			}
		}else{
			console.log("Error Accessing DB");
			console.log(err);
			msg = {
				status:"Error",
				message:"Error Accessing DB",
				description:err
			};
			dataStruct.res.json(msg);
			console.log(msg);
		}
	});
}

module.exports = {
	checkToken:checkToken
}