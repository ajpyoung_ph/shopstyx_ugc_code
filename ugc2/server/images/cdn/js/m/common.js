var vid = CS.vid,
	cloud_url = CS.cloudURL || location.protocol + '//' + location.host + '/',
	root = CS.siteURL || location.protocol + '//' + location.host + '/',
	current_url = location.href,
	loadMPhone = true,
	trending_load_status = false;

function getScripts(js) {
	if(typeof js == "object" && js.length > 0){
		for(var i = 0; i < js.length; i++){
			$.getScript(js[i]);
		}
	}
	return true;
}

var SSAjax = function( options ) {
		var form_data = {
				token: store.get('token'),
				user_id: store.get('user_id'),
				_Ssfp_ID: store.get('_Ssfp_ID')
			};

        // Default options.
        var settings = $.extend(true,{
            // These are the defaults.
			loadCSS: [],
            type: "POST",
            data: form_data,
            url: "",
            success: function(response) {
            	//console.log(response);
            },
            loadJS: []
            
        }, options );

        // loading external css and js
        var jsLib = "", cssLib = "", loadJS = new Array(), loadCSS = new Array();
        if(settings.hasOwnProperty('loadJS') && settings.loadJS.length > 0) {

        	if(typeof $('html').data('loadJS') != "object") {
        		$('html').data('loadJS',[]);
        	}

    		for(var i = 0; i < settings.loadJS.length; i++) {
    			if($.inArray(settings.loadJS[i], $('html').data('loadJS')) == -1) {
    				loadJS.push(settings.loadJS[i]);
					jsLib += ",$.getScript('"+settings.loadJS[i]+"')";
    			}
    		}
        }

        if(settings.hasOwnProperty('loadCSS') && settings.loadCSS.length > 0) {
        	if(typeof $('html').data('loadCSS') != "object") {
        		$('html').data('loadCSS',[]);
        	}

        	for(var i = 0; i < settings.loadCSS.length; i++) {
    			if($.inArray(settings.loadCSS[i], $('html').data('loadCSS')) == -1) {
    				loadCSS.push(settings.loadCSS[i]);
    			}
    		}
			/*
    		if(loadCSS.length > 0) {	
    			for(var i = 0; i < loadCSS.length; i++) {
					$.get(loadCSS[i], function(contents){
						$("<style type=\"text/css\">" + contents + "</style>").appendTo(document.head);
						// $('<link>')
							// .appendTo($('head'))
							// .attr({type : 'text/css', rel : 'stylesheet'})
							// .attr('href', loadCSS[i]);
					});
				}
			}
			*/
        }
       	
		var success = settings.success;
		settings.success = function(response, textStatus, xmlHttp) {
			
			if(response == "" || response == false) {
				// invalid toke and/or user_id, so logout
				//return;
			}
			success(response);	 
			//console.log(xmlHttp.getAllResponseHeaders())  
			//console.log(xmlHttp.getResponseHeader("Date")) 		
		};

		var token = store.get('token'), 
			last_stamp = store.get('session_date'),
			remember = store.get('remember');

		// check here if logged In session has already been expired
		var session = check_login_session({
							"token": token,
							"last_stamp": last_stamp,
							"remember": remember
						});

		// if session is expired, make a pop-up here
		if(!session) {
			//alert("Your session has been expired due to inactivity!");
			//autoLogout();
			//return;
		}


		var login_data = {post_data:1,
							token:token,
							user_id:settings.data.user_id
						};

		if(typeof store.get('_Ssfp_ID') == "undefined" && (typeof token == "undefined" || typeof settings.data.user_id == "undefined")) {
			login_data._Ssfp = $.fingerprint();
		}
		else {
			login_data._Ssfp_ID = store.get('_Ssfp_ID');
		}

		// check here if user is logged In
		$.ajax({url:"/checkuser.html",
				type:'post',
				data: login_data,
				success: function(data){
						var _data = $.parseJSON(data);
						if(typeof _data._Ssfp_ID != "undefined" && !$.isEmptyObject(_data._Ssfp_ID) && _data._Ssfp_ID != 0) {
							store.set('_Ssfp_ID',_data._Ssfp_ID);
							settings.data._Ssfp_ID = _data._Ssfp_ID;
						}

						if( _data.result){
							store.set('display_name', _data.display_name);
							store.set('user_id', _data.user_id);

				        	if(loadJS.length > 0 || loadCSS.length > 0) {
								if(loadCSS.length > 0) {
									$.get(loadCSS[0], function(contents){
										$("<style type=\"text/css\">" + contents + "</style>").appendTo(document.head);
										$('html').data('loadCSS',$.merge($('html').data('loadCSS'),loadCSS));
										
										if(loadJS.length > 0) {
							        		$.when(eval(jsLib.substr(1)))//getScripts(loadJS))
							        		 .then(
							        		 	function() {
								        		 	$('html').data('loadJS',$.merge($('html').data('loadJS'),loadJS));
													// load the specific page
													$.ajax(settings);
							        		 	},
							        		 	function() {
							        		 		console.log('Unable to load the js external files. ' + settings.loadJS);
							        		 	}
							        		 );
										}
										else {
											setTimeout(function(){$.ajax(settings)},100);
										}
									});
								}
								else {
									$.when(eval(jsLib.substr(1)))//getScripts(loadJS))
									 .then(
										function() {
											$('html').data('loadJS',$.merge($('html').data('loadJS'),loadJS));
											// load the specific page
											$.ajax(settings);
										},
										function() {
											console.log('Unable to load the js external files. ' + settings.loadJS);
										}
									 );
								}
							}
							else {
								$.ajax(settings);
							}
						}
						else {
							store.remove('token');
							store.remove('user_id');
							store.remove('display_name');
							store.remove('remember');
							//return;
							// redirect user if not logged in
							//alert("Sorry! You are not logged in.");
							//location.href = "/";
							$.ajax(settings);
						}
				}
			});		
	};


var check_login_session = function(options) {
		var new_timestamp = new Date().getTime();
		// milliseconds
		var minutes_to_expire = 120000; // 2 minutes 

		if(( typeof options.token !== 'undefined' || options.token != '' ) 
		&& (options.last_stamp !== 'undefined' || options.last_stamp != '')  
		&& options.remember == 0 ){
			minutes_to_expire = 600000; // 10 minutes
			if ((new_timestamp - options.last_stamp) > 120000) {
				return false;
			}
		}

		if(( typeof options.token !== 'undefined' || options.token != '' ) 
		&& (options.last_stamp !== 'undefined' || options.last_stamp != '')  
		&& options.remember == 1 ){
			minutes_to_expire = 604800000; // 7 days
			if (new_timestamp - options.last_stamp > 604800000) {
				return false;
			}
		}

		// renew the session date
		/*
		if( ( typeof store.get('token') !== 'undefined' || store.get('token') != '' ) 
		&& ( store.get('remember') == 0 || store.get('remember') == 1 ) ){

		}
		*/
		store.set('session_date', new_timestamp);
		return true;
	};

// check every 6 seconds if session has been expired	
/*
setInterval(function() {
	var token = store.get('token'), 
			last_stamp = store.get('session_date'),
			remember = store.get('remember');

	// check here if logged In session has already been expired
	var session = check_login_session({
						"token": token,
						"last_stamp": last_stamp,
						"remember": remember
					});

	// if session is expired, make a pop-up here
	if(!session) {
		//alert("Your session has been expired due to inactivity!");
		//autoLogout();
		return;
	}

}, 6000);	
*/

function autoLogout(){
	var url = root + 'signout.html';
	var form_data = {};
	form_data.post_dta = 1;
	form_data.temp_sessionid = store.get('temp_sessionid');
	form_data.token = store.get('token');
		
	$.ajax({
		type: "POST",
		url: url,
		data: form_data,
		cache: false,
		success: function(data){
			if(data == 'success'){
				//store.clear();
				store.remove('user_id');
				store.remove('cart');
				store.remove('token');

				var currentUrl = location.href.replace('#','');
				var pos = root.indexOf("?");
				var querystring = (pos == -1) ? "?referer="+ escape(currentUrl) : '&referer=' + pos;
				location.href = root + querystring;
			}
		}
	});
	return false;
}

function hash_querystring(hash){
	var query = {};
	if(typeof hash == "undefined" || hash == "") return query;

	var hash = hash.split('&');
	var q;

	for(var i = 0; i < hash.length; i++) {
		if(hash[i] != "") {
			q = hash[i].split('=');
			query[q[0]] = q[1].toString();
		}
	}
	return query;
}		

function get_hash_querystring_value(hash,name) {
	var hash = hash_querystring(hash);
	if(hash.hasOwnProperty(name)) {
		for(i in hash) {
			if(i == name) {
				return hash[i];
			}
		}
	}

	return "";
}

function load_page(param) {
	var url = $(location).attr('href').split('#');
	var mainURL = url[0], hash = url[1];
	
	//hash = typeof hash == "undefined" ? "" : hash;
	//var container = get_hash_querystring_value(hash,'c');
	var param = (typeof param == "undefined") ? {} : param; 
	var container = "#wrapper";
	var page_title;
	var user_id = store.get('user_id');
	var token = store.get('token');
	var form_data = {
		isAjax:true,
		token: token,
		user_id: user_id
	};
	var browserNav = false;
	
	if(param.hasOwnProperty('container')) {
		container = param.container;
	}

	if(param.hasOwnProperty('data')) {
		form_data = $.extend(true,form_data, param.data);
	}

	if(param.hasOwnProperty('page_tile')) {
		page_title = param.page_title;
	}
	else {
		page_title = $(document).find("title").text();
	}

	if(param.hasOwnProperty('url')) {
		mainURL = param.url;
		if(mainURL.indexOf(CS.siteURL) == -1) {
			mainURL = CS.siteURL + mainURL.substr(1);
		}
	}	

	if(param.hasOwnProperty('browserNav')) {
		browserNav = (param.browserNav) ? true : false;
	}

	if ( History.enabled) {
		History.pushState(null, page_title, mainURL);
	}

	//if(mainURL == History.getState().url && browserNav) {

	if(browserNav) {
		return;
	}

	if(container == "#subcontent-wrapper") {
		toggle_preloader_maincontent();
	}
	/*
	var controller = mainURL.replace(CS.siteURL,'');
	//alert(mainURL)
	var cssfile = CS.cloudURL + 'css/v2/main.css';
	alert(controller)
	if(controller != ""){
		controller = controller.split('/');
		controller = controller[1];
		controller = controller.split('?')[0];
		controller = controller.toLowerCase().replace(/[^a-z0-9]/g,'');
		var cssfile = CS.cloudURL + 'css/v2/' + controller + '.css';
	}
	var loadCSS = new Array();

	if($.inArray(cssfile, $('html').data('loadCSS')) == -1) {
		loadCSS.push(cssfile);
	}

	if(typeof $('html').data('loadCSS') != "object") {
		$('html').data('loadCSS',[]);
	}
	if(loadCSS.length > 0) {
		$.get(cssfile, function(contents){
			$("<style type=\"text/css\">" + contents + "</style>").appendTo(document.head);
		})
		.always(function(){
			$('html').data('loadCSS',$.merge($('html').data('loadCSS'),loadCSS));

			SSAjax({
				data: form_data,
				url: mainURL,
				success:function(response) {
					$(container).html(response);
					if(container == "#subcontent-wrapper") {
						toggle_preloader_maincontent();
					}
					else if(container == "#wrapper") {
						toggleScroll_leftmenu();
					}
				}
			});
		});

		return;
	}
	*/
	console.log("load: "+mainURL);
	SSAjax({
		data: form_data,
		url: mainURL,
		success:function(response) {
			$(container).html(response).removeClass('hide');
			/*if(container == "#subcontent-wrapper") {
				toggle_preloader_maincontent();
			}
			else if(container == "#wrapper") {
				toggleScroll_leftmenu();
			}*/
		}
	});
}

function toggle_preloader_maincontent() {
	if($('#body-wrapper .shopstyxloader').eq(0).hasClass('hide')) {
		$('#subcontent-wrapper').eq(0).addClass('hide');
		$('#body-wrapper .shopstyxloader').eq(0).removeClass('hide');
		$('#body-wrapper .overlay').eq(0).removeClass('hide');	
	}
	else {
		$('#body-wrapper .shopstyxloader').eq(0).addClass('hide');
		$('#body-wrapper .overlay').eq(0).addClass('hide');
		$('#subcontent-wrapper').eq(0).removeClass('hide');
	}
}

function putUpAddColl(item,colls){
	var licreate = function(){
		var li = $('<li>',{'data-cid':0,'data-sid':0}).html('<a href="#"><span></span> <i class="asset check"></i></a>');
		return li;
	}

	var modd = $('.add-collection-box.addtocoll');
	modd.find('img.prev').attr('src',item['product_image']);
	modd.find('h1.glbl').text(item['product_name']);
	modd.find('ul.choosecol').html('');
	for (var cl in colls){
		var ll = licreate();
		ll.attr('data-cid',cl['collection_id'])
		.attr('data-sid',cl['store_id'])
		.find('span').text(cl['collection_name']);
		modd.find('ul.choosecol').append(ll);
	}
}

var lastmodal, lscrollv=0;

function slideItUp(modalopen,loadmode){
	lastmodal = modalopen;
	dataService.resetform();
	$('#starterModal').css('top',$(window).height());
	$('#starterModal').css('height',$(window).height());
	$('body').addClass('noscroll');
	$('#starterModal').show();
	if (loadmode) $('#starterModal').find('.shopstyxloader').removeClass('hide');
	modalopen.removeClass('hide');
	$('#starterModal').animate({
	    top: 0
	  }, 300, function() {
	    
	  });
}

function slideItDown(){
	dataService.resetform();
	$('#starterModal').animate({
	    top: $(window).height()
	  }, 300, function() {
	  	lastmodal.addClass('hide');
	  	lastmodal = null;
	  	$('body').removeClass('noscroll');
	  	$('#starterModal > div').addClass('hide');
	    $('#starterModal').hide();
	  });
}

function swapLogs(typ){
	//1 sign 2 soc
	dataService.resetform();
	
	if (typ == 1){
		$('.signlog').removeClass('hide');
		$('.signsoc').addClass('hide');
	}else{
		$('.signlog').addClass('hide');
		$('.signsoc').removeClass('hide');
	}
}

function swapRegs(typ){
	//1 sign 2 soc
	dataService.resetform();
	if (typ == 1){
		$('.regfields').removeClass('hide');
		$('.regsoc').addClass('hide');
	}else{
		$('.regfields').addClass('hide');
		$('.regsoc').removeClass('hide');
	}
}



/* 	===================================
 *	Events Section
 *	===================================
 */

$(window).resize(function(){
	$('#starterModal').css('height',$(window).height());
	//$('.addit').css('left', ($(window).width()-70)/2);
});

$(window).scroll(function(event) {
	var toshow = (lscrollv < $(window).scrollTop());
	if (toshow != $('.addit').hasClass('showy')){
		if (toshow) $('.addit').addClass('showy');
		else $('.addit').removeClass('showy');
		$('.addit').animate({
		    bottom: (toshow)?10:-100
		  }, 300, function() {
		    
		  });
	}
	lscrollv = $(window).scrollTop();
		
});

$(document).on('touchstart, click', '.logsoc', function(e){ 
	e.preventDefault(); e.stopImmediatePropagation();
	swapLogs(2);
});

$(document).on('touchstart, click', '.logsign', function(e){ 
	e.preventDefault(); e.stopImmediatePropagation();
	swapLogs(1);
});

$(document).on('submit', '#loginform', function(e){ 
	e.preventDefault(); e.stopImmediatePropagation();
	referer = getUrlParameters('referer','', true);
	referer = typeof referer == "undefined" ? false : referer;

	$('.reg-log-loader').removeClass('hide');
	var tz = jstz.determine(),
		vid = CS.vid,
		$form = $(this),
		form_data = {},
		timestamp = new Date().getTime();
		
	form_data.email = $form.find( 'input[name="email"]' ).val();
	form_data.password = $form.find( 'input[name="password"]' ).val();
	form_data.remember= $form.find('input[name="remember"]').is(':checked')?1:0;
	form_data.url = $form.attr( 'action' );
	form_data.temp_sessionid = vid;
	form_data.token = vid;
	form_data.tz = tz.name();
	$('.message').html('');
	dataService.getData(form_data)
		.success(function(data, statusText, jqXHR){
			var _data = $.parseJSON(data);
			if(_data.result == 'failed'){
				$('.message').html(_data.message);
				$('.reg-log-loader').addClass('hide');
			}else{
				store.set('remember', form_data.remember);
				store.set('session_date', timestamp);
				store.set('temp_sessionid', vid);
				store.set('token', vid);
				store.set('display_name', _data.display_name);
				store.set('user_id', _data.user_id);
				//$('.close-reveal-modal').trigger('click');
				//$('.mp').addClass('hide');
				//$('.lp').removeClass('hide');
				
				if(!referer) {
					location.href = location.href;
				}
				else {
					location.href = referer;
				}
			}
			//$('.reg-log-loader').addClass('hide');
		})
		.fail(function (jqXHR, statusText, err){
			alert('Error login: ' + err);
			//$('.register-loader').addClass('hide');
		});
	return false;
	
});

$(document).on('keyup', '.user-name', function(){
	$(this).val(convertToSlug($(this).val()));
});

$(document).on('focus', '#email', function(){
	$(this).parent('.common_input_wrapp').find('.email_error').html('').addClass('hide');
});

$(document).on('submit', '#ssreg', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	
	$('#miniload').foundation('reveal', 'open');

	$(this).attr("disabled", "disabled");
	var tz = jstz.determine(),
		vid = CS.vid,
		$form = $(this),
		url = $form.attr( 'action' ),
		form_data = {},
		timestamp = new Date().getTime();

	form_data.username = $form.find( 'input[name="username"]' ).val();
	form_data.email = $form.find( 'input[name="email"]' ).val();
	form_data.password = $form.find( 'input[name="pwd"]' ).val();
	form_data.url = $form.attr( 'action' );
	form_data.temp_sessionid = vid;
	form_data.token = vid;
	form_data.tz = tz.name();
	form_data.user_id = $('#user_id').val();
	
	$form.find('.error2').html('').addClass('hide');
	$form.find('.email_error').addClass('hide');
	
	dataService.getData(form_data)
		.success(function(data, statusText, jqXHR){
			var _data = $.parseJSON(data);
			$('#miniload').foundation('reveal', 'close');
			if(_data.register_result == false){
				if(_data.error_class == '.error2'){
					$form.find('.error2').html(_data.error_msg).removeClass('hide');
				}else if(_data.error_class == '.gen_error'){
					$(_data.error_class).html('<span style="width:616px;" class="fberrorbox">' + _data.error_msg + '</span>');
				}else{
					$(_data.error_class).html(_data.error_msg);
				}
				$("#ssreg").removeAttr("disabled");
			}else{
				if(_data.type == 'normal'){
					store.set('remember', 1);
					store.set('session_date', timestamp);
					store.set('temp_sessionid', vid);
					store.set('token', vid);
					store.set('display_name', _data.display_name);
					store.set('user_id', _data.user_id);
					store.set('user_type', '');
				}
				if(_data.type == 'twitter'){
					var _uid = $('#user_id').val();
					if(isNaN(_uid))
					{
						store.set('remember', 1);
						store.set('session_date', timestamp);
						store.set('temp_sessionid', vid);
						store.set('token', vid);
						store.set('display_name', _data.display_name);
						store.set('user_id', _data.user_id);
						store.set('user_type', '');
					}
				}
				$('.mp').addClass('hide');
				$('.lp').removeClass('hide');
				
				if(store.get('register_data') == ''){ $('.close-reveal-modal').trigger('click'); }
				if(store.get('register_data') == 'color_data'){ store.set('user_color', $('.s-color').attr('data-color')); }
				if(store.get('register_data') == 'promote_data'){
					store.set('user_color', $('.color-group2').find('.selected').attr('data-color'));
					store.set('user_type', 'P');
					getPromoterData();
				}
				location.href = location.href;
			}
			$('#miniload').foundation('reveal', 'close');
			//$('.reg-log-loader').addClass('hide');
		})
		.fail(function (jqXHR, statusText, err){
			alert('Error register: ' + err);
			$('.reg-log-loader').addClass('hide');
		});
	
	return false;
});


$(document).on('click', '#addprodbtn', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	slideItUp($('.add-collection-box.addurl'));
});

$(document).on('click', '.closemodal', function(e){
	e.preventDefault();
	slideItDown();
	return false;
});

$(document).on('click', '.create-account',function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	$('.login-register-box.login-mod-box').addClass('hide');
	$('.login-register-box.register-mod-box').removeClass('hide');
	swapRegs(1);
});

$(document).on('touchstart, click', '.logsoc', function(e){ 
	e.preventDefault(); e.stopImmediatePropagation();
	swapLogs(2);
});

$(document).on('touchstart, click', '.logsign', function(e){ 
	e.preventDefault(); e.stopImmediatePropagation();
	swapLogs(1);
});

$(document).on('click', '#sign-in', function(e){
	e.preventDefault();
	swapLogs(1);
	slideItUp($('.login-register-box.login-mod-box'));
	return false;
});

$(document).on('submit', '#ssurl', function(e){
		e.preventDefault(); e.stopImmediatePropagation();
		
		var $form = $(this),
			url = $form.attr( 'action' );
			form_data = {};
		form_data.isAjax = true;
		form_data.url = $.trim($('.product-url').val());
		$('#miniload').foundation('reveal', 'open');
		SSAjax({
			data: form_data,
			url: url,
			success: function(response){
				var _data = $.parseJSON(response),
					c = 0,
					image_counter = 0;
				$('.add-collection-box.addurl').addClass('hide').hide();
				$('#miniload').foundation('reveal', 'close');
				$('#starterModal .shopstyxloader').removeClass('hide');
				if(_data.url_exist){
					var page = root + 'stick_products?action=url_step2',
						form_data = {};
					
					form_data.prod_user_id = _data.prod_user_id;
					form_data.prod_store_id = _data.prod_store_id;
					form_data.collection_prod_user_id = _data.collection_prod_user_id;
					form_data.collection_prod_store_id = _data.collection_prod_store_id;
					form_data.owner_user_id = _data.user_id;
					form_data.store_id = _data.store_id;
					form_data.collection_id = _data.collection_id;
					form_data.product_id = _data.product_id;
					form_data.product_user_id = _data.product_user_id;
					form_data.current_url = _data.current_url;
					form_data.url_exist = 1;					
						
					form_data.product_site_name = '';
					form_data.external_product_url = '';
					form_data.external_product_canonical_url = '';
					form_data.external_tags = '';
					form_data.product_name = _data.product_name;
					form_data.price = _data.price;
					form_data.currency = _data.currency;
					form_data.currency_symbol = (_data.currency == 'EURO')? '&euro':'$';
					form_data.image = _data.image;
					form_data.product_for = _data.department_id;
					form_data.product_for_name = _data.department_name;
					form_data.color = '';
					
					form_data.isAjax = true;
					
					SSAjax({
						data: form_data,
						url: page,
						success: function(response){
							//$('.stick-product-modal .stick-wrapper').html(response);
							$('#starterModal .shopstyxloader').addClass('hide');
							$('#starterModal .addtocoll').removeClass('hide').html(response);
						}
					});
				}
					
				if(! _data.url_exist){	
					$('.tmp-images').empty();
					if(_data.image != ''){
						//$('.tmp-images').append('<li class="active" style="background-image: url(\' ' + _data.image + ' \')" ><input type="hidden" name="product_image" value="' + _data.image + '" /></li>');
						$('.tmp-images').append('<div class="columns small-4 medium-3"><a href="#" class="img" style="background-image: url(\' ' + _data.image + ' \')" ><input type="hidden" name="product_image" value="' + _data.image + '" /></a></div>');
						image_counter++; c++;
					}
					total_img = _data.images.length - 1;
					$.each(_data.images, function(i,item){
						var newImg = new Image();
						newImg.src = item;
						var height = newImg.height,
							width = newImg.width;
							
						$(newImg).load(function(){
							if(parseInt(newImg.width) >= 250 || parseInt(newImg.height) >= 250){
								if(_data.image != item){
									// if( c == 0 ){
										// $('.tmp-images').append('<li class="active" style="background-image: url(\' ' + item + ' \')" ><input type="hidden" name="product_image" value="' + item + '" /></li>');
									// }
									// if( c > 0 ){
										// $('.tmp-images').append('<li style="background-image: url(\' ' + item + ' \')" ><input type="hidden" name="product_image" value="' + item + '" /></li>');
									// }
									$('.tmp-images').append('<div class="columns small-4 medium-3"><a href="#" class="img" style="background-image: url(\' ' + item + ' \')" ><input type="hidden" name="product_image" value="' + item + '" /></a></div>');
								}
								image_counter++; c++;
								if(image_counter > 7){ total_img = 1; }
							}
							total_img--;
						});
					});
					
					function checkImg( ) {
						if(total_img <= 1){
							if(image_counter > 0){
								var imageList = $('.tmp-images').html(),
									page = root + 'stick_products?action=new_url_step1';
								//$('.stick-product-modal .stick-wrapper').html('');
								$('.stick-add-new-product .add-product-step1-wrapper').removeClass('hide');
								var form_data2 = {};
								form_data2.isAjax = true;
								SSAjax({
									data: form_data2,
									url: page,
									success: function(result){
										$('#starterModal .shopstyxloader').addClass('hide');
										//$('.stick-product-modal .stick-wrapper').html(result);
										$('.add-collection-box.newprodform').html(result).removeClass('hide');
										$('.stick-product-site_name').val(_data.site_name);
										$('.stick-product-url').val(_data.web_url);
										$('.stick-product-canonical-url').val(_data.canonical_url);
										$('.stick-product-tags').val(_data.meta_keywords);
										$('.stick-product-name').val(_data.product_name);
										$('.stick-product-price').val(_data.price);
										$('#imgarea div.row').html(imageList);
									}
								});
							}
							if(image_counter == 0){
								$('#starterModal .shopstyxloader').addClass('hide');
								$('.add-collection-box.newprodform').removeClass('hide');
							}
							image_counter = 0;
						}else{
							setTimeout( checkImg, 30 );
						}
					}
					checkImg();
				}
			}
		});
		
		return false;
	});
$(document).on('touchstart, click', '.save-to-btn.stick-add-btn', function(e){
	console.log('huy');
	e.preventDefault(); e.stopImmediatePropagation();
	add_top_shopstyx();

});

$(document).on('touchstart, click', '#imgarea a.img', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	$('#imgarea a.selected').removeClass('selected');
	$(this).addClass('selected');
});

$(document).on('click', '.stick-a-product', function(e){
		e.preventDefault(); e.stopImmediatePropagation();
		/*var use_modal = false;
		if($(this).hasClass('stick-a-product')){
			$('#stickAddModal').foundation('reveal', 'open', {
				animation_speed: 0
			});
			$('.stick-product-add-modal .stick-loader').removeClass('hide');
			use_modal = true;
		}*/
		
		var $form = $(this),
			url = root + 'stick-products?action=get_external_product';
			form_data = {};
		form_data.isAjax = true;
		form_data.url = $.trim($(this).attr('data-url'));
		slideItUp($('#starterModal .addtocol'), true);
		/*$('.add-search-modal .stick-loader').removeClass('hide');
		$('.error2').addClass('hide');
		$('.tmp-images').html('');
		$('.add-product-step1-content').html('');
		$('.add-product-step2-content').html('');*/
		SSAjax({
			data: form_data,
			url: url,
			success: function(response){
				var _data = $.parseJSON(response),
					c = 0,
					image_counter = 0;
				if(_data.url_exist){
					var page = root + 'stick_products?action=url_step2',
						form_data = {};
					
					form_data.prod_user_id = _data.prod_user_id;
					form_data.prod_store_id = _data.prod_store_id;
					form_data.collection_prod_user_id = _data.collection_prod_user_id;
					form_data.collection_prod_store_id = _data.collection_prod_store_id;
					form_data.owner_user_id = _data.user_id;
					form_data.store_id = _data.store_id;
					form_data.collection_id = _data.collection_id;
					form_data.product_id = _data.product_id;
					form_data.product_user_id = _data.product_user_id;
					form_data.current_url = _data.current_url;
					form_data.seo_url = _data.seo_url;
					form_data.url_exist = 1;					
						
					form_data.product_site_name = '';
					form_data.external_product_url = '';
					form_data.external_product_canonical_url = '';
					form_data.external_tags = '';
					form_data.product_name = _data.product_name;
					form_data.price = _data.price;
					form_data.currency = _data.currency;
					form_data.currency_symbol = (_data.currency == 'EURO')? '&euro':'$';
					form_data.image = _data.image;
					form_data.product_for = _data.department_id;
					form_data.product_for_name = _data.department_name;
					form_data.color = '';
					
					form_data.isAjax = true;
					/*$('.add-search-modal .stick-loader').removeClass('hide');
					$('.add-search-modal .add-product-step2-wrapper').removeClass('hide');
					$('.add-search-modal .add-product-step2-wrapper').centerized();
					$('.bg-trans').removeClass('hide');
					if(use_modal){
						$('.stick-product-add-modal').css({'max-width':'880px'});
					}*/
					SSAjax({
						data: form_data,
						url: page,
						success: function(response){
							$('#starterModal .shopstyxloader').addClass('hide');
							$('#starterModal .addtocoll').removeClass('hide').html(response);
						}
					});
				}
					
				/*if(! _data.url_exist){	
					if(_data.image != ''){
						//$('.tmp-images').append('<li class="active" style="background-image: url(\' ' + _data.image + ' \')" ><input type="hidden" name="product_image" value="' + _data.image + '" /></li>');
						$('.tmp-images').append('<li style="background-image: url(\' ' + _data.image + ' \')" ><input type="hidden" name="product_image" value="' + _data.image + '" /></li>');
						image_counter++; c++;
					}
					total_img = _data.images.length - 1;
					$.each(_data.images, function(i,item){
						var newImg = new Image();
						newImg.src = item;
						var height = newImg.height,
							width = newImg.width;
							
						$(newImg).load(function(){
							if(parseInt(newImg.width) >= 250 || parseInt(newImg.height) >= 250){
								if(_data.image != item){
									// if( c == 0 ){
										// $('.tmp-images').append('<li class="active" style="background-image: url(\' ' + item + ' \')" ><input type="hidden" name="product_image" value="' + item + '" /></li>');
									// }
									// if( c > 0 ){
										// $('.tmp-images').append('<li style="background-image: url(\' ' + item + ' \')" ><input type="hidden" name="product_image" value="' + item + '" /></li>');
									// }
									$('.tmp-images').append('<li style="background-image: url(\' ' + item + ' \')" ><input type="hidden" name="product_image" value="' + item + '" /></li>');
								}
								image_counter++; c++;
								if(image_counter > 7){ total_img = 1; }
							}
							total_img--;
						});
					});
					
					function checkImg( ) {
						if(total_img <= 1){
							if(image_counter > 0){
								var imageList = $('.tmp-images').html(),
									page = root + 'stick_products?action=new_url_step1';
								$('.add-search-modal .add-product-step1-wrapper').removeClass('hide');
								$('.add-search-modal .add-product-step1-wrapper').centerized();
								$('.bg-trans').removeClass('hide');
								var form_data2 = {};
								form_data2.isAjax = true;
								SSAjax({
									data: form_data2,
									url: page,
									success: function(result){
										$('.add-search-modal .add-product-step1-content').html(result);
										$('.stick-product-site_name').val(_data.site_name);
										$('.stick-product-url').val(_data.web_url);
										$('.stick-product-canonical-url').val(_data.canonical_url);
										$('.stick-product-tags').val(_data.meta_keywords);
										$('.stick-product-name').val(_data.product_name);
										$('.stick-product-price').val(_data.price);
										$('.choose-img-content ul').html(imageList);
										$('.add-search-modal .stick-loader').addClass('hide');
									}
								});
							}
							if(image_counter == 0){
								$('.error2').removeClass('hide');
								$('.add-search-modal .stick-loader').addClass('hide');
							}
							image_counter = 0;
						}else{
							setTimeout( checkImg, 30 );
						}
					}
					checkImg();
				}*/
			}
		});
		
		return false;
	});

$(document).on('touchstart, click', '#newcolbtn', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	$('#aprodinfo').addClass('hide');
	$('#choosecol').addClass('hide');
	$('#cnewform').removeClass('hide');
});

$(document).on('touchstart, click', '#closencform', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	$('#aprodinfo').removeClass('hide');
	$('#choosecol').removeClass('hide');
	$('#cnewform').addClass('hide');
});

$(document).on('touchstart, click','#cnewform .subm', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	var collect_name = $.trim($('input#collection-name').val());
	var collection_desc = $.trim($('textarea#collection-desc').val());
	if(collect_name == ''){
		$('#collection-name').addClass('border-red');
		return false;
	}
	//$('.collection-box').removeClass('active');
	//$('.collection-box-new').remove();
	var el = $('<li>',{'class':'new'}).append('<a href="#" data="0">' + collect_name + '</a><input type="hidden" name="collection_id" value="new" /><input type="hidden" name="collection_name" value="' + collect_name + '" /><textarea class="hide" name="collection_description">' + collection_desc + '</textarea></li>');
	$('ul.choosecol').prepend(el);
	$('#aprodinfo').removeClass('hide');
	$('#choosecol').removeClass('hide');
	$('#cnewform').addClass('hide');
	el.trigger('click');
	//$('.cw-a-content ul').append('<li class="collection-add-btn new-collection-btn"><span class="collection-plus"></span></li>');

});

$(document).on('touchstart, click', 'ul.choosecol li a, .sengine-save-collection-btn', function(e){
		e.preventDefault(); e.stopImmediatePropagation();

		$(this).parents('li').addClass('selected');

		$('#miniload').foundation('reveal', 'open');

		//new collection here
		/*if(!$('._add-collections').hasClass('hide')){
			var collect_name = $.trim($('.collection-input-name').val());
			var collection_desc = $.trim($('.collection-description').val());
			if(collect_name == ''){
				$('.collection-input-name').addClass('border-red');
				return false;
			}
			//$('.new-collection-btn').remove();
			$('.collection-box').removeClass('active');
			$('.collection-box-new').remove();
			$('.cw-a-content ul').append('<li class="collection-box collection-box-new new active"><div class="collection-txt">' + collect_name + '</div><input type="hidden" name="collection_id" value="new" /><input type="hidden" name="collection_name" value="' + collect_name + '" /><textarea class="hide" name="collection_description">' + collection_desc + '</textarea></li>');
			//$('.cw-a-content ul').append('<li class="collection-add-btn new-collection-btn"><span class="collection-plus"></span></li>');
		}*/

		var page = root + 'stick_products?action=save_collection',
			$this = $(this),
			form_data = {};
		form_data.prod_user_id = $.trim($('.stick-product-prod_user_id').val());
		form_data.prod_store_id = $.trim($('.stick-product-prod_store_id').val());
		form_data.collection_prod_user_id = $.trim($('.stick-product-collection_prod_user_id').val());
		form_data.collection_prod_store_id = $.trim($('.stick-product-collection_prod_store_id').val());
		form_data.owner_user_id = $.trim($('.stick-product-owner_user_id').val());
		form_data.store_id = $.trim($('.stick-product-store_id').val());
		form_data.post_collection_id = $.trim($('.stick-product-collection_id').val());
		form_data.product_id = $.trim($('.stick-product-product_id').val());
		form_data.product_user_id = $.trim($('.stick-product-product_user_id').val());
		form_data.current_url = $.trim($('.stick-product-current_url').val());
		form_data.seo_url = $.trim($('.stick-product-seo_url').val());
		form_data.product_site_name = $.trim($('.stick-product-site_name2').val());
		form_data.external_product_url = $.trim($('.stick-product-url2').val());
		form_data.external_product_canonical_url = $.trim($('.stick-product-canonical-url2').val());
		form_data.product_name = $.trim($('.stick-product-name2').val());
		form_data.price = $.trim($('.stick-product-price2').val());
		form_data.currency_symbol = $( ".stick-product-currency_symbol2" ).val();
		form_data.currency = $('.stick-product-currency2').val();
		form_data.image = $('.stick-product-image2').val();
		form_data.product_for = $('.stick-product-product-for2').val();
		form_data.color = $('.stick-product-color2').val();
		form_data.external_tags = $.trim($('.stick-product-external-tags2').val());
		form_data.product_keyword = $.trim($('.stick-product-keyword2').val());
		form_data.product_description = $.trim($('.stick-product-description').val());
		form_data.collection_name_selected = $.trim($('ul.choosecol li.selected input[name="collection_name"]').val());
		form_data.collection_id_selected = $.trim($('ul.choosecol li.selected input[name="collection_id"]').val());
		form_data.added_first = $('.stick-product-exist2').val();
		form_data.isAjax = true;
		
		var collection_id = [],  
			collection_name = [],
			collection_description = [];
		
		$('#choosecol ul.choosecol li.new input[name="collection_id"]').each(function(){
			collection_id.push($.trim($(this).val())); 
		});
		$('#choosecol ul.choosecol li.new input[name="collection_name"]').each(function(){
			collection_name.push($.trim($(this).val()));
		});
		$('#choosecol ul.choosecol li.new textarea[name="collection_description"]').each(function(){
			collection_description.push($.trim($(this).val()));
		});
		form_data.collection_id = collection_id;
		form_data.collection_name = collection_name;
		form_data.collection_description = collection_description;
		
		var error = false;
		if($('.stick-product-description').hasClass('product-desc-required')){
			if(form_data.product_description == ""){
				$('.stick-product-description').addClass('border-red');
				error = true;
			}
		}
		
		if(error){
			return false;
		}

		SSAjax({
			data: form_data,
			url: page,
			success: function(response){
				$('#starterModal .add-collection-box.addtocoll').addClass('hide');
				$('#starterModal .add-collection-box.adddone').html(response).removeClass('hide');

				$('#miniload').foundation('reveal', 'close');
			}
		});
	});

$(document).on('click touchstart', '.logout-btn', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	autoLogout();
});

$(document).on('change', '#header #change-photo input', function(e){
	_upload_userpic($(this));
});

$(document).on('click','.srch-btn', function(e) {
	e.preventDefault(); e.stopImmediatePropagation();
	var $form = $(this),
			url = root + 'stick-products?action=trending_search';
			form_data = {};
		form_data.isAjax = true;
		//form_data.url = $.trim($(this).attr('data-url'));
		slideItUp($('#starterModal .search-box.start'), true);
		SSAjax({
			data: form_data,
			url: url,
			success: function(response){
				$('#starterModal .search-box.start').html(response);
				$('#starterModal .shopstyxloader').addClass('hide');
			}
		});
});

$(document).on('submit','form.search',function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	$('.strendlist').addClass('hide');
	$('.rsearches').addClass('hide');
	$('.search-box.start .shopstyxloader.foot').removeClass('hide');
	$('.search-box.start .shopstyxloader.foot .inmate').removeClass('done');
	$('.search-box.start #resultlist').removeClass('hide').empty();
	var serch = $('.search_text').val();
	var $form = $(this),
			url = root + 'stick-products?action=add_search';
	var form_data = {};
	form_data.isAjax = true;
	form_data.search_text = serch;

	SSAjax({
		data: form_data,
		url: url,
		success: function(response){
			$('#starterModal #resultlist').html(response).removeClass('hide');
			//$('#starterModal .shopstyxloader').addClass('hide');
			var opts = {search_text:serch, subcontent:true, pagemode: 1};
			infinite_scroll($('#starterModal'), 'for-product-search', opts, url, $('#starterModal #resultlist'),false);
		}
	});
});

$(document).on('touchstart, click', '.trend-keys a', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	$search_text = $.trim($(this).find('span.kword').html());
	$('.search_text').val($search_text);
	
	$('form.search').trigger('submit');
});

$(document).on('touchstart, click', '.rsearches li a', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	$search_text = $.trim($(this).html());
	$('.search_text').val($search_text);
	
	$('form.search').trigger('submit');
});



$(document).on('blur','.header-caption-action-btn .search-input', function() {
	var el = $(this);
	var select = el.parent().find('.dd-select');
	var input = el.parent().find('.search-input');

	if(input.val() != "") { return; }
	input.addClass('hide');
	select.removeClass('hide');
});

$(document).on('mouseover','.header-caption-action-btn .add-btn', function() {
	var el = $(this);
	var select = el.parent().find('.dd-select');
	var input = el.parent().find('.search-input');

	if(input.val() != "") { return; }
	input.addClass('hide');
	select.removeClass('hide');
});

$(document).on('click','.header-caption-action-btn .add-btn', function(e) {
	e.preventDefault(); e.stopImmediatePropagation();

	addproduct({
				data:{product_id:0, formType:'basic'},
				onSaved: function(data) {
							console.log(data);
						},
				beforeClose: function(){
					console.log('closing the window');
				}			
						
			});
});

$(document).on('touchstart click','#addproduct-btn', function(e) {
	e.preventDefault(); e.stopImmediatePropagation();
	var parent_catid = $('.main-category-hidden').val(),
		modal_id = '#category-product-modal',
		level = $('.level').val();
	addproduct({
		//loadType: "slideRight",
		data:{category_id: $(this).attr('data-cid'), product_id:0, formType:'basic'},
		onSaved: function(data) {
			$('body').css('overflow','hidden');
			reloadPage({link:'/admin/catalog_actions?action=reload_catalog', wrapper: '.catagory-list', preloader: '.mcatalogloader'});
			reloadPage({category_id: parent_catid, link:'/admin/catalog_actions?action=category_products', wrapper: modal_id + ' .mod-wrapper', preloader: '.mod-loader-wrapper', level:level});
		},
		beforeClose: function(){
			$('body').css('overflow','hidden');
		}
	});
});

$(document).on('touchstart click', '#fb-mod-btn', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	var modal_id = '#preview-shop-modal';
	$(modal_id + ' .mod-wrapper').html('');
	openCustomModal(modal_id);
	reloadPage({link:'/admin/build?action=shop-facebook-preview', wrapper: modal_id + ' .mod-wrapper', preloader: '.mod-loader-wrapper'});
});


$(document).on('touchstart, click', '.opme', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	slideItDown();
	doOpen($(this));
});

$(document).on('touchstart, click', '.gohome', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	slideItDown();
	doOpenURL('/');
});


$(document).on('click', '.pcol', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	view_proddetails($(this));
});

$(document).on('click', '.share.prod a', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	if (!$(this).hasClass('selected')){
		$(this).addClass('selected');
		$('.prod.shareopts').addClass('selected');
	}else{
		$(this).removeClass('selected');
		$('.prod.shareopts').removeClass('selected');
	}
});

$(document).on('click', '.promote-this-product', function(e){
	e.preventDefault(); e.stopImmediatePropagation();

	if(store.get('user_id') == 0 || typeof(store.get('user_id')) == "undefined" || typeof(store.get('token')) == "undefined") {
		signin_modal();
		if($(this).attr('ispopup')) {
			History.pushState(null, 'Shopstyx', '/');
		}
		return;
	}

	$('.reveal-modal-promoteproduct-bg').show();
	if($(this).attr('ispopup')) {
		$('.reveal-modal-promoteproduct-bg').addClass('ispopup')
	}

	$('body').addClass('hidescroll');
	$('#promoteProductModal').show().centerized();

	SSAjax({
		data: {isAjax:true,action:'ajax',action_type:'embedform'},
		url: $(this).attr('url'),
		success: function(response) {
			$('#promoteProductModal').html(response);
			$('#promoteProductModal').addClass('noloader');
		}
	});

	var data = $(this).attr('data');
	var mdata = $.parseJSON(data);
	$('#promoteProductModal').data('data',data);
});

$(document).on('click', '.promote-this', function (e) {
	e.preventDefault(); e.stopImmediatePropagation();

	if(store.get('user_id') == 0 || typeof(store.get('user_id')) == "undefined" || typeof(store.get('token')) == "undefined") {
		signin_modal();
		return;
	}

	$('.reveal-modal-promoteproduct-bg').show();
	$('body').addClass('hidescroll');
	$('#promoteProductModal').addClass('collection').show().centerized();

	SSAjax({
		data: {isAjax:true,action:'ajax',action_type:'embedform'},
		url: $(this).attr('url'),
		success: function(response) {
			$('#promoteProductModal').html(response);
			$('#promoteProductModal').addClass('noloader');
		}
	});

	var data = $(this).attr('data');
	//var mdata = $.parseJSON(data);
	//console.log(data)
	$('#promoteProductModal').data('data',data);
});

$(document).on('click', '.promote-product-view .p-right .share-wrapper .left-share .share-common', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	var data = {type:$(this).attr('type')};
	var mdata = $('#promoteProductModal').data('data');
	mdata = $.parseJSON(mdata);
	data = $.extend(data,mdata);
	//console.log(data)
	promote_product(data);
});

$(document).on('click', '.shareopts a.sharebtn', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	var data = {type:$(this).attr('type')};
	var mdata = $('.shareopts').data('data');
	data = $.extend(data,mdata);
	promote_product(data);
});

$(document).on('click', '.save-collection-data', function (e) {
	e.preventDefault(); e.stopImmediatePropagation();
	$(this).parents('form').submit();
});

$(document).on('submit', '#saveCollectionForm', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	var $form = $(this),
		url = $form.attr( 'action' );
		form_data = {};
	form_data.collection_name = $.trim($form.find( 'input[name="collection_name"]' ).val());
	form_data.collection_id = $.trim($form.find( 'input[name="collection_id"]' ).val());
	form_data.store_id = $.trim($form.find( 'input[name="store_id"]' ).val());
	form_data.description = $.trim($form.find( 'textarea[name="description"]' ).val());
	form_data.isAjax = true;
	$('.save-collection').html('Saving...');
	$('.save-collection').removeClass('bgcolor1');
	$('.save-collection').addClass('bgcolor4');
	$('.save-collection').addClass('.save-collection-btn-disabled');
	$('.save-collection').removeClass('.save-collection-data');
	SSAjax({
		data: form_data,
		url: url,
		success: function(response){
			var _data = $.parseJSON(response);
			if( _data.no_error){
				location.href = _data.url;
			}
		}
	});
	
	return false;
});


/* Follow/Unfollow user */
$(document).on('click', '.follow-wrapper li .top-wrapper .btn-follow, .product-listing.storepage-contributors .details .btn-follow', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	if(store.get('user_id') == 0 || typeof(store.get('user_id')) == "undefined") {
		signin_modal();
		return;
	}
					
	var username = $(this).attr('username');
	var type = $(this).attr('type');
	var uid = $(this).attr('uid');

	if(type == "following") {
		$('#followingID-'+uid).remove();
	}
	else {
		if($(this).hasClass('unfollow')) {
			$(this).removeClass('unfollow').addClass('follow').html('follow');
		}
		else {
			$(this).removeClass('follow').addClass('unfollow').html('followed<span>unfollow</span>');
		}
	}

	SSAjax({
		data: {action:'ajax',action_type:'follow_user',followtype:type,uid:uid},
		url: "/"+username,
		success: function(response){
			var data = $.parseJSON(response);
			if(data == "" || $.isEmptyObject(data)) return;

			var follower = (data.follower == 1) ? data.total_follower + ' follower' : data.total_follower + ' followers';

			$('.profile-header .inner .follow-link').eq(0).html(data.total_following + ' following');
			$('.profile-header .inner .follow-link').eq(1).html(follower);

			if(type == "following") {
				var text = (data.total_following == 1) ? data.total_following + ' person' : data.total_following + ' people';
				$('.list-collections .caption .total-people').html(text);
			}
		}
	});
});

$(document).on('click', '.u-prod-info-cont .bottom .add-this a', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	location.href = $(this).attr('href');
});

$(document).on('click', '.see-more-desc', function(){
	$('.more-desc').removeClass('hide');
});
$(document).on('click', '.info-close', function(){
	$('.more-desc').addClass('hide');
});

// follow/unfollow shop
$(document).on('click', '.visit-follow-wrapper .follow-btn', function(e){
	e.preventDefault(); e.stopImmediatePropagation();

	var storeURL = $(this).attr('storeURL');
	var sid = parseInt($(this).attr('sid'));
	var suid = parseInt($(this).attr('suid'));
	if(store.get('user_id') == 0 || typeof(store.get('user_id')) == "undefined") {
		signin_modal();
		return;
	}

	if($(this).hasClass('follow')) {
		$(this).removeClass('follow').addClass('unfollow').html('followed<span>unfollow</span>');
	}
	else {
		$(this).removeClass('unfollow').addClass('follow').html('follow<span>unfollow</span>');
	}

	SSAjax({
		data: {action:'ajax',action_type:'follow','sid':sid,'suid':suid},
		url: "/"+storeURL,
		success: function(response){
			var data = $.parseJSON(response);
			if(data == "" || $.isEmptyObject(data)) return;
		}
	});
});

// follow/unfollow collection
$(document).on('click', '.trend-box .follow-btn, .btn.btn-follow-collection, .sticked-more-collection .follow-box', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	if(store.get('user_id') == 0 || typeof(store.get('user_id')) == "undefined") {
		signin_modal();
		return;
	}
	var el = $(this);
	var data = $.parseJSON(el.attr('data'));
	var username = el.attr('username');

	// follow
	if(el.hasClass('follow')) {
		el.removeClass('follow');
		if(el.hasClass('btn-follow-collection')) {
			el.addClass('unfollow').html('followed' + '<span class="bgcolor1 color2">unfollow</span>');
		}
		else {
			el.addClass('unfollow').html('followed' + '<span>unfollow</span>');
		}
	}
	// unfollow
	else {
		el.removeClass('unfollow');
		if(el.hasClass('btn-follow-collection')) {
			el.addClass('follow').html('follow' + '<span class="bgcolor1 color2">unfollow</span>');
		}
		else {
			el.addClass('follow').html('follow' + '<span>unfollow</span>');
		}
	}

	$.extend(data,{action:'ajax',action_type:'follow_collection'});
	SSAjax({
		data: data,
		url: "/"+username,
		success: function(response){
			var data = $.parseJSON(response);
			if(data == "" || $.isEmptyObject(data)) return;
		}
	});
});


$(document).on('click', '.remove-prod-btn', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	
	var url = root + 'collections';
		form_data = {};
	form_data.uid = parseInt($(this).attr('uid'));
	form_data.csid = parseInt($(this).attr('csid'));
	form_data.cid = parseInt($(this).attr('cid'));
	form_data.pid = parseInt($(this).attr('pid'));
	form_data.isAjax = true;
	form_data.action = 'remove_product';
	$(this).removeClass('bgcolor1');
	$(this).addClass('bgcolor6');
	$(this).html('Removing...');
	SSAjax({
		data: form_data,
		url: url,
		success: function(response){
			//var _data = $.parseJSON(response);
			$('.prod-' + form_data.uid + form_data.pid ).parents('.plist-four').remove();
		}
	});
});

$(document).on('click', '.trend-box .follow-btn.edit, .collection-desc-wrapper .btn.edit', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	if(store.get('user_id') == 0 || typeof(store.get('user_id')) == "undefined") {
		signin_modal();
		return;
	}
	else {
		return location.href=$(this).attr('href');
	}
});
$(document).on('click', '.collection-details-link', function(e) {
	e.preventDefault(); e.stopImmediatePropagation();
	location.href = $(this).attr('data-url');
});

$(document).on('click', '.list-collections .two-list li a, .u-product a:not(.prodname)', function(e) {
	e.preventDefault(); e.stopImmediatePropagation();
	location.href = $(this).attr('href');
});
$(document).on('click', '.open-details', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	location.href = $(this).attr('data-url');
});



/* 	===================================
 *	Functions Section
 *	===================================
 */
 
function prod_resize(){
	var windowsize = $(window).width();
	if (windowsize <= 1000) {
		$('.pro-listing ul li').each(function(){
			$('.plist-three').removeClass("small-4 large-4");
			$('.plist-four').removeClass('small-6 large-3');
			$('.pro-listing ul li').removeClass('plist-three');
			$('.pro-listing ul li').removeClass('plist-four');
			$('.li-g-one').addClass('plist-three small-6 large-6');
			$('.li-g-two').addClass('plist-four small-4 large-4');
		});
	}
	if (windowsize > 1000) {
		$('.pro-listing ul li').each(function(){
			$('.li-g-one').removeClass('plist-three small-6 large-6');
			$('.li-g-two').removeClass('plist-four small-4 large-4');
			$('.li-g1').addClass('plist-three small-4 large-4');
			$('.li-g2').addClass('plist-four small-6 large-3');
		});
	}
}

function _upload_userpic(input) {
	$('#header .thumb-wrapper .loader-overlay').removeClass('hide');
	$('#header .thumb-wrapper .loader').removeClass('hide');

	var file = input[0].files;
	var form_data = new FormData();

	$.each(file, function(key, value)
	{
		form_data.append('userfile', value);
	});
	
	$.ajax({
		type: 'POST',
		data: form_data,
		cache: false,
		processData: false,
		contentType: false, 
		url: "/admin/account?action=upload&isAjax=true&token="+store.get('token')+"&user_id="+store.get('user_id'),
		success: function(response){
			var results = $.parseJSON(response);

			if(results.status == "success") {
				$.get(results.file, function(contents){
					$('#header .profile-img img').attr('src',results.file);
					$('#header .thumb-wrapper .thumb').css('background-image','url('+results.file+')');
				});
			}
			$('#header .thumb-wrapper .loader-overlay').addClass('hide');
			$('#header .thumb-wrapper .loader').addClass('hide');
		}
	});
}

function close_panels() {
	var el = $('#header .userNav');
		cart = el.find('.cart-panel'),
		wallet = el.find('.wallet-panel');

	el.find('.navLink').removeClass('selected');
	cart.find('.loader-overlay').addClass('hide');
	cart.find('.loader').addClass('hide');

	cart.find('.close-btn').addClass('hide');
	cart.find('h3').addClass('hide');
	cart.addClass('hide');

	wallet.addClass('hide');
}

function load_messages(element) {
	$('.messages-panel').removeClass('hide');
}

function close_messages() {
	$('.messages-panel').addClass('hide');
}

function load_cart(element) {
	close_panels();
	element = $('#header .userNav .navLink.cart');

	var container = element.find('.cart-panel'),
		loader = container.find('.loader'),
		overlay = container.find('.loader-overlay'),	
		closebtn = container.find('.close-btn'),
		h3 = element.find('h3'),
		ajax_container = container.find('.cart-panel-inner-scoller');

	if(element.hasClass('selected')) {
		/*
		element.removeClass('selected');
		overlay.addClass('hide');
		loader.addClass('hide');
		container.addClass('hide');
		*/
	}
	else {
		element.addClass('selected');
		overlay.removeClass('hide');
		loader.removeClass('hide');

		ajax_container.html('');
		updateCartSizeContainer();
		if($.trim(ajax_container.html()) == "") {
			container.removeClass('hide');
			SSAjax({
				url: '/admin/ajax?action=cart',
				data: {cart_sess:store.get('cart_sess')},
				success: function(response){
					overlay.addClass('hide');
					loader.addClass('hide');

					closebtn.removeClass('hide');
					h3.removeClass('hide');
					ajax_container.html(response);
					updateCartSizeContainer();
				}
			});		
		}
		else {
			overlay.addClass('hide');
			loader.addClass('hide');

			closebtn.removeClass('hide');
			h3.removeClass('hide');
			container.removeClass('hide');
			updateCartSizeContainer();
		}
	}
}

function close_cart() {
	var element = $('#header .userNav'),
		container = element.find('.cart-panel'),
		loader = container.find('.loader'),
		overlay = container.find('.loader-overlay'),
		
		closebtn = container.find('.close-btn'),
		h3 = container.find('h3');

	element.find('.navLink').removeClass('selected');
	overlay.addClass('hide');
	loader.addClass('hide');

	closebtn.addClass('hide');
	h3.addClass('hide');
	container.addClass('hide');
}

function delete_cart_item(el) {
	var input = el.parent().find('input');

	var quantity = input.val();
	var m = input.attr('m');
	var p = input.attr('p');
	var variant_id = input.attr('variant_id');
	var su = input.attr('su');
	var s = input.attr('s');
	var has_options = 0;
	var product_options = input.attr('options');
	
	if(typeof(variant_id) != 'undefined' && variant_id > 0) {
		has_options = 1;
	}
	var data = {product_user_id:m,
				product_id:p, 
				store_id: s,
				store_user_id: su,
				quantity: quantity,
				has_options: has_options,
				variant_id: variant_id,
				product_options:product_options,
				cart_sess: store.get('cart_sess')
				};


	SSAjax({
			data: data,
			url: '/admin/ajax?action=cart&loadtype=updatecart&update=delete_item',
			success: function(response){
				//var response = $.parseJSON(response);
				load_cart();
			}
	});						
}

function load_wallet(element) {
	close_panels();
	element = $('#header .userNav .navLink.wallet');
	//element = element.parent();
	var container = element.find('.wallet-panel'),
		loader = container.find('.loader'),
		overlay = container.find('.loader-overlay'),	
		closebtn = container.find('.close-btn'),
		h3 = element.find('h3'),
		ajax_container = container.find('#wallet-wrapper');	
	
	container.find('#purchases-history-wrapper').addClass('hide');
	container.find('#claimed-history-wrapper').addClass('hide');
	container.find('#claim-wallet-form').addClass('hide');
	container.find('#wallet-paypalemail-form').addClass('hide');
	container.find('#wallet-wrapper').removeClass('hide');
	ajax_container.html('');
	closebtn.addClass('hide');
	h3.addClass('hide');

	if(element.hasClass('selected')) {
		
	}
	else {
		element.addClass('selected');
		overlay.removeClass('hide');
		loader.removeClass('hide');
		container.removeClass('hide');

		if($.trim(ajax_container.html()) == "") {
			container.removeClass('hide');
			SSAjax({
				url: '/admin/ajax?action=wallet',
				success: function(response){
					overlay.addClass('hide');
					loader.addClass('hide');
					closebtn.removeClass('hide');
					h3.removeClass('hide');
	
					ajax_container.html(response);
					updateWalletSizeContainer();
				}
			});		
		}
		else {
			overlay.addClass('hide');
			loader.addClass('hide');

			container.removeClass('hide');
			updateWalletSizeContainer();
		}
	}
}

function close_wallet() {
	var element = $('#header .userNav'),
		container = element.find('.wallet-panel'),
		loader = container.find('.loader'),
		overlay = container.find('.loader-overlay'),
		closebtn = container.find('.close-btn'),
		h3 = container.find('h3');

	element.find('.navLink').removeClass('selected');
	overlay.addClass('hide');
	loader.addClass('hide');

	closebtn.addClass('hide');
	h3.addClass('hide');
	container.addClass('hide');
}

function claim_walet_popup() {
	$('#header .userNav .wallet-panel').addClass('bgGray');
	$('#header .userNav #wallet-wrapper').addClass('hide');

	var ajax_container = $('#header .userNav #claim-wallet-form'),
		loader = $('#header .userNav .wallet-panel .loader'),
		overlay = $('#header .userNav .wallet-panel .loader-overlay'),
		closebtn = $('#header .userNav .wallet-panel .close-btn'),
		h3 = $('#header .userNav .wallet-panel h3');

	closebtn.addClass('hide');
	h3.addClass('hide');

	overlay.removeClass('hide');
	loader.removeClass('hide');	
	//$('#header .userNav #purchases-history-wrapper').addClass('hide');
	ajax_container.html('').removeClass('hide');

	if($.trim(ajax_container.html()) == "") {
		SSAjax({
			url: '/admin/ajax?action=claim_wallet_form',
			success: function(response){
				overlay.addClass('hide');
				loader.addClass('hide');

				ajax_container.html(response);
				updateWalletSizeContainer();
			}
		});		
	}
	else {
		overlay.addClass('hide');
		loader.addClass('hide');

		container.removeClass('hide');
		updateWalletSizeContainer();
	}
} 

function wallet_paypalemail_popup() {
	$('#header .userNav .wallet-panel').addClass('bgGray');
	$('#header .userNav #wallet-wrapper').addClass('hide');

	var ajax_container = $('#header .userNav #wallet-paypalemail-form'),
		loader = $('#header .userNav .wallet-panel .loader'),
		overlay = $('#header .userNav .wallet-panel .loader-overlay'),
		closebtn = $('#header .userNav .wallet-panel .close-btn'),
		h3 = $('#header .userNav .wallet-panel h3');

	closebtn.addClass('hide');
	h3.addClass('hide');

	overlay.removeClass('hide');
	loader.removeClass('hide');	

	$('#header .userNav .wallet-panel').removeClass('bgGray');
	$('#header .userNav #claim-wallet-form').addClass('hide');
	ajax_container.html('').removeClass('hide');

	if($.trim(ajax_container.html()) == "") {
		SSAjax({
			url: '/admin/ajax?action=wallet_paypalemail_form',
			success: function(response){
				overlay.addClass('hide');
				loader.addClass('hide');

				ajax_container.html(response);
				updateWalletSizeContainer();
			}
		});		
	}
	else {
		overlay.addClass('hide');
		loader.addClass('hide');

		container.removeClass('hide');
		updateWalletSizeContainer();
	}
}

function save_promoter_paypalemail() {
	var email_field = $('#wallet-paypalemail-form .paypal-form-wrapper .field input');
	var country_field = $('#wallet-paypalemail-form .paypal-form-wrapper .field select');
	var email = $.trim(email_field.val());
	var country = country_field.val();
	var error_wrapper = $('#wallet-paypalemail-form .paypal-form-wrapper .field div.error-paypal-email');
	var loader = $('#header .userNav .wallet-panel .loader'),
		overlay = $('#header .userNav .wallet-panel .loader-overlay');

	email_field.val(email);
	error_wrapper.addClass('hide').html('');

	if(email == "") {
		error_wrapper.removeClass('hide').html('Please provide a valid email.');
		email_field.focus();
		country_field.focus();
		return;
	}

	if(country == "") {
		error_wrapper.removeClass('hide').html('Please select a country.');
		country_field.focus();
		return;
	}
	overlay.removeClass('hide');
	loader.removeClass('hide');	

	SSAjax({
			data: {paypal_email:email, country:country},
			url: '/admin/ajax?action=wallet_paypalemail_form&save_paypalemail=true',
			success: function(response){
				var response = $.parseJSON(response);

				overlay.addClass('hide');
				loader.addClass('hide');

				if(response.status == "success") {
					$('#wallet-paypalemail-form').addClass('hide');
					claim_walet_popup();
				}
				else {
					error_wrapper.removeClass('hide').html(response.error);
				}
			}
		});


}

function claim_wallet(element) {
	var errorfield = $('#claim-wallet-form .claim-form-wrapper .field .field-holder .error-wallet');
	var moneyonwallet = parseFloat(element.attr('moneyonwallet'));
	var min_amount = parseFloat(element.attr('min-amount'));
	var amount = parseFloat($('#claim-wallet-form .claim-form-wrapper .field input#claim-wallet-amount').val());

	errorfield.removeClass('hide');

	//alert(amount + ' == ' + min_amount)
	if(amount < min_amount) {
		errorfield.html('Please enter an amount greater than or equal to ' + min_amount + '.');
		return;
	}
	else if(amount > moneyonwallet) {
		errorfield.html('The amount you entered is greater than the money on your wallet.');
		return;
	}

	errorfield.addClass('hide').html('');

	$('#header .userNav .wallet-panel .loader').removeClass('hide');
	$('#header .userNav .wallet-panel .loader-overlay').removeClass('hide');

	SSAjax({
			data: {moneytoclaim:amount},
			url: '/admin/ajax?action=claim_wallet',
			success: function(response){
				var response = $.parseJSON(response);

				$('#header .userNav .wallet-panel .loader').addClass('hide');
				$('#header .userNav .wallet-panel .loader-overlay').addClass('hide');

				if(response.status == "success") {
					var html = '<div class="success-claimed-wallet">\
									<strong>You have successfully claimed your money.</strong>\
									<a>[OK]</a>\
								</div>';
					$('#claim-wallet-form').html(html);
				}
				else {
					errorfield.html(response.message).removeClass('hide');
				}
			}
		});
	
}

function load_wallet_claimed_history() {
	$('#header .userNav .wallet-panel').addClass('bgGray');
	$('#header .userNav #wallet-wrapper').addClass('hide');

	var ajax_container = $('#header .userNav #claimed-history-wrapper'),
		loader = $('#header .userNav .wallet-panel .loader'),
		overlay = $('#header .userNav .wallet-panel .loader-overlay'),
		closebtn = $('#header .userNav .wallet-panel .close-btn'),
		h3 = $('#header .userNav .wallet-panel h3');

	closebtn.addClass('hide');
	h3.addClass('hide');

	overlay.removeClass('hide');
	loader.removeClass('hide');	
	//$('#header .userNav #purchases-history-wrapper').addClass('hide');
	ajax_container.html('').removeClass('hide');

	if($.trim(ajax_container.html()) == "") {
		SSAjax({
			url: '/admin/ajax?action=wallet_claimed_history',
			success: function(response){
				overlay.addClass('hide');
				loader.addClass('hide');

				ajax_container.html(response);
				updateWalletSizeContainer();
			}
		});		
	}
	else {
		overlay.addClass('hide');
		loader.addClass('hide');

		container.removeClass('hide');
		updateWalletSizeContainer();
	}
}

function load_wallet_purchase_history() {
	$('#header .userNav .wallet-panel').addClass('bgGray');
	$('#header .userNav #wallet-wrapper').addClass('hide');

	var ajax_container = $('#header .userNav #purchases-history-wrapper'),
		loader = $('#header .userNav .wallet-panel .loader'),
		overlay = $('#header .userNav .wallet-panel .loader-overlay'),
		closebtn = $('#header .userNav .wallet-panel .close-btn'),
		h3 = $('#header .userNav .wallet-panel h3');

	closebtn.addClass('hide');
	h3.addClass('hide');

	overlay.removeClass('hide');
	loader.removeClass('hide');	
	//$('#header .userNav #claimed-history-wrapper').addClass('hide');
	ajax_container.html('').removeClass('hide');

	if($.trim(ajax_container.html()) == "") {
		SSAjax({
			url: '/admin/ajax?action=purchase_history',
			success: function(response){
				overlay.addClass('hide');
				loader.addClass('hide');

				ajax_container.html(response);
				updateWalletSizeContainer();
			}
		});		
	}
	else {
		overlay.addClass('hide');
		loader.addClass('hide');

		container.removeClass('hide');
		updateWalletSizeContainer();
	}

}

function load_message(element) {
	close_panels();

	var container = element.find('.message-panel'),
		loader = container.find('.loader'),
		overlay = container.find('.loader-overlay'),	
		closebtn = container.find('.close-btn'),
		h3 = element.find('h3'),
		ajax_container = container.find('.cart-panel-inner-scoller');

	element.addClass('selected');
	overlay.removeClass('hide');
	loader.removeClass('hide');

	overlay.addClass('hide');
	loader.addClass('hide');
	container.removeClass('hide');		
}

function updateCartSizeContainer(){
	class_or_id = '#header .userNav .cart-panel .cart-panel-inner .cart-panel-inner-scoller';
	parent_class_or_id = '#header .userNav .cart-panel .cart-panel-inner'; 
	var window_height = $('#header .userNav .cart-panel').height() - 86;
	
	$(class_or_id).removeAttr('style');
	$(class_or_id + ' .slimScrollBar, ' + class_or_id +' .slimScrollRail').remove();
	if($(parent_class_or_id + ' .slimScrollDiv').length > 0) {
		$(class_or_id).parent().replaceWith($(class_or_id));
	}
	
	$(class_or_id).slimScroll({
		height: window_height,
		width: '100%',
		distance: '6px'
	});
}

function updateWalletSizeContainer() {
	class_or_id = '#header .userNav .wallet-panel .panel-inner-scroller';
	parent_class_or_id = '#header .userNav .wallet-panel'; 
	//var window_height = $('#header .userNav .wallet-panel').height();
	var window_height = $(window).height() - 60;

	$(class_or_id).removeAttr('style');
	$(class_or_id + ' .slimScrollBar, ' + class_or_id +' .slimScrollRail').remove();
	if($(parent_class_or_id + ' .slimScrollDiv').length > 0) {
		$(class_or_id).parent().replaceWith($(class_or_id));
	}

	$(class_or_id).slimScroll({
		height: window_height,
		width: 'auto',
		distance: '4px'
	});
}
function resizeProductDetails(){
	var wH = $(window).height(),
		mH = wH - 68;
		mH2 = wH - 60;
	$('.prod-details-wrapper').css({'height': mH + 'px'});
	$('.modal-content').css({'height': mH + 'px'});
	$('.blog-content').css({'height': mH + 'px'});
}
function updateSizeContainer(class_or_id, parent_class_or_id){
	var height = $('.modal-header').height(),
		window_height = $(window).height();
	window_height = window_height - height;
	
	$(class_or_id).removeAttr('style');
	$(class_or_id + ' .slimScrollBar, ' + class_or_id +' .slimScrollRail').remove();
	if($(parent_class_or_id + ' .slimScrollDiv').length > 0) {
		$(class_or_id).parent().replaceWith($(class_or_id));
	}
	
	$(class_or_id).slimScroll({
		height: window_height,
		width: '100%',
		distance: '6px'
	});
	$('.prod-details-wrapper').css({'height': window_height + 'px'});
}
 
function toggleScroll_leftmenu() {
	$("#leftmenu-sc").removeAttr('style');
	$('#leftmenu-sc #slimScrollBar, #leftmenu-sc #slimScrollRail').remove();
	if($('.left-submenu .slimScrollDiv').length > 0) {
		$("#leftmenu-sc").parent().replaceWith($("#leftmenu-sc"));
	}

	var lm_height = $('.left-submenu').height();

	if(lm_height-95 < $('#leftmenu-sc').height()) {
		$('#leftmenu-sc').slimScroll({
			height: lm_height-1
		});
	}

	return;
} 

function doOpen(element){
	doOpenURL(element.attr('href'));
}

function doOpenURL(url){
	$('#wrapper').empty();
	$('#mload').show();
	var page = url;
	var form_data = {isAjax:true};
	History.pushState(null, 'Shopstyx', page);
	SSAjax({
		data: form_data,
		url: page,
		success: function(response){
			$('#wrapper').html(response);
			$('#mload').addClass('hide');
		}
	});
}

function view_proddetails(element) {
	if (lastmodal) slideItDown();
	$('#wrapper').empty();
	$('#mload').show();
	//$('#productViewModal').foundation('reveal', 'open');
	
	/*$('#productViewModal').foundation('reveal', 'open', {
		animation_speed: 0
	});*/
	
	var page = element.attr('href');
	var form_data = {};
	form_data.display_details = true;

	History.pushState(null, 'Shopstyx', page);
	SSAjax({
		data: form_data,
		url: page,
		success: function(response){
			$('#wrapper').html(response);
			$('#mload').hide();
		}
	});
}

function addproduct(options) {
	var settings = $.extend(true,{
			"url": "/admin/addproduct?action=basic",
			"data": {product_id:0},
			"loadJS": [cloud_url+'js/v2/addproduct.js'
						
					],
			"loadCSS": [cloud_url+'css/v2/addproduct.css'],
			"loadType": "default"
			//"onSaved": function(response) {},
			//"beforeClose": funcion() { }
	},options);

	var onSaved, beforeClose;
	if(settings.hasOwnProperty('onSaved')) {
		onSaved = settings.onSaved;
	}

	if(settings.hasOwnProperty('beforeClose')) {
		beforeClose = settings.beforeClose;
	}

	if(settings.loadType == "default") {
		if(settings.data.product_id > 0 || store.get('addproduct_modal') == "advanced") {
			settings.url = "/admin/addproduct?action=advanced";
			$('#addproductModal').removeClass('basic').addClass('advanced');
		}
		else {
			settings.url = "/admin/addproduct?action=basic";
			$('#addproductModal').removeClass('advanced').addClass('basic');
		}

		$('#addproductModal .shopstyxloader').removeClass('hide');
		$('#addproductModal #modal-content').html("").hide();
		$('#wrapper').hide();
		if(settings.data.product_id > 0 || store.get('addproduct_modal') == "advanced") {
			var wH = $(window).height();
			//$('#addproductModal').css('height',wH);
			$('#addproductModal').css('left',0);
		}
		else {
			$('#addproductModal').centerized({});
		}
		$('#addproductModal').foundation('reveal', 'open');

	} else {
		$('#addproductModal-basic').show('slide', {direction: 'right'}, 400);
	}

	$('body').css('overflow','inherit');
	settings.success = function(response){

		if(settings.loadType == "default") {
			$('#addproductModal .overlay').addClass('hide');
			$('#addproductModal .shopstyxloader').addClass('hide');
			$('#addproductModal #modal-content').html(response).show();
			$('#wrapper').css('height',$('#addproductModal').height()+2);
			
			resize_containers();

		}
		else {
			$('#addproductModal-basic .overlay').addClass('hide');
			$('#addproductModal-basic .shopstyxloader').addClass('hide');
			$('#addproductModal-basic #modal-content').html(response).show();
		}	

		$('#addproductModal form').data('image_files',[]);
		$('#addproductModal form').data('callback','');
		$('#addproductModal form').data('beforeClose','');

		if(typeof onSaved == "function") {
			$('#addproductModal form').data('callback', onSaved);			
		}

		if(typeof beforeClose == "function") {
			$('#addproductModal form').data('beforeClose', beforeClose);			
		}
	}

	SSAjax(settings);
}

function stepRedirect(current_step,referer){
	referer = typeof referer == "undefined" ? false : referer;
	if(store.get('user_type')){
		var dpromote = store.get('promoter_data');
		if(typeof dpromote !== "undefined"){
			SSAjax({
				url: '/admin/build?action=save-products',
				data: {isAjax:true,isContent:true,promote_data:dpromote},
				success: function(response){
					var _data = $.parseJSON(response);
					if( _data.no_error){
						store.remove('promoter_data');
					}
				}
			});
		}
	}
	
	if(current_step == 1){
		History.pushState(null, 'shop_info', '/admin/build?page=shop_info');
		mainURL = '/admin/build?page=shop_info';
	}
	if(current_step == 2){
		History.pushState(null, 'catalog', '/admin/build?page=catalog');
		mainURL = '/admin/build?page=catalog';
	}
	if(current_step == 3){
		History.pushState(null, 'get_paid', '/admin/build?page=get_paid');
		mainURL = '/admin/build?page=get_paid';
	}
	if(current_step == 4){
		History.pushState(null, 'shop_preview', '/admin/build?page=shop_preview');
		mainURL = '/admin/build?page=shop_preview';
	}


	if(current_step > 4){
		if(referer) {
			History.pushState(null, 'Shopstyx', referer);
			mainURL = referer;
			location.href = referer;
			return false;
		}
		else {
			History.pushState(null, 'shop_info', '/admin/dashboard');
			mainURL = '/admin/dashboard';
			location.href = root + 'admin/dashboard';
			return false;
		}
	}
	SSAjax({
		url: mainURL,
		data: {isAjax:true,isContent:true},
		loadJS: [cloud_url+'js/v2/build.js'],
		loadCSS: [cloud_url+'css/v2/build.css'],
		success: function(response){
			$('#wrapper').html(response);
			hideLoader();
			return false;
		}
	});
}

function showLoader(){
	$('#wrapper').addClass('hide');
	$('#loadbox-wrapper').removeClass('hide');
}

function hideLoader(){
	$('#loadbox-wrapper').addClass('hide');
	$('#wrapper').removeClass('hide');
}

function showModalLoader(){
	$('.mod-loader-wrapper').show();
}

function hideModalLoader(){
	$('.mod-loader-wrapper').hide();
}

function convertToSlug(Text)
{
	return Text
        .toLowerCase()
        .replace(/ /g,'-')
        .replace(/[^\w-]+/g,'');
}

function loadDepartments(dataObj){
	var form_data = {};
	form_data.isAjax = true;
	$.extend(form_data,dataObj);
	if(typeof form_data.loadProducts !== 'undefined' ){ $('.product-loader').removeClass('hide'); }
	SSAjax({
		data: form_data,
		url: "/admin/departments?action=load",
		success: function(response){
			if(form_data.department_level == 'main'){
				$('#department-menus').html(response);
				$('#department-sub-menus').addClass('hide');
				$('#department-menus').removeClass('hide');
			}
			if(form_data.department_level == 'sub'){
				$('#department-sub-menus').html(response);
				$('#department-menus').addClass('hide');
				$('#department-sub-menus').removeClass('hide');
				$('.dept-title').html(form_data.department_title);
				$('.all-dept-lbl').addClass('hide');
				//$('.dept-spacing').removeClass('hide');
				$('#back-departments').attr('data-id', form_data.parent_id);
				$('#back-departments').attr('data-catname', form_data.department_title);
			}
			$('.department-loading').remove();
			if(typeof form_data.loadProducts !== 'undefined' ){
				loadDepartmentProducts({department_id: form_data.department_id, level: form_data.level});
			}
		}
	});
}

function loadDepartmentProducts(dataObj){
	var form_data = {},
		link;
	form_data.isAjax = true;
	$.extend(form_data,dataObj);
	if(typeof form_data.more !== 'undefined'){
		link = '/admin/departments?action=load-products&page=' + form_data.page_num;
	}else{
		$('.product-loader').removeClass('hide');
		link = '/admin/departments?action=load-products';
	}
	SSAjax({
		data: form_data,
		url: link,
		success: function(response){
			if(typeof form_data.more !== 'undefined'){
				$('.more-product').before(response);
				$('.loader-image').addClass('hide');
			}else{
				$('.shop-product-wrapper').html(response);
				$('.shop-product-wrapper').append('<p class="more-product bgcolor3"><span class="more-button color4"><span class="loader-image hide"></span>Load more...</span></p>');
				var total_page = $('#h-total_pages').val(),
					current_page = $('#h-current_page').val();
				if(total_page > current_page){ 
					$('.more-product').removeClass('hide'); 
				}else{
					$('.more-product').addClass('hide'); 
				}
				$('.product-loader').addClass('hide');
			}
			$('.shop-left').addClass('hide2');
		}
	});
}

function loadProducts(dataObj){
	var form_data = {},
		link;
	form_data.isAjax = true;
	$.extend(form_data,dataObj);
	SSAjax({
		data: form_data,
		url: '/admin/build?page=catalog&page_num=' + form_data.page_num,
		success: function(response){ 
			//console.log('response');
			$('.promoter-products').append(response);
			var total_page = $('#c-total_pages').val(),
				current_page = $('#c-current_page').val();
			if(total_page > current_page){ 
				$('.more-promoter-products').removeClass('hide'); 
			}else{
				$('.more-promoter-products').addClass('hide'); 
			}
			$('.loader-image').addClass('hide');
		}
	});
}

var customer_purchase_order = function() {
	var height = $(window).height();
	var modal = $('.main-modal.modal-order-details');
	var overlay = $('.main-modal .overlay');
	var loader = $('.main-modal .loader');

	this.toggle_loader = function() {
		if(loader.hasClass('hide')) {
			loader.removeClass('hide');
		}
		else {
			loader.addClass('hide');
		}
	}

	this.open = function() {
		//modal.find('.mmodal-inner').html('');
		//modal.css({'height': height});
		$('body').css({'overflow':'hidden'});
		modal.show('slide', {direction: 'right'}, 400);
	}

	this.close = function() {
		modal.find('.mmodal-inner').html('');
		$('body').css({'overflow':'inherit'});
		modal.hide('slide', {direction: 'right'}, 200);
	}

	this.resizeModal = function() {
		class_or_id = '.main-modal.modal-order-details .mmodal-inner .content .inner-scoller';
		parent_class_or_id = '.main-modal.modal-order-details .mmodal-inner .content'; 
		var window_height = modal.height() - 68;
		
		$(class_or_id).removeAttr('style');
		$(class_or_id + ' .slimScrollBar, ' + class_or_id +' .slimScrollRail').remove();
		if($(parent_class_or_id + ' .slimScrollDiv').length > 0) {
			$(class_or_id).parent().replaceWith($(class_or_id));
		}
		
		$(class_or_id).slimScroll({
			height: window_height,
			width: '100%',
			distance: '6px'
		});

		$(parent_class_or_id).height(window_height);

	}
};

function openModal(){
	var height = $(window).height();
	$('.dashboard-modal .mod-wrapper').css({'height': height});
	$('body').css({'overflow':'hidden'});
	$('.dashboard-modal').show('slide', {direction: 'right'}, 400);
}
function closeModal(){
	$('body').css({'overflow':'inherit'});
	$('.dashboard-modal').hide('slide', {direction: 'right'}, 200);
}

function openModal2(){
	$('.dashboard-modal').show('slide', {direction: 'right'}, 400);
}
function openModal3(){
	$('.blog-modal2').show('slide', {direction: 'right'}, 400);
}
function closeModal2(){
	$('.dashboard-modal').hide('slide', {direction: 'right'}, 200);
}
function closeModal3(){
	$('.blog-modal2').hide('slide', {direction: 'right'}, 200);
}

function openCustomModal(id_or_class){
	var height = $(window).height();
	$(id_or_class + ' .mod-wrapper').css({'height': height});
	$('body').css({'overflow':'hidden'});
	$(id_or_class).show('slide', {direction: 'right'}, 400);
}
function closeCustomModal(id_or_class){
	$('body').css({'overflow':'inherit'});
	$(id_or_class).hide('slide', {direction: 'right'}, 200);
}
function closeCustomModal2(id_or_class){
	$(id_or_class).hide('slide', {direction: 'right'}, 200);
}
/* dot pre-loader function 
	sample:
	<span id="text-pre-loader" class="text-pre-loader-style bgcolor1 color3">Saving<span class="dots"></span></span>
*/
var dots = 0;
$(document).ready(function()
{
    setInterval (type, 600);
});

function type()
{
    if(dots < 3)
    {
        $('.dots').append('.');
        dots++;
    }
    else
    {
        $('.dots').html('');
        dots = 0;
    }
}

function checkdate(value){
	var validformat=/^\d{4}\/\d{2}\/\d{2}$/;

	if (!validformat.test(value)) {
		return false;
	}
	else{ 
		var month = value.split("/")[0], day = value.split("/")[1], year = value.split("/")[2];
		var myDate = new Date(year, month, day);

		if ((myDate.getMonth()+1!=month) || 
			(myDate.getDate()!=dayfield) ||
			(myDate.getFullYear()!=yearfield)) {
			return false;
		}
		else {
			return true;
		}
	}
}

function shopsetting_form_params(field) {
	var radioCheck = /radio|checkbox/i;
	var data = {},
				current;
	var keyBreaker = /[^\[\]]+/g;			

	$(field).each(function(){
		var el = this,
			type = el.type && el.type.toLowerCase();
		var key = el.name;
		var parts = key.match(keyBreaker),
			value = $.data(el, "value") || $.fn.val.call([el]),
			isRadioCheck = radioCheck.test(el.type),
			write = !isRadioCheck || !! $(el).attr('checked'),
			lastPart;

		//console.log(parts);
		current = data;
		for ( var i = 0; i < parts.length - 1; i++ ) {
			if (!current[parts[i]] ) {
				current[parts[i]] = {};
			}
			current = current[parts[i]];

			//console.log(current);
		}

		lastPart = parts[parts.length - 1];
		if ( lastPart in current && type === "checkbox" ) {
			if (!$.isArray(current[lastPart]) ) {
				current[lastPart] = current[lastPart] === undefined ? [] : [current[lastPart]];
			}
			if ( write ) {
				current[lastPart].push(value);
			}
		} else if ( write || !current[lastPart] ) {
			current[lastPart] = write ? value : undefined;
		}
	});

	return data;
}

function shopsetting_shipping_form_params() {
	var radioCheck = /radio|checkbox/i;
	var data = {},
				current;
	var keyBreaker = /[^\[\]]+/g;			

	$('.shopsettings input[name^="shipping_global_meta"]').each(function(){
		var el = this,
			type = el.type && el.type.toLowerCase();
		var key = el.name;
		var parts = key.match(keyBreaker),
			value = $.data(el, "value") || $.fn.val.call([el]),
			isRadioCheck = radioCheck.test(el.type),
			write = !isRadioCheck || !! $(el).attr('checked'),
			lastPart;

		//console.log(parts);
		current = data;
		for ( var i = 0; i < parts.length - 1; i++ ) {
			if (!current[parts[i]] ) {
				current[parts[i]] = {};
			}
			current = current[parts[i]];

			//console.log(current);
		}

		lastPart = parts[parts.length - 1];
		if ( lastPart in current && type === "checkbox" ) {
			if (!$.isArray(current[lastPart]) ) {
				current[lastPart] = current[lastPart] === undefined ? [] : [current[lastPart]];
			}
			if ( write ) {
				current[lastPart].push(value);
			}
		} else if ( write || !current[lastPart] ) {
			current[lastPart] = write ? value : undefined;
		}
	});

	return data;
}

function save_shopsettings(form) {
	if($('.shopsettings .save-btn').html() == "Saving...") {
		return;
	}

	toggle_preloader_maincontent();
	$('#body-wrapper .overlay').eq(0).addClass('hide');
	$('#subcontent-wrapper').eq(0).removeClass('hide');

	$('.shopsettings .save-btn').html('Saving...');
	$('.form-submission-message').stop().hide();

	var form_data = {action:'saveform',isAjax:true,body_wrapper:true,subcontent_wrapper:true};
	if(form.attr('name') != "shipping-settings-form") {
		form.serializeArray().map(function(x){
			form_data[x.name] = x.value;
		});
	}
	// for shipping form only
	else {
		$('.shopsettings table tr.add-country .error-shipping').addClass('hide');
		var shipping_data = shopsetting_shipping_form_params();
		$.extend(form_data,shipping_data);

		if($.isEmptyObject(shipping_data)) {
			$('.shopsettings tr.error-shipping').removeClass('hide');
			toggle_preloader_maincontent();
			$('.shopsettings .save-btn').html('Save Changes');


			$('.form-submission-message').removeClass('ss-success');
			$('.form-submission-message')
				.addClass('ss-error')
				.fadeIn(500, function() {
					$('.shopsettings .save-btn').html('Save Changes');
				});
			return;
		}
		else {
			$('.shopsettings tr.error-shipping').addClass('hide');
		}			
	}

	if(form.attr('name') == "tax-settings-form") {
		var tax_addedstate_length = $('.shopsettings .tax-wrapper li').not('.addstate-wrapper').find('.added-cityzip-wrapper div').length;

		if(tax_addedstate_length == 0) {
			/*
			toggle_preloader_maincontent();
			$('.form-submission-message').removeClass('ss-success');
			$('.shopsettings .tax-wrapper li.addstate-wrapper .common-field-wrapper').addClass('error').find('.error').html('Please add at least one state.');
			$('.form-submission-message')
				.addClass('ss-error')
				.fadeIn(500, function() {
					$('.shopsettings .save-btn').html('Saving Changes');
				});
			return;
			*/
		}
	}
	//$('.shopsettings form').attr('disabled','disabled').find('button.save-btn').attr('disabled','disabled');

	switch(form.attr('name')) {
		case 'general-settings-form': 
		case 'shipping-settings-form':
		case 'policy-settings-form': 
		case 'tax-settings-form':
			SSAjax({
				data: form_data,
				url: form.attr('action'),
				success:function(response) {
					var response = $.parseJSON(response);

					if(response.status != "success") {
						if(form.attr('name') != "shipping-settings-form") {
							for(e in response.errors) {
								if(e == "address") {
									form.find('textarea[name="'+e+'"]').parent().addClass('error').find('.error').html(response.errors[e]);
								} else {
									form.find('input[name="'+e+'"]').parent().addClass('error').find('.error').html(response.errors[e]);
								}
							}
						}
						
						$('.form-submission-message').removeClass('ss-success');
						$('.form-submission-message')
							.addClass('ss-error')
							.fadeIn(500, function() {
								$('.shopsettings .save-btn').html('Save Changes');;
							});
						
					}
					else {
						$('.form-submission-message').removeClass('ss-error');
						$('.form-submission-message')
							.addClass('ss-success')
							.fadeIn(500, function() {
								$('.shopsettings .save-btn').html('Save Changes');
							})
							.fadeOut(15000);

						if($('#header .userNav #claim-wallet-form').hasClass('hide') == false) {
							claim_walet_popup();
						}
					}

					toggle_preloader_maincontent();
					$('#body-wrapper .overlay').eq(0).addClass('hide');
					$('#subcontent-wrapper').eq(0).removeClass('hide');
				}
			});
			break;	
	}
}

function reloadPage(dataObj){
	var form_data = {};
	form_data.isAjax = true;
	$.extend(form_data,dataObj);
	$(form_data.preloader).show();
	SSAjax({
		data: form_data,
		url: form_data.link,
		success: function(response){
			$(form_data.wrapper).html(response);
			$(form_data.preloader).hide();
			$(form_data.preloader + ' .shopstyxloader div').html('LOADING...');
		}
	});
}

function toggle_statezip_autocomplete(opts) {
	opts = typeof opts == "undefined" ? {} : opts;
	var defaults = {enabled:true,
					state_code: '',
					state: 'select#state_code',
					city: 'input#city',
					zip: 'input#zip',
					};
	opts = $.extend(true,defaults,opts);
	if(opts.state_code == "") {
		if($(opts.state).val() == "" || $(opts.state).length == 0) {
			return;
		}
		else {
			opts.state_code = $(opts.state).val();
		}
	}

	if(!opts.enabled) {
		//$(opts.city).autocomplete( "option", "disabled", true );
		$(opts.zip).autocomplete( "option", "disabled", true );
	}
	else {
		//$(opts.city).autocomplete("enabled");
		$(opts.city).autocomplete({
			//define callback to format results  
			source: function(req, add){  
				//pass request to server  
				$.getJSON("/autocomplete/US_city.html?state_code="+opts.state_code+"&callback=?", req, function(data) {  
						//create array for response objects  
						var suggestions = [];  
						//process response  
						$.each(data, function(i, val){  
						suggestions.push(val.name);  
					});  

					//pass array to callback  
					add(suggestions);  
				});  
			}, 
			select: function(e, ui) {  
				//create formatted friend  
				var city = ui.item.value; 
				$.get("/autocomplete/US_zip.html?state_code="+opts.state_code+"&city="+city+"&callback=?", function(data) {
					data = data.replace('?','').replace('(','').replace(')','');
					data = $.parseJSON(data);
					//console.log(data);
					//console.log(data.length);
					if(data.length == 1) {
						$(opts.zip).val(data[0].name);	
					}
					else {
						$(opts.zip).val('');	
					}
				});
			},  
			//define select handler  
			change: function() {  
			}  
		}); 
		
		//$(opts.zip).autocomplete("enable");
		$(opts.zip).autocomplete({  
			//define callback to format results  
			source: function(req, add){  
				//pass request to server  
				$.getJSON("/autocomplete/US_zip.html?state_code="+opts.state_code+"&city="+$(opts.city).val()+"&callback=?", req, function(data) {  
						//create array for response objects  
						var suggestions = [];  
						//process response  
						$.each(data, function(i, val){  
						suggestions.push(val.name);  
					});  
					//pass array to callback  
					add(suggestions);  
				});  
			},  
			select: function(e, ui) {  
				//create formatted friend  
				var zip = ui.item.value; 
				$.get("/autocomplete/US_city.html?state_code="+opts.state_code+"&zip="+zip+"&callback=?", function(data) {
					data = data.replace('?','').replace('(','').replace(')','');
					data = $.parseJSON(data);
					//console.log(data);
					//console.log(data.length);
					if(data.length == 1) {
						$(opts.city).val(data[0].name);	
					}
					else {
						$(opts.city).val('');	
					}
				});
			}
		}); 
		
	}
}

function HomeLoader(){
	var user_id = store.get('user_id'),
		token = store.get('token'),
		login_data = {post_data:1,
						token:token,
						user_id:user_id
					};
	

	// check here if user is logged In
	$.post(root + "checkuser.html",login_data,function(data){
		var _data = $.parseJSON(data);
		if( _data.result){
			store.set('display_name', _data.display_name);
			store.set('user_id', _data.user_id);
		}
		else {
			store.remove('token');
			store.remove('user_id');
			store.remove('display_name');
			store.remove('remember');
			$('.sign-join-ontop').removeClass('hide');
			$('#wrapper').removeClass('hide');
			$('#loadbox-wrapper').addClass('hide');
		}


		$('#wrapper').html('');
		$('#loadbox-wrapper').removeClass('hide');
		var form_data = {};
		form_data.token = token;
		form_data.user_id = user_id;
		form_data.isAjax = true;
		form_data.isWrapper = true;
		$.post(location.href, form_data,function(response){
			$('#wrapper').html(response);
			$('.out-header').addClass('hide');
			$('#loadbox-wrapper').addClass('hide');
			$('#wrapper').removeClass('hide');
			$('#loadbox-wrapper').addClass('hide');
		});
	});	
}

var t = this;
var defdataopts, scrollopts, dataopts;

function init_scroll_opts(){
	if (!dataopts) dataopts = {};
	if (!scrollopts) scrollopts = {};
	defdataopts = {	user_id: store.get('user_id'),
		token: store.get('token'),
		isAjax: true,
		body_wrapper: false,
		subcontent_wrapper: false
		};
}

function load_scroll_opts(vartouse){
	dataopts[vartouse] = {};
	scrollopts[vartouse] = {
		page: 1,
		url: ''
	};
	reset_scroll_data_opts(vartouse);
}

function reset_scroll_page(vartouse){
	scrollopts[vartouse].page = 1;
}

function reset_scroll_data_opts(vartouse){
	dataopts[vartouse] = defdataopts;
}

function extend_scroll_opts(dat,vartouse){
	$.extend(dataopts[vartouse], dat, true);
}

function infinite_scroll(scrolltarget, vstorage, params, urlrequest, whereinsert, iswindow){
	init_scroll_opts();
	load_scroll_opts(vstorage);
	scrollopts[vstorage].url = urlrequest;
	scrollopts[vstorage].iscontent_loaded = false;
	var hascontent = true;
	var iscontent_loaded = false;
	extend_scroll_opts(params,vstorage);
	var loadMoreContent = function() {
		var url;
		scrollopts[vstorage].page++;
		if(scrollopts[vstorage].url.indexOf("?") == -1) {
			url = scrollopts[vstorage].url+'?page='+scrollopts[vstorage].page;
		}
		else {
			url = scrollopts[vstorage].url+'&page='+scrollopts[vstorage].page
		}
		
		SSAjax({
			data: dataopts[vstorage],
			url: url,
			success: function(response){
				scrollopts[vstorage].iscontent_loaded = false;
				if ($.trim(response) != '') {
				  	whereinsert.append(response);
				}else{
					hascontent = false;
					whereinsert.parent().find('.shopstyxloader.foot .inmate').addClass('done');
				}	
				
			}
		});
	}

	var trigger_scroll = function(){
		if (hascontent && !scrollopts[vstorage].iscontent_loaded){
			whereinsert.parent().find('.infinite-scroll-loader').removeClass('hide');
			var stargheight = (iswindow)?$(document).height():$(scrolltarget)[0].scrollHeight;
	  		if (scrolltarget.scrollTop() + 300  >= stargheight - scrolltarget.height()) {
	  			//console.log('toload '+page);
	  			scrollopts[vstorage].iscontent_loaded = true;
	    		loadMoreContent();
	    		
	  		}
	  	}
	}
	
	scrolltarget.scroll(trigger_scroll);
}

function trending_search(){
	var form_data = {};
	form_data.user_id = store.get('user_id');
	form_data.token = store.get('token');
	form_data.isAjax = true;
	form_data._Ssfp_ID = store.get('_Ssfp_ID');
	
	$.ajax({
		url: root + 'stick_products?action=trending_search',
		type:'post',
		data: form_data,
		success: function(data){
			var _data = $.parseJSON(data);
			var limit = 11, start = 0;
			$('.trend-search-wrapper').css({'display':'block'});
			if(_data.recent_key_counter == 0 && _data.trending_prod_counter == 0){
				$('.trend-search-wrapper').css({'display':'none'});
			}
			$.each(_data.keywords, function(i,item){
				$('.r-search-txt').append('<li><a class="color1" href="#">'+item+'</a></li>');
			});
			
			if(_data.recent_key_counter > 0){ limit =  5; }
			if(_data.recent_key_counter == 0){ $('.recent-search').addClass('hide'); }
			
			$.each(_data.trending_prod, function(i,item){
				if(start <= limit){
					$('.trend-keys').append('<li class="small-3 large-2"> \
												<span class="trend-img" style="background-image: url(\''+ item.image +'\')"></span> \
												<span class="trend-title-box"> \
													<label class="color1">'+ item.keyword +'</label> \
												</span> \
											</li>');
				}
				start++;
			});
		}
	});
}

function add_top_shopstyx(){
	//var $this = $(this),
	var	error = 0;
	
	/*$('.stick-product-name').removeClass('error');
	$('.stick-product-price').removeClass('error');
	$('.image-lbl').removeClass('data-required ');
	$('.color-lbl').removeClass('data-required ');
	$('.product-for').removeClass('data-required ');
	if($('.stick-product-name').val() == ''){
		$('.stick-product-name').addClass('error');
		error++;
	}
	
	var price = (/^[0-9,]{1,}(\.)?([0-9]{1,})?$/);
	if (!price.test($.trim($('.stick-product-price').val()))) {
		$('.stick-product-price').addClass('error');
		error++;
	}*/
	
	// if($.trim($('.stick-product-price').val()) == ""){
		// $('.stick-product-price').addClass('error');
		// error++;
	// }
	
	var page = root + 'stick_products?action=url_step2',
		form_data = {};
	form_data.product_site_name = $('.stick-product-site_name').val();
	form_data.external_product_url = $('.stick-product-url').val();
	form_data.external_product_canonical_url = $('.stick-product-canonical-url').val();
	form_data.external_tags = $('.stick-product-tags').val();
	form_data.url_exist = $('.stick-product-url-exist').val();
	form_data.product_name = $('.stick-product-name').val();
	form_data.product_description = $.trim($('.stick-product-description').val());
	form_data.price = $('.stick-product-price').val();
	form_data.currency_symbol = "$";
	form_data.currency = "USD";
	form_data.image = $('#imgarea div.columns a input').val();
	form_data.product_for = ( $.trim($('#sform .depts li.selected').attr('data')) != "")? $('#sform .depts li.selected').attr('data') : $('#sform .depts li.selected').attr('data');
	form_data.product_for_name = ( $.trim($('#sform .depts li.selected').html()) != "")? $('#sform .depts li.selected').html() : $('#sform .depts li.selected').html();
	form_data.color = $('#sform .color div.columns span.selected').attr('data-color');
	form_data.external_tags = $('.stick-product-tags').val();
	form_data.product_keyword = $('.get-search-text').val();
	form_data.isAjax = true;
	
	
	/*if( form_data.image == null ){
		$('.image-lbl').addClass('data-required ');
		error++;
	}
	
	if( form_data.color == null ){
		$('.color-lbl').addClass('data-required ');
		error++;
	}
	
	if( form_data.product_for == null || form_data.product_for == "" ){
		$('.product-for').addClass('data-required ');
		error++;
	}
	if(error){ return false; }
	
	$('.stick-product-modal .stick-loader').removeClass('hide');
	$('.stick-product-modal3 .stick-loader').removeClass('hide');
	$('.bg-trans').removeClass('hide');
	$('.add-search-modal .stick-loader').removeClass('hide');*/
	$('#miniload').foundation('reveal','open');
	console.log(form_data);
	SSAjax({
		data: form_data,
		url: page,
		success: function(response){
			$('#miniload').foundation('reveal','close');
			$('.add-collection-box.newprodform').addClass('hide');
			$('#starterModal .addtocoll').removeClass('hide').html(response);
		}
	});
}


function popUp(URL, data) {
	var day = new Date();
	var id = day.getTime();
	data = "_social_sharing_";
	eval("page" + id + " = window.open(URL, '" + id + "', 'directories=0,toolbar=0,scrollbars=0,location=0,statusbar=0,menubar=0,resizable=0,width=1000,height=430,left=200,top=50');");
}

/*
 *
 */
function promote_product(data) {
	var opts = {type:'fb',
				url:'',
				text:'',
				desc:'',
				image:'',
				current_url:''
				};
	data = (typeof data == "undefined" || $.isEmptyObject(data)) ? {} : data;
	opts = $.extend(opts,data);
	if($.isEmptyObject(opts.desc) || opts.desc == "") {
		opts.desc = opts.text;
	}

	switch(opts.type) {
		default:
		case 'fb':
			url = 'https://www.facebook.com/dialog/feed?app_id=142470092447374&link='+opts.url+'&picture='+opts.image+'&name='+opts.text+'&caption='+opts.text+'&description='+opts.desc+'&redirect_uri='+opts.current_url;
			break;
		case 'tw':
			url = 'http://twitter.com/share?count=false&original_referer='+opts.current_url+'&via=shopstyx.com&text='+opts.text+'&url='+opts.url;
			break;
		case 'gplus':
			url = 'https://plus.google.com/share?url='+opts.current_url;
			break;
		case 'linkedin':
			url = 'http://www.linkedin.com/shareArticle?mini=true&url='+opts.url+'&title='+opts.text+'&summary='+opts.desc+'&source=shopstyx.com';
			break;
		case 'pt':
			url = 'http://pinterest.com/pin/create/button/?url='+opts.url+'&media='+opts.image+'&description='+opts.desc;
			break;
	}

	location.href = "/go?r="+escape(url)+'&u='+opts.uid+'&pu='+opts.pu+'&s='+opts.s+'&c='+opts.c+'&p='+opts.p;
}

function check_allowed_image_filetype(file_mime,file_ext) {
	file_ext = file_ext.toLowerCase();
	if($.inArray(file_mime,["images/jpeg","images/jpg","images/png","images/gif"]) == -1 &&
		$.inArray(file_mime,["jpeg","jpg","png","gif"]) == -1
		) {
		return false;
	}

	return true;
}

function _support_html5_uploadform(form, inputfile) {
	var files = false;
	if (inputfile.length > 0 && inputfile[0].files) {
		files = inputfile[0].files;
	} 

	if (window.FormData && files && files.length > 0) {
		var form_data = new FormData();
		$.each(files, function(key, value) {
			form_data.append('userfile', value);
		});

		return form_data;
	}

	return false;
}
$(document).ready(function() {
	var popshowed;
	var hidebpops = function(){
		if (popshowed){
	    	popshowed.hide();
	    	popshowed = false;
	    }
	}
	$(document).click(hidebpops);

	function showthepop(el){
		if (popshowed) popshowed.hide();
		popshowed = el;
		el.show();
	}

	function filterAndCall(){
		var form_data = {'filter':1};
		if($.trim($('.input-search_text').val()) != ""){
			form_data.search_text = $.trim($('.input-search_text').val());
		}
		form_data.price_from = $('#pricepop .choosecont span.selected').attr('data-from');
		form_data.price_to = $('#pricepop .choosecont span.selected').attr('data-to');
		form_data.department_id = $('#deptpop .choosecont li.selected').attr('data');
		form_data.color = $('#colpop .choosecont span.selected').attr('data-color');
		if($('#colpop').hasClass('log-color')){
			form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
			form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
			form_data.isAjax = true;
		}else{
			form_data.isWrapper = true;
		}
		$('#mload').show();
		$.post(root, form_data,function(response){
			reset_scroll_page('forwin');
			$('#mload').hide();
			extend_scroll_opts(form_data,'forwin');
			$('#wrapper').html(response);
			//$('#loadbox-wrapper').addClass('hide');
			//$('#wrapper').removeClass('hide');
			//$('#loadbox-wrapper').addClass('hide');
			//removeBG();
		});
	}

	$(document).on('touchstart click','#profilebtn', function(e) {
		e.preventDefault(); e.stopImmediatePropagation();
		showthepop($('#optpop'));
	    return false;
	});

	$(document).on('touchstart click','#colbtn', function(e) {
		showthepop($('#colpop'));
	    e.stopPropagation(); 
	    return false;
	});

	$(document).on('touchstart click','#pricebtn', function(e) {
		showthepop($('#pricepop'));
	    e.stopPropagation();
	    return false;
	});

	$(document).on('touchstart click','#deptbtn', function(e) {
		showthepop($('#deptpop'));
	    e.stopPropagation();
	    return false;
	});

	$(document).on('touchstart click','#colchoosebtn', function(e) {
		showthepop($('#colchoosepop'));
	    e.stopPropagation(); 
	    return false;
	});

	$(document).on('touchstart click','#deptchoosebtn', function(e) {
		showthepop($('#deptchoosepop'));
	    e.stopPropagation();
	    return false;
	});


	$(document).on('touchstart click', '#colpop .choosecont span', function(e){
		e.stopPropagation();
		$('#colpop .choosecont span').removeClass('selected');
		$(this).addClass('selected');
		filterAndCall();
		return false;
	});

	$(document).on('touchstart click', '#pricepop .choosecont span', function(e){
		e.stopPropagation();
		$('#pricepop .choosecont span').removeClass('selected');
		$(this).addClass('selected');
		filterAndCall();
		return false;
	});

	$(document).on('touchstart click', '#deptpop .choosecont li', function(e){
		e.stopPropagation();
		$('#deptpop .choosecont li').removeClass('selected');
		$(this).addClass('selected');
		filterAndCall();
		return false;
	});

	$(document).on('touchstart click', '#colchoosepop .choosecont span', function(e){
		e.stopPropagation();
		$('#colchoosepop .choosecont span').removeClass('selected');
		$(this).addClass('selected');
		$('#colchoosebtn').removeClass('asset')
		.css('background-color',$(this).attr('data-color'));
		hidebpops();
		return false;
	});

	$(document).on('touchstart click', '#deptchoosepop .choosecont li', function(e){
		e.stopPropagation();
		$('#deptchoosepop .choosecont li').removeClass('selected');
		$(this).addClass('selected');
		$('#deptchoosebtn').html($('#deptchoosepop .choosecont li.selected').text());
		hidebpops();
		return false;
	});
});