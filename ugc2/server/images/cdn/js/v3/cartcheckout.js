// Details Page (add to cart)
//==============================================================
$(document).on('change','.pdage-addtocart .select-box select', function() {
	var parent_container = $(this).parents('.select-box');
	var variants_values = $.parseJSON($('#unique_variants').attr('variants_values'));
	var unique_variants = $.parseJSON($('#unique_variants').val());
	var index = parseInt(parent_container.find('select').index(this));
	var value = $(this).val();
	var next_option = (index + 1);

	if(index == 0) {
		parent_container.find('select').eq(1).val('');
		parent_container.find('select').eq(2).val('');
		parent_container.find('select').eq(1).attr('disabled','disabled');	
		parent_container.find('select').eq(2).attr('disabled','disabled');	
		
		//$(this).parent().removeClass('disabled');
		//$(this).removeAttr('disabled');
	}

	if(typeof parent_container.find('select').eq(next_option) != "undefined" && value != "") {
		parent_container.find('select').eq(next_option).find('option').hide();
		parent_container.find('select').eq(next_option).find('option').eq(0).show();
		parent_container.find('select').eq(next_option).removeAttr('disabled');
		parent_container.find('select').eq(next_option).parent().removeClass('disabled');
		parent_container.find('select').eq(next_option).val('');

		for(i = 0; i < variants_values.length; i++) {
			if(variants_values[i][index] == value) {
				parent_container.find('select').eq(next_option).find('option[value='+variants_values[i][next_option]+']').show();	
			}
		}
		
	}
	else {
		if(index > 0 && value == '') {
			parent_container.find('select').eq(next_option).attr('disabled','disabled');
			parent_container.find('select').eq(next_option).find('option').hide();
			parent_container.find('select').eq(next_option).val('');
			parent_container.find('select').eq(next_option).find('option').eq(0).show();
		}
	}
});

$(document).on("click",".add-to-cart-btn", function(e) {
	e.preventDefault(); e.stopImmediatePropagation();

	if(typeof CS.addtocartaction != "undefined" && CS.addtocartaction == true) {
		return;
	}
	var el = $(this);

	var qty = parseInt($('#quantity').val());
	if( el.hasClass('p-listing') ){
		qty = 1;
	}
	var data = $.parseJSON(el.attr('data'));
	var product_url = el.attr('product-url');
	var data = {product_user_id:data.p_uid,
				product_id:data.p_id, 
				product_store_id: data.p_sid,
				//product_store_url: CS.siteURL,
				product_store_url:product_url,
				store_id:data.store_id,
				store_user_id:data.store_uid ,
				quantity: parseInt(qty),
				has_options: data.has_options,
				shipto_country: CS.shipto_country,
				endorser_id:data.endorser_id
				};
	if($(this).hasClass('seemore-cart-btn') || $(this).parents('.product-details-view').length > 0) {
		addtocart(data,'popupdetails',$(this));
	}
	else {
		addtocart(data,"",$(this));	
	}
});

// Cart Page
//==============================================================
$(document).on("click",".checkout-wrapper.cart .notloggedin span", function(e) {
	e.preventDefault(); e.stopImmediatePropagation();
	signin_modal();
});
$(document).on("click",".customized-select", function(e) {
	e.preventDefault(); e.stopImmediatePropagation();
	$(this).find('ul').show();

	if($(this).hasClass('shiptocountry')) {
		$('.customized-select.shiptocountry ul').scrollTop(0);
		$('.customized-select.shiptocountry ul').scrollTop($('.customized-select.shiptocountry ul li.selected').position().top);
	}
});

$(document).on("click",".customized-select ul li", function(e) {
	e.preventDefault(); e.stopImmediatePropagation();
	$(this).parent().hide();
	var val = $(this).attr("value"),
		text = $(this).html(),
		data = {};
	$(this).addClass("selected");
	$(this).parent().parent().attr("value",val).find("em strong").html(text);

	if($(this).parent().parent().hasClass('shiptocountry')) {
		if(typeof CS.iswebstore == "undefined") {
			data.uid = store.get('user_id');
			data.token = store.get('token');
		}
		else {
			data.uid = $.cookies.get("uid");
			data.token = $.cookies.get("token");	
		}

		data.shipto_country = val;

		$.ajax({
			type: "POST",
			url: CS.main_url + 'global_content/update_shipto_country',
			data: data,
			cache: false,
			success: function(res){
				location.href = '/cart';
			}
		});

		//$.cookies.set("shipto_country",val,400);
	}
});

$(document).on("change",".shipwhere select", function(e) {
	e.preventDefault(); e.stopImmediatePropagation();
	var val = $(this).val(),
		text = $(this).html(),
		data = {};

	//store.set("shipto_country",val,400);

	if(typeof CS.iswebstore == "undefined") {
		data.uid = store.get('user_id');
		data.token = store.get('token');
	}
	else {
		data.uid = $.cookies.get("uid");
		data.token = $.cookies.get("token");	
	}

	data.shipto_country = val;

	$.ajax({
		type: "POST",
		url: CS.main_url + 'global_content/update_shipto_country',
		data: data,
		cache: false,
		success: function(res){
			location.href = '/cart';
		}
	});
});

$(document).on("mouseover",".customized-select ul", function(e) {
	e.preventDefault(); e.stopImmediatePropagation();
	$(this).show();
});

$(document).on("blur",".customized-select", function() {
	$(this).find('ul').hide();
});

$(document).on("click",".customized-select.shiptocountry ul li", function(e) {
	e.preventDefault(); e.stopImmediatePropagation();
	var val = $(this).attr("value"),
		text = $(this).html();
});

$(document).on("click",".link-aaq, .q-chat-btn", function(e) {
	e.preventDefault(); e.stopImmediatePropagation();
	var data;

	if($(this).hasClass('link-aaq')) {
		data = $.parseJSON($(this).parent().attr('data'));
	}
	else {
		data = $.parseJSON($(this).attr('data'));
	}
	
	if(typeof CS.iswebstore == "undefined") {
		data.uid = store.get('user_id');
		data.token = store.get('token');
	}
	else {
		data.uid = $.cookies.get("uid");
		data.token = $.cookies.get("token");	
	}
	// data.uid = store.get('user_id');
	// data.token = store.get('token');

	$('#cartpops').foundation('reveal', 'open', {
	    url: CS.main_url + 'global_content/ask_question',
	    type: 'post',
	    data: data,
	    success: function(reponse) {

	    },
	    error: function() {
	        
	    }
	});
});

$(document).on("change",".cartpops .popitem .popcont table td .prod select", function(e) {
	e.preventDefault(); e.stopImmediatePropagation();
	var data = $.parseJSON($(this).find("option:selected").attr('data'));
	var el = $('.cartpops .popitem .popcont table td .prod');
	el.find('.img').css('background-image',"url('"+data.thumb+"')");
	el.find('.prodname').html(data.name);
	el.find('.price').html('$ ' + data.actual_price);
	el.find('.options').html(data.options);
	
});

$(document).on("submit",".cartpops .popitem.askquestion", function(e) {
	e.preventDefault(); e.stopImmediatePropagation();
	var el = $('.cartpops .popitem .popcont table td .prod select'),
		product_id = el.val(),
		variant_id = el.find("option:selected").attr('variant_id'),
		merchant_id = el.find("option:selected").attr('merchant_id'),
		merchant_store_id = el.find("option:selected").attr('store_id'),
		claimed_by = el.find("option:selected").attr('claimed_by'),
		msg = $(this).find('textarea').val(),
		data = {};

	if(typeof CS.iswebstore == "undefined") {
		data.uid = store.get('user_id');
		data.token = store.get('token');
	}
	else {
		data.uid = $.cookies.get("uid");
		data.token = $.cookies.get("token");	
	}
	// data.uid = store.get('user_id');
	// data.token = store.get('token');

	data.product_id = product_id;
	data.varaint_id = variant_id;
	data.merchant_id = merchant_id;
	data.merchant_store_id = merchant_store_id;
	data.claimed_by = claimed_by;
	data.message = msg;

	$.ajax({
		type: "POST",
	    url: CS.main_url + 'global_content/ask_question?send_inquiry=1',
	    type: 'post',
	    data: data,
	    success: function(reponse) {
	    	$('#cartpops').foundation('reveal', 'close');
	    },
	    error: function() {
	        
	    }
	});
});

$(document).on("click",".cartpops .popitem.askquestion button.cw", function(e) {
	e.preventDefault(); e.stopImmediatePropagation();
	$('#cartpops').foundation('reveal', 'close')
});

$(document).on("click",".link-rrp", function(e) {
	e.preventDefault(); e.stopImmediatePropagation();
	//$('body').css('overflow','hidden');
	//$('#main-wrapper').addClass('hide');
	var data = $.parseJSON($(this).parent().attr('data'));
	$('#cartpops').foundation('reveal', 'open', {
	    url: CS.main_url + 'global_content/return_policy',
	    type: 'post',
	    data: data,
	    success: function(reponse) {
	    	
	    },
	    error: function() {
	        
	    }
	});
});

$(document).on("click",'.checkout-wrapper.cart .merchtop div.right .remove-store', function(e) {
	e.preventDefault(); e.stopImmediatePropagation();
	var data = $.parseJSON($(this).attr('data'));
	update_cart(data,'delete_merchant_items');
});

$(document).on("change",'.checkout-wrapper.cart .mdetails .selectqty select, .cart-panel.npops .top-wrapper ul.citems li .cinfo .qtypr select', function() {
	var data = $.parseJSON($(this).attr('data'));
	data.quantity = $(this).val();
	data.product_options = $.parseJSON($(this).attr('options'));

	if($(this).parents('.qtypr').length > 0 && $('.checkout-wrapper.cart').length == 0) {
		update_cart(data,'new_quantity','header_cart');
	}
	else {
		update_cart(data,'new_quantity');
	}
});

$(document).on("click",'.checkout-wrapper.cart .mdetails td.ship .remove-btn', function(e) {
	e.preventDefault(); e.stopImmediatePropagation();
	var data = $.parseJSON($(this).attr('data'));
	data.product_options = $.parseJSON($(this).attr('options'));
	update_cart(data,'delete_item');
});

$(document).on("change",'select[name="service_levels"]', function() {
	var data = $.parseJSON($(this).attr('data'));
	data.product_options = $.parseJSON($(this).attr('options'));
	data.shipping_selection = $(this).val();
	update_cart(data,'update_shipping_selection');
});

$(document).on("submit", 'form#cart-form', function(e) {
	e.preventDefault(); e.stopImmediatePropagation();
	var url_referer = $(this).attr('url-referer');
	if(typeof url_referer == "undefined" || url_referer == "") {
		url_referer = "";
	}

	var $this = $( "textarea[name*='customernote']" ), viewArr = $this.serializeArray(), data = {};
	for (var i in viewArr) {
		data[viewArr[i].name] = viewArr[i].value;
	}
	update_cart_customernote(data,'update_customernote',"",url_referer);
});

$(document).on("click", '.checkout-wrapper.cart .merchbtm .checkout-this-shop', function(e) {
	e.preventDefault(); e.stopImmediatePropagation();
	var url_referer = $(this).parents('form').attr('url-referer');
	if(typeof url_referer == "undefined" || url_referer == "") {
		url_referer = "";
	}

	var chekcout_store_url = $(this).attr('store-url');
	var $this = $( "textarea[name*='customernote']" ), viewArr = $this.serializeArray(), data = {};
	for (var i in viewArr) {
		data[viewArr[i].name] = viewArr[i].value;
	}
	update_cart_customernote(data,'update_customernote',chekcout_store_url,url_referer);
});


// Checkout page
//==============================================================
$(document).on("open","#cartpops, .reveal-modal.checkout-modal",function(e) {
	$(this).centerized();
});

$(document).on("close",".reveal-modal.checkout-modal",function() {
	$('.reveal-modal.checkout-modal form').removeAttr('address_id');
	$('.reveal-modal.checkout-modal form .error-content').addClass('hide');
	$('.reveal-modal.checkout-modal .error-content .wscolor2').removeClass('hide');

	$('.reveal-modal.checkout-modal form input').removeClass('error-field').val('');
	$('.reveal-modal.checkout-modal form .mystyled-select').removeClass('error-field');

	$('.reveal-modal.checkout-modal form').addClass('hide');
	$('.reveal-modal.checkout-modal').removeClass('preloader');
	$('.reveal-modal.checkout-modal form.listing-form').html('');
});

$(document).on("closed","#cartpops, .reveal-modal.checkout-modal",function(e) {
	$('.sa-header-label').html('Enter a new shipping address');
	if($('.reveal-modal.checkout-modal').length > 1) {
		$('.reveal-modal.checkout-modal').eq(0).remove();
	}
});

$(document).on("click",".checkout-wrapper .tooltip-icon", function(e) {
	e.preventDefault(); e.stopImmediatePropagation();
});

$(document).on("change",'select.country-code-select', function() {
	if($('form input[name="shipping_state_name"]').length > 0) {
		var sname = $('form input[name="shipping_state_name"]'),
			scode = $('form select[name="shipping_state_code"]');
	}
	else {
		var sname = $('form input[name="billing_state_name"]'),
			scode = $('form select[name="billing_state_code"]');
	}	

	if($(this).val() == "US") {
		sname.addClass('hide');
		scode.parent().removeClass('hide');
	}
	else {
		scode.parent().addClass('hide');
		sname.removeClass('hide');
	}
});

$(document).on("click",'#checkout-main-content #change-address-btn', function(e) {
	e.preventDefault(); e.stopImmediatePropagation();
	var type = $(this).attr('type');
	checkout_getaddress_html(type);
});

/*
$(document).on("click",'#checkout-main-content #change-shipping-default-country', function(e) {
	e.preventDefault(); e.stopImmediatePropagation();
	checkout_changedefault_shipping_country();
});
*/

$(document).on("submit",'.reveal-modal.checkout-modal form', function(e) {
	e.preventDefault(); e.stopImmediatePropagation();

	if($(this).hasClass('shipping-form')) {
		checkout_shiptoaddress($(this),'shipping');
	}
	else if($(this).hasClass('billing-form')) {
		checkout_shiptoaddress($(this),'billing');
	}
});

// Editing Shipping
$(document).on("click",'.reveal-modal.checkout-modal .editdelete-wrapper .edit-btn', function(e) {
	e.preventDefault(); e.stopImmediatePropagation();
	var address_id = $(this).parents('li').attr('address_id');
	var type;
	/*
	if($(this).parents('form').hasClass('shipping-form')) {
		type = "shipping";
		$('.reveal-modal.checkout-modal .sa-header-label').html('Edit shipping address');
	}
	else {
		type = "billing";
	}
	*/

	$('.reveal-modal.checkout-modal .sa-header-label').html('Edit shipping address');

	$('form.shipping-form button[type="submit"]').html('Update');
	checkout_getaddress(address_id,'shipping');
});
// Delete Shipping
$(document).on("click",'.reveal-modal.checkout-modal .editdelete-wrapper .delete-btn', function(e) {
	e.preventDefault(); e.stopImmediatePropagation();
	var address_id = $(this).parents('li').attr('address_id');
	checkout_deleteaddress(address_id,'shipping',true);
});
// select address to ship/pay
$(document).on("click",'.reveal-modal.checkout-modal .listing ul li button.selected-address', function(e) {
	e.preventDefault(); e.stopImmediatePropagation();
	var li = $(this).parents('li');
	var payment_type = li.attr('payment_type');
	var address_id = li.attr('address_id');
	var next = "billing";

	if($(this).parents('.listing').hasClass('shipping-listing')) {
		type = "shipping";
	}
	else {
		type = "billing";
	}

	checkout_steps(type,address_id,payment_type);
});

$(document).on("click",'.addnew-shipping', function(e) {
	e.preventDefault(); e.stopImmediatePropagation();
	add_shipping_modal();
});

$(document).on("click",'.addnew-billing', function(e) {
	e.preventDefault(); e.stopImmediatePropagation();
	checkout_getaddress_html('billing');
});

$(document).on("click",'.reveal-modal.checkout-modal .btn-cancel-form', function(e) {
	e.preventDefault(); e.stopImmediatePropagation();
	var type;
	if($(this).parents('form').hasClass('shipping-form')) {
		type = "shipping-form";

		var set_country_code = $(this).parents('form').attr('set_country_code');
		if(typeof set_country_code != "undefined" && set_country_code != "") {
			$(this).parents('form').find('select#shipping_country_code').val(set_country_code);
			$('.reveal-modal.checkout-modal form.billing-form').find('select#billing_country_code').val(set_country_code);

			$(this).parents('form').removeAttr('set_country_code');
			$('.reveal-modal.checkout-modal').addClass('preloader');

			checkout_changedefault_shipping_country();
			return;
		}
	}
	else {
		type = "billing-form";
	}
	var form = $('.reveal-modal.checkout-modal form.'+type);
	var data = form.serialize();

	if(data != form.data('formdata')) {
		$('.reveal-modal.checkout-modal').foundation('reveal', 'close');
	}
	else {
		$('.reveal-modal.checkout-modal').foundation('reveal', 'close');
	}

});

$(document).on("click",'.addnew-cc', function(e) {
	e.preventDefault(); e.stopImmediatePropagation();
	$('.reveal-modal.checkout-modal form').addClass('hide');
	$('.reveal-modal.checkout-modal form.billing-form').data('formdata',$('.reveal-modal.checkout-modal form.billing-form').serialize()).removeClass('hide');
	$('.reveal-modal.checkout-modal').foundation('reveal', 'open', { close_on_background_click: false});

	toggle_autocomplete_cityzip({
		elements: {
			country: '#billing_country_code',
			us_state: '[name="billing_state_code"]',
			state: '[name="billing_state_name"]',
			city: '[name="billing_city"]',
			zip: '[name="billing_zip"]'
		}
	});	
});


$(document).on("click",".reveal-modal.checkout-modal #same-as-shipping", function(e) {
	e.stopImmediatePropagation();

	var is_checked = $(this).prop("checked"),
		fields_wrapper  = $('.reveal-modal.checkout-modal .action-wrapper .diffaddr'),
		btn = $(this).parent().parent().find('.usethiscard-wrapper'),
		btn2 = $(this).parents('.tbl').find('.tbl-cell.billing-col2 .usethiscard-wrapper'),
		is_firstime = $(this).hasClass('firstime'),
		address_text = $(this).parent().find('div');
	
	if(is_checked) {
		//fields_wrapper.hide();
		$('.reveal-modal.checkout-modal .action-wrapper label.same-as-shipping.shownaddr div').show();
		fields_wrapper.slideUp( "fast", function() {
		});
		/*
		if(is_firstime) {
			fields_wrapper.hide('slide', {direction: 'left'}, 400);

			btn.removeClass('hide');
			btn2.addClass('hide');
		}
		else {
			$('#bcrc .tbl-cell.formfields .billing-form-holder .tbl-cell.billing-col2').addClass('hide');
			$('#bcrc .tbl-cell.formfields .billing-form-holder .tbl-cell.billing-col1').addClass('paddingLeft33');

			$('#bcrc .tbl-cell.billing-col1 .usethiscard-wrapper').removeClass('hide');
			$('#bcrc .tbl-cell.billing-col2 .usethiscard-wrapper').addClass('hide');


			$('.billing-form-inner').animate({
		          left: '299px'
		      },400, function() {
		      	$('#bcrc').removeClass('different-address');
		      });
		}
		address_text.removeClass('hide');
		*/
	}
	else {
		//fields_wrapper.show();
		//fields_wrapper.show('slide', {direction: 'top'}, 400);
		$('.reveal-modal.checkout-modal .action-wrapper label.same-as-shipping.shownaddr div').hide();
		fields_wrapper.slideDown( "slow", function() {
		});
		/*
		if(is_firstime) {
			fields_wrapper.show('slide', {direction: 'left'}, 400);
			btn.addClass('hide');
			btn2.removeClass('hide');
		}
		else {
			$('#bcrc').addClass('different-address');
			$('#bcrc .tbl-cell.formfields .billing-form-holder .tbl-cell.billing-col2').removeClass('hide');
			$('#bcrc .tbl-cell.formfields .billing-form-holder .tbl-cell.billing-col1').removeClass('paddingLeft33');

			$('#bcrc .tbl-cell.billing-col1 .usethiscard-wrapper').addClass('hide');
			$('#bcrc .tbl-cell.billing-col2 .usethiscard-wrapper').removeClass('hide');

			$('.billing-form-inner').animate({
		          left: '0'
		      },400, function() {
		      	
		      });
		}
		
		address_text.addClass('hide');
		*/
	}
});

$(document).on("click",'.reveal-modal.checkout-modal .billing-content .listing ul li div.bns .delete-btn', function(e) {
	e.preventDefault(); e.stopImmediatePropagation();
	$(this).parents('li').addClass('todel').find('.conf').removeClass('hide');
});

$(document).on("click",'.reveal-modal.checkout-modal .billing-content .listing ul li.todel .conf .confdelete-btn', function(e) {
	e.preventDefault(); e.stopImmediatePropagation();
	var address_id = $(this).parents('li').attr('address_id');
	checkout_toggle_loader();
	checkout_deleteaddress(address_id,'billing',true);
});

$(document).on("click",'.reveal-modal.checkout-modal .billing-content .listing ul li.todel .conf .canceldelete-btn', function(e) {
	e.preventDefault(); e.stopImmediatePropagation();

	$(this).parents('li').removeClass('todel').find('.conf').addClass('hide');
});

$(document).on("click",'#change-billing-btn', function(e) {
	e.preventDefault(); e.stopImmediatePropagation();
	checkout_load_content('billing');

});
$(document).on("click",'#change-shipping-btn', function(e) {
	e.preventDefault(); e.stopImmediatePropagation();
	checkout_load_content('shipping');
});

$(document).on("click",'.checkout-placeorder-enabled', function(e) {
	e.preventDefault(); e.stopImmediatePropagation();
	sscheckout();
});

$(document).on('click', '.prodsharelist .share-common', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	var data = {type:$(this).attr('type')};
	var mdata = decodeURIComponent($(this).parents('.share-wrapper').attr('data').replace(/\+/g, ' '));
	//var mdata = encodeURIComponent();
	//console.log(mdata);
	mdata = $.parseJSON(mdata);
	data = $.extend(data,mdata);
	//console.log(data)
	promote_product(data);
});

$(document).on('click', '.prodsharelist .share-mail', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	var data = {type:'mail'};
	var mdata = decodeURIComponent($(this).parents('.share-wrapper').attr('data').replace(/\+/g, ' '));
	mdata = $.parseJSON(mdata);
	data = $.extend(data,mdata);
	//console.log(data)
	promote_product(data);
});

$(document).on('click', '.donemsg .blue-btn', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	load_wallet_purchase_history();
});

$(document).on("mouseenter",".add-to-cart-btn-icon", function(e) {
	e.preventDefault(); e.stopImmediatePropagation();
	var el = $(this),
		mdata = el.attr("data"),
		data = {};
	if(el.hasClass('done')){ return false; }
	if(typeof CS.iswebstore == "undefined") {
		data.uid = store.get('user_id');
		data.token = store.get('token');
	}
	else {
		data.uid = $.cookies.get("uid");
		data.token = $.cookies.get("token");		
	}
	mdata = $.parseJSON(mdata);
	data = $.extend(data,mdata);
	
	if(typeof data.uid == "undefined" || data.uid == "" || data.uid == null || typeof data.token == "undefined" || data.token == "" || data.token == null) {
		el.find('.cmd-btn-bg div').html('Buy this &amp; earn <strong>$' + data.dte + '</strong> credits');
		if(el.hasClass('pcart-btn-wrapper')){
			el.find('.buythis').html('Buy this &amp; earn <strong>$' + data.dte + '</strong> credits');
		}
		return;
	}
	
	$.ajax({
		type: "POST",
		url: CS.main_url + 'global_content/check_for_endorser',
		data: data,
		cache: false,
		success: function(res){
			el.find('.cmd-btn-bg div').html('Buy this &amp; earn <strong>$' + res + '</strong> credits');
			if(el.hasClass('pcart-btn-wrapper')){
				el.find('.buythis').html('Buy this &amp; earn <strong>$' + res + '</strong> credits');
			}
			el.addClass('done');
		}
	});
});

var user_cart_data = {};

function set_cart_data(data,cart_sess) {
	user_cart_data = data;	
}

function get_cart_data() {
	return user_cart_data;
}

function addtocart(data, pagetype, btnElement) {
	if(typeof(data) != 'object') return;
	CS.addtocartaction = true;

	var product_user_id = data.product_user_id; // product owner or merchant
	var product_id = data.product_id;
	var product_store_id = data.product_store_id;
	var store_id = data.store_id;
	var store_user_id = data.store_user_id; // endorser or store owner
	var quantity = data.quantity;
	var has_options = data.has_options;
	data.product_options = {};
	
	// get existing cart values
	var cart = get_cart_data();
	cart = (typeof(cart) != 'object' || cart == null) ? {} : cart;

	//console.log(cart.merchants[user_id].products.hasOwnProperty(1));
	//console.log(cart);
	// retreive existing 
	var item, option = '';
	var error_option = false;
	var variants_values = new Array();
	var option_wrapper_name = ".pdage-addtocart .select-box select";


	if(typeof CS.iswebstore == "undefined") {
		var btn_parent_element = btnElement.parents('.more-desc-wrapper').find(option_wrapper_name);
	}
	else {
		var btn_parent_element = $(option_wrapper_name).not('.country-code');
	}
	
	/*
	for (var key in cart) {
		if (cart.hasOwnProperty(key)) {
			total_merchant++;
			new_cart[key] = cart[key]
		}
	}
	*/
	// Option Validations
	btn_parent_element.each(function() {
		if($(this).val() == '') {
			error_option = true;	
		}
		
		data.product_options[$(this).attr('option_id')] = $(this).val();
		variants_values.push($(this).val());
		
	});

	var options_string = variants_values.join(',');
	if( !btnElement.hasClass('p-listing') ){
		var variant_id = 0;
	}else{
		var variants = $.parseJSON($('#unique_variants').attr('variants'));
		var variant_id = 0;
		for (var key in variants) {
			if (variants.hasOwnProperty(key)) {
				console.log(variants[key] + ' == ' + options_string);
				if(variants[key] == options_string) {
					variant_id = key;	
				}
			}
		}
	}
	console.log(variant_id);
	if(error_option || (has_options > 0 && variant_id == 0)) {
		CS.addtocartaction = false;
		alert('Please select an option.');
		return;	
	}
	data.variant_id = variant_id;
	data.cart = cart;

	if(typeof CS.iswebstore == "undefined") {
		data.uid = store.get('user_id');
		data.token = store.get('token');
	}
	else {
		data.uid = $.cookies.get("uid");
		data.token = $.cookies.get("token");		
	}

	if(typeof data.uid == "undefined" || data.uid == "" || data.uid == null || typeof data.token == "undefined" || data.token == "" || data.token == null) {
		CS.addtocartaction = false;
		signin_modal();
		return;
	}
	//data.cart_sess = get_cart_sess();

	//console.log(data); return;
	if(typeof load_header_cart != "undefined") {
		load_header_cart('loading');
	}
	load_popup_cart(btnElement);
	$.ajax({
		type: "POST",
		url: CS.main_url + 'global_content/addtocart',
		data: data,
		cache: false,
		success: function(res){
			CS.addtocartaction = false;
			load_header_cart('adding');
			load_popup_cart_data(data);
			if(typeof get_cart_total_items != "undefined") {
				get_cart_total_items();
			}
				
			/*
			//var data = $.parseJSON(res);
			//location.href = "/cart";
			if(pagetype == "popupdetails" || typeof load_header_cart == "undefined") {
				location.href = "/cart";
			}
			else if(typeof load_header_cart != "undefined") {
				load_header_cart('adding');
				load_popup_cart();
				if(typeof get_cart_total_items != "undefined") {
					get_cart_total_items();
				}
			}
			

			set_cart_data(data.cart);
			set_cart_sess(data.cart_sess);

			//console.log(data); return;
			var form_data = {login_user_id:$.cookies.get("uid"),
								token:$.cookies.get("token"),
								action:'update_cart',
								cart_sess:data.cart_sess,
								cart:data.cart};
			

			if(parentIsIframe) {
				form_data = $.extend(form_data,{'parent_url_load':'/cart.html'});
				windowProxy1.post(form_data);
				//location.href = '/cart.html';
			}
			else {
				form_data = $.extend(form_data,{'parent_url_load':'/cart.html?frame=1'});
				windowProxy2.post(form_data);
			}
			*/
		},
		error: function(jqXHR, textStatus, errorThrown ) {
			load_header_cart();
		}
	});
}

function load_popup_cart(el) {
	$('#addToCartPopup').removeClass('hide');
	$('.global-loading-prod-img').css('backgroundImage','url(' + el.attr('data-image') + ')');
	$('.global-loading-prod-title').html(stripslashes(el.attr('data-title')));
	$('.global-loading-prod-price').html(el.attr('data-price'));
	$('.global-cart-loadbox').removeClass('hide');
	$('addToCartPopup').centerized();
}

function update_cart(data,action,pagetype) {
	var shipto_country;
	if(typeof CS.iswebstore == "undefined") {
		data.uid = store.get('user_id');
		data.token = store.get('token');
		
		shipto_country = store.get('shipto_country');
	}
	else {
		data.uid = $.cookies.get("uid");
		data.token = $.cookies.get("token");	
		shipto_country = $.cookies.get("shipto_country");	
	}

	action = typeof(action) == "undefined" ? "" : "&update="+action;
	data.shipto_country = shipto_country;
	$.ajax({
		type: "POST",
		url: CS.main_url + 'global_content/addtocart?'+action,
		data: data,
		cache: false,
		success: function(res){
			//var data = $.parseJSON(res);
			if(typeof pagetype == "undefined") {
				location.href = "/cart";
			}
			else {
				load_header_cart();
			}
		}
	});
}

function update_cart_customernote(data,type,chekcout_store_url,referer) {
	// if(typeof CS.iswebstore == "undefined") {
	if(typeof CS.is_cookies == "undefined" || !CS.is_cookies) {	
		data.uid = store.get('user_id');
		data.token = store.get('token');
	}
	else {
		data.uid = $.cookies.get("uid");
		data.token = $.cookies.get("token");
	}
	console.log(referer);
	referer_param = (referer == "") ?  "" : "&referer=" + (referer);

	chekcout_store_url = (typeof(chekcout_store_url) != "undefined" && chekcout_store_url != "") ? chekcout_store_url : "";
	var webstoresrc = "";

	if(typeof CS.webstoresrc != "undefined") {
		webstoresrc = "&src="+CS.webstoresrc;
	}

	$.ajax({
		type: "POST",
		url: CS.main_url + 'global_content/update_cart_customernote',
		data: data,
		cache: false,
		success: function(res){
			var data = $.parseJSON(res);
			var params = $.trim(referer_param + webstoresrc);
			var checkout_url = main_url + "checkout";
			if(params != "") {
				checkout_url += "?";
			}


			if(chekcout_store_url == "") {
				location.href = checkout_url + params;
			}
			else {
				location.href = checkout_url + "/" + chekcout_store_url + "?" + params;
			}

			/*
			if(data.onestepcheckout) {
				if(typeof CS.webstoresrc != "undefined") {
					webstoresrc = "&src="+CS.webstoresrc;
				}

				if(chekcout_store_url == "") {
					location.href = main_url + "checkout?step=review"+webstoresrc+"&onestep" + referer_param;
				}
				else {
					location.href = main_url + "checkout/" + chekcout_store_url + "?step=review"+webstoresrc+"&onestep" + referer_param;
				}
			}
			else {
				if(typeof CS.webstoresrc != "undefined") {
					webstoresrc = "?src="+CS.webstoresrc;
				}
				if(chekcout_store_url == "") {
					location.href = main_url + "checkout"+webstoresrc + referer_param;
				}
				else {
					location.href = main_url + "checkout/" + chekcout_store_url+webstoresrc+referer_param;
				}
			}
			*/
		},
		error: function(jqXHR, textStatus, errorThrown ) {
			location.href = location.href;
		}
	});
}


// checkout functions
//
function checkout_toggle_loader(text) {
	if($('#loadbox-wrapper').hasClass('hide')) {
		$('#main-wrapper').addClass('hide');
		$('#loadbox-wrapper').removeClass('hide');
		if(text != "") {
			$('#loadbox-wrapper .shopstyxloader div').html(text);
		}
	}
	else {
		$('#main-wrapper').removeClass('hide');
		$('#loadbox-wrapper').addClass('hide');
		$('#loadbox-wrapper .shopstyxloader div').html('LOADING...');
	}
}

function get_checkout_storeurl() {
	var path = window.location.pathname.toString().substr(1).split('/');
	var url = '';
	if(path.length == 2) {
		url = '/checkout/'+path[1];
	}
	else {
		url = '/checkout';
	}

	return url;
}

function get_checkout_storeurl_param() {
	var path = window.location.pathname.toString().substr(1).split('/');
	var url = '';
	if(path.length == 2) {
		return path[1];
	}
	else {
		return '';
	}
}

function get_checkout_user_data() {
	var data = {};
	data.uid = store.get('user_id');
	data.token = store.get('token');

	/*	
	if(typeof CS.iswebstore == "undefined") {
		data.uid = store.get('user_id');
		data.token = store.get('token');
	}
	else {
		data.uid = $.cookies.get("uid");
		data.token = $.cookies.get("token");		
	}
	*/

	return data;
}

function reload_checkout_content(hideModal,callback) {
	var container = $('#checkout-main-content');
	var url = $(location).attr('href').split('#');
	var mainURL = url[0];

	$('#main-wrapper').addClass('hide');
	$('#loadbox-wrapper').removeClass('hide');

	var webstoresrc = "";
	if(typeof CS.webstoresrc != "undefined") {
		webstoresrc = "&src="+CS.webstoresrc;
	}


	SSAjax({
		type: "POST",
		//url: CS.main_url + 'checkout?loadtype=checkoutcontent',
		url: get_checkout_storeurl() + '?loadtype=checkoutcontent' + webstoresrc,
		data: {isAjax:true},
		success:function(response) {

			try {
		        var object = JSON.parse(response);

		        // Handle non-exception-throwing cases:
		        // Neither JSON.parse(false) or JSON.parse(1234) throw errors, hence the type-checking,
		        // but... JSON.parse(null) returns 'null', and typeof null === "object", 
		        // so we must check for that, too.
		        if (object && typeof object === "object" && object !== null) {
		            console.log(object);
		            switch(object.redirect_code) {
		            	case 100:
		            	case 101:
		            	case 102:
		            		location.href = object.url;
		            		break;
		            }
		        }
		    }
		    catch (e) { 
				container.html(response);

				if(typeof callback == "function") {
					var callbacks = $.Callbacks();
					callbacks.add( callback );
					callbacks.fire();
				}

				if(hideModal) {
					$('.reveal-modal.checkout-modal').foundation('reveal', 'close');
				}
				$('#main-wrapper').removeClass('hide');
				$('#loadbox-wrapper').addClass('hide');

		    }
		},
		error: function(jqXHR, textStatus, errorThrown ) {
			if(jqXHR.status == 404) {
				location.href = CS.webstore_url;
			}
		}
	});
}

function add_shipping_modal(data) {
	toggle_autocomplete_cityzip({
		elements: {
			country: '#shipping_country_code',
			us_state: '[name="shipping_state_code"]',
			state: '[name="shipping_state_name"]',
			city: '[name="shipping_city"]',
			zip: '[name="shipping_zip"]'
		}
	});

	var form = $('.reveal-modal.checkout-modal form.shipping-form');

	$('.reveal-modal.checkout-modal .error-content').addClass('hide');
	$('.reveal-modal.checkout-modal form input, .reveal-modal.checkout-modal .mystyled-select, .form-wrapper input, .form-wrapper .mystyled-select').removeClass('error-field');

	$('.reveal-modal.checkout-modal').removeClass('preloader');
	form.attr('address_id',0);

	form.find('input').removeClass('error-field').val('');
	form.find('.mystyled-select').removeClass('error-field');

	//var shipto_country;
	// if(typeof CS.iswebstore != "undefined") {
	// 	shipto_country = $.cookies.get("shipto_country");
	// }
	// else {
	// 	shipto_country = store.get('shipto_country');
	// }
	//shipto_country = store.get('shipto_country');

	if(typeof data != "undefined" && typeof data.country_code != "undefined") {
		form.attr('set_country_code', data.country_code);

		$('.reveal-modal.checkout-modal .error-content .wscolor2').html('').addClass('hide');	
		$('.reveal-modal.checkout-modal .error-content .errortxt').html('Your current items are set to be shipped to a country that is not yet added to your shipping<br>address information. Please fill in a new shipping address details.');
		$('.reveal-modal.checkout-modal .error-content').removeClass('hide');
	}


	form.data('formdata',form.serialize());
	$('form.shipping-form button[type="submit"]').html('Add');

	$('.reveal-modal.checkout-modal form').addClass('hide');
	$('.reveal-modal.checkout-modal form.shipping-form').removeClass('hide');
	$('.reveal-modal.checkout-modal').foundation('reveal', 'open', { close_on_background_click: false});
	$('.reveal-modal.checkout-modal').centerized();
}

function checkout_changedefault_shipping_country() {
	$('#main-wrapper').addClass('hide');
	$('#loadbox-wrapper').removeClass('hide');

	var data = get_checkout_user_data();
	$.ajax({
		type: "POST",
		url: CS.main_url + 'global_content/update_shipping_default_country',
		data: data,
		cache: false,
		success: function(response){
			reload_checkout_content(true);
		},
		error: function(jqXHR, textStatus, errorThrown ) {
			if(jqXHR.status == 404) {
				//location.href = "/";
			}
		}
	});
}

function add_billing_modal() {
	$('.reveal-modal.checkout-modal .error-content').addClass('hide');
	$('.reveal-modal.checkout-modal form input, .reveal-modal.checkout-modal .mystyled-select, .form-wrapper input, .form-wrapper .mystyled-select').removeClass('error-field');

	$('.reveal-modal.checkout-modal form').addClass('hide');
	$('.reveal-modal.checkout-modal form.listing-form').removeClass('hide');
	$('.reveal-modal.checkout-modal').foundation('reveal', 'open', { close_on_background_click: false});
	$('.reveal-modal.checkout-modal').addClass('preloader');
	$('.reveal-modal.checkout-modal').centerized();

	var form = $('.reveal-modal.checkout-modal form.listing-form');
	form.html('');
	data = get_checkout_user_data();
	$.ajax({
		type: "POST",
		url: CS.main_url + 'global_content/getpaymenthtml',
		data: data,
		cache: false,
		success: function(response){
			form.html(response);
			$('.reveal-modal.checkout-modal').centerized();
			$('.reveal-modal.checkout-modal').removeClass('preloader');
		},
		error: function(jqXHR, textStatus, errorThrown ) {
			if(jqXHR.status == 404) {
				//location.href = "/";
			}
		}
	});
}

function checkout_shiptoaddress(form, type) {
	var address_id = form.attr('address_id');
	var form_data = get_checkout_user_data();
	var cc_data;
	var first_error = new Array();
	var error_msg = new Array();

	$('.reveal-modal.checkout-modal .error-content').addClass('hide');
	$('.reveal-modal.checkout-modal form input, .reveal-modal.checkout-modal .mystyled-select, .form-wrapper input, .form-wrapper .mystyled-select').removeClass('error-field');
	$('.reveal-modal.checkout-modal').addClass('preloader');

	if(type == "shipping") {
		$.extend(form_data,{"shipping_firstname":form.find('[name="shipping_firstname"]').val(),
						"shipping_lastname":form.find('[name="shipping_lastname"]').val(),
						"shipping_company":form.find('[name="shipping_company"]').val(),
						"shipping_address1":form.find('[name="shipping_address1"]').val(),
						"shipping_address2":form.find('[name="shipping_address2"]').val(),
						"shipping_country_code":form.find('[name="shipping_country_code"]').val(),
						"shipping_state_code":form.find('[name="shipping_state_code"]').val(),
						"shipping_state_name":form.find('[name="shipping_state_name"]').val(),
						"shipping_city":form.find('[name="shipping_city"]').val(),
						"shipping_zip":form.find('[name="shipping_zip"]').val(),
						"shipping_phone":form.find('[name="shipping_phone"]').val(),
						"shipping_address_id": address_id,
						"store_url": get_checkout_storeurl_param()
					});
	}
	else {
		cc_data = {"billing_fullname":form.find('[name="billing_firstname"]').val(),
					"cvv2Number":form.find("#cc_cvv").val(),
					"cc_number":form.find("#cc_number").val(),
					"cc_expire_month":form.find("#cc_expire_month").val(),
					"cc_expire_year":form.find("#cc_expire_year").val()
					};
		$.extend(form_data,{"billing_firstname":form.find('[name="billing_firstname"]').val(),
						"billing_company":form.find('[name="billing_company"]').val(),
						"billing_address1":form.find('[name="billing_address1"]').val(),
						"billing_address2":form.find('[name="billing_address2"]').val(),
						"billing_country_code":form.find('[name="billing_country_code"]').val(),
						"billing_state_code":form.find('[name="billing_state_code"]').val(),
						"billing_state_name":form.find('[name="billing_state_name"]').val(),
						"billing_city":form.find('[name="billing_city"]').val(),
						"billing_zip":form.find('[name="billing_zip"]').val(),
						"billing_phone":form.find('[name="billing_phone"]').val(),
						"billing_address_id": address_id,

						"same_as_shipping": ((form.find('#same-as-shipping').prop("checked")) ? 1 : 0)
					});

		// validation for credt card info
		if(typeof cc_data.cc_number == "undefined" || isNaN(parseInt(cc_data.cc_number)) == true) {
			$('#cc_number').addClass('error-field');
			first_error.push('#cc_number');
			error_msg.push("Card number is not correct.");
		}
		/*
		if(error['cc_expire_month'] || error['cc_expire']) {
			first_error.push('#cc_expire_month');
			f.find('#cc_expire_month').validationEngine('showPrompt', 'This is a required field.', 'error', 'centerRight', true);	
		}
		*/
		if(typeof cc_data.cc_expire_month == "undefined" || isNaN(parseInt(cc_data.cc_expire_month)) == true || typeof cc_data.cc_expire_year == "undefined" || isNaN(parseInt(cc_data.cc_expire_year)) == true) {
			first_error.push('#cc_expire_year');
			$('#cc_expire_month').parent().addClass('error-field');
			$('#cc_expire_year').parent().addClass('error-field');
			error_msg.push("Expiration date is not correct.");
		}
		if(typeof cc_data.cvv2Number == "undefined" || isNaN(parseInt(cc_data.cvv2Number)) == true) {
			first_error.push('#cvv2Number');
			$('#cc_cvv').addClass('error-field');
			error_msg.push("Security code is required.");
		}
	}

	$.ajax({
		type: "POST",
		url: CS.main_url + 'global_content/saveaddress?type='+type,
		data: form_data,
		cache: false,
		success: function(response){
			response = $.parseJSON(response);
			if(!response || response == null || $.isEmptyObject(response)) {
				location.href = "/";
				return false;
			}
			
			var error = response['error'];
			if(typeof(error) == "string") {
				var form_container = ".reveal-modal.checkout-modal ";


				var error2 = "";
				if(response.hasOwnProperty('error2')) {
					error2 = response.error2;
				}

				$(form_container + '.error-content .wscolor2').html(error);	
				$(form_container + '.error-content .errortxt').html(error2);
				$(form_container + '.error-content').removeClass('hide');

				$('.reveal-modal.checkout-modal').removeClass('preloader');

				var inputPos = $(form_container + '.error-content').offset();
				$(document).scrollTop(inputPos.top);

			}else if(response.hasOwnProperty('error') || error_msg.length > 0) {
				if(response.hasOwnProperty('error')) {
					if(error['country']) {
						first_error.push('[name="' + type + '_country_code"]');
						error_msg.push("Please supply a country for this address.");
					}				
					if(error['firstname']) {
						$('[name="' + type + '_firstname"]').addClass('error-field');
						first_error.push('[name="' + type + '_firstname"]');
						if(type == "shipping") {
							error_msg.push("Please supply a first name for this address.");
						}
						else {
							error_msg.push("Cardholder\'s name is required.");
						}
					}
					if(error['lastname']) {
						$('[name="' + type + '_lastname"]').addClass('error-field');
						first_error.push('[name="' + type + '_lastname"]');
						error_msg.push("Please supply a last name for this address.");
					}
					if(error['address']) {
						$('[name="' + type + '_address1"]').addClass('error-field');
						$('[name="' + type + '_address2"]').addClass('error-field');
						first_error.push('[name="' + type + '_address1"]');
						error_msg.push("At least one address line must be supplied.");
					}
					if(error['city']) {
						$('[name="' + type + '_city"]').addClass('error-field');
						first_error.push('[name="' + type + '_city"]');
						error_msg.push("Please enter a city name for this address.");
					}
					if(error['state_code']) {
						$('[name="' + type + '_state_code"]').parent().addClass('error-field');
						first_error.push('[name="' + type + '_state_code"]');
						error_msg.push("Please supply a state, province or county.");
						//toggle_disable_autocomplete('enable','' + type + '');	
					}
					if(error['state_name']) {
						$('[name="' + type + '_state_name"]').addClass('error-field');
						first_error.push('[name="' + type + '_state_name"]');
						error_msg.push("Please supply a state, province or county.");
						//toggle_disable_autocomplete('disable','' + type + '');
					}			
					if(error['zip']) {
						$('[name="' + type + '_zip"]').addClass('error-field');
						first_error.push('[name="' + type + '_zip"]');
						error_msg.push("Please enter a valid ZIP or postal code.");
					}
					if(error['phone']) {
						$('[name="' + type + '_phone"]').addClass('error-field');
						first_error.push('[name="' + type + '_phone"]');
						error_msg.push("Please supply a phone number so we can call if there are any problems using this address.");
					}
					if(error['statecityzip']) {
						$('[name="' + type + '_state_code"]').addClass('error-field');
						$('[name="' + type + '_city"]').addClass('error-field');
						$('[name="' + type + '_zip"]').addClass('error-field');

						first_error.push('[name="' + type + '_city"]');
						error_msg.push("State, City and/or Postal does not match.");
					}
					
					if(type == "billing") {
						if(error['cc_number']) {
							$('#cc_number').addClass('error-field');
							first_error.push('#cc_number');
							error_msg.push("Card number is not correct.");
						}
						/*
						if(error['cc_expire_month'] || error['cc_expire']) {
							first_error.push('#cc_expire_month');
							f.find('#cc_expire_month').validationEngine('showPrompt', 'This is a required field.', 'error', 'centerRight', true);	
						}
						*/
						if(error['cc_expire']) {
							first_error.push('#cc_expire_year');
							$('#cc_expire_month').parent().addClass('error-field');
							$('#cc_expire_year').parent().addClass('error-field');
							error_msg.push("Expiration date is not correct.");
						}
						if(error['cvv2Number']) {
							first_error.push('#cvv2Number');
							$('#cc_cvv').addClass('error-field');
							error_msg.push("Security code is required.");
						}
					}
				}
								
				if(error_msg.length > 0) {			
					error_msg = error_msg.join('<br /> ');
					var form_container = "";
					$(form_container + '.error-content .wscolor2').html("Please correct the following error(s):");	
					$(form_container + '.error-content .errortxt').html(error_msg);
					$(form_container + '.error-content').removeClass('hide');

					$('.reveal-modal.checkout-modal').removeClass('preloader');

					var inputPos = $(first_error[0]).offset();
					inputPos = $('form').offset(); 
					$(document).scrollTop(inputPos.top);
				}

				if(!response.hasOwnProperty('error')) {
					checkout_deleteaddress(response.address_id,'billing');
				}
			}
			else {
				if(type == "shipping") {
					if($('.accnt-settings').length > 0){
						location.href = location.href;
					}else{
						//checkout_steps(type,response.address_id,next,'','','',response.shipto_country);
						reload_checkout_content(true);
					}
				}
				else if(type == "billing") {
					create_customer_payment_method({
						number: cc_data.cc_number,
						cardholderName: cc_data.billing_fullname,
						expirationMonth: cc_data.cc_expire_month,
						expirationYear: cc_data.cc_expire_year,
						// CVV if required
						cvv: cc_data.cvv2Number,
						address_id: response.address_id
					});
				}
			}
		}
	});
}

function checkout_getaddress_html(type) {
	$('.reveal-modal.checkout-modal .error-content').addClass('hide');
	$('.reveal-modal.checkout-modal form input, .reveal-modal.checkout-modal .mystyled-select, .form-wrapper input, .form-wrapper .mystyled-select').removeClass('error-field');

	$('.reveal-modal.checkout-modal form').addClass('hide');
	$('.reveal-modal.checkout-modal form.listing-form').removeClass('hide');
	$('.reveal-modal.checkout-modal').foundation('reveal', 'open', { close_on_background_click: true});
	$('.reveal-modal.checkout-modal').addClass('preloader');
	$('.reveal-modal.checkout-modal').centerized();

	var form = $('.reveal-modal.checkout-modal form.listing-form');
	form.html('');
	data = get_checkout_user_data();
	data.type = type; 
	$.ajax({
		type: "POST",
		url: CS.main_url + 'global_content/getaddress_html?type='+type,
		data: data,
		cache: false,
		success: function(response){
			form.html(response);
			$('.reveal-modal.checkout-modal').centerized();
			$('.reveal-modal.checkout-modal').removeClass('preloader');
		},
		error: function(jqXHR, textStatus, errorThrown ) {
			if(jqXHR.status == 404) {
				//location.href = "/";
			}
		}
	});
}

function checkout_getaddress(address_id,type) {
	toggle_autocomplete_cityzip({
		elements: {
			country: '#shipping_country_code',
			us_state: '[name="shipping_state_code"]',
			state: '[name="shipping_state_name"]',
			city: '[name="shipping_city"]',
			zip: '[name="shipping_zip"]'
		}
	});

	var form = $('.reveal-modal.checkout-modal form.'+type+'-form');

	$('.reveal-modal.checkout-modal form').addClass('hide');
	form.removeClass('hide');

	$('.reveal-modal.checkout-modal').foundation('reveal', 'open', { close_on_background_click: true});
	$('.reveal-modal.checkout-modal').addClass('preloader');

	$('.reveal-modal.checkout-modal').centerized();

	//$('#mainpreloader-wrapper').show();
	data = get_checkout_user_data();
	data.address_id = address_id; 
	$.ajax({
		type: "POST",
		url: CS.main_url + 'global_content/getaddress?type='+type,
		data: data,
		cache: false,
		success: function(response){
			response = $.parseJSON(response);

			if(!response || response == null) {
				$('.reveal-modal.checkout-modal').foundation('reveal', 'close');
				return false;
			}
			
			for(var field in response) {
				if(response.hasOwnProperty(field)) {
					//value = typeof(response[d]) == 'undefined' ? '' : response[d];
					
					if(type == "shipping") {
						if(field != "shipping_state") {
							$('.shipping-form').find('[name="'+field+'"]').val(response[field]);
						}
					}
				}
			}
			
			if($('[name="shipping_country_code"] option:selected').val() == "US") {
				$('[name="shipping_state_code"] option').removeAttr('selected');
				$('[name="shipping_state_code"] option[value="' + response['shipping_state'] + '"]').attr('selected','selected');
				$('[name="shipping_state_name"]').val('');
				$('[name="shipping_state_name"]').addClass('hide');
				$('[name="shipping_state_code"]').parent().removeClass('hide');
				
				//toggle_disable_autocomplete('enable','shipping');
			}
			else {
				$('[name="shipping_state_code"]').val('');
				$('[name="shipping_state_name"]').val(response['shipping_state']);
				$('[name="shipping_state_name"]').removeClass('hide');
				$('[name="shipping_state_code"]').parent().addClass('hide');
				
				//toggle_disable_autocomplete('disable','shipping');
			}


			form.attr('address_id',address_id);
			//$('.checkout-wrapper .tbl-cell h4 span').html('Edit '+type+' address');
			//$('.checkout-wrapper .tbl-cell h4 .back-link').removeClass('hide');
			$('.reveal-modal.checkout-modal').removeClass('preloader');

			form.data('formdata',form.serialize());
		},
		error: function(jqXHR, textStatus, errorThrown ) {
			if(jqXHR.status == 404) {
				location.href = "/";
			}
		}
	});
}

function checkout_deleteaddress(address_id,type,reload_getaddress_html) {
	if(type == "shipping") {
		if(!confirm("Are you sure you want to delete this address?")) {
			return false;	
		}
	}
	
	$('.reveal-modal.checkout-modal').addClass('preloader');

	var data = get_checkout_user_data();
	data.address_id = address_id;
	$.ajax({
		type: "POST",
		url: CS.main_url + 'global_content/deleteaddress?type='+type,
		data: data,
		cache: false,
		success: function(response){
			if(reload_getaddress_html) {
				checkout_getaddress_html(type);

				reload_checkout_content(false);
			}
			else {
				$('.reveal-modal.checkout-modal').removeClass('preloader');
			}
		}
	});
}

function create_customer_payment_method(data) {
		CS.braintree_token.tokenizeCard({
			number: data.number,
			cardholderName: data.cardholderName,
			expirationMonth: data.expirationMonth,
			expirationYear: data.expirationYear,
			// CVV if required
			cvv: data.cvv,
			options: {
				verifyCard: true	
			}
		}, function (err, nonce) {
			if(err != null) {
				checkout_deleteaddress(data.address_id,'billing');
				alert('Sorry, we cannot validate your credit card as of the moment.');
				console.log('Error in creating payment method:' + err);
			}
			else {
				var form_data = get_checkout_user_data();
				form_data.address_id = data.address_id;
				form_data.nonce = nonce;

				$.ajax({
					type: "POST",
					url: CS.main_url + 'global_content/update_billing_address_nonce',
					data: form_data,
					cache: false,
					success: function(response){
						response = $.parseJSON(response);
						if(!response || response == null || $.isEmptyObject(response)) {
							location.href = '/';
						}
						
						if(response.hasOwnProperty('error')) {
							$('.reveal-modal.checkout-modal .error-content .wscolor2').html("We are unable to authorize your credit card.");	
							$('.reveal-modal.checkout-modal .error-content .errortxt').html('Please double-check your credit card details including your expiration date and CCV code and try again.');
							$('.reveal-modal.checkout-modal .error-content').removeClass('hide');

							$('.reveal-modal.checkout-modal').removeClass('preloader');
						}
						else {
							reload_checkout_content(true);
						}

					}
				});
			}
		  
		});
}

function checkout_steps(type,address_id,payment_type,nonce,email) {
	checkout_toggle_loader();
	var data = get_checkout_user_data();
	data.address_id = address_id;
	data.store_url = get_checkout_storeurl_param();
	data.nonce = nonce;
	data.email = email;

	if(type == "billing") {
		payment_type = (typeof payment_type == "undefined") ? "CreditCard" : "Paypal";
		payment_type = (payment_type != "Paypal") ? "CreditCard" : "Paypal";
		data.payment_type = payment_type;
	}
	else {
		var form_container = ".reveal-modal.checkout-modal ";
	}

	$('.reveal-modal.checkout-modal').addClass('preloader');

	$.ajax({
		type: "POST",
		url: CS.main_url + 'global_content/set_default_address?type='+type,
		data: data,
		cache: false,
		success: function(response){
			response = $.parseJSON(response);
			if(!response || response == null) {
				return false;
			}

			if ($.isEmptyObject(response)) {
				checkout_toggle_loader();
				return;
			}


			if(type == "shipping") {
				if(response.hasOwnProperty('error')) {
					var error = response.error;
					var error2 = "";
					if(response.hasOwnProperty('error2')) {
						error2 = response.error2;
					}

					$(form_container + '.error-content .wscolor2').html(error);	
					$(form_container + '.error-content .errortxt').html(error2);
					$(form_container + '.error-content').removeClass('hide');
					checkout_toggle_loader();
					return;
				}
			}

			reload_checkout_content(true);
		}
	});
}

function sscheckout(data) {
	checkout_toggle_loader("processing order...");
	var store_id =0, user_id = 0;
	if(typeof data == "object" && data.store_id >= 0 && data.user_id >= 0) {
		store_id = data.store_id;
		user_id = data.user_id;
	}

	var src = getUrlParameters("src","");
	var form_data = get_checkout_user_data();
	form_data.store_url = get_checkout_storeurl_param();
	//form_data.checkout_location = {store_id:store_id,user_id:user_id};
	form_data.checkout_location = src;

	$.ajax({
		type: "POST",
		url: CS.main_url + 'global_content/checkout_placeorder',
		data: form_data,
		cache: false,
		success: function(response){
			response = $.parseJSON(response);

			if(!response || response == null || $.isEmptyObject(response) || !response.hasOwnProperty('status')) {	
				alert('Sorry, server is busy as of the moment. Please try again.\n\nIf this problem persists, contact Shopstyx Customer Support (support@shopstyx.com).')
				//location.href = "/";
				//return false;
			}

			if(response.status == "Error") {
				switch(response.error_code) {
					case 100: // user not logged in
						alert('You are no longer signed in to Shoptyx. Please sign in to continue.');
						location.href = "/";
						return;
						break;
					case 101: // cart is empty
					case 102: // cart is empty (for specific merchant)
					case 103: // Cart is empty (merchant store is does not exist)
						location.href = "/";
						break;

					case 104:
						alert('You have no shipping address.\nPlease enter shipping address.');
						//location.href = "/";
						//return;
						break;
					case 105:
						alert('You have no billing address.\nPlease enter billing address.');
						//location.href = "/";
						//return;
						break;

					case 0: // Cannot access server (cannot create/log to transaction record DB): error status returned from nodejs server
					case 1: // Cannot access server (cannot create/log to transaction record DB): connection failed to nodejs server
						alert('Sorry, server is busy as of the moment. Please try again.\n\nIf this problem persists, contact Shopstyx Customer Support  (support@shopstyx.com).')
						break;
				}
				checkout_toggle_loader();
			}
			else {
				sscheckout_success(response);
			}

		},
		error: function(jqXHR, textStatus, errorThrown ) {
			if(jqXHR.status == 404) {
				location.href = "/";
			}
			else if(jqXHR.status == 500) {
				console.log("500 Internal Server Error");
				alert('Sorry, server is busy as of the moment. Please try again.\n\nIf this problem persists, contact Shopstyx Customer Support  (support@shopstyx.com).')
			}
			console.log(jqXHR);
			console.log(textStatus);
			console.log(errorThrown);

			checkout_toggle_loader();
		}
	});
}

function sscheckout_success(data) {
	if(data.is_webstore) {
		$('#checkout-placorder-form').append('<input type="hidden" name="order_id" value="'+data.data.order_id+'" />' );
		$('#checkout-placorder-form').submit();
	}
	else {
		var form_data = get_checkout_user_data();
		form_data.id = data.data.order_id;
		form_data.src = getUrlParameters("src","");
		$.ajax({
			type: "POST",
			url: CS.main_url + 'global_content/checkout_placeorder_success',
			data: form_data,
			cache: false,
			success: function(response){
				if(typeof CS.iswebstore == "undefined") {
					$('.checkout-wrapper').remove();
					$('.checkout-header').remove();
					$('#header').after(response);
				}
				else {
					$('#content-wrapper').html(response);
				}		

				if(!$('#loadbox-wrapper').hasClass('hide')) {
					checkout_toggle_loader();
				}
				else {
					checkout_toggle_loader();
					checkout_toggle_loader();
				}

				/*$('.checkout-wrapper div.top-wrapper').remove();
				$('.checkout-wrapper .bottom-wrapper').html(response);*/
			},
			error: function(jqXHR, textStatus, errorThrown ) {
				if(jqXHR.status == 404) {
					//location.href = "/";
				}
				console.log(jqXHR);
				console.log(textStatus);
				console.log(errorThrown);
				//location.href = "/";
				checkout_toggle_loader();
			}
		});
	}
}

function addslashes(str) {
  return (str + '')
    .replace(/[\\"']/g, '\\$&')
    .replace(/\u0000/g, '\\0');
}

function stripslashes(str) {
  return (str + '')
    .replace(/\\(.?)/g, function(s, n1) {
      switch (n1) {
        case '\\':
          return '\\';
        case '0':
          return '\u0000';
        case '':
          return '';
        default:
          return n1;
      }
    });
}