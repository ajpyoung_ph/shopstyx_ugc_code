function crypto(user_id,token) {
	this.user_id = user_id;
	this.token = token;
	this.key = "";
	this.resultstring = "";
	this.statuscode = "";
	
	// the following functions below are for getting the key
	this.crypto_get_token = function(){
			$.ajax({
						type: "POST",
						url: "/api/v1/crypto/get_token", 
						data: { user_id: user_id, token: token }, 
						dataType: "json",
						async: false,
						success: this.resultkey
					});
	}
	this.resultkey = function(data){
		statuscode = data.status;
		key = data.results.token;
	}
	this.get_cryptokey = function (){
		return key;
	}
	
	// the following functions below are for getting the encrypted and decrypted strings
	this.crypto_encrypt = function(token,user_id,key,msg){
		$.ajax({
						type: "POST",
						url: "/api/v1/crypto/encrypt", 
						data: { user_id: user_id, token: token, key: key, msg: msg }, 
						dataType: "json",
						async: false,
						success: this.resultstring
					});
	}
	this.crypto_decrypt = function(token,user_id,key,msg){
		$.ajax({
						type: "POST",
						url: "/api/v1/crypto/decrypt", 
						data: { user_id: user_id, token: token, key: key, msg: msg }, 
						dataType: "json",
						async: false,
						success: this.resultstring
					});
	}
	this.resultstring = function(data){
		statuscode = data.status;
		resultstring = data.results.msg;
	}
	this.get_resultstring = function (){
		return resultstring;
	}
}
