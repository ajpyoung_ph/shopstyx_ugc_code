//function toggle_autocomplete(enable,type) {

/* 
 *	Parameters:
 *		settings => {country_field: $('#country') }
 *
 */
function toggle_autocomplete_cityzip(settings) {	
	var t = this,
		el_country = $(settings.elements.country),
		el_us_state = $(settings.elements.us_state),
		el_state = $(settings.elements.state),
		el_city = $(settings.elements.city),
		el_zip = $(settings.elements.zip),
		values = {country: el_country.val(),
				us_state: el_us_state.val(),
				city: el_city.val(),
				zip: el_zip.val()
				};

	if(el_country.length == 0) {
		console.log('country element does not exists.');
		return false;
	}

	el_country.change(function(){
		values.country = $(this).val();
		t.shopstyx_toggle_automplete({country:values.country});
	});

	el_us_state.change(function(){
		values.us_state = $(this).val();
		t.shopstyx_toggle_automplete({us_state:values.us_state});
	});

	t.shopstyx_toggle_automplete = function(data) {
		values.city = $(settings.elements.city).val();
		values.zip = $(settings.elements.zip).val();

		if(typeof data == "object") {
			if(typeof data.country != "undefined") {
				values.country = data.country;
			}
			if(typeof data.us_state != "undefined") {
				values.us_state = data.us_state;
			}
		}

		//console.log(values)

		el_city.autocomplete({ disabled: true });
		el_zip.autocomplete({ disabled: true });

		if(values.country == "US") {
			//console.log(el_us_state)
			el_us_state.parent().removeClass('hide');
			el_state.addClass('hide');

			el_city.autocomplete( "enable" );
			el_city.autocomplete({  
				//define callback to format results  
				source: function(req, add){  
					//pass request to server  
					$.getJSON("/autocomplete/US_city.html?state_code="+values.us_state+"&callback=?", req, function(data) {  
							//create array for response objects  
							var suggestions = [];  
							//process response  
							$.each(data, function(i, val){  
							suggestions.push(val.name);  
						});  

						//pass array to callback  
						add(suggestions);  
					});  
				}, 
				 select: function(e, ui) {  
					//create formatted friend  
					var _city = ui.item.value; 
					values.city = _city;
						$.get("/autocomplete/US_zip.html?state_code="+values.us_state+"&city="+_city+"&callback=?", function(data) {
							data = data.replace('?','').replace('(','').replace(')','');
							data = $.parseJSON(data);
							
							if(!$.isEmptyObject(data) && data.length == 1) {
								el_zip.val(data[0].name);	
							}
							else {
								el_zip.val('');	
							}
						});
				},
				//define select handler  
				change: function() {  
					_city = $.trim($(this).val());
					values.city = _city;
					$.get("/autocomplete/US_zip.html?state_code="+values.us_state+"&city="+_city+"&callback=?", function(data) {
						data = data.replace('?','').replace('(','').replace(')','');
						data = $.parseJSON(data);
						if(!$.isEmptyObject(data) && data.length == 1) {
							el_zip.val(data[0].name);	
						}
					});
				}  
			});


			el_zip.autocomplete( "enable" );
			el_zip.autocomplete({  
				//define callback to format results  
				source: function(req, add){  
					//pass request to server  
					$.getJSON("/autocomplete/US_zip.html?state_code="+values.us_state+"&city="+values.city+"&callback=?", req, function(data) {  
						//create array for response objects  
						var suggestions = [];  
						//process response  
						$.each(data, function(i, val){  
							suggestions.push(val.name);  
						});  
						//pass array to callback  
						add(suggestions);  
					});  
				}, 
				 select: function(e, ui) {  
					//create formatted friend  
					var _zip = ui.item.value; 
					values.zip = _zip;
						$.get("/autocomplete/US_city.html?state_code="+values.us_state+"&zip="+_zip+"&callback=?", function(data) {
							data = data.replace('?','').replace('(','').replace(')','');
							data = $.parseJSON(data);
							if(!$.isEmptyObject(data) && data.length == 1) {
								el_city.val(data[0].name);	
							}
							else {
								//el_city.val('');	
							}
						});
				},
				//define select handler  
				change: function() {  
					_zip = $.trim($(this).val());
					values.zip = _zip;
					$.get("/autocomplete/US_city.html?state_code="+values.us_state+"&zip="+_zip+"&callback=?", function(data) {
						data = data.replace('?','').replace('(','').replace(')','');
						data = $.parseJSON(data);
						if(!$.isEmptyObject(data) && data.length == 1) {
							el_city.val(data[0].name);	
						}
						else {
							//el_city.val('');	
						}
					});
				}  
				 
			}); 
		}
		else {
			el_city.autocomplete({ disabled: true });
			el_zip.autocomplete({ disabled: true });

			el_us_state.parent().addClass('hide');
			el_state.removeClass('hide');
		}
	}

	t.shopstyx_automplete_enable = function() {

	}

	if(el_country.val() == "US") {
		values.us_state = el_us_state.val();
		t.shopstyx_toggle_automplete();
	}
}
