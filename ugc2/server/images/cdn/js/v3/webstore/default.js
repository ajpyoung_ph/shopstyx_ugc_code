var SSAjax = function( options ) {

		//if(typeof CS.iswebstore == "undefined" || (typeof options.is_checkout != "undefined" && options.is_checkout)) {
		if(typeof CS.is_cookies == "undefined" || !CS.is_cookies) {
			var form_data = {
					token: store.get('token'),
					user_id: store.get('user_id'),
					_Ssfp_ID: store.get('_Ssfp_ID')
				};
		}
		else {
			var form_data = {
					token: $.cookies.get('token'),
					user_id: $.cookies.get('uid')
				};
		}

        var settings = $.extend(true,{
                // These are the defaults.
                loadCSS: [],
                type: "POST",
                data: form_data,
                url: "",
                success: function(response) {
                    //console.log(response);
                },
                loadJS: []
                
            }, options );

        var success = settings.success;
        settings.success = function(response, textStatus, xmlHttp) {
            
            if(response == "" || response == false) {
                // invalid toke and/or user_id, so logout
                //return;
            }
            success(response);   
            //console.log(xmlHttp.getAllResponseHeaders())  
            //console.log(xmlHttp.getResponseHeader("Date"))        
        };

        $.ajax(settings);
}

/*
 * isSafariBrowser
 */

function isSafariBrowser() {
    return /^((?!chrome).)*safari/i.test(navigator.userAgent);
}

function onMessage(messageEvent) { 
	var data = messageEvent.data, method; 
	console.log(messageEvent);
	//console.log(CS.main_url);
	if(messageEvent.origin + '/' != CS.main_url) {
		return;
	}

	if(data.hasOwnProperty('method')) {
		method = data.method;
		switch(method) {
			case 'get_login_data':
				$.cookies.set("uid",data.user_id,400);
				$.cookies.set("token",data.token,400);
				$.cookies.set("ut",data.user_id + "." + data.token,400);
				setTimeout(function(){
					//console.log('webstore: get_login_data');
					window.location.href=window.location.href
				},100);
				break;
			case 'reload_page':
				//console.log('webstore: reload_page');
				window.location.href=window.location.href
				break;

		}
	}
}

function update_localstorage(method,data) {
	var postdata;
	if(method == "login") {
		$.cookies.set("uid",data.user_id,400);
		$.cookies.set("token",data.token,400);
		$.cookies.set("ut",data.user_id + "." + data.token,400);

		postdata = {"method":"set_login_data",
							"user_id":data.user_id,
							"token":data.token
						};
		setTimeout(function(){
			windowProxy.post(postdata);
		}, 100);
	}

	if(method == "logout") {
		if(isSafariBrowser())
		{
			$.cookies.del("uid");
			$.cookies.del("token");
			$.cookies.del("ut");
			location.href = '/mob_redirect.html?a=logout&d='+encodeURIComponent(location.href);
		} else {
			// Not safari
			postdata = {"method":"remove_login_data"};
			$.cookies.del("uid");
			$.cookies.del("token");
			$.cookies.del("ut");
			setTimeout(function(){
				windowProxy.post(postdata);
			}, 100);
		}
	}
	
}

function get_user_credentials_params() {
	var data = {user_id: $.cookies.get("uid"),
				token: $.cookies.get("token")};
	return data;
}

function load_page(param) {
	var url = $(location).attr('href').split('#');
	var mainURL = url[0], hash = url[1];
	
	//hash = typeof hash == "undefined" ? "" : hash;
	//var container = get_hash_querystring_value(hash,'c');
	var param = (typeof param == "undefined") ? {} : param; 
	var container = "#wrapper";
	var page_title;
	var user_id = store.get('user_id');
	var token = store.get('token');
	var form_data = {
		isAjax:true,
		token: token,
		user_id: user_id
	};
	var browserNav = false;
	
	if(param.hasOwnProperty('container')) {
		container = param.container;
	}

	if(param.hasOwnProperty('data')) {
		form_data = $.extend(true,form_data, param.data);
	}

	if(param.hasOwnProperty('page_tile')) {
		page_title = param.page_title;
	}
	else {
		page_title = $(document).find("title").text();
	}

	if(param.hasOwnProperty('url')) {
		mainURL = param.url;
		if(mainURL.indexOf(CS.siteURL) == -1) {
			mainURL = CS.siteURL + mainURL.substr(1);
		}
	}	

	if(param.hasOwnProperty('browserNav')) {
		browserNav = (param.browserNav) ? true : false;
	}

	if ( History.enabled) {
		History.pushState(null, page_title, mainURL);
	}

	//if(mainURL == History.getState().url && browserNav) {

	if(browserNav) {
		return;
	}

	SSAjax({
		is_checkout: true,
		data: form_data,
		url: mainURL,
		success:function(response) {

			try {
		        var object = JSON.parse(response);

		        // Handle non-exception-throwing cases:
		        // Neither JSON.parse(false) or JSON.parse(1234) throw errors, hence the type-checking,
		        // but... JSON.parse(null) returns 'null', and typeof null === "object", 
		        // so we must check for that, too.
		        if (object && typeof object === "object" && object !== null) {
		            console.log(object);
		            switch(object.redirect_code) {
		            	case 100:
		            	case 101:
		            	case 102:
		            		location.href = object.url;
		            		break;
		            }
		        }
		    }
		    catch (e) { 
				$(container).html(response);
				toggle_preloader_checkoutcontent();

		    }
		},
		error: function(jqXHR, textStatus, errorThrown ) {
			if(jqXHR.status == 404) {
				location.href = CS.webstore_url;
			}
		}
	});
}

function toggle_preloader_checkoutcontent() {
	if($('#loadbox-wrapper').eq(0).hasClass('hide')) {
		$('#loadbox-wrapper').removeClass('hide');
		$('#main-wrapper').addClass('hide');
	}
	else {
		$('#loadbox-wrapper').addClass('hide');
		$('#main-wrapper').removeClass('hide');
	}
}


var triggers = $('.lt-slideshow .nav span');
var triggers_prev = $('.lt-slideshow .slideshow .prev-btn');
var triggers_next = $('.lt-slideshow .slideshow .next-btn');
var images = $('.lt-slideshow ul li');
var lastElem = triggers.length-1;
var mask = $('.lt-slideshow ul');
var imgWidth = images.width();
var target;

triggers.first().addClass('active');
triggers_next.first().addClass('active');
mask.css('width', imgWidth*(lastElem+1) +'px');

function sliderResponse(target) {
    mask.stop(true,false).animate({'left':'-'+ imgWidth*target +'px'},300);
    triggers.removeClass('active').eq(target).addClass('active');
    triggers_next.removeClass('active').eq(target).addClass('active');
}

triggers.click(function() {
    if ( !$(this).hasClass('active') ) {
        target = $(this).index();
        sliderResponse(target);
        resetTiming();
    }
});

triggers_next.click(function(){
	var image_length = images.length - 1;
	target = parseInt($(this).attr('data')) + 1;
	if(target > image_length){ target = 0; }
	sliderResponse(target);
	clearInterval(timingRun);
});

triggers_prev.click(function(){
	target = parseInt($(this).attr('data')) - 1;
	if(target < 0){ target = images.length - 1; }
	sliderResponse(target);
	clearInterval(timingRun);
});

images.mouseenter(function(){
	clearInterval(timingRun);
});

images.mouseout(function(){
	resetTiming();
});


function sliderTiming() {
    target = $('.lt-slideshow .nav span.active').index();
    target === lastElem ? target = 0 : target = target+1;
    sliderResponse(target);
}
var timingRun = setInterval(function() { sliderTiming(); },6000);
function resetTiming() {
    clearInterval(timingRun);
    timingRun = setInterval(function() { sliderTiming(); },6000);
}

// $(document).on("click",".prod-left .nav span", function(e) {
	// e.preventDefault(); e.stopImmediatePropagation();
	// var $this = $(this);
	// var slide = $this.attr('data-id');
	
	// $('.preview-image .slide-show').fadeOut(300, function() {
		// $('.show' + slide).fadeIn(200, function(){
			// $('.prod-left .nav span').removeClass('active');
			// $this.addClass('active');
		// });
	// });
// });

// Header menu events
$(document).on('mouseenter', '.h-log .notif', function(e){
	//e.preventDefault(); e.stopImmediatePropagation();
	close_panels();
	var cntr = $('.navLink.notif .bg span.total-counter');
	cntr.addClass('hide');
	
	// $.ajax({
		// type: "post",
		// url: CS.main_url + 'notifications/mark_read_notifs',
		// data: get_user_credentials_params(),
		// success: function(response){
			// //overlay.addClass('hide');
			// //loader.addClass('hide');	
			// //console.log(ajax_container)
			// //ajax_container.html(response);
		// }
	// });
	numnotifs=0;
				
	$(this).find('.notification-panel.npops').removeClass('hide');
});

$(document).on('mouseleave', '.h-log .notif', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	close_panels();
});

$(document).on("mouseenter",".h-log .wallet", function(e) {
	e.preventDefault(); e.stopImmediatePropagation();
	close_panels();
	if($(this).hasClass('active')) {
		$(this).find('.wallet-popup').removeClass('hide');
	}
	else {
		load_wallet();
	}
});
$(document).on("mouseleave",".h-log .wallet", function(e) {
	e.preventDefault(); e.stopImmediatePropagation();
	close_panels();
});
$(document).on('open', '#wallet-pop', function () {
	$(this).centerized();
	$('#main-wrapper').hide();
});
$(document).on('opened', '#wallet-pop', function () {
	$('.reveal-modal-bg').addClass('reveal-modal-bg-color');
});
$(document).on('closed', '#wallet-pop', function () {
	if(typeof dispute_countdownTimer != "undefined") {
		clearInterval(dispute_countdownTimer);
	}
	var loader = $(this).find('.loadcont');
	loader.addClass('hide');	
	$(this).find('.loader').addClass('hide');	
	$(this).find('.loader-overlay').addClass('hide');
	$('#main-wrapper').show();

	if(typeof scrollopts != "undefined") {
		delete scrollopts['for-purchasehistory'];
	}
});

$(document).on('click', '.h-log .wallet-popup .claim-money-btn:not(".disabled")', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	var type = ($(this).hasClass('C')) ? "C" : "S";
	claim_walet_popup(type);
});

$(document).on('click', '.wallet-modal #claim-wallet-form .paypal-email-txt a, .wallet-modal #claim-wallet-form .no-paypal-email a', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	var type = $('.wallet-modal #claim-wallet-form').attr('wtype');
	wallet_paypalemail_popup(type);
});

$(document).on('click', '#wallet-pop .header-wrapper .right.back-btn, #wallet-pop .header-wrapper .claim-wallet-backbtn', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	var type = $(this).attr('wtype');
	claim_walet_popup(type);
});

$(document).on('click', '.wallet-modal .cont .pent #save-paypal-email-btn', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	var wtype = $(this).attr('wtype');
	save_promoter_paypalemail(wtype);
});

$(document).on('click', '.wallet .wallet-popup .history-btn.walletpurchases, #wallet-pop .header-wrapper .purchase-history-backbtn', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	load_wallet_purchase_history();
});

$(document).on('click', '.wallet .wallet-popup .history-btn.walletsales', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	load_wallet_sales_history();
});

$(document).on('click', '#wallet-pop .header-wrapper .viewhistory-btn', function(e){
	var wtype = $(this).attr('wtype');
	e.preventDefault(); e.stopImmediatePropagation();
	load_wallet_claimed_history(wtype);
});

/* Claim Money on wallet */
$(document).on('keyup ', '.wallet-modal #claim-wallet-form #claim-wallet-amount', function(e) {
	e.stopImmediatePropagation();
	val = parseFloat($(this).val());
	val = isNaN(val) ? 0 : val;
	paypal_fee = (val*0.02);
	paypal_fee = Math.round(paypal_fee*100) / 100; 
	paypal_fee = paypal_fee.toFixed(2);
	claim_amount = val - paypal_fee;
	claim_amount = Math.round(claim_amount*100) / 100; 
	//claim_amount = claim_amount.toFixed(2);
	claim_amount = claim_amount.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");
	$('#claim-wallet-form .claim-form-wrapper .field input#claim-wallet-paypal-fee').val(paypal_fee);
	$('#claim-wallet-form .claim-form-wrapper .claim-wallet-btn').html('Claim $'+claim_amount);
});

$(document).on('click', '.wallet-modal #claim-wallet-form .claim-form-wrapper .claim-wallet-btn:not(".disabled")', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	claim_wallet($(this),'authenticate');
});

$(document).on('click', '#wallet-pop .cont .claimcofirmation-wrapper button', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	claim_wallet($(this),'claim');
});

$(document).on('click', '#wallet-pop .cont .my-tbl.purchasehistory .tbl-cell.ordertotal .od-btns-wrapper', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	var id = $(this).attr('order_id');
	view_purchase_order_details(id);
});

$(document).on('click', '#wallet-pop .cont .order-details-wrapper .confirm-order-btn.enabled', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	var id = $(this).attr('order_id');
	confirm_transaction_order(id);
});

$(document).on('click', '#wallet-pop .cont .order-details-wrapper .open-dispute-product-btn .distpute-type-wrapper .dlink', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	var el = $(this).parent(),
		id = el.attr('order_id'),
		pid = el.attr('pid'),
		variant_id = el.attr('variant_id')
		dtype = $(this).attr('dtype');
	open_dispute_order_product(dtype,id,pid,variant_id);
});

$(document).on('click', '#wallet-pop .cont .open-dispute-modal .closedispute-btn', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	$('#wallet-pop .open-dispute-modal').addClass('hide').html('');
	$('#wallet-pop .cont .order-details-wrapper').removeClass('hide');
});

$(document).on('touchstart click', '.header-content .message .icon', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	doNewMessage();
});

$(document).on('mouseenter', '.header-content .message', function(e){
	//e.preventDefault(); e.stopImmediatePropagation();
	close_panels();
	nummsg = 0;
	$(this).find('.messages-panel.npops').removeClass('hide');
});

$(document).on('mouseleave', '.header-content .message', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	$(this).find('.messages-panel.npops').addClass('hide');
	//close_messages();
});


$(document).on("click",".prod-left .nav span, .pcontent-details .nav span", function(e) {
	e.preventDefault(); e.stopImmediatePropagation();
	var $this = $(this);
	var slide = $this.attr('data-id');
	
	if($this.hasClass('onpopup')){
		$('.preview-pic .not-active-slide').hide();
		$('.preview-pic .active-slide').fadeOut(200, function() {
			$(this).addClass('not-active-slide');
			$(this).removeClass('active-slide');
			$('.show' + slide).fadeIn(100, function(){
				$(this).addClass('active-slide');
				$('.prod-img-pop .nav span').removeClass('active');
				$this.addClass('active');
			});
		});
	}
	if(!$this.hasClass('onpopup')){
		$('.preview-image-wrapper .not-active-slide').hide();
		$('.preview-image-wrapper .active-slide').fadeOut(200, function() {
			$(this).addClass('not-active-slide');
			$(this).removeClass('active-slide');
			$('.show-img' + slide).fadeIn(100, function(){
				$(this).addClass('active-slide');
				$('.prod-img-content .nav span').removeClass('active');
				$this.addClass('active');
			});
		});
	}
	
});

$(document).on('click', '.product-info-modal .more-details .next, .product-info-modal .more-details .prev', function(e){
	var img_count = parseInt($('.product-info-modal .more-details .nav .onpopup').length);
	var on_img = parseInt($('.product-info-modal .more-details .nav .active').attr('data-id'));
	var pop;
	if($(this).hasClass('next')){
		pop = on_img + 1;
		if(on_img == img_count){ pop = 1; }
	}
	if($(this).hasClass('prev')){
		pop = on_img - 1;
		if(on_img <= 1){ pop = img_count; }
	}
	$('.onpopup' + pop).trigger('click');
});


$(document).on('click', '.stick-content a', function(e) {
	e.preventDefault(); e.stopImmediatePropagation();
	window.open($(this).attr('href'));
});

$(document).on('click', '.seemore', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	var show_img = $(this).attr('data-showing');
	var ontab = $(this).attr('data-tab');
	$('#seeMoreModal').foundation('reveal', 'open', {
		animation_speed: 0
	});
	$('.product-info-modal .stick-wrapper').html('');
	$('.product-info-modal .stick-loader').removeClass('hide');
	$.ajax({
		type: "POST",
		url: $(this).attr('data-url'),
		data: {seemore:true},
		cache: false,
		success: function(response){
			$('.product-info-modal .stick-wrapper').html(response);
			$('.onpopup' + show_img).trigger('click');
			$('.' + ontab).trigger('click');
			$('.product-info-modal .stick-loader').addClass('hide');
		}
	});
});

$(document).on('open', '#seeMoreModal', function (e) {
	$(this).centerized();
});

$(document).on('closed', '.product-info-modal', function (e) {
	$(this).find('.product-info-content .stick-wrapper').html('');
});

$(document).on('click', '.ulogin', function(e){
	if(!isSafariBrowser())
	{
		e.preventDefault(); e.stopImmediatePropagation();
		signin_modal();
	} else {
		// popup for safari
		dataService.popup('/mob_redirect.html?a=login&d='+window.location.hostname);
	}
});

$(document).on('keypress', '#search', function(e){
	e.stopImmediatePropagation();
	if (e.which == 13) {
		if($.trim($(this).val()) == ''){ return false; }
		
		if($('.uri-page').val() == 'promos'){
			location.href = root + 'promos?search=' + $(this).val();
		}else{
			location.href = root + '?search=' + $(this).val();
		}
	}
});

$(document).on('mouseenter', '.h-cart', function(e){
	e.stopImmediatePropagation();
	$('.h-cart .cart-item-box').removeClass('hide');
	return;

	if($(this).find('ul').length > 0 || $(this).find('.emptycart-wrapper').length > 0) {
		$('.h-cart .cart-item-box').removeClass('hide');
	}
	else {
		load_header_cart();
	}
});

$(document).on('mouseleave', '.h-cart', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	$('.h-cart .cart-item-box').addClass('hide');
});

$(document).on('click', '#checkout-external-link', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	var curr = escape(location.href);
	location.href = $(this).attr('href') + '&referer='+curr;
});


$(document).on('click', '#search-btn', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	if($.trim($('#search').val()) == ''){ return false; }
	if($('.uri-page').val() == 'promos'){
		location.href = root + 'promos?search=' + $('#search').val();
	}else{
		location.href = root + '?search=' + $('#search').val();
	}
});

$(document).on('click', '#tcollection', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	location.href = $(this).attr('data-url');
});

$(document).on('click', '.reveal-modal-promoteproduct-bg', function(e) {
	e.preventDefault(); e.stopImmediatePropagation();
	$(this).hide();
	$('#promoteProductModal').hide().html('').removeClass('noloader').removeClass('collection');

	if($(this).hasClass('ispopup')) {
		$(this).removeClass('ispopup');
	}
	else {
		$('body').removeClass('hidescroll');
	}
});

$(document).on('click', '.promote-this-product', function(e){
	e.preventDefault(); e.stopImmediatePropagation();

	if($.cookies.get("uid") == null || $.cookies.get("token") == null || $.cookies.get("uid") == 0) {
		signin_modal();
		if($(this).attr('ispopup')) {
			History.pushState(null, 'Shopstyx', '/');
		}
		return;
	}

	$('.reveal-modal-promoteproduct-bg').show();
	if($(this).attr('ispopup')) {
		$('.reveal-modal-promoteproduct-bg').addClass('ispopup')
	}

	$('body').addClass('hidescroll');
	$('#promoteProductModal').show().centerized();

	$.ajax({
		type: "POST",
		url: $(this).attr('url'),
		data: {token:$.cookies.get("token"),user_id:$.cookies.get("uid"),isAjax:true,action:'ajax',action_type:'embedform'},
		cache: false,
		success: function(response){
			$('#promoteProductModal').html(response);
			$('#promoteProductModal').addClass('noloader');
		}
	});

	var data = $(this).attr('data');
	var mdata = $.parseJSON(data);
	$('#promoteProductModal').data('data',data);
});

$(document).on('click', '.promote-this', function (e) {
	e.preventDefault(); e.stopImmediatePropagation();

	if($.cookies.get("uid") == null || $.cookies.get("token") == null || $.cookies.get("uid") == 0) {
		signin_modal();
		return;
	}

	$('.reveal-modal-promoteproduct-bg').show();
	$('body').addClass('hidescroll');
	$('#promoteProductModal').addClass('collection').show().centerized();

	$.ajax({
		type: "POST",
		url: $(this).attr('url'),
		data: {token:$.cookies.get("token"),user_id:$.cookies.get("uid"),isAjax:true,action:'ajax',action_type:'embedformcollection'},
		cache: false,
		success: function(response){
			$('#promoteProductModal').html(response);
			$('#promoteProductModal').addClass('noloader');
		}
	});
	
	var data = $(this).attr('data');
	//var mdata = $.parseJSON(data);
	//console.log(data)
	$('#promoteProductModal').data('data',data);
});

$(document).on('click', '.promote-product-view .p-right .share-wrapper .left-share .share-common', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	var data = {type:$(this).attr('type')};
	var mdata = decodeURIComponent($('#promoteProductModal').data('data').replace(/\+/g, ' '));
	
	mdata = $.parseJSON(mdata);
	data = $.extend(data,mdata);
	//console.log(data)
	promote_product(data);
});

$(document).on("click",".collection-lists li", function(e) {
	e.preventDefault(); e.stopImmediatePropagation();
	var link = $(this).attr('data-url');
	location.href = link;
});

$(document).on('click', '.collection-lists a', function(e) {
	e.preventDefault(); e.stopImmediatePropagation();
	window.open($(this).attr('href'),'_newtab');
});

$(document).on('click', '.login-follow', function(e) {
	e.preventDefault(); e.stopImmediatePropagation();
	$('#sign-in').trigger('click');
});

// follow/unfollow shop
$(document).on('click', '.follow-shop-btn', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	if($.cookies.get("uid") == null || $.cookies.get("token") == null || $.cookies.get("uid") == 0) {
		signin_modal();
		return;
	}
	
	var el = $(this);

	// follow
	if(el.hasClass('follow')) {
		el.removeClass('follow').addClass('unfollow').removeClass('wsbgcolor2').removeClass('wscolor3').addClass('wsbgcolor1').addClass('wscolor2').html('Followed' + '<span class="wsbgcolor3 wscolor2">Unfollow</span>');
	}
	// unfollow
	else {
		el.removeClass('unfollow').addClass('follow').removeClass('wsbgcolor1').removeClass('wscolor2').addClass('wsbgcolor2').addClass('wscolor3').html('Follow');
	}
	
	var data = {token:$.cookies.get("token"),user_id:$.cookies.get("uid"),isAjax:true,action:'ajax',action_type:'followshop'};
	$.ajax({
		type: "POST",
		url: "/",
		data: data,
		cache: false,
		success: function(response){
			var data = $.parseJSON(response);
			var ufollower = (data.total_user_followed > 1)? data.total_user_followed + ' Followers' : data.total_user_followed + ' Follower'; 
			if(parseInt(data.total_user_followed) > 20){
				$('.more-followers').html(ufollower);
			}
			location.href = location.href;
		}
	});
});


// follow/unfollow collection
$(document).on('click', '.collection-lists li .btn-follow-collection, .follow-this-collection', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	if($.cookies.get("uid") == null || $.cookies.get("token") == null || $.cookies.get("uid") == 0) {
		signin_modal();
		return;
	}
	var el = $(this);
	var data = $.parseJSON(el.attr('data'));
	var username = el.attr('username');

	// follow
	if(el.hasClass('follow')) {
		el.removeClass('follow');
		el.addClass('follow-active');
		if(el.hasClass('btn-follow-collection')) {
			el.parent().append('<span class="followers-coll wscolor3"></span>');
			el.addClass('unfollow').find('.follow-stat').html('Followed');
		}
		else {
			el.addClass('unfollow').html('Followed' + '<span class="unfollow-btn wsbgcolor1 wscolor2">Unfollow</span>');
		}
	}
	// unfollow
	else {
		el.removeClass('unfollow');
		el.removeClass('follow-active');
		if(el.hasClass('btn-follow-collection')) {
			el.parent().find('.followers-coll').remove();
			el.addClass('follow').find('.follow-stat').html('Follow');
		}
		else {
			el.addClass('follow').html('Follow' + '<span class="unfollow-btn wsbgcolor1 wscolor2">Unfollow</span>');
		}
	}
	
	$.extend(data,{token:$.cookies.get("token"),user_id:$.cookies.get("uid"),isAjax:true,action:'ajax',action_type:'follow_collection'});
	$.ajax({
		type: "POST",
		url: "/",
		data: data,
		cache: false,
		success: function(response){
			var data = $.parseJSON(response);
			var ufollower = (data.total_user_followed > 1)? data.total_user_followed + ' Followers' : data.total_user_followed + ' Follower'; 
			$('.no-followers').html(ufollower);
			// el.parent().find('.followers-coll').html(ufollower);
			location.href = location.href;
			//if(data == "" || $.isEmptyObject(data)) return;
		}
	});
	
});

$(document).on('click', '.follow-box', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	return false;
});

/* begin stick product */
/*
	$(document).on('touchstart, click', '.new-collection-btn', function(){
		$('._collections').addClass('hide');
		$('._add-collections').removeClass('hide');
	});
	
	$(document).on('touchstart, click', '.cancel-add', function(){
		$('._add-collections').addClass('hide');
		$('._collections').removeClass('hide');
	});
	
	
	$(document).on('touchstart, click', '.collection-box', function(){
		$('.collection-box').removeClass('active');
		$(this).addClass('active');
	});
	
	$(document).on('click', '#addToCollectionModal', function(){
		$(this).addClass('hide');
	});
	
	$(document).on('click', '#addToCollectionModal .modalDialog-content', function(e){
		e.stopImmediatePropagation();
	});
	
	$(document).on('click', '#assign-to-collection', function(e){
		e.preventDefault(); e.stopImmediatePropagation();
		if($.cookies.get("uid") == null || $.cookies.get("token") == null || $.cookies.get("uid") == 0) {
			signin_modal();
			return;
		}
		
		var $form = $(this),
			url = main_url + 'stick_products?action=assign_to_collection_form';
			form_data = {};
		form_data.isAjax = true;
		form_data.token = typeof $.cookies.get("token") !== "undefined" ? $.cookies.get("token") : "";
		form_data.user_id = typeof $.cookies.get("uid") !== "undefined" ? $.cookies.get("uid") : "";
		
		var data_attr = $.parseJSON($(this).attr('data'));
		$.extend(form_data,data_attr);
		
		$('.add-to-collection-modal').removeClass('hide');
		$('#addToCollectionModal .stick-loader').removeClass('hide');
		$('#addToCollectionModal .stick-content-b').html('');
		$.ajax({
			type: "POST",
			data: form_data,
			url: url,
			cache: false,
			success: function(data){
				$('#addToCollectionModal .stick-content-b').html(data);
				$('#addToCollectionModal .stick-loader').addClass('hide');
			}
		});
	});
	
	$(document).on('touchstart, click', '.assign-to-collection-btn', function(e){
		e.preventDefault(); e.stopImmediatePropagation();
		$('.no-collection').html('');
		$('.stick-product-description').removeClass('border-red');
		$('.collection-input-name').removeClass('border-red');
		
		if(!$('._add-collections').hasClass('hide')){
			var collect_name = $.trim($('.collection-input-name').val());
			var collection_desc = $.trim($('.collection-description').val());
			if(collect_name == ''){
				$('.collection-input-name').addClass('border-red');
				return false;
			}
			$('.collection-box').removeClass('active');
			$('.collection-box-new').remove();
			$('.cw-a-content ul').append('<li class="collection-box collection-box-new new active"><div class="collection-txt">' + collect_name + '</div><input type="hidden" name="collection_id" value="new" /><input type="hidden" name="collection_name" value="' + collect_name + '" /><textarea class="hide" name="collection_description">' + collection_desc + '</textarea></li>');
		}
		var page = main_url + 'stick_products?action=assign_to_collection',
			$this = $(this),
			form_data = {};
		form_data.isAjax = true;
		form_data.token = typeof $.cookies.get("token") !== "undefined" ? $.cookies.get("token") : "";
		form_data.user_id = typeof $.cookies.get("uid") !== "undefined" ? $.cookies.get("uid") : "";
		form_data.product_id = $.trim($('.stick-product-product_id').val());
		form_data.product_user_id = $.trim($('.stick-product-prod_user_id').val());
		form_data.product_store_id = $.trim($('.stick-product-prod_store_id').val());
		form_data.product_collection_id = $.trim($('.stick-product-collection_id').val());
		form_data.collection_name_selected = $.trim($('.cw-a-content ul li.active input[name="collection_name"]').val());
		form_data.collection_id_selected = $.trim($('.cw-a-content ul li.active input[name="collection_id"]').val());
		
		var collection_id = [],  
			collection_name = [],
			collection_description = [];
		
		$('.cw-a-content ul li.new input[name="collection_id"]').each(function(){
			collection_id.push($.trim($(this).val())); 
		});
		$('.cw-a-content ul li.new input[name="collection_name"]').each(function(){
			collection_name.push($.trim($(this).val()));
		});
		$('.cw-a-content ul li.new textarea[name="collection_description"]').each(function(){
			collection_description.push($.trim($(this).val()));
		});
		form_data.collection_id = collection_id;
		form_data.collection_name = collection_name;
		form_data.collection_description = collection_description;
		
		var error = false;
		if($('.stick-product-description').hasClass('product-desc-required')){
			if(form_data.product_description == ""){
				$('.stick-product-description').addClass('border-red');
				error = true;
			}
		}
		
		if ($('.cw-a-content ul li.active input[name="collection_id"]').length === 0) {
			$('.no-collection').html('Please add or select collection.');
			error = true;
		}
		
		if(error){
			return false;
		}
		$('#addToCollectionModal .stick-loader .shopstyxloader div').html('SAVING...');
		$('#addToCollectionModal .stick-loader').removeClass('hide');
		$.ajax({
			type: "POST",
			data: form_data,
			url: page,
			cache: false,
			success: function(response){
				var _data = $.parseJSON(response);
				$('#addToCollectionModal .stick-loader').addClass('hide');
				$('#addToCollectionModal .stick-loader .shopstyxloader div').html('LOADING...');
				$('.add-to-collection-modal').addClass('hide');
			}
		});
	});
*/
/* end stick product */

$(document).on('change', '#change-profile-photo', function(e){
	_upload_userpic($(this));
});

function _upload_userpic(input) {
	$('.upic-' + $.cookies.get('uid') + ' .upic-loader-overlay').removeClass('hide');
	$('.upic-' + $.cookies.get('uid') + ' .upic-loader').removeClass('hide');

	var file = input[0].files;
	var form_data = new FormData();

	$.each(file, function(key, value)
	{
		form_data.append('userfile', value);
	});
	
	$.ajax({
		type: 'POST',
		data: form_data,
		cache: false,
		processData: false,
		contentType: false, 
		url: CS.main_url + "admin/account?action=upload&isAjax=true&token="+$.cookies.get('token')+"&user_id="+$.cookies.get('uid'),
		success: function(response){
			var results = $.parseJSON(response);

			if(results.status == "success") {
				$('.upic-' + $.cookies.get('uid') + ' .ipic-thumb').addClass('hide');
				$('.upic-' + $.cookies.get('uid')).css('background-image','url('+results.file+')');
				$('.upic-' + $.cookies.get('uid')).removeClass('image-hide');
			}
			$('.upic-' + $.cookies.get('uid') + ' .upic-loader-overlay').addClass('hide');
			$('.upic-' + $.cookies.get('uid') + ' .upic-loader').addClass('hide');
		}
	});
}

function close_panels() {
	var el = $('.h-log');
		wallet = el.find('.wallet-popup'),
		msg_panel = el.find('.msg-popup').
		notification_panel = el.find('.notification-panel');

	el.find('.icon').removeClass('selected');
	wallet.addClass('hide');
	msg_panel.addClass('hide');
}

function load_wallet() {
	var element = $('.h-log .wallet'),
		container = element.find('.wallet-popup'),
		loader = container.find('.loader'),
		overlay = container.find('.loader-overlay'),	
		ajax_container = container.find('.inner-scroller');
		
	element.addClass('active');
	overlay.removeClass('hide');
	loader.removeClass('hide');
	container.removeClass('hide');

	$.ajax({
		type: "post",
		url: CS.main_url + 'admin/ajax?action=wallet',
		data: get_user_credentials_params(),
		success: function(response){
			overlay.addClass('hide');
			loader.addClass('hide');	
			//console.log(ajax_container)
			ajax_container.html(response);
		}
	});
}

function popUp(URL, data) {
	var day = new Date();
	var id = day.getTime();
	data = "_social_sharing_";
	eval("page" + id + " = window.open(URL, '" + id + "', 'directories=0,toolbar=0,scrollbars=0,location=0,statusbar=0,menubar=0,resizable=0,width=1000,height=430,left=200,top=50');");
}

/*
 *
 */
function promote_product(data) {
	var opts = {type:'fb',
				url:'',
				text:'',
				desc:'',
				image:'',
				current_url:''
				};

	data = (typeof data == "undefined" || $.isEmptyObject(data)) ? {} : data;
	opts = $.extend(opts,data);
	if($.isEmptyObject(opts.desc) || opts.desc == "") {
		opts.desc = opts.text;
	}

	opts.url = encodeURIComponent(opts.url);
	opts.text = encodeURIComponent(opts.text);
	opts.image = encodeURIComponent(opts.image);
	opts.desc = encodeURIComponent(opts.desc);

	console.log(opts);

	switch(opts.type) {
		default:
		case 'fb':
			url = 'https://www.facebook.com/dialog/share?app_id=142470092447374&display=popup&href='+opts.url+'&redirect_uri='+encodeURIComponent(main_url + "go/gotourl?r="+opts.current_url);
			break;
		case 'tw':
			url = 'http://twitter.com/share?count=false&original_referer='+opts.current_url+'&via=shopstyx.com&text='+opts.text+'&url='+opts.url;
			break;
		case 'gplus':
			url = 'https://plus.google.com/share?url='+opts.current_url;
			break;
		case 'linkedin':
			url = 'http://www.linkedin.com/shareArticle?mini=true&url='+opts.url+'&title='+opts.text+'&summary='+opts.desc+'&source=shopstyx.com';
			break;
		case 'pt':
			url = 'http://pinterest.com/pin/create/button/?url='+opts.url+'&media='+opts.image+'&description='+opts.desc;
			break;
		case 'mail':
			url = 'mailto:?subject='+opts.text+' @ Shopstyx&body=Check out this cool product '+opts.current_url;
			break;
	}
	popUp(CS.main_url + "go?r="+escape(url)+'&u='+opts.uid+'&pu='+opts.pu+'&s='+opts.s+'&c='+opts.c+'&p='+opts.p+'&ref=' + current_url);
}

/* infinite scroll */

var t = this;
var defdataopts, scrollopts, dataopts;

function init_scroll_opts(){
	if (!dataopts) dataopts = {};
	if (!scrollopts) scrollopts = {};
	defdataopts = {	user_id: $.cookies.get('uid'),
		token: $.cookies.get('token'),
		isAjax: true,
		body_wrapper: false,
		subcontent_wrapper: false
		};

	//if(typeof iswebstore != "undefined" && iswebstore) {
	if(typeof CS.is_cookies == "undefined" || !CS.is_cookies) {	
		defdataopts.user_id = store.get('user_id');
		defdataopts.token = store.get('token');
	}
}

function load_scroll_opts(vartouse){
	dataopts[vartouse] = {};
	scrollopts[vartouse] = {
		page: 1,
		url: ''
	};
	reset_scroll_data_opts(vartouse);
}

function reset_scroll_page(vartouse){
	scrollopts[vartouse].page = 1;
}

function reset_scroll_data_opts(vartouse){
	dataopts[vartouse] = defdataopts;
}

function extend_scroll_opts(dat,vartouse){
	$.extend(dataopts[vartouse], dat, true);
}

function infinite_scroll(scrolltarget, vstorage, params, urlrequest, whereinsert, iswindow){
	init_scroll_opts();
	load_scroll_opts(vstorage);
	scrollopts[vstorage].url = urlrequest;
	scrollopts[vstorage].iscontent_loaded = false;
	var hascontent = true;
	var iscontent_loaded = false;
	extend_scroll_opts(params,vstorage);
	var loadMoreContent = function() {
		var url;
		scrollopts[vstorage].page++;
		if(scrollopts[vstorage].url.indexOf("?") == -1) {
			url = scrollopts[vstorage].url+'?page='+scrollopts[vstorage].page;
		}
		else {
			url = scrollopts[vstorage].url+'&page='+scrollopts[vstorage].page
		}
		
		$.ajax({
			type: "POST",
			data: dataopts[vstorage],
			url: url,
			cache: false,
			success: function(response){
				scrollopts[vstorage].iscontent_loaded = false;
				if ($.trim(response) != '') {
				  	whereinsert.append(response);
				}else{
					hascontent = false;
				}	
				whereinsert.parent().find('.infinite-scroll-loader').addClass('hide');
			}
		});
	}

	var trigger_scroll = function(){
		//if (hascontent && !scrollopts[vstorage].iscontent_loaded){
		if (hascontent && typeof(scrollopts[vstorage]) != "undefined" && !scrollopts[vstorage].iscontent_loaded){
			whereinsert.parent().find('.infinite-scroll-loader').removeClass('hide');
			var stargheight = (iswindow)?$(document).height():$(scrolltarget)[0].scrollHeight;
	  		if (scrolltarget.scrollTop() + 300  >= stargheight - scrolltarget.height()) {
	  			//console.log('toload '+page);
	  			scrollopts[vstorage].iscontent_loaded = true;
	    		loadMoreContent();
	    		
	  		}
	  	}
	}
	
	scrolltarget.scroll(trigger_scroll);
}

function load_popup_cart_data(data) {
	$.ajax({
			type: "POST",
			url: '/ajax?type=get_header_cart',
			data: {cart_sess:store.get('cart_sess'), forpopup_cart: true, data: data},
			success: function(response){
				$('#addToCartPopup .atc-wrapper').html(response);
				$('.global-cart-loadbox').addClass('hide');
				
				var element = $('#sub-wrapper #header .userNav .navLink.cart'),
					container = element.find('.cart-panel.npops'),
					loader = container.find('.loader'),
					overlay = container.find('.loader-overlay'),	
					ajax_container = container.find('.inner-scroller');


				element.removeClass('active');
				overlay.addClass('hide');
				loader.addClass('hide');
				container.addClass('hide');
			}});
}

function load_header_cart(type) {
	var element = $('.h-cart'),
		container = element.find('.cart-item-box'),
		loader = container.find('.loader'),
		overlay = container.find('.loader-overlay'),
		ajax_container = container.find('.inner-scroller');	

	if(type == "loading") {
		// overlay.removeClass('hide');
		// loader.removeClass('hide');
		// container.removeClass('hide');
	}
	else {
		$.ajax({
			type: "POST",
			url: '/ajax?type=get_header_cart',
			data: {},
			success: function(res){
				ajax_container.html(res);
				overlay.addClass('hide');
				loader.addClass('hide');
			}});
	}
}

function updateWalletSizeContainer() {
	class_or_id = '#header .userNav .wallet-panel .panel-inner-scroller';
	parent_class_or_id = '#header .userNav .wallet-panel'; 
	//var window_height = $('#header .userNav .wallet-panel').height();
	var window_height = $(window).height() - 60;

	$(class_or_id).removeAttr('style');
	$(class_or_id + ' .slimScrollBar, ' + class_or_id +' .slimScrollRail').remove();
	if($(parent_class_or_id + ' .slimScrollDiv').length > 0) {
		$(class_or_id).parent().replaceWith($(class_or_id));
	}

	$(class_or_id).slimScroll({
		height: window_height,
		width: 'auto',
		distance: '4px'
	});
}

function get_cart_total_items() {
	var form_data = {};
	form_data.user_id = $.cookies.get("uid");
	form_data.token = $.cookies.get("token");
	form_data.isAjax = true;
	
	$.ajax({
		url: root + '/ajax?type=get_carttotalitems',
		type:'post',
		data: form_data,
		success: function(data){
			var _data = $.parseJSON(data);
			if(_data.cart_total_item > 0) {
				$('#header .h-cart .item-no').html(_data.cart_total_item).removeClass('hide');
			}
		}
	});
}
