var image_counter = 0,
	total_img = 0,
	$search_text = '';
$(document).ready(function() {
	$(document).foundation();
	
	$(document).on('open', '#stickModal', function (e) {
		e.preventDefault(); e.stopImmediatePropagation();
		$(this).removeClass('stick-product-modal2');
		$(this).removeClass('stick-product-modal3');
		$('.reveal-modal-bg').addClass('reveal-modal-bg-color');
		$(this).centerized();
	});
	
	$(document).on('opened', '#stickModal, #stickAddModal', function (e) {
		$('.reveal-modal-bg').addClass('reveal-modal-bg-color');
	});
	
	$(document).on('opened', '#addSearchModal', function (e) {
		$('.reveal-modal-bg').addClass('reveal-modal-bg-color');
		var top = parseInt($('.add-search-modal').position().top) - 88;
		$('.add-search-modal').css({'top': $('.add-search-modal').position().top});
	});
	
	$(document).on('open', '#stickAddModal, #addToCollectionModal, #nojsCheckoutModal', function (e) {
		$(this).centerized();
	});
	
	$(window).resize(function() {
		$('.add-search-modal .add-product-step1-wrapper').centerized();
		$('.add-search-modal .add-product-step2-wrapper').centerized();
		$('#stickModal').centerized();
		$('#stickAddModal').centerized();
		$('#addSearchModal').centerized();
	});
	
	$(document).on('open', '#addSearchModal', function (e) {
		e.preventDefault(); e.stopImmediatePropagation();
		$('body').addClass('hidescroll');
		$('.reveal-modal-bg').addClass('reveal-modal-bg-color');
		$(this).centerized();
		

		modal_class = '.add-search-modal';
		page = root + 'stick_products?action=add_search';
		$(modal_class + ' .stick-wrapper').html('');
		$(modal_class + ' .stick-loader').removeClass('hide');

		var form_data = {};
		form_data.isAjax = true;
		form_data.search_text = $search_text;
		SSAjax({
			data: form_data,
			url: page,
			success: function(response){
				$(modal_class + ' .stick-wrapper').html(response);
				$(modal_class + ' .stick-loader').addClass('hide');
			}
		});
	});
	
	$(document).on('focus', '.search-add2', function(){
		$(this).val('');
	});
	
	$(document).on('touchstart click', '.trnd-keys li a', function(e){
		e.preventDefault(); e.stopImmediatePropagation();
		$search_text = $.trim($(this).html());
		if($search_text == ''){
			return false;
		}
		modal_class = '.add-search-modal';
		page = root + 'stick_products?action=add_search&result_only=1';
		$(modal_class + ' .bottom-s').html('');
		$(modal_class + ' .stick-loader').removeClass('hide');
		$('.search-add2').val($search_text);
		
		var form_data = {};
		form_data.isAjax = true;
		form_data.search_text = $search_text;
		SSAjax({
			data: form_data,
			url: page,
			success: function(response){
				$(modal_class + ' .bottom-s').html(response);
				$(modal_class + ' .stick-loader').addClass('hide');
			}
		});
	});
	
	$(document).on('submit', '#sengineForm2', function(e){
		e.preventDefault(); e.stopImmediatePropagation();
		$search_text = $.trim($('.search-add2').val());
		if($search_text == ''){
			return false;
		}
		modal_class = '.add-search-modal';
		page = root + 'stick_products?action=add_search&result_only=1';
		$(modal_class + ' .bottom-s').html('');
		$(modal_class + ' .stick-loader').removeClass('hide');
		
		
		var form_data = {};
		form_data.isAjax = true;
		form_data.search_text = $search_text;
		SSAjax({
			data: form_data,
			url: page,
			success: function(response){
				$(modal_class + ' .bottom-s').html(response);
				$(modal_class + ' .stick-loader').addClass('hide');
			}
		});
	});
	
	$(document).on('click', '.add-product-close', function(){
		$('#stickModal').foundation('reveal', 'close');
		$('.add-product-step1-wrapper').addClass('hide');
		$('.add-product-step2-wrapper').addClass('hide');
		$('.add-product-step1-content').html('');
		$('.add-product-step2-content').html('');
		$('.tmp-images').html('');
		$('.product-url').val('');
		$('.reveal-modal-bg').remove();
	});
	$(document).on('click', '.search-product-close', function(){
		$('#addSearchModal').foundation('reveal', 'close');
		//$('.reveal-modal-bg').remove();
		
		$('.reveal-modal-bg').removeClass('reveal-modal-bg-color');
		//$('.stick-wrapper').html('');
		$('.reveal-modal-bg').remove();
		
	});
	$(document).on('click', '.add-search-step1-close', function(){
		$('.search-modals').addClass('hide');
		$('.add-search-step1').addClass('hide');
		$('.add-search-step1-content').html('');
	});
	$(document).on('click', '.add-search-step2-close', function(){
		if($('.add-search-step1').hasClass('hide')){
			$('.search-modals').addClass('hide');
		}
		$('.add-search-step2').addClass('hide');
		$('.add-search-step2-content').html('');
	});
	$(document).on('click', '.add-product-step1-close', function(){
		$('.add-product-step1-wrapper').addClass('hide');
		$('.add-product-step1-content').html('');
	});
	$(document).on('click', '.add-product-step2-close', function(){
		$('.add-product-step2-wrapper').addClass('hide');
		$('.add-product-step2-content').html('');
	});
	
	$(document).on('keyup', '.product-url', function(){
		$('.url-field').removeClass('error');
		$('.add-product-box error2').addClass('hide');
	});
	
	$(document).on('touchstart, click', '._manual-add', function(e){
		e.preventDefault(); e.stopImmediatePropagation();
		var imageList = $('.tmp-images').html(),
			page = root + 'stick_products?action=new_url_step1';
		$('.stick-add-new-product .add-product-step1-wrapper').removeClass('hide');
		var form_data = {};
		form_data.isAjax = true;
		$('.stick-product-modal .stick-loader').removeClass('hide');
		SSAjax({
			data: form_data,
			url: page,
			success: function(result){
				$('.stick-add-new-product .add-product-step1-content').html(result);
				$('.stick-product-modal .stick-loader').addClass('hide');
			}
		});
	});
	
	$(document).on('submit', '#addUrlForm', function(e){
		e.preventDefault(); e.stopImmediatePropagation();
		
		var $form = $(this),
			url = $form.attr( 'action' );
			form_data = {};
		form_data.isAjax = true;
		form_data.url = $.trim($('.product-url').val());
		$('.stick-product-modal .stick-loader').removeClass('hide');
		$('.error2').addClass('hide');
		$('.error3').html('');
		$('.tmp-images').html('');
		$('.add-product-step1-content').html('');
		$('.add-product-step2-content').html('');
		SSAjax({
			data: form_data,
			url: url,
			success: function(response){
				var _data = $.parseJSON(response);
				if(_data == "" || $.isEmptyObject(_data)){
					$('.stick-product-modal .stick-loader').addClass('hide');
					signin_modal();
					return;
				}
				
				if(!_data.status){
					$('.stick-product-modal .stick-loader').addClass('hide');
					$('.error3').html('Please provide a valid url.');
					return;
				}
				var	c = 0,
					image_counter = 0;
				if(_data.url_exist){
					var page = root + 'stick_products?action=url_step2',
						form_data = {};
					
					form_data.prod_user_id = _data.prod_user_id;
					form_data.prod_store_id = _data.prod_store_id;
					form_data.collection_prod_user_id = _data.collection_prod_user_id;
					form_data.collection_prod_store_id = _data.collection_prod_store_id;
					form_data.owner_user_id = _data.user_id;
					form_data.store_id = _data.store_id;
					form_data.collection_id = _data.collection_id;
					form_data.product_id = _data.product_id;
					form_data.product_user_id = _data.product_user_id;
					form_data.current_url = _data.current_url;
					form_data.url_exist = 1;					
						
					form_data.product_site_name = '';
					form_data.product_brand_name = '';
					form_data.product_brand_link = '';
					form_data.external_product_url = '';
					form_data.external_product_canonical_url = '';
					form_data.external_tags = '';
					form_data.product_name = _data.product_name;
					form_data.price = _data.price;
					form_data.currency = _data.currency;
					form_data.currency_symbol = (_data.currency == 'EURO')? '&euro':'$';
					form_data.image = _data.image;
					form_data.product_for = _data.department_id;
					form_data.product_for_name = _data.department_name;
					form_data.color = '';
					
					form_data.isAjax = true;
					$('.stick-product-modal .stick-loader').removeClass('hide');
					$('.stick-add-new-product .add-product-step2-wrapper').removeClass('hide');
					SSAjax({
						data: form_data,
						url: page,
						success: function(response){
							//$('.stick-product-modal .stick-wrapper').html(response);
							$('.stick-add-new-product .add-product-step2-content').html(response);
							$('.stick-product-modal .stick-loader').addClass('hide');
						}
					});
				}
					
				if(! _data.url_exist){	
					if(_data.image != ''){
						//$('.tmp-images').append('<li class="active" style="background-image: url(\' ' + _data.image + ' \')" ><input type="hidden" name="product_image" value="' + _data.image + '" /></li>');
						$('.tmp-images').append('<li style="background-image: url(\' ' + _data.image + ' \')" ><input type="hidden" name="product_image" value="' + _data.image + '" /></li>');
						image_counter++; c++;
					}
					total_img = _data.images.length - 1;
					$.each(_data.images, function(i,item){
						var newImg = new Image();
						newImg.src = item;
						var height = newImg.height,
							width = newImg.width;
							
						$(newImg).load(function(){
							if(parseInt(newImg.width) >= 250 || parseInt(newImg.height) >= 250){
								if(_data.image != item){
									// if( c == 0 ){
										// $('.tmp-images').append('<li class="active" style="background-image: url(\' ' + item + ' \')" ><input type="hidden" name="product_image" value="' + item + '" /></li>');
									// }
									// if( c > 0 ){
										// $('.tmp-images').append('<li style="background-image: url(\' ' + item + ' \')" ><input type="hidden" name="product_image" value="' + item + '" /></li>');
									// }
									$('.tmp-images').append('<li style="background-image: url(\' ' + item + ' \')" ><input type="hidden" name="product_image" value="' + item + '" /></li>');
								}
								image_counter++; c++;
								if(image_counter > 7){ total_img = 1; }
							}
							total_img--;
						});
					});
					
					function checkImg( ) {
						if(total_img <= 1){
							if(image_counter > 0){
								var imageList = $('.tmp-images').html(),
									page = root + 'stick_products?action=new_url_step1';
								//$('.stick-product-modal .stick-wrapper').html('');
								$('.stick-add-new-product .add-product-step1-wrapper').removeClass('hide');
								var form_data2 = {};
								form_data2.isAjax = true;
								SSAjax({
									data: form_data2,
									url: page,
									success: function(result){
										//$('.stick-product-modal .stick-wrapper').html(result);
										$('.stick-add-new-product .add-product-step1-content').html(result);
										$('.stick-product-site_name').val(_data.site_name);
										$('.stick-product-brand_name').val(_data.brand_name);
										$('.stick-product-brand_link').val(_data.brand_link);
										$('.stick-product-url').val(_data.web_url);
										$('.stick-product-canonical-url').val(_data.canonical_url);
										$('.stick-product-tags').val(_data.meta_keywords);
										$('.stick-product-name').val(_data.product_name);
										$('.stick-product-price').val(_data.price);
										$('.choose-img-content ul').html(imageList);
										$('.stick-product-modal .stick-loader').addClass('hide');
									}
								});
							}
							if(image_counter == 0){
								$('.error2').removeClass('hide');
								$('.stick-product-modal .stick-loader').addClass('hide');
							}
							image_counter = 0;
						}else{
							setTimeout( checkImg, 30 );
						}
					}
					checkImg();
				}
			}
		});
		
		return false;
	});
	
	
	
	$(document).on('click', '.stick-new-product, .stick-a-product', function(e){
		e.preventDefault(); e.stopImmediatePropagation();
		if(typeof CS.iswebstore == "undefined") {
			var uid = store.get('user_id');
			var token = store.get('token');
		}
		else {
			var uid = $.cookies.get("uid");
			var token = $.cookies.get("token");		
		}
	
		if(uid == 0 || typeof(uid) == "undefined" || typeof(token) == "undefined") {
			signin_modal();
			if($(this).attr('ispopup')) {
				History.pushState(null, 'Shopstyx', '/');
			}
			return;
		}
		var use_modal = false;
		if($(this).hasClass('stick-a-product')){
			$('#stickAddModal').foundation('reveal', 'open', {
				animation_speed: 0
			});
			$('.stick-product-add-modal .stick-loader').removeClass('hide');
			use_modal = true;
		}
		
		var $form = $(this),
			url = main_url + 'stick-products?action=get_external_product';
			form_data = {};
		form_data.isAjax = true;
		form_data.url = $.trim($(this).attr('data-url'));
		form_data.pid = $(this).attr('data-pid');
		form_data.puid = $(this).attr('data-puid');
		$('.add-search-modal .stick-loader').removeClass('hide');
		$('.error2').addClass('hide');
		$('.tmp-images').html('');
		$('.add-product-step1-content').html('');
		$('.add-product-step2-content').html('');
		SSAjax({
			data: form_data,
			url: url,
			success: function(response){
				var _data = $.parseJSON(response);
				if(_data == "" || $.isEmptyObject(_data)){
					$('#stickModal .stick-loader').addClass('hide');
					signin_modal();
					return;
				}
				
				if(!_data.status){
					$('#stickModal .stick-loader').addClass('hide');
					return;
				}
				var	c = 0,
					image_counter = 0;
				if(_data.url_exist){
					var page = main_url + 'stick_products?action=url_step2',
						form_data = {};
					
					form_data.prod_user_id = _data.prod_user_id;
					form_data.prod_store_id = _data.prod_store_id;
					form_data.collection_prod_user_id = _data.collection_prod_user_id;
					form_data.collection_prod_store_id = _data.collection_prod_store_id;
					form_data.owner_user_id = _data.user_id;
					form_data.store_id = _data.store_id;
					form_data.collection_id = _data.collection_id;
					form_data.product_id = _data.product_id;
					form_data.product_user_id = _data.product_user_id;
					form_data.current_url = _data.current_url;
					form_data.url_exist = 1;					
						
					form_data.product_site_name = '';
					form_data.product_brand_name = '';
					form_data.product_brand_link = '';
					form_data.external_product_url = '';
					form_data.external_product_canonical_url = '';
					form_data.external_tags = '';
					form_data.product_name = _data.product_name;
					form_data.price = _data.price;
					form_data.currency = _data.currency;
					form_data.currency_symbol = (_data.currency == 'EURO')? '&euro':'$';
					form_data.image = _data.image;
					form_data.product_for = _data.department_id;
					form_data.product_for_name = _data.department_name;
					form_data.color = '';
					
					form_data.isAjax = true;
					$('.add-search-modal .stick-loader').removeClass('hide');
					$('.add-search-modal .add-product-step2-wrapper').removeClass('hide');
					$('.add-search-modal .add-product-step2-wrapper').centerized();
					$('.bg-trans').removeClass('hide');
					if(use_modal){
						$('.stick-product-add-modal').css({'max-width':'880px'});
					}
					SSAjax({
						data: form_data,
						url: page,
						success: function(response){
							if(use_modal){
								$('.stick-product-add-modal .stick-wrapper').html(response);
								$('.stick-product-add-modal .stick-loader').addClass('hide');
								$('.stick-product-add-modal .posted-lbl').addClass('hide');
								$('.stick-product-add-modal .back-to-step-1').addClass('hide');
							}else{
								$('.add-search-modal .add-product-step2-content').html(response);
								$('.add-search-modal .stick-loader').addClass('hide');
								$('.posted-lbl').addClass('hide');
							}
						}
					});
				}
					
				if(! _data.url_exist){
					if(_data.image != ''){
						//$('.tmp-images').append('<li class="active" style="background-image: url(\' ' + _data.image + ' \')" ><input type="hidden" name="product_image" value="' + _data.image + '" /></li>');
						$('.tmp-images').append('<li style="background-image: url(\' ' + _data.image + ' \')" ><input type="hidden" name="product_image" value="' + _data.image + '" /></li>');
						image_counter++; c++;
					}
					total_img = _data.images.length - 1;
					$.each(_data.images, function(i,item){
						var newImg = new Image();
						newImg.src = item;
						var height = newImg.height,
							width = newImg.width;
							
						$(newImg).load(function(){
							if(parseInt(newImg.width) >= 250 || parseInt(newImg.height) >= 250){
								if(_data.image != item){
									// if( c == 0 ){
										// $('.tmp-images').append('<li class="active" style="background-image: url(\' ' + item + ' \')" ><input type="hidden" name="product_image" value="' + item + '" /></li>');
									// }
									// if( c > 0 ){
										// $('.tmp-images').append('<li style="background-image: url(\' ' + item + ' \')" ><input type="hidden" name="product_image" value="' + item + '" /></li>');
									// }
									$('.tmp-images').append('<li style="background-image: url(\' ' + item + ' \')" ><input type="hidden" name="product_image" value="' + item + '" /></li>');
								}
								image_counter++; c++;
								if(image_counter > 7){ total_img = 1; }
							}
							total_img--;
						});
					});
					
					function checkImg( ) {
						if(total_img <= 1){
							if(image_counter > 0){
								var imageList = $('.tmp-images').html(),
									page = main_url + 'stick_products?action=new_url_step1';
								$('.add-search-modal .add-product-step1-wrapper').removeClass('hide');
								$('.add-search-modal .add-product-step1-wrapper').centerized();
								$('.bg-trans').removeClass('hide');
								var form_data2 = {};
								form_data2.isAjax = true;
								SSAjax({
									data: form_data2,
									url: page,
									success: function(result){
										$('.add-search-modal .add-product-step1-content').html(result);
										$('.stick-product-site_name').val(_data.site_name);
										$('.stick-product-brand_name').val(_data.brand_name);
										$('.stick-product-brand_link').val(_data.brand_link);
										$('.stick-product-url').val(_data.web_url);
										$('.stick-product-canonical-url').val(_data.canonical_url);
										$('.stick-product-tags').val(_data.meta_keywords);
										$('.stick-product-name').val(_data.product_name);
										$('.stick-product-price').val(_data.price);
										$('.choose-img-content ul').html(imageList);
										$('.add-search-modal .stick-loader').addClass('hide');
									}
								});
							}
							if(image_counter == 0){
								$('.error2').removeClass('hide');
								$('.add-search-modal .stick-loader').addClass('hide');
							}
							image_counter = 0;
						}else{
							setTimeout( checkImg, 30 );
						}
					}
					checkImg();
				}
			}
		});
		
		return false;
	});
	
	
	$(document).on('touchstart, click', '.stick-for ul li', function(){
		if(! $(this).hasClass('more')){
			$('.stick-for ul li').removeClass('selected');
			$('.stick-for ul li').removeClass('active');
			$(this).addClass('selected');
			$(this).addClass('active');
		}
	});
	
	$(document).on('touchstart, click', '.stick-for .more ul li', function(){
		$('.stick-for .more ul li').removeClass('selected');
		$('.stick-for ul li').removeClass('selected');
		$('.stick-for .more ul li').removeClass('active');
		$('.stick-for ul li').removeClass('active');
		$('.stick-for ul li.more').addClass('selected');
		$(this).addClass('selected');
		$(this).addClass('active');
	});
	
	$(document).on('touchstart, click', '.add-color span', function(){
		$('.add-color span').removeClass('selected');
		$(this).addClass('selected');
	});
	
	$(document).on('touchstart, click', '.back-to-step-1', function(){
		$('.add-product-step1-wrapper').addClass('hide');
		$('.add-product-step2-wrapper').addClass('hide');
		$('.bg-trans').addClass('hide');
	});
	$(document).on('touchstart, click', '.back-to-step-2', function(){
		$('.add-product-step1-wrapper').removeClass('hide');
		$('.add-product-step2-wrapper').addClass('hide');
	});
	
	$(document).on('touchstart, click', '.choose-img-content ul li', function(){
		$('.choose-img-content ul li').removeClass('active');
		$(this).addClass('active');
	});
	
	$(document).on('touchstart, click', '.add-search-modal .stick-add-btn', function(e){
		e.preventDefault(); e.stopImmediatePropagation();
		add_top_shopstyx($(this), '.add-search-modal');
	});
	$(document).on('touchstart, click', '.stick-add-new-product .stick-add-btn', function(e){
		e.preventDefault(); e.stopImmediatePropagation();
		add_top_shopstyx($(this), '.stick-add-new-product');
	});
	
	$(document).on('touchstart, click', '.new-collection-btn', function(){
		$('.add-this-arrow2').addClass('hide');
		$('._collections').addClass('hide');
		$('.collection-wrapper-b').removeClass('hide');
		$('.add-to-collection-modal .add-this-arrow, .add-to-collection-modal .add-this-lbl').addClass('hide');
		if($.trim($('.stick-product-description').val()) == ''){
			$('.save-collection-btn').html('Create Collection');
		}else{
			$('.save-collection-btn').html('Save to Collection');
		}
		$('._add-collections').removeClass('hide');
	});
	
	$(document).on('touchstart, click', '.cancel-add', function(){
		$('.add-this-arrow2').removeClass('hide');
		$('.collection-wrapper-b').addClass('hide');
		$('.add-to-collection-modal .add-this-arrow, .add-to-collection-modal .add-this-lbl').removeClass('hide');
		$('._add-collections').addClass('hide');
		$('.save-collection-btn').html('Save to Collection');
		$('._collections').removeClass('hide');
	});
	
	
	$(document).on('touchstart, click', '.collection-box', function(){
		$('.collection-box').removeClass('active');
		$(this).addClass('active');
	});
	
	$(document).on('touchstart, click', '.save-collection-btn, .sengine-save-collection-btn, #stickModal .collection-box', function(e){
		e.preventDefault(); e.stopImmediatePropagation();
		$('.other-error').html('');
		$('.no-collection').html('');
		$('.stick-product-description').removeClass('border-red');
		$('.collection-input-name').removeClass('border-red');
		$('.err-desc').addClass('hide');
		
		if(!$('._add-collections').hasClass('hide')){
			var collect_name = $.trim($('.collection-input-name').val());
			var collection_desc = $.trim($('.collection-description').val());
			if(collect_name == ''){
				$('.collection-input-name').addClass('border-red');
				return false;
			}
			
			if($('.stick-product-description').hasClass('product-desc-required')){
				if($.trim($('.stick-product-description').val()) == ""){
					$('.stick-product-description').addClass('border-red');
					$('.err-desc').removeClass('hide');
					$('#stickModal .cancel-add').trigger('click');
					return false;
				}else{
					$('.collection-box').removeClass('active');
					$('.collection-box-new').remove();
					$('#stickModal .cw-a-content ul li.collection-add-btn').after('<li class="collection-box collection-box-new new active"><div class="collection-txt color11">' + collect_name + '</div><input type="hidden" name="collection_id" value="new" /><input type="hidden" name="collection_name" value="' + collect_name + '" /><textarea class="hide" name="collection_description">' + collection_desc + '</textarea></li>');
				}
			}else{
				$('.collection-box').removeClass('active');
				$('.collection-box-new').remove();
				$('#stickModal .cw-a-content ul li.collection-add-btn').after('<li class="collection-box collection-box-new new active"><div class="collection-txt color11">' + collect_name + '</div><input type="hidden" name="collection_id" value="new" /><input type="hidden" name="collection_name" value="' + collect_name + '" /><textarea class="hide" name="collection_description">' + collection_desc + '</textarea></li>');
			}
		
			
		}
		var page = root + 'stick_products?action=save_collection',
			$this = $(this),
			form_data = {};
		form_data.prod_user_id = $.trim($('.stick-product-prod_user_id').val());
		form_data.prod_store_id = $.trim($('.stick-product-prod_store_id').val());
		form_data.collection_prod_user_id = $.trim($('.stick-product-collection_prod_user_id').val());
		form_data.collection_prod_store_id = $.trim($('.stick-product-collection_prod_store_id').val());
		form_data.owner_user_id = $.trim($('.stick-product-owner_user_id').val());
		form_data.store_id = $.trim($('.stick-product-store_id').val());
		form_data.post_collection_id = $.trim($('.stick-product-collection_id').val());
		form_data.product_id = $.trim($('.stick-product-product_id').val());
		form_data.product_user_id = $.trim($('.stick-product-product_user_id').val());
		form_data.current_url = $.trim($('.stick-product-current_url').val());
		form_data.product_site_name = $.trim($('.stick-product-site_name2').val());
		form_data.product_brand_name = $.trim($('.stick-product-brand_name2').val());
		form_data.product_brand_link = $.trim($('.stick-product-brand_link2').val());
		form_data.external_product_url = $.trim($('.stick-product-url2').val());
		form_data.external_product_canonical_url = $.trim($('.stick-product-canonical-url2').val());
		form_data.product_name = $.trim($('.stick-product-name2').val());
		form_data.price = $.trim($('.stick-product-price2').val());
		form_data.currency_symbol = $( ".stick-product-currency_symbol2" ).val();
		form_data.currency = $('.stick-product-currency2').val();
		form_data.image = $('.stick-product-image2').val();
		form_data.product_for = $('.stick-product-product-for2').val();
		form_data.color = $('.stick-product-color2').val();
		form_data.external_tags = $.trim($('.stick-product-external-tags2').val());
		form_data.product_keyword = $.trim($('.stick-product-keyword2').val());
		form_data.product_description = $.trim($('.stick-product-description').val());
		form_data.collection_name_selected = $.trim($('.cw-a-content ul li.active input[name="collection_name"]').val());
		form_data.collection_id_selected = $.trim($('.cw-a-content ul li.active input[name="collection_id"]').val());
		form_data.collection_url = $.trim($('.cw-a-content ul li.active').attr('data-coll-url'));
		form_data.added_first = $('.stick-product-exist2').val();
		form_data.isAjax = true;
		
		var collection_id = [],  
			collection_name = [],
			collection_description = [];
		
		$('.cw-a-content ul li.new input[name="collection_id"]').each(function(){
			collection_id.push($.trim($(this).val())); 
		});
		$('.cw-a-content ul li.new input[name="collection_name"]').each(function(){
			collection_name.push($.trim($(this).val()));
		});
		$('.cw-a-content ul li.new textarea[name="collection_description"]').each(function(){
			collection_description.push($.trim($(this).val()));
		});
		form_data.collection_id = collection_id;
		form_data.collection_name = collection_name;
		form_data.collection_description = collection_description;
		
		var error = false;
		if($('.stick-product-description').hasClass('product-desc-required')){
			if(form_data.product_description == ""){
				$('.stick-product-description').addClass('border-red');
				$('.err-desc').removeClass('hide');
				$('#stickModal .cancel-add').trigger('click');
				error = true;
			}
		}
		
		if(error){
			$('.cw-a-content ul li').removeClass('active');
			return false;
		}
		$('.stick-product-modal .add-product-step-loader .shopstyxloader div').html('Adding product to collection...');
		$('.stick-product-modal3 .stick-loader .shopstyxloader div').html('Adding product to collection...');
		$('.stick-product-modal .add-product-step-loader').removeClass('hide');
		$('.stick-product-modal3 .stick-loader').removeClass('hide');
		$('.bg-trans').removeClass('hide');
		$('.add-search-modal .stick-loader').removeClass('hide');
		SSAjax({
			data: form_data,
			url: page,
			success: function(response){
				var _data = $.parseJSON(response);
				if(_data == "" || $.isEmptyObject(_data)){
					$('.stick-product-modal .add-product-step-loader').addClass('hide');
					signin_modal();
					return;
				}
				// if(!_data.result){
					// $('.other-error').html(_data.msg);
					// $('.stick-product-modal .add-product-step-loader').addClass('hide');
					// return;
				// }
				if($this.hasClass('sengine-save-collection-btn')){
					$('.stick-product-modal3 .stick-loader').addClass('hide');
					$('.stick-product-modal3 .stick-loader .shopstyxloader div').html('LOADING...');
					$('.stick-product-modal3 .search-modals').addClass('hide');
					$('.stick-product-modal3 .add-search-step2').addClass('hide');
					$('.bg-trans').addClass('hide');
					$('.add-search-modal .stick-loader').addClass('hide');
				}else{
					$('.bg-trans').addClass('hide');
					$('.add-search-modal .stick-loader').addClass('hide');
					$('.stick-product-modal .add-product-step-loader').addClass('hide');
					$('.stick-product-modal .add-product-step-loader .shopstyxloader div').html('LOADING...');
				}
				
				$('.stick-product-prod_user_id').val(_data.p.prod_user_id);
				$('.stick-product-prod_store_id').val(_data.p.prod_store_id);
				$('.stick-product-collection_prod_user_id').val(_data.p.collection_prod_user_id);
				$('.stick-product-collection_prod_store_id').val(_data.p.collection_prod_store_id);
				$('.stick-product-owner_user_id').val(_data.p.user_id);
				$('.stick-product-store_id').val(_data.p.store_id);
				$('.stick-product-collection_id').val(_data.p.collection_id);
				$('.stick-product-product_id').val(_data.p.product_id);
				$('.stick-product-product_user_id').val(_data.p.product_user_id);
				$('.stick-product-current_url').val(_data.p.current_url);
				$('.stick-product-exist2').val(0);
				
				$('.save-to-coll-step').addClass('hide');
				$('.added-to-coll-step').removeClass('hide');
				$('.collection-box.active').attr('data-coll-url', _data.collection_url);
				$('.collection-box.active').find( 'input[name="collection_id"]' ).val(_data.collection_id);
				$('.collection-box').removeClass('new').removeClass('collection-box-new').removeClass('active');
				$('.add-to-collection-modal .add-this-arrow, .add-to-collection-modal .add-this-lbl').removeClass('hide');
				$('.add-to-collection-modal .add-this-arrow').addClass('top31');
				$('.added-to-coll-step .coll-url').attr('href', main_url + _data.username + '/collections/' + _data.collection_url).html(form_data.collection_name_selected);
				
				var data = _data.sharedata;
				$('#stickModal').data('data',data);
				$('.added-to-coll-step .embed-wrapper .embed-text').val('<script>(function(){var s=document.createElement(\'script\');s.src = \'//shopstyx.com/cdn/js/v3/embed.js\';s.async = true;document.body.appendChild(s);}());</script><div data-type="embedProduct" data-url="' + _data.product_url + '" data-width="360" data-height="486" class="data-shopstyx-product"></div>');
				if(typeof CS.iswebstore != "undefined") {
					$('.added-to-coll-step .coll-url').attr('target', '_blank');
				}
				
				// if(_data.new_product){
					// location.href = _data.url;
				// }else{
					// $('#stickModal').foundation('reveal', 'close');
				// }
				
			}
		});
	});
	
	$(document).on('touchstart, click', '#stickModal .added-to-coll-step .right-head .close', function(){
		$('#stickModal').foundation('reveal', 'close');
	});
	
	$(document).on('touchstart, click', '.assign-to-collection-btn, #addToCollectionModal .collection-box', function(e){
		e.preventDefault(); e.stopImmediatePropagation();
		$('.no-collection').html('');
		$('.stick-product-description').removeClass('border-red');
		$('.collection-input-name').removeClass('border-red');
		
		if(!$('._add-collections').hasClass('hide')){
			var collect_name = $.trim($('.collection-input-name').val());
			var collection_desc = $.trim($('.collection-description').val());
			if(collect_name == ''){
				$('.collection-input-name').addClass('border-red');
				return false;
			}
			$('.collection-box').removeClass('active');
			$('.collection-box-new').remove();
			$('.cw-a-content ul li.collection-add-btn').after('<li class="collection-box collection-box-new new active"><div class="collection-txt color11">' + collect_name + '</div><input type="hidden" name="collection_id" value="new" /><input type="hidden" name="collection_name" value="' + collect_name + '" /><textarea class="hide" name="collection_description">' + collection_desc + '</textarea></li>');
		}
		var page = main_url + 'stick_products?action=assign_to_collection',
			$this = $(this),
			form_data = {};
		form_data.isAjax = true;
		form_data.product_id = $.trim($('.stick-product-product_id').val());
		form_data.product_user_id = $.trim($('.stick-product-prod_user_id').val());
		form_data.product_store_id = $.trim($('.stick-product-prod_store_id').val());
		form_data.product_collection_id = $.trim($('.stick-product-collection_id').val());
		form_data.collection_name_selected = $.trim($('.cw-a-content ul li.active input[name="collection_name"]').val());
		form_data.collection_id_selected = $.trim($('.cw-a-content ul li.active input[name="collection_id"]').val());
		form_data.collection_url = $.trim($('.cw-a-content ul li.active').attr('data-coll-url'));
		
		var collection_id = [],  
			collection_name = [],
			collection_description = [];
		
		$('.cw-a-content ul li.new input[name="collection_id"]').each(function(){
			collection_id.push($.trim($(this).val())); 
		});
		$('.cw-a-content ul li.new input[name="collection_name"]').each(function(){
			collection_name.push($.trim($(this).val()));
		});
		$('.cw-a-content ul li.new textarea[name="collection_description"]').each(function(){
			collection_description.push($.trim($(this).val()));
		});
		form_data.collection_id = collection_id;
		form_data.collection_name = collection_name;
		form_data.collection_description = collection_description;
		
		var error = false;
		if($('.stick-product-description').hasClass('product-desc-required')){
			if(form_data.product_description == ""){
				$('.stick-product-description').addClass('border-red');
				error = true;
			}
		}
		
		if ($('.cw-a-content ul li.active input[name="collection_id"]').length === 0) {
			$('.no-collection').html('Please add or select collection.');
			error = true;
		}
		
		if(error){
			return false;
		}
		$('#addToCollectionModal .stick-loader .shopstyxloader div').html('Adding product to collection...');
		$('#addToCollectionModal .stick-loader').removeClass('hide');
		SSAjax({
			data: form_data,
			url: page,
			success: function(response){
				var _data = $.parseJSON(response);
				$('#addToCollectionModal .stick-loader').addClass('hide');
				$('#addToCollectionModal .stick-loader .shopstyxloader div').html('LOADING...');
				$('.save-to-coll-step').addClass('hide');
				$('.added-to-coll-step').removeClass('hide');
				$('.collection-box.active').attr('data-coll-url', _data.collection_url);
				$('.collection-box.active').find( 'input[name="collection_id"]' ).val(_data.collection_id);
				$('.collection-box').removeClass('new').removeClass('collection-box-new').removeClass('active');
				$('.add-to-collection-modal .add-this-arrow, .add-to-collection-modal .add-this-lbl').removeClass('hide');
				$('.add-to-collection-modal .add-this-arrow').addClass('top31');
				$('.added-to-coll-step .coll-url').attr('href', main_url + _data.username + '/collections/' + _data.collection_url).html(form_data.collection_name_selected);
				$('#addToCollectionModal .posted-lbl2').addClass('hide');
				var data = _data.sharedata;
				$('#addToCollectionModal').data('data',data);
				$('.added-to-coll-step .embed-wrapper .embed-text').val('<script>(function(){var s=document.createElement(\'script\');s.src = \'//shopstyx.com/cdn/js/v3/embed.js\';s.async = true;document.body.appendChild(s);}());</script><div data-type="embedProduct" data-url="' + _data.product_url + '" data-width="360" data-height="486" class="data-shopstyx-product"></div>');
				if(parseInt(_data.type) < 4){
					$('#addToCollectionModal .added-to-coll-step .right-promote').removeClass('hide');
					$('#addToCollectionModal .added-to-coll-step .promote-share-prod').addClass('for-intr-prod');
					$('#addToCollectionModal .added-to-coll-step .promote-share-prod').removeClass('for-extr-prod');
					$('#addToCollectionModal .added-to-coll-step .promote-share-prod .total-share').html(_data.total_share);
				}else{
					$('#addToCollectionModal .added-to-coll-step .right-promote').addClass('hide');
					$('#addToCollectionModal .added-to-coll-step .promote-share-prod').addClass('for-extr-prod');
					$('#addToCollectionModal .added-to-coll-step .promote-share-prod').removeClass('for-intr-prod');
				}
				if(typeof CS.iswebstore != "undefined") {
					$('.added-to-coll-step .coll-url').attr('target', '_blank');
				}
			}
		});
	});
	
	$(document).on('touchstart, click', '.added-to-coll-step .back', function(){
		$('.added-to-coll-step').addClass('hide');
		$('.save-to-coll-step').removeClass('hide');
		$('#addToCollectionModal .posted-lbl2').removeClass('hide');
		$('.collection-wrapper-b').addClass('hide');
		$('.add-to-collection-modal .add-this-arrow, .add-to-collection-modal .add-this-lbl').removeClass('hide');
		$('.add-to-collection-modal .add-this-arrow').removeClass('top31');
		$('._add-collections').addClass('hide');
		$('.save-collection-btn').html('Save to Collection');
		$('._collections').removeClass('hide');
	});
	$(document).on('touchstart, click', '.added-to-coll-step .close', function(){
		$('.add-to-collection-modal').addClass('hide');
	});
	
	$(document).on('submit', '#sengineForm', function(e){
		e.preventDefault(); e.stopImmediatePropagation();
		if($(this).hasClass('small-header-form')){
			$search_text = $.trim($('.home-urlproduct-search').val());
			if($.trim($('.home-urlproduct-search').val()) == ''){
				return false;
			}
		}
		if($(this).hasClass('main-header-form')){
			$search_text = $.trim($('.home-product-search').val());
			if($.trim($('.home-product-search').val()) == ''){
				return false;
			}
		}
		
		$('#addSearchModal').foundation('reveal', 'open', {
			animation_speed: 0
		});
	});
	
	$(document).on('touchstart click', '.trend-keys li', function(e){
		e.preventDefault(); e.stopImmediatePropagation();
		$search_text = $.trim($(this).find('.trend-title-box label').html());
		if($.trim($search_text) == ''){
			return false;
		}
		
		$('#addSearchModal').foundation('reveal', 'open', {
			animation_speed: 0
		});
	});
	
	$(document).on('touchstart click', '.r-search-txt li a', function(e){
		e.preventDefault(); e.stopImmediatePropagation();
		$search_text = $.trim($(this).html());
		if($.trim($search_text) == ''){
			return false;
		}
		
		$('#addSearchModal').foundation('reveal', 'open', {
			animation_speed: 0
		});
	});
	
	$(document).on('focus', '.search-text, .sengine-search-text', function(){
		$(this).val('');
	});
	
	
	$(document).on('submit', '#sengineForm2', function(e){
		e.preventDefault(); e.stopImmediatePropagation();
		$('.error3').addClass('hide');
		var $form = $(this),
			url = $form.attr( 'action' );
			//url = root + 'admin/stick_products?action=external_products';
			form_data = {};
		form_data.isAjax = true;
		form_data.search_text = $.trim($('.sengine-search-text').val());
		form_data.color = $.trim($form.find( 'input[name="color"]' ).val());
		form_data.site = $.trim($form.find( 'select[name="site"]' ).val());;
		if(form_data.search_text == ''){
			return false;
		}
		
		$('.search-results-content ul').html('');
		$('.stick-product-modal3 .stick-loader .shopstyxloader div').html('SEARCHING...');
		$('.stick-product-modal3 .stick-loader').removeClass('hide');
		SSAjax({
			data: form_data,
			url: url,
			success: function(response){
				console.log('ss1');
				reset_scroll_page('forwin');
				extend_scroll_opts(form_data,'forwin');
				$('.image-lists ul').html(response);
				$('.stick-product-modal3 .stick-loader').addClass('hide');
				$('.stick-product-modal3 .stick-loader .shopstyxloader div').html('LOADING...');
			}
		});
		return false;
	});
	
	$(document).on('touchstart click', '.colors-content ul li', function(){
		alert('aaaw');
		$('.colors-content ul li').removeClass('selected');
		$(this).addClass('selected');
		$('.color-option').val($(this).attr('data'));
		
		$('.error3').addClass('hide');
		var $form = $('#sengineForm2'),
			url = $form.attr( 'action' );
			form_data = {};
		form_data.isAjax = true;
		form_data.search_text = $.trim($('.sengine-search-text').val());
		form_data.color = $.trim($form.find( 'input[name="color"]' ).val());
		form_data.site = $.trim($form.find( 'select[name="site"]' ).val());;
		if(form_data.search_text == ''){
			return false;
		}
		
		$('.search-results-content ul').html('');
		$('.stick-product-modal3 .stick-loader .shopstyxloader div').html('SEARCHING...');
		$('.stick-product-modal3 .stick-loader').removeClass('hide');
		SSAjax({
			data: form_data,
			url: url,
			success: function(response){
				
				reset_scroll_page('forwin');
				extend_scroll_opts(form_data,'forwin');
				$('.image-lists ul').html(response);
				$('.stick-product-modal3 .stick-loader').addClass('hide');
				$('.stick-product-modal3 .stick-loader .shopstyxloader div').html('LOADING...');
			}
		});
		return false;
		
	});
	
	$(document).on('change', '.site-option', function(){
		$('.error3').addClass('hide');
		var $form = $('#sengineForm2'),
			url = $form.attr( 'action' );
			form_data = {};
		form_data.isAjax = true;
		form_data.search_text = $.trim($('.sengine-search-text').val());
		form_data.color = $.trim($form.find( 'input[name="color"]' ).val());
		form_data.site = $.trim($form.find( 'select[name="site"]' ).val());;
		if(form_data.search_text == ''){
			return false;
		}
		
		$('.search-results-content ul').html('');
		$('.stick-product-modal3 .stick-loader .shopstyxloader div').html('SEARCHING...');
		$('.stick-product-modal3 .stick-loader').removeClass('hide');
		SSAjax({
			data: form_data,
			url: url,
			success: function(response){
				console.log('ss3');
				reset_scroll_page('forwin');
				extend_scroll_opts(form_data,'forwin');
				$('.image-lists ul').html(response);
				
				$('.stick-product-modal3 .stick-loader').addClass('hide');
				$('.stick-product-modal3 .stick-loader .shopstyxloader div').html('LOADING...');
			}
		});
		return false;
		
	});
	
	$(document).on('touchstart click', '.stick-content-a', function(){
		$('.add-search-step1, .add-search-step2, .search-modals').addClass('hide');
		$('.tmp-images').html('');
		$('.add-search-step1-content, .add-search-step2-content').html('');
	});
	
	$(document).on('close', '#stickModal, #addSearchModal, #stickAddModal', function (e) {
		e.preventDefault(); e.stopImmediatePropagation();
		$('body').removeClass('hidescroll');
		$('.reveal-modal-bg').removeClass('reveal-modal-bg-color');
		$('.reveal-modal-bg').remove();
		$('.add-product-step1-wrapper').addClass('hide');
		$('.add-product-step2-wrapper').addClass('hide');
		$('.add-product-step1-content').html('');
		$('.add-product-step2-content').html('');
		$('.tmp-images').html('');
		$('.product-url').val('');
		$(".stick-add-new-product .stick-wrapper input[type='hidden']").remove();
		$(".stick-product-add-modal .stick-wrapper input[type='hidden']").remove();
		$(".stick-add-new-product .stick-wrapper .stick-content-b").html('');
		$(".stick-product-add-modal .stick-wrapper .stick-content-b").html('');
	});
	
	$(document).on('touchstart click', '.stick-prod-btn', function(e){
		e.preventDefault(); e.stopImmediatePropagation();
		$('#stickAddModal').foundation('reveal', 'open', {
			animation_speed: 0
		});
		//var url = '/admin/scripter?action=product_urls';
		var url = '/admin/stick_products?action=external_single_product',
			form_data = {};
		form_data.isAjax = true;
		form_data.url = $.trim($(this).find('input').val());
		//form_data.new_product = true;
		form_data.product_image = $(this).find('img').attr('src');
		$('.stick-product-add-modal .stick-loader').removeClass('hide');
		$('.error3').addClass('hide');
		$('.stick-product-add-modal .stick-wrapper').html('');
		SSAjax({
			data: form_data,
			url: url,
			success: function(response){
				var _data = $.parseJSON(response),
					c = 0,
					image_counter = 0;
				if(_data.url_exist){
					var page = '/admin/stick_products?action=add_url_existing',
						form_data = {};
					form_data.product_site_name = _data.site_name;
					form_data.product_url = _data.web_url;
					form_data.product_name = _data.product_name;
					form_data.product_price = '$' + _data.price;
					form_data.product_image = _data.image;
					form_data.product_type = _data.product_type;
					form_data.product_keyword = _data.keyword;
					form_data.isAjax = true;
					$('.stick-product-add-modal .stick-loader').removeClass('hide');
					$('.add-product-step2-wrapper').removeClass('hide');
					SSAjax({
						data: form_data,
						url: page,
						success: function(response){
							$('.stick-product-add-modal .stick-wrapper').html(response);
							$('.stick-product-add-modal .stick-wrapper').prepend('<span class="stick-close2 posted-close"></span>');
							$('.stick-product-add-modal .stick-loader').addClass('hide');
							$('.stick-product-add-modal .posted-lbl').addClass('hide');
						}
					});
					return false
				}
			}
		});
		
		return false;
	});
	
	$(document).on('touchstart click', '.posted-close', function(){
		$('#stickAddModal').foundation('reveal', 'close');
		$('.stick-product-add-modal .stick-wrapper').html('');
		$('.reveal-modal-bg').remove();
	});
	
	
	
	$(document).on('click', '.stick-mod-close', function(){
		$('.dashboard-modal').hide('slide', {direction: 'right'}, 200);
		$('body').css({'overflow':'inherit'});
	});
	
	$(document).on('click', '.view-product-url', function(e){
		e.preventDefault(); e.stopImmediatePropagation();
		$('.product-info-modal .mod-wrapper').html('');
		openModalUrl();
		var $form = $(this),
			form_data = {};
		if($(this).hasClass('product_featured')){
			form_data.featured = 1;
		}else{
			form_data.featured = 0;
		}
		var settings = $.parseJSON($(this).attr('data'));
		$.extend(form_data,settings);
		showModalLoader();
		$.post("/admin/catalog?page=product-url-details", form_data,function(response){
			$('.product-info-modal .mod-wrapper').html(response);
			var height = $('.modal-header').height(),
				window_height = $(window).height();
			window_height = window_height - height;
			hideModalLoader();
		});
		
	});
	
	// $(document).on('click', '.product-sort-login li', function(e){
		// e.preventDefault(); e.stopImmediatePropagation();
		// if($(this).attr('data') == ""){ return false; }
		// $('#wrapper').html('');
		// $('#loadbox-wrapper').removeClass('hide');
		// var form_data = {};
		// form_data.display = $(this).attr('data');
		// form_data.isAjax = true;
		// SSAjax({
			// data: form_data,
			// url: location.href,
			// success: function(response){
				// $('#wrapper').html(response);
				// $('#loadbox-wrapper').addClass('hide');
				// $('#wrapper').removeClass('hide');
				// $('#loadbox-wrapper').addClass('hide');
			// }
		// });
	// });
	
	
	
	
	
	var count = 2;
	$(window).scroll(function(){
		if($(window).scrollTop() == $(document).height() - $(window).height()){
			if(count == 0){ return false; }
			count++;
		}
		if ($(".sticky-position").length > 0) {
			var sticky = $('.sticky-position').position();
			var movable = $('.movable-position').offset();
			var m = movable.top;
			var s = sticky.top;
			if($(".sticky-position").hasClass('sticky-store')){
				m = parseInt(m) - 130;
			}
			if(m > s){
				$('.movable-position').fadeIn();
			}
			if(m < s){
				$('.movable-position').fadeOut(110);
			}
		}
		
		// $(window).scroll(function() {
			// if ($(document).height() <= ($(window).height() + $(window).scrollTop())) {
				// //Bottom Reached
			// }
		// });
		
		// if ($(".more-from-external").length > 0) {
			// var sticky = $('.sticky-more-products').position();
			// var movable = $('.movable-bottom').offset();
			// var m = movable.top;
			// var s = sticky.top;
			// if(m > s){
				// $('.more-from-external').fadeIn(110);
			// }
		// }
		// if ($(".like-products").length > 0) {
			// var sticky = $('.sticky-like-products').position();
			// var movable = $('.movable-bottom').offset();
			// var m = movable.top;
			// var s = sticky.top;
			// if(m > s){
				// $('.like-products').fadeIn(110);
			// }
		// }
		
	});
	
	/* home script */
	
	$(document).on('click focus keyup', '.home-product-search', function(){
		$('.search-wrapper .trend-search-wrapper').removeClass('hide');
	});
	
	$(document).on('mouseleave', '.home-product-search, .search-wrapper .trend-search-wrapper', function(){
		$('.search-wrapper .trend-search-wrapper').addClass('hide');
	});
	$(document).on('mouseenter', '.search-wrapper .trend-search-wrapper', function(){
		$('.search-wrapper .trend-search-wrapper').removeClass('hide');
	});
	
	$(document).on('click focus keyup', '.home-urlproduct-search', function(){
		$('.small-header-search .trend-search-wrapper').removeClass('hide');
	});
	
	$(document).on('mouseleave', '.home-urlproduct-search, .small-header-search .trend-search-wrapper', function(){
		$('.small-header-search .trend-search-wrapper').addClass('hide');
	});
	$(document).on('mouseenter', '.small-header-search .trend-search-wrapper', function(){
		$('.small-header-search .trend-search-wrapper').removeClass('hide');
	});
	
	$(document).on('click', '.home-promos', function(e){
		e.preventDefault(); e.stopImmediatePropagation();

		var form_data = {};
		if($.trim($('.input-search_text').val()) != ""){
			form_data.search_text = $.trim($('.input-search_text').val());
		}
		form_data.price_from = $(this).parents('.tabs-wrapper').find('.display-price span.selected').attr('data-from');
		form_data.price_to = $(this).parents('.tabs-wrapper').find('.display-price span.selected').attr('data-to');
		form_data.color = $('.home-color-option span.selected').attr('data-color');
		form_data.department_id = (parseInt($('.depart-list-head li.active').attr('data')) != 0)? $('.depart-list-head li.active').attr('data') : $('.depart-list-small-head li.active').attr('data');
		if(!$(this).hasClass('active')){
			form_data.promos = true;
		}
		if($('.depart-list').hasClass('log-display')){
			form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
			form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
			form_data.isAjax = true;
		}else{
			form_data.isWrapper = true;
		}
		//$('#sub-wrapper').html('');
		$('#loadbox-wrapper').removeClass('hide');
		displayBG();
		$.post(root, form_data,function(response){
			resetNotifLoads();
			reset_scroll_page('forwin');
			extend_scroll_opts(form_data,'forwin');
			$('#sub-wrapper').html(response);
			$('#loadbox-wrapper').addClass('hide');
			$('#wrapper').removeClass('hide');
			$('#loadbox-wrapper').addClass('hide');
			removeBG();
		});
		
	});
	
	$(document).on('click', '.depart-list.homeproduct li', function(e){
		e.preventDefault(); e.stopImmediatePropagation();
		if($(this).attr('data') == ""){ return false; }
		var form_data = {};
		if($.trim($('.input-search_text').val()) != ""){
			form_data.search_text = $.trim($('.input-search_text').val());
		}
		form_data.price_from = $(this).parents('.tabs-wrapper').find('.display-price span.selected').attr('data-from');
		form_data.price_to = $(this).parents('.tabs-wrapper').find('.display-price span.selected').attr('data-to');
		form_data.color = $('.home-color-option span.selected').attr('data-color');
		form_data.department_id = $(this).attr('data');
		if($('.home-promos').hasClass('active')){
			form_data.promos = true;
		}
		if($('.depart-list').hasClass('log-display')){
			form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
			form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
			form_data.isAjax = true;
		}else{
			form_data.isWrapper = true;
		}
		//$('#sub-wrapper').html('');
		$('#loadbox-wrapper').removeClass('hide');
		displayBG();
		$.post(root, form_data,function(response){
			resetNotifLoads();
			reset_scroll_page('forwin');
			extend_scroll_opts(form_data,'forwin');
			$('#sub-wrapper').html(response);
			$('#loadbox-wrapper').addClass('hide');
			$('#wrapper').removeClass('hide');
			$('#loadbox-wrapper').addClass('hide');
			removeBG();
		});
		
	});
	
	$(document).on('touchstart click', '.removethis-color-filter', function(){
		$('.home-color-option span').removeClass('selected');
		$(this).addClass('hide');		
		var form_data = {};
		if($.trim($('.input-search_text').val()) != ""){
			form_data.search_text = $.trim($('.input-search_text').val());
		}
		form_data.price_from = $(this).parents('.tabs-wrapper').find('.display-price span.selected').attr('data-from');
		form_data.price_to = $(this).parents('.tabs-wrapper').find('.display-price span.selected').attr('data-to');
		form_data.department_id = (parseInt($('.depart-list-head li.active').attr('data')) != 0)? $('.depart-list-head li.active').attr('data') : $('.depart-list-small-head li.active').attr('data');
		if($('.home-promos').hasClass('active')){
			form_data.promos = true;
		}
		if($('.home-color-option').hasClass('log-color')){
			form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
			form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
			form_data.isAjax = true;
		}else{
			form_data.isWrapper = true;
		}
		
		//$('#sub-wrapper').html('');
		$('#loadbox-wrapper').removeClass('hide');
		displayBG();
		$.post(root, form_data,function(response){
			resetNotifLoads();
			reset_scroll_page('forwin');
			extend_scroll_opts(form_data,'forwin');
			$('#sub-wrapper').html(response);
			$('#loadbox-wrapper').addClass('hide');
			$('#wrapper').removeClass('hide');
			$('#loadbox-wrapper').addClass('hide');
			removeBG();
		});
		return false;
		
	});
	
	$(document).on('touchstart click', '.home-color-option span', function(){
		$('.home-color-option span').removeClass('selected');
		$(this).addClass('selected');
		var form_data = {};
		if($.trim($('.input-search_text').val()) != ""){
			form_data.search_text = $.trim($('.input-search_text').val());
		}
		form_data.price_from = $(this).parents('.tabs-wrapper').find('.display-price span.selected').attr('data-from');
		form_data.price_to = $(this).parents('.tabs-wrapper').find('.display-price span.selected').attr('data-to');
		form_data.department_id = (parseInt($('.depart-list-head li.active').attr('data')) != 0)? $('.depart-list-head li.active').attr('data') : $('.depart-list-small-head li.active').attr('data');
		form_data.color = $(this).attr('data-color');
		if($('.home-promos').hasClass('active')){
			form_data.promos = true;
		}
		if($('.home-color-option').hasClass('log-color')){
			form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
			form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
			form_data.isAjax = true;
		}else{
			form_data.isWrapper = true;
		}
		console.log(form_data);
		//$('#sub-wrapper').html('');
		$('#loadbox-wrapper').removeClass('hide');
		displayBG();
		$.post(root, form_data,function(response){
			resetNotifLoads();
			reset_scroll_page('forwin');
			extend_scroll_opts(form_data,'forwin');
			$('#sub-wrapper').html(response);
			$('#loadbox-wrapper').addClass('hide');
			$('#wrapper').removeClass('hide');
			$('#loadbox-wrapper').addClass('hide');
			removeBG();
		});
		return false;
		
	});
	
	$(document).on('click', '.display-price span', function(e){
		e.preventDefault(); e.stopImmediatePropagation();
		if($(this).attr('data') == ""){ return false; }
		var form_data = {};
		if($.trim($('.input-search_text').val()) != ""){
			form_data.search_text = $.trim($('.input-search_text').val());
		}
		form_data.color = $('.home-color-option span.selected').attr('data-color');
		form_data.department_id = (parseInt($('.depart-list-head li.active').attr('data')) != 0)? $('.depart-list-head li.active').attr('data') : $('.depart-list-small-head li.active').attr('data');
		form_data.price_from = $(this).attr('data-from');
		form_data.price_to = $(this).attr('data-to');
		if($('.home-promos').hasClass('active')){
			form_data.promos = true;
		}
		if($('.display-price').hasClass('log-price')){
			form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
			form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
			form_data.isAjax = true;
		}else{
			form_data.isWrapper = true;
		}
		//$('#sub-wrapper').html('');
		$('#loadbox-wrapper').removeClass('hide');
		displayBG();
		$.post(root, form_data,function(response){
			resetNotifLoads();
			reset_scroll_page('forwin');
			extend_scroll_opts(form_data,'forwin');
			$('#sub-wrapper').html(response);
			$('#loadbox-wrapper').addClass('hide');
			$('#wrapper').removeClass('hide');
			$('#loadbox-wrapper').addClass('hide');
			removeBG();
		});
	});
	
	$(document).on('touchstart click', '.removethis-price-filter', function(e){
		e.preventDefault(); e.stopImmediatePropagation();
		$('.display-price span').removeClass('selected');
		$(this).addClass('hide');
		var form_data = {};
		if($.trim($('.input-search_text').val()) != ""){
			form_data.search_text = $.trim($('.input-search_text').val());
		}
		form_data.department_id = (parseInt($('.depart-list-head li.active').attr('data')) != 0)? $('.depart-list-head li.active').attr('data') : $('.depart-list-small-head li.active').attr('data');
		form_data.color = $('.home-color-option span.selected').attr('data-color');
		if($('.home-promos').hasClass('active')){
			form_data.promos = true;
		}
		if($('.display-price').hasClass('log-price')){
			form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
			form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
			form_data.isAjax = true;
		}else{
			form_data.isWrapper = true;
		}
		
		//$('#sub-wrapper').html('');
		$('#loadbox-wrapper').removeClass('hide');
		displayBG();
		$.post(root, form_data,function(response){
			resetNotifLoads();
			reset_scroll_page('forwin');
			extend_scroll_opts(form_data,'forwin');
			$('#sub-wrapper').html(response);
			$('#loadbox-wrapper').addClass('hide');
			$('#wrapper').removeClass('hide');
			$('#loadbox-wrapper').addClass('hide');
			removeBG();
		});
		return false;
	});
		
	$(document).on('touchstart, click', '.search-by-color span', function(e){
		e.preventDefault(); e.stopImmediatePropagation();
		$('.search-by-color span').removeClass('selected');
		$(this).addClass('selected');
		loadExternalProducts({keywords:$('.s-text').val(),domain:'amazon.com',limit:1,total:$('.total-page').val(),color:$(this).attr('data-color')}, 0);
	});
	
	
	$(document).on('click', '#claimShopModal, #stickAddModal, #addToCollectionModal', function(){
		$(this).addClass('hide');
	});
	
	$(document).on('click', '#claimShopModal .modalDialog-content, #stickAddModal .modalDialog-content, #addToCollectionModal .modalDialog-content', function(e){
		e.stopImmediatePropagation();
		// if($(this).parent('#addToCollectionModal').hasClass('add-to-collection-modal')){
			// $('.add-to-collection-modal .modalDialog').removeClass('hide');
		// }
		// if($(this).parent('#claimShopModal').hasClass('claim-shop-modal')){
			// $('.claim-shop-modal .modalDialog').removeClass('hide');
		// }
	});
	
	$(document).on('click', '#assign-to-collection', function(e){
		e.preventDefault(); e.stopImmediatePropagation();
		if(typeof CS.iswebstore == "undefined") {
			var uid = store.get('user_id');
			var token = store.get('token');
		}
		else {
			var uid = $.cookies.get("uid");
			var token = $.cookies.get("token");		
		}
		if(uid == 0 || typeof(uid) == "undefined" || typeof(token) == "undefined") {
			signin_modal();
			if($(this).attr('ispopup')) {
				History.pushState(null, 'Shopstyx', '/');
			}
			return;
		}
		
		var $form = $(this),
			url = main_url + 'stick-products?action=assign_to_collection_form';
			form_data = {};
		form_data.isAjax = true;
		
		var data_attr = $.parseJSON($(this).attr('data'));
		$.extend(form_data,data_attr);
		
		$('#addToCollectionModal').removeClass('hide');
		$('#addToCollectionModal .stick-loader').removeClass('hide');
		$('#addToCollectionModal .stick-content-b').html('');
		SSAjax({
			data: form_data,
			url: url,
			success: function(data){
				$('#addToCollectionModal .stick-content-b').html(data);
				$('#addToCollectionModal .stick-loader').addClass('hide');
			}
		});
	});
	
	
});

function _assign_to_collection(fdata){
	var uid = store.get('user_id');
	var token = store.get('token');
	if(uid == 0 || typeof(uid) == "undefined" || typeof(token) == "undefined") {
		signin_modal();
		if($(this).attr('ispopup')) {
			History.pushState(null, 'Shopstyx', '/');
		}
		return;
	}
	
	var $form = $(this),
		url = main_url + 'stick-products?action=assign_to_collection_form';
		form_data = {};
	form_data.isAjax = true;
	
	var data_attr = $.parseJSON(fdata);
	$.extend(form_data,data_attr);
	
	$('#addToCollectionModal').removeClass('hide');
	$('#addToCollectionModal .stick-loader').removeClass('hide');
	$('#addToCollectionModal .stick-content-b').html('');
	SSAjax({
		data: form_data,
		url: url,
		success: function(data){
			$('#addToCollectionModal .stick-content-b').html(data);
			$('#addToCollectionModal .stick-loader').addClass('hide');
			$('#addToCollectionModal .posted-lbl2').removeClass('hide');
		}
	});
}

function openModalUrl(){
	var height = $(window).height();
	$('.dashboard-modal .mod-wrapper').css({'height': height});
	$('body').css({'overflow':'hidden'});
	$('.dashboard-modal').show('slide', {direction: 'right'}, 400);
}


function loadURLProducts(pageNumber){
	// $('a#inifiniteLoader').show('fast');
	$.ajax({
		url: root + "?page=" + pageNumber,
		type:'POST',
		data: "token=" + store.get('token') + "&user_id=" + store.get('user_id') + "&isAjax=" + true,
		success: function(response){
			if($.trim(response) == ''){ count = 0; return false; }
			//$('a#inifiniteLoader').hide('1000');
			$('.products-content ul').append(response);
		}
	});
	return false;
}


function loadExternalProducts(dataObj, offset)
{
	var page = root + 'stick_products?action=get_single_external_product',
		form_data = {};
	form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
	form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
	form_data.offset = offset;
	form_data.isAjax = true;
	$.extend(form_data,dataObj);
	//$('.bottom-s').append('<div class="u-prod-' + form_data.offset + ' u-product-loader small-3 large-3"></div>');
    $.ajax({
		type: "POST",
		url: page,
		data: form_data,
		cache: false,
		success: function(response){
			var _data = $.parseJSON(response);
			if(_data == null){ 
				//$('.u-product-loader').remove(); 
				return false; 
			}
			
							
			var get_data = ' <div class="u-product-height-home u-product u-pro-home prod-border lazy" style="background-image: url(\' ' + _data.image_url + ' \')"> \
								<div class="u-prod-info-wrapper withfade">	 \
									<div class="bg-opac bgcolor2"></div> \
									<div class="u-prod-info"> \
										<div class="u-prod-info-cont u-prod-info-padding1 left small-12 large-12 column"> \
											<div class="prod-cont left small-12 large-12 column"> \
												<div class="top"> \
													<span class="color1 prodname">' + _data.name + '</span> \
													<p class="price color8 clear">$' + _data.price + '</p> \
												</div> \
												<div class="mid"> \
													<div data-url="' + _data.canonical_url + '" class="stick-plus-btn bgcolor1 color2 left small-3 large-5 border-radius1 stick-new-product"> \
														STICK <span>+</span> \
													</div> \
												</div> \
												<div class="bottom"></div> \
											</div> \
										</div> \
									</div> \
								</div> \
							</div>';
							
							
			$('.u-prod-' + form_data.offset).html(get_data);
		}
	}).done(function()
    {
		if(form_data.offset <= form_data.total){
			// Success - Call Next
			var site_array = new Array ( 'amazon.com', 'etsy.com', 'ebay.com', 'zappos.com', 'barnesandnoble.com', 'homedepot.com', 'target.com', 'tigerdirect.com', 'singer22.com', 'shoptiques.com', 'renttherunway.com', 'otteny.com', 'mrporter.com', 'modcloth.com', 'asos.com', 'walmart.com', 'overstock.com', 'buyolympia.com', 'perpetualkid.com', 'amazon.com', 'etsy.com', 'ebay.com', 'zappos.com' );
			var num = form_data.offset + 1;
			var domain = site_array[num];
			$('.bottom-s').append('<div class="u-prod-' + num + ' u-product-loader left plist-four small-4 large-4"></div>');
			form_data.offset++;
			loadExternalProducts({keywords:form_data.keywords,domain:domain,limit:form_data.limit,total:form_data.total}, form_data.offset);
		}
    }).fail(function()
    {
        // Failed - Retry?
        loadExternalProducts({keywords:form_data.keywords,domain:form_data.domain,limit:form_data.limit,total:form_data.total}, form_data.offset);
    });
}


function loadExProducts(dataObj)
{
	var page = root + 'stick-products?action=getSearchResults',
		form_data = {};
	form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
	form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
	form_data.isAjax = true;
	$.extend(form_data,dataObj);
	form_data.domain = '';
    $.ajax({
		type: "POST",
		url: page,
		data: form_data,
		cache: false,
		success: function(response){
			var _data = $.parseJSON(response);
			if(! _data.data_status){
				return false;
			}
			if(_data.data_type == 'db'){
				$('.u-prod-0').remove();
				$.each(_data.data, function(i,item){
				var stick = '';
				if(parseInt($('.is-user').val()) == 1){
					stick = '<div data-url="' + item.canonical_url + '" class="stick-plus-btn bgcolor1 color2 left small-3 large-5 border-radius1 stick-new-product"> STICK <span>+</span></div>';
				}
				
				var get_data = ' <div class="u-prod-0 left u-product-loader plist-four small-4 large-4">\
									<div class="u-product-height-home u-product u-pro-home prod-border lazy" style="background-image: url(\' ' + item.image + ' \')"> \
										<div class="u-prod-info-wrapper withfade">	 \
											<div class="bg-opac bgcolor2"></div> \
											<div class="u-prod-info"> \
												<div class="u-prod-info-cont u-prod-info-padding1 left small-12 large-12 column"> \
													<div class="prod-cont left small-12 large-12 column"> \
														<div class="top"> \
															<span class="color1 prodname">' + item.product_name + '</span> \
															<p class="price color8 clear">$' + item.price + '</p> \
														</div> \
														<div class="mid"> ' + stick + ' </div> \
														<div class="bottom"></div> \
													</div> \
												</div> \
											</div> \
										</div> \
									</div> \
								</div>';
					$('.bottom-s').append(get_data);
				});
			}
			
			if(_data.data_type == 'bing'){
				$('.u-prod-0').remove();
				var x=0;
				$.each(_data.data, function(i,item){
					$('.bottom-s').append('<div class="u-prod-' + x + ' left u-product-loader plist-four small-4 large-4"></div>');
					
					var page = root + 'stick-products?action=getBing',
						form_data = {};
					form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
					form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
					form_data.isAjax = true;
					form_data.SourceUrl = item.SourceUrl;
					form_data.MediaUrl = item.MediaUrl;
					form_data.ContentType = item.ContentType;
					form_data.count = x;
					$.extend(form_data,dataObj);
					$.ajax({
						type: "POST",
						url: page,
						data: form_data,
						cache: false,
						success: function(res){
							var _datas = $.parseJSON(res);
							if(_data.is_valid == 1){
								var stick = '';
								if($('.is-user').val() == 1){
									stick = '<div data-url="' + _datas.canonical_url + '" class="stick-plus-btn bgcolor1 color2 left small-3 large-5 border-radius1 stick-new-product"> STICK <span>+</span></div>';
								}
				
								var get_data2 = ' <div class="u-product-height-home u-product u-pro-home prod-border lazy" style="background-image: url(\' ' + _datas.image + ' \')"> \
													<div class="u-prod-info-wrapper withfade">	 \
														<div class="bg-opac bgcolor2"></div> \
														<div class="u-prod-info"> \
															<div class="u-prod-info-cont u-prod-info-padding1 left small-12 large-12 column"> \
																<div class="prod-cont left small-12 large-12 column"> \
																	<div class="top"> \
																		<span class="color1 prodname">' + _datas.product_name + '</span> \
																		<p class="price color8 clear">$' + _datas.price + '</p> \
																	</div> \
																	<div class="mid">' + stick + '</div> \
																	<div class="bottom"></div> \
																</div> \
															</div> \
														</div> \
													</div> \
												</div>';
								$('.u-prod-' + form_data.count).html(get_data2);
							}else{
								$('.u-prod-' + form_data.count).remove();
							}
						}
					});
					x++;
				});
			}
		}
	});
}

function add_top_shopstyx($this, $container){
	//var $this = $(this),
	var	error = 0;
	
	$('.stick-product-name').removeClass('error');
	$('.stick-product-price').removeClass('error');
	$('.image-lbl').removeClass('data-required ');
	$('.color-lbl').removeClass('data-required ');
	$('.product-for').removeClass('data-required ');
	if($('.stick-product-name').val() == ''){
		$('.stick-product-name').addClass('error');
		error++;
	}
	
	var price = (/^[0-9,]{1,}(\.)?([0-9]{1,})?$/);
	if (!price.test($.trim($('.stick-product-price').val()))) {
		$('.stick-product-price').addClass('error');
		error++;
	}
	
	// if($.trim($('.stick-product-price').val()) == ""){
		// $('.stick-product-price').addClass('error');
		// error++;
	// }
	
	var page = root + 'stick_products?action=url_step2',
		form_data = {};
	form_data.product_site_name = $('.stick-product-site_name').val();
	form_data.product_brand_name = $('.stick-product-brand_name').val();
	form_data.product_brand_link = $('.stick-product-brand_link').val();
	form_data.external_product_url = $('.stick-product-url').val();
	form_data.external_product_canonical_url = $('.stick-product-canonical-url').val();
	form_data.external_tags = $('.stick-product-tags').val();
	form_data.url_exist = $('.stick-product-url-exist').val();
	form_data.product_name = $('.stick-product-name').val();
	form_data.price = $('.stick-product-price').val();
	form_data.currency_symbol = $( ".currency-symbol option:selected" ).text();
	form_data.currency = $('.currency-symbol').val();
	form_data.image = $('.choose-img-content ul li.active input').val();
	form_data.product_for = ( $.trim($('.stick-for li.active').attr('data')) != "")? $('.stick-for li.active').attr('data') : $('.stick-for li.selected').attr('data');
	form_data.product_for_name = ( $.trim($('.stick-for li.active').html()) != "")? $('.stick-for li.active').html() : $('.stick-for li.selected').html();
	form_data.color = $('.add-color span.selected').attr('data-color');
	form_data.external_tags = $('.stick-product-tags').val();
	form_data.product_keyword = $('.get-search-text').val();
	form_data.isAjax = true;
	
	
	if( form_data.image == null ){
		$('.image-lbl').addClass('data-required ');
		error++;
	}
	
	if( form_data.color == null ){
		$('.color-lbl').addClass('data-required ');
		error++;
	}
	
	if( form_data.product_for == null || form_data.product_for == "" ){
		$('.product-for').addClass('data-required ');
		error++;
	}
	if(error){ return false; }
	
	$('.stick-product-modal .add-product-step-loader').removeClass('hide');
	$('.stick-product-modal3 .stick-loader').removeClass('hide');
	$('.bg-trans').removeClass('hide');
	$('.add-search-modal .stick-loader').removeClass('hide');
	SSAjax({
		data: form_data,
		url: page,
		success: function(response){
			if($this.hasClass('sengine-stick-add-btn')){
				$('.tmp-images').html('');
				$('.stick-product-modal3 .add-search-step2').removeClass('hide');
				$('.stick-product-modal3 .add-search-step2-content').html(response);
				$('.stick-product-modal3 .stick-loader').addClass('hide');
				$('.add-search-step2 .stick-a-product').remove();
				$('.add-search-step2 .arrow-down').remove();
				$('.save-to-btn').removeClass('save-collection-btn');
				$('.save-to-btn').addClass('sengine-save-collection-btn');
				$('.add-search-modal .stick-loader').addClass('hide');
			}else{
				$( $container + ' .add-product-step2-content').html(response);
				$('.add-product-step1-wrapper').addClass('hide');
				$('.add-product-step2-wrapper').removeClass('hide');
				$('.stick-product-modal .add-product-step-loader').addClass('hide');
				$('.add-search-modal .stick-loader').addClass('hide');
			}
		}
	});
}

function displayBG(){
	$('.header-bg').remove()
	$('.listing-wrapper').remove()
	$('.container-top-bg').removeClass('hide');
	$('.container-content-bg').removeClass('hide');
}
function removeBG(){
	$('.container-top-bg').addClass('hide');
	$('.container-content-bg').addClass('hide');
}