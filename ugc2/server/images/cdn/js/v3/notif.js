var intvlnotifs, seall, nummsg, numnotifs, msgmod=0, 
	lastnotif="",lastmsg=0, smodes=false, plist, punreads=-1,
	checkloads,whoseturn=1;

//chat props
var chatprops = {
	'msghovd': false,
	'notifhovd': false,
	'useini':true,
	'ini':'',
	'msgurl': 'notifications/message_list',
	'thrurl': 'notifications/get_thread',
	'openthread': 0, //1 - chat, 2 - prod, 3 - order, 4 - store
	'target': 0,
	'intrvl': null,
	'last_id': '',
	'mmode': '',
	'running': 0,
	'prod': null,
	'prodimg':'',
	'prodname':'',
	'storeimg':'',
	'storename':'',
	'order':null,
	'store':null,
	'chattype': 'chat',
	'chatindex': -1,
	'modalStarted': 0
};

//#EVENTS START

$(document).on('click', '#messageModal .newmsg', function(){
	prepareNewMessage();
});

$(document).on('click', '.messages-panel.npops .mmore', function(){
	$('#messageModal').foundation('reveal', 'open');

});

$(document).on('click', '.messages-panel.npops .newmsg', function(){
	doNewMessage();
});

$(document).on('click', '#messageModal .aarea.right .backbtn', function(){
	switchMode(2);
});

//open message list of a chosen user from the message menu at navbar
//trigger modal to open
$(document).on('click', '.messages-panel.npops .mitems li', function(){
	//console.log('hoy');
	$('#messageModal').attr('targetopen',$(this).attr('user'));
	$('#messageModal').foundation('reveal', 'open');
});


$(document).on('click', '#messageModal .search', function(){
	switchSearch(smodes);
	if (smodes){
		$('#messageModal .mitems.spmain').removeClass('hide');
		$('#messageModal .mitems.spres').addClass('hide');
	}
	smodes = !smodes;
});

var pt;

//open message list from people list on message window
$(document).on('click', '#messageModal .mitems li', function(){
	var eln = $('#messageModal');
	eln.find('.mitems li').removeClass('selected');
	eln.find('.setchat').addClass('hide');
	$(this).addClass('selected');
	chatprops.target = 0;


	if (smodes){
		var newchat = eln.find('.chat.you.loading.hide').clone();
		eln.find('.chatarea .innerchat div.chat').remove();
		eln.find('.chatarea .innerchat').append(newchat);
		eln.find('.right.aarea.thread .chead > .left > strong').text('Start a New Conversation');


		$('#messageModal .mitems.spmain').removeClass('hide');
		$('#messageModal .mitems.spres').addClass('hide');
		switchSearch(true);
		smodes = false;
	}
	
	if ($(this).attr('typd')==1){
		eln.attr('targetopen',$(this).attr('user'));
	//eln.attr('targetid',$(this).attr('chid'));
		clearInterval(chatprops.intrvl);
		chatprops.last_id = 0;
		chatprops.target = $(this).attr('user');
		chatprops.running = 0;
		var datalod = gatherReq();
		datalod['target'] = chatprops.target;

		$.ajax({
			data: datalod,
			type: 'POST',
			url: CS.main_url+chatprops.msgurl,
			success: function(response) {
				pt = eln.find('ul.chatlist');
				switchMode(2);
				pt.html(response);
				if (pt.find('li[fname]').length == 1){
					pt.find('li[fname]:last-of-type').trigger('click');
				}
			}
		});
	}else{

		switchMode(3);



		$('.setchat .chosname').remove();

		var lli = $('<li>',{'class':'clearfix result'});
		var theimg = $(this).find('.img.left').css('background-image');
		theimg = theimg.substr(4,theimg.length-5);
		lli.attr('namedisp',$(this).attr('namedisp'))
		   .attr('uname',$(this).attr('username'))
		   .attr('fname',$(this).attr('fname'))
		   .attr('lname',$(this).attr('lname'))
		   .attr('userid',$(this).attr('user'))
		   .attr('imgurl',theimg);
		var imgleft = $('<div>',{'class':'img left'});
		imgleft.css('background-image','url('+theimg+')');
		var nchose = $('<div>',{'class':'namechoose left'}).text($(this).find('span.head strong').text());
		lli.append(imgleft).append(nchose);
		
		applyNewNameToSend(lli);
		prepareSendMessage();
	}

});

$(document).on('open', '#messageModal', function () {
	$('.reveal-modal-bg').css('display', 'block');
	$(this).find('.shopstyxloader-box').removeClass('hide');
	$(this).find('.messagecont').addClass('hide');
	$(this).centerized();
});
$(document).on('closed', '#messageModal', function () {
	$('.reveal-modal-bg').css('display', 'none');
	$('#messageModal').removeAttr('targetopen');
	$('body').css('overflow','auto');
	chatprops.openthread = 0;
	chatprops.mmode = '';
	clearInterval(chatprops.intrvl);
	switchMode(0);
});

$(document).on('opened', '#messageModal', function () {
	var eln = $(this);
	//var hasthread = false;
	var load = $(this).find('.shopstyxloader-box');
	var cont = $(this).find('.messagecont');
	var datalod = gatherReq();
	$('body').css('overflow','hidden');
	if (eln.attr('targetopen')) datalod['target'] = eln.attr('targetopen');
	if (chatprops.openthread > 0) {
		//datalod['ctype'] = chatprops.openthread;
		msgmod=4;
		switch(chatprops.openthread){
			case 1:
				datalod['ctype'] = 'chat';
				break;
			case 2: //product
				datalod['ctype'] = 'product-chat';
				datalod['product_id'] = chatprops.prod['product_id'];
				datalod['store_id'] = chatprops.prod['store_id'];
				datalod['store_user_id'] = chatprops.prod['store_user_id'];
				datalod['collection_id'] = chatprops.prod['collection_id'];
				datalod['variant_id'] = chatprops.prod['variant_id'];
				break;
			case 3:
				datalod['ctype'] = 'order-chat';
				datalod['order_id'] = chatprops.order.order_id;
				datalod['cust_order_id'] = chatprops.order.cust_order_id;
				datalod['scust_order_id'] = chatprops.order.scust_order_id;
				break;
			case 4:
				datalod['ctype'] = 'store-chat';
				datalod['store_id'] = chatprops.store.store_id;
				datalod['store_user_id'] = chatprops.store.store_user_id;
				datalod['claimed_by'] = chatprops.store.claimed_by;
				break;
		}
		//eln.find('.aarea.right .chead .left strong').text('Loading Conversation...');

	}
	if (chatprops.mmode != '') datalod['mmode'] = chatprops.mmode;
	SSAjax({
		data: datalod,
		url: eln.attr('url'),
		success: function(response) {
			load.addClass('hide');
			cont.removeClass('hide');
			eln.find('.messagecont').html(response);
			if (eln.attr('targetopen')){
				if (chatprops.openthread > 0){ // for threads
					eln.find('.chatarea').scrollTop(eln.find('.chatarea .innerchat').height());
				}else{
					chatprops.target = eln.attr('targetopen');
					msgmod = (chatprops.target > 0)?2:1;
					chatprops.prod = {};
					chatprops['prodname'] = "";
					chatprops['prodimg'] = "";
					chatprops['storename'] = "";
					chatprops['storeimg'] = "";
					var pt = eln.find('.chatlist');
					if (pt.find('li[fname]').length == 1){
						pt.find('li[fname]:last-of-type').trigger('click');
					}
				}
			}
		}
	});
	
	//var top = parseInt($('.product-details-view').position().top) - 88;
	//$('.product-details-view').css({'top': $('.product-details-view').position().top});
});

$(document).on('click', '.messages-panel.npops .mitems li', function(){
	$('#messageModal').attr('targetopen',$(this).attr('user'));
	$('#messageModal').foundation('reveal', 'open');
});


$(document).on('keyup','#messageModal input[name=qppl]', function(e){
	if ($(this).val()!=""){ 
		$('ul.spmain').addClass('hide');
		$('ul.spres').removeClass('hide');
		var res = $('ul.spmain li[fullname*="'+$(this).val()+'"]').clone();
		$('ul.spres').html(res);
		/*var uids=[];
		res.each(function(index, val) {
			 uids.push($(val).attr('user'));
		});
		uids = uids.join(',');*/
		var dataout = gatherReq();
		dataout.term = $(this).val().toLowerCase();
		dataout.peopleview = 1;
		//dataout.exempt = uids;
		$.ajax({
			url: CS.main_url+'notifications/search_name',
			type: 'POST',
			data: dataout,
			success: function(response) {
				$('ul.spres').empty();
				$('ul.spres').append(response);
			}
		});
	}else{
		$('ul.spmain').removeClass('hide');
		$('ul.spres').addClass('hide');
	}
});

$(document).on('keyup','#messageModal input[name=chatwhom]', function(e){
	var eln = $('#messageModal');
	var dul = eln.find('.chatarea .nameres').removeClass('hide');
	dul.find('.nores').remove();
	if ($(this).val()!=""){
		var prg = dul.find('.progress').removeClass('hide');
		var prgdup = prg.clone().addClass('hide');
		var datalod = gatherReq();
		datalod['term'] = $(this).val();
		SSAjax({
			data: datalod,
			type: "POST",
			url: CS.main_url+"notifications/search_name",
			success: function(response) {
				dul.empty();
				dul.append(prgdup);
				dul.append(response);
			}
		});
	}else{
		dul.addClass('hide');
	}
});

$(document).on('click', '#messageModal .chatlist li', function() {
	var eln = $('#messageModal');
	
	eln.attr('targetid',$(this).attr('chid'));
	clearInterval(chatprops.intrvl);
	chatprops.last_id = 0;
	chatprops.target = $(this).attr('user');
	chatprops.running = 0;
	chatprops.useini = ($(this).attr('useini')=="1");
	chatprops.ini = $(this).attr('ini');
	chatprops.newmsg = false;
	chatprops.usrimg = $(this).attr('img');
	chatprops.targetid = $(this).attr('chid');
	chatprops.chattype = $(this).attr('type');
	if (chatprops.chattype == "product-chat"){
		var ct = $(this).attr('about').split('-');
		chatprops.prodname = $(this).attr('prodname');
		chatprops.prodimg = $(this).attr('prodimg');
		chatprops.prod = {
			'product_id': ct[0],
			'store_id': ct[1],
			'store_user_id': ct[2],
			'collection_id': ct[3],
			'variant_id': ct[4]
		};
	}
	if (chatprops.chattype == "order-chat"){
		chatprops.order = {'order_id': $(this).attr('about'),
			'cust_order_id': $(this).attr('cust_order_id'),
			'scust_order_id': $(this).attr('scust_order_id')};
	}
	if (chatprops.chattype == "store-chat"){
		chatprops.storename = $(this).attr('storename');
		chatprops.storeimg = $(this).attr('storeimg');
		chatprops.store = {'store_id': $(this).attr('about'),
			'store_user_id': $(this).attr('store_user_id'),
			'claimed_by': $(this).attr('claimed_by')};
	}
	var thename = $(this).attr('fname')+" "+$(this).attr('lname');
	showThread(thename);
	//'last_id': '',
	//'chatindex': -1
});

$(document).on('click','#messageModal .chatarea .nameres li.result', function(e){
	applyNewNameToSend($(this));
	prepareSendMessage();
});


$(document).on('keydown', '#messageModal textarea[name=message]', function (e) {
	if (e.which == 13 && !e.shiftKey && $(this).val() != "") {
		prepareSendMessage();
		e.preventDefault(); e.stopImmediatePropagation();
	}
});

$(document).on('click', '#messageModal input[name=sendmsg]', function (e) {
	e.preventDefault(); e.stopImmediatePropagation();
	var eln = $('#messageModal');
	if (eln.find('textarea[name=message]').val() != "") prepareSendMessage();
});




//#FUNCTIONS START

function gatherReq(){
	var datalod = {};
	if(typeof CS.iswebstore == "undefined") {
		datalod.user_id = store.get('user_id');
		datalod.token = store.get('token');
	}
	else {
		datalod.user_id = $.cookies.get("uid");
		datalod.token = $.cookies.get("token");	
	}
	return datalod;
}

function switchSearch(ssm){
	$('#messageModal .left.aarea input#qppl').val('');
	if (!ssm){
		$('#messageModal .left.aarea .chead').addClass('smode');
		$('#messageModal .left.aarea input#qppl').focus();
		$('#messageModal .left.aarea .tbtn .sbtn').addClass('hide');
		$('#messageModal .left.aarea .tbtn .cbtn').removeClass('hide');
	}else{
		$('#messageModal .left.aarea .chead').removeClass('smode');
		$('#messageModal .left.aarea input#qppl').focus();
		$('#messageModal .left.aarea .tbtn .sbtn').removeClass('hide');
		$('#messageModal .left.aarea .tbtn .cbtn').addClass('hide');
		//search starts here
	}
}

function applyNewNameToSend(el){
	var eln = $('#messageModal .setchat');
	eln.find('input').addClass('hide');
	var chosen = $('<div>',{'class':'chosname clearfix'}).html(el.html());
	chosen.attr('userid',el.attr('userid'))
		  .attr('fname',el.attr('fname'))
		  .attr('lname',el.attr('lname'))
		  .attr('uname',el.attr('uname'))
		  .attr('namedisp',el.attr('namedisp'))
		  .attr('imgurl',el.attr('imgurl'));
	eln.find('.nameres').addClass('hide');
	eln.append(chosen);
}

function prepareSendMessage(){
	var eln = $('#messageModal');
	var ltl = $('#messageModal div.chatarea div.chat:last-of-type');
	var tochats = false;
	var thename;
	if (chatprops.target==0){
		var chosen = eln.find('.setchat .chosname');
		if (typeof(chosen.attr('fname')) != "undefined"){
			// to identify if it's a new chat
			var thename = chosen.attr('namedisp');
			eln.find('.aarea.right .chead .left strong').html('<span class="imgconv" style="background-image:url(\''+chosen.attr('imgurl')+'\');"></span> Conversation with <a href="/'+ chosen.attr('uname')+'">'+thename+'</a>');
			chatprops.target = chosen.attr('userid');
			chatprops.chattype = "chat";
			chatprops.usrimg = chosen.attr('imgurl');
			eln.find('.setchat').addClass('hide');
			tochats = true;

			//lets highlight if it's in the list
			$('#messageModal .mitems li').removeClass('selected');
			$('#messageModal .mitems li[user='+chatprops.target+']').addClass('selected');

			//showThread(thename,sendMessage);
			showThread(thename,null);
		}

	}else{
		sendMessage();
	}
}

function sendMessage(){
	var eln = $('#messageModal');
	var ltl = $('#messageModal div.chatarea div.chat:last-of-type');

	
	var datalod = gatherReq();
	datalod['message'] = nl2br(eln.find('textarea[name=message]').val());

		
	datalod['target'] = chatprops.target;
	datalod['ctype'] = chatprops.chattype;

	if (chatprops.chattype == "product-chat"){
		datalod['product_id'] = chatprops.prod['product_id'];
		datalod['store_id'] = chatprops.prod['store_id'];
		datalod['store_user_id'] = chatprops.prod['store_user_id'];
		datalod['collection_id'] = chatprops.prod['collection_id'];
		datalod['variant_id'] = chatprops.prod['variant_id'];
	}

	if (chatprops.chattype == "order-chat"){
		datalod['order_id'] = chatprops.order['order_id'];
		datalod['cust_order_id'] = chatprops.order['cust_order_id'];
		datalod['scust_order_id'] = chatprops.order['scust_order_id'];
	}

	if (chatprops.chattype == "store-chat"){
		datalod['store_id'] = chatprops.store['store_id'];
		datalod['store_user_id'] = chatprops.store['store_user_id'];
		datalod['claimed_by'] = chatprops.store['claimed_by'];
	}

	msgmod = 4;
	eln.find('textarea[name=message]').val('');
	var newchat = eln.find('.chat.you.loading.hide').clone().removeClass('hide');
	newchat.find('.msg .im p').html(datalod['message']);
	eln.find('.innerchat').append(newchat);
	eln.find('.chatarea').scrollTop(eln.find('.chatarea .innerchat').height());
	//alert('muscle');
	SSAjax({
		data: datalod,
		type: "POST",
		url: CS.main_url+"notifications/send_chat",
		success: function(response) {
			//load.addClass('hide');
			var res = eval('('+response+')');
			chatprops.last_id = res['res']['data'][0]['_id'];
			chatprops.targetid = res['res']['data'][0]['_id'];
			newchat.removeClass('loading');
			//newchat.attr('cid',res['_id']);
			newchat.find('.msg i').text('Now');
			var ltlr = $('#messageModal .mitems.spmain li[user='+chatprops.target+']');
			
			if (typeof(ltlr.attr('user'))!="undefined"){ //it means existing thread
				//move up in message pop
				//var tel = eln.find('.mitems.spmain li[user='+ltl.attr('user')+']');
				//var upd = tel.clone();
				var upd = ltlr.clone();
				ltlr.remove();
				upd.find('p').html(datalod['message']);
				eln.find('.mitems.spmain').prepend(upd);

				//move up in message nav
				var tel = $('.messages-panel.npops .mitems li[user='+datalod['target']+']');
				upd = tel.clone();
				tel.remove();
				upd.find('p').html(datalod['message']);
				$('.messages-panel.npops .mitems').prepend(upd);
			}else{ // if new
				eln.find('.mitems li').removeClass('selected');
				/*var nw = eln.find('.mitems.spmain li.hide').clone();

				var hinfo = eln.find('.right.aarea .chead');

				nw.removeClass('hide').addClass('selected')
				.attr('user',datalod['target']);
				nw.find('.left.img').css('background-image','url('+eln.find('.setchat > .chosname').attr('imgurl')+')');
				nw.find('.right .head strong').text(eln.find('.setchat > .chosname').attr('namedisp'));
				//nw.find('.right .head i').text('Now');
				nw.find('.right p').html(datalod['message']);

				var nw2 = nw.clone().removeAttr('selected');
				$('.messages-panel.npops .mitems').prepend(nw2);
				$('#messageModal .mitems.spmain').prepend(nw);*/
				load_messages(true);
			}
			$('#messageModal .mitems.spmain').removeClass('hide');
			$('#messageModal .mitems.spres').addClass('hide');
			switchSearch(true);
			smodes = false;
		}
	});
	
}

function showThread(name,nextfnx){
	var eln = $('#messageModal');
	var thename = name;
	var imghide = chatprops.useini?' image-hide':'';
	var iniput = '<span class="ipic-thumb'+(chatprops.useini?'':' hide')+'">'+chatprops.ini+'</span>';
	var theface = '<span class="imgconv'+imghide+'" style="background-image: url('+chatprops.usrimg+');">'+iniput+'</span>';
	var newchat = eln.find('.chat.you.loading.hide').clone();
	eln.find('.chatarea .innerchat div.chat').remove();
	eln.find('.chatarea .innerchat').append(newchat);

	eln.find('.aarea.right .chead .left strong').text('Loading Conversation...');
	var datalod = gatherReq();
	datalod['target'] = chatprops.target;
	switchMode(3);
	eln.find('.setchat').addClass('hide');
	var schat = eln.find('.setchat').clone();
	var snode = eln.find('.chat.you.loading.hide').clone();

	//datalod['targetid'] = chatprops.targetid;
	datalod['ctype'] = chatprops.chattype;
	if (chatprops.chattype == "product-chat")
		$.extend(datalod,chatprops.prod);
	if (chatprops.chattype == "order-chat")
		$.extend(datalod,chatprops.order);
	if (chatprops.chattype == "store-chat")
		$.extend(datalod,chatprops.store);
	$.ajax({
		data: datalod,
		type: 'POST',
		url: CS.main_url+chatprops.thrurl,
		success: function(response) {
			eln.find('.innerchat').empty().append(schat).append(snode).append(response);
			if (chatprops.chattype == "chat"){
				eln.find('.aarea.right .chead .left strong').html(theface+'Conversation with <a href="#" class="wscolor1">'+thename+'<a/>');
			}else if (chatprops.chattype == "product-chat"){
				eln.find('.aarea.right .chead .left strong').html(theface+'Product Inquiry <a href="#" class="pname wsborder-color1"><span class="img" style="background-image: url('+chatprops.prodimg+');"></span><span class="nm wscolor2 wsbgcolor1">'+chatprops.prodname+"</span></a>");
			}else if (chatprops.chattype == "order-chat"){
				eln.find('.aarea.right .chead .left strong').html(theface+'Order Conversation <a href="#" class="pname wsborder-color1"><span class="img"><span class="oconv"></span></span><span class="nm wscolor2 wsbgcolor1">Order No. <b>'+lpad(chatprops.order['cust_order_id'],"0",10)+"</b></span></a>");
			}else if (chatprops.chattype == "store-chat"){
				eln.find('.aarea.right .chead .left strong').html(theface+'Shop Owner Inquiry <a href="#" class="pname wsborder-color1"><span class="img" style="background-image: url('+chatprops.storeimg+');"></span><span class="nm wscolor2 wsbgcolor1">'+chatprops.storename+"</span></a>");
			}

			if ($.trim(response)!=''){
				chatprops.last_id = eln.find('.chatarea .innerchat div.chat:last-of-type').attr('cid')?eln.find('.chatarea .innerchat div.chat:last-of-type').attr('cid'):0;
				eln.find('.chatarea').scrollTop(eln.find('.chatarea .innerchat').height());
			}
			msgmod = 4;
			if (nextfnx!=null) nextfnx();
			//startchatcheck();
		}
	});
}


function openThisThread(target,ctype,opts){
	$('#messageModal').attr('targetopen',target);
	chatprops.chattype = ctype;
	chatprops.openthread = 1;
	chatprops.target = target;
	//console.log(opts);
	switch(ctype){
		case 'product-chat':
			chatprops.prod = opts;
			chatprops.openthread = 2;
			break;
		case 'order-chat':
			chatprops.order = opts;
			//console.log(chatprops.order);
			chatprops.openthread = 3;
			break;
		case 'store-chat':
			chatprops.store = {'store_id':opts['store_id'],
			'store_user_id':opts['store_user_id'],
			'claimed_by':opts['claimed_by']
			};
			chatprops.storename = opts['store_name'];
			chatprops.storeimg = opts['store_logo'];
			//console.log(chatprops.order);
			chatprops.openthread = 4;
			break;
	}
	$('#messageModal').foundation('reveal', 'open');
}

function doNewMessage(){
	prepareNewMessage();
	chatprops.mmode = 'new';
	$('#messageModal').foundation('reveal', 'open');
}

function switchMode(mode){
	//mode - 0 for unopened (no effect)
	//mode - 1 for no item window, 2 - for message list, 
	//mode - 3 for new message, 4 - for thread
	var eln;
	switch(mode){
		case 0: break;
		default: case 1: case 2:
			eln = $('#messageModal .right.aarea.convos');
			eln.removeClass('hide');
			$('#messageModal .right.aarea.thread').addClass('hide');
			if (mode == 2){
				eln.find('.outchatlist').removeClass('hide');
				eln.find('.noitems').addClass('hide');
			}else{
				eln.find('.noitems').removeClass('hide');
				eln.find('.outchatlist').addClass('hide');
			}
			break;
		case 3: case 4:
			eln = $('#messageModal .right.aarea.thread');
			eln.removeClass('hide');
			$('#messageModal .right.aarea.convos').addClass('hide');
			if (mode == 3){
				eln.find('.setchat').removeClass('hide');
			}else{
				eln.find('.setchat').addClass('hide');
			}
			break;

	}
	msgmod = mode;
}

function init_set_last_thread(_id){
	chatprops.last_id = _id;
}

function checkunreads(){
	var punc=0;
	for (var pp in plist){
		if (plist[pp]['unread']>0) punc++;
	}
	if (punreads != -1 && punreads < punc){

	}
	punreads = punc;
	var cntr;
	if(typeof CS.iswebstore == "undefined") {
		cntr = $('.navLink.messages .bg span.total-counter');
	}else{
		cntr = $('.header-content .message span.total-counter');
	}
	cntr.addClass('hide');
	if (punc>0){
		cntr.text(punc);
		cntr.removeClass('hide');
	}
}

function begin_checkfeeds(){
	load_messages();
	load_notification();
	whoseturn = 1;
	//calloutfeeds();
	nummsg = 0;
	numnotifs = 0;
	checkloads = false;
	infinite_scroll($(".notification-panel.npops .scarea"), 'fornotif', {}, CS.main_url+'notifications/get_notif', $(".notification-panel.npops ul.nitems"), false);
	//intvlnotifs = setInterval(calloutfeeds, 7000);
}

function calloutfeeds(){
	clearInterval(intvlnotifs);
	switch (whoseturn){
		default: case 1:
			load_messages(); break;
		case 2:
			load_notification(); break;
		case 3:
			if (msgmod == 2) load_user_message();
			else if (msgmod == 4) {
				whoseturn = 4;
				checknewthread();
			}else{
				whoseturn = 1;
				load_messages();
			}
			break;
		case 4:
			if (msgmod == 4) checknewthread();
			else {
				whoseturn = 1;
				load_messages();
			}
			break;
	}
	whoseturn++;
	if (whoseturn>4) whoseturn=1;
}

function igniteNotif(){
	//console.log('the: '+checkloads);
	if (checkloads) intvlnotifs = setInterval(calloutfeeds, 7000);
	checkloads = true;
}

function load_user_message(){
	var eln = $('#messageModal');
	var datalod = gatherReq();
	datalod['target'] = chatprops.target;
	$.ajax({
		data: datalod,
		type: 'POST',
		url: CS.main_url+chatprops.msgurl,
		success: function(response) {
			pt = eln.find('ul.chatlist');
			pt.html(response);
			igniteNotif();
		}
	});
}

function load_messages(notimer) {
	var eln = $(".messages-panel.npops"),
		ld = eln.find('.loader'),
		ajax_container = eln.find('ul.mitems'),
		seall = $('<li>',{'class':'mmore'}).text('See All');
		var dataout = gatherReq();
		dataout.lastdate = lastmsg;
	$.ajax({
		url: CS.main_url+'notifications/people_list',
		type: 'POST',
		data: dataout,
		success: function(response){
			var res = $('<ul>').html(response);
			/*if (res.find('li:last-of-type').attr('date'))
				lastmsg = res.find('li:last-of-type').attr('date');*/
			//if (!eln.hasClass('active')){
				ld.addClass('hide');
				eln.addClass('active');
				ajax_container.removeClass('hide');
				/*$(res.find('li')).each(function(index, val) {
					if ($(val).attr('read')=="false") nummsg++;
				});*/
				ajax_container.html(response);
			/*}else{
				var lis=[], nis=[], flag=[], fnd;
				if ($(res.find('li')).length>0){*/
					/*$(res.find('li')).each(function(index, val1) {
						var newip = $(val1).clone();
						$(ajax_container.find('li')).each(function(index2, val2) {
							flag[index2] = false;
						 	if (!fnd && ($(val2).attr('chid') == $(val1).attr('chid'))){
						 		flag[index2] = true;
						 		return false;
						 		//lis.push(newip);
						 	}
						 });
						
						//if (!fnd) nis.push(val1);
					});*/

					/*for(var ii=0; ii<flag.length; ii++){
						if (flag[ii]){
							$(ajax_container.find('li')[ii]).remove();
						}
					}*/				
					/*$(res.find('li')).each(function(index, val1) {
						ajax_container.append(val1);
					});
				}
			}*/
			if (msgmod > 0){
				$('#messageModal .mitems.spmain').html(response);
				if (chatprops.target != 0) {
					$('#messageModal .mitems.spmain li[user='+chatprops.target+']').addClass('selected');
				}
			}
			checkunreads();
			if (!notimer) igniteNotif();
			/*var cntr = $('.navLink.messages .bg span.total-counter');
			if (nummsg>0){
				cntr.text(nummsg);
				cntr.removeClass('hide');
			}*/
			
			//ajax_container.append(seall);
			//updateCartSizeContainer();
		}
	});
}

//

function close_messages() {
	$('.messages-panel').addClass('hide');
}


function load_notification() {
	var eln = $(".notification-panel.npops"),
		ld = eln.find('.loader'),
		ajax_container = eln.find('ul.nitems');
		//seall = $('<li>',{'class':'mmore'}).text('See All');
		if (!eln.hasClass('active')) ld.removeClass('hide');
		var dataout = gatherReq();
		dataout.st = lastnotif;
	$.ajax({
		url: CS.main_url+'notifications/get_notif',
		type: 'POST',
		data: dataout,
		success: function(response){
			var dres = $('<ul>').html(response);
			var firstadded = false;
			$(dres.find('li')).each(function(index, val) {
				 if ($(val).attr('read')=="false"){
				 	numnotifs++;
				 }
			});
			//numnotifs += $(dres.find('li')).length;
			if (dres.find('li:first-of-type').attr('nid'))
				lastnotif = dres.find('li:first-of-type').attr('nid');
			if (!eln.hasClass('active')){
				firstadded = true;
				ld.addClass('hide');
				eln.addClass('active');
				ajax_container.removeClass('hide');
			}
			var cntr = $('.navLink.notif span.total-counter');
			if (numnotifs>0){
				cntr.text(numnotifs);
				cntr.removeClass('hide');
			}
			if (lastnotif != '') {
				if (ajax_container.find('li.nonotif').length > 0) ajax_container.html(dres.find('li'));
				else ajax_container.prepend(dres.find('li'));
			}else{
				if (firstadded) ajax_container.html(dres.find('li'));
			}
			igniteNotif();
			//ajax_container.append(seall);
			//updateCartSizeContainer();
		}
	});
}

function resetNotifLoads(){
	var eln = $(".notification-panel.npops"),
		ld = eln.find('.loader');
	lastnotif = "";
	ld.removeClass('hide');
	eln.removeClass('active');

}

function prepareNewMessage(){
	var eln = $('#messageModal');
	eln.find('.mitems.spmain').removeClass('hide');
	eln.find('.mitems.spres').addClass('hide');
	switchSearch(true);
	smodes = false;


	//if (chatprops.modalStarted){
		eln.find('.setchat').removeClass('hide');
		eln.find('.mitems.spmain li').removeClass('selected');
		chatprops.last_id = 0;
		chatprops.target = 0;
		chatprops.targetid = "";
		chatprops.running = 0;
		chatprops.chattype = 'chat';
		chatprops.prod = null;
		chatprops.prodname = "";
		chatprops.prodimg = "";
		chatprops.order=null;
		chatprops.newmsg=true;
		eln.find('.setchat .chosname').remove();
		eln.find('.setchat input').removeClass('hide');
		eln.find('.right.aarea.thread .chead > .left > strong').text('Start a New Conversation');

		var newchat = eln.find('.chat.you.loading.hide').clone();
		eln.find('.chatarea > .innerchat > div.chat').remove();

		eln.find('.chatarea > .innerchat').append(newchat);
		switchMode(3);
	/*}else{
		chatprops.modalStarted = 1;
	}*/
}



// ----------------- for message threads
function startchatcheck(){
	if (!chatprops['running']){
		chatprops['running'] = 1;
		chatprops['intrvl'] = setInterval(checknewthread,7000);
	}
}


function checknewthread(){
	if (chatprops['target']!=0){
		var eln = $('#messageModal');
		var datalod = gatherReq();
		datalod['target'] = chatprops.target;
		datalod['targetid'] = chatprops.targetid;
		datalod['ctype'] = chatprops.chattype;
		if (chatprops.chattype == "product-chat"){
			datalod['product_id'] = chatprops.prod['product_id'];
			datalod['store_id'] = chatprops.prod['store_id'];
			datalod['store_user_id'] = chatprops.prod['store_user_id'];
			datalod['collection_id'] = chatprops.prod['collection_id'];
			datalod['variant_id'] = chatprops.prod['variant_id'];
		}
		if (chatprops.chattype == "order-chat"){
			datalod['order_id'] = chatprops.order.order_id;
			datalod['cust_order_id'] = chatprops.order.cust_order_id;
			datalod['scust_order_id'] = chatprops.order.scust_order_id;
		}
		if (chatprops.chattype == "store-chat"){
			datalod['store_id'] = chatprops.store.store_id;
			datalod['store_user_id'] = chatprops.store.store_user_id;
			datalod['claimed_by'] = chatprops.store.claimed_by;
		}
		datalod['where'] = chatprops.last_id;
		//console.log(datalod);
		$.ajax({
			data: datalod,
			type: 'POST',
			url: CS.main_url+chatprops['thrurl'],
			success: function(response) {
				eln.find('.chatarea .innerchat').append(response);
				if ($.trim(response)!=''){
					chatprops.last_id = eln.find('.chatarea .innerchat div.chat:last-of-type').attr('cid')?eln.find('.chatarea .innerchat div.chat:last-of-type').attr('cid'):0;
					eln.find('.chatarea').scrollTop(eln.find('.chatarea .innerchat').height());
				}
				igniteNotif();
			}
		});
	}
}

function markread(target){
	var datalod = gatherReq();
	datalod['target'] = target;
	switch(chatprops.chattype){
		case 'product-chat': 
			datalod['ctype'] = "product";
			datalod['product_id'] = chatprops.prod['product_id'];
			datalod['store_id'] = chatprops.prod['store_id'];
			datalod['store_user_id'] = chatprops.prod['store_user_id'];
			break;
		case 'order-chat': 
			datalod['ctype'] = "order";
			datalod['order_id'] = chatprops.order.order_id;
			datalod['cust_order_id'] = chatprops.order.cust_order_id;
			datalod['scust_order_id'] = chatprops.order.scust_order_id;
			break;
		case 'store-chat': 
			datalod['ctype'] = "store";
			datalod['store_id'] = chatprops.store.store_id;
			datalod['store_user_id'] = chatprops.store.store_user_id;
			datalod['claimed_by'] = chatprops.store.claimed_by;
			break;
		case 'chat': default: 
			datalod['ctype'] = "generic";
			break;
	}
	$.ajax({
		data: datalod,
		type: 'POST',
		url: CS.main_url+"notifications/mark_read_messages"
	});
}

function lpad (str, padString, length) {
  while (str.length < length) {
    str = padString + str;
  }
  return str;
}

function nl2br(str, is_xhtml) {
  var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br ' + '/>' : '<br>'; // Adjust comment to avoid issue on phpjs.org display

  return (str + '')
    .replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
}