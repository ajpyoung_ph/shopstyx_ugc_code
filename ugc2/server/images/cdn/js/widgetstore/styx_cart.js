function get_cart_items(data) {
	var form_data = {};
	form_data.cart = store.get('cart');
	form_data.cart_sess = store.get('cart_sess');
	form_data.login_user_id = store.get('user_id');
	form_data.token = store.get('token');
	$.extend(form_data,data);

	$.ajax({
		type: "POST",
		url: '/widget-store-cart-items.html',
		data: form_data,
		crossDomain: true,
		cache: false,
		success: function(response){
			var _data = $.parseJSON(response);
			var total_item = parseInt(_data.total_item);
			var total_price = _data.total_price;
			
			
			if(total_item > 1) {
				total_item += " items";	
			}
			else if(total_item == 1) {
				total_item += " item";	
			}
			else {
				total_item = "no item";	
			}
			
			switch(data.widget_type) {
				case 'type1': 
						$('#wdgt-main-footer #main-footer-items').html(total_item + ' added - Subtotal: <strong>$'+total_price+'</strong>');
						$('#wdgt-main-footer #main-footer-items').show();
						break;	
				case 'type2':
						$('.items-added').html(total_item + ' added - Subtotal: <strong>$'+total_price+'</strong>');
						$('.items-added').show();
						break;
				case 'type3':
						$('#wdgt-main-footer #main-footer-items').html(total_item + ' added - Subtotal: $'+total_price);
						$('#wdgt-main-footer #main-footer-items').show();
						$('.wdgt-item').html(total_item + ' - $'+total_price);
						break;
			}
			
		}
	})
}

function load_cart(data) {
	if(typeof(data) == 'undefined') {
		return;	
	}
	var form_data = {cart: store.get('cart'),
					cart_sess:store.get('cart_sess'),
					login_user_id:store.get('user_id'),
					token:store.get('token')
					};

	$.extend(form_data,data);
	
	$.post( '/widget-store-cart-listing.html', form_data,function(data){
		switch(form_data.widget_type) {
			case 'type1':
					$('#cart-content').html(data);
					$('#wdgt-cart-popup').show();
					$('#wdgt-cart-main-center-wrapper').jScrollPane();
					break;
			case 'type2':
					$('#addtocart-wrapper').html(data);
					$('#addtocart-wrapper').show();
					$('#addtocart-wrapper-list #scrollbar').jScrollPane();
					break;		
			case 'type3':
					$('#wdgt-cart-popup').html(data);
					$('#wdgt-cart-popup').show();
					$('#wdgt-main-center-wrapper').jScrollPane();
					break;		
		}
		
		$('#preloader-wrapper').hide();
	});
}

function update_cart_quantity(data) {
	if(typeof(data) != 'object') return;
	/*
	var product_user_id = data.product_user_id; // product owner or merchant
	var product_id = data.product_id;
	var store_id = data.store_id;
	var store_user_id = data.store_user_id; // endorser or store owner
	var quantity = data.quantity;
	var has_options = data.has_options;
	*/
	var variant_id = data.variant_id;
	data.product_options = {};
	// get existing cart values
	var cart = store.get('cart');
	cart = (typeof(cart) != 'object' || cart == null) ? {} : cart;
	
	data.variant_id = variant_id;
		
	data.cart = cart;
	data.cart_sess = store.get('cart_sess');
	data.login_user_id = store.get('user_id');
	data.token = store.get('token');

	//preLoader($('#ms-wrapper'));
	$.ajax({
		type: "POST",
		url: '/widget-store-update-cart.html?update=new_quantity',
		data: data,
		cache: false,
		success: function(res){
			result = $.parseJSON(res);
			store.set('cart',result.cart);
			store.set('cart_sess',result.cart_sess);
			
			var form_data = {};
				form_data.key = $('#key').val();
				form_data.user_id = $('#ms_uid').val();
				form_data.store_id = $('#ms_sid').val();
				form_data.widget_type = $('#widget_type').val();
				form_data.home_type = $('#home_type').val();
				
			get_cart_items({ key: $('#key').val(),
							user_id: $('#ms_uid').val(),
							store_id: $('#ms_sid').val(),
							widget_type: $('#widget_type').val()});
				
			preLoader($('#ms-wrapper'));
			load_cart(form_data);
		}
	});
}

function delete_cart_product(data) {
	if(typeof(data) != 'object') return;
	var variant_id = data.variant_id;
	data.product_options = {};
	// get existing cart values
	var cart = store.get('cart');
	cart = (typeof(cart) != 'object' || cart == null) ? {} : cart;
	
	data.variant_id = variant_id;
		
	data.cart = cart;
	data.cart_sess = store.get('cart_sess');
	data.login_user_id = store.get('user_id');
	data.token = store.get('token');
	//preLoader($('#ms-wrapper'));
	$.ajax({
		type: "POST",
		url: '/widget-store-update-cart.html?update=delete_item',
		data: data,
		cache: false,
		success: function(res){
			cart = $.parseJSON(res);
			store.set('cart',cart);
			var form_data = {};
				form_data.key = $('#key').val();
				form_data.user_id = $('#ms_uid').val();
				form_data.store_id = $('#ms_sid').val();
				form_data.widget_type = $('#widget_type').val();
				form_data.home_type = $('#home_type').val();
				
			get_cart_items({ key: $('#key').val(),
							user_id: $('#ms_uid').val(),
							store_id: $('#ms_sid').val(),
							widget_type: $('#widget_type').val()});
							
			preLoader($('#ms-wrapper'));
			load_cart(form_data);
		}
	});
}


function addtocart(data) {
	if(typeof(data) != 'object') return;
	
	var product_user_id = data.product_user_id; // product owner or merchant
	var product_id = data.product_id;
	var store_id = data.store_id;
	var store_user_id = data.store_user_id; // endorser or store owner
	var quantity = data.quantity;
	var has_options = data.has_options;
	data.product_options = {};
	
	// get existing cart values
	var cart = store.get('cart');
	cart = (typeof(cart) != 'object' || cart == null) ? {} : cart;

	//console.log(cart.merchants[user_id].products.hasOwnProperty(1));
	//console.log(cart);
	// retreive existing 
	var item, option = '';
	var error_option = false;
	var variants_values = new Array();
	
	/*
	for (var key in cart) {
		if (cart.hasOwnProperty(key)) {
			total_merchant++;
			new_cart[key] = cart[key]
		}
	}
	*/
	
	// Option Validations
	$('.option-value-wrapper').each(function() {
		if($(this).find('select').val() == '') {
			error_option = true;	
		}
		
		data.product_options[$(this).attr('option_id')] = $(this).find('select').val();
		variants_values.push($(this).find('select').val());
	});
	
	var options_string = variants_values.join(',');
	var variants = $.parseJSON($('#unique_variants').attr('variants'));
	var variant_id = 0;
	for (var key in variants) {
		if (variants.hasOwnProperty(key)) {
			if(variants[key] == options_string) {
				variant_id = key;	
			}
		}
	}
	
	if(error_option || (has_options > 0 && variant_id == 0)) {
		alert('Please select an option.');
		return;	
	}

	data.variant_id = variant_id;
	data.cart = cart;
	data.cart_sess = store.get('cart_sess');
	data.login_user_id = store.get('user_id');
	data.token = store.get('token');
	//$.extend(data,ms_getQueryParams());
	//preLoader($('#ms-wrapper'));
	$.ajax({
		type: "POST",
		url: '/widget-store-update-cart.html',
		data: data,
		cache: false,
		success: function(res){
			cart = $.parseJSON(res);
			store.set('cart',cart);
			var form_data = {};
				form_data.key = $('#key').val();
				form_data.user_id = $('#ms_uid').val();
				form_data.store_id = $('#ms_sid').val();
				form_data.widget_type = $('#widget_type').val();
				form_data.home_type = $('#home_type').val();
				
			get_cart_items({ key: $('#key').val(),
							user_id: $('#ms_uid').val(),
							store_id: $('#ms_sid').val(),
							widget_type: $('#widget_type').val()});
			// 
			$('#option-wrapper').hide(); $('#option-wrapper #option-selection-form').html('');	
			
			preLoader($('#ms-wrapper'));
			load_cart(form_data);
		}
	});
}

function show_selectoptions(data) {
	preLoader($('#ms-wrapper'));
	
	var form_data = {};
		form_data.key = $('#key').val();
		form_data.user_id = $('#ms_uid').val();
		form_data.store_id = $('#ms_sid').val();
		form_data.widget_type = $('#widget_type').val();
		form_data.home_type = $('#home_type').val();
		
	$.extend(form_data,data);	
	$.post( '/widget-store-select-option.html', data, function(data){
		$('#preloader-wrapper').hide();
		$('#option-wrapper #option-selection-form').html(data);
		$('#option-wrapper').show();
		$('#option-content .scroller').jScrollPane();
		
		//jSpane_api_product_option.reinitialise();
		customized_select();
	});
}

function customized_select() {
	$('.customized_select select').each(function(){
		if($(this).parent().find('div').length == 0) {
			title = $('option:selected',this).text();
			$(this)
				.css({'z-index':10,'opacity':0,'-khtml-appearance':'none'})
				.after('<div>' + title + '</div>')
				.change(function(){
					val = $('option:selected',this).text();
					$(this).next().text(val);
				})
		}
	});
}
$(document).ready(function() {
	$(document).on('click','#option-wrapper #close-btn', function() { $('#option-wrapper').hide(); $('#option-wrapper #option-selection-form').html(''); });
	
	$(document).on('change','.option-value-wrapper .customized_select select', function() {
		var variants_values = $.parseJSON($('#unique_variants').attr('variants_values'));
		var unique_variants = $.parseJSON($('#unique_variants').val());
		var index = parseInt($('.option-value-wrapper .customized_select select').index(this));
		var value = $(this).val();
		var next_option = (index + 1);

		if(index == 0) {
			$('.option-value-wrapper .customized_select').eq(1).addClass('disabled');	
			$('.option-value-wrapper .customized_select').eq(2).addClass('disabled');	
			$('.option-value-wrapper .customized_select select').eq(1).val('');
			$('.option-value-wrapper .customized_select select').eq(2).val('');
			$('.option-value-wrapper .customized_select select').eq(1).attr('disabled','disabled');	
			$('.option-value-wrapper .customized_select select').eq(2).attr('disabled','disabled');	
			$('.option-value-wrapper .customized_select div').eq(1).html('Select Item');
			$('.option-value-wrapper .customized_select div').eq(2).html('Select Item');
			
			$(this).parent().removeClass('disabled');
			$(this).removeAttr('disabled');
		}

		if(typeof $('.option-value-wrapper .customized_select select').eq(next_option) != "undefined" && value != "") {
			$('.option-value-wrapper .customized_select select').eq(next_option).find('option').hide();
			$('.option-value-wrapper .customized_select select').eq(next_option).find('option').eq(0).show();
			$('.option-value-wrapper .customized_select select').eq(next_option).removeAttr('disabled');
			$('.option-value-wrapper .customized_select select').eq(next_option).parent().removeClass('disabled');
			$('.option-value-wrapper .customized_select select').eq(next_option).val('');
			$('.option-value-wrapper .customized_select div').eq(next_option).html('Select Item');
			for(i = 0; i < variants_values.length; i++) {
				if(variants_values[i][index] == value) {
					$('.option-value-wrapper .customized_select select').eq(next_option).find('option[value='+variants_values[i][next_option]+']').show();	
				}
			}
			
		}
		
	});
	
	$(document).on('click','.option-value-wrapper.color .option-value, .option-value-wrapper.size .option-value', function() {
		var el = $(this);
		var parent = el.parent().parent();
		
		parent.find('.option-value').removeClass('selected');
		parent.find('input').removeAttr('checked');
		parent.find('.value-txt').removeClass('selected');
		el.addClass('selected');
		el.find('input').attr('checked','checked');
		el.next().addClass('selected');
	});
	
	// radio button
	$(document).on('click','.option-value-wrapper.radio .option-value-lbl', function() {
		var el = $(this);
		var parent = el.parent().parent();
		
		parent.find('input').removeAttr('checked');
		parent.find('.value-txt').removeClass('selected');
		el.addClass('selected');
		el.find('input').attr('checked','checked');
		el.next().addClass('selected');
	});
	
	// checkbox button
	$(document).on('click','.option-value-wrapper.checkbox .option-value-lbl', function() {
		var el = $(this);

		if(typeof(el.find('input').attr('checked')) == 'undefined') {
			console.log(el.next());
			el.next().removeClass('selected');
		}
		else {
			el.next().addClass('selected');
		}
	});
});
