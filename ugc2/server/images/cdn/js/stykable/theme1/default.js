var	cloud_url = CS.cloudURL || location.protocol + '//' + location.host + '/';
var main_url = CS.siteURL || location.protocol + '//' + location.host + '/';
var root = CS.siteURLMain || location.protocol + '//' + location.host + '/';
var store_cart_data = {};
var is_remotestorecart = false;

$(document).ready(function(){
	$('.title-text').textfill({innerTag:'a', maxFontPixels: 50});
	$('#image-slideshow-wrapper').slides({
		preload: true,
		preloadImage: cloud_url+'images/webstore_images/loading.gif',
		play: 5000,
		pause: 2500,
		effect: 'fade',
		hoverPause: true,
		slidesLoaded: function(){
			var current_page = $('.pagination .current a').html();
			var height = parseInt($('.product_title'+current_page).height());
			if(height < 100){
				$('.psd'+current_page).css({'top':'380px'});
			}
			if(height > 100){
				$('.psd'+current_page).css({'top':'412px'});
			}
		},
		animationComplete: function(){
			$('.wdgt-sharebox').hide();
			$('.wdgt-wishlistbox').hide();
			var current_page = $('.pagination .current a').html();
			var height = parseInt($('.product_title'+current_page).height());
			if(height < 100){
				$('.psd'+current_page).css({'top':'380px'});
			}
			if(height > 100){
				$('.psd'+current_page).css({'top':'412px'});
			}
		},
		
	});
	
	$(document).on('click', '.sendbtn', function(e){
		e.preventDefault();
		e.stopImmediatePropagation();
		$(this).parents('form').submit();
	});
	
	$(document).on('submit', '#emailForm', function(e){
		e.preventDefault(); e.stopImmediatePropagation();
		var $form = $(this);
		var url = $form.attr( 'action' );
		var form_data = {};
		var sender_id = store.get('user_id');
		
		form_data.subject = jQuery.trim($form.find( 'input[name="subject"]' ).val());
		form_data.sender_email = jQuery.trim($form.find( 'input[name="email"]' ).val());
		form_data.sender = sender_id;
		if(typeof sender_id === 'undefined'){
			form_data.sender = form_data.sender_email;
		}
		form_data.recipient = jQuery.trim($form.find( 'input[name="recipient_id"]' ).val());
		form_data.message = jQuery.trim($form.find( 'textarea[name="message"]' ).val());
		form_data.reference = 'webstore';
		$('#contact-us-send-wrapper #preloader-wrapper').show();
		$('.sendbtn').attr('disabled', 'disabled');
		$.post(url, form_data,function(data){
			var _data = $.parseJSON(data);
			$('#contact-us-send-wrapper #preloader-wrapper').hide();
			if(_data.sending_result){
				$('#e-subject').val('');
				$('#e-message').val('');
			}
			$('.sendbtn').removeAttr('disabled');
		});
		return false;
	});
	
	$(document).on('mouseleave', '.ms-category-slide', function(e){
		e.preventDefault();
		var $this = $(this);
		$(this).find('.ms-cat-top').slideDown("fast", function(){
			$this.find('.ms-subcat-list').fadeOut('fast');
		});
		$(this).find('.ms-subcat-list').fadeOut('fast', function(){
			$this.find('.ms-cat-top').slideDown('fast');
		});
	});
	$(document).on('mouseenter', '.ms-category-slide', function(e){
		e.preventDefault();
		var $this = $(this);
		$(this).find('.ms-cat-top').slideUp(400, function(){
			$this.find('.ms-subcat-list').fadeIn(100);
		});
	});
	
	$(document).on('mouseenter', '.ms-subcat-list', function(e){
		$(this).parents('.ms-category-slide').find('.ms-cat-top').slideUp("fast");
	});
	
	$(document.body).on('touchstart click', '.goto-listing', function(){
		var cid = $(this).attr('cid');
		window.location.href = main_url + 'product-listing.html?cid=' + cid;
	});
	
	
	$(document).on('click', '.webstore_prod_page', function(){
		var cid = $('#cid').val();
		var page;
		page = $(this).attr('href') + '?cid=' + cid;
		window.location.href = page;
		return false;
	});
	
	$(document).on('click', '.webstore_search_page', function(){
		var key_word = $('#key_word').val();
		var page;
		page = $(this).attr('href') + '?key_word=' + key_word;
		window.location.href = page;
		return false;
	});
	
	$('#_search').bind('keypress', function(e) {
		$(this).removeClass('red-border');
		var key_word = encodeURIComponent($.trim($(this).val()));
		if(e.keyCode==13){
			if(key_word == ""){
				$(this).addClass('red-border');
				return false;
			}
			window.location.href = main_url + 'search.html?key_word=' + key_word;
		}
	});
	
	$(document.body).delegate('.search_btn', 'click', function(){
		$('#_search').removeClass('red-border');
		var key_word = encodeURIComponent($.trim($('#_search').val()));
		if(key_word == ""){
			$('#_search').addClass('red-border');
			return false;
		}
		window.location.href = main_url + 'search.html?key_word=' + key_word;
	});
	
	$(document.body).on('click', '.photo-btn', function(){
		$('#video-photos-btn-wrapper span').removeClass('selected');
		$(this).addClass('selected');
		$('#video-wrapper').addClass('hide');
		$('#details-slideshow-wrapper').removeClass('hide');
	});
	
	$(document.body).on('click', '.video-btn', function(){
		$('#video-photos-btn-wrapper span').removeClass('selected');
		$(this).addClass('selected');
		$('#details-slideshow-wrapper').addClass('hide');
		$('#video-wrapper').removeClass('hide');
	});
	
	
	$('select.sorting').change(function(){
		$(this).parent('form').submit();
	});
	$(document).on('mouseenter', '.#slideshow-each-title-div-top #slideshow-title-top label', function(){
		$('#slideshow-title label').css({'opacity':1, 'filter': 'Alpha(opacity=100)'});
	});
	$(document).on('mouseleave', '.#slideshow-each-title-div-top #slideshow-title-top label', function(){
		$('#slideshow-title label').css({'opacity':0.8, 'filter': 'Alpha(opacity=80)'});
	});
	
	$(document).on('click', '#wdgt-wishlistbox ul li', function(){
		if(typeof store.get('user_id') === 'undefined'){
			$(login).trigger('click');
			return false;
		}
		url = root + 'add-to-wishlist.html';
		$this = $(this);
		var form_data = {};
		form_data.wishlist_val = $(this).find('#wishtype').val();
		form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
		form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
		$this.parents('#wdgt-wishlistbox').find('#preloader-wrapper').show();
		$.ajax({
			type: "POST",
			url: url,
			data: form_data,
			cache: false,
			success: function(data){
				if(!data){ $this.parents('#wdgt-wishlistbox').find('#preloader-wrapper').hide(); return false; }
				var _data = $.parseJSON(data);
				$.each(_data, function(i, item) {
					$('.wishlist_'+_data[i].ref_user_id+'_'+_data[i].product_id+'_'+_data[i].store_id+'_1').removeClass('active');
					$('.wishlist_'+_data[i].ref_user_id+'_'+_data[i].product_id+'_'+_data[i].store_id+'_2').removeClass('active');
					$('.wishlist_'+_data[i].ref_user_id+'_'+_data[i].product_id+'_'+_data[i].store_id+'_3').removeClass('active');
					$('.wishlist_'+_data[i].ref_user_id+'_'+_data[i].product_id+'_'+_data[i].store_id+'_'+_data[i].status).addClass('active');
					$('.wishlists_'+_data[i].product_id+'_'+_data[i].store_id+'_1').removeClass('active');
					$('.wishlists_'+_data[i].product_id+'_'+_data[i].store_id+'_2').removeClass('active');
					$('.wishlists_'+_data[i].product_id+'_'+_data[i].store_id+'_3').removeClass('active');
					$('.wishlists_'+_data[i].product_id+'_'+_data[i].store_id+'_'+_data[i].status).addClass('active');
					$this.parents('#wdgt-wishlistbox').find('#preloader-wrapper').hide();
				});
			}
		});
		return false;
	});
	
	// product options (pop-up)
	$(document).on('click','#option-wrapper #close-btn, #option-wrapper #overlay', function() { 
		$('#option-wrapper').hide(); 
		$('#option-wrapper #option-selection-form').html(''); 
	});
	$(document).on('change','.option-value-wrapper .customized_select select, .details-option-value-wrapper .customized_select select', function() {
		var parent_container = ($(this).parent().parent().hasClass('details-option-value-wrapper')) ? '.details-option-value-wrapper': '.option-value-wrapper';
		var variants_values = $.parseJSON($('#unique_variants').attr('variants_values'));
		var unique_variants = $.parseJSON($('#unique_variants').val());
		var index = parseInt($(parent_container + ' .customized_select select').index(this));
		var value = $(this).val();
		var next_option = (index + 1);

		if(index == 0) {
			$(parent_container + ' .customized_select').eq(1).addClass('disabled');	
			$(parent_container + ' .customized_select').eq(2).addClass('disabled');	
			$(parent_container + ' .customized_select select').eq(1).val('');
			$(parent_container + ' .customized_select select').eq(2).val('');
			$(parent_container + ' .customized_select select').eq(1).attr('disabled','disabled');	
			$(parent_container + ' .customized_select select').eq(2).attr('disabled','disabled');	
			$(parent_container + ' .customized_select div').eq(1).html('Select Item');
			$(parent_container + ' .customized_select div').eq(2).html('Select Item');
			
			$(this).parent().removeClass('disabled');
			$(this).removeAttr('disabled');
		}

		if(typeof $(parent_container + ' .customized_select select').eq(next_option) != "undefined" && value != "") {
			$(parent_container + ' .customized_select select').eq(next_option).find('option').hide();
			$(parent_container + ' .customized_select select').eq(next_option).find('option').eq(0).show();
			$(parent_container + ' .customized_select select').eq(next_option).removeAttr('disabled');
			$(parent_container + ' .customized_select select').eq(next_option).parent().removeClass('disabled');
			$(parent_container + ' .customized_select select').eq(next_option).val('');
			$(parent_container + ' .customized_select div').eq(next_option).html('Select Item');
			//console.log(variants_values);
			for(i = 0; i < variants_values.length; i++) {
				if(variants_values[i][index] == value) {
					$(parent_container + ' .customized_select select').eq(next_option).find('option[value='+variants_values[i][next_option]+']').show();	
				}
			}
			
		}
		else {
			if(index > 0 && value == '') {
				$(parent_container + ' .customized_select select').eq(next_option).parent().addClass('disabled');
				$(parent_container + ' .customized_select select').eq(next_option).attr('disabled','disabled');
				$(parent_container + ' .customized_select select').eq(next_option).find('option').hide();
				$(parent_container + ' .customized_select select').eq(next_option).val('');
				$(parent_container + ' .customized_select select').eq(next_option).find('option').eq(0).show();
				$(parent_container + ' .customized_select div').eq(next_option).html('Select Item');
			}
		}
	});
		
	// end product options (pop-up)
	
	$(document).on("touchstart click", ".addtocart-details-btn", function(){
		var el = $(this);
		var qty = parseInt(el.parent().find('input[name="qty"]').val());
		var data = $.parseJSON(el.attr('data'));
		var data = {product_user_id:data.p_uid,
					product_id:data.p_id, 
					product_store_id: data.store_id,
					product_store_url: CS.parent_embed_url,
					store_id:data.store_id,
					store_user_id:data.store_uid ,
					quantity: parseInt(qty),
					has_options: data.has_options };
					
					
		if(parseInt(data.has_options) > 0) {
			addtocart(data,'details');
		}
		else {
			addtocart(data);	
		}
	});
	$(document).on("click", ".addtocart-btn", function(){
		var el = $(this);
		var qty = parseInt(el.parent().find('input[name="qty"]').val());
		var data = $.parseJSON(el.attr('data'));
		var data = {product_user_id:data.p_uid,
					product_id:data.p_id, 
					product_store_id: data.store_id,
					product_store_url: CS.parent_embed_url,
					store_id:data.store_id,
					store_user_id:data.store_uid ,
					quantity: parseInt(qty),
					has_options: data.has_options };
					
					
		if(parseInt(data.has_options) > 0) {
			show_selectoptions(data);
		}
		else {
			addtocart(data);	
		}
	});
	
	$(document).on('click', '#cartlisting-wrapper .table .td .qty a', function() {
		var quantity = $(this).parent().find('input').val();
		var m = $(this).parent().find('input').attr('m');
		var p = $(this).parent().find('input').attr('p');
		var variant_id = parseInt($(this).parent().find('input').attr('variant_id'));
		var su = $(this).parent().find('input').attr('su');
		var s = $(this).parent().find('input').attr('s');
		var has_options = 0;
		var product_options = $(this).parent().find('input').attr('options');
		
		if(typeof(variant_id) != 'undefined' && variant_id > 0) {
			has_options = 1;
		}
		
		var data = {product_user_id:m,
					product_id:p, 
					store_id: s,
					store_user_id: su,
					quantity: quantity,
					has_options: has_options,
					variant_id: variant_id,
					product_options:product_options};
		update_cart_quantity(data);
	});
	
	$(document).on('click', '#cartlisting-wrapper .table .td .qty span', function() {		
		var quantity = $(this).parent().find('input').val();
		var m = $(this).parent().find('input').attr('m');
		var p = $(this).parent().find('input').attr('p');
		var variant_id = $(this).parent().find('input').attr('variant_id');
		var su = $(this).parent().find('input').attr('su');
		var s = $(this).parent().find('input').attr('s');
		var has_options = 0;
		var product_options = $(this).parent().find('input').attr('options');
		
		if(typeof(variant_id) != 'undefined' && variant_id > 0) {
			has_options = 1;
		}
		var data = {product_user_id:m,
					product_id:p, 
					store_id: s,
					store_user_id: su,
					quantity: quantity,
					has_options: has_options,
					variant_id: variant_id,
					product_options:product_options};
		delete_cart_product(data);
	});
	
	$(document).on('click', '#cartlisting-wrapper .table .td .qty em.link-delete', function(e) {	
		e.stopImmediatePropagation();	
		var quantity = $(this).attr('q');
		var m = $(this).attr('m');
		var p = $(this).attr('p');
		var variant_id = $(this).attr('variant_id');
		var su = $(this).attr('su');
		var s = $(this).attr('s');
		var has_options = 0;
		var product_options = $(this).parent().find('input').attr('options');
		
		if(typeof(variant_id) != 'undefined' && variant_id > 0) {
			has_options = 1;
		}
		var data = {product_user_id:m,
					product_id:p, 
					store_id: s,
					store_user_id: su,
					quantity: quantity,
					has_options: has_options,
					variant_id: variant_id,
					product_options: product_options};
		delete_cart_product(data,'placeorder');
	});
	
	
	$(document).on('change', '#shipto_country', function(e){
		if($(this).val() != '') {
			
			var merchant_note = {};
			$('#cartlisting-wrapper textarea').each(function() {
				var val = $(this).val();
				var id = $(this).attr('name').replace(/merchant_note_/,'');
				merchant_note[id] = val;
				//merchant_note.push($(this).val());	
			});
			$(document).data('co_note',merchant_note);
			store.set('co_note','');
			load_cart({shipto_country:$(this).val()});
		}
	});
	
	// Checkout button 
	$(document).on('click', '#checkout-process', function(e){
		//e.preventDefault(); 
		e.stopImmediatePropagation();
		var merchant_note = {};
		$('#cartlisting-wrapper textarea').each(function() {
			var val = $(this).val();
			var id = $(this).attr('name').replace(/merchant_note_/,'');
			merchant_note[id] = val;
		});
		store.set('co_note',merchant_note);
		store.set('shipto_country',$('#shipto_country').val());
		
		var new_window = false;
		if(new_window) {
			$(this).attr('href',CS.checkoutURL);
		}
		else {
			parent.location.href = CS.checkoutURL + '?shipto_country='+$('#shipto_country').val();

		}
	});
	
	$(document).on('click', '.return-policy-link', function(e){
		e.stopImmediatePropagation();
		e.preventDefault();
		popup_returnpolicy($(this).attr('href'));
	});
	
});

// Textarea auto expand height
(function($){$.fn.TextAreaExpander=function(minHeight,maxHeight){var hCheck=!($.browser.msie||$.browser.opera);function ResizeTextarea(e){e=e.target||e;var vlen=e.value.length,ewidth=e.offsetWidth;if(vlen!=e.valLength||ewidth!=e.boxWidth){if(hCheck&&(vlen<e.valLength||ewidth!=e.boxWidth))e.style.height="0px";var h=Math.max(e.expandMin,Math.min(e.scrollHeight,e.expandMax));e.style.overflow=(e.scrollHeight>h?"auto":"hidden");e.style.height=h+"px";e.valLength=vlen;e.boxWidth=ewidth}return true};this.each(function(){if(this.nodeName.toLowerCase()!="textarea")return;var p=this.className.match(/expand(\d+)\-*(\d+)*/i);this.expandMin=minHeight||(p?parseInt('0'+p[1],10):0);this.expandMax=maxHeight||(p?parseInt('0'+p[2],10):99999);ResizeTextarea(this);if(!this.Initialized){this.Initialized=true;$(this).css("padding-top",0).css("padding-bottom",0);$(this).bind("keyup",ResizeTextarea).bind("focus",ResizeTextarea)}});return this}})(jQuery);
// number format
function number_format(number,decimals,dec_point,thousands_sep){number=(number+'').replace(/[^0-9+\-Ee.]/g,'');var n=!isFinite(+number)?0:+number,prec=!isFinite(+decimals)?0:Math.abs(decimals),sep=(typeof thousands_sep==='undefined')?',':thousands_sep,dec=(typeof dec_point==='undefined')?'.':dec_point,s='',toFixedFix=function(n,prec){var k=Math.pow(10,prec);return''+Math.round(n*k)/k};s=(prec?toFixedFix(n,prec):''+Math.round(n)).split('.');if(s[0].length>3){s[0]=s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g,sep)}if((s[1]||'').length<prec){s[1]=s[1]||'';s[1]+=new Array(prec-s[1].length+1).join('0')}return s.join(dec)}

function getUrlParam( param ){
	param = param.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
	var exp = "[\\?&]"+param+"=([^&#]*)";
	var regexp = new RegExp( exp );	
	var results = regexp.exec( window.location.href );
	if( results == null ){
		return "";
	} else {
		return results[1];
	}
}

function hideShareAndWish(){
	$('#wdgt-sharebox').hide();
	$('#wdgt-wishlistbox').hide();
}
function trimText (str) {
	str = str.replace(/^\s+/, '');
	for (var i = str.length - 1; i >= 0; i--) {
		if (/\S/.test(str.charAt(i))) {
			str = str.substring(0, i + 1);
			break;
		}
	}
	return str;
}

function popUp(URL) {
	day = new Date();
	id = day.getTime();
	eval("page" + id + " = window.open(URL, '" + id + "', 'directories=0,toolbar=0,scrollbars=0,location=0,statusbar=0,menubar=0,resizable=0,width=520,height=364,left=200,top=50');");
}

function loadWishlist(){
	if(typeof store.get('user_id') === 'undefined'){
		return false;
	}
	url = root + 'load-wishlist.html';
	var form_data = {};
	form_data.wishlist_val = $(this).find('#wishtype').val();
	form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
	form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
	
	$.ajax({
		type: "POST",
		url: url,
		data: form_data,
		cache: false,
		success: function(data){
			var _data = $.parseJSON(data);
			$.each(_data, function(i, item) {
				$('.wishlist_'+_data[i].ref_user_id+'_'+_data[i].product_id+'_'+_data[i].store_id+'_'+_data[i].status).addClass('active');
				$('.wishlists_'+_data[i].product_id+'_'+_data[i].store_id+'_'+_data[i].status).addClass('active');
			});
		}
	});
	return false;
}

function popup_returnpolicy(url) {
	var  screenX    = typeof window.screenX != 'undefined' ? window.screenX : window.screenLeft,
	screenY    = typeof window.screenY != 'undefined' ? window.screenY : window.screenTop,
	outerWidth = typeof window.outerWidth != 'undefined' ? window.outerWidth : document.body.clientWidth,
	outerHeight = typeof window.outerHeight != 'undefined' ? window.outerHeight : (document.body.clientHeight - 22),
	width    = 600,
	height   = 500,
	left     = parseInt(screenX + ((outerWidth - width) / 2), 10),
	top      = parseInt(screenY + ((outerHeight - height) / 2.5), 10),
	features = (
			'width=' + width +
			',height=' + height +
			',left=' + left +
			',top=' + top +
			',menubar=no,toolbar=no'
	);
	newwindow = window.open(url,'shopstyxPopUp',features);
	
	if (window.focus) {newwindow.focus()}
	return false;
}

function customized_select() {
	$('.customized_select select').each(function(){
		if($(this).parent().find('div').length == 0) {
			title = $('option:selected',this).text();
			$(this)
				.css({'z-index':10,'opacity':0,'-khtml-appearance':'none'})
				.after('<div>' + title + '</div>')
				.change(function(){
					val = $('option:selected',this).text();
					$(this).next().text(val);
				})
		}
	});
}

function show_selectoptions(data) {
	$('#option-wrapper').find('#preloader-wrapper').show();
	$('#option-wrapper').show();
	
	// Centerize
	var dWidth = $(document).width();
	var dHeight = $(document).height();
	$('#option-wrapper').css({width:dWidth, height:dHeight});
	
	var sWidth = $(window).width(); 
	var sHeight = $(window).height(); 
	var left = $(document).scrollLeft();
	var top = $(document).scrollTop();
	
	var modal = $('#option-content');
	var top = ((sHeight - modal.height()) / 2) + top;
	var left = (sWidth - modal.width()) / 2;
	modal.css({top:top, left:left});
	
	var form_data = {};
		form_data.storeURI = CS.storeURI;

	$.extend(form_data,data);	
	$.post( main_url + 'ajax.html?content=option_selection', data, function(data){
		//$('#preloader-wrapper').hide();
		$('#option-wrapper #option-selection-form').html(data);
		$('#option-wrapper').show();
		$('#option-content .scroller').jScrollPane();
		$('#option-wrapper').find('#preloader-wrapper').hide();
		
		customized_select();
	});
}

function get_cart_data() {
	return store.get('cart');
}

function load_productdetails_cartitems() {
	preLoader($('#checkout-right-wrapper'));
	$.ajax({
		type: "POST",
		url: main_url + "ajax.html?content=details_cartitems",
		data: { user_id: store.get('user_id'),
				token: store.get('token'),
				cart_sess:store.get('cart_sess'), 
				cart: get_cart_data(), 
				storeURI:CS.storeURI 
			},
		crossDomain: true,
		cache: false,
		success: function(response){
			$('.your-items-wrapper.rightbar-cartitems').html(response);
			$('.your-items-wrapper.rightbar-cartitems').show();
			$('.your-items-wrapper.rightbar-cartitems').find('#preloader-wrapper').hide();
			if($.trim(response) == "") {
				$('.your-items-wrapper.rightbar-cartitems').hide();
			}
		}
	})
	//.fail(function(jqXHR, textStatus, errorThrown) { alert("cannot load cart"); });
}

function load_cartitems(data) {
	data = typeof(data) == 'undefined' ? {} : data;
	var form_data = {user_id:store.get('user_id'),
						token:store.get('token'),
						cart_sess:store.get('cart_sess'), 
						cart: get_cart_data(),
						storeURI:CS.storeURI};
	$.extend(form_data,data);
	console.log(form_data)
	preLoader($('#cart_items'));
	$.ajax({
		type: "POST",
		url: main_url + "ajax.html?content=cartitems",
		data: form_data,
		crossDomain: true,
		cache: false,
		success: function(response){
			var _data = $.parseJSON(response);
			var total_item = parseInt(_data.total_item);
			var total_price = parseFloat(_data.total_price);
			
			if(total_item > 1) {
				total_item += " items";	
			}
			else if(total_item == 1) {
				total_item += " item";	
			}
			else {
				total_item = "no item";	
			}

			$('#shopping-text').show();
			$('#shopping-count').html(total_item + ' - $' + number_format(total_price, 2, '.', ','));
			$('#shopping-count').show();
			$('#cart_items').find('#preloader-wrapper').hide();
		}
	})
	//.fail(function(jqXHR, textStatus, errorThrown) { alert("cannot load cart"); });
}

function update_cart_merchant_notes() {
	var merchant_note = {};
	$('#cartlisting-wrapper textarea').each(function() {
		var val = $(this).val();
		var id = $(this).attr('name').replace(/merchant_note_/,'');
		merchant_note[id] = val;
		//merchant_note.push($(this).val());	
	});
	$(document).data('co_note',merchant_note);
	store.set('co_note',merchant_note);
}

function load_cart(data) {
	$('#cart-box-content').hide();
	$('#main-cart-container').find('#preloader-wrapper').show();
	
	data = typeof(data) == 'undefined' ? {} : data;
	if(data.hasOwnProperty('shipto_country') && data.shipto_country != '') {
		data.shipto_country = data.shipto_country;
	}
	else {
		data.shipto_country = store.get('shipto_country');
	}
	
	var form_data = {cart: get_cart_data(),
						cart_sess: store.get('cart_sess'),
						key:store.get('token'),
						user_id:store.get('user_id'),
						m_note:store.get('co_note')};
	
	$.extend(form_data,data);

	$.post(main_url + 'ajax.html?content=cartlisting', form_data,function(data){
		$('#main-cart-container').find('#preloader-wrapper').hide();
		$('#cart-box-content').html(data);
		$(".table textarea").TextAreaExpander(37);	
		$('#cart-box-content').show();
	});
}

function addtocart(data, pagetype) {
	if(typeof(data) != 'object') return;
	
	var product_user_id = data.product_user_id; // product owner or merchant
	var product_id = data.product_id;
	var store_id = data.store_id;
	var store_user_id = data.store_user_id; // endorser or store owner
	var quantity = data.quantity;
	var has_options = data.has_options;
	data.product_options = {};
	
	// get existing cart values
	var cart = get_cart_data();
	cart = (typeof(cart) != 'object' || cart == null) ? {} : cart;

	//console.log(cart.merchants[user_id].products.hasOwnProperty(1));
	//console.log(cart);
	// retreive existing 
	var item, option = '';
	var error_option = false;
	var variants_values = new Array();
	
	/*
	for (var key in cart) {
		if (cart.hasOwnProperty(key)) {
			total_merchant++;
			new_cart[key] = cart[key]
		}
	}
	*/
	var option_wrapper_name = "";
	if(pagetype == "details") {
		option_wrapper_name = ".details-option-value-wrapper";
	}
	else {
		option_wrapper_name = ".option-value-wrapper";
	}
	// Option Validations
	$(option_wrapper_name).each(function() {
		if($(this).find('select').val() == '') {
			error_option = true;	
		}
		
		data.product_options[$(this).attr('option_id')] = $(this).find('select').val();
		variants_values.push($(this).find('select').val());
		
	});

	var options_string = variants_values.join(',');
	var variants = $.parseJSON($('#unique_variants').attr('variants'));
	var variant_id = 0;
	for (var key in variants) {
		if (variants.hasOwnProperty(key)) {
			if(variants[key] == options_string) {
				variant_id = key;	
			}
		}
	}
	
	if(error_option || (has_options > 0 && variant_id == 0)) {
		
		alert('Please select an option.');
		return;	
	}
	data.variant_id = variant_id;
	data.cart = cart;
	data.cart_sess = store.get('cart_sess');
	data.token = store.get('token');
	data.user_id = store.get('user_id');

	//console.log(data); return;
	//preLoader($('#ms-wrapper'));
	$.ajax({
		type: "POST",
		url: main_url + 'ajax.html?content=updatecart',
		data: data,
		cache: false,
		success: function(res){
			var result = $.parseJSON(res);
			store.set('cart',result.cart);
			store.set('cart_sess',result.cart_sess);
			location.href =  main_url + 'cart.html';
		}
	});
}

function update_cart_quantity(data) {
	$('#cart-box-content').hide();
	$('#main-cart-container').find('#preloader-wrapper').show();
	
	if(typeof(data) != 'object') return;
	/*
	var product_user_id = data.product_user_id; // product owner or merchant
	var product_id = data.product_id;
	var store_id = data.store_id;
	var store_user_id = data.store_user_id; // endorser or store owner
	var quantity = data.quantity;
	var has_options = data.has_options;
	*/

	// get existing cart values
	var cart = get_cart_data();
	cart = (typeof(cart) != 'object' || cart == null) ? {} : cart;
	data.cart = cart;
	data.cart_sess = store.get('cart_sess');
	data.token = store.get('token');
	data.user_id = store.get('user_id');
	
	//preLoader($('#ms-wrapper'));
	$.ajax({
		type: "POST",
		url: main_url + 'ajax.html?content=updatecart&update=new_quantity',
		data: data,
		cache: false,
		success: function(res){
			var result = $.parseJSON(res);
			
			store.set('cart',result.cart);
			store.set('cart_sess',result.cart_sess);

			load_cartitems();
			load_cart({shipto_country:$('#shipto_country').val()});
		}
	});
}

function delete_cart_product(data,reloadtype) {
	if(reloadtype == 'placeorder') {
		mainpreLoader();
	}
	else {
		$('#cart-box-content').hide();
		$('#main-cart-container').find('#preloader-wrapper').show();
	}
	
	if(typeof(data) != 'object') return;

	// get existing cart values
	var cart = get_cart_data();
	cart = (typeof(cart) != 'object' || cart == null) ? {} : cart;
	data.cart = cart;
	data.cart_sess = store.get('cart_sess');
	data.token = store.get('token');
	data.user_id = store.get('user_id');

	//preLoader($('#ms-wrapper'));
	$.ajax({
		type: "POST",
		url: main_url + 'ajax.html?content=updatecart&update=delete_item',
		data: data,
		cache: false,
		success: function(res){
			var result = $.parseJSON(res);
			store.set('cart',result.cart);
			store.set('cart_sess',result.cart_sess);
			
			load_cartitems();
			load_cart({shipto_country:$('#shipto_country').val()});
		}
	});
}

/* Returns values (in json or url querystring format):
 * 		Logged user: user_id and token
 *		Store info: store_key (which identifies store owner and store_id
 */
function ms_getQueryParams(type) {
	type = typeof(type) == 'undefined' ? 0 : 1;
	var params = eval( {login_user_id:store.get('user_id'),
						token:store.get('token'),
						store_id:store.get('store_id'),
						store_url: CS.parent_embed_url,
						cart:get_cart_data(),
						m_note:store.get('co_note'),
						storeURI:CS.storeURI
						} );
	var data = '';
	var value;
	
	// Json format
	if(!type) {
		data = params;		
	}
	// Serialize format
	else {
		for(var d in params) {
			if(params.hasOwnProperty(d)) {
				value = typeof(params[d]) == 'undefined' ? '' : params[d];
				data += '&' + d + '=' + value;
			}
		}
	}
	
	return data;
}

//allowed domains
var whitelist_domains = ["shopstyx.com","www.shopstyx.com", "devtest.shopstyx.com", "devtest.brandbazaar.com", "shopstyx.local"];
function verifyOrigin(origin){
	var domain = origin.replace(/^https?:\/\/|:\d{1,4}$/g, "").toLowerCase(),
		i = 0,
		len = whitelist_domains.length;

	while(i < len){
		if (whitelist_domains[i] == domain){
			return true;
		}
		i++;
	}

	return false;
}	
	
function onMessage_cart(messageEvent) {
	// order success
	var regex = new RegExp( '\\bstep=ordersuccess\\b' );
	var result = regex.test( location.href );
	if(result) {
		return;	
	}
	
	if (verifyOrigin(messageEvent.origin)){
		if(messageEvent.data.hasOwnProperty('action')) {
			if(messageEvent.data.action == "parent_url_load") {
				location.href = messageEvent.data.parent_url_load;
			}
			else {
				location.href = '/checkout.html?step=ordersuccess#'+messageEvent.data.checkout_data.o+'_'+messageEvent.data.checkout_data.u+'_'+messageEvent.data.checkout_data.e;
				//alert('checkout success' + location.href);
			}
			setTimeout(function() {} , 100);
			return;
		}
		else {
			// Setting up the cart data from the main site to the webstore
			if($.isEmptyObject(messageEvent.data.cartitems)) {
				set_cart_data({});
			}
			else {
				set_cart_data(messageEvent.data.cartitems);
				//console.log(messageEvent.data.cartitems);
			}
	
			if(messageEvent.data.hasOwnProperty('type') && messageEvent.data.type == "checkout_shippingbilling") {
				load_rightbar_cartitems();
				load_checkout();
			}
			else {
				
				// Load cart listing only if the current page is on cart.html
				var regex = new RegExp( '\\bcart.html\\b' );
				var result = regex.test( location.href );
				if(result) {
					load_cart({shipto_country:getUrlParam('shipto_country')}); 
				}
				
				var regex = new RegExp( '\\bcheckout.html\\b' );
				var result = regex.test( location.href );
				if(result) {
					load_rightbar_cartitems();
					load_checkout();
				}
			}
			
			// Load Total Cart Items and amount found in the header
			load_cartitems();
			
		}
	}
	return;
}

