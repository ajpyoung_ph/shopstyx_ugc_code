var	cloud_url = CS.cloudURL || location.protocol + '//' + location.host + '/';
var	root = CS.siteURL || location.protocol + '//' + location.host + '/';
var vid = CS.vid;
var doNotLoad = false;
var ms_store_key = CS.store_key;
var ms_store_user_id = 0;
var ms_store_id = 0;

$(document).ready(function(){
	$(document).on('click','#ms-hp', function() {
		ministore_getMainPage(1);
	});
	
	$(document).on('click', '.ms-category-name, #ms-cat-lists .ms-category', function(e){
		e.stopImmediatePropagation();
		var data = {};
			data.catname = $(this).find('.ms-cname').html();
			data.total_products = $(this).find('.ms-cat-quantity').html() == null ? $(this).parents('.ms-category').find('.ms-cat-quantity').html() : $(this).find('.ms-cat-quantity').html();
			data.store_category_id = $(this).attr('cid');
		ministore_gotoProductLists(data, 1);
	});
	
	$(document).on('click', '.ms-sub-cname', function(e){
		e.stopImmediatePropagation();
		var data = {};
			data.catname = $(this).find('.subcat_N').html();
			data.total_products = $(this).find('.subcat_total').val();
			data.store_category_id = $(this).attr('cid');
		ministore_gotoProductLists(data, 1);
	});
	
	$(document).on('mouseenter', '.ms-category-slide', function(e){
		e.preventDefault();
		var $this = $(this);
		$(this).find('.ms-cat-top').slideUp(400, function(){
			$this.find('.ms-subcat-list').fadeIn(100);
		});
	});
	
	$(document).on('mouseenter', '.ms-subcat-list', function(e){
		$(this).parents('.ms-category-slide').find('.ms-cat-top').slideUp("fast");
	});
	
	// $(document).on('click', '.ms-subcat-list', function(e){
		// $(this).find('li:nth-child(1)').fadeOut("slow");
	// });
			  
	$(document).on('mouseleave', '.ms-category-slide', function(e){
		e.preventDefault();
		var $this = $(this);
		$(this).find('.ms-cat-top').slideDown("fast", function(){
			$this.find('.ms-subcat-list').fadeOut('fast');
		});
		$(this).find('.ms-subcat-list').fadeOut('fast', function(){
			$this.find('.ms-cat-top').slideDown('fast');
		});
	});
	
	$(document).on('mouseenter', '#ms-footer', function(){
		$('.ms-subcat-list').fadeOut('fast');
	});
	
	$(document).on('click', '.ms-back', function(){
		var page = $('#catpage').val();
		ministore_getMainPage(page);
	});
	
	$(document).on('click', '.ms-more-details, .pst', function(){
		ministore_getProductDetails($(this));
	});
	
	$(document).on('click','.table .td .details .pname', function() {
		ministore_getProductDetails($(this));
	});
	
	$(document).on('click','.table .td .thumb', function() {
		ministore_getProductDetails($(this));
	});
	
	$(document).on('click', '.ms-back-to-list, .continueshopping', function(e){
		e.stopImmediatePropagation();
		preLoader($('#ms-wrapper'));
		
		if ($('#searchtext').val() != "") {
			var error_status = 0;
			var form_data = {};
				form_data.catname = $('#catname').val();
				form_data.total_products = $('#total_products').val();
				form_data.store_category_id = $('#ms_cid').val();
				form_data.catpage = $('#catpage').val();
				form_data.ms_search_text = $('#searchtext').val();
				form_data.ms_search_products_option = $('#products_option').val();
				
			$.extend(form_data,ms_getQueryParams());
				
			var searchpage = 0;
			if($('#searchpage').val() != ""){
				searchpage = $('#searchpage').val();
			}
			$('#ms-wrapper').data('data',form_data);
			$.post(root + 'mini-store/search.html?page=' + searchpage, form_data,function(data){ 
				$('#ms-wrapper').html(data);
				$('#preloader-wrapper').hide();
			});
			
			return false;
		}else if($('#catname').val() == '') {
			var page = $('#catpage').val();
			ministore_getMainPage(page);
		}else{
			var page = $('#prodpage').val();
			var data = {};
				data.catname = $('#catname').val();
				data.total_products = $('#total_products').val();
				data.store_category_id = $('#ms_cid').val();
			ministore_gotoProductLists(data, page);
		}
	});
	
	$(document).on('click', '.ms-cancel-link', function(){
		$('#ms-search-container').remove();
	});
	$(document).on('click', '.c-thumb-image', function(){
		$('#ms-search-container').remove();
	});
	
	$(document.body).on('click', '.photo-btn', function(){
		$('#video-photos-btn-wrapper span').removeClass('selected');
		$(this).addClass('selected');
		$('#video-wrapper').addClass('hide');
		$('#details-slideshow-wrapper').removeClass('hide');
	});
	
	$(document.body).on('click', '.video-btn', function(){
		$('#video-photos-btn-wrapper span').removeClass('selected');
		$(this).addClass('selected');
		$('#details-slideshow-wrapper').addClass('hide');
		$('#video-wrapper').removeClass('hide');
	});
	
	$(document).on('click', '#wdgt-wishlistbox ul li', function(){
		if(typeof store.get('user_id') === 'undefined' || store.get('user_id') <= 0){
			$('#ms-header-login').trigger('click');
			return false;
		}
		url = root + 'add-to-wishlist.html';
		$this = $(this);
		var form_data = {};
		form_data.wishlist_val = $(this).find('#wishtype').val();
		form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
		form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
		$this.parents('#wdgt-wishlistbox').find('#preloader-wrapper').show();
		$.ajax({
			type: "POST",
			url: url,
			data: form_data,
			cache: false,
			success: function(data){
				if(!data){ $this.parents('#wdgt-wishlistbox').find('#preloader-wrapper').hide(); return false; }
				var _data = $.parseJSON(data);
				$.each(_data, function(i, item) {
					$('.wishlist_'+_data[i].ref_user_id+'_'+_data[i].product_id+'_'+_data[i].store_id+'_1').removeClass('active');
					$('.wishlist_'+_data[i].ref_user_id+'_'+_data[i].product_id+'_'+_data[i].store_id+'_2').removeClass('active');
					$('.wishlist_'+_data[i].ref_user_id+'_'+_data[i].product_id+'_'+_data[i].store_id+'_3').removeClass('active');
					$('.wishlist_'+_data[i].ref_user_id+'_'+_data[i].product_id+'_'+_data[i].store_id+'_'+_data[i].status).addClass('active');
					$('.wishlists_'+_data[i].product_id+'_'+_data[i].store_id+'_1').removeClass('active');
					$('.wishlists_'+_data[i].product_id+'_'+_data[i].store_id+'_2').removeClass('active');
					$('.wishlists_'+_data[i].product_id+'_'+_data[i].store_id+'_3').removeClass('active');
					$('.wishlists_'+_data[i].product_id+'_'+_data[i].store_id+'_'+_data[i].status).addClass('active');
					$this.parents('#wdgt-wishlistbox').find('#preloader-wrapper').hide();
				});
			}
		});
		return false;
	});
	
	$(document).on('click', '.ms_cat_page', function(){
		var form_data = {};
			form_data.key = $('#key').val();
			form_data.catname = $(this).find('.ms-cname').html();
			form_data.total_products = $(this).find('.ms-cat-quantity').html() == null ? $(this).parents('.ms-category').find('.ms-cat-quantity').html() : $(this).find('.ms-cat-quantity').html();
			form_data.store_category_id = $(this).attr('cid');
			form_data.store_name = $('#ms_storename').val();
			form_data.store_tag = $('#ms_storetag').val();
			form_data.user_id = $('#ms_uid').val();
			form_data.store_id = $('#ms_sid').val();
			
		$.extend(form_data,ms_getQueryParams());
		preLoader($('#ms-wrapper'));	
		$('#ms-wrapper').data('data',form_data);
		var page;
		if($(this).attr('id') == "page_link"){
			page = $(this).attr('href');
		}else{
			page = root + "mini-store/main.html";
		}
		
		$.post( page, form_data,function(data){
			$('#ms-wrapper').html(data);
			$('#preloader-wrapper').hide();
		});
		
		return false;
	});
	
	$(document).on('click', '.ms_prod_page', function(event){
		event.stopImmediatePropagation();
		var form_data = {};
			form_data.key = $('#key').val();
			form_data.catname = $('#catname').val();
			form_data.total_products = $('#total_products').val();
			form_data.store_category_id = $('#ms_cid').val();
			form_data.store_name = $('#ms_storename').val();
			form_data.store_tag = $('#ms_storetag').val();
			form_data.user_id = $('#ms_uid').val();
			form_data.store_id = $('#ms_sid').val();
			form_data.catpage = $('#catpage').val();
			
		$.extend(form_data,ms_getQueryParams());
		preLoader($('#ms-wrapper'));
		$('#ms-wrapper').data('data',form_data);
			
		var page;
		if($(this).attr('id') == "page_link"){
			page = $(this).attr('href');
		}else{
			page = root + "mini-store/product-listing.html";
		}
		$.post( page, form_data,function(data){
			$('#ms-wrapper').html(data);
			$('#preloader-wrapper').hide();
		});
		return false;
	});
	
	$(document).on('keypress', '.ms-search', function(e){
		if (e.which == 13) {
			$(this).removeClass('input_error');
			var error_status = 0;
			var form_data = {};
				form_data.catname = $('#catname').val();
				form_data.total_products = $('#total_products').val();
				form_data.store_category_id = $('#ms_cid').val();
				form_data.catpage = $('#catpage').val();
				form_data.ms_search_text = $.trim($(this).val());
				form_data.ms_search_products_option = $('.search_products_option').val();
			
			$.extend(form_data,ms_getQueryParams());
			
			$('#ms-wrapper').data('data',form_data);
				
			if(form_data.ms_search_text == ""){
				$(this).addClass('input_error');
				error_status++;
				return false;
			}
			
			store.set('ms_search_text', form_data.ms_search_text);
			store.set('ms_search_products_option', form_data.ms_search_products_option);
			preLoader($('#ms-wrapper'));
			$.post(root + 'mini-store/search.html', form_data,function(data){ 
				$('#ms-wrapper').html(data);
				$('#preloader-wrapper').hide();
			});
			
			return false;
		}
	});
	
	$(document).on('click', '.ms-search-btn', function(e){
		$('.ms-search').removeClass('input_error');
		var error_status = 0;
		var form_data = {};
			form_data.catname = $('#catname').val();
			form_data.total_products = $('#total_products').val();
			form_data.store_category_id = $('#ms_cid').val();
			form_data.catpage = $('#catpage').val();
			form_data.ms_search_text = trimText($('.ms-search').val());
			form_data.ms_search_products_option = $('.search_products_option').val();
		$.extend(form_data,ms_getQueryParams());
		
		$('#ms-wrapper').data('data',form_data);
			
		if(form_data.ms_search_text == ""){
			$('.ms-search').addClass('input_error');
			error_status++;
			return false;
		}
		
		store.set('ms_search_text', form_data.ms_search_text);
		store.set('ms_search_products_option', form_data.ms_search_products_option);
		preLoader($('#ms-wrapper'));
		$.post(root + 'mini-store/search.html', form_data,function(data){ 
			$('#ms-wrapper').html(data);
			$('#preloader-wrapper').hide();
		});
		
		return false;
	});
	
	$(document).on('click', '.ms_search_page', function(e){
		
		$(this).removeClass('input_error');
		var error_status = 0;
		var form_data = {};
			form_data.key = $('#key').val();
			form_data.catname = $('#catname').val();
			form_data.total_products = $('#total_products').val();
			form_data.store_category_id = $('#ms_cid').val();
			form_data.store_name = $('#ms_storename').val();
			form_data.store_tag = $('#ms_storetag').val();
			form_data.user_id = $('#ms_uid').val();
			form_data.store_id = $('#ms_sid').val();
			form_data.catpage = $('#catpage').val();
			form_data.ms_search_text = $('#searchtext').val();
			form_data.ms_search_products_option = $('#products_option').val();
		
		$.extend(form_data,ms_getQueryParams());
		preLoader($('#ms-wrapper'));
		$('#ms-wrapper').data('data',form_data);
		
		var page;
		if($(this).attr('id') == "page_link"){
			page = $(this).attr('href');
		}else{
			page = root + "mini-store/search.html";
		}

		$.post( page, form_data,function(data){
			$('#ms-wrapper').html(data);
			$('#preloader-wrapper').hide();
		});
		return false;
		
	});
	
	// add to cart
	$(document).on('click','.addtocart-btn', function(e) {
		e.stopImmediatePropagation();	
		var el = $(this);
		var qty = parseInt(el.parent().find('input[name="qty"]').val());
		var data = $.parseJSON(el.attr('data'));
		var data = {product_user_id:data.p_uid,
					product_id:data.p_id, 
					product_store_id: data.store_id,
					product_store_url: CS.parent_embed_url,
					store_id:data.store_id,
					store_user_id:data.store_uid ,
					quantity: parseInt(qty),
					has_options: data.has_options };
		if(parseInt(data.has_options) > 0) {
			preLoader($('#ms-wrapper'));
			$.extend(data,ms_getQueryParams());
			show_modal({ overlay: true,
						 overlay_class: '',
						 modal: '#ms-modal-window',
						 modal_class: 'option',
						 ajaxURL: true,
						 data: data,
						 onBeforeLoad: function() {
			
						 },
						 onAfterLoad: function() {
							var jSpane_modal_window = $('#ms-modal-window .scroller');
							jSpane_modal_window.jScrollPane();
							jSpane_api_modal_window = jSpane_modal_window.data('jsp');
							$('#preloader-wrapper').hide();
						 }
						});
		}
		else {
			addtocart(data);	
		}
		
	});
	
	/* Product options */
	$(document).on('click', '#ms-modal-window .btn-close', hide_modal);
	$(document).on('click','#ms-overlay', hide_modal);
	$(document).on('change','.ms-mainoption-value-wrapper .customized_select select, .details-option-value-wrapper .customized_select select', function() {
		var parent_container = ($(this).parent().parent().hasClass('details-option-value-wrapper')) ? '.details-option-value-wrapper': '.ms-mainoption-value-wrapper';
		var variants_values = $.parseJSON($('#unique_variants').attr('variants_values'));
		var unique_variants = $.parseJSON($('#unique_variants').val());
		var index = parseInt($(parent_container + ' .customized_select select').index(this));
		var value = $(this).val();
		var next_option = (index + 1);

		if(index == 0) {
			$(parent_container + ' .customized_select').eq(1).addClass('disabled');	
			$(parent_container + ' .customized_select').eq(2).addClass('disabled');	
			$(parent_container + ' .customized_select select').eq(1).val('');
			$(parent_container + ' .customized_select select').eq(2).val('');
			$(parent_container + ' .customized_select select').eq(1).attr('disabled','disabled');	
			$(parent_container + ' .customized_select select').eq(2).attr('disabled','disabled');	
			$(parent_container + ' .customized_select div').eq(1).html('Select Item');
			$(parent_container + ' .customized_select div').eq(2).html('Select Item');
			
			$(this).parent().removeClass('disabled');
			$(this).removeAttr('disabled');
		}

		if(typeof $(parent_container + ' .customized_select select').eq(next_option) != "undefined" && value != "") {
			$(parent_container + ' .customized_select select').eq(next_option).find('option').hide();
			$(parent_container + ' .customized_select select').eq(next_option).find('option').eq(0).show();
			$(parent_container + ' .customized_select select').eq(next_option).removeAttr('disabled');
			$(parent_container + ' .customized_select select').eq(next_option).parent().removeClass('disabled');
			$(parent_container + ' .customized_select select').eq(next_option).val('');
			$(parent_container + ' .customized_select div').eq(next_option).html('Select Item');
			//console.log(variants_values);
			for(i = 0; i < variants_values.length; i++) {
				if(variants_values[i][index] == value) {
					$(parent_container + ' .customized_select select').eq(next_option).find('option[value='+variants_values[i][next_option]+']').show();	
				}
			}
			
		}
		else {
			if(index > 0 && value == '') {
				$(parent_container + ' .customized_select select').eq(next_option).parent().addClass('disabled');
				$(parent_container + ' .customized_select select').eq(next_option).attr('disabled','disabled');
				$(parent_container + ' .customized_select select').eq(next_option).find('option').hide();
				$(parent_container + ' .customized_select select').eq(next_option).val('');
				$(parent_container + ' .customized_select select').eq(next_option).find('option').eq(0).show();
				$(parent_container + ' .customized_select div').eq(next_option).html('Select Item');
			}
		}
	});
	
	// Cart
	$(document).on('click', '.table .td .qty a', function() {
		var quantity = $(this).prev().val();
		var m = $(this).prev().attr('m');
		var p = $(this).prev().attr('p');
		var variant_id = $(this).parent().find('input').attr('variant_id');
		var su = $(this).prev().attr('su');
		var s = $(this).prev().attr('s');
		var has_options = 0;
		var product_options = $(this).parent().find('input').attr('options');
		
		if(typeof(variant_id) != 'undefined' && variant_id > 0) {
			has_options = 1;
		}

		var data = {product_user_id:m,
					product_id:p, 
					store_id: s,
					store_user_id: su,
					quantity: quantity,
					has_options: has_options,
					variant_id: variant_id,
					product_options:product_options };
		preLoader($('#ms-wrapper'));
		update_cart_quantity(data);
	});
	$(document).on('click', '.table .td .qty span', function() {
		var m = $(this).attr('m');
		var p = $(this).attr('p');
		var variant_id = $(this).parent().find('input').attr('variant_id');;
		var su = $(this).attr('su');
		var s = $(this).attr('s');
		var product_options = $(this).parent().find('input').attr('options');
		var has_options = 0;
		
		if(typeof(variant_id) != 'undefined' && variant_id > 0) {
			has_options = 1;
		}

		var data = {product_user_id:m,
					product_id:p, 
					store_id: s,
					store_user_id: su,
					has_options: has_options,
					variant_id: variant_id,
					product_options:product_options};
		preLoader($('#ms-wrapper'));
		delete_cart_product(data);
	});
	$(document).on('mouseup', "textarea", function() {
		jSpane_api_cartlisting.reinitialise();
	});
	$(document).on('keyup', "textarea", function() {
		jSpane_api_cartlisting.reinitialise();
	});
	$(document).on('change', '#shipto_country', function(e){
		if($(this).val() != '') {
			
			var merchant_note = {};
			$('.ms-merchant-wrapper textarea').each(function() {
				var val = $(this).val();
				var id = $(this).attr('name').replace(/merchant_note_/,'');
				merchant_note[id] = val;
				//merchant_note.push($(this).val());	
			});
			$(document).data('co_note',merchant_note);
			
			load_cart({shipto_country:$(this).val()});
		}
	});
	
	// Checkout button 
	$(document).on('click', '#checkout-process', function(){
		/*
		var merchant_note = {};
		$('.ms-merchant-wrapper textarea').each(function() {
			var val = $(this).val();
			var id = $(this).attr('name').replace(/merchant_note_/,'');
			merchant_note[id] = val;
		});
		$(document).data('co_note',merchant_note);
		store.set('shipto_country',$('#shipto_country').val());
		
		ministore_checkout('shipping');
		*/
		
		var newtab = window.open();
		store.set('shipto_country',$('#shipto_country').val());
		newtab.location = $(this).attr('url') + '#&shipto_country='+$('#shipto_country').val();
	});
	$(document).on('click', '.ms-checkout-header #close', function() {
		$('#ms-checkout-inner-wrapper').html('');
		$('#ms-checkout-wrapper').hide();
		preLoader($('#ms-wrapper'));
		$('#preloader-wrapper').hide();
	});
	
	// Card holder's name
	$(document).on('keyup','#billing_firstname, #billing_lastname', function() {
		var name = $.trim($('#billing_firstname').val()) + ' ' + $.trim($('#billing_lastname').val());
		$('#cc_name').val(name);
	});
	
	// auto complete
	$(document).on('change','select#shipping_country, select#billing_country', function(e) {
		e.stopImmediatePropagation();
		var id = $(this).attr('id');
		var c = $(this).val();
		var type, state_code, state_name, city, zip;
		if(id == 'shipping_country') {
			type = 'shipping';
			state_code = 'shipping_state_code';
			state_name = 'shipping_state_name';
			city = 'shipping_city';
			zip = 'shipping_zip';
		}
		else {
			type = 'billing';
			state_code = 'billing_state_code';
			state_name = 'billing_state_name';
			city = 'billing_city';
			zip = 'billingg_zip';
		}
		$('input#'+city).val('');
		$('input#'+zip).val('');
		
		if(c == 'US') {
			$('input#'+state_name).hide();
			$('select#'+state_code).show();
			
			if($('select#shipping_state_code').val() != '') {
				toggle_disable_autocomplete('enable',type);
			}
			else {
				toggle_disable_autocomplete('disable',type);
			}
		}
		else {
			$('input#'+state_name).show();
			$('select#'+state_code).hide();
			
			toggle_disable_autocomplete('disable',type); // disable auto complete for Non-US state	
		}
	});

	$(document).on('change','select#shipping_state_code, select#billing_state_code', function(e) {
		e.stopImmediatePropagation();
		var id = $(this).attr('id');
		var city, zip;
		if(id == 'shipping_state_code') {
			type = 'shipping';
			city = 'shipping_city';
			zip = 'shipping_zip';
		}
		else {
			type = 'billing';
			city = 'billing_city';
			zip = 'billing_zip';
		}
		
		$('input#'+city).val('');
		$('input#'+zip).val('');
	
		if($.trim($(this).val()) == '') {
			toggle_disable_autocomplete('disable',type);
		}
		else {
			toggle_disable_autocomplete('enable',type);
		}
		
	});
	
	// Save for login user and proceed to billing info; for guest user just proceed to billing info
	$(document).on('click', '#shipaddress-form', function(e) {
		e.stopImmediatePropagation();
		ms_shiptoaddress('shipping');
	});
	$(document).on('click', '#ms-co-footer-billing .back', function(e) {
		e.stopImmediatePropagation();
		ms_co_step1();
	});
	
	
	// For registered users
	// add new shipping address
	$(document).on('click', '#addnewshipping', function(e){
		e.stopImmediatePropagation();
		$('#cp-shippinglist-form').hide();
		$('#cp-shipping-form').show();
		
		$('#cp-shipping-form input').each(function() {
			$(this).val('');
		});
		
		$('#cp-shipping-form').find('select#shipping_country option').eq(0).attr('selected','selected');
		$('#cp-shipping-form').find('select#shipping_state_code option').eq(0).attr('selected','selected');
		$('#cp-shipping-form').find('select#shipping_state_code').hide();
		$('#cp-shipping-form').find('input#shipping_state_name').show();

		$(document).data('shipping_address_id',0);
		$('.checkoutprocess-wrapper.shipping.list .scrollbar').jScrollPane();
	});
	
	// edit shipping address button
	$(document).on('click', '#editshippingaddress', function(e){
		e.stopImmediatePropagation();
		var id = $(this).parent().find('.address_id').val();
		ms_edit_shipping(id);
	});
	// delete shipping address button
	$(document).on('click', '#deleteshippingaddress', function(e){
		e.stopImmediatePropagation();
		var id = $(this).parent().find('.address_id').val();
		ms_delete_shipping(id);
	});
	// ship to address in shipping address book 
	$(document).on('click', '#shiptoaddress', function(e){
		e.stopImmediatePropagation();
		var id = $(this).parent().find('.address_id').val();
		$(document).data('shipping_address_id',id);
		
		ms_co_step2();
	});
	
	// Save(add/edit) address
	$(document).on('click', '#saveshippingaddress', function(e){
		e.stopImmediatePropagation();
		ms_shiptoaddress('shipping','shippingaddress');
	});
	$(document).on('click', '#cancelsaveshippingaddress', function(e){
		e.stopImmediatePropagation();
		$(document).data('shipping_address_id',0);
		$('#cp-shippinglist-form').show();
		$('#cp-shipping-form').hide();
	});
	
	// -------- billing -----------
	$(document).on('click', '#addbillinginfo', function(e){
		e.stopImmediatePropagation();
		
		$('#billing-editing-form input').each(function() {
			$(this).val('');
		});
		
		$('#billing-editing-form').find('select#billing_country option').eq(0).attr('selected','selected');
		$('#billing-editing-form').find('select#billing_state_code option').eq(0).attr('selected','selected');
		$('#billing-editing-form').find('select#billing_state_code').hide();
		$('#billing-editing-form').find('input#billing_state_name').show();
		
		$('#billing-editing-form').find('select#cc_type option').eq(0).attr('selected','selected');
		$('#billing-editing-form').find('select#cc_expire_month option').eq(0).attr('selected','selected');
		$('#billing-editing-form').find('select#cc_expire_year option').eq(0).attr('selected','selected');

		$(document).data('billing_address_id',0);
		$("#billing-selection-form").hide();
		$("#ms-co-footer-billing button").addClass('disabled');
		$("#billing-editing-form").show();
		$('.checkoutprocess-wrapper.billing .scrollbar').jScrollPane();
	});
	
	$(document).on('click', '#editbillinginfo', function(e){
		e.stopImmediatePropagation();
		var id = $('#billing_address_id').val();
		ms_edit_billing(id);
	});
	
	
	$(document).on('click', '#cancelsavebillingaddress', function(e){
		e.stopImmediatePropagation();
		$(document).data('billing_address_id',0);
		$('#billing-editing-form').hide();
		$("#ms-co-footer-billing button").removeClass('disabled');
		$('#billing-selection-form').show();
		$('.checkoutprocess-wrapper.billing .scrollbar').jScrollPane();
	});
	$(document).on('click', '#savebillingaddress', function(e){
		e.stopImmediatePropagation();
		ms_shiptoaddress('billing');
	});
	
	$(document).on('change', '#billing_address_id', function(e){
		e.stopImmediatePropagation();
		var data = $(document).data('co_billing_list');
		var id = $(this).val();
		data = data[id];
		
		var html = 		data.city + ', ' + data.state_name + ' ' + data.zip + '<br /> ' +
						data.country_name + ' <br /> ' +
						'Phone: ' + data.phone + '<br /> \
                        <h3 class="color1"> \
                            <span class="float-left">Payment Details</span>  \
                            <span class="clearfix"></span> \
                        </h3> \
                        <div id="ccdetails-wrapper"> \
                            <p> \
                                <label>Card Type &amp; Number:</label> \
                                <strong>' + data.card.cc_type + ' ************' + data.card.cc_number_short + '</strong> \
                            </p> \
                            <span class="clearfix"></span> \
                            <p> \
                                <label>Cardholder\s Name:</label> ' + data.card.cc_firstname  + ' ' + data.card.cc_lastname + '\
                            </p> \
                            <span class="clearfix"></span> \
                            <p> \
                                <label>Expiration Date:</label> ' + data.card.cc_expire_month  + ' / ' + data.card.cc_expire_year + ' \
                            </p> \
                            <span class="clearfix"></span> \
                        </div>';
		$(document).data('billing_address_id',id);
		$('#billinginfo-details-wrapper').html(html);
	});
	
	$(document).on('click', '#ms-co-footer-billing button', function(e){
		e.stopImmediatePropagation();
		
		//if(!$(this).hasClass('disabled')) {
		if(store.get('user_id') > 0 && $('#billing-selection-form').length) { 
			if($('#billing_address_id option').length > 0) {
				$(document).data('billing_address_id',$('#billing_address_id').val());
				ms_co_step3();
			}
			else {
				ms_shiptoaddress('billing','',1);
			}
		}
		else {
			ms_shiptoaddress('billing');	
		}

	});
	
	// Review and Place order page
	$(document).on('click', '#changeshippinginfo', function(e){
		ministore_checkout('shipping',$(document).data('co_selected_SABAPD'));
	});
	$(document).on('click', '#changebillinginfo', function(e){
		ministore_checkout('billing',$(document).data('co_selected_SABAPD'));
	});
	
	$(document).on('change', '.ms-merchant-header select', function(e){
		e.stopImmediatePropagation();
		e.preventDefault();
		update_cart_shipping_selection($(this).attr('mid'),$(this).val(),$(this).attr('shipto_country'));
	});
	
	$(document).on('click', '#ms-btn-placeorder', function(e){
		e.stopImmediatePropagation();
		purchase_order();
	});
	
	// Main header
	$(document).on('click', '#ms-header-cart', load_cart);
	$(document).on('click', '#ms-header-login', ministore_csLogin);
	$(document).on('click', '#ms-header-logout', ministore_csLogout);
	$(document).on('click', '#ms-header-account .submenu .socials.fb', ministore_fbLogin);
	$(document).on('click', '#ms-header-account .submenu .socials.yahoo', ministore_yahooLogin);
	$(document).on('click', '#ms-header-account .submenu .socials.gplus', ministore_gplusLogin);
	$(document).on("hover", "#ms-header-account", function(e) {
		if(e.type == 'mouseenter') { $(this).find('.submenu').show();}
		else { $(this).find('.submenu').hide(); }
	});
});


function loadWishlist(){
	if(typeof store.get('user_id') === 'undefined'){
		return false;
	}
	url = root + 'load-wishlist.html';
	var form_data = {};
	form_data.wishlist_val = $(this).find('#wishtype').val();
	form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
	form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
	
	$.ajax({
		type: "POST",
		url: url,
		data: form_data,
		cache: false,
		success: function(data){
			var _data = $.parseJSON(data);
			$.each(_data, function(i, item) {
				$('.wishlist_'+_data[i].ref_user_id+'_'+_data[i].product_id+'_'+_data[i].store_id+'_'+_data[i].status).addClass('active');
				$('.wishlists_'+_data[i].product_id+'_'+_data[i].store_id+'_'+_data[i].status).addClass('active');
			});
		}
	});
	return false;
}
	
	
// Textarea auto expand height
(function($){$.fn.TextAreaExpander=function(minHeight,maxHeight){var hCheck=!($.browser.msie||$.browser.opera);function ResizeTextarea(e){e=e.target||e;var vlen=e.value.length,ewidth=e.offsetWidth;if(vlen!=e.valLength||ewidth!=e.boxWidth){if(hCheck&&(vlen<e.valLength||ewidth!=e.boxWidth))e.style.height="0px";var h=Math.max(e.expandMin,Math.min(e.scrollHeight,e.expandMax));e.style.overflow=(e.scrollHeight>h?"auto":"hidden");e.style.height=h+"px";e.valLength=vlen;e.boxWidth=ewidth}return true};this.each(function(){if(this.nodeName.toLowerCase()!="textarea")return;var p=this.className.match(/expand(\d+)\-*(\d+)*/i);this.expandMin=minHeight||(p?parseInt('0'+p[1],10):0);this.expandMax=maxHeight||(p?parseInt('0'+p[2],10):99999);ResizeTextarea(this);if(!this.Initialized){this.Initialized=true;$(this).css("padding-top",0).css("padding-bottom",0);$(this).bind("keyup",ResizeTextarea).bind("focus",ResizeTextarea)}});return this}})(jQuery);
function trimText (str) {str = str.replace(/^\s+/, '');for (var i = str.length - 1; i >= 0; i--){if (/\S/.test(str.charAt(i))){str = str.substring(0, i + 1);break;}}return str;}

function customized_select() {
	$('.customized_select select').each(function(){
		if($(this).parent().find('div').length == 0) {
			title = $('option:selected',this).text();
			$(this)
				.css({'z-index':10,'opacity':0,'-khtml-appearance':'none'})
				.after('<div>' + title + '</div>')
				.change(function(){
					val = $('option:selected',this).text();
					$(this).next().text(val);
				})
		}
	});
}

function ministore_csConfirm(msg) {
	doNotLoad == true;
	$("#ms-modal-message-bar").html(msg);
	$("#ms-overlay").show();
	$("#ms-modal-notify").show();
}
// Login Window
function ministore_csLogin() {
	ministore_popUp('login.html?' + 'returnUrl=' + encodeURIComponent(location.href) + '&type=ministore&id=1234567890');
	//newwindow = window.open(url,'CSLoginPage',features);
}
function ministore_fbLogin() { ministore_popUp('ms_fb_login.html'); }
function ministore_yahooLogin() { ministore_popUp('user/openid_login/yahoo?login&type=ministore'); }
function ministore_gplusLogin() { ministore_popUp('user/openid_login/google?login&type=ministore'); }
function ministore_popUp(url) {
	var  screenX    = typeof window.screenX != 'undefined' ? window.screenX : window.screenLeft,
	screenY    = typeof window.screenY != 'undefined' ? window.screenY : window.screenTop,
	outerWidth = typeof window.outerWidth != 'undefined' ? window.outerWidth : document.body.clientWidth,
	outerHeight = typeof window.outerHeight != 'undefined' ? window.outerHeight : (document.body.clientHeight - 22),
	width    = 500,
	height   = 270,
	left     = parseInt(screenX + ((outerWidth - width) / 2), 10),
	top      = parseInt(screenY + ((outerHeight - height) / 2.5), 10),
	features = (
			'width=' + width +
			',height=' + height +
			',left=' + left +
			',top=' + top
	);
	var url = root + url;
	newwindow = window.open(url,'CSLoginPage',features);
	
	if (window.focus) {newwindow.focus()}
	return false;
}

// logout		
function ministore_csLogout(){
	preLoader($('#ms-wrapper'));
	var url = root + 'signout.html';
	var form_data = {};
		form_data.post_dta = 1;
		form_data.temp_sessionid = store.get('temp_sessionid');
		form_data.token = store.get('token');
		
	$.ajax({
		type: "POST",
		url: url,
		data: form_data,
		cache: false,
		success: function(data){
			if(data == 'success'){
				store.clear();
				//$(document).data('shipping_address_id',0);
				//$(document).data('billing_address_id',0);
				//$(document).data('co_selected_SABAPD',{});
				//$(document).data('co_note',{});				
				ministore_getMainPage(1);
				//location.href = location.href;
			}
		}
	});
	return false;
}
/* Returns values (in json or url querystring format):
 * 		Logged user: user_id and token
 *		Store info: store_key (which identifies store owner and store_id
 */
function ms_getQueryParams(type) {
	type = typeof(type) == 'undefined' ? 0 : 1;
	var params = eval( {login_user_id:store.get('user_id'),token:store.get('token'),store_key:ms_store_key,cart_sess:store.get('cart_sess'),cart:store.get('cart'),m_note:$(document).data('co_note')} );
	var data = '';
	var value;
	
	// Json format
	if(!type) {
		data = params;		
	}
	// Serialize format
	else {
		for(var d in params) {
			if(params.hasOwnProperty(d)) {
				value = typeof(params[d]) == 'undefined' ? '' : params[d];
				data += '&' + d + '=' + value;
			}
		}
	}
	
	return data;
}
	
// reload certain ajax content
function ministore_csReload(pageToReload) {
	preLoader($('#ms-wrapper'));	
	//ministore_getMainPage(1);
	$.ajax({
		type: "POST",
		url: $('#ms-wrapper').data('current_url'),
		data: $.extend($('#ms-wrapper').data('data'),ms_getQueryParams()),
		cache: false,
		success: function(res){
			$('#ms-wrapper').html(res);
			
			if(typeof jSpane_api_cartlisting != 'undefined') {
				jSpane_api_cartlisting.reinitialise();	
			}
			
			$('#preloader-wrapper').hide();
		}
	});
	
	//location.href = location.href;
}

function preLoader(mcontainer) {
	var w = parseInt(mcontainer.width());
	var h = parseInt(mcontainer.height());
	
	$('#preloader-wrapper').css({width:w,height:h});
	$('#preloader-wrapper').show();
	var t = (h-parseInt($('#preloader-center').height())) / 2;
	$('#preloader-center').css({'top':t});
}

// Page Contents Here
// ================================
// Main Page (Home)
function ministore_getMainPage(page){
	var isOK = checkBrowser('ministore');
	if(!isOK) return false;
	
	form_data = ms_getQueryParams();
	preLoader($('#ms-wrapper'));
	$('#ms-wrapper').data('data',form_data);	
	
	var queryparam = window.location.search.substring(1);
	
	$.post(root + 'mini-store/main.html?page=' + page + '&' + queryparam, form_data,function(data){ $('#ms-wrapper').html(data); $('#preloader-wrapper').hide(); });
}
// Product Listing
function ministore_gotoProductLists(data, page){
	var isOK = checkBrowser('ministore');
	if(!isOK) return false;
	
	var form_data = {};
		form_data.catname = data.catname;
		form_data.total_products = data.total_products;
		form_data.store_category_id = data.store_category_id;
		form_data.catpage = $('#catpage').val();

	$.extend(form_data,ms_getQueryParams());
	preLoader($('#ms-wrapper'));
	$('#ms-wrapper').data('data',form_data);	

	$.post(root + 'mini-store/product-listing.html?page=' + page, form_data,function(data){ 
		$('#ms-wrapper').html(data);
		$('#preloader-wrapper').hide();
	});
}

function ministore_getProductDetails(el) {
	var isOK = checkBrowser('ministore');
	if(!isOK) return false;
	
		var form_data = {};
		
		form_data.catname = $('#catname').val();
		form_data.total_products = $('#total_products').val();
		form_data.store_category_id = $('#ms_cid').val();
		form_data.product_id = el.attr('pid');
		form_data.product_user_id = $(el).attr('uid');
		form_data.catpage = $('#catpage').val();
		form_data.prodpage = $('#prodpage').val();
		form_data.searchtext = $('#searchtext').val();
		form_data.searchpage = $('#searchpage').val();
		form_data.products_option = $('#products_option').val();
		
		$.extend(form_data,ms_getQueryParams());
		preLoader($('#ms-wrapper'));
		$('#ms-wrapper').data('data',form_data);
		
		$.post(root + 'mini-store/details.html', form_data,function(data){ 
			$('#ms-wrapper').html(data);
			$('#preloader-wrapper').hide();
		});
}
// Product option content (pop-up modal)
function show_modal(options) { 
	var opts = $.extend({overlay: true,
						 overlay_class: '',
						 modal: '#ms-modal-window',
						 modal_class: 'option',
						 ajaxURL: false,
						 data: {},
						 content: '',
						 onBeforeLoad: false,
						 onAfterLoad: false
						}, options);
	
	var top, left, modal = $(opts.modal);					
	var cb_before, cb_after;					

	if(opts.overlay != false) { $('#ms-overlay').show(); }
	if(opts.overlay_class != '') { $('.ms-overlay').addClass('overlay_class'); }
	if($(opts.modal).length == 0) { modal = $('#ms-modal-window'); }
	if(opts.modal_class != '') { modal.addClass(opts.modal_class); }
	
	modal.css({height:modal.height(), width:modal.width()} );
	
	if(opts.onBeforeLoad != false) {
		cb_before = $.Callbacks();
		cb_before.add( opts.onBeforeLoad );
		cb_before.fire();	
	}
	
	//opts.modal.addClass('option');
	top = (560 - modal.height()) / 2;
	left = (600 - modal.width()) / 2;
	modal.css({top:top, left:left});
	
	if(opts.ajaxURL) {
		$.post( '/mini-store/options.html?type=listing', opts.data, function(data){
			$('#ms-modal-window').html(data);
			$('#ms-modal-window').show();
			if(opts.onAfterLoad != false) {
				cb_after = $.Callbacks();
				cb_after.add( opts.onAfterLoad );
				cb_after.fire();	
			}
		});
	}
	else {
		$('#ms-modal-window').show();
		if(opts.onAfterLoad != false) {
			cb_after = $.Callbacks();
			cb_after.add( opts.onAfterLoad );
			cb_after.fire();	
		}
	}
}

function hide_modal() {
	$('#ms-modal-window').hide();
	$('#ms-modal-window').html('');
	$('#ms-overlay').hide();
}

function get_cart_data() {
	var cart_data = JSON.stringify(store.get('cart'));
	return cart_data = eval( '(' + cart_data + ')' );
}

function load_cart(data) {
	data = typeof(data) == 'undefined' ? {} : data;
	var form_data = {};
		form_data.catname = $('#catname').val();
		form_data.total_products = $('#total_products').val();
		form_data.store_category_id = $('#ms_cid').val();
		form_data.catpage = $('#catpage').val();
		form_data.ms_search_text = $('#searchtext').val();
		form_data.ms_search_products_option = $('#products_option').val();
		
	if(data.hasOwnProperty('shipto_country')) {
		form_data.shipto_country = data.shipto_country;
	}
	else {
		form_data.shipto_country = store.get('shipto_country');
	}
		
	form_data.cart = store.get('cart');
	preLoader($('#ms-wrapper'));
	form_data = $.extend(form_data,ms_getQueryParams());
	$('#ms-wrapper').data('data',form_data);
	$.post( '/mini-store/cart.html?type=listing', form_data,function(data){
		$('#ms-wrapper').html(data);
		jSpane_api_cartlisting.reinitialise();
		$('#preloader-wrapper').hide();
	});
}

function addtocart(data) {
	if(typeof(data) != 'object') return;
	
	var product_user_id = data.product_user_id; // product owner or merchant
	var product_id = data.product_id;
	var store_id = data.store_id;
	var store_user_id = data.store_user_id; // endorser or store owner
	var quantity = data.quantity;
	var has_options = data.has_options;
	data.product_options = {};
	
	// get existing cart values
	var cart = store.get('cart');
	cart = (typeof(cart) != 'object' || cart == null) ? {} : cart;

	//console.log(cart.merchants[user_id].products.hasOwnProperty(1));
	//console.log(cart);
	// retreive existing 
	var item, option = '';
	var error_option = false;
	var variants_values = new Array();
	
	// Option Validations
	$('.ms-mainoption-value-wrapper').each(function() {
		if($(this).find('select').val() == '') {
			error_option = true;	
		}
		
		data.product_options[$(this).attr('option_id')] = $(this).find('select').val();
		variants_values.push($(this).find('select').val());
	});
	
	var options_string = variants_values.join(',');
	var variants = $.parseJSON($('#unique_variants').attr('variants'));
	var variant_id = 0;
	for (var key in variants) {
		if (variants.hasOwnProperty(key)) {
			if(variants[key] == options_string) {
				variant_id = key;	
			}
		}
	}
	
	if(error_option || (has_options > 0 && variant_id == 0)) {
		alert('Please select an option.');
		return;	
	}
	data.variant_id = variant_id;
	data.cart = cart;
	
	hide_modal();
	
	var query_store = ms_getQueryParams();
	delete query_store['cart'];
	$.extend(data,ms_getQueryParams());
	//preLoader($('#ms-wrapper'));
	$.ajax({
		type: "POST",
		url: '/mini-store/update-cart-data.html',
		data: data,
		cache: false,
		success: function(res){
			data = $.parseJSON(res);
			store.set('cart',data.cart);
			store.set('cart_sess',data.cart_sess);
			load_cart();
		}
	});
}

function update_cart_quantity(data) {
	if(typeof(data) != 'object') return;
	
	// get existing cart values
	var cart = store.get('cart');
	cart = (typeof(cart) != 'object' || cart == null) ? {} : cart;
		
	data.cart = cart;
	$.extend(data,ms_getQueryParams());
	//preLoader($('#ms-wrapper'));
	$.ajax({
		type: "POST",
		url: '/mini-store/update-cart-data.html?update=new_quantity',
		data: data,
		cache: false,
		success: function(res){
			data = $.parseJSON(res);
			store.set('cart',data.cart);
			store.set('cart_sess',data.cart_sess);
			load_cart();
		}
	});
}

function delete_cart_product(data) {
	if(typeof(data) != 'object') return;
	
	// get existing cart values
	var cart = store.get('cart');
	cart = (typeof(cart) != 'object' || cart == null) ? {} : cart;
		
	data.cart = cart;
	$.extend(data,ms_getQueryParams());
	//preLoader($('#ms-wrapper'));
	$.ajax({
		type: "POST",
		url: '/mini-store/update-cart-data.html?update=delete_item',
		data: data,
		cache: false,
		success: function(res){
			data = $.parseJSON(res);
			store.set('cart',data.cart);
			store.set('cart_sess',data.cart_sess);
			load_cart();
		}
	});
}

function update_cart_shipping_selection(mid,value,shipto_country) {
	var data = {};
	data.cart = store.get('cart');
	data.product_user_id = mid;
	data.shipping_selection = value;
	data.shipto_country = shipto_country;
	$.extend(data,ms_getQueryParams());
	preLoader($('#ms-wrapper'));	
	$.ajax({
		type: "POST",
		url: '/mini-store/update-cart-data.html?update=update_shipping_selection',
		data: data,
		cache: false,
		success: function(res){
			cart = $.parseJSON(res);
			store.set('cart',data.cart);
			store.set('cart_sess',data.cart_sess);
			ms_co_step3();
		}
	});
}


function toggle_disable_autocomplete(enable,type) {
	enable = (typeof enable == 'undefined') ? 'disabled' : enable;
	var state_code, state_name, city, zip;
	if(type == 'shipping') {
		state_code = 'shipping_state_code';
		state_name = 'shipping_state_name';
		city = 'shipping_city';
		zip = 'shipping_zip';
	}
	else {
		state_code = 'billing_state_code';
		state_name = 'billing_state_name';
		city = 'billing_city';
		zip = 'billing_zip';
	}

	if(enable == 'disable') {
		$('input#'+city).autocomplete( "option", "disabled", true );
		$('input#'+zip).autocomplete( "option", "disabled", true );
	}
	else {
		$('input#'+city).autocomplete( "enable" );
		$('input#'+city).autocomplete({  
			//define callback to format results  
			source: function(req, add){  
				//pass request to server  
				$.getJSON("/autocomplete/US_city.html?state_code="+$('select#'+state_code).val()+"&callback=?", req, function(data) {  
						//create array for response objects  
						var suggestions = [];  
						//process response  
						$.each(data, function(i, val){  
						suggestions.push(val.name);  
					});  

					//pass array to callback  
					add(suggestions);  
				});  
			}, 
			 select: function(e, ui) {  
				//create formatted friend  
				var _city = ui.item.value; 
					$.get("/autocomplete/US_zip.html?state_code="+$('select#'+state_code).val()+"&city="+_city+"&callback=?", function(data) {
						data = data.replace('?','').replace('(','').replace(')','');
						data = $.parseJSON(data);
						
						if(!$.isEmptyObject(data) && data.length == 1) {
							$('input#'+zip).val(data[0].name);	
						}
						else {
							//$('input#'+zip).val('');	
						}
					});
			},
			//define select handler  
			change: function() {  
				_city = $.trim($(this).val());
				$.get("/autocomplete/US_zip.html?state_code="+$('select#'+state_code).val()+"&city="+_city+"&callback=?", function(data) {
					data = data.replace('?','').replace('(','').replace(')','');
					data = $.parseJSON(data);
					if(!$.isEmptyObject(data) && data.length == 1) {
						$('input#'+zip).val(data[0].name);	
					}
				});
			}  
		}); 
		
		$('input#'+zip).autocomplete( "enable" );
		$('input#'+zip).autocomplete({  
			//define callback to format results  
			source: function(req, add){  
				//pass request to server  
				$.getJSON("/autocomplete/US_zip.html?state_code="+$('select#'+state_code).val()+"&city="+$('input#'+city).val()+"&callback=?", req, function(data) {  
					//create array for response objects  
					var suggestions = [];  
					//process response  
					$.each(data, function(i, val){  
						suggestions.push(val.name);  
					});  
					//pass array to callback  
					add(suggestions);  
				});  
			}, 
			 select: function(e, ui) {  
				//create formatted friend  
				var _zip = ui.item.value; 
					$.get("/autocomplete/US_city.html?state_code="+$('select#'+state_code).val()+"&zip="+_zip+"&callback=?", function(data) {
						data = data.replace('?','').replace('(','').replace(')','');
						data = $.parseJSON(data);
						if(!$.isEmptyObject(data) && data.length == 1) {
							$('input#'+city).val(data[0].name);	
						}
						else {
							//$('input#'+city).val('');	
						}
					});
			},
			//define select handler  
			change: function() {  
				_zip = $.trim($(this).val());
				$.get("/autocomplete/US_city.html?state_code="+$('select#'+state_code).val()+"&zip="+_zip+"&callback=?", function(data) {
					data = data.replace('?','').replace('(','').replace(')','');
					data = $.parseJSON(data);
					if(!$.isEmptyObject(data) && data.length == 1) {
						$('input#'+city).val(data[0].name);	
					}
					else {
						//$('input#'+city).val('');	
					}
				});
			}  
			 
		}); 
		
	}
}

function ministore_checkout(section,form_data) {
	preLoader($('#ms-wrapper'));
	var form_data = (typeof(form_data) == 'undefined') ? {} : form_data;
	form_data.shipto_country = $('#shipto_country').val();
	$.extend(form_data,ms_getQueryParams());
	$.ajax({
		type: "POST",
		url: '/mini-store/checkout.html?type=shippingbilling&section='+section,
		data: form_data,
		cache: false,
		success: function(res){
			$('#ms-checkout-inner-wrapper').html(res);
			if(section == 'shipping') {
				ms_co_step1();
			}
			else {
				ms_co_step2();
			}
			$('#ms-checkout-wrapper').removeClass('placeorder');
			$('#ms-checkout-wrapper').show();
			$('#preloader-wrapper').hide();
			
			$('.checkoutprocess-wrapper.shipping.list .scrollbar').jScrollPane();
			$('.checkoutprocess-wrapper.billing .scrollbar').jScrollPane();
			//jSpane_api_placeorder.reinitialise();
		}
	});
}

function ms_shiptoaddress(type, action, checkingOnly) {
	preLoader($('#ms-wrapper'));
	checkingOnly = (typeof(checkingOnly) == 'undefined') ? 0: checkingOnly;
	
	if(type == "shipping") {
		var f = $('#cp-shipping-form form');
		f.validationEngine('hideAll');
		var shipping_address_id = $(document).data('shipping_address_id');
		var form_data = {"shipping_firstname":f.find('#shipping_firstname').val(),
							"shipping_lastname":f.find('#shipping_lastname').val(),
							"shipping_company":f.find('#shipping_company').val(),
							"shipping_address1":f.find('#shipping_address1').val(),
							"shipping_address2":f.find('#shipping_address2').val(),
							"shipping_country":f.find('#shipping_country').val(),
							"shipping_state_code":f.find('#shipping_state_code').val(),
							"shipping_state_name":f.find('#shipping_state_name').val(),
							"shipping_city":f.find('#shipping_city').val(),
							"shipping_zip":f.find('#shipping_zip').val(),
							"shipping_phone":f.find('#shipping_phone').val(),
							"shipping_address_id": shipping_address_id,
						};
	}
	else if(type == "billing") {
		var f = $('#billing-editing-form');
		f.validationEngine('hideAll');
		var billing_address_id = $(document).data('billing_address_id');
		var form_data = {"billing_firstname":f.find('#billing_firstname').val(),
							"billing_lastname":f.find('#billing_lastname').val(),
							"billing_company":f.find('#billing_company').val(),
							"billing_address1":f.find('#billing_address1').val(),
							"billing_address2":f.find('#billing_address2').val(),
							"billing_country":f.find('#billing_country').val(),
							"billing_state_code":f.find('#billing_state_code').val(),
							"billing_state_name":f.find('#billing_state_name').val(),
							"billing_city":f.find('#billing_city').val(),
							"billing_zip":f.find('#billing_zip').val(),
							"billing_phone":f.find('#billing_phone').val(),
							"billing_address_id": billing_address_id,
							"billing_email_address": f.find("#billing_email_address").val(),
							"cc_type":f.find("#cc_type").val(),
							"cc_number":f.find("#cc_number").val(),
							"cc_expire_month":f.find("#cc_expire_month").val(),
							"cc_expire_year":f.find("#cc_expire_year").val(),
						};
		if(store.get('user_id') > 0) {
			form_data.cvv2Number = f.find('#cvv2Number').val();	
		}
	}
	else {
		return;	
	}
	
	$.extend(form_data,ms_getQueryParams());
	$.ajax({
		type: "POST",
		url: '/mini-store/save-address.html?type='+type,
		data: form_data,
		cache: false,
		success: function(response){
			response = $.parseJSON(response);
			if(!response || response == null || $.isEmptyObject(response)) {
				$('#preloader-wrapper').hide();
				return false;
			}
			
			var error = response['error'];
			if(typeof(error) == "string") {
				// state, city and/or zip does not match
			}else if(response.hasOwnProperty('error')) {
				if(error['firstname']) {
					f.find('#' + type + '_firstname').validationEngine('showPrompt', 'This is a required field.', 'error', 'bottomLeft', true);	
				}
				if(error['lastname']) {
					f.find('#' + type + '_lastname').validationEngine('showPrompt', 'This is a required field.', 'error', 'bottomLeft', true);	
				}
				if(error['address']) {
					f.find('#' + type + '_address1').validationEngine('showPrompt', 'This is a required field.', 'error', 'bottomLeft', true);	
				}
				if(error['country']) {
					f.find('#' + type + '_country').validationEngine('showPrompt', 'This is a required field.', 'error', 'bottomLeft', true);	
				}
				if(error['state_code']) {
					f.find('#' + type + '_state_code').validationEngine('showPrompt', 'This is a required field.', 'error', 'bottomLeft', true);
					toggle_disable_autocomplete('enable','' + type + '');	
				}
				if(error['state_name']) {
					f.find('#' + type + '_state_name').validationEngine('showPrompt', 'This is a required field.', 'error', 'bottomLeft', true);	
					toggle_disable_autocomplete('disable','' + type + '');
				}
				if(error['city']) {
					f.find('#' + type + '_city').validationEngine('showPrompt', 'This is a required field.', 'error', 'centerRight', true);	
				}
				if(error['zip']) {
					f.find('#' + type + '_zip').validationEngine('showPrompt', 'This is a required field.', 'error', 'centerRight', true);	
				}
				if(error['phone']) {
					f.find('#' + type + '_phone').validationEngine('showPrompt', 'This is a required field.', 'error', 'centerRight', true);	
				}
				if(error['statecityzip']) {
					f.find('#' + type + '_city').validationEngine('showPrompt', error['statecityzip'], 'error', 'centerRight', true);	
				}
				
				if(type == "billing") {
					if(error['billing_email_address']) {
						f.find('#billing_email_address').validationEngine('showPrompt', 'This is a required field.', 'error', 'centerRight', true);	
					}
					if(error['cc_type']) {
						f.find('#cc_type').parent().validationEngine('showPrompt', 'This is a required field.', 'error', 'centerRight', true);	
					}
					if(error['cc_number']) {
						f.find('#cc_number').validationEngine('showPrompt', 'This is a required field.', 'error', 'centerRight', true);	
					}
					if(error['cc_expire_month']) {
						f.find('#cc_expire_month').validationEngine('showPrompt', 'This is a required field.', 'error', 'centerRight', true);	
					}
					if(error['cc_expire']) {
						f.find('#cc_expire_error_wrapper').validationEngine('showPrompt', 'This is a required field.', 'error', 'centerRight', true);	
					}
					if(error['cvv2Number']) {
						//f.find('#cvv2Number').validationEngine('showPrompt', 'This is a required field.', 'error', 'centerRight', true);	
					}
				}

				if(type == "billing") {
					$('.checkoutprocess-wrapper.billing .scrollbar').jScrollPane();
				}
				$('#preloader-wrapper').hide();
			}
			else {
				if(type == "shipping") {
					if(action != "shippingaddress") {
						ms_get_shippinglist('hide');
						store.get('shipto_country',f.find('#shipping_country').val());
						ms_co_step2();
						
						if(store.get('user_id') > 0 && $('#billing-selection-form').length > 0) {
							$(document).data('shipping_address_id',response.address_id);
						}
						else {
							$(document).data('shipping_address_id',0);
						}
						$('#cp-shippinglist-form').hide();
						$('#preloader-wrapper').hide();
					}
					else {
						if(store.get('user_id') > 0 && $('#billing-selection-form').length > 0) {
							ms_get_shippinglist('show');
							$('.shipping-form').hide();
						}
					}
					
					
					
					
					
					if(store.get('user_id') > 0) {
						// reload shipping adress listing
						ms_get_shippinglist(type);
					}
					// proceed to billing form
					if(action != "shippingaddress") {
						ms_co_step2();
						
						if(store.get('user_id') > 0) {
							$(document).data('shipping_address_id',response.address_id);
						}
						else {
							$(document).data('shipping_address_id',0);
						}
						
						$('#preloader-wrapper').hide();
					}
				}
				else if(type == "billing") {
					ms_co_step3();
				}
			}
		}
	});
}

function ms_get_shippinglist(type) {
	//preLoader($('#ms-wrapper'));
	$(document).data('shipping_address_id',0);
	var form_data = {};
	$.extend(form_data,ms_getQueryParams());
	$.ajax({
		type: "POST",
		url: '/mini-store/get-address.html?type=shipping&action=get_list',
		data: form_data,
		cache: false,
		success: function(response){
			if(response != false) {
				$('#cp-shippinglist-form').html(response);
				if(show != 'hide') {
					$('#cp-shippinglist-form').show();
				}
			}
			
			$('#preloader-wrapper').hide();
			
			/*
			if(response != false) {
				$('#cp-shippinglist-form').html(response);
			}
			if(type == "shippingaddress") {
				$('#cp-shippinglist-form').show();
				$('#cp-shipping-form').hide();
				$('#preloader-wrapper').hide();
			}
			
			$('.checkoutprocess-wrapper.shipping.list .scrollbar').jScrollPane();
			*/
		}
	});
}

function ms_edit_shipping(id) {
	preLoader($('#ms-wrapper'));
	$(document).data('shipping_address_id',id);
	var form_data = {shipping_address_id:id};
	$.extend(form_data,ms_getQueryParams());
	$.ajax({
		type: "POST",
		url: '/mini-store/get-address.html?type=shipping&action=get_edit',
		data: form_data,
		cache: false,
		success: function(response){
			response = $.parseJSON(response);

			if(!response || response == null) {
				$('#preloader-wrapper').hide();
				return false;
			}
			
			for(var field in response) {
				if(response.hasOwnProperty(field)) {
					//value = typeof(response[d]) == 'undefined' ? '' : response[d];
					
					if(field != "shipping_state") {
						$('#cp-shipping-form').find('#'+field).val(response[field]);
					}
				}
			}
			
			if($('#shipping_country option:selected').val() == "US") {
				$('#shipping_state_code option').removeAttr('selected');
				$('#shipping_state_code option[value="' + response['shipping_state'] + '"]').attr('selected','selected');
				$('#shipping_state_name').val('');
				$('#shipping_state_name').hide();
				$('#shipping_state_code').show();
				
				toggle_disable_autocomplete('enable','shipping');
			}
			else {
				$('#shipping_state_code').val('');
				$('#shipping_state_name').val(response['shipping_state']);
				$('#shipping_state_name').show();
				$('#shipping_state_code').hide();
				
				toggle_disable_autocomplete('disable','shipping');
			}
			
			
			$('#cp-shippinglist-form').hide();
			$('#cp-shipping-form').show();
			$('#preloader-wrapper').hide();
		}
	});
}

function ms_delete_shipping(id) {
	if(!confirm("Are you sure you want to delete this address?")) {
		return false;	
	}
	
	preLoader($('#ms-wrapper'));
	var form_data = {address_id:id};
	$.extend(form_data,ms_getQueryParams());
	$.ajax({
		type: "POST",
		url: '/mini-store/delete-address.html?type=shipping',
		data: form_data,
		cache: false,
		success: function(response){
			var pElement = $('#shipping-address-num'+id).parent();
			$('#shipping-address-num'+id).remove();

			if(pElement.find("li").length == 0) {
				$('#addnewshipping').click();
			}
			
			$('.checkoutprocess-wrapper.shipping.list .scrollbar').jScrollPane();
			$('#preloader-wrapper').hide();
		}
	});
}

function ms_edit_billing(id) {
	preLoader($('#ms-wrapper'));
	$(document).data('billing_address_id',id);
	var form_data = {billing_address_id:id};
	$.extend(form_data,ms_getQueryParams());
	$.ajax({
		type: "POST",
		url: '/mini-store/get-address.html?type=billing&action=get_edit',
		data: form_data,
		cache: false,
		success: function(response){
			response = $.parseJSON(response);

			if(!response || response == null) {
				$('#preloader-wrapper').hide();
				return false;
			}
			
			for(var field in response) {
				if(response.hasOwnProperty(field)) {
					//value = typeof(response[d]) == 'undefined' ? '' : response[d];
					
					if(field != "billing_state") {
						if(field == 'cc_expire_month' || field == 'cc_expire_year' || field == 'cc_type') {
							if(field != 'cc_type') {
								response[field]	= parseInt(response[field]);
							}	
							$('#billing-editing-form').find('#'+field+' option[value="' + response[field] + '"]').attr('selected','selected');
							$('#billing-editing-form').find('#'+field+' option[value="' + response[field] + '"]').parent().parent().find('#txtvalue').html(response[field]);
							
						}
						else {
							$('#billing-editing-form').find('#'+field).val(response[field]);
						}
					}
				}
			}
			
			$('#cc_name').val($('#billing_firstname').val() + ' ' + $('#billing_lastname').val());
			
			if($('#billing_country option:selected').val() == "US") {
				$('#billing_state_code option').removeAttr('selected');
				$('#billing_state_code option[value="' + response['billing_state'] + '"]').attr('selected','selected');
				$('#billing_state_name').val('');
				$('#billing_state_name').hide();
				$('#billing_state_code').show();
				
				toggle_disable_autocomplete('enable','billing');
			}
			else {
				$('#billing_state_code').val('');
				$('#billing_state_name').val(response['billing_state']);
				$('#billing_state_name').show();
				$('#billing_state_code').hide();
				
				toggle_disable_autocomplete('disable','billing');
			}
			
			$('#billing-selection-form').hide();
			$("#ms-co-footer-billing button").addClass('disabled');
			$('#billing-editing-form').show();
			$('.checkoutprocess-wrapper.billing .scrollbar').jScrollPane();
			$('#preloader-wrapper').hide();
		}
	});
}

function ms_co_step1() {
	$('.ms-checkout-menusteps li').removeClass('selected').removeClass('done');
	$('.ms-checkout-menusteps li.shipping').addClass('selected');
	
	$('#ms-co-footer-billing').hide();
	$('#ms-co-footer-shipping').show();
	
	$('#cp-billing-form').hide();
	$("#ms-co-footer-billing button").removeClass('disabled');
	
	if(store.get("user_id") > 0) {
		$('#cp-shipping-form').hide();
		$('#cp-shippinglist-form').show();
	}
	else {
		$('#cp-shippinglist-form').hide();
		$('#cp-shipping-form').show();
	}
	$('.checkoutprocess-wrapper.shipping.list .scrollbar').jScrollPane();
}

function ms_co_step2() {
	$('.ms-checkout-menusteps li').removeClass('selected').removeClass('done');
	$('.ms-checkout-menusteps li.shipping').addClass('done');
	$('.ms-checkout-menusteps li.billing').addClass('selected');
	
	$('#ms-co-footer-shipping').hide();
	$('#ms-co-footer-billing').show();
	
	$('#cp-shippinglist-form').hide();
	$('#cp-shipping-form').hide();
	$('#cp-billing-form').show();

	if(!$.isEmptyObject($(document).data('co_billing_list')) && store.get('user_id') > 0) {
		$('#billing-editing-form').hide();
		$('#billing-selection-form').show();
	}
	
	$('.checkoutprocess-wrapper.billing .scrollbar').jScrollPane();
}

function ms_co_step3() {
	preLoader($('#ms-wrapper'));
	var sf = $('#cp-shipping-form form');
	var bf = $('#billing-editing-form');
	var form_data = {"shipping_firstname":sf.find('#shipping_firstname').val(),
						"shipping_lastname":sf.find('#shipping_lastname').val(),
						"shipping_company":sf.find('#shipping_company').val(),
						"shipping_address1":sf.find('#shipping_address1').val(),
						"shipping_address2":sf.find('#shipping_address2').val(),
						"shipping_country":sf.find('#shipping_country').val(),
						"shipping_state_code":sf.find('#shipping_state_code').val(),
						"shipping_state_name":sf.find('#shipping_state_name').val(),
						"shipping_city":sf.find('#shipping_city').val(),
						"shipping_zip":sf.find('#shipping_zip').val(),
						"shipping_phone":sf.find('#shipping_phone').val(),
						"shipping_address_id":$(document).data('shipping_address_id'),
						"billing_address_id":$(document).data('billing_address_id'),
						"billing_firstname":bf.find('#billing_firstname').val(),
						"billing_lastname":bf.find('#billing_lastname').val(),
						"billing_company":bf.find('#billing_company').val(),
						"billing_address1":bf.find('#billing_address1').val(),
						"billing_address2":bf.find('#billing_address2').val(),
						"billing_country":bf.find('#billing_country').val(),
						"billing_state_code":bf.find('#billing_state_code').val(),
						"billing_state_name":bf.find('#billing_state_name').val(),
						"billing_city":bf.find('#billing_city').val(),
						"billing_zip":bf.find('#billing_zip').val(),
						"billing_phone":bf.find('#billing_phone').val(),
						"cc_type":bf.find('#cc_type').val(),
						"cc_number":bf.find('#cc_number').val(),
						"cc_expire_month":bf.find('#cc_expire_month').val(),
						"cc_expire_year":bf.find('#cc_expire_year').val(),
					};
	if(store.get("user_id") == 0) {
		form_data.billing_email_address = bf.find('#billing_email_address').val();
	}
					
	$(document).data('co_selected_SABAPD', form_data);				
	//form_data.shipto_country = $('#shipto_country').val();
	$.extend(form_data,ms_getQueryParams());
	$('#ms-checkout-inner-wrapper').html('');
	$.ajax({
		type: "POST",
		url: '/mini-store/checkout.html?type=review',
		data: form_data,
		cache: false,
		success: function(res){
			$('#ms-checkout-inner-wrapper').html(res);
			$('#ms-checkout-wrapper').addClass('placeorder');
			$('#ms-checkout-wrapper').show();
			$('#preloader-wrapper').hide();
			//$('.checkoutprocess-wrapper.billing .scrollbar').jScrollPane();
		}
	});
}

function purchase_order() {
	var cvv2Number = parseInt($('#card-verify').val());
	if(cvv2Number == 0 || isNaN(cvv2Number)) {
		$('#card-verify').validationEngine('showPrompt', 'This is a required field.', 'error', 'topLeft', true);
		return;	
	}
	$('#card-verify').validationEngine('hide');
	
	preLoader($('#ms-wrapper'));	
	var form_data = $(document).data('co_selected_SABAPD');
	form_data.cvv2Number = cvv2Number;
	$.extend(form_data,ms_getQueryParams());
	$.ajax({
		type: "POST",
		url: '/mini-store/purchase-order.html',
		data: form_data,
		cache: false,
		success: function(res){
			var data = $.parseJSON(res);
			var checkout_data = {"cvv2Number" : cvv2Number};

			if($.isEmptyObject(data.error) && data.hasOwnProperty('success') && (data.success.hasOwnProperty('u') || data.success.hasOwnProperty('e'))) {
				checkout_data.o = data.success.o;
				// login
				if(data.success.hasOwnProperty('u')) {
					checkout_data.u = data.success.u;
				}
				// guest
				else {
					checkout_data.e = data.success.e;
					checkout_data.cc_number = data.success.cc_number;
					checkout_data.cc_type = data.success.cc_type;
					checkout_data.cc_expire_month = data.success.cc_expire_month;
					checkout_data.cc_expire_year = data.success.cc_expire_year;
					checkout_data.cc_firstname = data.success.cc_expire_year;
					checkout_data.cc_lastname = data.success.cc_lastname;
				}
				$.ajax({
					type: "POST",
					url: CS.checkoutURL + "w/checkout",
					data: checkout_data,
					crossDomain: true,
					cache: false,
					success: function(response){
						data2 = $.parseJSON(response);
						if($.isEmptyObject(data2.error)) {
							store.remove('cart');
							
							
							//location.href = '/' + CS.storeURI + '/checkout.html?step=ordersuccess';
							preLoader($('#ms-wrapper'));
							alert('You have succesfully purchase an order.');
							$('#ms-checkout-inner-wrapper').html('');
							$('#ms-checkout-wrapper').hide();
							$('#preloader-wrapper').hide();
						}
						else {
							console.log(data2.error);

							if($.isEmptyObject(data2.error[0].error_code)) {
								var error = "Error message: " + data2.error;
							}
							else {
								var error = "Error code: " + data2.error[0].error_code + "\r\nError message: " + data2.error[0].error_msg;
							}
							alert(error);
							$('#preloader-wrapper').hide();
						}
					}
				})
				.done(function(data, textStatus, jqXHR) { })
				.fail(function(jqXHR, textStatus, errorThrown) { console.log(jqXHR); console.log(textStatus); console.log(errorThrown); });
			}
			else {
				var e = '';
				var errors = data.error;
				for(var errortype in errors) {
					if(errors.hasOwnProperty(errortype)) {
						e += errors[errortype] + '\r\n';
					}
				}
				
				alert(e);
				$('#preloader-wrapper').hide();
			}
			
			
			
			
		}
	});
}

