var store_cart_data = {};
var is_remotestorecart = false;

var addEvent = function addEvent(element, eventName, func) {
	if (element.addEventListener) {
    	return element.addEventListener(eventName, func, false);
    } else if (element.attachEvent) {
        return element.attachEvent("on" + eventName, func);
    }
};

addEvent(document.getElementById('open-left'), 'click', function(){
	snapper.open('left');
});

/* Prevent Safari opening links when viewing as a Mobile App */
(function (a, b, c) {
    if(c in b && b[c]) {
        var d, e = a.location,
            f = /^(a|html)$/i;
        a.addEventListener("click", function (a) {
            d = a.target;
            while(!f.test(d.nodeName)) d = d.parentNode;
            "href" in d && (d.href.indexOf("http") || ~d.href.indexOf(e.host)) && (a.preventDefault(), e.href = d.href)
        }, !1)
    }
})(document, window.navigator, "standalone");

$(document).ready(function(){ 
	$('#_search').bind('keypress', function(e) {
		$(this).removeClass('red-border');
		var key_word = encodeURIComponent($.trim($(this).val()));
		if(e.keyCode==13){
			if(key_word == ""){
				$(this).addClass('red-border');
				return false;
			}
			
			var frame = getUrlParam('frame');
			frame = (frame != "") ? "?frame=1&" : "?";
			key_word = 'key_word=' + key_word;
			window.location.href = main_url + 'search.html' + frame + key_word;
		}
	});
	$(document).on('click', '#cat-icon', function(){
		if($('.swiper-nav').hasClass('hide')){
			$('.swiper-nav').removeClass('hide');
			$('#content').removeClass('top-low');
			$('#content').addClass('top-high');
			//$('#content').css('top', '106px');
			reInitPage();
		}else{
			$('.swiper-nav').addClass('hide');
			$('#content').removeClass('top-high');
			$('#content').addClass('top-low');
			//$('#content').css('top', '59px');
		}
	});
	$(document).on('click', '.signin-lbl-btn', function(){
		var error = false;
		if($.trim($('.y-email').val()) == ''){ console.log('empty'); }
		if($.trim($('.y-password').val()) == ''){ console.log('empty'); }
		
	});

	$(document).on('touchstart, click', '.sign-in-button', function(){
		snapper.close();
		$('.sign-in-wrapper input').val('');
		$('.field').removeClass('error');
		$('.r-message').addClass('hide-error');
		$('.message').addClass('hide-error');
		$('.modal_close_level2').trigger('click');
		$('.sign-in-modal').trigger('click');
		Foundation.libs.abide.settings.patterns.password = /^\w{1,}$/;
	});

	$(document).on('touchstart, click', '.register-button', function(){
		snapper.close();
		$('.sign-in-wrapper input').val('');
		$('.field').removeClass('error');
		$('.r-message').addClass('hide-error');
		$('.message').addClass('hide-error');
		$('.modal_close').trigger('click');
		$('.register-modal').trigger('click');
		Foundation.libs.abide.settings.patterns.password = /^\w{6,}$/;
	});
	
	$(document).on('touchstart, click', '.search-icon-btn', function(){
		if($('#header-search').hasClass('search-icon-active')){
			$('#header-search').removeClass('search-icon-active');
			$('#header-title').show();
			$('.category-icon').removeClass('hide');
			$('#_search').val('');
		}else{
			$('#header-search').addClass('search-icon-active');
			$('#header-title').hide();
			$('.category-icon').addClass('hide');
		}
	});
	
	$(document).on('touchstart, click', '.snap-drawer-header-wrapper li', function(){
		$('.snap-drawer-header-wrapper li').removeClass('selected');
		$(this).addClass('selected');
	});
	
	$(document).on('touchstart, click', '.pages-menu', function(){
		$('.settings-menu').removeClass('active');
		$(this).addClass('active');
		$('.tab-pages').removeClass('hide');
		$('.tab-settings').addClass('hide');
	});
	$(document).on('touchstart, click', '.settings-menu', function(){
		$('.pages-menu').removeClass('active');
		$(this).addClass('active');
		$('.tab-settings').removeClass('hide');
		$('.tab-pages').addClass('hide');
	});

	$(document).on('touchstart, click', '.nav-a', function(){
		$('.details-nav span').removeClass('active');
		$(this).addClass('active');
		$('.common-wrap').addClass('d-none');
		$('.details-wrap').removeClass('d-none');
	});
	$(document).on('touchstart, click', '.nav-b', function(){
		$('.details-nav span').removeClass('active');
		$(this).addClass('active');
		$('.common-wrap').addClass('d-none');
		$('.ships-wrap').removeClass('d-none');
	});
	$(document).on('touchstart, click', '.nav-c', function(){
		$('.details-nav span').removeClass('active');
		$(this).addClass('active');
		$('.common-wrap').addClass('d-none');
		$('.vendor-wrap').removeClass('d-none');
	});
	
	$(document).on('submit', '#emailForm', function(e){
		e.preventDefault(); e.stopImmediatePropagation();
		var $form = $(this);
		var url = $form.attr( 'action' );
		var form_data = {};
		var sender_id = store.get('user_id');
		
		form_data.subject = jQuery.trim($form.find( 'input[name="subject"]' ).val());
		form_data.sender_email = jQuery.trim($form.find( 'input[name="email"]' ).val());
		form_data.sender = sender_id;
		if(typeof sender_id === 'undefined'){
			form_data.sender = form_data.sender_email;
		}
		form_data.recipient = jQuery.trim($form.find( 'input[name="recipient_id"]' ).val());
		form_data.message = jQuery.trim($form.find( 'textarea[name="message"]' ).val());
		form_data.reference = 'mobile store';
		$('.sendbtn').attr('disabled', 'disabled');
		$.post(url, form_data,function(data){
			var _data = $.parseJSON(data);
			if(_data.sending_result){
				$('#emailForm input, #emailForm textarea').val('');
			}
			$('.sendbtn').removeAttr('disabled');
		});
		return false;
	});
	
	
	
	$(document).on('touchstart, click','.share-box', function(e) {
		e.stopImmediatePropagation();
		var data = $.parseJSON($('.share-box').attr('data'))
		$('#sharebox-wrapper .promotetxt .amount').html('$'+data.payout);
		$('.share_modal_wrapper').show();
		$('#main-wrapper').css('overflow','visible');
		
	});
	
	$(document).on('touchstart, click','#sharebox-wrapper .signintxt .txt-signin', function(e) {
		e.stopImmediatePropagation();
		//$('#sharebox-wrapper').hide();
		//$('#share_modal_signin_wrapper').show();	
		$('.share_modal_wrapper').addClass('bgcolor1').removeClass('bgcolor3');
		$( "#sharebox-wrapper" ).fadeOut(400, function() {
			$('#share_modal_signin_wrapper').fadeIn();
		});
	});
	
	$(document).on('touchstart, click','#sharebox-wrapper #btn-close-sharemmodal', function(e) {
		e.stopImmediatePropagation();
		$('.share_modal_wrapper').hide();
		$('#main-wrapper').css('overflow','auto');
	});
	
	$(document).on('touchstart, click','#share_modal_signin_wrapper #btn-close-sharemmodal', function(e) {
		e.stopImmediatePropagation();
		$('.share_modal_wrapper').addClass('bgcolor3').removeClass('bgcolor1');
		$( "#share_modal_signin_wrapper" ).fadeOut(400, function() {
			$('#sharebox-wrapper').fadeIn();
			
		});
	});
	
	$(document).on('touchstart, click','#link-share-product a', function() {
		var url;
		var share = shopstyx_share_data;
		var uid = parseInt($('#link-share-product').attr('uid'));
		if(uid > 0) {
			uid = escape('?id='+uid);
		}
		else {
			uid = "";	
		}
		
		share.product_url += uid
		switch($(this).attr('class')) {
			default:
			case 'fb':
				url = 'https://www.facebook.com/dialog/feed?app_id=142470092447374&link='+share.product_url+'&picture='+share.image+'&name='+share.product_name+'&caption=&description='+share.desc+'&redirect_uri='+share.current_url;
				break;
			case 'tw':
				url = 'http://twitter.com/share?count=false&original_referer='+share.current_url+'&via=shopstyx.com&text='+share.product_name+'&url='+share.product_url;
				break;
			case 'gplus':
				url = 'https://plus.google.com/share?url='+share.current_url;
				break;
			case 'linkedin':
				url = 'http://www.linkedin.com/shareArticle?mini=true&url='+share.product_url+'&title='+share.product_name+'&summary='+share.desc+'&source=shopstyx.com';
				break;
			case 'pinterest':
				url = 'http://pinterest.com/pin/create/button/?url='+share.product_url+'&media='+share.image+'&description='+share.desc;
				break;
		}
  		
		window.open(url);
	});
	
	$(document).on('touchstart, click', '.share-register-button', function(){
		$('.share_modal_wrapper').addClass('bgcolor1').removeClass('bgcolor3');
		$( "#share_modal_signin_wrapper" ).fadeOut(400, function() {
			$('#share_modal_register_wrapper').fadeIn();
		});
	});
	
	$(document).on('touchstart, click', '.share-sign-in-button', function(){
		$( "#share_modal_register_wrapper" ).fadeOut(400, function() {
			$('#share_modal_signin_wrapper').fadeIn();
		});
	});
	
	$(document).on('touchstart, click','#share_modal_register_wrapper #btn-close-sharemmodal', function(e) {
		e.stopImmediatePropagation();
		$('.share_modal_wrapper').addClass('bgcolor3').removeClass('bgcolor1');
		$( "#share_modal_register_wrapper" ).fadeOut(400, function() {
			$('#sharebox-wrapper').fadeIn();
		});
	});
	
	
	// Add to cart 
	$(document).on('change','.pdetails-cart-option select', function() {
		
		var parent_container = '.pdetails-cart-option';
		var variants_values = $.parseJSON($('#unique_variants').attr('variants_values'));
		var unique_variants = $.parseJSON($('#unique_variants').val());
		var index = parseInt($(parent_container + ' select').index(this));
		var value = $(this).val();
		var next_option = (index + 1);


		if(index == 0 || (index == 0 && value == "")) {
			$(parent_container + ' select').eq(1).val('');
			$(parent_container + ' select').eq(2).val('');
			$(parent_container + ' select').eq(1).attr('disabled','disabled');	
			$(parent_container + ' select').eq(2).attr('disabled','disabled');	
			
			$(this).removeAttr('disabled');
		}

		if(typeof $(parent_container + ' select').eq(next_option) != "undefined" && value != "") {
			$(parent_container + ' select').eq(next_option).find('option').hide();
			$(parent_container + ' select').eq(next_option).find('option').eq(0).show();
			$(parent_container + ' select').eq(next_option).removeAttr('disabled');
			$(parent_container + ' select').eq(next_option).val('');
			for(i = 0; i < variants_values.length; i++) {
				if(variants_values[i][index] == value) {
					$(parent_container + ' select').eq(next_option).find('option[value='+variants_values[i][next_option]+']').show();	
				}
			}
			
		}
		else {
			if(index > 0 && value == '') {
				$(parent_container + ' select').eq(next_option).attr('disabled','disabled');
				$(parent_container + ' select').eq(next_option).find('option').hide();
				$(parent_container + ' select').eq(next_option).val('');
				$(parent_container + ' select').eq(next_option).find('option').eq(0).show();
			}
		}
	});
	
	$(document).on("touchstart click", "#add-to-cart-btn", function(e){
		e.stopImmediatePropagation();
		e.preventDefault();

		var el = $(this);
		var qty = parseInt(el.parent().find('input[name="qty"]').val());
		var data = $.parseJSON(el.attr('data'));
		var data = {product_user_id:data.p_uid,
					product_id:data.p_id, 
					product_store_id: data.store_id,
					product_store_url: CS.parent_embed_url,
					store_id:data.store_id,
					store_user_id:data.store_uid ,
					quantity: parseInt(qty),
					has_options: data.has_options,
					promoter_id: data.promoter_id };
		addtocart(data);	
	});
	
	$(document).on('click','.add-note-btn', function(e) {
		e.preventDefault(); e.stopImmediatePropagation();
		$(this).hide().parent().parent().find('textarea').show();
	});
	
	$(document).on('click','.prod-info-oncart .update-btn', function(e) {
		e.preventDefault(); e.stopImmediatePropagation();
		
		var quantity = $(this).parent().find('input').val();
		var m = $(this).parent().find('input').attr('m');
		var p = $(this).parent().find('input').attr('p');
		var variant_id = parseInt($(this).parent().find('input').attr('variant_id'));
		var su = $(this).parent().find('input').attr('su');
		var s = $(this).parent().find('input').attr('s');
		var has_options = 0;
		var product_options = $(this).parent().find('input').attr('options');
		
		if(typeof(variant_id) != 'undefined' && variant_id > 0) {
			has_options = 1;
		}
		
		var data = {product_user_id:m,
					product_id:p, 
					store_id: s,
					store_user_id: su,
					quantity: quantity,
					has_options: has_options,
					variant_id: variant_id,
					product_options:product_options};
		
		if(data.hasOwnProperty('promoter_id')) {
			data.promoter_id = data.promoter_id;	
		}
		//console.log(data);
		update_cart_quantity(data);
	});
	
	$(document).on('click','.prod-info-delete-btn', function(e) {
		e.preventDefault(); e.stopImmediatePropagation();
		var quantity = $(this).parent().find('input').val();
		var m = $(this).parent().find('input').attr('m');
		var p = $(this).parent().find('input').attr('p');
		var variant_id = $(this).parent().find('input').attr('variant_id');
		var su = $(this).parent().find('input').attr('su');
		var s = $(this).parent().find('input').attr('s');
		var has_options = 0;
		
		if(typeof(variant_id) != 'undefined' && variant_id > 0) {
			has_options = 1;
		}
		var data = {product_user_id:m,
					product_id:p, 
					store_id: s,
					store_user_id: su,
					quantity: quantity,
					has_options: has_options,
					variant_id: variant_id};
					
		if(data.hasOwnProperty('promoter_id')) {
			data.promoter_id = data.promoter_id;	
		}
					
		delete_cart_product(data);
	});
	
	$(document).on('click','.remove-order-btn', function(e) {
		e.preventDefault(); e.stopImmediatePropagation();
		var m = $(this).attr('m');
		var p = $(this).attr('p');
		var su = $(this).parent().find('input').attr('su');
		var s = $(this).parent().find('input').attr('s');
		
		var data = {product_user_id:m,
					product_id:p, 
					store_id: s,
					store_user_id: su};
		delete_cart_merchant_product(data);
	});
	
	$(document).on('change', '#shipto_country', function(e){
		if($(this).val() != '') {
			
			var merchant_note = {};
			$('#cart-contentr textarea').each(function() {
				var val = $(this).val();
				var id = $(this).attr('name').replace(/merchant_note_/,'');
				if($.trim(val) != "") {
					merchant_note[id] = val;
					//merchant_note.push($(this).val());	
				}
			});
			$(document).data('co_note',merchant_note);
			store.set('co_note','');
			cartLoader_show();
			load_cart({shipto_country:$(this).val()}, 'shipto');
		}
	});
	
	// Checkout button 
	$(document).on('click', '#checkout-process', function(){
		var merchant_note = {};
		$('#cart-content textarea').each(function() {
			var val = $(this).val();
			var id = $(this).attr('name').replace(/merchant_note_/,'');
			merchant_note[id] = val;
		});
		store.set('co_note',merchant_note);
		store.set('shipto_country',$('#shipto_country').val());

		location.href = "/m/"+CS.storeURI+"/checkout.html";
	});
	
	
	$(document).on('submit', '#checkout-loginform', function(e){ e.preventDefault(); e.stopImmediatePropagation(); checkout_login($(this));});
	$(document).on('click', '#btn-skip-signin', function(e){ e.preventDefault(); e.stopImmediatePropagation(); checkout_guest_goto(1); });
	$(document).on('click', '.cardtype-field a', function(e){ 
		e.preventDefault(); e.stopImmediatePropagation();
		$('.cardtype-field a').removeClass('selected');
		$(this).addClass('selected');
		
		var cctype = $(this).attr('cctype');
		switch(cctype) {
			case 'Visa':	
			case 'MasterCard':
			case 'Discover':
			case 'Amex':
				break;
			default:
				cctype = "Visa";
		}
		$('#cc_type').val(cctype);
	});
	
	$('#shipping-form')
		.on('submit', function (e) {
			e.preventDefault(); e.stopImmediatePropagation();
		})
		.on('invalid', function () {
			var invalid_fields = $(this).find('[data-invalid]');
			console.log(invalid_fields);
		})
		.on('valid', function (e) {
			shipbill_to_address('shipping');
		});	
		
	$('#billing-form')
		.on('submit', function (e) {
			e.preventDefault(); e.stopImmediatePropagation();
		})
		.on('invalid', function () {
			var invalid_fields = $(this).find('[data-invalid]');
			console.log(invalid_fields);
		})
		.on('valid', function (e) {
			shipbill_to_address('billing');
		});
		
	// Card holder's name
	$(document).on('keyup','#billing_firstname, #billing_lastname', function() {
		var name = $.trim($('#billing_firstname').val()) + ' ' + $.trim($('#billing_lastname').val());
		$('#cc_name').val(name);
	});
	
	$(document).on('keyup','#shipping_firstname, #shipping_lastname', function() {
		var name = $.trim($('#shipping_firstname').val()) + ' ' + $.trim($('#shipping_lastname').val());
		
		if($('#use_shipping').val() == "Y") {
			$('#cc_name').val(name);
			
			if($(this).attr('id') == "shipping_firstname") {
				$('#billing_firstname').val($.trim($('#shipping_firstname').val()));
			}
			
			if($(this).attr('id') == "shipping_lastname") {
				$('#billing_lastname').val($.trim($('#shipping_firstname').val()));
			}
		}
	});
	
	$(document).on('click','#ba-checkbox-wrapper .checkbox', function(e) {
		e.stopImmediatePropagation();
		$('#ba-checkbox-wrapper .checkbox').show().attr('selected','selected');
		$(this).hide().removeAttr('selected');
		
		var check = $('#ba-checkbox-wrapper #ba-checkbox-check').attr('selected');
		check = (typeof check == "undefined") ? "" : check;
		
		if(check == "") {
			$('#billing-address-wrapper').show();
			$('#use_shipping').val('N');
			$('#billing-address-wrapper input, #billing-address-wrapper select').removeAttr('required');
			
			if($('#billing-form #billing_country').val() == "US") {
				$('#billing_state_code').attr('required','').parent().show();
				$('#billing_state_name').removeAttr('required').parent().hide();
				toggle_disable_autocomplete('enable','billing');
			}
			else {
				$('#billing_state_name').attr('required','').parent().show();
				$('#billing_state_code').removeAttr('required').parent().hide();
				toggle_disable_autocomplete('disable','billing');
			}
		}
		else {
			$('#use_shipping').val('Y');
			use_shipping_as_billing();	
			$('#billing-address-wrapper').hide();
		}
	});
	
	$(document).on('change','#checkout-form-wrapper select', function(e) {
		if($(this).val() == "") {
			$(this).removeClass('color2').addClass('color1');
		}
		else {
			$(this).removeClass('color1').addClass('color2');	
		}
	});
	
	$(document).on('click','#shipping-form button.back', function(e) {
		e.stopImmediatePropagation();
		
		$('#checkout-form-wrapper').hide();
		$('#shipping-form').hide();
		
		$('#checkout-login-wrapper').show();
	});


	$(document).on('click','#billing-form button.back', function(e) {
		e.stopImmediatePropagation();
		
		$('#shipping-form').show();
		$('#billing-form').hide();
	});
	
	// auto complete
	$(document).on('change','select#shipping_country, select#billing_country', function(e) {
		e.stopImmediatePropagation();
		var id = $(this).attr('id');
		var c = $(this).val();
		var type, state_code, state_name, city, zip;
		if(id == 'shipping_country') {
			type = 'shipping';
			state_code = 'shipping_state_code';
			state_name = 'shipping_state_name';
			city = 'shipping_city';
			zip = 'shipping_zip';
		}
		else {
			type = 'billing';
			state_code = 'billing_state_code';
			state_name = 'billing_state_name';
			city = 'billing_city';
			zip = 'billingg_zip';
		}
		$('input#'+city).val('');
		$('input#'+zip).val('');
		
		if(c == 'US') {
			$('input#'+state_name).removeAttr('required').parent().hide();
			$('select#'+state_code).attr('required','').parent().show();
			$('select#'+state_code).focus();
			
			if($('select#shipping_state_code').val() != '') {
				toggle_disable_autocomplete('enable',type);
			}
			else {
				toggle_disable_autocomplete('disable',type);
			}
		}
		else {
			$('input#'+state_name).attr('required','').parent().show();
			$('select#'+state_code).removeAttr('required').parent().hide();
			$('input#'+state_name).focus();
			
			toggle_disable_autocomplete('disable',type); // disable auto complete for Non-US state	
		}
	});
	
	$(document).on('change','select#shipping_state_code, select#billing_state_code', function(e) {
		e.stopImmediatePropagation();
		var id = $(this).attr('id');
		var city, zip;
		if(id == 'shipping_state_code') {
			type = 'shipping';
			city = 'shipping_city';
			zip = 'shipping_zip';
		}
		else {
			type = 'billing';
			city = 'billing_city';
			zip = 'billing_zip';
		}
		
		$('input#'+city).val('');
		$('input#'+zip).val('');
	
		if($.trim($(this).val()) == '') {
			toggle_disable_autocomplete('disable',type);
		}
		else {
			toggle_disable_autocomplete('enable',type);
		}
	});
	
	$(document).on('change', '#billing_address_id', function(e){
		e.stopImmediatePropagation();
		var data = $(document).data('co_billing_list');
		var id = $(this).val();
		data = data[id];

		var html = 		data.city + ', ' + data.state_name + ' ' + data.zip + '<br /> ' +
						data.country_name + ' <br /> ' +
						'Phone: ' + data.phone + '<br /> \
						<span class="clearfix"></span> \
						<div class="color3" style="margin-top:24px;">Payment Details</div> \
                        <div id="ccdetails-wrapper"> \
                            <div> \
                                <span>Card Type &amp; Number:</span> \
                                <strong>' + data.card.cc_type + ' ************' + data.card.cc_number_short + '</strong> \
								<span class="clearfix"></span> \
                            </div> \
                            <div> \
                                <span>Cardholder\s Name:</span> ' + data.card.cc_firstname  + ' ' + data.card.cc_lastname + '\
								<span class="clearfix"></span> \
                            </div> \
                            <div> \
                                <span>Expiration Date:</span> ' + data.card.cc_expire_month  + ' / ' + data.card.cc_expire_year + ' \
								<span class="clearfix"></span> \
                            </div> \
                        </div>';
		$(document).data('billing_address_id',id);
		$('#billinginfo-details-wrapper').html(html);
	});
	
	
	$(document).on('click', '#shipping-form .btn-existing-address', function(e){
		e.stopImmediatePropagation();
		$('#checkout-form-wrapper').hide();
		$('#shippinglist-wrapper').show();
	});
	
	$(document).on('click', '#billing-form .btn-existing-address', function(e){
		e.stopImmediatePropagation();
		$('#checkout-form-wrapper').hide();
		$('#billinglist-wrapper').show();
	});
	
	// ship to address in shipping address book 
	$(document).on('click', '#btn-shiptoaddress', function(e){
		e.stopImmediatePropagation();
		var id = $(this).parent().find('.address_id').val();
		$(document).data('shipping_address_id',id);
		$('#shippinglist-wrapper').hide();
		
		checkout_guest_goto(2);
	});
	
	$(document).on('click', '#btn-billtoaddress', function(e){
		e.stopImmediatePropagation();
		$(document).data('billing_address_id',$('#billing_address_id').val());
		$('#billinglist-wrapper').hide();
		$('#checkout-content-wrapper #preloader-wrapper').show();
		review_order();
	});
	
	
	$(document).on('click', '#btn-change-shipping-info', function(e){
		e.stopImmediatePropagation();
		checkout_guest_goto(1);
	});
	
	$(document).on('click', '#btn-change-billing-info', function(e){
		e.stopImmediatePropagation();
		
		$('#checkout-content-wrapper #preloader-wrapper').hide();
		checkout_guest_goto(2);

	});
	
	
	$(document)
		.on('submit', '#form-place-order', function (e) {
			e.preventDefault(); e.stopImmediatePropagation();
			purchase_order();
		})
		.on('invalid', '#form-place-order', function () {
			var invalid_fields = $(this).find('[data-invalid]');
			console.log(invalid_fields);
		})
		.on('valid', '#form-place-order', function (e) {
			purchase_order();
		});
		
	$(document).on('change', '#checkout-content select', function(e){
		e.stopImmediatePropagation();
		e.preventDefault();
		update_cart_shipping_selection($(this).attr('mid'),$(this).val());
	});
		
	
});

// number format
function number_format(number,decimals,dec_point,thousands_sep){number=(number+'').replace(/[^0-9+\-Ee.]/g,'');var n=!isFinite(+number)?0:+number,prec=!isFinite(+decimals)?0:Math.abs(decimals),sep=(typeof thousands_sep==='undefined')?',':thousands_sep,dec=(typeof dec_point==='undefined')?'.':dec_point,s='',toFixedFix=function(n,prec){var k=Math.pow(10,prec);return''+Math.round(n*k)/k};s=(prec?toFixedFix(n,prec):''+Math.round(n)).split('.');if(s[0].length>3){s[0]=s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g,sep)}if((s[1]||'').length<prec){s[1]=s[1]||'';s[1]+=new Array(prec-s[1].length+1).join('0')}return s.join(dec)}

function getUrlParam( param ){
	param = param.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
	var exp = "[\\?&]"+param+"=([^&#]*)";
	var regexp = new RegExp( exp );	
	var results = regexp.exec( window.location.href );
	if( results == null ){
		return "";
	} else {
		return results[1];
	}
}

function popUp(URL) {
	day = new Date();
	id = day.getTime();
	eval("page" + id + " = window.open(URL, '" + id + "', 'directories=0,toolbar=0,scrollbars=0,location=0,statusbar=0,menubar=0,resizable=0,width=520,height=364,left=200,top=50');");
}

function toggel_share_userlogin() {
	var user_id = store.get('user_id');
	if(typeof user_id !== 'undefined'){
		var token = store.get('token');
		var form_data = {
			postdata: 1,
			token: token,
			user_id: user_id
		};
		$.post(root + "checkuser.html",form_data,function(data){
			var _data = $.parseJSON(data);
			if( _data.result ){
				$('#link-share-product').attr('uid',_data.user_id);
				$('#sharebox-wrapper .signintxt').hide();
			}
			else {
				$('#sharebox-wrapper .signintxt').show();
			}
		});
	}
	else {
		$('#sharebox-wrapper .signintxt').show();	
	}
}

function get_cart_data() {
	return store.get('cart');
}

function set_cart_data(data) {
	is_remotestorecart = true;
	store_cart_data = data;
	store.set('cart',data);
}

function get_user_id() {
	var user_id = (typeof store.get('user_id') !== "undefined" && store.get('user_id') != 0 && store.get('user_id') != "") ? store.get('user_id') : 0;	
	return parseInt(user_id);
}

function cartLoader_show() {
	$('#content').removeClass('bgcolor2').addClass('bgcolor3');
	$('#cart-wrapper').html('<div id="preloader-wrapper" class="bgcolor3 preloaderCart"> <div class="preloader-holder"> <div id="preloader-center"> <div id="fountainG"> <div id="fountainG_1" class="fountainG"></div> <div id="fountainG_2" class="fountainG"> </div> <div id="fountainG_3" class="fountainG"> </div> <div id="fountainG_4" class="fountainG"> </div> <div id="fountainG_5" class="fountainG"> </div> <div id="fountainG_6" class="fountainG"> </div> <div id="fountainG_7" class="fountainG"> </div> <div id="fountainG_8" class="fountainG"> </div> </div> <div id="preloader-txt" class="color2">loading...</div> </div> </div> </div>');	
}

function load_cart(data, type) {
	data = typeof(data) == 'undefined' ? {} : data;
	if(data.hasOwnProperty('shipto_country') && data.shipto_country != '') {
		data.shipto_country = data.shipto_country;
	}
	else {
		data.shipto_country = store.get('shipto_country');
	}
	
	var form_data = {cart: get_cart_data(),
						cart_sess: store.get('cart_sess'),
						key:store.get('token'),
						user_id:store.get('user_id'),
						m_note:store.get('co_note')};
	
	$.extend(form_data,data);
	var querystr = "";
	
	if(window.location.href.indexOf('?') > 0) {
		querystr = '&' + window.location.href.slice(window.location.href.indexOf('?') + 1);
	}
	
	$.post('/m/'+CS.storeURI+'/ajax.html?content=cartlisting', form_data,function(data){
		$('#cart-wrapper').html(data);
		//$("#cart-wrapper textarea").TextAreaExpander(37);	
		
		if(type == "shipto") {
			$('#content').scrollTop($('#main-wrapper').height()+100000);
		}
	});
}

function load_cartitems(data) {
	data = typeof(data) == 'undefined' ? {} : data;
	var form_data = {user_id:store.get('user_id'),
						token:store.get('token'),
						cart_sess:store.get('cart_sess'), 
						cart: get_cart_data(),
						storeURI:CS.storeURI};
	$.extend(form_data,data);
	$.ajax({
		type: "POST",
		url: "/m/"+CS.storeURI+"/ajax.html?content=cartitems",
		data: form_data,
		crossDomain: true,
		cache: false,
		success: function(response){
			var _data = $.parseJSON(response);
			var total_item = parseInt(_data.total_item);
			var total_price = parseFloat(_data.total_price);
			if(total_item > 0) {
				$('.cartCountValue').html(total_item).show();
			}
			else {
				$('.cartCountValue').html('').hide();
			}
		}
	})
	//.fail(function(jqXHR, textStatus, errorThrown) { alert("cannot load cart"); });
}

function addtocart(data) {
	if(typeof(data) != 'object') return;
	
	var product_user_id = data.product_user_id; // product owner or merchant
	var product_id = data.product_id;
	var store_id = data.store_id;
	var store_user_id = data.store_user_id; // endorser or store owner
	var quantity = data.quantity;
	var has_options = data.has_options;
	data.product_options = {};
	
	// get existing cart values
	var cart = get_cart_data();
	cart = (typeof(cart) != 'object' || cart == null) ? {} : cart;

	//console.log(cart.merchants[user_id].products.hasOwnProperty(1));
	//console.log(cart);
	// retreive existing 
	var item, option = '';
	var error_option = false;
	var variants_values = new Array();
	
	/*
	for (var key in cart) {
		if (cart.hasOwnProperty(key)) {
			total_merchant++;
			new_cart[key] = cart[key]
		}
	}
	*/
	var option_wrapper_name = ".pdetails-cart-option select";

	// Option Validations
	$(option_wrapper_name).each(function() {
		if($(this).val() == '') {
			error_option = true;	
		}
		
		data.product_options[$(this).attr('option_id')] = $(this).val();
		variants_values.push($(this).val());
		
	});

	var options_string = variants_values.join(',');
	var variants = $.parseJSON($('#unique_variants').attr('variants'));
	var variant_id = 0;
	for (var key in variants) {
		if (variants.hasOwnProperty(key)) {
			if(variants[key] == options_string) {
				variant_id = key;	
			}
		}
	}
	if(error_option || (has_options > 0 && variant_id == 0)) {
		
		alert('Please select an option.');
		return;	
	}
	data.variant_id = variant_id;
	data.cart = cart;
	data.cart_sess = store.get('cart_sess');
	data.token = store.get('token');
	data.user_id = store.get('user_id');
	
	//console.log(options_string); return;
	//preLoader($('#ms-wrapper'));
	$.ajax({
		type: "POST",
		url: "/m/"+CS.storeURI+"/ajax.html?content=updatecart",
		data: data,
		cache: false,
		success: function(res){
			var result = $.parseJSON(res);
			store.set('cart',result.cart);
			store.set('cart_sess',result.cart_sess);

			//location.href = "/m/"+CS.storeURI+"/cart.html";
			$('.cart-icon-wrapper').click();
			load_cartitems();
		}
	});
}

function update_cart_quantity(data) {
	cartLoader_show()
	
	if(typeof(data) != 'object') return;

	// get existing cart values
	var cart = get_cart_data();
	cart = (typeof(cart) != 'object' || cart == null) ? {} : cart;
	data.cart = cart;
	data.cart_sess = store.get('cart_sess');
	data.token = store.get('token');
	data.user_id = store.get('user_id');
	
	//preLoader($('#ms-wrapper'));
	$.ajax({
		type: "POST",
		url:  "/m/"+CS.storeURI+"/ajax.html?content=updatecart&update=new_quantity",
		data: data,
		cache: false,
		success: function(res){
			var result = $.parseJSON(res);
			
			store.set('cart',result.cart);
			store.set('cart_sess',result.cart_sess);

			load_cartitems();
			load_cart({shipto_country:$('#shipto_country').val()});
		}
	});
}

function delete_cart_product(data,reloadtype) {
	if(reloadtype == 'placeorder') {
		mainpreLoader();
	}
	else {
		cartLoader_show();
	}
	
	if(typeof(data) != 'object') return;

	// get existing cart values
	var cart = get_cart_data();
	cart = (typeof(cart) != 'object' || cart == null) ? {} : cart;
	data.cart = cart;
	data.cart_sess = store.get('cart_sess');
	data.token = store.get('token');
	data.user_id = store.get('user_id');

	//preLoader($('#ms-wrapper'));
	$.ajax({
		type: "POST",
		url:  "/m/"+CS.storeURI+"/ajax.html?content=updatecart&update=delete_item",
		data: data,
		cache: false,
		success: function(res){
			var result = $.parseJSON(res);
			store.set('cart',result.cart);
			store.set('cart_sess',result.cart_sess);
			
			if(reloadtype == 'placeorder') {
				if(Object.keys(cart).length === 0) {
					location.href = '/cart.html';	
				}
				else {
					load_cartitems();
					ms_co_step3();
				}
			}
			else {
				load_cartitems();
				load_cart({shipto_country:$('#shipto_country').val()});
			}
		}
	});
}

function delete_cart_merchant_product(data,reloadtype) {
	cartLoader_show();
	
	if(typeof(data) != 'object') return;

	// get existing cart values
	var cart = get_cart_data();
	cart = (typeof(cart) != 'object' || cart == null) ? {} : cart;
	data.cart = cart;
	data.cart_sess = store.get('cart_sess');
	data.token = store.get('token');
	data.user_id = store.get('user_id');
	
	$.ajax({
		type: "POST",
		url:  "/m/"+CS.storeURI+"/ajax.html?content=updatecart&update=delete_merchant_items",
		data: data,
		cache: false,
		success: function(res){
			var result = $.parseJSON(res);
			store.set('cart',result.cart);
			store.set('cart_sess',result.cart_sess);
			
			if(reloadtype == 'placeorder') {
				if(Object.keys(cart).length === 0) {
					location.href = '/cart.html';	
				}
				else {
					load_cartitems();
					ms_co_step3();
				}
			}
			else {
				load_cartitems();
				load_cart({shipto_country:$('#shipto_country').val()});
			}
		}
	});
}

function load_checkout_process() {
	var data = {cart: get_cart_data(),
				cart_sess:store.get('cart_sess'),
				token:store.get('token'),
				login_user_id:store.get('user_id'),
				m_note:store.get('co_note'),
				shipto_country:store.get('shipto_country')};

	$.ajax({
		type: "POST",
		url:  "/m/"+CS.storeURI+"/checkout.html?checklogin=true",
		data: data,
		cache: false,
		success: function(response){
			$('#checkout-content-wrapper').removeAttr('class').html(response);
			load_cartitems();
		}
	});
}

function checkout_login($form){
	//$('.loader').remove();
	//$('.ajaxloader').html('<img class="loader" src="'+cloud_url+'images/ajax-loader.gif" />');
	var tz = jstz.determine();
	
	url = $form.attr( 'action' );
	var form_data = {};

	form_data.email = $form.find( 'input[name="email"]' ).val();
	form_data.password = $form.find( 'input[name="password"]' ).val();
	form_data.remember= 1;
	form_data.url = $form.attr( 'action' );
	form_data.temp_sessionid = vid;
	form_data.token = vid;
	form_data.tz = tz.name();
	
	store.set('remember', form_data.remember);
	
	var timestamp = new Date().getTime();
	store.set('session_date', timestamp);
		

	store.set('temp_sessionid', vid);
	store.set('token', vid);
	
	$('.message').html('');
	$('.message').addClass('hide-error');
	
	$.ajax({
		type: "POST",
		url: url,
		data: form_data,
		cache: false,
		success: function(data){
			var _data = $.parseJSON(data);
			if(_data.result == 'failed'){
				$('.message').html(_data.message);
				$('.message').removeClass('hide-error');
			}else{
				store.set('display_name', _data.display_name);
				store.set('user_id', _data.user_id);
				//$('.main-preloader').show();
				location.href = location.href;
			}
		}
	});
	return false;
}

function toggle_disable_autocomplete(enable,type) {
	enable = (typeof enable == 'undefined') ? 'disabled' : enable;
	var state_code, state_name, city, zip;
	if(type == 'shipping') {
		state_code = 'shipping_state_code';
		state_name = 'shipping_state_name';
		city = 'shipping_city';
		zip = 'shipping_zip';
	}
	else {
		state_code = 'billing_state_code';
		state_name = 'billing_state_name';
		city = 'billing_city';
		zip = 'billing_zip';
	}

	if(enable == 'disable') {
		$('input#'+city).autocomplete({ disabled: true });
		$('input#'+zip).autocomplete({ disabled: true });
	}
	else {
		$('input#'+city).autocomplete({ disabled: true });
		$('input#'+zip).autocomplete({ disabled: true });
		
		$('input#'+city).autocomplete( "enable" );
		$('input#'+city).autocomplete({  
			//define callback to format results  
			source: function(req, add){  
				//pass request to server  
				$.getJSON("/autocomplete/US_city.html?state_code="+$('select#'+state_code).val()+"&callback=?", req, function(data) {  
						//create array for response objects  
						var suggestions = [];  
						//process response  
						$.each(data, function(i, val){  
						suggestions.push(val.name);  
					});  

					//pass array to callback  
					add(suggestions);  
				});  
			}, 
			 select: function(e, ui) {  
				//create formatted friend  
				var _city = ui.item.value; 
					$.get("/autocomplete/US_zip.html?state_code="+$('select#'+state_code).val()+"&city="+_city+"&callback=?", function(data) {
						data = data.replace('?','').replace('(','').replace(')','');
						data = $.parseJSON(data);
						
						if(!$.isEmptyObject(data) && data.length == 1) {
							$('input#'+zip).val(data[0].name);	
						}
						else {
							$('input#'+zip).val('');	
						}
					});
			},
			//define select handler  
			change: function() {  
				_city = $.trim($(this).val());
				$.get("/autocomplete/US_zip.html?state_code="+$('select#'+state_code).val()+"&city="+_city+"&callback=?", function(data) {
					data = data.replace('?','').replace('(','').replace(')','');
					data = $.parseJSON(data);
					if(!$.isEmptyObject(data) && data.length == 1) {
						$('input#'+zip).val(data[0].name);	
					}
				});
			}  
		}); 
		
		$('input#'+zip).autocomplete( "enable" );
		$('input#'+zip).autocomplete({  
			//define callback to format results  
			source: function(req, add){  
				//pass request to server  
				$.getJSON("/autocomplete/US_zip.html?state_code="+$('select#'+state_code).val()+"&city="+$('input#'+city).val()+"&callback=?", req, function(data) {  
					//create array for response objects  
					var suggestions = [];  
					//process response  
					$.each(data, function(i, val){  
						suggestions.push(val.name);  
					});  
					//pass array to callback  
					add(suggestions);  
				});  
			}, 
			 select: function(e, ui) {  
				//create formatted friend  
				var _zip = ui.item.value; 
					$.get("/autocomplete/US_city.html?state_code="+$('select#'+state_code).val()+"&zip="+_zip+"&callback=?", function(data) {
						data = data.replace('?','').replace('(','').replace(')','');
						data = $.parseJSON(data);
						if(!$.isEmptyObject(data) && data.length == 1) {
							$('input#'+city).val(data[0].name);	
						}
						else {
							//$('input#'+city).val('');	
						}
					});
			},
			//define select handler  
			change: function() {  
				_zip = $.trim($(this).val());
				$.get("/autocomplete/US_city.html?state_code="+$('select#'+state_code).val()+"&zip="+_zip+"&callback=?", function(data) {
					data = data.replace('?','').replace('(','').replace(')','');
					data = $.parseJSON(data);
					if(!$.isEmptyObject(data) && data.length == 1) {
						$('input#'+city).val(data[0].name);	
					}
					else {
						//$('input#'+city).val('');	
					}
				});
			}  
			 
		}); 
		
	}
}

function use_shipping_as_billing() {
	var fields = new Array ("firstname", "lastname", "company", "address1", "address2", "country", "state_code", "state_name", "city", "zip", "phone");
	var value;
	
	$('#billing-address-wrapper input, #billing-address-wrapper select').attr('required');
	for(var i = 0; i < fields.length; i++) {
		value = $("#shipping_"+fields[i]).val();
		$("#billing_"+fields[i]).val(value);
		
		if(fields[i] == "country") {
			if(value == "US") {
				$("#billing_state_code").removeClass('color1').addClass('color2').parent().show();
				$("#billing_state_name").removeAttr("required").parent().hide();
				toggle_disable_autocomplete('enable','billing');
			}
			else {
				$("#billing_state_code").removeAttr("required").parent().hide();
				$("#billing_state_name").parent().show();
				toggle_disable_autocomplete('disable','billing');
			}
		}
	}
}

function checkout_guest_goto(step) {
	var user_id = get_user_id();
	var shipping_form = $('#shipping-form');
	var billing_form = $('#billing-form');
	
	$('#checkout-login-wrapper').hide();
	$('.checkout_sign_in_modal_wrapper').show();
	$('#checkout-form-wrapper').show();
	
	switch(step) {
		case 1:
			shipping_form.show();
			billing_form.hide();
			
			if(user_id == '') {
				shipping_form.find('button.back').show();
			}
			break;	
			
		case 2:
			if($('#use_shipping').val() == "Y") {
				use_shipping_as_billing();	
			}
		
			shipping_form.hide();
			billing_form.show();
			break;	
	}
}

function shipbill_to_address(type, action, checkingOnly) {
	$('#checkout-form-wrapper').show();
	$('#checkout-form-wrapper #preloader-wrapper').show();
	
	var shipping_form = $('#shipping-form');
	var billing_form = $('#billing-form');
	
	$('#checkout-form-wrapper').find('.field').removeClass('error');
	$('#checkout-form-wrapper').find('.field input').removeAttr('data-invalid');
	
	if(type == "shipping") {
		var shipping_address_id = $(document).data('shipping_address_id');
		var form_data = {"shipping_firstname":$('#shipping_firstname').val(),
							"shipping_lastname":$('#shipping_lastname').val(),
							"shipping_company":$('#shipping_company').val(),
							"shipping_address1":$('#shipping_address1').val(),
							"shipping_address2":$('#shipping_address2').val(),
							"shipping_country":$('#shipping_country').val(),
							"shipping_state_code":$('#shipping_state_code').val(),
							"shipping_state_name":$('#shipping_state_name').val(),
							"shipping_city":$('#shipping_city').val(),
							"shipping_zip":$('#shipping_zip').val(),
							"shipping_phone":$('#shipping_phone').val(),
							"shipping_address_id": shipping_address_id,
						};
		shipping_form.hide();
	}
	else if(type == "billing") {
		var billing_address_id = $(document).data('billing_address_id');
		var form_data = {"billing_firstname":$('#billing_firstname').val(),
							"billing_lastname":$('#billing_lastname').val(),
							"billing_company":$('#billing_company').val(),
							"billing_address1":$('#billing_address1').val(),
							"billing_address2":$('#billing_address2').val(),
							"billing_country":$('#billing_country').val(),
							"billing_state_code":$('#billing_state_code').val(),
							"billing_state_name":$('#billing_state_name').val(),
							"billing_city":$('#billing_city').val(),
							"billing_zip":$('#billing_zip').val(),
							"billing_phone":$('#billing_phone').val(),
							"billing_address_id": billing_address_id,
							"billing_email_address": $("#billing_email_address").val(),
							"cc_type":$("#cc_type").val(),
							"cc_number":$("#cc_number").val(),
							"cc_expire_month":$("#cc_expire_month").val(),
							"cc_expire_year":$("#cc_expire_year").val(),
						};
		billing_form.hide();				
		if(get_user_id() > 0) {
			//form_data.cvv2Number = $('#cvv2Number').val();	
		}
	}
	else {
		return;	
	}
	//console.log(form_data);
	
	var first_error = new Array();
	$.extend(form_data,ms_getQueryParams());
	$.ajax({
		type: "POST",
		url: "/m/"+CS.storeURI+"/checkout.html?step=saveaddress&type="+type+"&checkingOnly="+checkingOnly,
		data: form_data,
		cache: false,
		success: function(response){
			response = $.parseJSON(response);
			if(!response || response == null || $.isEmptyObject(response)) {
				$('#checkout-form-wrapper #preloader-wrapper').hide();
				return false;
			}
			
			var error = response['error'];
			if(typeof(error) == "string") {
				// state, city and/or zip does not match
			}else if(response.hasOwnProperty('error')) {
				if(error['firstname'] || error['lastname']) {
					first_error.push('#' + type + '_lastname');
					$('#' + type + '_lastname').parent().addClass('error').find('input').attr('data-invalid','');	
				}
				if(error['address']) {
					first_error.push('#' + type + '_address1');
					$('#' + type + '_address1').parent().addClass('error').find('input').attr('data-invalid','');	
				}
				if(error['country']) {
					first_error.push('#' + type + '_country');
					$('#' + type + '_country').parent().addClass('error').find('select').attr('data-invalid','');;	
				}
				if(error['state_code']) {
					first_error.push('#' + type + '_state_code');
					$('#' + type + '_state_code').parent().addClass('error').find('select').attr('data-invalid','');
					toggle_disable_autocomplete('enable','' + type + '');	
				}
				if(error['state_name']) {
					first_error.push('#' + type + '_state_name');
					$('#' + type + '_state_name').parent().addClass('error').find('input').attr('data-invalid','');	
					toggle_disable_autocomplete('disable','' + type + '');
				}
				if(error['city']) {
					first_error.push('#' + type + '_city');
					$('#' + type + '_city').parent().addClass('error').find('input').attr('data-invalid','');
				}
				if(error['zip']) {
					first_error.push('#' + type + '_zip');
					$('#' + type + '_zip').parent().addClass('error').find('input').attr('data-invalid','');	
				}
				if(error['phone']) {
					first_error.push('#' + type + '_phone');
					$('#' + type + '_phone').parent().addClass('error').find('input').attr('data-invalid','');
				}
				if(error['statecityzip']) {
					first_error.push('#' + type + '_city');
					$('#' + type + '_city').parent().addClass('error').find('input').attr('data-invalid','');
					$('#' + type + '_zip').parent().addClass('error').find('input').attr('data-invalid','');
				}
				
				if(type == "billing") {
					if(error['billing_email_address']) {
						first_error.push('#billing_email_address');
						$('#billing_email_address').addClass('error').find('input').attr('data-invalid','');	
					}
					if(error['cc_type']) {
						first_error.push('#cc_type');
						$('#cc_type').addClass('error').find('input').attr('data-invalid','');	
					}
					if(error['cc_number']) {
						first_error.push('#cc_number');
						$('#cc_number').addClass('error').find('input').attr('data-invalid','');	
					}
					if(error['cc_expire_month']) {
						first_error.push('#cc_expire_month');
						$('#cc_expire_month').addClass('error').find('select').attr('data-invalid','');	
					}
					if(error['cc_expire']) {
						first_error.push('#cc_expire_year');
						$('#cc_expire_year').addClass('error').find('select').attr('data-invalid','');
					}
					if(error['cvv2Number']) {
						//$('#cvv2Number').validationEngine('showPrompt', 'This is a required field.', 'error', 'centerRight', true);	
					}
				}
				
				$('#checkout-form-wrapper #preloader-wrapper').hide();
				if(type == "shipping") {
					shipping_form.show();
				}
				else if(type == "billing") {
					billing_form.show();
				}
				
				if(first_error.length > 0) {
					var inputPos = $(first_error[0]).offset();
					$(document).scrollTop(inputPos.top);
					$(first_error[0]).focus();
					
					if(error['statecityzip']) {
						alert("State, City and/or Zip does not match.");
					}
				}
			}
			else {
				if(type == "shipping") {
					store.get('shipto_country',$('#shipping_country').val());
					
					if(get_user_id() > 0) {
						$(document).data('shipping_address_id',response.address_id);
					}
					else {
						$(document).data('shipping_address_id',0);
					}
					
					checkout_guest_goto(2);
					$('#checkout-form-wrapper #preloader-wrapper').hide();
				}
				else if(type == "billing") {
					if(get_user_id() > 0) {
						//ms_get_billinglist(true,{billing_address_id:response.address_id});
						$(document).data('billing_address_id',response.address_id);
					}
					else {
						
					}
					
					review_order();
				}
			}
		}
	});
}

function review_order() {
	var form_data = {"shipping_firstname":$('#shipping_firstname').val(),
						"shipping_lastname":$('#shipping_lastname').val(),
						"shipping_company":$('#shipping_company').val(),
						"shipping_address1":$('#shipping_address1').val(),
						"shipping_address2":$('#shipping_address2').val(),
						"shipping_country":$('#shipping_country').val(),
						"shipping_state_code":$('#shipping_state_code').val(),
						"shipping_state_name":$('#shipping_state_name').val(),
						"shipping_city":$('#shipping_city').val(),
						"shipping_zip":$('#shipping_zip').val(),
						"shipping_phone":$('#shipping_phone').val(),
						"shipping_address_id":$(document).data('shipping_address_id'),
						"billing_address_id":$(document).data('billing_address_id'),
						"billing_firstname":$('#billing_firstname').val(),
						"billing_lastname":$('#billing_lastname').val(),
						"billing_company":$('#billing_company').val(),
						"billing_address1":$('#billing_address1').val(),
						"billing_address2":$('#billing_address2').val(),
						"billing_country":$('#billing_country').val(),
						"billing_state_code":$('#billing_state_code').val(),
						"billing_state_name":$('#billing_state_name').val(),
						"billing_city":$('#billing_city').val(),
						"billing_zip":$('#billing_zip').val(),
						"billing_phone":$('#billing_phone').val(),
						"cc_type":$('#cc_type').val(),
						"cc_number":$('#cc_number').val(),
						"cc_expire_month":$('#cc_expire_month').val(),
						"cc_expire_year":$('#cc_expire_year').val(),
					};
	
	if(get_user_id() == 0) {
		form_data.billing_email_address = $('#billing_email_address').val();
	}
	$(document).data('co_selected_SABAPD', form_data);
	$.extend(form_data,ms_getQueryParams());
	$.ajax({
		type: "POST",
		url:  "/m/"+CS.storeURI+"/checkout.html?step=placeorder",
		data: form_data,
		cache: false,
		success: function(response){
			$('#content').html('');
			$('#content').html(response);
			$('.checkout_sign_in_modal_wrapper').hide();
			$('#checkout-form-wrapper #preloader-wrapper').hide();	
		}
	});
}

function update_cart_shipping_selection(mid,value) {
	var data = $.extend(ms_getQueryParams(),$(document).data('co_selected_SABAPD'));
	data.product_user_id = mid;
	data.shipping_selection = value;
	
	$.ajax({
		type: "POST",
		url: "/m/"+CS.storeURI+"/ajax.html?content=updatecart&update=update_shipping_selection",
		data: data,
		cache: false,
		success: function(res){
			cart = $.parseJSON(res);
			
			set_cart_data(cart);
			showLoader('#content');
			review_order();
		}
	});
}


/* Returns values (in json or url querystring format):
 * 		Logged user: user_id and token
 *		Store info: store_key (which identifies store owner and store_id
 */
function ms_getQueryParams(type) {
	type = typeof(type) == 'undefined' ? 0 : 1;
	var params = eval( {login_user_id:store.get('user_id'),
						token:store.get('token'),
						store_id:store.get('store_id'),
						store_url: CS.parent_embed_url,
						cart:get_cart_data(),
						cart_sess:store.get('cart_sess'),
						m_note:store.get('co_note'),
						storeURI:CS.storeURI
						} );
	var data = '';
	var value;
	
	// Json format
	if(!type) {
		data = params;		
	}
	// Serialize format
	else {
		for(var d in params) {
			if(params.hasOwnProperty(d)) {
				value = typeof(params[d]) == 'undefined' ? '' : params[d];
				data += '&' + d + '=' + value;
			}
		}
	}
	
	return data;
}

function purchase_order() {
	var cvv2Number = parseInt($('#cvv2Number').val());
	if(cvv2Number == 0 || isNaN(cvv2Number)) {
		$('#cvv2Number').parent().addClass('error');
		return;	
	}
	
	$('#content #preloader-wrapper').show();
	$('#content #checkout-wrapper').hide();


	var form_data = $(document).data('co_selected_SABAPD');
	form_data.cvv2Number = cvv2Number;
	
	var tz = jstz.determine();
	form_data.tz = tz.name();

	$.extend(form_data,ms_getQueryParams());
	$.ajax({
		type: "POST",
		url: "/m/"+CS.storeURI+"/checkout.html?step=purchaseorder",
		data: form_data,
		cache: false,
		success: function(res){
			var data = $.parseJSON(res);
			var checkout_data = {"cvv2Number" : cvv2Number};
			if(typeof data == "undefined") {
				$('#content #preloader-wrapper').hide();
				$('#content #checkout-wrapper').show();
				return;
			}

			if($.isEmptyObject(data.error) && data.hasOwnProperty('success') && (data.success.hasOwnProperty('u') || data.success.hasOwnProperty('e'))) {
				checkout_data.o = data.success.o;
				checkout_data.u = "";
				checkout_data.e = "";
				// login
				if(data.success.hasOwnProperty('u')) {
					checkout_data.u = data.success.u;
				}
				// guest
				else {
					checkout_data.e = data.success.e;
					checkout_data.cc_number = data.success.card.cc_number;
					checkout_data.cc_type = data.success.card.cc_type;
					checkout_data.cc_expire_month = data.success.card.cc_expire_month;
					checkout_data.cc_expire_year = data.success.card.cc_expire_year;
					checkout_data.cc_firstname = data.success.card.cc_firstname;
					checkout_data.cc_lastname = data.success.card.cc_lastname;
				}
				$.ajax({
					type: "POST",
					url: CS.checkoutURL + "w/checkout",
					data: checkout_data,
					crossDomain: true,
					cache: false,
					success: function(response){
						data2 = $.parseJSON(response);
						if($.isEmptyObject(data2.error)) {
							store.set('shipto_country',form_data.shipping_country);
							store.remove('cart');
							load_cartitems();
							$('#content').removeClass('bgcolor2').addClass('bgcolor3').html($('#txt-order-success').html());
							$('#content').find('a').html(data2.id);
							return false;
						}
						else {
							console.log(data2.error);

							if($.isEmptyObject(data2.error[0].error_code)) {
								var error = "Error message: " + data2.error;
							}
							else {
								var error = "Error code: " + data2.error[0].error_code + "\r\nError message: " + data2.error[0].error_msg;
							}
							alert(error);
							$('#content #preloader-wrapper').hide();
							$('#content #checkout-wrapper').show();
						}
					}
				})
				.done(function(data, textStatus, jqXHR) { })
				.fail(function(jqXHR, textStatus, errorThrown) { console.log(jqXHR); console.log(textStatus); console.log(errorThrown); });
			}
			else {
				var e = '';
				var errors = data.error;
				for(var errortype in errors) {
					if(errors.hasOwnProperty(errortype)) {
						e += errors[errortype] + '\r\n';
					}
				}
				
				alert(e);
				if(data.hasOwnProperty('product_data')) {
					console.log(data.product_data);
				}
				$('#content #preloader-wrapper').hide();
				$('#content #checkout-wrapper').show();
			}
		}
	})
	.done(function(data, textStatus, jqXHR) { })
	.fail(function(jqXHR, textStatus, errorThrown) { console.log(jqXHR); console.log(textStatus); console.log(errorThrown); });
}


function loadSVG(){
	jQuery('img.svg').each(function(){
		var $img = jQuery(this);
		var imgID = $img.attr('id');
		var imgClass = $img.attr('class');
		var imgURL = $img.attr('src');

		jQuery.get(imgURL, function(data) {
			// Get the SVG tag, ignore the rest
			var $svg = jQuery(data).find('svg');

			// Add replaced image's ID to the new SVG
			if(typeof imgID !== 'undefined') {
				$svg = $svg.attr('id', imgID);
			}
			// Add replaced image's classes to the new SVG
			if(typeof imgClass !== 'undefined') {
				$svg = $svg.attr('class', imgClass+' replaced-svg');
			}
			
			// Remove any invalid XML tags as per http://validator.w3.org
			$svg = $svg.removeAttr('xmlns:a');
			
			// Replace image with new SVG
			$img.replaceWith($svg);
		});

	});
}
