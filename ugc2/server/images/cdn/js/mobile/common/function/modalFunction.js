/*
 * RegisterForm
 */
function RegisterForm($form){
	$('.loader').remove();
	
	// if($('#agree').attr('checked')){
		$("#mainRegisterForm button").attr("disabled", "disabled");
		$('.ajaxloader').html('<img class="loader" src="'+cloud_url+'images/ajax-loader.gif" />');
		url = $form.attr( 'action' );
		var form_data = {};

		form_data.firstname = $form.find( 'input[name="firstname"]' ).val();
		form_data.lastname = $form.find( 'input[name="lastname"]' ).val();
		form_data.email = $form.find( 'input[name="email"]' ).val();
		form_data.password = $form.find( 'input[name="password"]' ).val();
		form_data.temp_sessionid = vid;
		form_data.token = vid;
		
		store.set('temp_sessionid', vid);
		store.set('token', vid);
		
		$('r-message').html('');
		$('.r-message').addClass('hide-error');
		
		$.ajax({
			type: "POST",
			url: url,
			data: form_data,
			cache: false,
			success: function(data){
				var _data = $.parseJSON(data);
				
				if(_data.register_result == false){
					$('.r-message').html(_data.error_msg);
					$('.r-message').removeClass('hide-error');
					$("#mainRegisterForm button").removeAttr("disabled");
					
				}else{
					$('.modal_close_level2').trigger('click');
					$('.modal-tap').trigger('click');
				}
				$('.loader').remove();
			}
		});
		return false;
	// }else{
		// alert('You must agree to the terms and conditions');
		// return false;
	// }

}

/*
 * LoginForm
 */
function LoginForm($form){
	//$('.loader').remove();
	//$('.ajaxloader').html('<img class="loader" src="'+cloud_url+'images/ajax-loader.gif" />');
	var tz = jstz.determine();
	
	url = $form.attr( 'action' );
	var form_data = {};

	form_data.email = $form.find( 'input[name="email"]' ).val();
	form_data.password = $form.find( 'input[name="password"]' ).val();
	form_data.remember= 1;
	form_data.url = $form.attr( 'action' );
	form_data.temp_sessionid = vid;
	form_data.token = vid;
	form_data.tz = tz.name();
	
	store.set('remember', form_data.remember);
	
	var timestamp = new Date().getTime();
	store.set('session_date', timestamp);
		

	store.set('temp_sessionid', vid);
	store.set('token', vid);
	
	$('.message').html('');
	$('.message').addClass('hide-error');
	
	$.ajax({
		type: "POST",
		url: url,
		data: form_data,
		cache: false,
		success: function(data){
			var _data = $.parseJSON(data);
			if(_data.result == 'failed'){
				$('.message').html(_data.message);
				$('.message').removeClass('hide-error');
			}else{
				store.set('display_name', _data.display_name);
				store.set('user_id', _data.user_id);
				$('.main-preloader').show();
				location.href = location.href;
			}
			$('.loader').remove();
		}
	});
	return false;
}
/*
 * Logout
 */
function Logout(){
	var url = root + 'signout.html';
	var form_data = {};
		form_data.post_dta = 1;
		form_data.temp_sessionid = store.get('temp_sessionid');
		form_data.token = store.get('token');
		
	$.ajax({
		type: "POST",
		url: url,
		data: form_data,
		cache: false,
		success: function(data){
			if(data == 'success'){
				store.clear();
				store.set('logout_status', 1);
				location.href = location.href;
			}
		}
	});
	return false;
}