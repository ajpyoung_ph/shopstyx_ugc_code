$(window).resize(function() {
	resize_containers();
});

$(document).on('click','a.close-reveal-modal-btn', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	addprod_modal_close();

	var callback = $('.addproduct-modal form').data('beforeClose');
	if(typeof callback == "function") {
		var callbacks = $.Callbacks();
		callbacks.add( callback );
		callbacks.fire();

		addprod_modal_close();
	}
	else {
		addprod_modal_close();
	}
});

$(document).on('click','a.close-basicmodal-btn, .cancel-sell-btn', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	addprod_modal_close();

	var callback = $('.addproduct-modal form').data('beforeClose');
	if(typeof callback == "function") {
		var callbacks = $.Callbacks();
		callbacks.add( callback );
		callbacks.fire();

		addprod_modal_close();
	}
	else {
		addprod_modal_close();
	}
});

$(document).on('click','#advance-btn, #advance-link', function(e) {
	e.preventDefault(); e.stopImmediatePropagation();
	if($(this).parents('.addproduct-modal').hasClass('basic')) {
		addproduct_form_type('advanced');
		//addproduct({data:{formType:"advanced"}});
	}
	else {
		addproduct_form_type('basic');
		//addproduct({data:{formType:"basic"}});
	}
});

$(document).on('change','.reveal-modal select', function() {
	if($(this).hasClass('js-notenabled')) { return false; }
	if($(this).val() != "") {
		$(this).addClass('selected');
	}
	else {
		$(this).removeClass('selected');
	}
});	

$('.reveal-modal').on('mouseenter', 'select', function() {
	if($(this).hasClass('classic-select')) {
		return;
	}
    //$(this).attr('size', 15);
    //$(this).css({position:'absolute', 'z-index':2, width: 350, 'background-image': 'none'});
});

$('.reveal-modal').on('mouseleave', 'select', function() {
	if($(this).hasClass('classic-select')) {
		return;
	}
	//$(this).removeAttr('size');
	//$(this).css({position:'', width: '', 'background-image': ''});
});

$('.reveal-modal').on('click', 'select option', function(e) {
	e.preventDefault(); e.stopImmediatePropagation();
	if($(this).parent().hasClass('classic-select')) {
		return;
	}
	$(this).parent().removeAttr('size');
	$(this).parent().css({position:'', width: '', 'background-image': ''});
});


/* Basic
 *
 */
$(document).on('click','.input-radio input', function(e) {
	e.preventDefault(); e.stopImmediatePropagation();
	var name = $(this).attr('name'),
		wrapper_class = $(this).parent().attr('class');

	
	$(this).parents('.common-field-wrapper').find('input').removeAttr('checked');
	
	if($(this).parents('.optional-details').hasClass('weight-wrapper') || $(this).parents('.optional-details').hasClass('size-wrapper')){
		$(this).parents('.optional-details').find('.input-radio ').removeClass('color2');
		$(this).attr('checked','checked').parent().removeClass('bgcolor2 color1').addClass('color2');
	}else{
		$(this).parents('.common-field-wrapper').find('.input-radio').removeClass('bgcolor1 color2').addClass('color1');
		$(this).attr('checked','checked').parent().removeClass('color1').addClass('color2');
	}

	if($(this).attr('name') == "payout_type") {
		if($(this).val() == "0") {
			$('input.payoutrate').removeClass('decimal-4').addClass('decimal');
			$("input.decimal").floatnumber('.',2);
		}
		else {
			$('input.payoutrate').removeClass('decimal').addClass('decimal-4');
			$("input.decimal-4").floatnumber('.',4);
		}
	}

});

$(document).on('click','.addproduct-modal .color-wrapper ul li', function(e) {
	e.preventDefault(); e.stopImmediatePropagation();
	var color = $(this).attr('data');
	$('.addproduct-modal .color-wrapper ul li').removeClass('selected');
	$(this).addClass('selected');

	$('.addproduct-modal input[name="color"]').val(color);
});

$(document).on('click','.addproduct-modal .mystyled-select.selectstyle1', function(e) {
	e.preventDefault(); e.stopImmediatePropagation();
	var el = $(this);
	var clone = el.find('.selection-list .scrollbar').clone();

	if(el.find('.selection-list .slimScrollDiv').length) {
		el.find('.selection-list .slimScrollDiv').remove();
		el.find('.selection-list').prepend(clone);
		el.find('.selection-list .scrollbar').removeAttr('style');
	}

	$(this).find('.scrollbar').slimScroll();
	$(this).find('.selection-list').show();
	$(this).addClass('focus');
});

$(document).on('blur','.addproduct-modal .mystyled-select.selectstyle1', function() {
	if($(this).find('.collection-entry-wrapper').css('display') == "none") {
		$(this).find('.selection-list').hide();
		$(this).removeClass('focus');
	}
});

$(document).on('click','.addproduct-modal .mystyled-select.selectstyle1 .selection-list ul li', function(e) {
	e.preventDefault(); e.stopImmediatePropagation();
	var el = $(this), collection_id = el.attr('value'), text = el.html();

	el.parents('.selection-list').hide();
	el.parents('.mystyled-select').removeClass('focus').find('.text-selected').html(text);
	el.parents('.common-field-wrapper').find('input[name="collection_id"]').val(collection_id);
});

$(document).on('click','.addproduct-modal .mystyled-select.selectstyle1 .selection-list ul li .edit-btn', function(e) {
	e.preventDefault(); e.stopImmediatePropagation();
	var collection_id = $(this).parent().attr('value');
	load_addedit_collection_form(collection_id);
});

$(document).on('click','.addproduct-modal .create-collection-link, .ap-content-categories .collectionlist-wrapper li.add', function(e) {
	e.preventDefault(); e.stopImmediatePropagation();
	load_addedit_collection_form(0);
});

$(document).on('click','.addproduct-modal .collection-entry-wrapper', function(e) {
	e.preventDefault(); e.stopImmediatePropagation();
});
$(document).on('click','.addproduct-modal .collection-entry-wrapper .btns .cancel', function(e) {
	e.preventDefault(); e.stopImmediatePropagation();
	$('.addproduct-modal').find('.collection-entry-wrapper').hide();
	$('.addproduct-modal .collection-entry-wrapper input').val('').parent().removeClass('error');
	$('.addproduct-modal .collection-entry-wrapper textarea').val('');
});

$(document).on('click','.addproduct-modal .collection-entry-wrapper .btns .create', function(e) {
	e.preventDefault(); e.stopImmediatePropagation();
	save_collection();
});

$(document).on('change','#main-deparment', function(){
	$('#sub-department')
		.empty()
		.append('<option value="" disabled="disabled" selected="selected">What type?</option>')
		.removeClass('color1  color2')
		.addClass('disabled loading')
		.attr('disabled', 'disabled');
	$this = $(this);

	get_subdepartments({
		data:{category_id:$this.val()},
		success: function(results){
			var results = $.parseJSON(results);
			for(i in results) {
				$('#sub-department').append("<option class=\"color2\" value=\""+results[i].category_id+"\">"+results[i].category_name+"</option>");
			}

			if($this.val() != "") {
				$('#sub-department').removeAttr('disabled').removeClass('disabled').addClass('color1');
			}
			else {
				$('#sub-department').attr('disabled','disabled').addlass('disabled').removeClass('color1');
			}	
			$('#sub-department').removeClass('loading');	
		}
	});
});	

$(document).on('mouseenter','.input-cat-wrapper', function(e) {
	$('#category-content-wrapper').show();
});

$(document).on('mouseleave','.input-cat-wrapper', function(e) {
	$('#category_name').parent('.common-field-wrapper').removeClass('error');
	setTimeout(function(){$('#category-content-wrapper').hide();},100);
});

$(document).on('click','.addon-imgdesc-btn', function(e) {
	e.preventDefault(); e.stopImmediatePropagation();
	$(this).hide();
	$('#addon-imgdesc-wrapper').show();
});

$(document).on('click','.addnew-wrapper .update-btn.addnew', function(e) {
	e.preventDefault(); e.stopImmediatePropagation();

	$('.cat-img-upload-wrapper').removeClass('setimage').removeAttr('style');
	$('.cat-img-upload-wrapper span').show();
	$('.cat-img-upload-wrapper img').remove();
	$('#category_name').val('')
	$('#category_parent_id').val(0);
	$('#category_desc').val('');
	$('#addon-imgdesc-wrapper').hide();
	$('.addon-imgdesc-btn').show();
	$('#save-category-btn').html('Add');


	$('#cat-list-wrapper').hide();
	$('#cat-addform').attr('cat_id',0).show();
});

$(document).on('click', '.back-link', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	$("#cat_id_assign")[0].selectedIndex = 0; 
	$('#cat_id_assign option').attr('style','');
	$('#category_parent_id option').attr('style','');

	$('#cat-addform').hide().attr('cat_id',0);
	$('#cat-assignform').hide().attr('cat_id',0);
	$('#cat-list-wrapper').show();
});

$(document).on('click', '#cat-list-wrapper li:not(.notSelectable) label .checkbox-wrapper', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	/*
	if($(this).not('.notSelectable').find('label .checkbox-wrapper')) {
		alert('a');
	}
	*/

	var cat_id = $(this).find('input').val();

	if($(this).hasClass('checked')) {
		$(this).removeClass('checked')
			.find('input').removeAttr('checked');

		if($(this).parents('li.notSelectable').length && $(this).parents('.sub-cat').find('.checkbox-wrapper.checked').length == 0) {
			$(this).parents('li.notSelectable').find('.checkbox-wrapper').removeClass('checked');
			$(this).parents('li.notSelectable').find('.checkbox-wrapper input').removeAttr('checked');
		}
		$('#selected-cat-texts').find("#cat-text-wrapper-ID"+cat_id).remove();

		if($('.cat-list-scroller').find('.checkbox-wrapper.checked').length == 0) {
			$('#selected-cat-texts .placeholder').html("Please add a cateogry.");
		}
	}
	else {
		$(this).addClass('checked');
		$(this).find('input').attr('checked','checked');
		$('#selected-cat-texts .placeholder').html('');

		if($(this).parents('li.notSelectable').length) {
			$(this).parents('li.notSelectable').find('.main-cat .checkbox-wrapper').addClass('checked');
		}

		var text = $(this).attr('text');

		if($('#selected-cat-texts .cat-text-wrapper').length == 0 ) {
			$('#selected-cat-texts').prepend('<div class="cat-text-wrapper" id="cat-text-wrapper-ID'+cat_id+'">\
												<span class="text-label">'+text+'</span>\
												<a class="text-remove" cat_id="'+cat_id+'"></a>\
											</div>');
		}
		else {
			$('#selected-cat-texts .cat-text-wrapper:last').after('<div class="cat-text-wrapper" id="cat-text-wrapper-ID'+cat_id+'">\
												<span class="text-label">'+text+'</span>\
												<a class="text-remove" cat_id="'+cat_id+'"></a>\
											</div>');
		}
	}

	if($('.input-cat-wrapper #selected-cat-texts').height() > 26) {
		$('#category-content-wrapper').css('top','63px');
	}
	else {
		$('#category-content-wrapper').css('top','37px');
	}

	$('#cat-error-display').removeClass('error');
});	

$(document).on('click', '.cat-text-wrapper .text-remove', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	$(this).parent().remove();
	var cat_id = $(this).attr('cat_id');
	$('#cat-list-wrapper input[value="'+cat_id+'"]').removeAttr('checked');
	$('#cat-list-wrapper input[value="'+cat_id+'"]').parent().removeClass('checked');

	if($('#cat-list-wrapper input[value="'+cat_id+'"]').parents('.sub-cat').length 
	&& $('#cat-list-wrapper input[value="'+cat_id+'"]').parents('.sub-cat').find('.checkbox-wrapper.checked').length == 0) {
		$('#cat-list-wrapper input[value="'+cat_id+'"]').parents('li').find('.main-cat .checkbox-wrapper').removeClass('checked');
		$('#cat-list-wrapper input[value="'+cat_id+'"]').parents('li').find('.main-cat .checkbox-wrapper input').removeAttr('checked');
	}

	if($('.input-cat-wrapper #selected-cat-texts').height() > 26) {
		$('#category-content-wrapper').css('top','63px');
	}
	else {
		$('#category-content-wrapper').css('top','37px');
	}
});

$(document).on('click', '#cat-list-wrapper li .action-btn-wrapper .del-btn', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	addprod_delete_category($(this).parent().attr('cat_id'),$(this));
});

$(document).on('click', '#cat-list-wrapper li .action-btn-wrapper .edit-btn', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	addprod_edit_category($(this).parent().attr('cat_id'));
});

$(document).on('click', '#save-category-btn', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	addprod_save_category();
});

$(document).on('change', '.cat-img-upload-wrapper input', function(e){
	var input = this;
	if (input.files && input.files[0]) {

		var ftype = input.files[0].type;
		var fname = input.files[0].name.split('.');
		var flength = fname.length-1;
		var fext = fname[flength];
		//var fname = fname.spli

		if (!ftype.match('image.*') && !check_allowed_image_filetype(ftype,fext)) {
			return;
		}

        var reader = new FileReader();

        reader.onload = function (e) {
            //$('.cat-img-upload-wrapper img').attr('src', e.target.result);
            $('.cat-img-upload-wrapper span').hide();
            if($('.cat-img-upload-wrapper img').length == 0) {
            	$('.cat-img-upload-wrapper').append('<img src="'+e.target.result+'" />');
		    }
	        else {
	        	$('.cat-img-upload-wrapper img').attr('src', e.target.result);
	        }
            $('.cat-img-upload-wrapper').addClass('auto');
        };

        reader.readAsDataURL(input.files[0]);
    }
});

$(document).on('click', '#cat-assign-btn', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	addprod_transfer_category();
});

$(document).on('change', '#fileupload_photos', function(e){
	var max_images = $('.addproduct-modal form').attr('max_images');
	var input = this;
	var limit_reached = false;
	if (input.files && input.files[0]) {

        for(var i = 0, f; f = this.files[i]; i++) {
	        // Only process image files.
	        var ftype = f.type;
			var fname = f.name.split('.');
			var flength = fname.length-1;
			var fext = fname[flength];

	        // Only process image files.
			if (!ftype.match('image.*') && !check_allowed_image_filetype(ftype,fext)) {
				continue;
			}
			else {
				if(limit_reached) {
					continue;
				} else {
					if($('.photo-wrapper .upload-image-bg img').length >= max_images) {
						//alert("You have reached the maximum photos that can be uploaded.");
						limit_reached = true;
						$('.photo-wrapper .tbl-cell.holder').parents('li').addClass('hide');
						continue;
					}

					
				}
			}

			var reader = new FileReader();

	        reader.onload = (function(theFile) {
		        return function(e) {
		        	if($('.photo-wrapper .upload-image-bg img').length >= max_images) {
			        	//alert("You have reached the maximum photos that can be uploaded.");
						limit_reached = true;
						$('.photo-wrapper .tbl-cell.holder li').addClass('hide');
		        	}
		        	else {
		        		var img_length = $('.photo-wrapper .tbl-cell.holder').not('.holder').find('img').length;
			        	var holder = $('.photo-wrapper .tbl-cell.holder').parents('li');
			        	var clone = holder.clone();
			        	var input = holder.html();
			        	var next_element = holder.next();
			        	var left = 0;

	        			holder.removeClass('bordercolor1').addClass('bordercolor8')
	        				.find('input').remove()
	        			holder.find('.upload-image-bg div.color1').remove();
		        		holder.find('.upload-image-bg').addClass('auto')
		        			.append('<img alt="" src="'+e.target.result+'" />');
		        		holder.find('.image-btn-wrapper').removeClass('hide');	
		        		holder.find('.tbl-cell').removeClass('holder');

		        		if(next_element.length == 0) {
		        			holder.last().after(clone);
		        		}	
		        		else {
				        	next_element.removeClass('bordercolor8').addClass('bordercolor1');
				        	next_element.html(input);
			       	 	}

			       	 	var holder_length = $('.photo-wrapper li').length;

	        			if($('.photo-wrapper .upload-image-bg img').length >= max_images) {
	        				if(!limit_reached){
	        					//alert("You have reached the maximum photos that can be uploaded.");
	        				}
							limit_reached = true;
							$('.photo-wrapper .tbl-cell.holder').parents('li').addClass('hide');
							holder_length--;
						}
						else {
							//left = $('.photo-wrapper .upload-image-bg.holder').position().left;

						}
			       	 	
		        		$('.photo-wrapper .slider').width(holder_length*138);	
		        		$('.addproduct-modal form').data('image_files').push(theFile);

		        		$('.photo-wrapper').scrollLeft(holder_length*138);
		        	}
		        };
		      })(f);

	        // Read in the image file as a data URL.
			reader.readAsDataURL(f)
    	}
    }
});


$(document).on('click','.photo-wrapper .delete-img', function(e){
	e.preventDefault(); e.stopImmediatePropagation();

	var max_images = $('.addproduct-modal form').attr('max_images');
	var el = $(this).parents('li');
	var index = $('.photo-wrapper ul li').index(el);

	if($('.photo-wrapper .upload-image-bg img').length >= 3) {
		el.remove();
		if($('.photo-wrapper .upload-image-bg img').length < max_images) {
			$('.photo-wrapper ul li').last().removeClass('hide');
		}
	}
	else {
		var html = '<div class="color1">Upload Image</div><input id="fileupload_photos" type="file" name="files[]" multiple="mulitple" style="opacity:0;" />';
		/*
		*/

		if($('.photo-wrapper .upload-image-bg img').length <= 2) {
			var new_li = '<li class="bordercolor8"> <div class="tbl-cell"> <div class="inner-holder"><div class="upload-image-bg"></div></div></div></li>';

			el.remove();
			$('.photo-wrapper ul').append(new_li);

		}
		else {
			el.next().removeClass('bordercolor1').addClass('bordercolor8')
				.find('.tbl-cell').removeClass('holder').html('<div class="upload-image-bg"></div>');
		}


	}

	var holder_length = $('.photo-wrapper li').length;
	$('.photo-wrapper .slider').width(holder_length*125);
	$('.photo-wrapper').scrollLeft(holder_length*125);

	$('.addproduct-modal form').data('image_files').splice(index,1);
});

/* Advanced */

$(document).on('click','#ap-left-menu li', function(e){
	e.preventDefault(); e.stopImmediatePropagation();

	$('#ap-left-menu li').removeClass('selected');
	$(this).addClass('selected');
	switch($(this).index()) {
		case 0: container = '.ap-content-details'; break;
		case 1: container = '.ap-content-media'; break;
		case 2: container = '.ap-content-categories'; break;
		case 3: container = '.ap-content-options'; break;
		case 4: container = '.ap-content-shipping'; break;
		case 5: container = '.ap-content-seo'; break;
	}

	$('.ap-content').hide();
	$('.addproduct-modal.advanced .content .shopstyxloader').removeClass('hide');

	$(container).show(); $('.addproduct-modal.advanced .content .shopstyxloader').addClass('hide');
	resize_containers();
});

$(document).on('keyup, blur', '#product-entry-product_name', function(e) {
	var val = $.trim($(this).val());
	$('#product-entry-seo_title').val(val);
	var seo_url = val.toLowerCase().replace(/[^a-z0-9 -]/g,'').replace(/\s+/g,'-').replace(/-+/g, '-');
	$('#product-entry-seo_url').val(seo_url);

	if(val != "") {
		$('#gg-result-preview').removeClass('hide');
		$('#gg-result-preview .gg-title').html(val.substr(0,40));
	}
	else {
		$('#gg-result-preview .gg-title').html('');
		//$('#gg-result-preview').addClass('hide');
	}
	
});

$(document).on('keyup', '#product-entry-seo_url', function(e) {
	var val = $.trim($(this).val());
	var seo_url = val.toLowerCase().replace(/[^a-z0-9 -]/g,'').replace(/\s+/g,'-').replace(/-+/g, '-');
	$('#product-entry-seo_url').val(seo_url);
});

$(document).on('blur', '#product-entry-seo_url', function(e) {
	var val = $.trim($(this).val());
	var seo_url = val.toLowerCase().replace(/[^a-z0-9 -]/g,'').replace(/\s+/g,'-').replace(/-+/g, '-');

	if (seo_url.charAt(seo_url.length - 1) == '-') {
		seo_url = seo_url.substr(0, seo_url.length - 1);
	}
	$('#product-entry-seo_url').val(seo_url);
});

$(document).on('keyup', '#product-entry-product_desc', function(e) {
	var val = $.trim($(this).val()).substr(0,500);
	$('#product-entry-seo_desc').val(val);

	var name = $.trim($('#product-entry-seo_title').val());
	if(name != "") {
		$('#gg-result-preview').removeClass('hide');
	}
	else {
		//$('#gg-result-preview').addClass('hide');
	}

	$('#gg-result-preview .gg-desc').html(val.substr(0,100));
});

$(document).on('keyup', '#product-entry-seo_title', function(e) {
	var val = $.trim($(this).val());
	var seo_url = val.toLowerCase().replace(/[^a-z0-9 -]/g,'').replace(/\s+/g,'-').replace(/-+/g, '-');

	if (seo_url.charAt(seo_url.length - 1) == '-') {
		seo_url = seo_url.substr(0, seo_url.length - 1);
	}
	$('#product-entry-seo_url').val(seo_url);


	if(val != "") {
		$('#gg-result-preview').removeClass('hide');
	}
	else {
		//$('#gg-result-preview').addClass('hide');
	}

	$('#gg-result-preview .gg-title').html(val.substr(0,40));
});

$(document).on('keyup', '#product-entry-seo_desc', function(e) {
	var val = $.trim($(this).val());
	var name = $.trim($('#product-entry-seo_title').val());

	if(name != "") {
		$('#gg-result-preview').removeClass('hide');
	}
	else {
		//$('#gg-result-preview').addClass('hide');
	}

	$('#gg-result-preview .gg-desc').html(val.substr(0,100));
});


$(document).on('click','#show-weight-btn', function(e) {
	e.preventDefault(); e.stopImmediatePropagation();
	$('#weight-wrapper').show();
	$(this).hide();
	if($('#size-wrapper').css('display') != "none") {
		$('.optional-product-info').hide();
	}
	resize_containers()
});

$(document).on('click','#show-size-btn', function(e) {
	e.preventDefault(); e.stopImmediatePropagation();
	$('#size-wrapper').show();
	$(this).hide();
	if($('#weight-wrapper').css('display') != "none") {
		$('.optional-product-info').hide();
	}	
	resize_containers()
});

$(document).on('blur','#video_url', function(e) {
	e.preventDefault(); e.stopImmediatePropagation();
	var url = $.trim($(this).val());
	$('#videoplayer-wrapper').html('');
	$('#videoplayer-wrapper').hide();
	$('#video-wrapper span').show();
	//$('#video-wrapper').addClass('loader');
	if(url == '') {
		return false;
	}
	
	addprod_checkvideo_url(url);
});

// Image
$(document).on('change', '#product_images', function(e){
	var max_images = $('.addproduct-modal form').attr('max_images');
	var input = this;
	var limit_reached = false;
	if (input.files && input.files[0]) {
        for(var i = 0, x = 0, f; f = this.files[i]; i++) {
        	/*
	        reader.onload = function (e) {
	            //$('.cat-img-upload-wrapper img').attr('src', e.target.result);
	            //$('.photo-wrapper .upload-image-bg div').setimage();
	            if($('.photo-wrapper .upload-image-bg').eq().find('img').length == 0) {
	            	console.log($('.photo-wrapper .upload-image-bg').eq(i).attr('dd','dd'));
	            	$('.photo-wrapper .upload-image-bg').eq(i).append('<img src="'+e.target.result+'" />');
			    }
		        else {
		        	$('.photo-wrapper .upload-image-bg').eq(i).find('img').attr('src', e.target.result);
		        
	            $('.photo-wrapper .upload-image-bg').eq(i).addClass('auto');
	        };
	        reader.readAsDataURL(input.files[i]);
	        */
	        var ftype = f.type;
			var fname = f.name.split('.');
			var flength = fname.length-1;
			var fext = fname[flength];

	        // Only process image files.
			if (!ftype.match('image.*') && !check_allowed_image_filetype(ftype,fext)) {
				continue;
			}
			else {

				if(limit_reached) {
					continue;

				} else {
					if($('.image-wrapper li').not('.uploadbtn').length >= max_images) {
						alert("You have reached the maximum photos that can be uploaded.");
						limit_reached = true;
						$('.image-wrapper li.uploadbtn').addClass('hide');
						continue;
					}

					
				}
			}

			var reader = new FileReader();

	        reader.onload = (function(theFile) {
		        return function(e) {
		        	//console.log(e);
					if($('.image-wrapper li').not('.uploadbtn').length >= max_images) {
						limit_reached = true;
						$('.image-wrapper li.uploadbtn').addClass('hide');
					}
					else {
			        	var holder_length = $('.image-wrapper li').not('.uploadbtn').length;
			        	var clone = $('.image-wrapper li.uploadbtn').clone();

		        		clone.removeClass('uploadbtn').addClass('toBeUploaded')
		        			.find('.uploadholder').addClass('preview')
		        			.find('div.color1, input').remove();
		        		clone.find('.uploadholder').find('.inner-wrapper').append('<img src="'+e.target.result+'" />');
		        		clone.find('.image-btn-wrapper').removeClass('hide');


			        	if(holder_length == 0) {
			        		$('.image-wrapper ul').prepend(clone);

			        	}
			        	else {
			        		$('.image-wrapper ul li').not('.uploadbtn').last().after(clone);
			        	}
			        	
			        	if($('.image-wrapper li').not('.uploadbtn').length >= max_images) {
			        		limit_reached = true;
							$('.image-wrapper li.uploadbtn').addClass('hide');
			        	}
			        	
			        	var files = $('.addproduct-modal form').data('image_files');
			        	files = typeof(files) == "undefined" ? [] : files;
			        	files.push(theFile);
			        	$('.addproduct-modal form').data('image_files',files);
			        	//$('.addproduct-modal form').data('image_files').push(theFile);
			        	resize_containers();
					}
		        };
		      })(f);

	        // Read in the image file as a data URL.
			reader.readAsDataURL(f)
    	}
    }
});

$(document).on('click','.image-wrapper li .featured-img', function(e) {
	e.preventDefault(); e.stopImmediatePropagation();
	var sort_order = $(this).parents('.image-btn-wrapper').attr('sort_order');

	$('.image-wrapper li .featured-img').removeClass('selected');
	$(this).addClass('selected');
	set_default_product_photo({sort_order:sort_order, product_id:get_product_id()});
});

$(document).on('click','.image-wrapper li .delete-img', function(e) {
	e.preventDefault(); e.stopImmediatePropagation();

	var max_images = $('.addproduct-modal form').attr('max_images');

	if($(this).parents('li').hasClass('toBeUploaded')) {
		var index = $('.image-wrapper li.toBeUploaded').index($(this).parents('li'));
		$('.addproduct-modal form').data('image_files').splice(index,1);
		$(this).parents('li').remove();

		var total = $('.image-wrapper li').not('.uploadbtn').length;
		if(total < max_images) {
			$('.image-wrapper li.uploadbtn').removeClass('hide');
		}
	}
	else {
		var sort_order = $(this).parents('.image-btn-wrapper').attr('sort_order');
		delete_product_photo({sort_order:sort_order, product_id:get_product_id(), deleted:'Y'});
		//$(this).parents('li').remove();
		$(this).parents('li').find('.undo-wrapper').removeClass('hide');

		var img_ids = $('input[name="product_images_tobe_deleted"]').val();
		img_ids = (img_ids == "") ? sort_order : img_ids + ',' + sort_order;
		$('input[name="product_images_tobe_deleted"]').val(img_ids);

		if($('.image-wrapper li').not('.uploadbtn').length < max_images) {
			$('.image-wrapper li.uploadbtn').removeClass('hide');
		}
	}
});

$(document).on('click','.image-wrapper li .undo-wrapper button', function(e) {
	e.preventDefault(); e.stopImmediatePropagation();

	var sort_order = $(this).parents('li').find('.image-btn-wrapper').attr('sort_order');
	var img_ids = $('input[name="product_images_tobe_deleted"]').val();

	$(this).parent().addClass('hide');
	img_ids = img_ids.replace("," + sort_order, "");
	img_ids = img_ids.replace(sort_order + ",", "");
	img_ids = img_ids.replace(sort_order, "");
	$('input[name="product_images_tobe_deleted"]').val(img_ids);
	delete_product_photo({sort_order:sort_order, product_id:get_product_id(), deleted:'N'}); 
});

$(document).on('click','.image-wrapper li .alt-img', function(e) {
	e.preventDefault(); e.stopImmediatePropagation();
	var sort_order = $(this).parents('.image-btn-wrapper').attr('sort_order');
	var product_id = get_product_id();
	get_product_photo_alt(this,{sort_order:sort_order, product_id:product_id});
});

$(document).on('click','#productimageAltModal a, .overlay.product_photo_alt', function(e) {
	e.preventDefault(); e.stopImmediatePropagation();
	$('#productimageAltModal').removeClass('open');
	$('.addproduct-modal .overlay.mainLoader').addClass('hide').removeClass('product_photo_alt');
	$('#productimageAltModal .content-wrapper').addClass('hide').find('.imgholder').html('');
	$('#productimageAltModal .overlay, #productimageAltModal .shopstyxloader').removeClass('hide');
});

$(document).on('click','#productimageAltModal button', function(e) {
	e.preventDefault(); e.stopImmediatePropagation();
	var sort_order = $('#productimageAltModal').attr('sort_order');
	var product_id = get_product_id();
	var alt = $('#productimageAltModal input').val();
	save_product_photo_alt({sort_order:sort_order, product_id:product_id, alt:alt});
});

$(document).on('click','.overlay.product_photo_alt', function(e) {
	e.preventDefault(); e.stopImmediatePropagation();
});

// Collection & Category
$(document).on('click','.ap-content-categories .collectionlist-wrapper li:not(.add)', function(e) {
	e.preventDefault(); e.stopImmediatePropagation();
	$('.ap-content-categories .collectionlist-wrapper li').removeClass('selected');
	$(this).addClass('selected');
	$('.ap-content-categories input[name="collection_id"]').val($(this).attr('collection_id'));
});

$(document).on('click','.ap-content-categories .department-list-wrapper li', function(e) {
	e.preventDefault(); e.stopImmediatePropagation();
	$('.ap-content-categories .department-list-wrapper li').removeClass('selected');
	$(this).addClass('selected');
	$('.ap-content-categories input[name="department_id"]').val($(this).attr('data-value'));
});

// Option & inventory

$(document).on('change','.inventory-lbl .prod-inv-type', function(e) {
	e.preventDefault(); e.stopImmediatePropagation();
	var val = $(this).val();
	$(this).removeClass('I').removeClass('Q').removeClass('O');
	$(this).addClass(val);
	$('.prod-inv-details-wrapper .inv-track, .prod-inv-details-wrapper .inv-track-variants').addClass('hide');
	if(val == 'Q') {
		$('.prod-inv-details-wrapper .inv-track').removeClass('hide');
		$('.inventory-lbl .qty-wrapper').find('input[type="text"]').focus();
	}
	if(val == 'O') {
		$('.prod-inv-details-wrapper .inv-track-variants').removeClass('hide');
		$('.col-variants tr.data td.input-qty input').removeAttr('disabled');
	}
	else {
		$('.col-variants tr.data td.input-qty input').attr('disabled','disabled').val(0);
	}
});

$(document).on('click','.inventory-lbl .inventory-type', function(e) {
	e.preventDefault(); e.stopImmediatePropagation();

	//if($('.inventory-lbl .inventory-type').index($(this)) == 2) return false;
	if($(this).hasClass('disabled')) { 
		$(this).removeClass('selected').find('input[type="radio"]').removeAttr('checked');
		return false; 
	}
	$('.inventory-lbl .inventory-type').removeClass('selected').find('input').removeAttr('checked');
	$(this).addClass('selected').find('input[type="radio"]').attr('checked','checked');

	if($('.inventory-lbl .inventory-type').index($(this)) == 1) {
		$(this).find('input[type="text"]').focus();
	}

	if($('.inventory-lbl .inventory-type').index($(this)) == 2) {
		$('.col-variants tr.data td.input-qty input').removeAttr('disabled');
	}
	else {
		$('.col-variants tr.data td.input-qty input').attr('disabled','disabled').val(0);
	}
});

$(document).on('click','.prod-inv-details-wrapper .checkbox-wrapper', function(e) {
	e.preventDefault(); e.stopImmediatePropagation();

	if($(this).hasClass('checked')) {
		$(this).removeClass('checked')
			.find('input').removeAttr('checked','checked');
	}
	else {
		$(this).addClass('checked')
			.find('input').attr('checked','checked');
	}
});

$(document).on('click','.add-variants-wrapper .checkbox-wrapper', function(e) {
	e.preventDefault(); e.stopImmediatePropagation();

	if($(this).hasClass('checked')) {
		$(this).removeClass('checked')
			.find('input').removeAttr('checked','checked');
	}
	else {
		$(this).addClass('checked')
			.find('input').attr('checked','checked');
	}
});

$(document).on('click','#option-entry-wrapper .addoption button', function(e) {
	e.preventDefault(); e.stopImmediatePropagation();
	$('#option-entry-wrapper').addClass("add");
});


$(document).on('change','#option-entry-wrapper.add ul li label select', function(e) {
	e.preventDefault(); e.stopImmediatePropagation();
	var val = $(this).val();
	if(val == -1) {
		$(this).parents('label')
			.addClass('nomargin')
				.parent().find('input.new-option-name')
				.parents('label').removeClass('hide')
				.find('input').focus();
	}
	else {
		$(this).parents('label').removeClass('nomargin').parent().find('input').eq(0).parents('label').addClass('hide');
	}
});

$(document).on('click','#option-entry-wrapper.add ul li.next-option button', function(e) {
	e.preventDefault(); e.stopImmediatePropagation();
	$(this).parents('li').attr('enabled','true');
	$(this).parent().find('.fader').addClass('hide');
	$(this).addClass('hide');

	$('#option-entry-wrapper.add ul.list-option-entry-wrapper li.option-li').eq(2).removeClass('hide').addClass('next-option');
});

$(document).on('click','#option-entry-wrapper .add-variants-wrapper button', function(e) {
	e.preventDefault(); e.stopImmediatePropagation();
	var el = $('#option-entry-wrapper.add ul li[enabled="true"]');
	var errors = false;

	el.find("ul.tagfield").parent().removeClass('error');
	el.each(function() {
		var li = $(this);
		var select = li.find('select'), 
			input = li.find('input.new-option-name'), 
			textarea = li.find('textarea');

		if(select.val() == "" || select.val() == null) {
			select.parent().addClass('error');
			errors = true;
		}
		else {

			if(select.val() == -1 && input.val() == "") {
				input.parent().addClass('error');
				errors = true;
			}
		}

		//#option-entry-wrapper.add li.option-li:nth-child(2) ul.tagfield
		if($.isEmptyObject(li.find("ul.tagfield").tagit("tags"))) {
			li.find("ul.tagfield").parent().addClass('error');
			errors = true;
		}
	});

	//$('#option-entry-wrapper.add ul li[enabled="true"].option-li:nth-child(1)')

	if(errors) { return; }
	var option_name = el.find('select').map(function() {
								return $(this).val();
							}).get();
	var option_new = $('#option-entry-wrapper.add ul li.option-li input.new-option-name').map(function() {
								return $(this).val();
							}).get();

	var opvalues1 = $('#option-entry-wrapper.add ul li[enabled="true"].option-li:nth-child(1) ul.tagfield').tagit("tags");
	var opvalues2 = $('#option-entry-wrapper.add ul li[enabled="true"].option-li:nth-child(2) ul.tagfield').tagit("tags");
	var opvalues3 = $('#option-entry-wrapper.add ul li[enabled="true"].option-li:nth-child(3) ul.tagfield').tagit("tags");
	var values1 = {}, values2 = {}, values3 = {}, option_values = {};

	for(i in opvalues1) {
		values1[i] = opvalues1[i].value;
	}

	for(i in opvalues2) {
		values2[i] = opvalues2[i].value;
	}

	for(i in opvalues3) {
		values3[i] = opvalues3[i].value;
	}


	option_values = {0:values1,1:values2,2:values3};

	var data = {option_name: option_name, 
				option_new: option_new, 
				option_values: option_values,
				auto_create_variants:$('#auto-create-variants[checked]').val()
				};
	var mycallback = function() {
		$('#option-form-wrapper .headerpart-wrapper .option-label.rightpart').removeClass('hide');
		$('#option-entry-wrapper').removeClass("add").addClass("variants");
	};			

	$.extend(data, {product_id: get_product_id()});
	save_product_options(data,mycallback);
});

// Back button
$(document).on('click','#option-entry-wrapper .add-variants-wrapper a', function(e) {
	e.preventDefault(); e.stopImmediatePropagation();

	$('#option-form-wrapper .headerpart-wrapper .option-label.rightpart').removeClass('hide');
	$('#option-entry-wrapper').removeClass("add").addClass("variants");

	//var option_values = $(document).data('addproductform-option-values');
	var option_values = JSON.parse($('.addproduct-modal form').attr('option-values'));
	for(i in option_values) {
		$('ul.tagfield#option_values'+i+'-tagfield').tagit("reset");
		//console.log(option_values[i])
		for(x in option_values[i]) {
			$('ul.tagfield#option_values'+i+'-tagfield')
				.tagit("add", {label:option_values[i][x], value: option_values[i][x]});
			
		}		
	}
});

// Removing an option
$(document).on('click','#option-entry-wrapper.add ul li a.remove-btn', function(e) {
	e.preventDefault(); e.stopImmediatePropagation();
	var li = $(this).parent();
	li.find('select').val('');
	li.find('input.new-option-name').val('');

	var clone = li.clone();
	var index = $('#option-entry-wrapper.add ul li.option-li').index(li);
	var active_length = $('#option-entry-wrapper.add ul li[enabled].option-li').length
	index++;

	//$('ul.tagfield#option_values'+index+'-tagfield').tagit("reset");
	//return;
	if(active_length < 3) {
		//li.addClass('next-option').removeAttr('enabled','true');
	}
	else {
		li.remove();
		$('.list-option-entry-wrapper li.option-li').last().after(clone);
	}
});

// Manage Variants
$(document).on('click','#variants-entry-wrapper .col-options .add-option-btn', function(e) {
	e.preventDefault(); e.stopImmediatePropagation();
	$('#option-form-wrapper .headerpart-wrapper .option-label.rightpart').addClass('hide');

	$('#option-entry-wrapper').removeClass('variants').addClass("add");

	$('#option-entry-wrapper .add-variants-wrapper button').html('Update Options');
});

$(document).on('click','#variants-entry-wrapper .col-options .edit-option-btn', function(e) {
	e.preventDefault(); e.stopImmediatePropagation();
	$('#option-form-wrapper .headerpart-wrapper .option-label.rightpart').addClass('hide');

	$('#option-entry-wrapper').removeClass('variants').addClass("add");
	$('#option-entry-wrapper .add-variants-wrapper button').html('Update Options');

	//var option_values = $(document).data('addproductform-option-values');
	var option_values = JSON.parse($('.addproduct-modal form').attr('option-values'));
	var count_values, li_index;
	for(i in option_values) {
		//console.log(option_values[i])
		count_values = 0;
		for(x in option_values[i]) {
			count_values++;
			$('ul.tagfield#option_values'+i+'-tagfield')
				.tagit("add", {label:option_values[i][x], value: option_values[i][x]});
			
		}

		li_index = i--;
		if(count_values > 1) {
			$('#option-entry-wrapper.add ul li[enabled="true"].option-li').eq(i).find('.remove-btn').addClass('hide');
		} 
		else {
			$('#option-entry-wrapper.add ul li[enabled="true"].option-li').eq(i).find('.remove-btn').removeClass('hide');
		}
	}
});

// Add New Button
$(document).on('click','#addnew-variant-btn', function(e) {
	e.preventDefault(); e.stopImmediatePropagation();
	var inventory_type = $('input[name="inventory"][checked]').val();
	var max_allowed_variants = $('.addproduct-modal form').attr('max_variants_allowed');
	var addnew_col = $('#variants-entry-wrapper .col-variants tr.addnew').first();
	var clone = addnew_col.clone();
	
	clone.removeClass('hide');	
	if(inventory_type == "O") {
		clone.find('.input-qty input').removeAttr('disabled');
	}
	else {
		clone.find('.input-qty input').attr('disabled','disabled');
	}
	$('#variants-entry-wrapper .col-variants table tbody tr.addnew.hide').parent().append(clone);
	resize_containers();

	var existing_variants = $('#variants-entry-wrapper .col-variants tr.data').not('.addedit, .hide').length;
	var new_variants = $('#variants-entry-wrapper .col-variants tr.addnew').not('.edit, .hide').length;
	var total_varaints = (existing_variants + new_variants);
	if(max_allowed_variants == total_varaints) {
		//$(this).addClass('hide');
	}

	clone.find("input.numeric").numeric({ decimal: false, negative: false }, function() { this.value = ""; this.focus(); });
	clone.find("input.decimal-negative").numeric({ decimal: ".", negative: true }, function() { this.value = ""; this.focus(); });
	clone.find("input.decimal-negative").floatnumber('.',2);

	resize_containers();
});

$(document).on('change','#managevariant-options-select', function(e) {
	e.preventDefault(); e.stopImmediatePropagation();
	var val = $(this).val();
	$('#managevariant-values-select option').not(':first-child()').remove();
	if(val > 0) {
		//var option_values = $(document).data('addproductform-option-values')[val];
		var option_values = JSON.parse($('.addproduct-modal form').attr('option-values'))[val];

		for(i in option_values) {
			$('#managevariant-values-select').append('<option value="'+i+'">'+option_values[i]+'</option>');
		}
		$('#managevariant-values-select').removeClass('hide');
		$('.option-label.rightpart div.action-btn').removeClass('hide');
		$('.option-label.rightpart a').removeClass('hide');

		$('#variants-entry-wrapper .col-variants tr.data').not('.addnew,.addedit').addClass('selected');
	} 
	else {
		$('#managevariant-values-select').addClass('hide');
		$('.option-label.rightpart div.action-btn').addClass('hide');
		$('.option-label.rightpart a').addClass('hide');

		$('#variants-entry-wrapper .col-variants tr.data').not('.addnew,.addedit').removeClass('selected');
	}
});

$(document).on('change','#managevariant-values-select', function(e) {
	e.preventDefault(); e.stopImmediatePropagation();
	var val = $(this).val();

	if(val == "") {
		$('#variants-entry-wrapper .col-variants tr.data').not('.addnew,.addedit').addClass('selected');
	}
	else {
		$('#variants-entry-wrapper .col-variants tr.data').not('.addnew,.addedit').removeClass('selected');

		var text = $(this).find('option:selected').text();
		$('#variants-entry-wrapper .col-variants tr.data').not('.addnew,.addedit').each(function(){
			var values = $(this).find('td.op-value div').map(function() {
								return $(this).html();
							}).get();

			if($.inArray(text, values) != -1) {
				$(this).addClass('selected');
			}
		});
	}
});

// Bulk action
$(document).on('click','.option-label.rightpart div.action-btn .opts div', function(e) {
	e.preventDefault(); e.stopImmediatePropagation();
	var val = $(this).attr('value');

	switch(val) {
		case 'delete':
			product_option_delete_variants('bulk');
			break;
	}
});

$(document).on('click','.option-label.rightpart a', function(e) {
	e.preventDefault(); e.stopImmediatePropagation();
	$('#managevariant-options-select').val('');
	$('#managevariant-values-select').addClass('hide');
	$('.option-label.rightpart div.action-btn').addClass('hide');
	$('.option-label.rightpart a').addClass('hide');

	$('#variants-entry-wrapper .col-variants tr.data').not('.addnew,.addedit').removeClass('selected');
});

$(document).on('click','#variants-entry-wrapper .remove-variant-btn', function(e) {
	e.preventDefault(); e.stopImmediatePropagation();
	var row = $(this).parents('tr.addnew');
	var index = $('#variants-entry-wrapper .col-variants tr.addnew').index(row);
	var variant_id = row.attr('variant_id');
	variant_id = (typeof variant_id == "undefined") ? 0 : variant_id;

	if(row.hasClass('edit')) {
		row.prev().removeClass('hide');
		row.remove();
	}
	else {
		row.addClass('hide');
		$('#addnew-variant-btn').removeClass('hide');
	}

	var max_allowed_variants = $('.addproduct-modal form').attr('max_variants_allowed');
	var existing_variants = $('#variants-entry-wrapper .col-variants tr.data').not('.addedit, .hide').length;
	var new_variants = $('#variants-entry-wrapper .col-variants tr.addnew').not('.edit, .hide').length;
	var total_varaints = (existing_variants + new_variants);

	if(total_varaints < max_allowed_variants) {
		$(this).removeClass('hide');
	}

	resize_containers();
});

// Edit Button
$(document).on('click','.col-variants tr.data td .edit-delete-btn .edit-btn', function(e) {
	e.preventDefault(); e.stopImmediatePropagation();
	var inventory_type = $('input[name="inventory"][checked]').val();
	var col = $(this).parents('tr.data');
	var clone = $('.col-variants tr.addnew').eq(0).clone();
	col.addClass('hide');

	var values = col.find('td.op-value div').map(function() {
								return $(this).attr('value_id');
							}).get();
	
	for(var i = 0; i < values.length; i++) {
		clone.find('select').eq(i).val(values[i]);
	}

	clone.addClass('edit').removeClass('hide').attr('variant_id',col.attr('variant_id'));
	clone.find('.addedit-variant-lbl').html('Edit Variant');
	clone.find('button').html('Update');
	clone.find('input').val('');
	if(inventory_type == "O") {
		clone.find('.input-qty input').removeAttr('disabled');
	}
	else {
		clone.find('.input-qty input').attr('disabled','disabled');
	}
	col.after(clone);

	clone.find('.input-sku input').val(col.find('.input-sku input').val());
	clone.find('.input-price input').val(col.find('.input-price input').val());
	clone.find('.input-qty input').val(col.find('.input-qty input').val());

	resize_containers();
});

// Delete Button
$(document).on('click','.col-variants tr.data td .edit-delete-btn .delete-btn', function(e) {
	e.preventDefault(); e.stopImmediatePropagation();
	var variant_id = $(this).parents('tr.data').attr('variant_id');
	product_option_delete_variants('variant',{variant_id:variant_id});
	$(this).parents('tr.data').remove();

	resize_containers();
});

// Adding New
$(document).on('click','#variants-entry-wrapper .col-variants tr.addnew:not(.edit) button', function(e) {
	e.preventDefault(); e.stopImmediatePropagation();
	var inventory_type = $('input[name="inventory"][checked]').val();
	var col = $(this).parents('tr.addnew');
	var values = col.find('td.op-value select').map(function() {
								if($(this).val() == -1 && $.trim($(this).next().val()) == "") {
									return "";
								}

								return $(this).val();
							}).get();

	var inputs = col.find('td.op-value input').map(function() {
								return $(this).val();
							}).get();
	var sku = col.find('.input-sku input').val();
	var price = col.find('.input-price input').val();
	var qty = parseInt(col.find('.input-qty input').val());
	qty = (isNaN(qty)) ? 0 : qty;

	var check_values = values.join('');
	var variants = product_option_get_all_existing_variants();
	
	if($.inArray( check_values, variants ) != -1) {
		alert('Sorry, this variant is already exists.');
		return;
	}

	if(check_values == "" || $.inArray('',values) != -1) {
		return;
	}

	if($.trim(sku) == "") {
		col.find('.input-sku input').focus();
		alert("Please enter an SKU.");
		return;
	}

	if(inventory_type == "O" && qty == 0) {
		//col.find('.input-qty input').focus();
		//alert('Please enter an inventory quantity.');
		//return;
	}

	var data = {product_id:get_product_id(),
				variant_id: 0,
				value_ids: values,
				new_values: inputs,
				sku: sku,
				price: price,
				quantity:qty,	
				inventory_type: inventory_type
				}
	var mycallback = function(data) {
		/*
		var data = $.parseJSON(data);
		var clone = col.find('tr.data.addedit').clone();

		var op_value = clone.find('.op-value');
		var length = op_value.length;
		for(var i = 0; i < length; i++) {
			op_value.html('<div value_id="'+values[i]+'"></div>');
		}
		$('#variants-entry-wrapper .col-variants table tbody tr.addnew.hide').parent().append(clone);
		col.remove();
		*/
		reload_product_option();
	};
	product_option_save_variant(data, mycallback);			
	
});

// Update button
$(document).on('click','#variants-entry-wrapper .col-variants tr.addnew.edit button', function(e) {
	e.preventDefault(); e.stopImmediatePropagation();
	var inventory_type = $('input[name="inventory"][checked]').val();
	var col = $(this).parents('tr.addnew.edit');
	var variant_id = col.attr('variant_id');

	var values = col.find('td.op-value select').map(function() {
								if($(this).val() == -1 && $.trim($(this).next().val()) == "") {
									return "";
								}

								return $(this).val();
							}).get();

	var inputs = col.find('td.op-value input').map(function() {
								return $(this).val();
							}).get();

	var sku = col.find('.input-sku input').val();
	var price = col.find('.input-price input').val();
	var qty = parseInt(col.find('.input-qty input').val());
	qty = (isNaN(qty)) ? 0 : qty;

	var check_values = values.join('');
	var variants = product_option_get_all_existing_variants();
	
	if($.inArray( check_values, variants ) != -1) {
		alert('Sorry, this variant is already exists.');
		return;
	}

	if(check_values == "" || $.inArray('',values) != -1) {
		return;
	}

	if($.trim(sku) == "") {
		col.find('.input-sku input').focus();
		alert("Please enter an SKU.");
		return;
	}

	if(inventory_type == "O" && qty == 0) {
		//col.find('.input-qty input').focus();
		//alert('Please enter an inventory quantity.');
		//return;
	}

	var data = {product_id:get_product_id(),
				variant_id: variant_id,
				value_ids: values,
				new_values: inputs,
				sku: sku,
				price: price,
				quantity:qty,
				inventory_type: inventory_type	
				}


	mycallback = function() {
		col.prev().removeClass('hide');
		col.prev().find('.input-sku input').val(sku);
		col.prev().find('.input-price input').val(price);
		col.prev().find('.input-qty input').val(qty);
		col.remove();
	};
	product_option_save_variant(data, mycallback);
	
	resize_containers();
});

$(document).on('change','#variants-entry-wrapper .col-variants tr.addnew select', function(e) {
	e.preventDefault(); e.stopImmediatePropagation();

	if($(this).val() == -1) {
		$(this).addClass('hide');
		$(this).next().removeClass('hide');
	}
	else {
		$(this).removeClass('hide');
		$(this).next().addClass('hide');
	}
});

// Shipping
$(document).on('click','.shipping-lbl .shipping-meta-type', function(e) {
	e.preventDefault(); e.stopImmediatePropagation();

	$('.shipping-lbl .shipping-meta-type').removeClass('selected').find('input').removeAttr('checked');
	$(this).addClass('selected').find('input').attr('checked','checked');

	var type = $('input[name="shipping_meta_type"][checked]').val();
	var container = $('#global-shipping-settings');
	var shipping_data = $.parseJSON($(this).attr('data'));

	if(type == "C") {
		$('.shipto-info-wrapper').addClass('hide');
		$('#global-shipping-settings').removeClass('hide');
	
		//container.find('.input-wrapper input').attr('disabled','disabled').addClass('disabled');
		//container.find('.checkbox-wrapper').addClass('readonly').find('input').attr('readonly');
		container.find('tr.customized-col').removeClass('hide');
		$('.ap-content-shipping table td.col3').addClass('gray');
		$('.ap-content-shipping table td.col3 a').addClass('hide');
		$('.ap-content-shipping table tr.add-country').addClass('hide');
		/*
		container.find('tr.add-country').addClass('hide');
		container.find('.col3 a').hide();
		container.find('tr.customized-col').removeClass('hide');

		container.find('tr.data:not(.hide)').find('.col2 input[type="hidden"]').each(function() {
			var val = $(this).val();

			if(val != 2) {
				$(this).parents('tr')
					.find('.checkbox-wrapper').addClass('readonly')
					.find('input').attr('readonly','readonly');
			}
		});
		*/

		/*
		$('#customized-shipping-settings').remove();
		if($('#customized-shipping-settings').length == 0) 

			var clone = $('#global-shipping-settings').clone();
			clone.attr('id','customized-shipping-settings');
			clone.find('.input-wrapper input').attr('disabled','disabled').addClass('disabled');
			clone.find('tr.data.hide').remove();
			clone.find('tr.add-country').remove();
			clone.find('tr.customized-col').addClass('hide');
			clone.find('.col3 a').remove();
			clone.find('tr.customized-col').removeClass('hide');
			$('#global-shipping-settings').after(clone);
		}		
		$('#global-shipping-settings').hide();
		$('#customized-shipping-settings').show();
		*/

	}
	else {
		if(!$('.edit-global-btn').hasClass('clicked')){
			$('.shipto-info-wrapper').removeClass('hide');
			$('#global-shipping-settings').addClass('hide');
			return false;
		}
	
		container.find('tr.customized-col').addClass('hide');
		$('.ap-content-shipping table td.col3').removeClass('gray');
		$('.ap-content-shipping table td.col3 a').removeClass('hide');
		$('.ap-content-shipping table tr.add-country').removeClass('hide');
		//$('#global-shipping-settings').show();
		//$('#customized-shipping-settings').hide();
		/*
		container.find('tr.add-country').removeClass('hide');
		container.find('.col3 a').show();
		container.find('tr.customized-col').addClass('hide');

		container.find('tr.data:not(.hide)').find('.col2 input[type="hidden"]').each(function() {
			var val = $(this).val();

			if(val != 2) {
				$(this).parents('tr')
					.find('.checkbox-wrapper').removeClass('readonly')
					.find('input').removeAttr('readonly');	
			}
		});
		*/
		if(shipping_data != null) {
			$('.ap-content-shipping .additional-info-wrapper select').val(shipping_data.readytoship);
			$('.ap-content-shipping .additional-info-wrapper textarea').val(shipping_data.readytoshiptext);
		}
	}		
});

$(document).on('click', '.edit-global-btn', function(){
	$(this).addClass('clicked');
	$('.shipto-info-wrapper').addClass('hide');
	$('#global-shipping-settings').removeClass('hide');
	
	$('.ap-content-shipping table td.col3').removeClass('gray');
	$('.ap-content-shipping table td.col3 a').removeClass('hide');
	$('.ap-content-shipping table tr.add-country').removeClass('hide');
});

$(document).on('click','.ap-content-shipping table td.col3 label', function(e) {
	e.preventDefault(); e.stopImmediatePropagation();

	var checkbox = $(this).find('.checkbox-wrapper');
	var radio = $(this).find('.radio-wrapper');

	var input = (checkbox.length > 0) ? checkbox : radio;
	var type = (checkbox.length > 0) ? 'checkbox' : 'radio';

	if(input.parents('.shipping-option-wrapper').hasClass('disabled') 
	|| input.hasClass('readonly')) {
		input.find('input').removeAttr('checked');
		return;
	}

	if(type == 'checkbox') {
		if(input.hasClass('checked')) {
			input.removeClass('checked');
			input.find('input').removeAttr('checked');

			input.parents('.shipping-option-wrapper')
				.find('.input-wrapper')
				.addClass('disabled')
				.find('input')
				.attr('disabled','disabled');
		}
		else {
			input.addClass('checked');
			input.find('input').attr('checked','checked');

			input.parents('.shipping-option-wrapper')
				.find('.input-wrapper')
				.removeClass('disabled')
				.removeAttr('disabled')
				.find('input').removeAttr('disabled').focus();

		}
	}
	else {
		input.parents('.col3').find('.radio-wrapper').removeClass('checked')
			.find('input')
			.removeAttr('checked');

		input.addClass('checked');
		input.find('input').attr('checked','checked');
	}
});

$(document).on('click','.ap-content-shipping table td.col3 a', function(e) {
	e.preventDefault(); e.stopImmediatePropagation();

	$(this).parents('tr').remove();

	if($('#global-shipping-settings tr.data:not(.hide)').length == 0) {
		$('.ap-content-shipping tr.error-shipping').removeClass('hide');

		var country_code = $('.ap-content-shipping .shipsfrom input').attr('country_code');
		$('#addnewshipping-country').val(country_code);
	}
});

$(document).on('click','.addproduct-modal #add-new-shipping', function(e) {
	e.preventDefault(); e.stopImmediatePropagation();

	var country = $('.addproduct-modal #addnewshipping-country');
	var country_code = country.val();
	var country_name = country.find('option:selected').text();
	var shipping_rate = $('.addproduct-modal #addnewshipping-shippingrates');
	var sp_name = shipping_rate.find('option:selected').text();
	var sp_value = shipping_rate.val();
	var ships_from = $('.ap-content-shipping .shipsfrom input').attr('country_code'); 

	if(country_code == "") {
		country.parent().addClass('error').find('small').html("Please select a country.");
		return;
	}
	
	var countries = $('.ap-content-shipping #global-shipping-settings tr.data').map(function() {
					return $(this).attr('country_code');
				}).get();

	if($.inArray( country_code, countries ) != -1) {
		country.parent().addClass('error').find('small').html("Country already exists.");
		return;
	}
	country.parent().removeClass('error');

	var tr = $('.ap-content-shipping #global-shipping-settings tr.data.hide').clone();

	if(sp_value == 0) {
		sp_name = "FEDEX";
		tr.find('.col3 .ac-shipping-txtwrapper').removeClass('hide').find('.carrier-txt').html(sp_name);
	}	
	tr.removeClass('hide').attr('country_code',country_code);
	tr.find('.col1').html(country_name);
	tr.find('.col2').find('input.shipping-type-input').attr('name','shipping_global_meta['+country_code+'][shipping_type]').val(sp_value);
	tr.find('.col2').find('input.shipping-carrier-input').attr('name','shipping_global_meta['+country_code+'][carrier]').val('FEDEX');
	tr.find('em').html(sp_name);
	tr.find('.col3').find('.selections').eq(0).find('input').attr('name','shipping_global_meta['+country_code+'][options][standard]');
	tr.find('.col3').find('.selections').eq(1).find('input').attr('name','shipping_global_meta['+country_code+'][options][express]');
	tr.find('.col3').find('.selections').eq(2).find('input').attr('name','shipping_global_meta['+country_code+'][options][next_day]');
	tr.find('.col3').find('.input-wrapper input').eq(0).attr('name','shipping_global_meta['+country_code+'][rate][standard]').val('0.00');
	tr.find('.col3').find('.input-wrapper input').eq(1).attr('name','shipping_global_meta['+country_code+'][rate][express]').val('0.00');
	tr.find('.col3').find('.input-wrapper input').eq(2).attr('name','shipping_global_meta['+country_code+'][rate][next_day]').val('0.00');
	
	if(sp_value != 2) {
		tr.find('.col3')
			.find('.selections')
			.addClass('checkbox-wrapper')
			.find('input')
			.attr('type','checkbox');
	} else {
		tr.find('.col3')
			.find('.selections')
			.addClass('radio-wrapper')
			.find('input')
			.attr('type','radio');
	}

	tr.find('.selections input').removeAttr('disabled');
	$('.ap-content-shipping #global-shipping-settings tr.data').removeClass('noborder');

	if(sp_value != 1) {
		tr.find('.input-wrapper').addClass('hide');
	}

	if(ships_from == "US" && country_code == "US" && sp_value == 0) {
		//tr.find('.input-wrapper:last')
	}

	// Make Service Level default: standard
	var default_serviceLevel = tr.find('.col3').find('.shipping-option-wrapper').eq(0);
	default_serviceLevel.find('.selections').addClass('checked').find('input').attr('checked','checked');

	if(sp_value == 1) {
		default_serviceLevel.find('.input-wrapper').removeClass('disabled').find('input').removeAttr('disabled');
	}

	$('.ap-content-shipping tr.error-shipping').addClass('hide');
	$('.addproduct-modal #global-shipping-settings tr.data:last').after(tr);
	resize_containers();
});

$(document).on('click','.ap-content-shipping .select.shippingrates .opts div', function(e) {
	e.preventDefault(); e.stopImmediatePropagation();
	var shipping_meta_type = $('input[name="shipping_meta_type"][checked]').val();
	var val = $(this).attr('value');
	var carrier = $(this).attr('carrier');
	var text = $(this).html();
	var col2 = $(this).parents('tr').find('.col2');
	var col3 = $(this).parents('tr').find('.col3');

	if(val == "" || typeof val == "undefined") {
		if(!$(this).hasClass('auto')) {
			// Call Subscription Modal Here (to be able to upgrade a plan)

			col2.find('.select.shippingrates .outer').hide();
			setTimeout(function(){ col2.find('.select .outer').attr('style','') }, 400);

		}
		return;
	}

	$(this).addClass('selected');
	if(val == 0) {
		text = "Carrier Calculated";
		text = carrier;

		col3.find('.ac-shipping-txtwrapper').removeClass('hide');
		col3.find('.ac-shipping-txtwrapper .carrier-txt').html(text);
	}
	else {
		carrier = "";
		col3.find('.ac-shipping-txtwrapper').addClass('hide');
	}

	col2.find('.select.shippingrates em').html(text);
	col2.find('.select.shippingrates .outer').hide();
	col2.find('input.shipping-type-input').val(val);
	col2.find('input.shipping-carrier-input').val(carrier);
	setTimeout(function(){ col2.find('.select .outer').attr('style','') }, 400);

	if(val !=2) {
		col3.find('.selections')
			.addClass('checkbox-wrapper')
			.removeClass('radio-wrapper')
			.find('input')
			.attr('type','checkbox');
	}
	else {
		col3.find('.selections')
			.addClass('radio-wrapper')
			.removeClass('checkbox-wrapper')
			.find('input')
			.attr('type','radio');
	}
	/*
	if(col3.find('.selections.checked').length == 0 || col3.find('.selections.checked').length > 1) {
			col3.find('.selections')
				.find('input')
				.removeAttr('checked');
			col3.find('.selections').eq(0).addClass('checked')
				.find('input').attr('checked','checked');
	}
	*/

	if(shipping_meta_type == "G") {
		col3.find('.selections input').removeAttr('disabled');
	}
	else {
		/*
		if(val == 2) {
			col3.find('.selections').removeClass('readonly').find('input').removeAttr('readonly');
		} 
		else {
			col3.find('.selections').addClass('readonly').find('input').attr('readonly','readonly');
		}
		*/
	}

	

	// If from flat rate or auto calculated to free shipping have more than 1 checked button, 
	// make the Standard as the only free shipping
	if(val == 2 && col3.find('.selections.checked').length > 1) {
		var el = col3.find('.selections.checked').eq(0);

		col3.find('.selections').removeClass('checked')
			.find('input').removeAttr('checked');
		col3.find('.input-wrapper input').val('0.00').attr('readonly','readonly');
		el.addClass('checked')
			.find('input').attr('checked','checked');
	} 
	else {
		col3.find('.selections').each(function() {
			if(!$(this).hasClass('checked')) {
				$(this).find('input')//.attr('readonly','readonly')
					.parents('.shipping-option-wrapper')
					.find('.input-wrapper').addClass('disabled')
					.find('input').val('0.00').attr('disabled','disabled');
			}
			else {
				$(this).find('input').removeAttr('readonly')
					.parents('.shipping-option-wrapper')
					.find('.input-wrapper').removeClass('disabled')
					.find('input').removeAttr('disabled').removeAttr('readonly');
			}
		});
	}

	if(val != 1) {
		col3.find('.input-wrapper').addClass('hide');
	}
	else {
		col3.find('.input-wrapper').removeClass('hide');
	}
});

$(document).on('mouseover','.ap-content-shipping .select .outer', function(e) {
	$('.ap-content-shipping .select.shippingrates .opts div').removeClass('selected');
});

$(document).on('mouseleave','.ap-content-shipping .select', function(e) {
	var row = $(this).parents('tr');
	var val = row.find('.col2 input.shipping-type-input').val();
	var carrier = row.find('.col2 input.shipping-carrier-input').val();

	if(val != 0) {
		row.find('.col2 .select .opts div[value="'+val+'"]').addClass('selected');
	}
	else {
		row.find('.col2 .select .opts div[carrier="'+carrier+'"]').addClass('selected');
	}
});


$(document).on('click','.ap-content-shipping .select.shippingcarrier .opts div', function(e) {
	e.preventDefault(); e.stopImmediatePropagation();
	var val = $(this).attr('value');
	var text = $(this).html();
	var col2 = $(this).parents('tr').find('.col2');

	col2.find('.select.shippingcarrier em').html(text);
	col2.find('.select.shippingcarrier .outer').hide();
	col2.find('input.shipping-carrier-input').val(val);

	setTimeout(function(){ col2.find('.select.shippingcarrier .outer').attr('style','') }, 400);
});


$(document).on('click','.addproduct-modal .publish-btn', function(e) {
	e.preventDefault(); e.stopImmediatePropagation();
	var input = $('.addproduct-modal .publish-type-wrapper');

	$('.action-btn button').removeClass('selected');
	$(this).addClass('selected');
	//if(get_product_id() == 0) {
		input.find('input').removeAttr('checked');
		input.find('input[value="1"]').attr('checked','checked');
	//}

	//$('.addproduct-modal form').attr('submittype','1').submit();
	$('.addproduct-modal form').submit();
});

$(document).on('click','.addproduct-modal .draft-btn', function(e) {
	e.preventDefault(); e.stopImmediatePropagation();
	var input = $('.addproduct-modal .publish-type-wrapper');

	$('.action-btn button').removeClass('selected');
	$(this).addClass('selected');

	input.find('input').removeAttr('checked');
	input.find('input[value="0"]').attr('checked','checked');

	$('.addproduct-modal form').submit();
});

$(document).on('click','.addproduct-modal .publish-type-wrapper input', function(e) {
	//e.stopImmediatePropagation();
	if($(this).attr('checked')) {
		return;
	}

	var input = $('.addproduct-modal .publish-type-wrapper input[checked]');
	input.removeAttr('checked');
	$(this).attr('checked','checked');
});

$(document).on('submit', '.addproduct-modal form',function(e) {
	e.preventDefault(); e.stopImmediatePropagation();
	addprod_save();
});		


/* add product external */
$(document).on('submit', '#addUrlForm', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	
	var $form = $(this),
		url = $form.attr( 'action' );
		form_data = {};
	form_data.isAjax = true;
	form_data.url = $.trim($('.product-url').val());
	form_data.cid = $.trim($form.find('input[name="collection"]').val());
	$('.add-prod-web-modal .stick-loader').removeClass('hide');
	$('.error2').addClass('hide');
	$('.tmp-images').html('');
	$('.add-product-step1-content').html('');
	$('.add-product-step2-content').html('');
	SSAjax({
		data: form_data,
		url: url,
		success: function(response){
			var _data = $.parseJSON(response),
				c = 0,
				image_counter = 0;
			if(!_data.status){
				$('.add-prod-web-modal .error2').html(_data.msg);
				$('.error2').removeClass('hide');
				$('.add-prod-web-modal .stick-loader').addClass('hide');
				return false;
			}
			if(_data.url_exist){
				var page = root + 'stick_products?action=url_step2',
					form_data = {};
				
				form_data.prod_user_id = _data.prod_user_id;
				form_data.prod_store_id = _data.prod_store_id;
				form_data.collection_prod_user_id = _data.collection_prod_user_id;
				form_data.collection_prod_store_id = _data.collection_prod_store_id;
				form_data.owner_user_id = _data.user_id;
				form_data.store_id = _data.store_id;
				form_data.collection_id = _data.collection_id;
				form_data.product_id = _data.product_id;
				form_data.product_user_id = _data.product_user_id;
				form_data.current_url = _data.current_url;
				form_data.url_exist = 1;					
					
				form_data.product_site_name = '';
				form_data.external_product_url = '';
				form_data.external_product_canonical_url = '';
				form_data.external_tags = '';
				form_data.product_name = _data.product_name;
				form_data.price = _data.price;
				form_data.currency = _data.currency;
				form_data.currency_symbol = (_data.currency == 'EURO')? '&euro':'$';
				form_data.image = _data.image;
				form_data.product_for = _data.department_id;
				form_data.product_for_name = _data.department_name;
				form_data.color = '';
				
				form_data.isAjax = true;
				$('.add-prod-web-modal .stick-loader').removeClass('hide');
				$('.add-prod-web-modal .add-product-step2-wrapper').removeClass('hide');
				SSAjax({
					data: form_data,
					url: page,
					success: function(response){
						//$('.stick-product-modal .stick-wrapper').html(response);
						$('.add-prod-web-modal .add-product-step2-content').html(response);
						$('.add-prod-web-modal .stick-loader').addClass('hide');
					}
				});
			}
				
			if(! _data.url_exist){
				
				if(_data.image != ''){
					$('.tmp-images').append('<li style="background-image: url(\' ' + _data.image + ' \')" ><input type="hidden" name="product_image" value="' + _data.image + '" /></li>');
					image_counter++; c++;
				}
				total_img = _data.images.length - 1;
				$.each(_data.images, function(i,item){
					var newImg = new Image();
					newImg.src = item;
					var height = newImg.height,
						width = newImg.width;
						
					$(newImg).load(function(){
						if(parseInt(newImg.width) >= 250 || parseInt(newImg.height) >= 250){
							if(_data.image != item){
								$('.tmp-images').append('<li style="background-image: url(\' ' + item + ' \')" ><input type="hidden" name="product_image" value="' + item + '" /></li>');
							}
							image_counter++; c++;
							if(image_counter > 7){ total_img = 1; }
						}
						total_img--;
					});
				});
						
				function checkImg( ) {
					if(total_img <= 1){
						if(image_counter > 0){
							var imageList = $('.tmp-images').html();
							
							$('#addNewProdModal').foundation('reveal', 'close');
							var modal_id = '#product-editor-modal';
							$(modal_id + ' .mod-wrapper').html('');
							openCustomModal(modal_id);
							var form_data = {};
							form_data.isAjax = true;
							$(modal_id + ' .mod-loader-wrapper').show();
							SSAjax({
								data: form_data,
								url: "/admin/catalog_actions?action=add_external_product",
								success: function(response){
									$(modal_id + ' .mod-wrapper').html(response);
									$('.stick-prod-cid').val(_data.collection_id);
									$('.stick-prod-sid').val(_data.store_id);
									$('.stick-product-site_name').val(_data.site_name);
									$('.stick-product-url').val(_data.web_url);
									$('.stick-product-canonical-url').val(_data.canonical_url);
									$('.stick-product-tags').val(_data.meta_keywords);
									$('.stick-product-name').val(_data.product_name);
									$('.stick-product-price').val(_data.price);
									$('.choose-img-content ul').html(imageList);
									$(modal_id + ' .mod-loader-wrapper').hide();
								}
							});
						}
						if(image_counter == 0){
							$('.add-prod-web-modal .error2').html("Can't find any appropriate images from the product url provided.");
							$('.error2').removeClass('hide');
							$(modal_id + ' .mod-loader-wrapper').hide();
						}
						image_counter = 0;
					}else{
						setTimeout( checkImg, 30 );
					}
				}
				checkImg();
			}
		}
	});
	
	return false;
});

$(document).on('touchstart, click', '.choose-img-content ul li', function(){
	$('.choose-img-content ul li').removeClass('active');
	$(this).addClass('active');
	$(".image-link").val($(this).find('input').val());
});

$(document).on('touchstart, click', '.stick-add-btn', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	add_top_shopstyx($(this), '.add-search-modal');
});

$(document).on('touchstart, click', '.collection-box', function(){
	$('.collection-box').removeClass('active');
	$(this).addClass('active');
});

$(document).on('touchstart, click', '.back-to-step-1', function(){
	$('.add-product-step1-wrapper').addClass('hide');
	$('.add-product-step2-wrapper').addClass('hide');
	$('.bg-trans').addClass('hide');
});

$(document).on('close', '#addProdWebModal', function (e) {
	$('.add-product-step1-wrapper').addClass('hide');
	$('.add-product-step2-wrapper').addClass('hide');
	$('.bg-trans').addClass('hide');
});

$(document).on('touchstart, click', '.save-collection-btn', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	$('.no-collection').html('');
	$('.stick-product-description').removeClass('border-red');
	$('.collection-input-name').removeClass('border-red');
	
	if(!$('._add-collections').hasClass('hide')){
		var collect_name = $.trim($('.collection-input-name').val());
		var collection_desc = $.trim($('.collection-description').val());
		if(collect_name == ''){
			$('.collection-input-name').addClass('border-red');
			return false;
		}
		//$('.new-collection-btn').remove();
		$('.collection-box').removeClass('active');
		$('.collection-box-new').remove();
		$('.cw-a-content ul').append('<li class="collection-box collection-box-new new active"><div class="collection-txt">' + collect_name + '</div><input type="hidden" name="collection_id" value="new" /><input type="hidden" name="collection_name" value="' + collect_name + '" /><textarea class="hide" name="collection_description">' + collection_desc + '</textarea></li>');
		//$('.cw-a-content ul').append('<li class="collection-add-btn new-collection-btn"><span class="collection-plus"></span></li>');
	}
	var page = root + 'stick_products?action=save_collection',
		$this = $(this),
		form_data = {};
	form_data.prod_user_id = $.trim($('.stick-product-prod_user_id').val());
	form_data.prod_store_id = $.trim($('.stick-product-prod_store_id').val());
	form_data.collection_prod_user_id = $.trim($('.stick-product-collection_prod_user_id').val());
	form_data.collection_prod_store_id = $.trim($('.stick-product-collection_prod_store_id').val());
	form_data.owner_user_id = $.trim($('.stick-product-owner_user_id').val());
	form_data.store_id = $.trim($('.stick-product-store_id').val());
	form_data.post_collection_id = $.trim($('.stick-product-collection_id').val());
	form_data.product_id = $.trim($('.stick-product-product_id').val());
	form_data.product_user_id = $.trim($('.stick-product-product_user_id').val());
	form_data.current_url = $.trim($('.stick-product-current_url').val());
	form_data.product_site_name = $.trim($('.stick-product-site_name2').val());
	form_data.external_product_url = $.trim($('.stick-product-url2').val());
	form_data.external_product_canonical_url = $.trim($('.stick-product-canonical-url2').val());
	form_data.product_name = $.trim($('.stick-product-name2').val());
	form_data.price = $.trim($('.stick-product-price2').val());
	form_data.currency_symbol = $( ".stick-product-currency_symbol2" ).val();
	form_data.currency = $('.stick-product-currency2').val();
	form_data.image = $('.stick-product-image2').val();
	form_data.product_for = $('.stick-product-product-for2').val();
	form_data.color = $('.stick-product-color2').val();
	form_data.external_tags = $.trim($('.stick-product-external-tags2').val());
	form_data.product_keyword = $.trim($('.stick-product-keyword2').val());
	form_data.product_description = $.trim($('.stick-product-description').val());
	form_data.collection_name_selected = $.trim($('.cw-a-content ul li.active input[name="collection_name"]').val());
	form_data.collection_id_selected = $.trim($('.cw-a-content ul li.active input[name="collection_id"]').val());
	form_data.added_first = $('.stick-product-exist2').val();
	form_data.isAjax = true;
	
	var collection_id = [],  
		collection_name = [],
		collection_description = [];
	
	$('.cw-a-content ul li.new input[name="collection_id"]').each(function(){
		collection_id.push($.trim($(this).val())); 
	});
	$('.cw-a-content ul li.new input[name="collection_name"]').each(function(){
		collection_name.push($.trim($(this).val()));
	});
	$('.cw-a-content ul li.new textarea[name="collection_description"]').each(function(){
		collection_description.push($.trim($(this).val()));
	});
	form_data.collection_id = collection_id;
	form_data.collection_name = collection_name;
	form_data.collection_description = collection_description;
	
	var error = false;
	if($('.stick-product-description').hasClass('product-desc-required')){
		if(form_data.product_description == ""){
			$('.stick-product-description').addClass('border-red');
			error = true;
		}
	}
	
	if ($('.cw-a-content ul li.active input[name="collection_id"]').length === 0) {
		$('.no-collection').html('Please add or select collection.');
		error = true;
	}
	
	if(error){
		return false;
	}
	$('.stick-product-modal .stick-loader .shopstyxloader div').html('SAVING...');
	$('.stick-product-modal3 .stick-loader .shopstyxloader div').html('SAVING...');
	$('.stick-product-modal .stick-loader').removeClass('hide');
	$('.stick-product-modal3 .stick-loader').removeClass('hide');
	$('.bg-trans').removeClass('hide');
	$('.add-search-modal .stick-loader').removeClass('hide');
	SSAjax({
		data: form_data,
		url: page,
		success: function(response){
			var _data = $.parseJSON(response);
			if($this.hasClass('sengine-save-collection-btn')){
				$('.stick-product-modal3 .stick-loader').addClass('hide');
				$('.stick-product-modal3 .stick-loader .shopstyxloader div').html('LOADING...');
				$('.stick-product-modal3 .search-modals').addClass('hide');
				$('.stick-product-modal3 .add-search-step2').addClass('hide');
				$('.bg-trans').addClass('hide');
				$('.add-search-modal .stick-loader').addClass('hide');
			}else{
				$('.bg-trans').addClass('hide');
				$('.add-search-modal .stick-loader').addClass('hide');
				$('.stick-product-modal .stick-loader').addClass('hide');
				$('.stick-product-modal .stick-loader .shopstyxloader div').html('LOADING...');
			}
			reloadPage({link:'/admin/catalog_actions?action=reload_collections', clear_wrapper: '.m-catalog-list .catagory-list', wrapper: '.catagory-list', preloader: '.mcatalogloader'});
			$('#stickModal').foundation('reveal', 'close');
		}
	});
});


$(document).on('touchstart, click', '.basic-css-btn', function(e){
	$(this).css('backgroundPosition', '-143px -294px');
});

$(document).on('touchstart, click', '.advanced-css-btn', function(e){
	$(this).css('backgroundPosition', '-244px -294px');
});

function add_top_shopstyx($this, $container){
	//var $this = $(this),
	var	error = 0;
	
	$('.stick-product-name').removeClass('error');
	$('.stick-product-price').removeClass('error');
	$('.image-lbl').removeClass('data-required ');
	$('.color-lbl').removeClass('data-required ');
	$('.product-for').removeClass('data-required ');
	if($('.stick-product-name').val() == ''){
		$('.stick-product-name').addClass('error');
		error++;
	}
	
	var price = (/^[0-9,]{1,}(\.)?([0-9]{1,})?$/);
	if (!price.test($.trim($('.stick-product-price').val()))) {
		$('.stick-product-price').addClass('error');
		error++;
	}
	
	// if($.trim($('.stick-product-price').val()) == ""){
		// $('.stick-product-price').addClass('error');
		// error++;
	// }
	
	var page = root + 'stick_products?action=url_step2',
		form_data = {};
	form_data.product_site_name = $('.stick-product-site_name').val();
	form_data.external_product_url = $('.stick-product-url').val();
	form_data.external_product_canonical_url = $('.stick-product-canonical-url').val();
	form_data.external_tags = $('.stick-product-tags').val();
	form_data.url_exist = $('.stick-product-url-exist').val();
	form_data.product_name = $('.stick-product-name').val();
	form_data.price = $('.stick-product-price').val();
	form_data.currency_symbol = $( ".currency-symbol option:selected" ).text();
	form_data.currency = $('.currency-symbol').val();
	form_data.image = $('.choose-img-content ul li.active input').val();
	form_data.product_for = ( $.trim($('.stick-for li.active').attr('data')) != "")? $('.stick-for li.active').attr('data') : $('.stick-for li.selected').attr('data');
	form_data.product_for_name = ( $.trim($('.stick-for li.active').html()) != "")? $('.stick-for li.active').html() : $('.stick-for li.selected').html();
	form_data.color = $('.add-color span.selected').attr('data-color');
	form_data.external_tags = $('.stick-product-tags').val();
	form_data.product_keyword = $('.get-search-text').val();
	form_data.isAjax = true;
	
	
	if( form_data.image == null ){
		$('.image-lbl').addClass('data-required ');
		error++;
	}
	
	if( form_data.color == null ){
		$('.color-lbl').addClass('data-required ');
		error++;
	}
	
	if( form_data.product_for == null || form_data.product_for == "" ){
		$('.product-for').addClass('data-required ');
		error++;
	}
	if(error){ return false; }
	
	$('.stick-product-modal .stick-loader').removeClass('hide');
	$('.stick-product-modal3 .stick-loader').removeClass('hide');
	$('.bg-trans').removeClass('hide');
	$('.add-search-modal .stick-loader').removeClass('hide');
	SSAjax({
		data: form_data,
		url: page,
		success: function(response){
			if($this.hasClass('sengine-stick-add-btn')){
				$('.tmp-images').html('');
				$('.stick-product-modal3 .add-search-step2').removeClass('hide');
				$('.stick-product-modal3 .add-search-step2-content').html(response);
				$('.stick-product-modal3 .stick-loader').addClass('hide');
				$('.add-search-step2 .stick-a-product').remove();
				$('.add-search-step2 .arrow-down').remove();
				$('.save-to-btn').removeClass('save-collection-btn');
				$('.save-to-btn').addClass('sengine-save-collection-btn');
				$('.add-search-modal .stick-loader').addClass('hide');
			}else{
				$( $container + ' .add-product-step2-content').html(response);
				$('.add-product-step1-wrapper').addClass('hide');
				$('.add-product-step2-wrapper').removeClass('hide');
				$('.stick-product-modal .stick-loader').addClass('hide');
				$('.add-search-modal .stick-loader').addClass('hide');
			}
		}
	});
}

function isParseJSON (jsonString){
    try {
        var o = JSON.parse(jsonString);

        // Handle non-exception-throwing cases:
        // Neither JSON.parse(false) or JSON.parse(1234) throw errors, hence the type-checking,
        // but... JSON.parse(null) returns 'null', and typeof null === "object", 
        // so we must check for that, too.
        if (o && typeof o === "object" && o !== null) {
            return o;
        }
    }
    catch (e) { }

    return false;
};

function addproduct_form_type(type){
	var data = $.parseJSON($('.addproduct-modal form').attr('postdata'));
	if(type == "advanced") {
		store.set('addproduct_modal','advanced');
		//$('.addproduct-modal').removeClass('basic').addClass('advanced');
		//var wH = $(window).height();
		//$('.addproduct-modal').css('height',wH);
		//$('#addproductModal').css('left',0);	

		$.extend(data,{formType:"advanced",loadType:"slideRight"});
		//addproduct({data:data});
		addproduct({
			data:data,
			onSaved: function(data) {
				if(data.product_id){
					if($('.add-newproduct-wrapper').length > 0){
						var data_zindex = parseInt($('.coll-prod-list li.admin-prod-list:first-child').next().attr('data-z-index')) + 1;
						SSAjax({
							url: '/admin/catalog_actions?action=load_single_product',
							data: {isAjax:true,product_id:data.product_id,zindex:data_zindex},
							success: function(result){
								var list = '<li data-prod-id="' + data.product_id + '" data-z-index="' + data_zindex + '" class="left small-4 large-3 admin-prod-list pzindex' + data.product_id + '">'+ result +'</li>';
								$('.coll-prod-list li.admin-prod-list:first-child').after(list);
								_assign_to_collection('{"cid":"' + data.collection_id + '","puid":"' + data.user_id + '","pid":"' + data.product_id + '","sid":"' + data.store_id + '"}');
								return;
							}
						});
					}else{
						$('body').css('overflow','hidden');
						var data_zindex = parseInt($('.coll-prod-list li.admin-prod-list:first-child').attr('data-z-index')) + 1;
						SSAjax({
							url: '/admin/catalog_actions?action=load_single_product',
							data: {isAjax:true,product_id:data.product_id,zindex:data_zindex,addfrom_modal:true},
							success: function(result){
								var list = '<li data-prod-id="' + data.product_id + '" data-z-index="' + data_zindex + '" class="left small-4 large-4 admin-prod-list pzindex' + data.product_id + '">'+ result +'</li>';
								$('.coll-prod-list li.admin-prod-list:first-child').before(list);
								reloadPage({link:'/admin/catalog_actions?action=reload_collections', clear_wrapper: '.m-catalog-list .catagory-list', wrapper: '.catagory-list', preloader: '.mcatalogloader'});
								$('body').css('overflow','hidden');
								_assign_to_collection('{"cid":"' + data.collection_id + '","puid":"' + data.user_id + '","pid":"' + data.product_id + '","sid":"' + data.store_id + '"}');
								return;
							}
						});
					}
				}
			}
		});
	}
	else {
		store.set('addproduct_modal','basic');
		$('#addproductModal-basic').removeClass('advanced').addClass('basic');
		$('#addproductModal-basic').css('height','');
		$('#addproductModal').hide('slide', {direction: 'right'}, 400);
		//$('.addproduct-modal').centerized();
		$.extend(data,{formType:"basic",loadType:"slideRight"});
		addproduct({data:data});
	}
	$(document).scrollTop(0);
	//$('#wrapper').css('height',$('.addproduct-modal').height()+2);	
}

function addprod_modal_close() {
	$('.addproduct-modal .mainLoader').removeClass('saving').find('div').html('LOADING...');
	if($.trim($('#addproductModal-basic #modal-content').html()) != "") {
		$('#addproductModal-basic').hide('slide', {direction: 'right'}, 200, function() {
			$(this).find('#modal-content').html('');
		});
	}
	else {
		//$('#addproductModal').foundation('reveal', 'close');
		$('#addproductModal').hide('slide', {direction: 'right'}, 400);
	}
	$('#wrapper').show().css({height:'',display:''});
	$('body').css({'overflow':''});
}
function addprod_toggle_loader(text, modal_close) {


	var container = ($.trim($('#addproductModal #modal-content').html()) == "") ? "#addproductModal-basic" : "#addproductModal";
	var loader = $(container + ' .shopstyxloader.mainLoader').addClass('saving');
	var overlay = $(container + '.addproduct-modal .mainLoader.overlay');
	var div = loader.find('div');

	loader.centerized({main:$(container)});

	if(typeof text != "undefined") {
		div.html(text);
	}
	if(loader.hasClass('hide')) {
		$(container + ' .mainLoader.overlay').removeClass('hide');
		loader.removeClass('hide');
	}
	else {
		overlay.animate({'opacity':0}, 3500, function() {
								overlay.addClass('hide');
								loader.addClass('hide').removeClass('saving');
								loader.attr('style','');
								overlay.attr('style','').addClass('hide');

								if(typeof modal_close == "boolean" && modal_close == true) {
									addprod_modal_close();
								}
						});
	}
}

function addprod_save() {
	var product_id = get_product_id();
	var type = ($('.addproduct-modal.basic form').length || $('#addproductModal-basic form').length) ? "basic" : "advanced";
	var form = $('.addproduct-modal form'), collection_id, department_id;
	if(type == "basic") {
		collection_id = form.find('input[name="collection_id"]').val();
		department_id = form.find('select[name="department_id"]').val();
	} else {
		collection_id = form.find('input[name="collection_id"]').val();
		department_id = form.find('input[name="department_id"]').val();
	}

	var fields = {product_name: $.trim(form.find('input[name="product_name"]').val()),
					product_desc: $.trim(form.find('textarea[name="product_desc"]').val()),
					base_price: $.trim(form.find('input[name="base_price"]').val()),
					//sale_price: $.trim(form.find('input[name="sale_price"]').val()),
					color: $.trim(form.find('input[name="color"]').val()),
					collection_id: collection_id,
					department_id: department_id,
					payout_rate: $.trim(form.find('input[name="payout_rate"]').val()),
					payout_type: form.find('input[name="payout_type"][checked]').val(),
					//categories: addprod_selected_categories(),
					publish_type: form.find('input[name="publish_type"][checked]').val(),
					save_status: $.trim(form.find('input[name="save_status"]').val())
				}
    var fields_length = fields.length;
    var errors = {};

    if(product_id == 0 && form.attr('total_product') == form.attr('max_products')) {
    	alert('You have reached the maximum allowed number of products that can be added.');
    	return false;
    }

	for(var f in fields) {
		if(fields.hasOwnProperty(f)) {
			value = typeof(fields[f]) == 'undefined' ? '' : fields[f];
			
			if(value == "" || value == null) {
				if(f == "collection_id" || f == "department_id") {
					if(form.find('[name="'+f+'"]').parent().hasClass('common-field-wrapper')) {
						form.find('[name="'+f+'"]').parent().addClass('error');
					}
					else {
						form.find('[name="'+f+'"]').parent().parent().addClass('error');
					}
					errors.categories = true;
				}
				else {
					if(f != "categories") {
						form.find('[name="'+f+'"]').parent().addClass('error');
						errors.details = true;
					}
				}
			}
			else {
				if(f == "collection_id" || f == "department_id") {
					if(form.find('[name="'+f+'"]').parent().hasClass('common-field-wrapper')) {
						form.find('[name="'+f+'"]').parent().removeClass('error');
					}
					else { 
						form.find('[name="'+f+'"]').parent().parent().removeClass('error');
					}
				}
				else {
					if(f != "categories") {
						form.find('[name="'+f+'"]').parent().removeClass('error');
					}
				}
			}

			input_val = parseFloat(value);

			if((f == "base_price" || f == "payout_rate") && (value == 0 || value == 0.00 || isNaN(input_val))) {
				form.find('[name="'+f+'"]').parent().addClass('error');
				errors.details = true;
			}

			if(f == "categories") {
				if(value.length == 0) {
					$('#cat-error-display').addClass('error');
					errors.categories = true;
				}
				else {
					fields.categories = eval(JSON.stringify(addprod_selected_categories()));
				}
			}
		}
	}

	var $base_price = Math.round(fields.base_price * 100) / 100;
	var $sale_price = Math.round(form.find('input[name="sale_price"]').val() * 100) / 100;

	if(((fields.base_price != "" && fields.base_price != 0.00 && fields.base_price != 0) && ($sale_price >= $base_price || isNaN($sale_price))) || isNaN($sale_price)) {
		form.find('[name="sale_price"]').parent().addClass('error');
		errors.details = true;
	}
	else {
		form.find('[name="sale_price"]').parent().removeClass('error');
	}

	fields.sale_price = form.find('input[name="sale_price"]').val();
	
	var $payout_rate = Math.round(form.find('input[name="payout_rate"]').val() * 100) / 100;

	if(fields.base_price != "" && $payout_rate >= $base_price || ((isNaN($payout_rate) || $payout_rate == 0 || $payout_rate == 0.00))) {
		form.find('[name="payout_rate"]').parent().addClass('error');
		errors.details = true;
	}
	else {
		if(form.find('input[name="payout_rate"]').val() == 0 || form.find('input[name="payout_rate"]').val() == 0.00) {

		}
		else {
			form.find('[name="payout_rate"]').parent().removeClass('error');
		}

	}
	
	if($('.image-wrapper li').not('.uploadbtn').not('.toBeUploaded').length == 0 && 
		(typeof $('.addproduct-modal form').data('image_files') == "undefined" || $('.addproduct-modal form').data('image_files').length == 0)
		) {
		$('#image-error-display').addClass('error');
		errors.media = true;
	}
	else {
		$('#image-error-display').removeClass('error');
	}

	if(type == "basic") {
		//var basic_form = $('.addproduct-modal.basic form');
		//var data = basic_form.serialize();
		fields.sale_price = form.find('input[name="sale_price"]').val();
		fields.weight = $.trim(form.find('input[name="weight"]').val());
		fields.weight_id = $.trim(form.find('select[name="weight_id"]').val());
		fields.dimension_length = $.trim(form.find('input[name="dimension_length"]').val());
		fields.dimension_width = $.trim(form.find('input[name="dimension_width"]').val());
		fields.dimension_height = $.trim(form.find('input[name="dimension_height"]').val());
		fields.length_id = $.trim(form.find('select[name="length_id"]').val());

		if(fields.weight == "" || fields.weight == 0  || isNaN(parseFloat(fields.weight))) {
			form.find('[name="weight"]').parent().addClass('error');
			errors.details = true;
		}
		else {
			form.find('[name="weight"]').parent().removeClass('error');
		}

		if(fields.weight_id == "" || typeof fields.weight_id == "undefined") {
			errors.details = true;
		}

		if(fields.dimension_length == "" || fields.dimension_width == "" || fields.dimension_height == "" ||
		fields.dimension_length <= 0 || fields.dimension_width <= 0 || fields.dimension_height <= 0 ||
		isNaN(parseFloat(fields.dimension_length)) || isNaN(parseFloat(fields.dimension_width)) || isNaN(parseFloat(fields.dimension_height)) ) {
			
			form.find('[name="dimension_length"]').parent().addClass('error');
			errors.details = true;
		}
		else {
			form.find('[name="dimension_length"]').parent().removeClass('error');
		}
	}
	else {
		advanced_fields = { sku:$.trim(form.find('input[name="sku"]').val()),
							is_taxable:$.trim(form.find('input[name="is_taxable"][checked]').val()),
							weight:$.trim(form.find('input[name="weight"]').val()),
							weight_id:$.trim(form.find('select[name="weight_id"]').val()),
							dimension_length:$.trim(form.find('input[name="dimension_length"]').val()),
							dimension_width:$.trim(form.find('input[name="dimension_width"]').val()),
							dimension_height:$.trim(form.find('input[name="dimension_height"]').val()),
							length_id:$.trim(form.find('select[name="length_id"]').val()),
							video_url:$.trim(form.find('input[name="video_url"]').val()),
							inventory:$.trim(form.find('select[name="inventory"]').val()),
							allow_customer_purchase:form.find('input[name="allow_customer_purchase"][checked]').val(),
							quantity:form.find('input[name="quantity"]').val(),
							readytoship:form.find('select[name="readytoship"]').val(),
							readytoshiptext:form.find('textarea[name="readytoshiptext"]').val(),
							shipping_meta_type:form.find('input[name="shipping_meta_type"][checked]').val(),
							seo_title:$.trim(form.find('input[name="seo_title"]').val()),
							seo_url:$.trim(form.find('input[name="seo_url"]').val()),
							seo_metadata_desc:$.trim(form.find('textarea[name="seo_metadata[description]"]').val())
							};
		$.extend(fields,advanced_fields);


		if(advanced_fields.sku == "") {
			form.find('[name="sku"]').parent().addClass('error');
			errors.details = true;
		}
		else {
			form.find('[name="sku"]').parent().removeClass('error');
		}

		if(advanced_fields.weight == "" || advanced_fields.weight == 0 || isNaN(parseFloat(advanced_fields.weight))) {
			form.find('[name="weight"]').parent().addClass('error');
			errors.details = true;
		}
		else {
			form.find('[name="weight"]').parent().removeClass('error');
		}

		if(advanced_fields.weight_id == "" || typeof advanced_fields.weight_id == "undefined") {
			//form.find('[name="weight_id"]').parent().addClass('error');
			errors.details = true;
		}
		else {
			//form.find('[name="weight_id"]').parent().removeClass('error');
		}

		if(advanced_fields.dimension_length == "" || advanced_fields.dimension_width == "" || advanced_fields.dimension_height == "" ||
		advanced_fields.dimension_length <= 0 || advanced_fields.dimension_width <= 0 || advanced_fields.dimension_height <= 0 || 
		isNaN(parseFloat(advanced_fields.dimension_length)) || isNaN(parseFloat(advanced_fields.dimension_width)) || isNaN(parseFloat(advanced_fields.dimension_height))) {
			
			form.find('[name="dimension_length"]').parent().addClass('error');
			errors.details = true;
		}
		else {
			form.find('[name="dimension_length"]').parent().removeClass('error');
		}

		if(advanced_fields == "Q") {

		}

		if(advanced_fields.seo_title == "") {
			form.find('[name="seo_title"]').parent().addClass('error');
			errors.seo = true;
		}

		if(advanced_fields.seo_url == "") {
			form.find('[name="seo_url"]').parent().addClass('error');
			errors.seo = true;
		}

		$('.ap-content-shipping table tr.add-country .error-shipping').addClass('hide');
		var shipping_data = shipping_form_params();
		if($.isEmptyObject(shipping_data)) {
			$('.ap-content-shipping tr.error-shipping').removeClass('hide');
			errors.shipping = true;
		}
		else {
			$('.ap-content-shipping tr.error-shipping').addClass('hide');
		}

		$.extend(fields,shipping_data);

	}
	//console.log(fields);
	//console.log(JSON.stringify(errors));

	if(!$.isEmptyObject(errors)) { 

		if(type == "advanced") {
			$('#ap-left-menu li').removeClass('selected');
			$('.reveal-modal.advanced .content .ap-content').hide();

			if(errors.hasOwnProperty('details')) {
				$('#ap-left-menu li:nth-child(1)').addClass('selected');
				$('.ap-content-details').show();
			}
			else if(errors.hasOwnProperty('media')) {
				$('#ap-left-menu li:nth-child(2)').addClass('selected');
				$('.ap-content-media').show();
			}
			else if(errors.hasOwnProperty('categories')) {
				$('#ap-left-menu li:nth-child(3)').addClass('selected');
				$('.ap-content-categories').show();
			}
			else if(errors.hasOwnProperty('options')) {
				$('#ap-left-menu li:nth-child(4)').addClass('selected');
				$('.ap-content-options').show();
			}
			else if(errors.hasOwnProperty('shipping')) {
				$('#ap-left-menu li:nth-child(5)').addClass('selected');
				$('.ap-content-shipping').show();
			}
			else if(errors.hasOwnProperty('seo')) {
				$('#ap-left-menu li:nth-child(6)').addClass('selected');
				$('.ap-content-seo').show();
			}
		}

		return false; 
	}

	fields.product_id = product_id;
	addprod_toggle_loader('Saving product...');

	/*
	$('#ap-left-menu li').removeClass('selected');
	$('.reveal-modal.advanced .content .ap-content').hide();
	$('#ap-left-menu li:nth-child(1)').addClass('selected');
	$('.ap-content-details').show();
	*/

	SSAjax({
		url: "/admin/addproduct?action=save_product&type="+type,
		data: fields,
		success: function(response){
			if(!isParseJSON(response)) {
				//alert("");
				addprod_toggle_loader();
				return;
			}
			var data = $.parseJSON(response);	
			var callback = form.data('callback');
			
			if(data.hasOwnProperty('error')) {
				if(data.error_type == "no_shipto_country") {
					$('#ap-left-menu li').removeClass('selected');
					$('.reveal-modal.advanced .content .ap-content').hide();

					$('.ap-content-shipping table tr.add-country .error-shipping').removeClass('hide');
					$('#ap-left-menu li:nth-child(5)').addClass('selected');
					$('.ap-content-shipping').show()
				}
				addprod_toggle_loader(data.error);
			} else {
				var product_id = data.product_id;
				var container = (type == "basic") ? "#addproductModal-basic" : "#addproductModal";

				if(typeof $(container + ' form').data('image_files') != "undefined" && $(container + ' form').data('image_files').length > 0) {
					upload_product_photos(product_id, data, callback);
				}
				else {
					form.attr('product_id',product_id);
					//console.log(callback)
					if(typeof callback == "function") {
						var callbacks = $.Callbacks();
						callbacks.add( callback );
						callbacks.fire(data);
					}
					/*
					if(typeof CS.addproductOnSave == "function") {
						var callbacks = $.Callbacks();
						callbacks.add( CS.addproductOnSave );
						callbacks.fire(data);
					}
					*/

					if(fields.product_id == 0) {
						addprod_toggle_loader('You have added a new product.',true);
					}
					else {
						addprod_toggle_loader('Your product has been updated.',true);
					}
				}
			}	
		}
	});
}

function load_addedit_collection_form(cid) {
	var el = $('.addproduct-modal').find('.collection-entry-wrapper'),
		input = el.find('input'),
		textarea = el.find('textarea');
	var label = "Edit Collection";

	input.val('');
	textarea.val('');

	if(cid == 0) {
		el.find('.create-form').attr('cid',0);
		el.find('.labeltext').html("Create New Collection");
		el.show();
		return;
	}

	el.find('.savestatus').html('loading...');
	el.addClass('saving').show();

	SSAjax({
		url: "/admin/addproduct?action=addedit_collection_form",
		data: {collection_id:cid},
		success: function(data){
			var result = $.parseJSON(data);
			if(!$.isEmptyObject(result)) {
				el.find('.create-form').attr('cid',result.collection_id);
				input.val(result.collection_name);
				textarea.val(result.collection_desc);
			}
			else {
				el.find('.create-form').attr('cid',0);
				label = "Create New Collection";
			}

			el.find('.labeltext').html(label);
			el.removeClass('saving');
		}
	});
}

function save_collection() {
	var el = $('.addproduct-modal').find('.collection-entry-wrapper'),
		collection_id = el.find('.create-form').attr('cid'),
		input = el.find('input'),
		textarea = el.find('textarea'),
		collection_name = $.trim(input.val()),
		collection_desc = $.trim(textarea.val()),
		html;

	if(collection_name == "") {
		input.parent().addClass('error');
		return;
	}
	else {
		input.parent().removeClass('error');
	}

	el.find('.savestatus').html('saving collection...');
	el.addClass('saving');


	SSAjax({
		url: "/admin/addproduct?action=save_collection",
		data: {collection_id:collection_id,collection_name:collection_name,collection_desc:collection_desc},
		success: function(data){
			var result = $.parseJSON(data);
			if(!$.isEmptyObject(result)) {

			}
			else {

			}

			var cid = result.collection_id;
			if(collection_id == 0) {
				
				if(el.hasClass('advanced')) {
					html = '<li collection_id="'+cid+'"><div class="left small-12 large-12 coll-container"><span class="checked"></span><div class="left small-12 large-12 coll-image"></div><div class="left small-12 large-12 tbl-title"><div class="tbl-cell">'+collection_name+'</div></div></div></li>';
					$('.ap-content-categories .collectionlist-wrapper').append(html);
					
				}
				else {
					html = '<li value="'+cid+'" class="word_wrap">'+collection_name+' <div class="edit-btn"><div></div></div></li>';
					$('.addproduct-modal .mystyled-select.selectstyle1 .selection-list ul').append(html);
				}	
			}
			else {
				if(!el.hasClass('advanced')) {

					$('.addproduct-modal .mystyled-select.selectstyle1 .selection-list ul li[value="'+cid+'"]').html(collection_name + ' <div class="edit-btn"><div></div></div>');
				}
			}

			el.removeClass('saving').hide();
		}
	});
}

function upload_product_photos(product_id, data, callback) {
	var files = $('.addproduct-modal form').data('image_files');
	if(files.length == 0) {
		if(get_product_id() == 0) {
			addprod_toggle_loader('You have added a new product.',true);
		}
		else {
			addprod_toggle_loader('Your product has been updated.',true);
		}

		form.attr('product_id',product_id);

		return false;
	}

	//console.log(files[0]);
	$.ajax({
		type: 'POST',
		dataType: 'json',
		cache: false,
		data: files[0],
		processData: false,
		contentType: "application/octet-stream", 
		headers: {
			"X-File-Name": encodeURIComponent(files[0].name),
			"X-Mime-Type" : files[0].type
		},
		url: "/admin/addproduct?action=upload&type=product_photo&filename="+files[0].name+"&product_id="+product_id+"&token="+store.get('token')+"&user_id="+store.get('user_id'),
		success: function(response){
			//var results = $.parseJSON(response);
			$('.addproduct-modal form').data('image_files').shift();
			if($('.addproduct-modal form').data('image_files').length == 0) {
				if(typeof callback == "function") {
					var callbacks = $.Callbacks();
					callbacks.add( callback );
					callbacks.fire(data);
				}

				if(product_id == 0) {
					addprod_toggle_loader('You have added a new product.',true);
				}
				else {
					addprod_toggle_loader('Your product has been updated.',true);
				}
			}
			else {
				upload_product_photos(product_id);
			}
		}
	});

}

function set_default_product_photo(dataObj) {
	var form_data = {product_id:dataObj.product_id, sort_order: dataObj.sort_order};
	SSAjax({
		url: "/admin/addproduct?action=set_default_photo",
		data: form_data,
		success: function(data){}
	});
}

function delete_product_photo(dataObj) {
	var form_data = {product_id:dataObj.product_id, sort_order: dataObj.sort_order,deleted:dataObj.deleted};
	SSAjax({
		url: "/admin/addproduct?action=delete_photo",
		data: form_data,
		success: function(data){}
	});
}

function get_product_photo_alt(el,dataObj) {
	$('#productimageAltModal').attr('sort_order',dataObj.sort_order);
	$('#productimageAltModal .content-wrapper').addClass('hide');
	$('#productimageAltModal .overlay, #productimageAltModal .shopstyxloader').removeClass('hide');

	$('.addproduct-modal .overlay.mainLoader').removeClass('hide').addClass('product_photo_alt');
	$('#productimageAltModal').addClass('open');
	$('#productimageAltModal').centerized({main:'.addproduct-modal',vertical:true});
	$('#productimageAltModal .imgholder img').remove();
	var img = $(el).parents('li').find('img').attr('src');


	SSAjax({
		url: "/admin/addproduct?action=get_product_photo_alt",
		data: dataObj,
		success: function(data){
			var data = $.parseJSON(data);

			$('#productimageAltModal .inputholder input').val(data.text);
			$('#productimageAltModal .imgholder').append('<img src="'+img+'">');
			$('#productimageAltModal .content-wrapper').removeClass('hide');
			$('#productimageAltModal .overlay, #productimageAltModal .shopstyxloader').addClass('hide');
		}
	});
}

function save_product_photo_alt(dataObj) {
	$('#productimageAltModal .shopstyxloader div').html('SAVING...');
	$('#productimageAltModal .overlay, #productimageAltModal .shopstyxloader').removeClass('hide');
	SSAjax({
		url: "/admin/addproduct?action=save_product_photo_alt",
		data: dataObj,
		success: function(data){
			//var data = $.parseJSON(data);
			$('#productimageAltModal').removeClass('open');
			$('#productimageAltModal .content-wrapper').addClass('hide');
			$('#productimageAltModal .imgholder img').remove();
			$('#productimageAltModal .inputholder input').val();	
			$('#productimageAltModal .shopstyxloader div').html('LOADING...');
			$('.addproduct-modal .overlay.mainLoader').addClass('hide').removeClass('product_photo_alt');
		}
	});
}

function shipping_form_params() {
	var radioCheck = /radio|checkbox/i;
	var data = {},
				current;
	var keyBreaker = /[^\[\]]+/g;			

	$('.addproduct-modal input[name^="shipping_global_meta"]').each(function(){
		var el = this,
			type = el.type && el.type.toLowerCase();
		var key = el.name;
		var parts = key.match(keyBreaker),
			value = $.data(el, "value") || $.fn.val.call([el]),
			isRadioCheck = radioCheck.test(el.type),
			write = !isRadioCheck || !! $(el).attr('checked'),
			lastPart;

		//console.log(parts);
		current = data;
		for ( var i = 0; i < parts.length - 1; i++ ) {
			if (!current[parts[i]] ) {
				current[parts[i]] = {};
			}
			current = current[parts[i]];

			//console.log(current);
		}

		lastPart = parts[parts.length - 1];
		if ( lastPart in current && type === "checkbox" ) {
			if (!$.isArray(current[lastPart]) ) {
				current[lastPart] = current[lastPart] === undefined ? [] : [current[lastPart]];
			}
			if ( write ) {
				current[lastPart].push(value);
			}
		} else if ( write || !current[lastPart] ) {
			current[lastPart] = write ? value : undefined;
		}
	});

	return data;
}

function get_product_id() {
	return $('.addproduct-modal form').attr('product_id');
}

function generate_product_id() {
	SSAjax({
		url: "/admin/addproduct?action=generate_product_id",
		success: function(response) {
			var data = $.parseJSON(response);
		}
	});
}

function check_allowed_image_filetype(file_mime,file_ext) {
	file_ext = file_ext.toLowerCase();
	if($.inArray(file_mime,["images/jpeg","images/jpg","images/png","images/gif"]) == -1 &&
		$.inArray(file_mime,["jpeg","jpg","png","gif"]) == -1
		) {
		return false;
	}

	return true;
}

function get_subdepartments(options){
	SSAjax({
		url: "/admin/addproduct?action=get_subdepartments",
		data: {category_id:options.data.category_id},
		success: options.success
	});
}

function resize_containers(){
	$('#ap-left-menu').css('height','');
	$('#addproductModal .columns.borderline div').css('height','');

	$('#addproductModal #modal-content').height('');
	var wH = $(window).height() - 2; 
	var mH = $('#addproductModal').height();
	var cH = $('#addproductModal #modal-content');
	
	if(mH < wH) {
		cH.height(wH); 
	}

	cH = cH.height();

	lh = (cH > wH) ? cH: wH;

	$('#ap-left-menu').height(lh-70);
	$('#addproductModal .columns.borderline div').height(cH-91);
	return true;
}

function toggle_category_loader() {

	if($('#category-content-wrapper .overlay').hasClass('hide')) {
		$('#category-content-wrapper .overlay').removeClass('hide');
		$('#category-content-wrapper .shopstyxloader').removeClass('hide');
	}
	else {
		$('#category-content-wrapper .overlay').addClass('hide');
		$('#category-content-wrapper .shopstyxloader').addClass('hide');
	}
	return true;
}

function addprod_selected_categories() {
	return $('#cat-list-wrapper li input[name="categories[]"][checked]').map(function() {
					return $(this).val();
				}).get();
}

function addprod_edit_category(cat_id) {
	toggle_category_loader();

	$('.cat-img-upload-wrapper').removeClass('setimage').removeAttr('style');
	$('.cat-img-upload-wrapper img').remove();
	$('#category_name').val('');
	$('#category_parent_id').val(0);
	$('#category_desc').val('');
	$('#addon-imgdesc-wrapper').hide();
	$('.addon-imgdesc-btn').show();
	$('#category_name').parent('.common-field-wrapper').removeClass('error');

	SSAjax({
		data: {cat_id: cat_id},
		url: "/admin/addproduct?action=get_category_data",
		success: function(response){

			if(response == "") { return; }	

			results = $.parseJSON(response);
			$('#cat-addform').attr('cat_id',results.order_id);
			$('#category_name').val(results.category_name);
			$('#category_parent_id').val(results.parent_order_id);	
			$('#category_parent_id option[value="'+results.order_id+'"]').hide();

			if(results.hasOwnProperty('has_subcat')) {
				$('#category_parent_id').attr('disabled','disabled').addClass('disabled');
			}
			else {
				$('#category_parent_id').removeAttr('disabled').removeClass('disabled ');
			}

			if($.trim(results.description) != "" || $.trim(results.category_image) != "") {
				$('#addon-imgdesc-wrapper').show();
				$('.addon-imgdesc-btn').hide();

				if(results.category_image != "") {
					$('.cat-img-upload-wrapper')
						.addClass('setimage')
						.removeClass('auto')
						.css({'background-image':'url('+cloud_url+'uploads/category_images/'+results.category_image+')'});
				}
				$('#category_desc').val(results.description);				
			}

			$('#save-category-btn').html('Update');
			$('#cat-list-wrapper').hide();
			$('#cat-addform').show();
			toggle_category_loader();
		}
	});
}

function addprod_save_category() {
	var cat_id = typeof $('#cat-addform').attr('cat_id') != "undefined" ? $('#cat-addform').attr('cat_id') : 0;
	var category_name = $.trim($('#category_name').val()),
		description = $.trim($('#category_desc').val()),
		parent_order_id = $('#category_parent_id').val();
	var form_data = {'order_id':cat_id,'category_name':category_name,'description':description,'parent_order_id':parent_order_id};

	toggle_category_loader();
	SSAjax({
		data: form_data,
		url: "/admin/addproduct?action=save_category",
		success: function(response){
			var results = $.parseJSON(response);
			if(results.hasOwnProperty('error')) {
				$('#category_name').parent('.common-field-wrapper').addClass('error');
				toggle_category_loader();
			}
			else {
				$('#category_name').parent('.common-field-wrapper').removeClass('error');

				var d;
				if(cat_id == 0) {
					d = results.order_id;

					$('#selected-cat-texts .placeholder').html('');

					if($('#selected-cat-texts .cat-text-wrapper').length == 0 ) {
						$('#selected-cat-texts').prepend('<div class="cat-text-wrapper" id="cat-text-wrapper-ID'+d+'">\
															<span class="text-label">'+category_name+'</span>\
															<a class="text-remove" cat_id="'+d+'"></a>\
														</div>');
					}
					else {
						$('#selected-cat-texts .cat-text-wrapper:last').after('<div class="cat-text-wrapper" id="cat-text-wrapper-ID'+d+'">\
															<span class="text-label">'+category_name+'</span>\
															<a class="text-remove" cat_id="'+d+'"></a>\
														</div>');
					}

					if($('.input-cat-wrapper #selected-cat-texts').height() > 26) {
						$('#category-content-wrapper').css('top','63px');
					}
					else {
						$('#category-content-wrapper').css('top','37px');
					}
				}

				recreate_assign_category_list(results.categories);

				if($('.cat-img-upload-wrapper input')[0].files.length > 0) {
					$('#cat-addform').attr('cat_id',0);
					cat_id = (cat_id == 0) ? results.order_id : cat_id;
					addprod_uploadIMG_category(cat_id);
				}
				else {
					$('#cat-addform').hide().attr('cat_id',0);
					$('#cat-list-wrapper').show();
					reload_category_list(d);
					toggle_category_loader();
				}
			}
		}
	});
}

function addprod_uploadIMG_category(cat_id){
	var files = $('.cat-img-upload-wrapper input')[0].files;
	var form_data = new FormData();
	var product_id = get_product_id();

	$.each(files, function(key, value)
	{
		form_data.append('userfile', value);
	});
	
	$.ajax({
		type: 'POST',
		data: form_data,
		cache: false,
		processData: false,
		contentType: false, 
		url: "/admin/addproduct?action=upload&type=category_photo&product_id="+product_id+"&category_id="+cat_id+"&token="+store.get('token')+"&user_id="+store.get('user_id'),
		success: function(response){
			//var results = $.parseJSON(response);
			$('.cat-img-upload-wrapper img').remove();
			$('#cat-addform').hide();
			$('#cat-list-wrapper').show();	
			toggle_category_loader();
			reload_category_list(cat_id);
		}
	});
}

function addprod_delete_category(cat_id,element) {
	toggle_category_loader();
	SSAjax({
		data: {cat_id: cat_id},
		url: "/admin/addproduct?action=delete_category_data",
		success: function(response){
			var results = $.parseJSON(response);
			if(results.hasOwnProperty('error')) {
				toggle_category_loader();
				var catname = element.parent().attr('text');
				$('#catname-tobe-delete').html(catname);
				$('#cat_id_assign option[value="'+cat_id+'"]').hide();
				$('#cat-assignform').show().attr('cat_id',cat_id);
				$('#cat-addform').hide();
				$('#cat-list-wrapper').hide();
			}
			else {
				$('#category_parent_id option[value="'+cat_id+'"]').remove();
				$('#cat-text-wrapper-ID'+cat_id).remove();
				reload_category_list();
				toggle_category_loader();
				recreate_assign_category_list(results.categories);
			}
		}
	});
}

function addprod_transfer_category() {
	var cat_id = typeof $('#cat-assignform').attr('cat_id') != "undefined" ? $('#cat-assignform').attr('cat_id') : 0;
	var transfer_id = $('#cat_id_assign').val();

	if(transfer_id == 0 || transfer_id == null || transfer_id == "") {
		alert("Please select a category.");
		return;
	}
	toggle_category_loader();
	SSAjax({
		data: {"transfer_id": transfer_id, "cat_id": cat_id},
		url: "/admin/addproduct?action=transfer_category_products",
		success: function(response){
			var results = $.parseJSON(response);
			$('#cat-assignform').hide().attr('cat_id',0);
			$('#cat-addform').hide();
			$('#cat-list-wrapper').show();

			$('#cat-text-wrapper-ID'+cat_id).remove();
			$('#category_parent_id option[value="'+cat_id+'"]').remove();

			toggle_category_loader();
			reload_category_list();

			recreate_assign_category_list(results.categories);
		}
	});
}

function reload_category_list(d) {
	var product_categories = addprod_selected_categories();

	if(typeof d != "undefined") {
		product_categories[product_categories.length] = d;
	}

	var type = "basic";
	if($('.addproduct-modal').hasClass('advanced')) {
		type = "advanced";
	}
	var category_id = $('.addproduct-modal form').attr('max_images');
	var data = $.parseJSON($('.addproduct-modal form').attr('postdata'));
	$.extend(data,{"product_categories":product_categories,"type":type});
	
	toggle_category_loader();
	SSAjax({
		data: data,
		url: "/admin/addproduct?action=get_category_list",
		success: function(response){
			$('#cat-list-wrapper').html(response);
			toggle_category_loader();
			resize_containers();
		}
	});
}

function recreate_assign_category_list(categories) {
	var sub;
	var html = '<option value="0" disabled="disabled" selected="selected">Assign to category</option>';
	var parent_cat = '<option value="0" selected="selected">No parent category</option>';
	//console.log(categories);
	if(!$.isEmptyObject(categories)) {
		for(i in categories) {
			if(categories[i].hasOwnProperty('sub') && !$.isEmptyObject(categories[i].sub)) {
				sub = categories[i].sub;
				html += '<optgroup label="'+categories[i].category_name+'">';
				for(x in sub) {
					html += '<option value="'+sub[x].order_id+'">'+sub[x].category_name+'</option>';	
				}
				html += '</optgroup>';
			}
			else {
				html += '<option value="'+categories[i].order_id+'">'+categories[i].category_name+'</option>';		
			}

			if(categories[i].parent_order_id == 0) {
				parent_cat += '<option value="'+categories[i].order_id+'">'+categories[i].category_name+'</option>';
			}
		}
	}
	//console.log(html);

	$('#cat_id_assign').html(html);
	$('#category_parent_id').html(parent_cat);
}

function addprod_checkvideo_url(url) {
	SSAjax({
		url: '/admin/addproduct?action=check_video_url',
		data: {"url":url},
		success: function(data){
			var _data = $.parseJSON(data);
			var html = '';
			if(_data.valid) {
				var id = _data.id;
				if(_data.type == 'vimeo') {
					html = '<iframe width="408" height="275" style="height:275px; width:408px" src="http://player.vimeo.com/video/'+id+'?title=0&amp;byline=0&amp;portrait=0&amp;badge=0&amp;color=ffffff" width="380" height="260" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen></iframe>';
				}
				else if(_data.type == 'youtube'){
					html = '<iframe width="408" height="275" style="height:275px; width:408px" src="https://www.youtube.com/embed/'+id+'?wmode=transparent" frameborder="0" allowfullscreen></iframe>';
				}
				else {
					//html = '<span>Video Preview</span>';
				}
				$('#videoplayer-wrapper').html(html);
				$('#videoplayer-wrapper').show();
				$('#video-wrapper span').hide();
			}
			else {
				$('#video-wrapper span').show();
				$('#videoplayer-wrapper').html('');
				$('#videoplayer-wrapper').hide();
				$('#video-wrapper').removeClass('loader');
			}
			
		}
	});
}

function product_option_toggle_loader(text) {
	var loader = $('.ap-content-options #option-form-wrapper .shopstyxloader');
	var overlay = $('.ap-content-options #option-form-wrapper .overlay');

	if(typeof text != "undefined") {
		loader.find('div').html(text);
	}
	if(loader.hasClass('hide')) {
		loader.removeClass('hide');
		overlay.removeClass('hide');
	}
	else {
		loader.addClass('hide');
		overlay.addClass('hide');
	}
}

function reload_product_option() {
	product_option_toggle_loader();

	SSAjax({
			url: '/admin/addproduct?action=option_form',
			data: {product_id:get_product_id()},
			success: function(data){
				//var _data = $.parseJSON(data);
				product_option_toggle_loader();
				$('.ap-content-options').html(data);
			}
		});
}

function save_product_options(data, mycallback) {
	product_option_toggle_loader()

	SSAjax({
			url: '/admin/addproduct?action=save_product_options',
			data: data,
			success: function(data){
				var _data = $.parseJSON(data);

				$('.addproduct-modal form').attr('product_id',_data.product_id)
				/*
				if(typeof mycallback == "function") {
					var callbacks = $.Callbacks();
					callbacks.add( mycallback );
					callbacks.fire();
				}
				*/

				product_option_toggle_loader();
				reload_product_option();
			}
		});	
}

function product_option_get_total_optionvalues(tagValue, action, element) {
	var vLength1 = $('#option-entry-wrapper.add ul li[enabled="true"].option-li:nth-child(1) ul.tagfield').tagit("tags").length,
		vLength2 = $('#option-entry-wrapper.add ul li[enabled="true"].option-li:nth-child(2) ul.tagfield').tagit("tags").length,
		vLength3 = $('#option-entry-wrapper.add ul li[enabled="true"].option-li:nth-child(3) ul.tagfield').tagit("tags").length;
	var container = $('.add-variants-wrapper .auto-create-variants-wrapper span.color5 span');				
	container.html('').hide();

	vLength = vLength1;
	if(vLength2 > 0) {
		vLength *= vLength2;
	}

	if(vLength3 > 0) {
		vLength *= vLength3;
	}

	if(vLength > 0) {
		container.html(vLength).show();
	} 
}

function product_option_get_all_existing_variants() {
	var variants = $('#variants-entry-wrapper .col-variants tr.data').not('.addedit, .hide').map(function(){
					var values = $(this).find('.op-value div').map(function(){
						return $(this).attr('value_id');
					});

					values = $.makeArray(values).join('');
					return values;
			});
	return  $.makeArray(variants);
}

function product_option_delete_variants(type,data) {
	if(type == "bulk") {
		var option_id = $('#managevariant-options-select').val();
		var value_id = $('#managevariant-values-select').val();
		product_option_toggle_loader('Updating content...');
		SSAjax({
			url: '/admin/addproduct?action=delete_variants',
			data: {product_id:get_product_id(),"option_id":option_id,"value_id":value_id},
			success: function(data){
				//var _data = $.parseJSON(data);
				product_option_toggle_loader();
				reload_product_option();
			}
		});

	}
	else {
		product_option_toggle_loader('Updating content...');
		$.extend(data, {product_id:get_product_id()});
		SSAjax({
			url: '/admin/addproduct?action=delete_variants',
			data: data,
			success: function(data){
				//var _data = $.parseJSON(data);
				product_option_toggle_loader();
				reload_product_option();
			}
		});
	}
}

function product_option_save_variant(data,mycallback) {
	product_option_toggle_loader();

	SSAjax({
			url: '/admin/addproduct?action=save_variant',
			data: data,
			success: function(data){
				product_option_toggle_loader();
				data =  $.parseJSON(data);
				if(data.hasOwnProperty('error')) {
					alert(data.error);
					return;
				}

				if(data.hasOwnProperty('new_option_value')) {
					reload_product_option();
				}
				
				if(typeof mycallback == "function") {
					var callbacks = $.Callbacks();
					callbacks.add( mycallback );
					callbacks.fire(data);
				}

				
			}
		});	
}
