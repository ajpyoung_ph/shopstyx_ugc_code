var vid = CS.vid,
	cloud_url = CS.cloudURL || location.protocol + '//' + location.host + '/',
	root = CS.siteURL || location.protocol + '//' + location.host + '/',
	main_url = root,
	current_url = location.href,
	loadMPhone = true;

function getScripts(js) {
	if(typeof js == "object" && js.length > 0){
		for(var i = 0; i < js.length; i++){
			$.getScript(js[i]);
		}
	}
	return true;
}

var SSAjax = function( options ) {
		var form_data = {
				token: store.get('token'),
				user_id: store.get('user_id')
			};

        // Default options.
        var settings = $.extend(true,{
            // These are the defaults.
			loadCSS: [],
            type: "POST",
            data: form_data,
            url: "",
            success: function(response) {
            	//console.log(response);
            },
            loadJS: []
            
        }, options );

        // loading external css and js
        var jsLib = "", cssLib = "", loadJS = new Array(), loadCSS = new Array();
        if(settings.hasOwnProperty('loadJS') && settings.loadJS.length > 0) {

        	if(typeof $('html').data('loadJS') != "object") {
        		$('html').data('loadJS',[]);
        	}

    		for(var i = 0; i < settings.loadJS.length; i++) {
    			if($.inArray(settings.loadJS[i], $('html').data('loadJS')) == -1) {
    				loadJS.push(settings.loadJS[i]);
					jsLib += ",$.getScript('"+settings.loadJS[i]+"')";
    			}
    		}
        }

        if(settings.hasOwnProperty('loadCSS') && settings.loadCSS.length > 0) {
        	if(typeof $('html').data('loadCSS') != "object") {
        		$('html').data('loadCSS',[]);
        	}

        	for(var i = 0; i < settings.loadCSS.length; i++) {
    			if($.inArray(settings.loadCSS[i], $('html').data('loadCSS')) == -1) {
    				loadCSS.push(settings.loadCSS[i]);
    			}
    		}
			/*
    		if(loadCSS.length > 0) {	
    			for(var i = 0; i < loadCSS.length; i++) {
					$.get(loadCSS[i], function(contents){
						$("<style type=\"text/css\">" + contents + "</style>").appendTo(document.head);
						// $('<link>')
							// .appendTo($('head'))
							// .attr({type : 'text/css', rel : 'stylesheet'})
							// .attr('href', loadCSS[i]);
					});
				}
			}
			*/
        }
       	
		var success = settings.success;
		settings.success = function(response) {
			
			if(response == "" || response == false) {
				// invalid toke and/or user_id, so logout
				return;
			}
			success(response);	   		
		};

		var token = store.get('token'), 
			last_stamp = store.get('session_date'),
			remember = store.get('remember');

		// check here if logged In session has already been expired
		var session = check_login_session({
							"token": token,
							"last_stamp": last_stamp,
							"remember": remember
						});

		// if session is expired, make a pop-up here
		if(!session) {
			alert("Your session has been expired due to inactivity!");
			autoLogout();
			return;
		}


		var login_data = {post_data:1,
							token:token,
							user_id:settings.data.user_id
						};

		// check here if user is logged In
		$.post("/checkuser.html",login_data,function(data){
			var _data = $.parseJSON(data);
			if(! _data.has_store){
				//alert(JSON.stringify(login_data));
				location.href = '/';
			}
			if( _data.result){
				store.set('display_name', _data.display_name);
				store.set('user_id', _data.user_id);

	        	if(loadJS.length > 0 || loadCSS.length > 0) {
					if(loadCSS.length > 0) {
						$.get(loadCSS[0], function(contents){
							$("<style type=\"text/css\">" + contents + "</style>").appendTo(document.head);
							$('html').data('loadCSS',$.merge($('html').data('loadCSS'),loadCSS));
							
							if(loadJS.length > 0) {
				        		$.when(eval(jsLib.substr(1)))//getScripts(loadJS))
				        		 .then(
				        		 	function() {
					        		 	$('html').data('loadJS',$.merge($('html').data('loadJS'),loadJS));
										// load the specific page
										$.ajax(settings);
				        		 	},
				        		 	function() {
				        		 		console.log('Unable to load the js external files. ' + settings.loadJS);
				        		 	}
				        		 );
							}
							else {
								setTimeout(function(){$.ajax(settings)},100);
							}
						});
					}
					else {
						$.when(eval(jsLib.substr(1)))//getScripts(loadJS))
						 .then(
							function() {
								$('html').data('loadJS',$.merge($('html').data('loadJS'),loadJS));
								// load the specific page
								$.ajax(settings);
							},
							function() {
								console.log('Unable to load the js external files. ' + settings.loadJS);
							}
						 );
					}
				}
				else {
					$.ajax(settings);
				}
			}
			else {
				/*
				store.remove('token');
				store.remove('user_id');
				store.remove('display_name');
				store.remove('remember');
				//return;
				// redirect user if not logged in
				alert("Sorry! You are not logged in.");
				location.href = "/";
				*/
			}
		});		
	};


var check_login_session = function(options) {
		var new_timestamp = new Date().getTime();
		// milliseconds
		var minutes_to_expire = 120000; // 2 minutes 

		if(( typeof options.token !== 'undefined' || options.token != '' ) 
		&& (options.last_stamp !== 'undefined' || options.last_stamp != '')  
		&& options.remember == 0 ){
			minutes_to_expire = 600000; // 10 minutes
			if ((new_timestamp - options.last_stamp) > 120000) {
				//return false;
			}
		}

		if(( typeof options.token !== 'undefined' || options.token != '' ) 
		&& (options.last_stamp !== 'undefined' || options.last_stamp != '')  
		&& options.remember == 1 ){
			minutes_to_expire = 604800000; // 7 days
			if (new_timestamp - options.last_stamp > 604800000) {
				return false;
			}
		}

		// renew the session date
		/*
		if( ( typeof store.get('token') !== 'undefined' || store.get('token') != '' ) 
		&& ( store.get('remember') == 0 || store.get('remember') == 1 ) ){

		}
		*/
		store.set('session_date', new_timestamp);
		return true;
	};

// check every 6 seconds if session has been expired	
setInterval(function() {
	var token = store.get('token'), 
			last_stamp = store.get('session_date'),
			remember = store.get('remember');

	// check here if logged In session has already been expired
	var session = check_login_session({
						"token": token,
						"last_stamp": last_stamp,
						"remember": remember
					});

	// if session is expired, make a pop-up here
	if(!session) {
		alert("Your session has been expired due to inactivity!");
		autoLogout();
		return;
	}

}, 6000);	

function autoLogout(){
	var url = root + 'signout.html';
	var form_data = {};
	form_data.post_dta = 1;
	form_data.temp_sessionid = store.get('temp_sessionid');
	form_data.token = store.get('token');
		
	$.ajax({
		type: "POST",
		url: url,
		data: form_data,
		cache: false,
		success: function(data){
			if(data == 'success'){
				//store.clear();
				store.remove('user_id');
				store.remove('cart');
				store.remove('token');

				var currentUrl = location.href.replace('#','');
				var pos = root.indexOf("?");
				var querystring = (pos == -1) ? "?referer="+ escape(currentUrl) : '&referer=' + pos;
				location.href = root + querystring;
			}
		}
	});
	return false;
}

function hash_querystring(hash){
	var query = {};
	if(typeof hash == "undefined" || hash == "") return query;

	var hash = hash.split('&');
	var q;

	for(var i = 0; i < hash.length; i++) {
		if(hash[i] != "") {
			q = hash[i].split('=');
			query[q[0]] = q[1].toString();
		}
	}
	return query;
}		

function get_hash_querystring_value(hash,name) {
	var hash = hash_querystring(hash);
	if(hash.hasOwnProperty(name)) {
		for(i in hash) {
			if(i == name) {
				return hash[i];
			}
		}
	}

	return "";
}

function load_page(param) {
	var url = $(location).attr('href').split('#');
	var mainURL = url[0], hash = url[1];
	
	//hash = typeof hash == "undefined" ? "" : hash;
	//var container = get_hash_querystring_value(hash,'c');
	var param = (typeof param == "undefined") ? {} : param; 
	var container = "#wrapper";
	var page_title;
	var user_id = store.get('user_id');
	var token = store.get('token');
	var form_data = {
		isAjax:true,
		token: token,
		user_id: user_id
	};
	var browserNav = false;
	
	if(param.hasOwnProperty('container')) {
		container = param.container;
	}

	if(param.hasOwnProperty('data')) {
		form_data = $.extend(true,form_data, param.data);
	}

	if(param.hasOwnProperty('page_tile')) {
		page_title = param.page_title;
	}
	else {
		page_title = $(document).find("title").text();
	}

	if(param.hasOwnProperty('url')) {
		mainURL = param.url;
		if(mainURL.indexOf(CS.siteURL) == -1) {
			mainURL = CS.siteURL + mainURL.substr(1);
		}
	}	

	if(param.hasOwnProperty('browserNav')) {
		browserNav = (param.browserNav) ? true : false;
	}

	if ( History.enabled) {
		History.pushState(null, page_title, mainURL);
	}

	//if(mainURL == History.getState().url && browserNav) {

	if(browserNav) {
		return;
	}

	if(container == "#subcontent-wrapper") {
		toggle_preloader_maincontent();
	}

	var controller = mainURL.replace(CS.siteURL,'');
	//alert(mainURL)
	var cssfile = CS.cloudURL + 'css/v2/main.css';
	if(controller != ""){
		controller = controller.split('/');
		controller = controller[1];
		controller = controller.split('?')[0];
		controller = controller.toLowerCase().replace(/[^a-z0-9]/g,'');
		var cssfile = CS.cloudURL + 'css/v2/' + controller + '.css';
	}
	var loadCSS = new Array();

	if($.inArray(cssfile, $('html').data('loadCSS')) == -1) {
		loadCSS.push(cssfile);
	}

	if(typeof $('html').data('loadCSS') != "object") {
		$('html').data('loadCSS',[]);
	}
	if(loadCSS.length > 0) {
		$.get(cssfile, function(contents){
			$("<style type=\"text/css\">" + contents + "</style>").appendTo(document.head);
		})
		.always(function(){
			$('html').data('loadCSS',$.merge($('html').data('loadCSS'),loadCSS));

			SSAjax({
				data: form_data,
				url: mainURL,
				success:function(response) {
					$(container).html(response);
					if(container == "#subcontent-wrapper") {
						toggle_preloader_maincontent();
					}
					else if(container == "#wrapper") {
						toggleScroll_leftmenu();
					}
				},
				error: function(jqXHR, textStatus, errorThrown ) {
					location.href = "/";
				}
			});
		});

		return;
	}

	SSAjax({
		data: form_data,
		url: mainURL,
		success:function(response) {
			$(container).html(response);
			if(container == "#subcontent-wrapper") {
				toggle_preloader_maincontent();
			}
			else if(container == "#wrapper") {
				toggleScroll_leftmenu();
			}
		},
		error: function(jqXHR, textStatus, errorThrown ) {
			if(jqXHR.status == 404) {
				location.href = "/admin/dashboard";
			}
		}
	});
}

function toggle_preloader_maincontent() {
	if($('#body-wrapper .shopstyxloader').eq(0).hasClass('hide')) {
		$('#subcontent-wrapper').eq(0).addClass('hide');
		$('#body-wrapper .shopstyxloader').eq(0).removeClass('hide');
		$('#body-wrapper .overlay').eq(0).removeClass('hide');	
	}
	else {
		$('#body-wrapper .shopstyxloader').eq(0).addClass('hide');
		$('#body-wrapper .overlay').eq(0).addClass('hide');
		$('#subcontent-wrapper').eq(0).removeClass('hide');
	}
}


/* 	===================================
 *	Events Section
 *	===================================
 */

$(document).on('click touchstart', '#header .userNav .logout', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	autoLogout();
});

$(document).on('change', '#change-profile-photo', function(e){
	_upload_userpic($(this));
});

$(document).on('mouseenter', '#header .userNav .navLink.messages', function(e){
	close_panels();
	nummsg = 0;
	$(this).find('.messages-panel.npops').removeClass('hide');
});

$(document).on('mouseleave', '#header .userNav .navLink.messages', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	close_messages();
});


$(document).on('mouseenter', '#header .userNav .navLink.cart', function(){
	close_panels();
	if($(this).hasClass('active')) {
		$(this).find('.cart-panel.npops').removeClass('hide');
	}
	else {
		load_cart();
	}
});

$(document).on('mouseleave', '#header .userNav .navLink.cart', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	close_cart();
});

$(document).on('mouseenter', '#header .userNav .navLink.wallet', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	close_panels();
	if($(this).hasClass('active')) {
		$(this).find('.wallet-panel.npops').removeClass('hide');
	}
	else {
		load_wallet();
	}
});

$(document).on('mouseleave', '#header .userNav .navLink.wallet', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	close_wallet();
});

$(document).on('mouseenter', '#header .userNav .navLink.notif', function(e){
	close_panels();
	var cntr = $('.navLink.notif .bg span.total-counter');
	cntr.addClass('hide');
	
	SSAjax({
		url: '/notifications/mark_read_notifs',
		data: {user_id: store.get('user_id'), 
			   token: store.get('token') }
	});
	numnotifs=0;
				
	$(this).find('.notification-panel.npops').removeClass('hide');
});

$(document).on('mouseleave', '#header .userNav .navLink.notif', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	close_panels();
});

$(document).on('click', '#header .userNav .wallet-panel .claim-money-btn:not(".disabled")', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	var type = ($(this).hasClass('C')) ? "C" : "S";
	claim_walet_popup(type);
});

$(document).on('click', '.wallet-modal #claim-wallet-form .paypal-email-txt a, .wallet-modal #claim-wallet-form .no-paypal-email a', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	var type = $('.wallet-modal #claim-wallet-form').attr('wtype');
	wallet_paypalemail_popup(type);
});

$(document).on('click', '#wallet-pop .header-wrapper .right.back-btn, #wallet-pop .header-wrapper .claim-wallet-backbtn', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	var type = $(this).attr('wtype');
	claim_walet_popup(type);
});

$(document).on('open', '#wallet-pop', function () {
	$(this).centerized();
	$('#main-wrapper').hide();

});
$(document).on('opened', '#wallet-pop', function () {
	$('.reveal-modal-bg').addClass('reveal-modal-bg-color').show();
	//var top = parseInt($('.product-details-view').position().top) - 88;
	//$('.product-details-view').css({'top': $('.product-details-view').position().top});
});

$(document).on('close', '#wallet-pop', function () {
	$('.reveal-modal-bg.reveal-modal-bg-color').hide();
});

$(document).on('closed', '#wallet-pop', function () {
	if(typeof dispute_countdownTimer != "undefined") {
		clearInterval(dispute_countdownTimer);
	}
	var loader = $(this).find('.loadcont');
	loader.addClass('hide');	
	$(this).find('.loader').addClass('hide');	
	$(this).find('.loader-overlay').addClass('hide');
	$('#main-wrapper').show();
	
	if(typeof scrollopts != "undefined") {
		delete scrollopts['for-purchasehistory'];
	}
});


$(document).on('click', '.wallet-modal .cont .pent #save-paypal-email-btn', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	var wtype = $(this).attr('wtype');
	save_promoter_paypalemail(wtype);
});

$(document).on('click', '.wallet-panel .history-btn.walletpurchases, #wallet-pop .header-wrapper .purchase-history-backbtn', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	load_wallet_purchase_history();
});

$(document).on('click', '.wallet-panel .history-btn.walletsales', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	load_wallet_sales_history();
});

$(document).on('click', '#wallet-pop .header-wrapper .viewhistory-btn', function(e){
	var wtype = $(this).attr('wtype');
	e.preventDefault(); e.stopImmediatePropagation();
	load_wallet_claimed_history(wtype);
});

/* Claim Money on wallet */
$(document).on('keyup ', '.wallet-modal #claim-wallet-form #claim-wallet-amount', function(e) {
	e.stopImmediatePropagation();
	val = parseFloat($(this).val());
	val = isNaN(val) ? 0 : val;
	paypal_fee = (val*0.02);
	paypal_fee = Math.round(paypal_fee*100) / 100; 
	paypal_fee = paypal_fee.toFixed(2);
	claim_amount = val - paypal_fee;
	claim_amount = Math.round(claim_amount*100) / 100; 
	//claim_amount = claim_amount.toFixed(2);
	claim_amount = claim_amount.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");
	$('#claim-wallet-form .claim-form-wrapper .field input#claim-wallet-paypal-fee').val(paypal_fee);
	$('#claim-wallet-form .claim-form-wrapper .claim-wallet-btn').html('Claim $'+claim_amount);
});

$(document).on('click', '.wallet-modal #claim-wallet-form .claim-form-wrapper .claim-wallet-btn:not(".disabled")', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	claim_wallet($(this),'authenticate');
});

$(document).on('click', '#wallet-pop .cont .claimcofirmation-wrapper button', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	claim_wallet($(this),'claim');
});

$(document).on('click', '#wallet-pop .cont .my-tbl.purchasehistory .tbl-cell.ordertotal .od-btns-wrapper', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	var id = $(this).attr('order_id');
	view_purchase_order_details(id);
});

$(document).on('click', '#wallet-pop .cont .order-details-wrapper .confirm-order-btn.enabled', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	var id = $(this).attr('order_id');
	confirm_transaction_order(id);
});

$(document).on('click', '#wallet-pop .cont .order-details-wrapper .open-dispute-product-btn .distpute-type-wrapper .dlink', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	var el = $(this).parent(),
		id = el.attr('order_id'),
		pid = el.attr('pid'),
		variant_id = el.attr('variant_id')
		dtype = $(this).attr('dtype');
	open_dispute_order_product(dtype,id,pid,variant_id);
});

$(document).on('click', '#wallet-pop .cont .order-details-wrapper .order-dispute-btn', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	var el = $(this),
		id = el.attr('order_id'),
		pid = 0,
		variant_id = 0
		dtype = 1;
	open_dispute_order_product(dtype,id,pid,variant_id);
});

$(document).on('click', '#wallet-pop .cont .open-dispute-modal .closedispute-btn', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	$('#wallet-pop .open-dispute-modal').addClass('hide').html('');
	$('#wallet-pop .cont .order-details-wrapper').removeClass('hide');
});

$(document).on('change','.open-dispute-modal .uploadfile-btn input[name="dispute_images"]', function(e) {
	var input = this;
	var limit_reached = false;
	var form = $('#wallet-pop .open-dispute-modal form');
	var upload_btn = $('.open-dispute-modal .uploadfile-btn');
	var ul = $('.open-dispute-modal .dispute-form-wrapper .dispute-images ul');
	var max_images = 3;
    if (input.files && input.files[0]) {
        for(var i = 0, f; f = this.files[i]; i++) {
            // Only process image files.
            var file_mime = f.type;
            var fname = f.name.split('.');
            var flength = fname.length-1;
            var fext = fname[flength];


            var is_allowed = ($.inArray(file_mime,["images/jpeg","images/jpg","images/png"]) == -1 &&
								$.inArray(file_mime,["jpeg","jpg","png"]) == -1) ? false : true;

            // Only process image files.
            if (!file_mime.match('image.*') && !is_allowed) {
                continue;
            }
            else {
                if(limit_reached) {
                	upload_btn.addClass('hide');
                    continue;
                } else {
                    if(typeof form.data('image_files') != "undefined" && form.data('image_files').length >= max_images) {
                        //alert("You have reached the maximum photos that can be uploaded.");
                        limit_reached = true;
                        upload_btn.addClass('hide');
                        continue;
                    }
                    else {
                    	if(typeof form.data('image_files') == "undefined") {
                    		form.data('image_files',[]);
                    	}
                    	form.data('image_files').push(f);

                    	var li = document.createElement("li");
						li.innerHTML = f.name + " <a>delete</a>";
						ul.append(li);

						if(typeof form.data('image_files') != "undefined" && form.data('image_files').length >= max_images) {
							limit_reached = true;
	                        upload_btn.addClass('hide');
						}
                    }
                }
            }
        }
    }
}); 

$(document).on('click', '.open-dispute-modal .dispute-form-wrapper .dispute-images ul li a', function(e) {
	e.preventDefault(); e.stopImmediatePropagation();
	var form = $('#wallet-pop .open-dispute-modal form');
	var index = $('.open-dispute-modal .dispute-form-wrapper .dispute-images ul li').index($(this).parent());
	var upload_btn = $('.open-dispute-modal .uploadfile-btn'),
		max_images = 3;

	form.data('image_files').splice(index,1);
	$(this).parent().remove();

	if(form.data('image_files').length == 0) {
		form.data('image_files',[]);
	}

	if(form.data('image_files').length < max_images) {
		upload_btn.removeClass('hide');
	}
});

$(document).on('submit', '#wallet-pop .open-dispute-modal form', function(e) {
	e.preventDefault(); e.stopImmediatePropagation();
	submit_order_dispute($(this));
});

$(document).on('click', '.main-modal .mmodal-header .action-buttons .close-btn', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	var cpo = new customer_purchase_order();
	cpo.close();
});

$(document).on('click', '#header .userNav .navLink.messages', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	load_message($(this));
});

$(document).on('click', '#header .userNav .message-panel .close-btn', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	close_message();
});

$(document).on('touchstart click', '.leftmenu ul li', function(){
	var id = $(this).attr('class');
	id = id.replace('hover','').replace('selected','');
	id = $.trim(id);

	$('.leftmenu ul li').removeClass('hover');
	$('.left-submenu').removeClass('hide');
	$(this).addClass('hover');

	// Set the effect type
    var effect = 'slide';
 
    // Set the options for the effect type chosen
    var options = { direction: 'left' };
 
    // Set the duration (default: 400 milliseconds)
    var duration = 200;

    $('.leftmenu').data('mouseleave',false);
    //console.log($('.left-submenu').find('#'+id).css('display'))
    if($('.left-submenu').find('#'+id).css('display') != "none" && $('.left-menu').hasClass('show')) {
    	return;
    }
    $('.leftmenu-sub').hide();
    $('.left-submenu').addClass('show');
    $('#body-wrapper').addClass('show');
    $('.left-submenu').find('#'+id).toggle(effect, options, duration);

    toggleScroll_leftmenu();
});

$(document).on('mouseleave blur', '.left-submenu', function(){
	$('.leftmenu').data('mouseleave',true);	
	setTimeout(function(){
		if($('.leftmenu').data('mouseleave')) {
			$('.leftmenu').data('mouseleave',true);

			var effect = 'slide';
		    var options = { direction: 'left' };
		    var duration = 200;
			var id = $('.leftmenu ul li.selected').attr('class');
			id = id.replace('hover','').replace('selected','');
			id = $.trim(id);

			if($('.left-submenu').find('#'+id).css('display') != "none") {
				return;
			}
		    
			$('.leftmenu ul li').removeClass('hover');
			$('.leftmenu-sub').hide();
			$('.left-submenu').find('#'+id).toggle(effect, options, duration);
		}
	},100);
});

$(document).on('mouseenter touchstart mousedown', '.left-submenu', function(){
	$('.leftmenu').data('mouseleave',false);
});

$(document).on('mouseleave', '.leftmenu ul li', function(){
	$('.leftmenu').data('mouseleave',true);
	setTimeout(function(){
		if($('.leftmenu').data('mouseleave')) {
			$('.leftmenu').data('mouseleave',true);

			
			var effect = 'slide';
		    var options = { direction: 'left' };
		    var duration = 200;
			var id = $('.leftmenu ul li.selected').attr('class');
			id = id.replace('hover','').replace('selected','');
			id = $.trim(id);

			if($('.left-submenu').find('#'+id).css('display') != "none") {
				return;
			}
		    
			$('.leftmenu ul li').removeClass('hover');
			$('.leftmenu-sub').hide();
			$('.left-submenu').find('#'+id).toggle(effect, options, duration);
		}
	},100);
});

$(document).on('click touchstart', '.mmenu-link', function(e){
	if ( !History.enabled ) {
		return;
	}
	else {
		e.preventDefault(); e.stopImmediatePropagation();

		var el = $(this);
		var currentURL = $(location).attr('href').split('#');
		var mainURL = currentURL[0];
		var url  = el.attr('href'), title = el.attr('page_title');
		url = (url == "") ? mainURL : url;

		if(el.parents('#inner-leftmenu').length > 0) {
			el.parents('#inner-leftmenu').find('li').removeClass('selected');
			el.parent().addClass('selected');
		}
		
		if($('.close-basicmodal-btn').length > 0){ $('.close-basicmodal-btn').trigger("click"); }
		if($('.close-reveal-modal-btn').length > 0){ $('.close-reveal-modal-btn').trigger("click"); }
		if($('.modal-close').length > 0){ $('.modal-close').trigger("click"); }
		if($('.close-modal').length > 0){ $('.close-modal').trigger("click"); }
		
		if(el.parent().hasClass('sm-catalog')){
			$('.preview-shop-btn').removeClass('hide');
			$('.manage-coll-wrapper').removeClass('hide');
			$('.manage-design-wrapper').addClass('hide');
			$('.manage-people-wrapper').addClass('hide');
			$('.manage-pages-wrapper').addClass('hide');
			$('.manage-orders-wrapper').addClass('hide');
			$('.manage-sales-wrapper').addClass('hide');
			$('.manage-shopsettings-wrapper').addClass('hide');
			$('.manage-shipping-wrapper').addClass('hide');
			$('.manage-taxes-wrapper').addClass('hide');
			$('.manage-policy-wrapper').addClass('hide');
			$('.manage-getpaid-wrapper').addClass('hide');
			$('.prod-btnd').addClass('active');
			$('.coll-btnd').removeClass('active');
		}else if(el.parent().hasClass('sm-orders')){
			$('.preview-shop-btn').removeClass('hide');
			$('.manage-coll-wrapper').addClass('hide');
			$('.manage-people-wrapper').addClass('hide');
			$('.manage-pages-wrapper').addClass('hide');
			$('.manage-design-wrapper').addClass('hide');
			$('.manage-sales-wrapper').addClass('hide');
			$('.manage-shopsettings-wrapper').addClass('hide');
			$('.manage-shipping-wrapper').addClass('hide');
			$('.manage-policy-wrapper').addClass('hide');
			$('.manage-getpaid-wrapper').addClass('hide');
			$('.manage-taxes-wrapper').addClass('hide');
			$('.manage-orders-wrapper').removeClass('hide');
		}else if(el.parent().hasClass('sm-sales')){
			$('.preview-shop-btn').removeClass('hide');
			$('.manage-coll-wrapper').addClass('hide');
			$('.manage-people-wrapper').addClass('hide');
			$('.manage-pages-wrapper').addClass('hide');
			$('.manage-design-wrapper').addClass('hide');
			$('.manage-orders-wrapper').addClass('hide');
			$('.manage-shopsettings-wrapper').addClass('hide');
			$('.manage-shipping-wrapper').addClass('hide');
			$('.manage-policy-wrapper').addClass('hide');
			$('.manage-getpaid-wrapper').addClass('hide');
			$('.manage-taxes-wrapper').addClass('hide');
			$('.manage-sales-wrapper').removeClass('hide');
		}else if(el.parent().hasClass('sm-people')){
			$('.preview-shop-btn').removeClass('hide');
			$('.manage-coll-wrapper').addClass('hide');
			$('.manage-design-wrapper').addClass('hide');
			$('.manage-pages-wrapper').addClass('hide');
			$('.manage-orders-wrapper').addClass('hide');
			$('.manage-sales-wrapper').addClass('hide');
			$('.manage-shopsettings-wrapper').addClass('hide');
			$('.manage-shipping-wrapper').addClass('hide');
			$('.manage-policy-wrapper').addClass('hide');
			$('.manage-getpaid-wrapper').addClass('hide');
			$('.manage-taxes-wrapper').addClass('hide');
			$('.manage-people-wrapper').removeClass('hide');
		}else if(el.parent().hasClass('sm-design')){
			$('.preview-shop-btn').removeClass('hide');
			$('.manage-coll-wrapper').addClass('hide');
			$('.manage-people-wrapper').addClass('hide');
			$('.manage-pages-wrapper').addClass('hide');
			$('.manage-orders-wrapper').addClass('hide');
			$('.manage-sales-wrapper').addClass('hide');
			$('.manage-shopsettings-wrapper').addClass('hide');
			$('.manage-shipping-wrapper').addClass('hide');
			$('.manage-policy-wrapper').addClass('hide');
			$('.manage-getpaid-wrapper').addClass('hide');
			$('.manage-taxes-wrapper').addClass('hide');
			$('.manage-design-wrapper').removeClass('hide');
		}else if(el.parent().hasClass('sm-pages')){
			$('.preview-shop-btn').removeClass('hide');
			$('.manage-coll-wrapper').addClass('hide');
			$('.manage-design-wrapper').addClass('hide');
			$('.manage-people-wrapper').addClass('hide');
			$('.manage-orders-wrapper').addClass('hide');
			$('.manage-sales-wrapper').addClass('hide');
			$('.manage-shopsettings-wrapper').addClass('hide');
			$('.manage-shipping-wrapper').addClass('hide');
			$('.manage-policy-wrapper').addClass('hide');
			$('.manage-getpaid-wrapper').addClass('hide');
			$('.manage-taxes-wrapper').addClass('hide');
			$('.manage-pages-wrapper').removeClass('hide');
		}else if(el.parent().hasClass('sm-general')){
			$('.preview-shop-btn').removeClass('hide');
			$('.manage-coll-wrapper').addClass('hide');
			$('.manage-design-wrapper').addClass('hide');
			$('.manage-people-wrapper').addClass('hide');
			$('.manage-orders-wrapper').addClass('hide');
			$('.manage-sales-wrapper').addClass('hide');
			$('.manage-pages-wrapper').addClass('hide');
			$('.manage-shipping-wrapper').addClass('hide');
			$('.manage-policy-wrapper').addClass('hide');
			$('.manage-getpaid-wrapper').addClass('hide');
			$('.manage-taxes-wrapper').addClass('hide');
			$('.manage-shopsettings-wrapper').removeClass('hide');
		}else if(el.parent().hasClass('sm-shipping')){
			$('.preview-shop-btn').removeClass('hide');
			$('.manage-coll-wrapper').addClass('hide');
			$('.manage-design-wrapper').addClass('hide');
			$('.manage-people-wrapper').addClass('hide');
			$('.manage-orders-wrapper').addClass('hide');
			$('.manage-sales-wrapper').addClass('hide');
			$('.manage-shopsettings-wrapper').addClass('hide');
			$('.manage-pages-wrapper').addClass('hide');
			$('.manage-taxes-wrapper').addClass('hide');
			$('.manage-policy-wrapper').addClass('hide');
			$('.manage-getpaid-wrapper').addClass('hide');
			$('.manage-shipping-wrapper').removeClass('hide');
		}else if(el.parent().hasClass('sm-policy')){
			$('.preview-shop-btn').removeClass('hide');
			$('.manage-coll-wrapper').addClass('hide');
			$('.manage-design-wrapper').addClass('hide');
			$('.manage-people-wrapper').addClass('hide');
			$('.manage-orders-wrapper').addClass('hide');
			$('.manage-sales-wrapper').addClass('hide');
			$('.manage-shopsettings-wrapper').addClass('hide');
			$('.manage-pages-wrapper').addClass('hide');
			$('.manage-taxes-wrapper').addClass('hide');
			$('.manage-shipping-wrapper').addClass('hide');
			$('.manage-getpaid-wrapper').addClass('hide');
			$('.manage-policy-wrapper').removeClass('hide');
		}else if(el.parent().hasClass('sm-getpaid')){
			$('.preview-shop-btn').removeClass('hide');
			$('.manage-coll-wrapper').addClass('hide');
			$('.manage-design-wrapper').addClass('hide');
			$('.manage-people-wrapper').addClass('hide');
			$('.manage-orders-wrapper').addClass('hide');
			$('.manage-sales-wrapper').addClass('hide');
			$('.manage-shopsettings-wrapper').addClass('hide');
			$('.manage-pages-wrapper').addClass('hide');
			$('.manage-taxes-wrapper').addClass('hide');
			$('.manage-shipping-wrapper').addClass('hide');
			$('.manage-policy-wrapper').addClass('hide');
			$('.manage-getpaid-wrapper').removeClass('hide');
		}else if(el.parent().hasClass('sm-taxes')){
			$('.preview-shop-btn').removeClass('hide');
			$('.manage-coll-wrapper').addClass('hide');
			$('.manage-design-wrapper').addClass('hide');
			$('.manage-people-wrapper').addClass('hide');
			$('.manage-orders-wrapper').addClass('hide');
			$('.manage-sales-wrapper').addClass('hide');
			$('.manage-shopsettings-wrapper').addClass('hide');
			$('.manage-pages-wrapper').addClass('hide');
			$('.manage-shipping-wrapper').addClass('hide');
			$('.manage-policy-wrapper').addClass('hide');
			$('.manage-getpaid-wrapper').addClass('hide');
			$('.manage-taxes-wrapper').removeClass('hide');
		}else{
			$('.preview-shop-btn').addClass('hide');
			$('.manage-coll-wrapper').addClass('hide');
			$('.manage-design-wrapper').addClass('hide');
			$('.manage-people-wrapper').addClass('hide');
			$('.manage-pages-wrapper').addClass('hide');
			$('.manage-sales-wrapper').addClass('hide');
			$('.manage-orders-wrapper').addClass('hide');
			$('.manage-shopsettings-wrapper').addClass('hide');
			$('.manage-shipping-wrapper').addClass('hide');
			$('.manage-policy-wrapper').addClass('hide');
			$('.manage-getpaid-wrapper').addClass('hide');
			$('.manage-taxes-wrapper').addClass('hide');
		}
		
		$('body').css({'overflow':'inherit'});

		load_page({
			browserNav: true,
			url: url,
			container: "#subcontent-wrapper",
			data: {body_wrapper:true,subcontent_wrapper:true}
		});
	}	
});

$(document).on('mouseover','.dd-select .outer', function(e) {
	$('.dd-select .opts div').removeClass('selected');
});

$(document).on('mouseleave','.dd-select', function(e) {
	var el = $(this);
	var val = el.attr('value');

	el.find('.opts div[value="'+val+'"]').addClass('selected');
});

$(document).on('click','.dd-select .opts div', function(e) {
	var val = $(this).attr('value');
	var text = $(this).html();
	var col2 = $(this).parents('.dd-select');

	col2.attr('value',val);
	col2.find('em').html(text);
	col2.find('.outer').hide();
	$(this).addClass('selected');

	setTimeout(function(){ col2.find('.outer').attr('style','') }, 400);
});

$(document).on('mouseover','.header-caption-action-btn .search-btn', function() {
	var el = $(this);
	var select = el.parent().find('.dd-select');
	var input = el.parent().find('.search-input');

	select.addClass('hide');
	input.removeClass('hide');

	input.focus();
});

$(document).on('blur','.header-caption-action-btn .search-input', function() {
	var el = $(this);
	var select = el.parent().find('.dd-select');
	var input = el.parent().find('.search-input');

	if(input.val() != "") { return; }
	input.addClass('hide');
	select.removeClass('hide');
});

$(document).on('mouseover','.header-caption-action-btn .add-btn', function() {
	var el = $(this);
	var select = el.parent().find('.dd-select');
	var input = el.parent().find('.search-input');

	if(input.val() != "") { return; }
	input.addClass('hide');
	select.removeClass('hide');
});

$(document).on('click','.header-caption-action-btn .add-btn', function(e) {
	e.preventDefault(); e.stopImmediatePropagation();

	addproduct({
				data:{product_id:0, formType:'basic'},
				onSaved: function(data) {
							console.log(data);
						},
				beforeClose: function(){
					console.log('closing the window');
				}			
						
			});
});

$(document).on('change','#subcontent-wrapper select', function() {
	if($(this).hasClass('js-notenabled')) { return false; }
	if($(this).val() != "") {
		$(this).removeClass('color1').addClass('color2 selected');
	}
	else {
		$(this).removeClass('color2').addClass('color1').removeClass('selected');
	}
});	

$(document).on('touchstart click','#addproduct-btn', function(e) {
	e.preventDefault(); e.stopImmediatePropagation();
	var element = $(this),
		category_id = element.attr('data-cid');
	$('#addNewProdModal').foundation('reveal', 'close');	
	addproduct({
		loadType: "slideRight",
		data:{category_id: category_id, product_id:0, formType:'basic'},
		onSaved: function(data) {
			$('body').css('overflow','hidden');
			if(data.product_id){
				var data_zindex = parseInt($('.coll-prod-list li.admin-prod-list:first-child').attr('data-z-index')) + 1;
				SSAjax({
					url: '/admin/catalog_actions?action=load_single_product',
					data: {isAjax:true,product_id:data.product_id,zindex:data_zindex,addfrom_modal:true},
					success: function(result){
						var list = '<li data-prod-id="' + data.product_id + '" data-z-index="' + data_zindex + '" class="left small-4 large-4 admin-prod-list pzindex' + data.product_id + '">'+ result +'</li>';
						$('.coll-prod-list li.admin-prod-list:first-child').before(list);
						reloadPage({link:'/admin/catalog_actions?action=reload_collections', clear_wrapper: '.m-catalog-list .catagory-list', wrapper: '.catagory-list', preloader: '.mcatalogloader'});
						$('body').css('overflow','hidden');
						return;
					}
				});
			}
			
		},
		beforeClose: function(){
			$('body').css('overflow','hidden');
		}
	});
});

$(document).on('click', '.mod-close', function(){
	if($('.typelist').val() == 'product'){ closeModal(); }
	if($('.typelist').val() == 'collection'){ closeModal2(); }
});
$(document).on('click', '.blog-mod-close', function(){
	$('.blog-modal2 .mod-wrapper').html('');
	closeModal3();
});

$(document).on('click', '.vproduct', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	view_product_details($.parseJSON($(this).attr('data')));
});

$(document).on('click', '.see-more-desc', function(){
	$('.more-desc').removeClass('hide');
});
$(document).on('click', '.info-close', function(){
	$('.more-desc').addClass('hide');
});

$(document).on('click', '#edit-external-product', function(e){
	e.stopImmediatePropagation();
	var modal_id = '#product-editor-modal';
	$(modal_id + ' .mod-wrapper').html('');
	openCustomModal(modal_id);
	
	var form_data = {};
	form_data.isAjax = true;
	form_data.pid = $(this).attr('data-pid');
	form_data.cid = $(this).attr('data-cid');
	$(modal_id + ' .mod-loader-wrapper').show();
	SSAjax({
		data: form_data,
		url: "/admin/catalog_actions?action=edit_external_product",
		success: function(response){
			$(modal_id + ' .mod-wrapper').html(response);
			$(modal_id + ' .mod-loader-wrapper').hide();
		}
	});
});

$(document).on("click","#product-editor-modal .entry-wrapper .color-wrapper li", function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	$("#product-editor-modal .entry-wrapper .color-wrapper li").removeClass('selected');
	$(this).addClass('selected');
	$(this).parents('.color-wrapper').find('input').val($(this).attr('data-color'));
});

$(document).on("click","#product-editor-modal .entry-wrapper .dept-wrapper li", function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	$("#product-editor-modal .entry-wrapper .dept-wrapper li").removeClass('selected');
	$(this).addClass('selected');
	$(this).parents('.dept-wrapper').find('input').val($(this).attr('department_id'));
});

$(document).on('change', '#product-editor-modal .entry-wrapper .image-wrapper input', function(e){
	var input = this,
		element = $(this),
		li = element.parents('li'),
		index = $('#product-editor-modal .entry-wrapper .image-wrapper li').index(li);

	if (input.files && input.files[0]) {

		var ftype = input.files[0].type;
		var fname = input.files[0].name.split('.');
		var flength = fname.length-1;
		var fext = fname[flength];
		var element = $(this);
		//var fname = fname.spli

		if (!ftype.match('image.*') && !check_allowed_image_filetype(ftype,fext)) {
			return;
		}

		li.addClass('uploading');
		$('#product-editor-modal .image-wrapper .uploading-status').removeClass('hide');

		var reader = new FileReader();

		reader.onload = function (e) {
			if(li.find('img').length == 0) {
				li.find('.thumb-wrapper').append('<img src="'+e.target.result+'" />');
			}
			else {
				li.find('.thumb-wrapper img').attr('src', e.target.result);
			}
			upload_shop_product_image(element,input.files[0].name);
		};

		reader.readAsDataURL(input.files[0]);
	}
});

$(document).on("click","#product-editor-modal .image-wrapper .action-buttons div.isdefault", function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	var pid = $('#product-editor-modal form').attr('pid'),
		cid = $('.external-prod-cid').val(),
		sort_order = $(this).parents('li').attr('sort_order'),
		divs = $('#product-editor-modal .image-wrapper .action-buttons div.isdefault');

	divs.removeClass('yes');
	divs.parents('li').removeClass('active');

	$(this).parents('li').addClass('active');
	$(this).addClass('yes');
	SSAjax({
			url: '/admin/catalog_actions?action=set_product_imagedefault',
			data: {isAjax:true,pid:pid,sort_order:sort_order},
			success: function(response){
				var _data = $.parseJSON(response);
				if($('.m-catalog-list-box').length > 0 ){
					if(_data.success){
						var modal_id = '#category-product-modal';
						$(modal_id + ' .mod-wrapper').html('');
						reloadPage({link:'/admin/catalog_actions?action=reload_collections', clear_wrapper: '.m-catalog-list .catagory-list', wrapper: '.catagory-list', preloader: '.mcatalogloader'});
						reloadPage({collection_id: cid, link:'/admin/catalog_actions?action=collection_products', wrapper: modal_id + ' .mod-wrapper', preloader: modal_id + '.mod-loader-wrapper'});
						return false;
					}
				}
			}
		});
});

$(document).on("click","#product-editor-modal .image-wrapper .action-buttons div.trash", function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	var pid = $('#product-editor-modal form').attr('pid'),
		sort_order = $(this).parents('li').attr('sort_order');

	$(this).parents('li').addClass('add-image').find('img').remove();
	SSAjax({
			url: '/admin/catalog_actions?action=delete_product_image',
			data: {isAjax:true,pid:pid,sort_order:sort_order,deleted:"Y"},
			success: function(response){

			}
		});
});

$(document).on('click', '.update-prod-btn', function(e){
	e.stopImmediatePropagation();
	$('.update-coll-prod-btn').trigger('click');
});

$(document).on("submit","#product-editor-modal .entry-wrapper form", function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	if(store.get('user_id') == 0 || typeof(store.get('user_id')) == "undefined") {
		signin_modal();
		return;
	}

	var form = $(this), hasError = false;
	var pname = $.trim(form.find('input[name="product_name"]').val()),
		price = $.trim(form.find('input[name="price"]').val()),
		sale_price = $.trim(form.find('input[name="sale_price"]').val()),
		color = $.trim(form.find('input[name="color"]').val()),
		dept_id = $.trim(form.find('input[name="department_id"]').val()),
		cid = $.trim(form.find('input[name="cid"]').val()),
		pdesc = $.trim(form.find('textarea[name="product_desc"]').val()),
		is_external = $.trim(form.find('input[name="is_external"]').val()),
		container = $('#product-editor-modal .topwrapper');
	
	if(is_external == ""){
		var link = '/admin/catalog_actions?action=save_product';
	}else{
		var link = '/admin/catalog_actions?action=save_exproduct';
	}
	
	var data = {isAjax:true,product_name:pname,price:price,sale_price:sale_price,color:color,department_id:dept_id,product_desc:pdesc};
	var i = 0;
	$.each(data, function (key, value) {
		form.find('[name="'+key+'"]').parent().removeClass('error');
		if(value == "" && form.find('[name="'+key+'"]').attr('required')) {
			hasError = true;
			form.find('[name="'+key+'"]').parent().addClass('error');
			if(i == 0) {
				form.find('[name="'+key+'"]').focus();
			}
			i++;
		}
	});	

	if(hasError) { return; }	

	$.extend(data,{pid:form.attr('pid')});
	$('#product-editor-modal .mod-loader-wrapper').show();
	$('#product-editor-modal .shopstyxloader div').html('SAVING...');
	$('#product-editor-modal .product-editor').addClass('hide');
	SSAjax({
		url: link,
		data: data,
		success: function(response){
			response = $.parseJSON(response);
			if($('.admin-prod-list').length > 0){
				SSAjax({
					url: '/admin/catalog_actions?action=load_single_product',
					data: {isAjax:true,product_id:response.pid,zindex:$('.pzindex'+response.pid).attr('data-z-index')},
					success: function(result){
						$('.pzindex'+response.pid).html(result);
						closeCustomModal('#product-editor-modal');
						return;
					}
				});
			}else{
			
				if(is_external == ""){
					if(response.hasOwnProperty('status')) {
						if(response.status == "success") {
							if((container.attr('color') && response.color != container.attr('color')) ||
								(container.attr('department_id') && response.department_id != container.attr('department_id')) ||
								(container.attr('price_from') && response.price < container.attr('price_from') || response.price > container.attr('price_to'))
							 ) {
								$('#claimed-shop-product-list-ID'+response.pid).remove();
								$('#product-editor-modal .bottom-content .inner').removeClass('hide');
								$('#product-editor-modal .product-editor').removeClass('hide');
								$('#product-editor-modal .mod-loader-wrapper').hide();
								$('#product-editor-modal .shopstyxloader div').html('LOADING...');
								$("#product-editor-modal").scrollTop($('.shop-product-list li.products').eq(form.attr('product_index')).position().top);
							}
							else {
								var modal_id = '#category-product-modal';
								$(modal_id + ' .mod-wrapper').html('');
								reloadPage({collection_id: cid, link:'/admin/catalog_actions?action=collection_products', wrapper: modal_id + ' .mod-wrapper', preloader: '.mod-loader-wrapper'});
								closeCustomModal2('#product-editor-modal');
							}
						}
					}
				}
			}
		}
	});
});

$(document).on('click', '#add-collection-btn, #edit-collection-btn', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	var modal_id = '#category-modal';
	$(modal_id + ' .mod-wrapper').html('');
	openCustomModal(modal_id);
	var $form = $(this),
		form_data = {};
	form_data.isAjax = true;
	form_data.collection_id = $(this).attr('data-cid');
	showModalLoader();
	$('#category-modal .mod-loader-wrapper .shopstyxloader div').html('LOADING...');
	SSAjax({
		data: form_data,
		url: "/admin/catalog_actions?action=manage_collection",
		success: function(response){
			$(modal_id + ' .mod-wrapper').html(response);
			hideModalLoader();
			$('.mod-category-loader-wrapper').addClass('hide');
		}
	});
});

$(document).on('click', '#edit-collection', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	var modal_id = '#category-modal2';
	$(modal_id + ' .mod-wrapper').html('');
	var top_margin = parseInt($('.category-product-modal .prod-desc').height()) + 82;
	$('.category-modal2').css('margin-top', top_margin + 'px');
	openCustomModal(modal_id);
	$('#category-modal2 .mod-category-loader-wrapper .shopstyxloader div').html('LOADING...');
	var $form = $(this),
		form_data = {};
	form_data.isAjax = true;
	form_data.collection_id = $(this).attr('data-cid');
	$('.mod-category-loader-wrapper').removeClass('hide');
	SSAjax({
		data: form_data,
		url: "/admin/catalog_actions?action=edit_collection",
		success: function(response){
			$(modal_id + ' .mod-wrapper').html(response);
			$('.mod-category-loader-wrapper').addClass('hide');
		}
	});
});

$(document).on('click', '.update-cat-btn, .create-cat-btn', function(e){
	e.preventDefault();
	e.stopImmediatePropagation();
	$(this).parents("form").trigger("submit");
	$('.update-cat-btn').attr('disabled', 'disabled');
	return false;
});

$(document).on('submit', '#updateCollectionForm, #newCollectionForm', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	var $form = $(this),
		url = $form.attr( 'action' );
		form_data = {};
	form_data.collection_name = $.trim($form.find( 'input[name="collection_name"]' ).val());
	form_data.description = $.trim($form.find( 'textarea[name="description"]' ).val());
	form_data.collection_id = $.trim($form.find( 'input[name="coll_id"]' ).val());
	form_data.form_type = $.trim($form.find( 'input[name="form_type"]' ).val());
	form_data.isAjax = true;
	
	if(form_data.form_type == 'add' || form_data.form_type == 'editing'){
		$('#category-modal .mod-loader-wrapper').show();
		$('#category-modal .mod-loader-wrapper .shopstyxloader div').html('SAVING...');
		$('#category-modal .modal-content').html('');
	}
	if(form_data.form_type == 'edit'){
		$('#category-modal2 .mod-category-loader-wrapper').removeClass('hide');
		$('#category-modal2 .mod-category-loader-wrapper .shopstyxloader div').html('SAVING...');
		$('#category-modal2 .modal-content').html('');
	}
	SSAjax({
		data: form_data,
		url: url,
		success: function(response){
			var _data = $.parseJSON(response);
			
			if(form_data.form_type == 'edit'){
				collection_id = $('.main-category-hidden').val();
				var modal_id = '#category-product-modal';
				$(modal_id + ' .mod-wrapper').html('');
				openCustomModal(modal_id);
				if(_data.status == 'success'){
					$('#display_message').html('');
					reloadPage({link:'/admin/catalog_actions?action=reload_collections', clear_wrapper: '.m-catalog-list .catagory-list', wrapper: '.catagory-list', preloader: '.mcatalogloader'});
					reloadPage({collection_id: collection_id, link:'/admin/catalog_actions?action=collection_products', wrapper: modal_id + ' .mod-wrapper', preloader: '.mod-loader-wrapper'});
					return false;
				}
			}
			
			if(form_data.form_type == 'add' || form_data.form_type == 'editing'){
				$('#category-modal .mod-loader-wrapper').hide();
				$('.create-cat-btn').removeAttr('disabled');

				if(_data.status == 'success'){
					var modal_id = '#category-product-modal';
					$(modal_id + ' .mod-wrapper').html('');
					closeCustomModal2('#category-modal');
					if(form_data.form_type == 'add'){
						openCustomModal(modal_id);
						$('#category-modal .modal-content').html('');
						reloadPage({collection_id: _data.collection_id, link:'/admin/catalog_actions?action=collection_products', wrapper: modal_id + ' .mod-wrapper', preloader: '.mod-loader-wrapper'});
					}
					$('body').css({'overflow':'inherit'});
					reloadPage({link:'/admin/catalog_actions?action=reload_collections', clear_wrapper: '.m-catalog-list .catagory-list', wrapper: '.catagory-list', preloader: '.mcatalogloader'});
					return false;
				}
			}
		}
	});
	
	return false;
});

$(document).on('click', '#delete-collection', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	var $form = $(this),
		form_data = {},
		parent_catid = $('.parent-cid').val(),
		modal_id = '#category-product-modal';
		
	form_data.isAjax = true;
	form_data.collection_id = $(this).attr('data-cid');
	
	$('.mod-loader-wrapper').show();
	$('.mod-loader-wrapper .shopstyxloader div').html('DELETING...');
	$('.modal-content .product-list').hide();
	SSAjax({
		data: form_data,
		url: "/admin/catalog_actions?action=delete_collection",
		success: function(response){
			var _datas = $.parseJSON(response);
			if( _datas.status == "has products"){
				$('.mod-loader-wrapper').hide();
				$('.mod-loader-wrapper .shopstyxloader div').html('LOADING...');
				$('.modal-content .product-list').show();
				alert(_datas.msg);
				return false;
			}
			reloadPage({link:'/admin/catalog_actions?action=reload_collections', clear_wrapper: '.m-catalog-list .catagory-list', wrapper: '.catagory-list', preloader: '.mcatalogloader'});
			$('.cp-modal-close').trigger('click');
		}
	});
});

// $(document).on('click', '#delete-category', function(e){
	// e.preventDefault(); e.stopImmediatePropagation();
	// var $form = $(this),
		// form_data = {},
		// parent_catid = $('.parent-cid').val(),
		// modal_id = '#category-product-modal',
		// category_id = parseInt($('.product-list .sub-nav .active').attr('data-cid'));
		// if(category_id == 0){
			// category_id = $('.main-category-hidden').val();
		// }
		
	// form_data.isAjax = true;
	// form_data.order_id = $(this).attr('data-cid');
	// form_data.parent_order_id = $(this).attr('data-pcid');
	// if(parseInt(form_data.parent_order_id) > 0){
		// //$(modal_id + ' .mod-wrapper').html('');
	// }
	// $('.mod-loader-wrapper').show();
	// SSAjax({
		// data: form_data,
		// url: "/admin/catalog_actions?action=delete_category",
		// success: function(response){
			// var _datas = $.parseJSON(response);
			// if( _datas.status == "has products"){
				// $('.mod-loader-wrapper').hide();
				// alert(_datas.msg);
				// return false;
			// }
			// if( _datas.status == "failed"){
				// $('.mod-loader-wrapper').hide();
				// alert('Delete first the sub category before deleting the main category...');
				// return false;
			// }
			// reloadPage({link:'/admin/catalog_actions?action=reload_catalog', wrapper: '.catagory-list', preloader: '.mcatalogloader'});
			// if(parseInt(form_data.parent_order_id) > 0){
				// reloadPage({category_id: parent_catid, link:'/admin/catalog_actions?action=category_products', wrapper: modal_id + ' .mod-wrapper', preloader: '.mod-loader-wrapper'});
			// }
			// if(parseInt(form_data.parent_order_id) == 0){ $('.cp-modal-close').trigger('click'); }
			
		// }
	// });
// });

$(document).on('touchstart click', '.category-list-box', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	var modal_id = '#category-product-modal';
	$(modal_id + ' .mod-wrapper').html('');
	openCustomModal(modal_id);
	reloadPage({collection_id:$(this).attr('data-cid'), link:'/admin/catalog_actions?action=collection_products', wrapper: modal_id + ' .mod-wrapper', preloader: '.mod-loader-wrapper'});
});

$(document).on('touchstart click', '._search .search-btn', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	var collection_id = $('.main-category-hidden').val();
	var searchtext = $.trim($('._search input').val());
	if( searchtext == ''){
		return false;
	}
	
	var modal_id = '#category-product-modal';
	$(modal_id + ' .mod-wrapper').html('');
	openCustomModal(modal_id);
	reloadPage({searchtext: searchtext, collection_id: collection_id, link:'/admin/catalog_actions?action=collection_products', wrapper: modal_id + ' .mod-wrapper', preloader: '.mod-loader-wrapper'});
});

$(document).on('keypress', '._search input', function(e){
	e.stopImmediatePropagation();
	if (e.which == 13) {
		var collection_id = $('.main-category-hidden').val();
		var searchtext = $.trim($('._search input').val());
		if( searchtext == ''){
			return false;
		}
		
		var modal_id = '#category-product-modal';
		$(modal_id + ' .mod-wrapper').html('');
		openCustomModal(modal_id);
		reloadPage({searchtext: searchtext, collection_id: collection_id, link:'/admin/catalog_actions?action=collection_products', wrapper: modal_id + ' .mod-wrapper', preloader: '.mod-loader-wrapper'});
	}
});

$(document).on('touchstart click', '.mod-page-filter div', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	var collection_id = $('.main-category-hidden').val();
	var type = $(this).attr('data');
	var modal_id = '#category-product-modal';
	$(modal_id + ' .mod-wrapper').html('');
	openCustomModal(modal_id);
	reloadPage({collection_id: collection_id, type: type, link:'/admin/catalog_actions?action=collection_products', wrapper: modal_id + ' .mod-wrapper', preloader: '.mod-loader-wrapper'});
});

$(document).on('click', '.page-filter div', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	var collection_id = $('.main-category-hidden').val(),
		modal_id = '#category-product-modal',
		type = $(this).attr('data');
	$(modal_id + ' .mod-wrapper').html('');
	reloadPage({type:type, body_wrapper: true, subcontent_wrapper: true, link:'/admin/dashboard', clear_wrapper: '.m-catalog-list .catagory-list', wrapper: '.catagory-list', preloader: '.mcatalogloader'});

});
// $(document).on('keypress', '.coll-tabs div', function(e){
	// e.preventDefault(); e.stopImmediatePropagation();
	// var collection_id = $('.main-category-hidden').val(),
		// modal_id = '#category-product-modal';
	// $(modal_id + ' .mod-wrapper').html('');
	// openCustomModal(modal_id);
	// reloadPage({collection_id: collection_id, link:'/admin/catalog_actions?action=collection_products', wrapper: modal_id + ' .mod-wrapper', preloader: '.mod-loader-wrapper'});

// });

// $(document).on('click', '.collection-input', function(e){
	// e.preventDefault(); e.stopImmediatePropagation();
	// // var p_class = $(this).attr('data');
	// // $('.moveto' + p_class).parent('ul').find('input').removeAttr('checked');
	// // $('.moveto' + p_class).parent('ul').find('label').removeClass('checked');
	// // $('.moveto' + p_class).find('input').attr('checked','checked');
	// // $('.moveto' + p_class).find('label').addClass('checked');
	// // $('.moveto' + p_class).parents('.collection-list-wrapper').find('.coll-loader').show();
	
	// var modal_id = '#category-product-modal';
	// var form_data = {};
	// form_data.isAjax = true;
	// form_data.collection_id = $('.main-category-hidden').val();
	// // $('.mod-loader-wrapper .shopstyxloader div').html('DELETING...');
	// // $('.mod-loader-wrapper').show();
	// SSAjax({
		// data: form_data,
		// url: '/admin/catalog_actions?action=move_to_collection',
		// success: function(response){
			
		// }
	// });
	
	// //_move_to_collection
	
// });
$(document).on('touchstart click','#convert-product-btn', function(e) {
	e.preventDefault(); e.stopImmediatePropagation();
	convertproduct({
		data:{product_id: $(this).attr('data-pid'), command: 'edit', convert: true},
		onSaved: function(data){
			$('.prod-editor-close').trigger('click');
			$('body').css('overflow','hidden');
			if($('.category-product-modal .product-list').length > 0){
				collection_id = $('.main-category-hidden').val();
				var modal_id = '#category-product-modal';
				$(modal_id + ' .mod-wrapper').html('');
				reloadPage({collection_id:collection_id, link:'/admin/catalog_actions?action=collection_products', wrapper: modal_id + ' .mod-wrapper', preloader: '.mod-loader-wrapper'});
				if($('.category-list-box').length > 0){
					reloadPage({link:'/admin/catalog_actions?action=reload_collections', clear_wrapper: '.m-catalog-list .catagory-list', wrapper: '.catagory-list', preloader: '.mcatalogloader'});
				}
			}
		},
		beforeClose: function(){
			$('.prod-editor-close').trigger('click');
			$('body').css('overflow','hidden');
		}
	});
});

$(document).on('touchstart click','#edit-product-btn', function(e) {
	e.preventDefault(); e.stopImmediatePropagation();
	var modal_id = '#category-product-modal',
		category_id = $(this).attr('data-cid'),
		load_cat = $(this).attr('data-load-cat'),
		pid = $(this).attr('data-pid'),
		level = $('.level').val();
	addproduct({
		data:{product_id: $(this).attr('data-pid')},
		onSaved: function(data) {
			$('body').css('overflow','hidden');
			if(data.product_id){
				if($('.admin-prod-list').length > 0 || $('.category-product-modal .product-list').length > 0){
					SSAjax({
						url: '/admin/catalog_actions?action=load_single_product',
						data: {isAjax:true,product_id:data.product_id,zindex:$('.pzindex'+data.product_id).attr('data-z-index')},
						success: function(result){
							$('.pzindex'+data.product_id).html(result);
							if ( $('.category-product-modal').css('display') == 'block'){
							   $('body').css('overflow','hidden');
							}
							return;
						}
					});

					// reload product details modal
					if($('.dashboard-modal').css('display') != "none") {
						view_product_details({"uid":data.user_id,"sid":data.store_id,"pid":data.product_id,"type":"0"});
					}
				}else{
					reloadPage({category_id:load_cat, link:'/admin/catalog_actions?action=category_products', wrapper: modal_id + ' .mod-wrapper', preloader: '.mod-loader-wrapper', level:level});
				}
			}
		},
		beforeClose: function(){
			$('body').css('overflow','hidden');
		}
	});
});

$(document).on('touchstart click','#product-edit-btn', function(e) {
	e.preventDefault(); e.stopImmediatePropagation();
	var modal_id = '#category-product-modal',
		load_cat = parseInt($('.product-list .sub-nav .active').attr('data-cid'));
		if(load_cat == 0){
			load_cat = $('.main-category-hidden').val();
		}
	addproduct({
		data:{product_id: $(this).attr('data-pid')},
		onSaved: function(data) {
				$('body').css('overflow','hidden');
				$('.reload-product').trigger('click');
				reloadPage({category_id:load_cat, link:'/admin/catalog_actions?action=category_products', wrapper: modal_id + ' .mod-wrapper', preloader: '.mod-loader-wrapper'});
				reloadPage({link:'/admin/catalog_actions?action=reload_catalog', wrapper: '.catagory-list', preloader: '.mcatalogloader'});
			},
		beforeClose: function(){
			$('body').css('overflow','hidden');
		}
	});
});

$(document).on('touchstart click', '#remove-product-btn', function(e){
	e.stopImmediatePropagation();
	
	var $this = $(this),
		form_data = {};
		
	form_data.isAjax = true;
	form_data.collection_id = $(this).attr('data-cid');
	form_data.product_id = $(this).attr('data-pid');
	form_data.store_id = $(this).attr('data-sid');
	
	$this.parents('.collection-product').find('.bg-undo').addClass('hide');
	$this.parents('.coll-prod-img-wrapper').addClass('hide');
	$this.parents('.collection-product').find('.bg-loader span').html('REMOVING...');
	$this.parents('.collection-product').find('.bg-loader').removeClass('hide');
	
	SSAjax({
		data: form_data,
		url: '/admin/catalog_actions?action=remove_collection_product',
		success: function(response){
			var _datas = $.parseJSON(response);
			if(_datas.status == 'success'){
				var prod_count = parseInt($('#p-items').val()) - 1,
					item = (prod_count > 1)? '(' + prod_count + ' items)': '(' + prod_count + ' item)',
					item_span =  $this.parents('.mod-wrapper').find('.product-items');
				item_span.html(item);
				$('#p-items').val(prod_count);
				$this.parents('.admin-prod-list').remove();
				reloadPage({link:'/admin/catalog_actions?action=reload_collections', clear_wrapper: '.m-catalog-list .catagory-list', wrapper: '.catagory-list', preloader: '.mcatalogloader'});
			}else{
				$this.parents('.collection-product').find('.bg-loader').addClass('hide');
				$this.parents('.collection-product').find('.bg-undo').addClass('hide');
				$this.parents('.coll-prod-img-wrapper').removeClass('hide');
			}
		}
	});
});

$(document).on('touchstart click', '#delete-product-btn', function(e){
	e.stopImmediatePropagation();
	// var product_name = $(this).parents('.coll-more-info').find('.prod-name').html();

	// if (! confirm("Are you sure you want to delete " + product_name + " ?")) {
		// return false;
	// }
	
	var $this = $(this),
		form_data = {};
		
	form_data.isAjax = true;
	form_data.collection_id = $(this).attr('data-cid');
	form_data.product_id = $(this).attr('data-pid');
	
	$this.parents('.collection-product').find('.bg-undo').addClass('hide');
	$this.parents('.coll-prod-img-wrapper').addClass('hide');
	$this.parents('.collection-product').find('.bg-loader span').html('DELETING...');
	$this.parents('.collection-product').find('.bg-loader').removeClass('hide');
	
	SSAjax({
		data: form_data,
		url: '/admin/catalog_actions?action=del_collection_product',
		success: function(response){
			var _datas = $.parseJSON(response);
			if(_datas.status == 'success'){
				if(!$this.hasClass('dashboad-plist')){
					var prod_count = parseInt($('#p-items').val()) - 1,
						item = (prod_count > 1)? '(' + prod_count + ' items)': '(' + prod_count + ' item)',
						item_span =  $this.parents('.mod-wrapper').find('.product-items');
					item_span.html(item);
					$('#p-items').val(prod_count);
					
					reloadPage({link:'/admin/catalog_actions?action=reload_collections', clear_wrapper: '.m-catalog-list .catagory-list', wrapper: '.catagory-list', preloader: '.mcatalogloader'});
				}
				$this.parents('.collection-product').find('.bg-loader').addClass('hide');
				$this.parents('.collection-product').find('.bg-undo').removeClass('hide');
			}else{
				$this.parents('.collection-product').find('.bg-loader').addClass('hide');
				$this.parents('.collection-product').find('.bg-undo').addClass('hide');
				$this.parents('.coll-prod-img-wrapper').removeClass('hide');
			}
		}
	});
});

$(document).on('touchstart click', '#product-delete-btn', function(e){
	e.stopImmediatePropagation();
	var product_name = $(this).parents('.prod-title-wrapper').find('.prod-title h2').html();

	if (! confirm("Are you sure you want to delete " + product_name + " ?")) {
		return false;
	}
	
	var $this = $(this),
		form_data = {};
		
	form_data.isAjax = true;
	form_data.collection_id = $(this).attr('data-cid');
	form_data.product_id = $(this).attr('data-pid');
	
	$this.parents('.product-info-modal').find('.shopstyxloader div').html('DELETING...');
	$this.parents('.product-info-modal').find('.prod-content-wrapper').addClass('hide');
	$this.parents('.product-info-modal').find('.mod-loader-wrapper').show();
	
	SSAjax({
		data: form_data,
		url: '/admin/catalog_actions?action=del_collection_product',
		success: function(response){
			var _datas = $.parseJSON(response);
			if(_datas.status == 'success'){
				$this.parents('.product-info-modal').find('.mod-close').trigger('click');
				$('body').css('overflow','hidden');
				var modal_id = '#category-product-modal';
				$(modal_id + ' .mod-wrapper').html('');
				reloadPage({collection_id:form_data.collection_id, link:'/admin/catalog_actions?action=collection_products', wrapper: modal_id + ' .mod-wrapper', preloader: '.mod-loader-wrapper'});
				reloadPage({link:'/admin/catalog_actions?action=reload_collections', clear_wrapper: '.m-catalog-list .catagory-list', wrapper: '.catagory-list', preloader: '.mcatalogloader'});
			}else{
				$this.parents('.product-info-modal').find('.shopstyxloader div').html('LOADING...');
				$this.parents('.product-info-modal').find('.mod-loader-wrapper').hide();
				$this.parents('.product-info-modal').find('.prod-content-wrapper').removeClass('hide');
			}
		}
	});
});

$(document).on('touchstart click', '#undo-delete-product-btn', function(e){
	e.stopImmediatePropagation();
	var $this = $(this),
		form_data = {};
		
	form_data.isAjax = true;
	form_data.collection_id = $(this).attr('data-cid');
	form_data.product_id = $(this).attr('data-pid');
	
	$this.parents('.collection-product').find('.bg-undo').addClass('hide');
	$this.parents('.coll-prod-img-wrapper').addClass('hide');
	$this.parents('.collection-product').find('.bg-loader span').html('UNDO...');
	$this.parents('.collection-product').find('.bg-loader').removeClass('hide');
	
	SSAjax({
		data: form_data,
		url: '/admin/catalog_actions?action=undo_del_collection_product',
		success: function(response){
			var _data = $.parseJSON(response);

			if(_data.status == "failed") {
				alert('Your current plan (starter) has reached it\'s inventory limit (max: '+_data.max_products+').\nYou can increase the limit by upgrading your account.')
				$this.parents('.collection-product').find('.bg-loader').addClass('hide');
				return;
			}

			if(!$this.hasClass('dashboad-plist')){
				var prod_count = parseInt($('#p-items').val()) + 1,
					item = (prod_count > 1)? '(' + prod_count + ' items)': '(' + prod_count + ' item)',
					item_span =  $this.parents('.mod-wrapper').find('.product-items');
				item_span.html(item);
				$('#p-items').val(prod_count);
				
				reloadPage({link:'/admin/catalog_actions?action=reload_collections', clear_wrapper: '.m-catalog-list .catagory-list', wrapper: '.catagory-list', preloader: '.mcatalogloader'});
			}
			$this.parents('.collection-product').find('.bg-loader').addClass('hide');
			$this.parents('.collection-product').find('.bg-undo').addClass('hide');
			$this.parents('.coll-prod-img-wrapper').removeClass('hide');
		}
	});
	return false;
});


$(document).on("click",".collection-list-wrapper .create-collection-wrapper a", function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	$(this).parent().parent().addClass('hide').parent().find('.collection-entry').removeClass('hide');
});	
$(document).on("mouseenter",".common-move-to-btn", function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	$(this).find('.collection-entry').addClass('hide');
	$(this).find('.collection-list-wrapper').removeClass('hide');
});	
$(document).on("click",".common-move-to-btn .collection-entry span", function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	$(this).parent().parent().addClass('hide').find('input,textarea').val('').html('');
	$('.error-collection').addClass('hide');
});

$(document).on("submit",".common-move-to-btn .create-collection-box form", function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	if(store.get('user_id') == 0 || typeof(store.get('user_id')) == "undefined") {
		signin_modal();
		return false;
	}
	
	var el = $(this),
		form_data = {},
		error_el = el.find('.error-collection');
		
	form_data.product_name = $.trim(el.find( 'input[name="product_name"]' ).val());
	form_data.collection_name = $.trim(el.find( 'input[name="collection_name"]' ).val());
	form_data.description = $.trim(el.find('textarea').val());
	form_data.puid = $.trim(el.find( 'input[name="puid"]' ).val());
	form_data.pid = $.trim(el.find( 'input[name="pid"]' ).val());
	form_data.from_cid = $.trim(el.find( 'input[name="from_cid"]' ).val());
	form_data.from_sid = $.trim(el.find( 'input[name="from_sid"]' ).val());
	form_data.form_type = 'add';
	form_data.isAjax = true;
	
	if(form_data.collection_name == "") {
		error_el.removeClass('hide');
		return false;
	}
	else {
		error_el.addClass('hide');
	}

	el.parents('.collection-product').find('.collection-list-wrapper').addClass('hide');
	el.parents('.collection-product').find('.coll-prod-img-wrapper').addClass('hide');
	el.parents('.collection-product').find('.bg-loader').removeClass('hide');
	el.parents('.collection-product').find('.collection-entry').addClass('hide');
	el.parents('.collection-product').find('.bg-loader span').html('Moving this product to ' + form_data.collection_name + '...');

	SSAjax({
		data: form_data,
		url: root + 'admin/catalog_actions?action=add_collection',
		success: function(response){
			var _data = $.parseJSON(response);
			if(_data.status == 'success'){
				form_data.move_data = '{"to_sid":"' + _data.store_id + '", "to_cid":"' + _data.collection_id + '", "from_sid":"' + form_data.from_sid + '", "from_cid":"' + form_data.from_cid + '", "pid":"' + form_data.pid + '", "puid":"' + form_data.puid + '"}';
				SSAjax({
					data: form_data,
					url: '/admin/catalog_actions?action=add_to_collection',
					success: function(response){
						el.parents('.collection-product').parent().fadeOut(300, function(){ 
							$(this).remove();
							//var modal_id = '#category-product-modal';
							//$(modal_id + ' .mod-wrapper').html('');
							//$('#category-modal .modal-content').html('');
							//reloadPage({collection_id: form_data.from_cid, link:'/admin/catalog_actions?action=collection_products', wrapper: modal_id + ' .mod-wrapper', preloader: '.mod-loader-wrapper'});
							$('.prod-btns .move-to-collection').attr('data-reload_collection','0');
							reloadPage({link:'/admin/catalog_actions?action=reload_collections', clear_wrapper: '.m-catalog-list .catagory-list', wrapper: '.catagory-list', preloader: '.mcatalogloader'});
						});
					}
				});
				return false;
			}
		}
	});
});

$(document).on("submit",".common-move-to-btn .create-new-collection-box form", function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	if(store.get('user_id') == 0 || typeof(store.get('user_id')) == "undefined") {
		signin_modal();
		return false;
	}
	
	var el = $(this),
		form_data = {},
		error_el = el.find('.error-collection');
		
	form_data.product_name = $.trim(el.find( 'input[name="product_name"]' ).val());
	form_data.collection_name = $.trim(el.find( 'input[name="collection_name"]' ).val());
	form_data.description = $.trim(el.find('textarea').val());
	form_data.puid = $.trim(el.find( 'input[name="puid"]' ).val());
	form_data.pid = $.trim(el.find( 'input[name="pid"]' ).val());
	form_data.from_cid = $.trim(el.find( 'input[name="from_cid"]' ).val());
	form_data.from_sid = $.trim(el.find( 'input[name="from_sid"]' ).val());
	form_data.form_type = 'add';
	form_data.isAjax = true;
	
	if(form_data.collection_name == "") {
		error_el.removeClass('hide');
		return false;
	}
	else {
		error_el.addClass('hide');
	}
	el.parents('.common-move-to-btn').find('.collection-entry').addClass('hide');
	el.parents('.common-move-to-btn').find('.collection-list-wrapper').removeClass('hide');
	el.parents('.common-move-to-btn').find('.coll-loader').show();
	el.parents('.common-move-to-btn').removeClass('assign-to').addClass('move-to').find('.move-lbl').html('Move to...');
	el.parents('.collection-product').find('.collection-lbl').html(form_data.collection_name);
	SSAjax({
		data: form_data,
		url: root + 'admin/catalog_actions?action=add_collection',
		success: function(response){
			var _data = $.parseJSON(response);
			if(_data.status == 'success'){
				form_data.move_data = '{"to_sid":"' + _data.store_id + '", "to_cid":"' + _data.collection_id + '", "from_sid":"' + form_data.from_sid + '", "from_cid":"' + form_data.from_cid + '", "pid":"' + form_data.pid + '", "puid":"' + form_data.puid + '"}';
				SSAjax({
					data: form_data,
					url: '/admin/catalog_actions?action=add_to_collection',
					success: function(response){
						$('.prod-btns .common-move-to-btn').attr('data-reload_collection','0');
						var $this = el.parents('.common-move-to-btn');
							form_data2 = {};
						
						$this.attr('data-reload_collection','1');
						form_data2.isAjax = true;
						var j_data = $.parseJSON($this.attr('data'));
						$.extend(form_data2,j_data);
						form_data2.from_cid = $this.find('.from-cid').val();
						form_data2.with_selection = true;
						
						SSAjax({
							data: form_data2,
							url: '/admin/catalog_actions?action=load_collections',
							success: function(response){
								$this.find('ul').html(response);
								$this.find('.coll-loader').hide();
							}
						});
					}
				});
				return false;
			}
		}
	});
});

$(document).on("mouseover",".prod-btns .move-to-collection, .prod-btns .moveto-btn", function(e){
	var $this = $(this),
		form_data = {};
		
	if(parseInt($(this).attr('data-reload_collection')) == 0){
		$this.attr('data-reload_collection','1');
		form_data.isAjax = true;
		var j_data = $.parseJSON($this.attr('data'));
		$.extend(form_data,j_data);
		if($this.hasClass('moveto-btn')){
			form_data.from_cid = $this.find('.from-cid').val();
			form_data.with_selection = true;
		}
		$this.find('.coll-loader').show();
		SSAjax({
			data: form_data,
			url: '/admin/catalog_actions?action=load_collections',
			success: function(response){
				$this.find('ul').html(response);
				$this.find('.coll-loader').hide();
			}
		});
	}
});

$(document).on("click",".move-to-collection .collection-list-wrapper li input", function(e){
	e.stopImmediatePropagation();
	if(store.get('user_id') == 0 || typeof(store.get('user_id')) == "undefined") {
		signin_modal();
		return;
	}
	
	var $this = $(this),
		form_data = {};
		
	form_data.isAjax = true;
	form_data.move_data = $(this).val();
	var j_data = $.parseJSON(form_data.move_data);
	$this.parents('.collection-product').find('.collection-list-wrapper').addClass('hide');
	$this.parents('.collection-product').find('.coll-prod-img-wrapper').addClass('hide');
	$this.parents('.collection-product').find('.bg-loader').removeClass('hide');
	$this.parents('.collection-product').find('.bg-loader span').html('Moving this product to ' + j_data.collection_name + '...');
	SSAjax({
		data: form_data,
		url: '/admin/catalog_actions?action=add_to_collection',
		success: function(response){
			$this.parents('.collection-product').parent().fadeOut(300, function(){
				var prod_count = parseInt($this.parents('.coll-prod-list').children('li').length) - 1;
				var item = (prod_count > 1)? '(' + prod_count + ' items)': '(' + prod_count + ' item)';
				var item_span =  $this.parents('.mod-wrapper').find('.product-items');
				$(this).remove();
				
				reloadPage({link:'/admin/catalog_actions?action=reload_collections', clear_wrapper: '.m-catalog-list .catagory-list', wrapper: '.catagory-list', preloader: '.mcatalogloader'});
				
				if(prod_count <= 0){
					var modal_id = '#category-product-modal';
					$(modal_id + ' .mod-wrapper').html('');
					reloadPage({collection_id: j_data.from_cid, link:'/admin/catalog_actions?action=collection_products', wrapper: modal_id + ' .mod-wrapper', preloader: '.mod-loader-wrapper'});
				}else{
					item_span.html(item);
				}
			});
		}
	});
	
	
});

$(document).on("click",".move-to .collection-list-wrapper li input.radio-prod-coll", function(e){
	e.stopImmediatePropagation();
	if(store.get('user_id') == 0 || typeof(store.get('user_id')) == "undefined") {
		signin_modal();
		return;
	}
	
	var $this = $(this),
		form_data = {};
		
	form_data.isAjax = true;
	form_data.move_data = $(this).val();
	var j_data = $.parseJSON(form_data.move_data);
	
	var from_cid = $this.parents('.coll-list').find('.from-cid').val();
	form_data.from_cid = from_cid;
	$('.from-cid' + j_data.pid).val(j_data.to_cid);
	$this.parents('.collection-product').find('.collection-lbl').html(j_data.collection_name);
	SSAjax({
		data: form_data,
		url: '/admin/catalog_actions?action=add_to_collection',
		success: function(response){ return; }
	});
	
});

$(document).on("click",".add-to-collection .collection-list-wrapper li input", function(e){
	e.stopImmediatePropagation();
	if(store.get('user_id') == 0 || typeof(store.get('user_id')) == "undefined") {
		signin_modal();
		return;
	}
	
	var $this = $(this),
		form_data = {};
		
	form_data.isAjax = true;
	form_data.move_data = $this.val(); 
	var j_data = $.parseJSON(form_data.move_data);
	$this.parents('.collection-product').find('.collection-list-wrapper').addClass('hide');
	$this.parents('.collection-product').find('.coll-prod-img-wrapper').addClass('hide');
	$this.parents('.collection-product').find('.bg-loader').removeClass('hide');
	$this.parents('.collection-product').find('.bg-loader span').html('Add this product to ' + j_data.collection_name + '...');
	SSAjax({
		data: form_data,
		url: '/admin/catalog_actions?action=add_to_collection',
		success: function(response){
			var _data = $.parseJSON(response);
			if(!_data.error){
				$this.parents('.collection-product').parent().fadeOut(300, function(){
					var prod_count = parseInt($this.parents('.coll-prod-list').children('li').length) - 1;
					$(this).remove();
					if(prod_count <= 0){
						reloadPage({link:'/admin/catalog_actions?action=reload_collections', clear_wrapper: '.m-catalog-list .catagory-list', wrapper: '.catagory-list', preloader: '.mcatalogloader'});
					}
				});
			}
		}
	});
	
	
});

$(document).on("click",".assign-to .collection-list-wrapper li input.radio-prod-coll", function(e){
	e.stopImmediatePropagation();
	if(store.get('user_id') == 0 || typeof(store.get('user_id')) == "undefined") {
		signin_modal();
		return;
	}
	
	var $this = $(this),
		form_data = {};
		
	form_data.isAjax = true;
	form_data.move_data = $this.val(); 
	var j_data = $.parseJSON(form_data.move_data);
	$this.parents('.common-move-to-btn').removeClass('assign-to').addClass('move-to').find('.move-lbl').html('Move to...');
	
	var from_cid = $this.parents('.coll-list').find('.from-cid').val();
	form_data.from_cid = from_cid;
	$('.from-cid' + j_data.pid).val(j_data.to_cid);
	$this.parents('.collection-product').find('.collection-lbl').html(j_data.collection_name);
	SSAjax({
		data: form_data,
		url: '/admin/catalog_actions?action=add_to_collection',
		success: function(response){ return; }
	});
	
	
});
// $(document).on('touchstart click', '#product-delete-btn', function(e){
	// e.stopImmediatePropagation();
	// var product_name = $(this).parents('.prod-title-wrapper').find('.prod-title h2').html();

	// if (! confirm("Are you sure you want to delete " + product_name + " ?")) {
		// return false;
	// }
	
	// var modal_id = '#category-product-modal';
	// var form_data = {},
		// load_cat = parseInt($('.product-list .sub-nav .active').attr('data-cid'));
		// if(load_cat == 0){
			// load_cat = $('.main-category-hidden').val();
		// }
	// form_data.isAjax = true;
	// form_data.store_category_id = $(this).attr('data-cid');
	// form_data.product_id = $(this).attr('data-pid');
	// $('.mod-loader-wrapper .shopstyxloader div').html('DELETING...');
	// $('.mod-loader-wrapper').show();
	// SSAjax({
		// data: form_data,
		// url: "/admin/catalog_actions?action=remove_category_product",
		// success: function(response){
			// var _datas = $.parseJSON(response);
			// if(_datas.has_product){ 
				// reloadPage({category_id:load_cat, link:'/admin/catalog_actions?action=category_products', wrapper: modal_id + ' .mod-wrapper', preloader: '.mod-loader-wrapper'});
			// }else{
				// $('.cp-modal-close').trigger('click'); 
			// }
			// reloadPage({link:'/admin/catalog_actions?action=reload_catalog', wrapper: '.catagory-list', preloader: '.mcatalogloader'});
			// $('.prod-title-wrapper .mod-close').trigger('click');
		// }
	// });
// });

$(document).on('touchstart click', '.feature-product-btn', function(e){
	e.stopImmediatePropagation();
	$('.category-product-modal .mod-loader-wrapper').show();
	var form_data = {};

	if($(this).hasClass('featured')){
		$(this).removeClass('featured');
		$('.vproduct').removeClass('product_featured');
		form_data.featured = 0;
	}else{
		$(this).addClass('featured');
		$('.vproduct').addClass('product_featured');
		form_data.featured = 1;
	}
	form_data.isAjax = true;
	var featured_val = $.parseJSON($(this).attr('data-featured'));
	$.extend(form_data,featured_val);
	SSAjax({
		data: form_data,
		url: "/admin/catalog_actions?action=feature_product",
		success: function(response){
			$('.category-product-modal .mod-loader-wrapper').hide();
		}
	});
	
});

$(document).on('touchstart click', '.feature-this-product', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	$('.' + $(this).attr('data-feat')).trigger('click');
	if($(this).hasClass('featured-active')){
		$(this).removeClass('featured-active');
	}else{
		$(this).addClass('featured-active');
	}
});

// $(document).on('touchstart click', '#shop-preview-btn, .back-shop-btn', function(e){
	// e.preventDefault(); e.stopImmediatePropagation();
	// loadMPhone = true;
	// $('.preview-shop-modal').css({'width':'904px'});
	// var modal_id = '#preview-shop-modal';
	// $(modal_id + ' .mod-wrapper').html('');
	// openCustomModal(modal_id);
	// reloadPage({link:'/admin/build?action=shop-design-preview', wrapper: modal_id + ' .mod-wrapper', preloader: '.mod-loader-wrapper'});
// });

$(document).on('touchstart click', '#tablet-mod-btn', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	var modal_id = '#preview-shop-modal';
	$(modal_id + ' .mod-wrapper').html('');
	openCustomModal(modal_id);
	reloadPage({link:'/admin/build?action=shop-tablet-preview', wrapper: modal_id + ' .mod-wrapper', preloader: '.mod-loader-wrapper'});
});

$(document).on('click', '.tabphone-btn2', function(){
	$('.tabphone-btn2').removeClass('tabphone-active');
	$(this).addClass('tabphone-active');
	$('.tabphone-wrapper').addClass('hide');
	$('.' + $(this).attr('data-class')).removeClass('hide');
});
$(document).on('click', '.tablet-btn2', function(){
	$('.preview-shop-modal').css({'width':'904px'});
	$('.mod-other-option label').show();
	$('.edit-shop').html('Edit');
});

$(document).on('click', '.phone-btn2', function(e){
	$('.preview-shop-modal').css({'width':'514px'});
	$('.mod-other-option label').hide();
	$('.edit-shop').html('Edit Design');
	e.preventDefault(); e.stopImmediatePropagation();
	if(loadMPhone){
		var modal_id = '#preview-shop-modal';
		openCustomModal(modal_id);
		loadMPhone = false;
		reloadPage({link:'/admin/build?action=shop-phone-preview', wrapper: modal_id + ' .phone-frame', preloader: ''});
	}
});

$(document).on('touchstart click', '#fb-mod-btn', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	var modal_id = '#preview-shop-modal';
	$(modal_id + ' .mod-wrapper').html('');
	openCustomModal(modal_id);
	reloadPage({link:'/admin/build?action=shop-facebook-preview', wrapper: modal_id + ' .mod-wrapper', preloader: '.mod-loader-wrapper'});
});

$(document).on('touchstart click', '#minishop-mod-btn', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	var modal_id = '#preview-shop-modal';
	$(modal_id + ' .mod-wrapper').html('');
	openCustomModal(modal_id);
	reloadPage({link:'/admin/build?action=shop-minishop-preview', wrapper: modal_id + ' .mod-wrapper', preloader: '.mod-loader-wrapper'});
});

$(document).on('click touchstart', '.edit-shop-btn', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	if($(this).hasClass('data-tablet-phone')){
		store.set('manage_design', 'tablet-phone');
		if($('.tablet-btn2').hasClass('tabphone-active')){ store.set('manage_design_active', 'tablet'); }
		if($('.phone-btn2').hasClass('tabphone-active')){ store.set('manage_design_active', 'phone'); }
	}
	if($(this).hasClass('data-facebook')){
		store.set('manage_design', 'facebook');
	}
	if($(this).hasClass('data-minishop')){
		store.set('manage_design', 'minishop');
	}
	var el = $(this);
	var url  = el.attr('data-link');
	$('#inner-leftmenu').find('li').removeClass('selected');
	$('.sm-design').addClass('selected');
	
	$('body').css({'overflow':'inherit'});

	load_page({
		browserNav: true,
		url: url,
		container: "#subcontent-wrapper",
		data: {body_wrapper:true,subcontent_wrapper:true}
	});
});

$(document).on('click touchstart', '#manage-shop-btn', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	var el = $(this);
	var url  = el.attr('href');
	$('#inner-leftmenu').find('li').removeClass('selected');
	$('.sm-catalog').addClass('selected');
	
	$('body').css({'overflow':'inherit'});

	load_page({
		browserNav: true,
		url: url,
		container: "#subcontent-wrapper",
		data: {body_wrapper:true,subcontent_wrapper:true}
	});
});

$(document).on('click', '.view-blog-btn2', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	openModal3();
	reloadPage({link:'/admin/build?action=blog-template', wrapper: '.blog-modal2 .mod-wrapper', preloader: '.blog-modal2 .mod-loader-wrapper'});
});
$(document).on('click', '.back-details-btn', function(e){
	e.stopImmediatePropagation();
	$('.reload-product').trigger('click');
});

$(document).on('touchstart click', '.stickit-btn, .promte-btn, .stykable-size-btn', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	$('.product-info-modal .mod-wrapper').html('');
	openModal();
	var $form = $(this),
		form_data = {};
	form_data.isAjax = true;
	
	var settings = $.parseJSON($(this).attr('data'));
	$.extend(form_data,settings);
	showModalLoader();
	SSAjax({
		data: form_data,
		url: "/admin/stykable_actions?action=stykable",
		success: function(response){
			$('.product-info-modal .mod-wrapper').html(response);
			hideModalLoader();
		}
	});
});

$(document).on('click', '.promote-this-product', function(e){
	e.preventDefault(); e.stopImmediatePropagation();

	if(store.get('user_id') == 0 || typeof(store.get('user_id')) == "undefined" || typeof(store.get('token')) == "undefined") {
		signin_modal();
		if($(this).attr('ispopup')) {
			History.pushState(null, 'Shopstyx', '/');
		}
		return;
	}

	$('.reveal-modal-promoteproduct-bg').show();
	if($(this).attr('ispopup')) {
		$('.reveal-modal-promoteproduct-bg').addClass('ispopup')
	}

	$('body').addClass('hidescroll');
	$('#promoteProductModal').show().centerized();

	SSAjax({
		data: {isAjax:true,action:'ajax',action_type:'embedform'},
		url: $(this).attr('url'),
		success: function(response) {
			$('#promoteProductModal').html(response);
			$('#promoteProductModal').addClass('noloader');
		}
	});

	var data = $(this).attr('data');
	var mdata = $.parseJSON(data);
	$('#promoteProductModal').data('data',data);
});

$(document).on('click', '.promote-this', function (e) {
	e.preventDefault(); e.stopImmediatePropagation();

	if(store.get('user_id') == 0 || typeof(store.get('user_id')) == "undefined" || typeof(store.get('token')) == "undefined") {
		signin_modal();
		return;
	}

	$('.reveal-modal-promoteproduct-bg').show();
	$('body').addClass('hidescroll');
	$('#promoteProductModal').addClass('collection').show().centerized();

	SSAjax({
		data: {isAjax:true,action:'ajax',action_type:'embedform'},
		url: $(this).attr('url'),
		success: function(response) {
			$('#promoteProductModal').html(response);
			$('#promoteProductModal').addClass('noloader');
		}
	});

	var data = $(this).attr('data');
	//var mdata = $.parseJSON(data);
	//console.log(data)
	$('#promoteProductModal').data('data',data);
});

$(document).on('click', '.reveal-modal-promoteproduct-bg', function(e) {
	e.preventDefault(); e.stopImmediatePropagation();
	$(this).hide();
	$('#promoteProductModal').hide().html('').removeClass('noloader').removeClass('collection');

	if($(this).hasClass('ispopup')) {
		$(this).removeClass('ispopup');
	}
	else {
		$('body').removeClass('hidescroll');
	}
});

$(document).on('click', '.promote-product-view .p-right .share-wrapper .left-share .share-common', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	var data = {type:$(this).attr('type')};
	var mdata = $('#promoteProductModal').data('data');
	mdata = $.parseJSON(mdata);
	data = $.extend(data,mdata);
	//console.log(data)
	promote_product(data);
});

$(document).on('click', '.category-modal-close', function(){
	closeCustomModal('#category-modal');
});
$(document).on('click', '.category-modal2-close', function(){
	closeCustomModal2('#category-modal2');
});
$(document).on('click', '.cp-modal-close', function(){
	closeCustomModal('#category-product-modal');
	$('.category-product-modal .modal-content').html('');
});
$(document).on('click', '.prod-editor-close', function(){
	closeCustomModal('#product-editor-modal');
	$('.product-editor-modal .mod-wrapper').html('');
});
$(document).on('click', '.shop-preview-close', function(){
	loadMPhone = true;
	closeCustomModal('#preview-shop-modal');
});
$(document).on('click', '.coll-prod-hover a, .coll-prod-hover .common-move-to-btn', function(e) {
	e.stopImmediatePropagation();
});

$(window).resize(function() {
	toggleScroll_leftmenu();
	//updateSizeContainer('.mod-wrapper-content', '.mod-wrapper');
	updateCartSizeContainer();
	updateWalletSizeContainer();
	var cpo = new customer_purchase_order();
	cpo.resizeModal();
	var height = $(window).height();
	$('.custom-modal .mod-wrapper').css({'height': height});
	resizeProductDetails();
});

/* Shop Settings */
$(document).on('click', '.save-global-shipping-btn, .save-general-settings-btn, .save-policy-btn, .save-getpayment-btn', function (e) {
	e.preventDefault(); e.stopImmediatePropagation();
	$('.shopsettings .save-btn').trigger('click');
});

$(document).on('submit valid', '.shopsettings form', function (e) {
	e.preventDefault(); e.stopImmediatePropagation();
	save_shopsettings($(this));
}).on('invalid', function() {
	//alert('a');
	$('.form-submission-message').removeClass('ss-success').stop();
	$('.form-submission-message')
		.addClass('ss-error')
		.fadeIn(500);						
});

$(document).on('click', '.sort-transaction', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	var $this = $(this),
		form_data = {};
	form_data.isAjax = true;
	form_data.sorting = true;
	form_data.body_wrapper = true;
	if($this.hasClass('asc')){
		form_data.sort = 'asc';
	}
	if($this.hasClass('desc')){
		form_data.sort = 'desc';
	}
	$('#main-content .shopstyxloader').removeClass('hide');
	$('#main-content .bg-overlay').removeClass('hide');
	SSAjax({
		data: form_data,
		url: "/admin/shop-settings/wallet-claimed-history",
		success: function(response){
			$('.claimed-sales-history .tbl-left').html(response);
			$('#main-content .shopstyxloader').addClass('hide');
			$('#main-content .bg-overlay').addClass('hide');
		}
	});
});

/* Design Section */

$(document).on('click', '.mobile-phone, .tablet', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	
	var $this = $(this),
		form_data = {},
		modal_id;
	
	if($this.hasClass('mobile-phone')){
		form_data.display = 'phone';
		modal_id = '#mobile-modal';
	}
	if($this.hasClass('tablet')){
		form_data.display = 'tablet';
		modal_id = '#tablet-modal';
	}
	
	$(modal_id + ' .mod-wrapper').html('');
	openCustomModal(modal_id);	
	form_data.isAjax = true;
	
	$(modal_id + ' .mod-loader-wrapper').css({'display':'block'});
	SSAjax({
		data: form_data,
		url: "/admin/build?page=tab_phone",
		success: function(response){
			$(modal_id + ' .mod-wrapper').html(response);
			$(modal_id + ' .mod-loader-wrapper').css({'display':'none'});
		}
	});
});

$(document).on('click', '.mobile-modal-close', function(){
	closeCustomModal('#mobile-modal');
	$('#mobile-modal .mod-wrapper').html('');
});
$(document).on('click', '.tablet-modal-close', function(){
	closeCustomModal('#tablet-modal');
	$('#tablet-modal .mod-wrapper').html('');
});

$(document).on('close', '#desktopModal', function (e) {
		e.preventDefault(); e.stopImmediatePropagation();
		$('.reveal-modal-bg').css({'background-color':'#FFFFFF'});
		$(this).find('.settings-wrapper').html('');
		loadPhone = true;
	});

$(document).on('click', '.save-desktop-settings-btn', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	$(this).css({'opacity':0.8}).html('Saving...');
	$('.settings-mini-view').addClass('hide');
	$('.settings-mini-view').removeClass('unhide');
	if($('.store-type').val() == 'desktop'){
		saveDesktopSettings({color1:$('.desktop-pcolor1').val(),color2:$('.desktop-pcolor2').val(),color3:$('.desktop-pcolor3').val(),body_background:$('.desktop-pbody_background').val(), preset_colors_id: 0});
		if($('.desktop-home-type').val() ==  'slideshow'){
			$('.for-slideshow-listing').addClass('unhide');
			$('.for-slideshow-listing').removeClass('hide');
		}
		if($('.desktop-home-type').val() ==  'category'){
			$('.for-categories').addClass('unhide');
			$('.for-categories').removeClass('hide');
		}
		if($('.desktop-home-type').val() ==  'slideshow_only'){
			$('.for-slideshow-only').addClass('unhide');
			$('.for-slideshow-only').removeClass('hide');
		}
	}
});

$(document).on('open', '#desktopModal', function (e) {
	$(this).centerized();
	e.preventDefault(); e.stopImmediatePropagation();
	var wH = $(window).height();
		mwH = $('#body-wrapper').height() + 100,
		page = '',
		modal_class = '';
	$('.shop-settings-modal').css({'height': mwH + 'px'});
	$('.reveal-modal-bg').css({'background-color':'#F2F5F6'});
	
	if($(this).hasClass('desktopModal')){
		modal_class = '.desktopModal';
		page = '/admin/build?page=desktop';
		$(modal_class + ' .settings-wrapper').html('');
		$(modal_class + ' .settings-loader').removeClass('hide');
		
	}
	var form_data = {};
	form_data.isAjax = true;
	SSAjax({
		data: form_data,
		url: page,
		success: function(response){
			$(modal_class + ' .settings-wrapper').html(response);
			$(modal_class + ' .settings-loader').addClass('hide');
			if(modal_class == '.tabletphoneModal'){
				if(typeof store.get('manage_design_active') !== 'undefined' && store.get('manage_design_active') == 'phone'){
					$('.phone-btn1').trigger('click');
					loadTablet = true;
					store.remove('manage_design_active');
					return false;
				}
				store.remove('manage_design_active');
				previewPage({page: 'tablet', color1:$('.tabletphone-pcolor1').val(),color2:$('.tabletphone-pcolor2').val(),body_background:$('.tabletphone-pbody_background').val(), home_type: $('.tabletphone-home-type').val()});
			}
		}
	});
});

$(document).on('touchstart click', '#add-new-basic-product', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	store.set('addproduct_modal','basic');
	addproduct({
		data:{product_id:0,formType:'basic'},
		onSaved: function(data) {
			$('body').css('overflow','hidden');
			if(parseInt($('.admin-prod-listing').length) > 0 ){
				var data_zindex = parseInt($('.coll-prod-list li.admin-prod-list:first-child').next().attr('data-z-index')) + 1;
				if(data.product_id){
					SSAjax({
						url: '/admin/catalog_actions?action=load_single_product',
						data: {isAjax:true,product_id:data.product_id,zindex:data_zindex},
						success: function(result){
							var list = '<li data-prod-id="' + data.product_id + '" data-z-index="' + data_zindex + '" class="left small-4 large-3 admin-prod-list pzindex' + data.product_id + '">'+ result +'</li>';
							$('.coll-prod-list li.admin-prod-list:first-child').after(list);
							return;
						}
					});
				}
				_assign_to_collection('{"cid":"' + data.collection_id + '","puid":"' + data.user_id + '","pid":"' + data.product_id + '","sid":"' + data.store_id + '"}');
			}else{
				//reloadPage({link: root + 'admin/dashboard?reload=products', clear_wrapper: '#result-product-list', wrapper: '#result-product-list', preloader: '.mcatalogloader'});
				location.href= root + 'admin/dashboard';
			}
		}
	});
});


/* product search and filters */

$(document).on('touchstart click', '.product-filter li', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	var form_data = {};
	if($.trim($('.input-search_text').val()) != ""){
		form_data.search_text = $.trim($('.input-search_text').val());
	}
	form_data.price_from = $('.display-price span.selected').attr('data-from');
	form_data.price_to = $('.display-price span.selected').attr('data-to');
	form_data.department_id = (parseInt($('.depart-list-head li.active').attr('data')) != 0)? $('.depart-list-head li.active').attr('data') : $('.depart-list-small-head li.active').attr('data');
	form_data.color = $('.home-color-option span.selected').attr('data-color');
	form_data.type = $(this).attr('data');
		
	form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
	form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
	form_data.body_wrapper = true;
	form_data.subcontent_wrapper = true;
	form_data.isAjax = true;
	
	toggle_preloader_maincontent();
	$.post(root + 'admin/dashboard/products', form_data,function(response){
		reset_scroll_page('forwin');
		extend_scroll_opts(form_data,'forwin');
		$('#subcontent-wrapper').html(response);
		toggle_preloader_maincontent();
	});
	
});

$(document).on('touchstart click', '.admin-product-search-btn', function(e){
	e.stopImmediatePropagation();
	var search_text = $.trim($('.admin-product-search-btn').val());
	if(search_text == ""){ return false; }

	var form_data = {};
	form_data.search_text = search_text;
	
	form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
	form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
	form_data.type = $('.p-io-active').attr('data');
	form_data.body_wrapper = true;
	form_data.subcontent_wrapper = true;
	form_data.isAjax = true;
	
	toggle_preloader_maincontent();
	$.post(root + 'admin/dashboard/products', form_data,function(response){
		reset_scroll_page('forwin');
		extend_scroll_opts(form_data,'forwin');
		$('#subcontent-wrapper').html(response);
		toggle_preloader_maincontent();
	});
});

$(document).on('keypress', '.admin-product-search', function(e){
	e.stopImmediatePropagation();
	if (e.which == 13) {
		if($(this).val() == ""){ return false; }

		var form_data = {};
		form_data.search_text = $.trim($(this).val());
		
		form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
		form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
		form_data.type = $('.p-io-active').attr('data');
		form_data.body_wrapper = true;
		form_data.subcontent_wrapper = true;
		form_data.isAjax = true;
		
		toggle_preloader_maincontent();
		$.post(root + 'admin/dashboard/products', form_data,function(response){
			reset_scroll_page('forwin');
			extend_scroll_opts(form_data,'forwin');
			$('#subcontent-wrapper').html(response);
			toggle_preloader_maincontent();
		});
	}
});

$(document).on('click', '.home-promos', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	var form_data = {};
	if($.trim($('.input-search_text').val()) != ""){
		form_data.search_text = $.trim($('.input-search_text').val());
	}
	form_data.price_from = $('.display-price span.selected').attr('data-from');
	form_data.price_to = $('.display-price span.selected').attr('data-to');
	form_data.color = $('.home-color-option span.selected').attr('data-color');
	form_data.department_id = (parseInt($('.depart-list-head li.active').attr('data')) != 0)? $('.depart-list-head li.active').attr('data') : $('.depart-list-small-head li.active').attr('data');
	form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
	form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
	form_data.type = $('.p-io-active').attr('data');
	form_data.body_wrapper = true;
	form_data.subcontent_wrapper = true;
	form_data.isAjax = true;
	if(!$(this).hasClass('active')){
		form_data.promos = true;
	}
	toggle_preloader_maincontent();
	$.post(root + 'admin/dashboard/products', form_data,function(response){
		reset_scroll_page('forwin');
		extend_scroll_opts(form_data,'forwin');
		$('#subcontent-wrapper').html(response);
		toggle_preloader_maincontent();
	});
});

$(document).on('click', '.depart-list.homeproduct li', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	if($(this).attr('data') == ""){ return false; }
	var form_data = {};
	if($.trim($('.input-search_text').val()) != ""){
		form_data.search_text = $.trim($('.input-search_text').val());
	}
	form_data.price_from = $('.display-price span.selected').attr('data-from');
	form_data.price_to = $('.display-price span.selected').attr('data-to');
	form_data.color = $('.home-color-option span.selected').attr('data-color');
	form_data.department_id = $(this).attr('data');

	form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
	form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
	form_data.type = $('.p-io-active').attr('data');
	form_data.body_wrapper = true;
	form_data.subcontent_wrapper = true;
	form_data.isAjax = true;
	if($('.home-promos').hasClass('active')){
		form_data.promos = true;
	}
	toggle_preloader_maincontent();
	$.post(root + 'admin/dashboard/products', form_data,function(response){
		reset_scroll_page('forwin');
		extend_scroll_opts(form_data,'forwin');
		$('#subcontent-wrapper').html(response);
		toggle_preloader_maincontent();
	});
	
});

$(document).on('touchstart click', '.removethis-color-filter', function(){
	$('.home-color-option span').removeClass('selected');
	$(this).addClass('hide');		
	var form_data = {};
	if($.trim($('.input-search_text').val()) != ""){
		form_data.search_text = $.trim($('.input-search_text').val());
	}
	form_data.price_from = $('.display-price span.selected').attr('data-from');
	form_data.price_to = $('.display-price span.selected').attr('data-to');
	form_data.department_id = (parseInt($('.depart-list-head li.active').attr('data')) != 0)? $('.depart-list-head li.active').attr('data') : $('.depart-list-small-head li.active').attr('data');
	
	form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
	form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
	form_data.type = $('.p-io-active').attr('data');
	form_data.body_wrapper = true;
	form_data.subcontent_wrapper = true;
	form_data.isAjax = true;
	if($('.home-promos').hasClass('active')){
		form_data.promos = true;
	}
	toggle_preloader_maincontent();
	$.post(root + 'admin/dashboard/products', form_data,function(response){
		reset_scroll_page('forwin');
		extend_scroll_opts(form_data,'forwin');
		$('#subcontent-wrapper').html(response);
		toggle_preloader_maincontent();
	});
	
});

$(document).on('touchstart click', '.home-color-option span', function(){
	$('.home-color-option span').removeClass('selected');
	$(this).addClass('selected');
	var form_data = {};
	if($.trim($('.input-search_text').val()) != ""){
		form_data.search_text = $.trim($('.input-search_text').val());
	}
	form_data.price_from = $('.display-price span.selected').attr('data-from');
	form_data.price_to = $('.display-price span.selected').attr('data-to');
	form_data.department_id = (parseInt($('.depart-list-head li.active').attr('data')) != 0)? $('.depart-list-head li.active').attr('data') : $('.depart-list-small-head li.active').attr('data');
	form_data.color = $(this).attr('data-color');
		
	form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
	form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
	form_data.type = $('.p-io-active').attr('data');
	form_data.body_wrapper = true;
	form_data.subcontent_wrapper = true;
	form_data.isAjax = true;
	if($('.home-promos').hasClass('active')){
		form_data.promos = true;
	}
	toggle_preloader_maincontent();
	$.post(root + 'admin/dashboard/products', form_data,function(response){
		reset_scroll_page('forwin');
		extend_scroll_opts(form_data,'forwin');
		$('#subcontent-wrapper').html(response);
		toggle_preloader_maincontent();
	});
	
});

$(document).on('click', '.display-price span', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	if($(this).attr('data') == ""){ return false; }
	var form_data = {};
	if($.trim($('.input-search_text').val()) != ""){
		form_data.search_text = $.trim($('.input-search_text').val());
	}
	form_data.color = $('.home-color-option span.selected').attr('data-color');
	form_data.department_id = (parseInt($('.depart-list-head li.active').attr('data')) != 0)? $('.depart-list-head li.active').attr('data') : $('.depart-list-small-head li.active').attr('data');
	form_data.price_from = $(this).attr('data-from');
	form_data.price_to = $(this).attr('data-to');
	form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
	form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
	form_data.type = $('.p-io-active').attr('data');
	form_data.body_wrapper = true;
	form_data.subcontent_wrapper = true;
	form_data.isAjax = true;
	if($('.home-promos').hasClass('active')){
		form_data.promos = true;
	}
	toggle_preloader_maincontent();
	$.post(root + 'admin/dashboard/products', form_data,function(response){
		reset_scroll_page('forwin');
		extend_scroll_opts(form_data,'forwin');
		$('#subcontent-wrapper').html(response);
		toggle_preloader_maincontent();
	});
});

$(document).on('touchstart click', '.removethis-price-filter', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	$('.display-price span').removeClass('selected');
	$(this).addClass('hide');
	var form_data = {};
	if($.trim($('.input-search_text').val()) != ""){
		form_data.search_text = $.trim($('.input-search_text').val());
	}
	form_data.department_id = (parseInt($('.depart-list-head li.active').attr('data')) != 0)? $('.depart-list-head li.active').attr('data') : $('.depart-list-small-head li.active').attr('data');
	form_data.color = $('.home-color-option span.selected').attr('data-color');
	
	form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
	form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
	form_data.type = $('.p-io-active').attr('data');
	form_data.body_wrapper = true;
	form_data.subcontent_wrapper = true;
	form_data.isAjax = true;
	if($('.home-promos').hasClass('active')){
		form_data.promos = true;
	}
	toggle_preloader_maincontent();
	$.post(root + 'admin/dashboard/products', form_data,function(response){
		reset_scroll_page('forwin');
		extend_scroll_opts(form_data,'forwin');
		$('#subcontent-wrapper').html(response);
		toggle_preloader_maincontent();
	});
});

/* people page section */

$(document).on('click', '.customers #order-details-btn', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	var modal_id = '#custom-order-modal';
	$(modal_id + ' .mod-wrapper').html('');
	openCustomModal(modal_id);
	var $form = $(this),
		form_data = {isAjax:true,body_wrapper:true,customer_id:$form.attr('customer_id')};
	form_data.isAjax = true;
	showModalLoader();
	SSAjax({
		data: form_data,
		url: "/admin/people/customer_orders",
		success: function(response){
			$(modal_id + ' .mod-wrapper').html(response);
			hideModalLoader();
		},
		error: function(jqXHR, textStatus, errorThrown ) {
			closeCustomModal('#custom-order-modal');
			//location.href = location.href;
		}
	});
});

$(document).on('click', '.promoters #order-details-btn', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	var modal_id = '#custom-order-modal';
	$(modal_id + ' .mod-wrapper').html('');
	openCustomModal(modal_id);
	var $form = $(this),
		form_data = {isAjax:true,body_wrapper:true,promoter_id:$form.attr('promoter_id')};
	form_data.isAjax = true;
	showModalLoader();
	SSAjax({
		data: form_data,
		url: "/admin/people/promoter_products",
		success: function(response){
			$(modal_id + ' .mod-wrapper').html(response);
			hideModalLoader();
		},
		error: function(jqXHR, textStatus, errorThrown ) {
			closeCustomModal('#custom-order-modal');
			//location.href = location.href;
		}
	});
});

$(document).on('click', '.people-modal-close', function(){
	closeCustomModal('#custom-order-modal');
});

$(document).on('click', '.customer-modal-close', function(){
	closeCustomModal('#custom-order-modal');
});


/* orders page section */

$(document).on('click', '#order-info-btn', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	view_seller_order_details($(this).attr('order_id'));
});

$(document).on('click', '#edit-shipped-btn', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	$('.ship-order-form').css({'display':'block'});
});

$(document).on('click', '#shipped-cancel-btn', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	$('.ship-order-form').css({'display':'none'});
});

$(document).on("click", ".conversation-btn:not('.generic-chat-user-window')", function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	var data = $.parseJSON($(this).attr('data'));
	openThisThread(data.to,'order-chat',{order_id:data.order_id,
										cust_order_id:data.transaction_number,
										scust_order_id:data.order_number
									});
});

$(document).on("click", ".generic-chat-user-window", function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	openThisThread($(this).attr('target_user_id'),'chat',null);
});

$(document).on("click", "#refund-transaction-order", function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	refund_transaction_order($(this).attr('order_id'));
});

$(document).on("click", "#refund-transaction-order-product", function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	refund_transaction_order($(this).attr('order_id'),$(this).attr('pid'),$(this).attr('variant_id'));
});

$(document).on("click", "#resolve-transaction-order-product,#resolve-transaction-order", function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	resolve_transaction_order($(this).attr('order_id'),$(this).attr('pid'),$(this).attr('variant_id'));
});


/* Subscription */
$(document).on("click", "#demosubscrbtn", function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	var modaltype = $(this).attr('modaltype'),
		loadcontent = $(this).attr('loadcontent'),
		modal = $('#subscribeModal1');
	modal.attr('url',$(this).attr('url'));
	modal.attr('plan_type',$(this).attr('plan_type'));
	modal.removeClass('wider');
	modal.removeClass('shorter');

	if(modaltype == 1) {
		modal.addClass('wider');
	}
	else if (modaltype == 2) {
		modal.addClass('shorter');
	}

	modal.find('.subscrcont').html('');
	//modal.find('.shopstyxloader-box').removeClass('hide');
	$('#wrapper').addClass('hide');
	modal.centerized();
	subscription_toggle_loader();
	if(typeof loadcontent == "undefined") {
		modal.foundation('reveal', 'open', {close_on_background_click: false});
	}
	else {
		$(this).centerized();
		subscription_load_modal();
	}
});

$(document).on('open', '#subscribeModal1', function () {
	$('.reveal-modal-bg').css('display', 'block');
	$(this).centerized();
});

$(document).on('opened', '#subscribeModal1', function () {
	subscription_load_modal();
});

$(document).on('closed', '#subscribeModal1', function () {
	$('#wrapper').removeClass('hide');
	$('.reveal-modal-bg').css('display', 'none');

	var modal = $('#subscribeModal1');
	modal.removeClass('wider');
	modal.removeClass('shorter');
	modal.removeClass('cancelled');
	//$('body').css('overflow','auto');
});

$(document).on("submit", "#subscript-addcreditcard-form", function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	subscription_addedit_creditcard($(this));	
});

$(document).on("click", ".close-subscription-modal", function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	subscription_close_modal();
});

$(document).on('submit', '#subscription-save-cname-form', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	subscription_save_cname();
});

$(document).on('submit', '#subscription-upgrade-plan-form', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	subscription_upgrade($(this).attr('plan_type'));
});

$(document).on("click", "#subscription-cancel-btn", function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	subscription_cancel($(this).attr('plan_type'));
});

$(document).on('click touchstart', '.vtabs a', function(){
	var dterm = $(this).attr('data');
	$('.vtabs li a').removeClass('selected');
	$(this).addClass('selected');
	$('.vids li').removeClass('selected');
	$('.vids li.'+dterm).addClass('selected');
	return false;
});


/* 	===================================
 *	Functions Section
 *	===================================
 */

function load_subscription_button() {
	$('#shop-preview-btn').removeClass('hide');

	$.ajax({
		type: 'post',
		data: gatherReq(),
		url: CS.main_url + 'admin/subscription/loadbutton',
		success: function(response) {
			reponse = response.split('||');

			if(reponse.length == 2) {
				$('#shop-preview-btn').html(reponse[1]);
			}
		}
	});
} 

function subscription_toggle_loader() {
	var pp = $('#subscribeModal1'),
		load = pp.find('.shopstyxloader-box'),
		overlay = pp.find('.shopstyxloader-box-overlay');

	if(load.hasClass('hide')) {
		overlay.removeClass('hide');
		load.removeClass('hide');
	}
	else {
		load.addClass('hide');
		overlay.addClass('hide');
	}
}

function subscription_close_modal() {
	$('#subscribeModal1').foundation('reveal', 'close');
}

function subscription_load_modal(data, callback) {
	data = (typeof data == "object") ? data : {};
	var pp = $('#subscribeModal1'),
		load = pp.find('.shopstyxloader-box');
	//$('body').css('overflow','hidden');
	var datalod = gatherReq();
	$.extend(datalod,{plan_type:pp.attr('plan_type')},data);
	$.ajax({
		data: datalod,
		url: CS.main_url+pp.attr('url'),
		type: 'POST',
		success: function(response){
			pp.find('.subscrcont').html(response);

			if(typeof callback == "function") {
				var callbacks = $.Callbacks();
				callbacks.add( callback );
				callbacks.fire();
			}
			pp.centerized();
			subscription_toggle_loader();
		},
		error: function(jqXHR, textStatus, errorThrown ) {
			if(jqXHR.status == 404) {
				//location.href = "/admin/dashboard";
			}
		}
	});
}

function subscription_addedit_creditcard(form) {
	var error_msg = new Array(),
		first_error = new Array();

	var cc_number = form.find('[name="cc_number"]'),
		cc_nameoncard = form.find('[name="cc_nameoncard"]'),
		cc_expire_month = form.find('[name="cc_expire_month"]'),
		cc_expire_year = form.find('[name="cc_expire_year"]'),
		cc_cvv = form.find('[name="cc_cvv"]'),
		plan_type = form.find('[name="plan_type"]').val();

	form.find('.error-content').addClass('hide');
	form.find('input, .mystyled-select').removeClass('error-field');
	subscription_toggle_loader();

	var datalod = gatherReq();
	$.extend(datalod,{plan_type:plan_type,
					cc_number: cc_number.val(),
					cc_nameoncard: cc_nameoncard.val(),
					cc_expire_month: cc_expire_month.val(),
					cc_expire_year: cc_expire_month.val(),
					cc_cvv: cc_cvv.val(),
					address_type:"SCC",
					checkfields:true});
	$.ajax({
		data: datalod,
		url: CS.main_url+'admin/subscription/create_subscription_billing',
		type: 'POST',
		success: function(response){
			response = $.parseJSON(response);
			if(!response || response == "" || response == null || $.isEmptyObject(response)) {
				subscription_toggle_loader();
				location.href = location.href;
				return false;
			}

			
			if(typeof(response['success']) != "undefined" && response.hasOwnProperty('success') && response.success == false) {
				var error = response['error'];
				if(error['cc_number']) {
					cc_number.addClass('error-field');
					first_error.push('cc_number');
					error_msg.push("Card number is not correct.");
				}
				if(error['cc_nameoncard']) {
					cc_nameoncard.addClass('error-field');
					first_error.push('cc_nameoncard');
					error_msg.push("Name on Card is required.");
				}
				if(error['cc_expire']) {
					cc_expire_month.parent().addClass('error-field');
					cc_expire_year.parent().addClass('error-field');
					first_error.push('cc_expire_year');
					error_msg.push("Expiration date is not correct.");
				}
				if(error['cc_cvv']) {
					first_error.push('cc_cvv');
					cc_cvv.addClass('error-field');
					error_msg.push("Security code is required.");
				}

				error_msg = error_msg.join('<br /> ');

				form.find('.error-content div').html("Please correct the following error(s):");
				form.find('.error-content span').html(error_msg);
				form.find('.error-content').removeClass('hide');

				subscription_toggle_loader();

				var inputPos = form.offset(); 
				$(document).scrollTop(inputPos.top);
			}
			else {
				subscription_create_customer_payment_method({
					number: cc_number.val(),
					cardholderName: cc_nameoncard.val(),
					expirationMonth: cc_expire_month.val(),
					expirationYear: cc_expire_year.val(),
					// CVV if required
					cvv: cc_cvv.val(),
					address_id: response.address_id,
					plan_type: plan_type,
					subscription_form:form
				});
			}

		},
		error: function(jqXHR, textStatus, errorThrown ) {
			subscription_toggle_loader();
			if(jqXHR.status == 404) {
				location.href = location.href;
				//location.href = "/admin/dashboard";
			}
		}
	});

	
}

function subscription_create_customer_payment_method(data) {
		CS.braintree_token.tokenizeCard({
			number: data.number,
			cardholderName: data.cardholderName,
			expirationMonth: data.expirationMonth,
			expirationYear: data.expirationYear,
			// CVV if required
			cvv: data.cvv,
			options: {
				verifyCard: true	
			}
		}, function (err, nonce) {
			if(err != null) {
				subscription_delete_address(data.address_id);
				alert('Sorry, we cannot validate your credit card as of the moment.');
				console.log('Error in creating payment method:' + err);
				subscription_toggle_loader();
			}
			else {
				var datalod = gatherReq();
				$.extend(datalod,{plan_type:data.plan_type,
								address_id: data.address_id,
								nonce: nonce,
								address_type:"SCC"});

				$.ajax({
					type: "POST",
					url: CS.main_url+'admin/subscription/create_subscription_billing',
					data: datalod,
					success: function(response){
						response = $.parseJSON(response);
						if(!response || response == null || $.isEmptyObject(response)) {
							subscription_toggle_loader();
							//location.href = '/';
						}

						var modal = $('#subscribeModal1');

						if(!response.success) {
							var error_msg;

							if(response.errorcode == 101) {
								error_msg = response.error_array.join('<br /> ');
								data.subscription_form.find('.error-content div').html("We are unable to authorize your credit card.");
								data.subscription_form.find('.error-content span').html(error_msg);
								data.subscription_form.find('.error-content').removeClass('hide');
								subscription_toggle_loader();
							}
							else if(response.errorcode == 102) {
								modal.attr('url',"admin/subscription/subscribe_error");
								modal.attr('plan_type',data.plan_type);
								subscription_load_modal({"error_msg":response.error_array,"upgrade_plan_type":data.plan_type},
														function() {
															modal.removeClass('wider');
															modal.addClass('shorter');
														});	
							}
							else {
								subscription_toggle_loader();
								alert('Sorry, we cannot validate your credit card as of the moment.');
								console.log(response);
							}
						}
						else {
							modal.find('.subscrcont').html('');
							var callback_func = function() {};

							if(response.action_type == "A") {
								modal.attr('url',"admin/subscription/subscribe_success");
								modal.attr('plan_type',data.plan_type);
								
								callback_func = function() {
													modal.removeClass('wider');
													modal.addClass('shorter');
												};
							}
							else if(response.action_type == "UPM") {
								modal.attr('url',"admin/subscription/change_paymentmethod_success");
								modal.attr('plan_type',data.plan_type);

								callback_func = function() {
													modal.removeClass('wider');
													modal.removeClass('shorter');
												};
							}
							else if(response.action_type == "UP") {
								modal.attr('url',"admin/subscription/upgrade_subscription_success");
								modal.attr('plan_type',data.plan_type);

								callback_func = function() {
													modal.removeClass('wider');
													modal.addClass('shorter');
												};
							}

							subscription_load_modal({address_id:response.address_id,
													customer_id:response.customer_id,
													subscription_id:response.subscription_id},
													callback_func);	
							load_subscription_button();
						}

					},
					error: function(jqXHR, textStatus, errorThrown ) {
						subscription_toggle_loader();
						if(jqXHR.status == 404) {
							//location.href = location.href;
							//location.href = "/admin/dashboard";
						}
					}

				});
			}
		  
		});
}

function subscription_add_paypal_payment_method(nonce, email) {
	subscription_toggle_loader();
	
	var datalod = gatherReq(),
		plan_type = $('#paypal-button').attr('plan_type');

	$.extend(datalod,{plan_type:plan_type,
					nonce: nonce,
					email: email,
					address_type:"SPayPal"});
	$.ajax({
		data: datalod,
		url: CS.main_url+'admin/subscription/create_subscription_billing',
		type: 'POST',
		success: function(response){
			response = $.parseJSON(response);
			if(!response || response == "" || response == null || $.isEmptyObject(response)) {
				subscription_toggle_loader();
				location.href = location.href;
				return false;
			}

			var modal = $('#subscribeModal1');
			var callback_func = function() {};

			if(!response.success) {
				var error_msg;

				modal.attr('url',"admin/subscription/subscribe_error");
				modal.attr('plan_type',plan_type);
				subscription_load_modal({"error_msg":response.error_array,"upgrade_plan_type":plan_type},
										function(){
											modal.removeClass('wider');
											modal.addClass('shorter');
										});	

			}
			else {
				modal.find('.subscrcont').html('');

				if(response.action_type == "A") {
					modal.attr('url',"admin/subscription/subscribe_success");
					modal.attr('plan_type',datalod.plan_type);
					callback_func = function () {
										modal.removeClass('wider');
										modal.addClass('shorter');
									};
				}
				else if(response.action_type == "UPM") {
					modal.attr('url',"admin/subscription/change_paymentmethod_success");
					modal.attr('plan_type',datalod.plan_type);
					callback_func = function () {
										modal.removeClass('wider');
										modal.removeClass('shorter');
									};
				}
				else if(response.action_type == "UP") {
					modal.attr('url',"admin/subscription/upgrade_subscription_success");
					modal.attr('plan_type',datalod.plan_type);
					callback_func = function () {
										modal.removeClass('wider');
										modal.addClass('shorter');
									};
				}

				subscription_load_modal({address_id:response.address_id,
										customer_id:response.customer_id,
										subscription_id:response.subscription_id});	

				load_subscription_button();
			}

			shopstyx_subscription_paypal_action = false;
		},
		error: function(jqXHR, textStatus, errorThrown ) {
			subscription_toggle_loader();
			if(jqXHR.status == 404) {
				location.href = location.href;
				//location.href = "/admin/dashboard";
			}
		}
	});
}

function subscription_delete_address(address_id) {
	var datalod = gatherReq();

	$.extend(datalod,{address_id: data.address_id,
					address_type:"SCC"});
	$.ajax({
		type: "POST",
		url: CS.main_url+'admin/subscription/subscription_delete_address',
		data: datalod
	});
}

function subscription_save_cname() {
	var modal = $('#subscribeModal1'),
		input = $('#subscription-cname-input'),
		datalod = gatherReq();

	if($.trim(input.val()) == "") {
		input.val('');
		return;
	}
	$.extend(datalod,{shop_url:input.val()});

	subscription_toggle_loader();
	$.ajax({
		url: CS.main_url+'admin/subscription/save_cname',
		type: 'POST',
		data: datalod,
		success: function(response){
			response = $.parseJSON(response);
			if(!response || response == "" || response == null || $.isEmptyObject(response)) {
				subscription_toggle_loader();
				location.href = location.href;
				return false;
			}

			if(!response.success) {
				input.val('');
				subscription_toggle_loader();
			}
			else {
				modal.find('.subscrcont').html('');
				modal.attr('url',"admin/subscription/viewsettings");
				modal.removeClass('shorter');
				subscription_load_modal();	
			}

		}
	});
}

function subscription_upgrade(plan_type) {
	subscription_toggle_loader();
	
	var datalod = gatherReq();
	$.extend(datalod,{plan_type:plan_type});	

	$.ajax({
		data: datalod,
		url: CS.main_url+'admin/subscription/upgrade_subscription',
		type: 'POST',
		success: function(response){
			response = $.parseJSON(response);
			if(!response || response == "" || response == null || $.isEmptyObject(response)) {
				subscription_toggle_loader();
				location.href = location.href;
				return false;
			}

			var modal = $('#subscribeModal1');

			if(!response.success) {
				var error_msg;

				modal.attr('url',"admin/subscription/subscribe_error");
				modal.attr('plan_type',datalod.plan_type);
				modal.removeClass('wider');
				modal.addClass('shorter');
				subscription_load_modal({"error_msg":response.error_array,"upgrade_plan_type":data.plan_type});	

			}
			else {
				modal.find('.subscrcont').html('');
				if(response.action_type == "upgradeplan") {
					
					modal.attr('url',"admin/subscription/upgrade_subscription_success");
					modal.attr('plan_type',datalod.plan_type);
					modal.removeClass('wider');
					modal.addClass('shorter');
				}
				else {
					modal.attr('url',"admin/subscription/viewsettings");
					modal.attr('plan_type',datalod.plan_type);
					modal.removeClass('wider');
					modal.removeClass('shorter');
				}


				subscription_load_modal({address_id:response.address_id,
										customer_id:response.customer_id,
										subscription_id:response.subscription_id});	

				load_subscription_button();
			}

			shopstyx_subscription_paypal_action = false;
		},
		error: function(jqXHR, textStatus, errorThrown ) {
			subscription_toggle_loader();
			if(jqXHR.status == 404) {
				location.href = location.href;
				//location.href = "/admin/dashboard";
			}
		}
	});
}

function subscription_cancel(plan_type) {
	subscription_toggle_loader();
	
	var datalod = gatherReq();

	$.ajax({
		data: datalod,
		url: CS.main_url+'admin/subscription/subscription_cancel',
		type: 'POST',
		success: function(response){
			response = $.parseJSON(response);
			if(!response || response == "" || response == null || $.isEmptyObject(response)) {
				subscription_toggle_loader();
				location.href = location.href;
				return false;
			}

			var modal = $('#subscribeModal1');

			if(!response.success) {
				var error_msg;

				modal.attr('url',"admin/subscription/subscribe_error");
				modal.attr('plan_type',plan_type);
				modal.removeClass('wider');
				modal.addClass('shorter');
				subscription_load_modal({"error_msg":response.error_array,"upgrade_plan_type":data.plan_type});	

			}
			else {
				modal.find('.subscrcont').html('');
				modal.attr('url',"admin/subscription/subscription_cancel_success");
				modal.attr('plan_type',plan_type);

				
				subscription_load_modal({address_id:response.address_id,
										customer_id:response.customer_id,
										subscription_id:response.subscription_id},
										function() {
											modal.removeClass('wider');
											modal.addClass('shorter');
											modal.addClass('cancelled');	
										});	

				load_subscription_button();
			}

			shopstyx_subscription_paypal_action = false;
		},
		error: function(jqXHR, textStatus, errorThrown ) {
			subscription_toggle_loader();
			if(jqXHR.status == 404) {
				location.href = location.href;
				//location.href = "/admin/dashboard";
			}
		}
	});
}

function view_product_details(data) {
	$('.product-info-modal .mod-wrapper').html('');
	openModal();
	var form_data = {};
	form_data.isAjax = true;
	$.extend(form_data,data);
	showModalLoader();
	SSAjax({
		data: form_data,
		url: "/admin/catalog_actions?action=product_details",
		success: function(response){
			$('.product-info-modal .mod-wrapper').html(response);
			var height = $('.modal-header').height(),
				window_height = $(window).height();
			window_height = window_height - height;
			hideModalLoader();
		}
	});
}

function view_seller_order_details(order_id) {
	var modal_id = '#custom-order-modal';
	$(modal_id + ' .mod-wrapper').html('');
	openCustomModal(modal_id);
	var form_data = {isAjax:true,body_wrapper:true,order_id: order_id};
	showModalLoader();
	SSAjax({
		data: form_data,
		url: "/admin/orders/order_details",
		success: function(response){
			$(modal_id + ' .mod-wrapper').html(response);
			hideModalLoader();
		},
		error: function(jqXHR, textStatus, errorThrown ) {
			if(jqXHR.status == 404) {
				closeCustomModal(modal_id);
				hideModalLoader();
				//location.href = location.href;
			}
			else {
				location.href = location.href;
			}
		}
	});
}

function refund_transaction_order(order_id,pid,variant_id) {
	var wrapper = $('.custom-modal .mod-wrapper');
	var form_data = {isAjax:true,body_wrapper:true,order_id:order_id,product_id:pid,variant_id:variant_id};
	showModalLoader();
	//wrapper.addClass('hide');

	SSAjax({
		data: form_data,
		url: "/admin/orders/refund",
		success: function(response){
			if(!response || response == null || $.isEmptyObject(response)) {
				//location.href = "/";
				return false;
			}	

			response = $.parseJSON(response);	
			if(!response.hasOwnProperty('status')){
				//location.href = "/";
				return false;
			}

			if(response.status == "Error") {
				$('.custom-order-modal .order-stats .payment-btn-box').removeClass('hide');
				$('.custom-order-modal .order-stats .canc-order-btn').removeClass('hide');
				$('.custom-order-modal .mod-option .modal-close').removeClass('hide');
				hideModalLoader();
				wrapper.removeClass('hide');

				switch(response.error_code) {
					case 100: // Braintree error transaction (payment)
						alert(response.data.message);
						break;
					case 101: // No Products found
						alert(response.data.message);
						break;
					case 102: // Critical error: did not update status to mongoDB ("PaymentSuccessful" or "Shipped")
						alert(response.error_message);
						break;	

					default:	
					case 1:
						alert(response.error_message);
						break;	
				}
				view_seller_order_details(order_id);
			}	
			else {
				view_seller_order_details(order_id);
			}
		},
		error: function(jqXHR, textStatus, errorThrown ) {
			//location.href = "/";
		}
	});
}

function saveDesktopSettings(dataObj){
	var form_data = {};
	form_data.isAjax = true;
	$.extend(form_data,dataObj);
	SSAjax({
		data: form_data,
		url: '/admin/build?action=save-desktop-settings',
		success: function(response){
			$('.change-msg').addClass('hide');
			$('.save-colors-btn').css({'opacity':1}).addClass('hide').html('Save Changes');
			$('.save-desktop-settings-btn').addClass('hide');
			$('.save-desktop-settings-btn').css({'opacity':1}).html('Save Settings');
			$('.pcolor1').minicolors('value', $('.desktop-pcolor1').val());
			$('.pcolor2').minicolors('value', $('.desktop-pcolor2').val());
			$('.pcolor3').minicolors('value', $('.desktop-pcolor3').val());
			$('.pbody_background').minicolors('value', $('.desktop-pbody_background').val());
			$('.upload-image-logo').css({'background-color':$('.desktop-pcolor1').val(), 'color':$('.desktop-pbody_background').val() });
			$('.text-logo').css({'background-color':$('.desktop-pcolor1').val(), 'color':$('.desktop-pbody_background').val() });
			
			if($('.store-type').val() == 'desktop'){
				$('.pcolor1').minicolors('value', $('.desktop-pcolor1').val());
				$('.pcolor2').minicolors('value', $('.desktop-pcolor2').val());
				$('.pcolor3').minicolors('value', $('.desktop-pcolor3').val());
				$('.pbody_background').minicolors('value', $('.desktop-pbody_background').val());
				$('.tmpcolor1').val( $('.desktop-pcolor1').val() );
				$('.tmpcolor2').val( $('.desktop-pcolor2').val() );
				$('.tmpcolor3').val( $('.desktop-pcolor3').val() );
				$('.tmpcolor4').val( $('.desktop-pbody_background').val() );
				$('.upload-image-logo').css({'background-color':$('.desktop-pcolor1').val(), 'color':$('.desktop-pcolor2').val(), 'border-color':$('.desktop-pcolor3').val() });
				$('.text-logo-box').css({'background-color':$('.desktop-pcolor1').val(), 'color':$('.desktop-pcolor2').val(), 'border-color':$('.desktop-pcolor3').val() });
			}
		}
	});
}

function previewPage(dataObj){
	var form_data = {};
	form_data.isAjax = true;
	$.extend(form_data,dataObj);
	SSAjax({
		data: form_data,
		url: '/admin/build?page=preview_page',
		success: function(response){
			$('#pageFrame').removeClass('hide');
			if(form_data.page == 'tablet'){
				$('.tablet-frame').html(response);
				return false
			}
			if(form_data.page == 'phone'){
				$('.phone-frame').html(response);
				return false
			}
			$('.shop-preview-page').html(response);
		}
	});
}

function _upload_userpic(input) {
	$('.upic-' + store.get('user_id') + ' .upic-loader-overlay').removeClass('hide');
	$('.upic-' + store.get('user_id') + ' .upic-loader').removeClass('hide');

	var file = input[0].files;
	var form_data = new FormData();

	$.each(file, function(key, value)
	{
		form_data.append('userfile', value);
	});
	
	$.ajax({
		type: 'POST',
		data: form_data,
		cache: false,
		processData: false,
		contentType: false, 
		url: "/admin/account?action=upload&isAjax=true&token="+store.get('token')+"&user_id="+store.get('user_id'),
		success: function(response){
			var results = $.parseJSON(response);
			if(results.status == "success") {
				$.get(results.file, function(contents){
					$('.upic-' + store.get('user_id')).css('background-image','url('+results.file+')');
					$('.upic-' + store.get('user_id')).removeClass('image-hide');
				});
			}
			$('.upic-' + store.get('user_id') + ' .upic-loader-overlay').addClass('hide');
			$('.upic-' + store.get('user_id') + ' .upic-loader').addClass('hide');
		}
	});
}

function _update_pic_display($this) {
	$('.upic-loader-overlay').removeClass('hide');
	$('.upic-loader').removeClass('hide');
	var form_data = {};
		form_data.user_picture_display = parseInt($this.attr('data'));
	
	$.ajax({
		url: "/admin/account?action=update_picture_display&isAjax=true&token="+store.get('token')+"&user_id="+store.get('user_id'),
		type:'post',
		data: form_data,
		success: function(response){
			var results = $.parseJSON(response);
			if(results.status == "success") {
				if(form_data.user_picture_display == 0){
					$('.ipic-thumb').removeClass('hide');
					$('.upic-' + store.get('user_id')).addClass('image-hide');
				}
				if(form_data.user_picture_display == 1){
					$('.ipic-thumb').addClass('hide');
					$('.upic-' + store.get('user_id')).removeClass('image-hide');
				}
			}
			$('.upic-loader-overlay').addClass('hide');
			$('.upic-loader').addClass('hide');
		}
	});
}

function close_panels() {
	var el = $('#header .userNav');
		cart = el.find('.cart-panel'),
		wallet = el.find('.wallet-panel'),
		notification_panel = el.find('.notification-panel');

	el.find('.navLink').removeClass('selected');
	cart.find('.loader-overlay').addClass('hide');
	cart.find('.loader').addClass('hide');
	cart.addClass('hide');
	wallet.addClass('hide');
	notification_panel.addClass('hide');
}



function load_cart() {
	close_panels();
	
	var element = $('#header .userNav .navLink.cart'),
		container = element.find('.cart-panel.npops'),
		loader = container.find('.loader'),
		overlay = container.find('.loader-overlay'),	
		ajax_container = container.find('.inner-scroller');
		
	element.addClass('active');
	overlay.removeClass('hide');
	loader.removeClass('hide');
	container.removeClass('hide');
	
	SSAjax({
		url: '/admin/ajax?action=cart',
		data: {cart_sess:store.get('cart_sess')},
		success: function(response){
			overlay.addClass('hide');
			loader.addClass('hide');
			ajax_container.html(response);
		}
	});
}

function close_cart() {

	var element = $('#header .userNav .navLink.cart'),
		container = element.find('.cart-panel.npops');
	container.addClass('hide');
}

function load_header_cart(type) {
	if(type == "loading") {
		var element = $('#header .userNav .navLink.cart'),
			container = element.find('.cart-panel.npops'),
			loader = container.find('.loader'),
			overlay = container.find('.loader-overlay');	

		element.addClass('active');
		overlay.removeClass('hide');
		loader.removeClass('hide');
		container.removeClass('hide');
	}
	else {
		load_cart();
	}
}

function get_cart_total_items() {
	var form_data = {};
	form_data.user_id = store.get('user_id');
	form_data.token = store.get('token');
	form_data.isAjax = true;
	form_data._Ssfp_ID = store.get('_Ssfp_ID');
	
	$.ajax({
		url: root + 'admin/ajax?action=get_cart_total_items',
		type:'post',
		data: form_data,
		success: function(data){
			var _data = $.parseJSON(data);
			if(_data.cart_total_item > 0) {
				$('#header .userNav .navLink.cart .bg .total-counter').html(_data.cart_total_item).removeClass('hide');
			}
		}
	});
}

function delete_cart_item(el) {
	var input = el.parent().find('input');

	var quantity = input.val();
	var m = input.attr('m');
	var p = input.attr('p');
	var variant_id = input.attr('variant_id');
	var su = input.attr('su');
	var s = input.attr('s');
	var has_options = 0;
	var product_options = input.attr('options');
	
	if(typeof(variant_id) != 'undefined' && variant_id > 0) {
		has_options = 1;
	}
	var data = {product_user_id:m,
				product_id:p, 
				store_id: s,
				store_user_id: su,
				quantity: quantity,
				has_options: has_options,
				variant_id: variant_id,
				product_options:product_options,
				cart_sess: store.get('cart_sess')
				};


	SSAjax({
			data: data,
			url: '/admin/ajax?action=cart&loadtype=updatecart&update=delete_item',
			success: function(response){
				//var response = $.parseJSON(response);
				load_cart();
			}
	});						
}

function load_wallet() {
	close_panels();
	
	var element = $('#header .userNav .navLink.wallet'),
		container = element.find('.wallet-panel.npops'),
		loader = container.find('.loader'),
		overlay = container.find('.loader-overlay'),	
		ajax_container = container.find('.inner-scroller');
		
	element.addClass('active');
	overlay.removeClass('hide');
	loader.removeClass('hide');
	container.removeClass('hide');
	
	SSAjax({
		url: '/admin/ajax?action=wallet',
		data: {},
		success: function(response){
			overlay.addClass('hide');
			loader.addClass('hide');
			ajax_container.html(response);
		}
	});
}

function close_wallet() {
	var element = $('#header .userNav'),
		container = element.find('.wallet-panel.npops');
	container.addClass('hide');
}


function load_message(element) {
	close_panels();

	var container = element.find('.message-panel'),
		loader = container.find('.loader'),
		overlay = container.find('.loader-overlay'),	
		closebtn = container.find('.close-btn'),
		h3 = element.find('h3'),
		ajax_container = container.find('.cart-panel-inner-scoller');

	element.addClass('selected');
	overlay.removeClass('hide');
	loader.removeClass('hide');

	overlay.addClass('hide');
	loader.addClass('hide');
	container.removeClass('hide');		
}

function updateCartSizeContainer(){
	class_or_id = '#sub-wrapper #header .userNav .cart-panel.npops .inner-scroller';
	parent_class_or_id = '#sub-wrapper  #header .userNav .cart-panel.npops .top-wrapper'; 
	var window_height = $('#sub-wrapper  #header .userNav .cart-panel.npops').height();
	window_height = 385;
	$(class_or_id).removeAttr('style');
	$(class_or_id + ' .slimScrollBar, ' + class_or_id +' .slimScrollRail').remove();
	if($(parent_class_or_id + ' .slimScrollDiv').length > 0) {
		$(class_or_id).parent().replaceWith($(class_or_id));
	}
	
	$(class_or_id).slimScroll({
		height: window_height,
		width: '100%',
		distance: '6px'
	});
}

function updateWalletSizeContainer() {
	return;
	
	class_or_id = '#header .userNav .wallet-panel .panel-inner-scroller';
	parent_class_or_id = '#header .userNav .wallet-panel'; 
	//var window_height = $('#header .userNav .wallet-panel').height();
	var window_height = $(window).height() - 60;

	$(class_or_id).removeAttr('style');
	$(class_or_id + ' .slimScrollBar, ' + class_or_id +' .slimScrollRail').remove();
	if($(parent_class_or_id + ' .slimScrollDiv').length > 0) {
		$(class_or_id).parent().replaceWith($(class_or_id));
	}

	$(class_or_id).slimScroll({
		height: window_height,
		width: 'auto',
		distance: '4px'
	});
}
function resizeProductDetails(){
	var wH = $(window).height(),
		mH = wH - 68;
		mH2 = wH - 60;
	$('.prod-details-wrapper').css({'height': mH + 'px'});
	$('.modal-content').css({'height': mH + 'px'});
	$('.blog-content').css({'height': mH + 'px'});
}
function updateSizeContainer(class_or_id, parent_class_or_id){
	var height = $('.modal-header').height(),
		window_height = $(window).height();
	window_height = window_height - height;
	
	$(class_or_id).removeAttr('style');
	$(class_or_id + ' .slimScrollBar, ' + class_or_id +' .slimScrollRail').remove();
	if($(parent_class_or_id + ' .slimScrollDiv').length > 0) {
		$(class_or_id).parent().replaceWith($(class_or_id));
	}
	
	$(class_or_id).slimScroll({
		height: window_height,
		width: '100%',
		distance: '6px'
	});
	$('.prod-details-wrapper').css({'height': window_height + 'px'});
}
 
function toggleScroll_leftmenu() {
	$("#leftmenu-sc").removeAttr('style');
	$('#leftmenu-sc #slimScrollBar, #leftmenu-sc #slimScrollRail').remove();
	if($('.left-submenu .slimScrollDiv').length > 0) {
		$("#leftmenu-sc").parent().replaceWith($("#leftmenu-sc"));
	}

	var lm_height = $('.left-submenu').height();

	if(lm_height-95 < $('#leftmenu-sc').height()) {
		$('#leftmenu-sc').slimScroll({
			height: lm_height-1
		});
	}

	return;
} 

function convertproduct(options) {
	var settings = $.extend(true,{
			"url": "/admin/addproduct?action=basic",
			"data": {product_id:0},
			/*
			"loadJS": [cloud_url+'js/v2/addproduct.js'
						
					],
			"loadCSS": [cloud_url+'css/v2/addproduct.css'],
			*/
			"loadType": "default"
			//"onSaved": function(response) {},
			//"beforeClose": funcion() { }
	},options);

	var onSaved, beforeClose;
	if(settings.hasOwnProperty('onSaved')) {
		onSaved = settings.onSaved;
	}

	if(settings.hasOwnProperty('beforeClose')) {
		beforeClose = settings.beforeClose;
	}

	$('body').css({'overflow':'hidden'});

	
	settings.url = "/admin/addproduct?action=basic";
	$('#addproductModal').foundation('reveal', 'close');
	$('#addproductModal #modal-content').html("").hide();
	$('#wrapper').show().css('height','');
	$('#addproductModal .overlay').removeClass('hide');
	$('#addproductModal .shopstyxloader').removeClass('hide');

	$('#addproductModal-basic .overlay').removeClass('hide');
	$('#addproductModal-basic .shopstyxloader').removeClass('hide');
	$('#addproductModal-basic #modal-content').html('');
	$('#addproductModal-basic').show('slide', {direction: 'right'}, 400);
	

	settings.success = function(response){
		$('#addproductModal-basic .overlay').addClass('hide');
		$('#addproductModal-basic .shopstyxloader').addClass('hide');
		$('#addproductModal-basic #modal-content').html(response).show();
		$('.addproduct-modal form').data('image_files',[]);
		$('.addproduct-modal form').data('callback','');
		$('.addproduct-modal form').data('beforeClose','');

		if(typeof onSaved == "function") {
			$('.addproduct-modal form').data('callback', onSaved);			
		}

		if(typeof beforeClose == "function") {
			$('.addproduct-modal form').data('beforeClose', beforeClose);			
		}
	}

	SSAjax(settings);
}

function addproduct(options) {
	var settings = $.extend(true,{
			"url": "/admin/addproduct?action=basic",
			"data": {product_id:0},
			/*
			"loadJS": [cloud_url+'js/v2/addproduct.js'
						
					],
			"loadCSS": [cloud_url+'css/v2/addproduct.css'],
			*/
			"loadType": "default",
			"onSaved": function(response) {},
			"beforeClose": function() { }
	},options);

	var onSaved, beforeClose;
	if(settings.hasOwnProperty('onSaved')) {
		onSaved = settings.onSaved;
	}

	if(settings.hasOwnProperty('beforeClose')) {
		beforeClose = settings.beforeClose;
	}

	$('body').css({'overflow':'hidden'});

	if(settings.data.product_id > 0 || store.get('addproduct_modal') == "advanced") {
		settings.url = "/admin/addproduct?action=advanced";

		$('#addproductModal-basic').hide('slide', {direction: 'right'}, 400,function() {
			$(this).find('#modal-content').html("").hide();

			$('#addproductModal .shopstyxloader').removeClass('hide');
			$('#addproductModal #modal-content').html("").hide();
			//$('#wrapper').hide();
			//$('#addproductModal').css('left',0);
			//$('#addproductModal').foundation('reveal', 'open');
			$('#addproductModal').show('slide', {direction: 'right'}, 400);

			settings.success = function(response){
				if(settings.data.product_id > 0 || store.get('addproduct_modal') == "advanced") {
					$('#addproductModal .overlay').addClass('hide');
					$('#addproductModal .shopstyxloader').addClass('hide');
					$('#addproductModal #modal-content').html(response).show();
					$('#wrapper').css('height',$('#addproductModal').height()+2);
					
					resize_containers(); 

					if(typeof onSaved == "function") {
						$('.addproduct-modal form').data('callback', onSaved);			
					}

					if(typeof beforeClose == "function") {
						$('.addproduct-modal form').data('beforeClose', beforeClose);			
					}

				}
			}

			$('.addproduct-modal form').data('image_files',[]);
			$('.addproduct-modal form').data('callback','');
			$('.addproduct-modal form').data('beforeClose','');
			/*
			if(typeof onSaved == "function") {
				CS.addproductOnSave = onSaved;
				$('.addproduct-modal form').data('callback', onSaved);			
			}

			if(typeof beforeClose == "function") {
				$('.addproduct-modal form').data('beforeClose', beforeClose);			
			}
			*/
			SSAjax(settings);
		});
		return;
	}
	else {
		settings.url = "/admin/addproduct?action=basic";
		$('#addproductModal').foundation('reveal', 'close');
		$('#addproductModal #modal-content').html("").hide();
		$('#wrapper').show().css('height','');
		$('#addproductModal .overlay').removeClass('hide');
		$('#addproductModal .shopstyxloader').removeClass('hide');

		$('#addproductModal-basic .overlay').removeClass('hide');
		$('#addproductModal-basic .shopstyxloader').removeClass('hide');
		$('#addproductModal-basic #modal-content').html('');
		$('#addproductModal-basic').show('slide', {direction: 'right'}, 400);
	}

	settings.success = function(response){
		$('#addproductModal-basic .overlay').addClass('hide');
		$('#addproductModal-basic .shopstyxloader').addClass('hide');
		$('#addproductModal-basic #modal-content').html(response).show();
		$('.addproduct-modal form').data('image_files',[]);
		$('.addproduct-modal form').data('callback','');
		$('.addproduct-modal form').data('beforeClose','');

		if(typeof onSaved == "function") {
			$('.addproduct-modal form').data('callback', onSaved);			
		}

		if(typeof beforeClose == "function") {
			$('.addproduct-modal form').data('beforeClose', beforeClose);			
		}
	}

	SSAjax(settings);
}

function stepRedirect(current_step,referer){
	referer = typeof referer == "undefined" ? false : referer;
	if(store.get('user_type')){
		var dpromote = store.get('promoter_data');
		if(typeof dpromote !== "undefined"){
			SSAjax({
				url: '/admin/build?action=save-products',
				data: {isAjax:true,isContent:true,promote_data:dpromote},
				success: function(response){
					var _data = $.parseJSON(response);
					if( _data.no_error){
						store.remove('promoter_data');
					}
				}
			});
		}
	}
	
	if(current_step == 1){
		History.pushState(null, 'shop_info', '/admin/build?page=shop_info');
		mainURL = '/admin/build?page=shop_info';
	}
	if(current_step == 2){
		History.pushState(null, 'catalog', '/admin/build?page=catalog');
		mainURL = '/admin/build?page=catalog';
	}
	if(current_step == 3){
		History.pushState(null, 'get_paid', '/admin/build?page=get_paid');
		mainURL = '/admin/build?page=get_paid';
	}
	if(current_step == 4){
		History.pushState(null, 'shop_preview', '/admin/build?page=shop_preview');
		mainURL = '/admin/build?page=shop_preview';
	}


	if(current_step > 4){
		if(referer) {
			History.pushState(null, 'Shopstyx', referer);
			mainURL = referer;
			location.href = referer;
			return false;
		}
		else {
			History.pushState(null, 'shop_info', '/admin/dashboard');
			mainURL = '/admin/dashboard';
			location.href = root + 'admin/dashboard';
			return false;
		}
	}
	SSAjax({
		url: mainURL,
		data: {isAjax:true,isContent:true},
		loadJS: [cloud_url+'js/v2/build.js'],
		loadCSS: [cloud_url+'css/v2/build.css'],
		success: function(response){
			$('#wrapper').html(response);
			hideLoader();
			return false;
		}
	});
}

function showLoader(){
	$('#wrapper').addClass('hide');
	$('#loadbox-wrapper').removeClass('hide');
}

function hideLoader(){
	$('#loadbox-wrapper').addClass('hide');
	$('#wrapper').removeClass('hide');
}

function showModalLoader(){
	$('.mod-loader-wrapper').show();
}

function hideModalLoader(){
	$('.mod-loader-wrapper').hide();
}

function convertToSlug(Text)
{
	return Text
        .toLowerCase()
        .replace(/ /g,'-')
        .replace(/[^\w-]+/g,'');
}

function loadDepartments(dataObj){
	var form_data = {};
	form_data.isAjax = true;
	$.extend(form_data,dataObj);
	if(typeof form_data.loadProducts !== 'undefined' ){ $('.product-loader').removeClass('hide'); }
	SSAjax({
		data: form_data,
		url: "/admin/departments?action=load",
		success: function(response){
			if(form_data.department_level == 'main'){
				$('#department-menus').html(response);
				$('#department-sub-menus').addClass('hide');
				$('#department-menus').removeClass('hide');
			}
			if(form_data.department_level == 'sub'){
				$('#department-sub-menus').html(response);
				$('#department-menus').addClass('hide');
				$('#department-sub-menus').removeClass('hide');
				$('.dept-title').html(form_data.department_title);
				$('.all-dept-lbl').addClass('hide');
				//$('.dept-spacing').removeClass('hide');
				$('#back-departments').attr('data-id', form_data.parent_id);
				$('#back-departments').attr('data-catname', form_data.department_title);
			}
			$('.department-loading').remove();
			if(typeof form_data.loadProducts !== 'undefined' ){
				loadDepartmentProducts({department_id: form_data.department_id, level: form_data.level});
			}
		}
	});
}

function loadDepartmentProducts(dataObj){
	var form_data = {},
		link;
	form_data.isAjax = true;
	$.extend(form_data,dataObj);
	if(typeof form_data.more !== 'undefined'){
		link = '/admin/departments?action=load-products&page=' + form_data.page_num;
	}else{
		$('.product-loader').removeClass('hide');
		link = '/admin/departments?action=load-products';
	}
	SSAjax({
		data: form_data,
		url: link,
		success: function(response){
			if(typeof form_data.more !== 'undefined'){
				$('.more-product').before(response);
				$('.loader-image').addClass('hide');
			}else{
				$('.shop-product-wrapper').html(response);
				$('.shop-product-wrapper').append('<p class="more-product bgcolor3"><span class="more-button color4"><span class="loader-image hide"></span>Load more...</span></p>');
				var total_page = $('#h-total_pages').val(),
					current_page = $('#h-current_page').val();
				if(total_page > current_page){ 
					$('.more-product').removeClass('hide'); 
				}else{
					$('.more-product').addClass('hide'); 
				}
				$('.product-loader').addClass('hide');
			}
			$('.shop-left').addClass('hide2');
		}
	});
}

function loadProducts(dataObj){
	var form_data = {},
		link;
	form_data.isAjax = true;
	$.extend(form_data,dataObj);
	SSAjax({
		data: form_data,
		url: '/admin/build?page=catalog&page_num=' + form_data.page_num,
		success: function(response){ 
			//console.log('response');
			$('.promoter-products').append(response);
			var total_page = $('#c-total_pages').val(),
				current_page = $('#c-current_page').val();
			if(total_page > current_page){ 
				$('.more-promoter-products').removeClass('hide'); 
			}else{
				$('.more-promoter-products').addClass('hide'); 
			}
			$('.loader-image').addClass('hide');
		}
	});
}

var customer_purchase_order = function() {
	var height = $(window).height();
	var modal = $('.main-modal.modal-order-details');
	var overlay = $('.main-modal .overlay');
	var loader = $('.main-modal .loader');

	this.toggle_loader = function() {
		if(loader.hasClass('hide')) {
			loader.removeClass('hide');
		}
		else {
			loader.addClass('hide');
		}
	}

	this.open = function() {
		//modal.find('.mmodal-inner').html('');
		//modal.css({'height': height});
		$('body').css({'overflow':'hidden'});
		modal.show('slide', {direction: 'right'}, 400);
	}

	this.close = function() {
		modal.find('.mmodal-inner').html('');
		$('body').css({'overflow':'inherit'});
		modal.hide('slide', {direction: 'right'}, 200);
	}

	this.resizeModal = function() {
		class_or_id = '.main-modal.modal-order-details .mmodal-inner .content .inner-scoller';
		parent_class_or_id = '.main-modal.modal-order-details .mmodal-inner .content'; 
		var window_height = modal.height() - 68;
		
		$(class_or_id).removeAttr('style');
		$(class_or_id + ' .slimScrollBar, ' + class_or_id +' .slimScrollRail').remove();
		if($(parent_class_or_id + ' .slimScrollDiv').length > 0) {
			$(class_or_id).parent().replaceWith($(class_or_id));
		}
		
		$(class_or_id).slimScroll({
			height: window_height,
			width: '100%',
			distance: '6px'
		});

		$(parent_class_or_id).height(window_height);

	}
};

function openModal(){
	var height = $(window).height();
	$('.dashboard-modal .mod-wrapper').css({'height': height});
	$('body').css({'overflow':'hidden'});
	$('.dashboard-modal').show('slide', {direction: 'right'}, 400);
}
function closeModal(){
	$('body').css({'overflow':'inherit'});
	$('.dashboard-modal').hide('slide', {direction: 'right'}, 200);
}

function openModal2(){
	$('.dashboard-modal').show('slide', {direction: 'right'}, 400);
}
function openModal3(){
	$('.blog-modal2').show('slide', {direction: 'right'}, 400);
}
function closeModal2(){
	$('.dashboard-modal').hide('slide', {direction: 'right'}, 200);
}
function closeModal3(){
	$('.blog-modal2').hide('slide', {direction: 'right'}, 200);
}

function openCustomModal(id_or_class){
	var height = $(window).height();
	$(id_or_class + ' .mod-wrapper').css({'height': height});
	$('body').css({'overflow':'hidden'});
	$(id_or_class).show('slide', {direction: 'right'}, 400);
}
function closeCustomModal(id_or_class){
	$('body').css({'overflow':'inherit'});
	$(id_or_class).hide('slide', {direction: 'right'}, 200);
}
function closeCustomModal2(id_or_class){
	$(id_or_class).hide('slide', {direction: 'right'}, 200);
}
/* dot pre-loader function 
	sample:
	<span id="text-pre-loader" class="text-pre-loader-style bgcolor1 color3">Saving<span class="dots"></span></span>
*/
var dots = 0;
$(document).ready(function()
{
    setInterval (type, 600);
});

function type()
{
    if(dots < 3)
    {
        $('.dots').append('.');
        dots++;
    }
    else
    {
        $('.dots').html('');
        dots = 0;
    }
}

function checkdate(value){
	var validformat=/^\d{4}\/\d{2}\/\d{2}$/;

	if (!validformat.test(value)) {
		return false;
	}
	else{ 
		var month = value.split("/")[0], day = value.split("/")[1], year = value.split("/")[2];
		var myDate = new Date(year, month, day);

		if ((myDate.getMonth()+1!=month) || 
			(myDate.getDate()!=dayfield) ||
			(myDate.getFullYear()!=yearfield)) {
			return false;
		}
		else {
			return true;
		}
	}
}

function shopsetting_form_params(field) {
	var radioCheck = /radio|checkbox/i;
	var data = {},
				current;
	var keyBreaker = /[^\[\]]+/g;			

	$(field).each(function(){
		var el = this,
			type = el.type && el.type.toLowerCase();
		var key = el.name;
		var parts = key.match(keyBreaker),
			value = $.data(el, "value") || $.fn.val.call([el]),
			isRadioCheck = radioCheck.test(el.type),
			write = !isRadioCheck || !! $(el).attr('checked'),
			lastPart;

		//console.log(parts);
		current = data;
		for ( var i = 0; i < parts.length - 1; i++ ) {
			if (!current[parts[i]] ) {
				current[parts[i]] = {};
			}
			current = current[parts[i]];

			//console.log(current);
		}

		lastPart = parts[parts.length - 1];
		if ( lastPart in current && type === "checkbox" ) {
			if (!$.isArray(current[lastPart]) ) {
				current[lastPart] = current[lastPart] === undefined ? [] : [current[lastPart]];
			}
			if ( write ) {
				current[lastPart].push(value);
			}
		} else if ( write || !current[lastPart] ) {
			current[lastPart] = write ? value : undefined;
		}
	});

	return data;
}

function shopsetting_shipping_form_params() {
	var radioCheck = /radio|checkbox/i;
	var data = {},
				current;
	var keyBreaker = /[^\[\]]+/g;			

	$('.shopsettings input[name^="shipping_global_meta"]').each(function(){
		var el = this,
			type = el.type && el.type.toLowerCase();
		var key = el.name;
		var parts = key.match(keyBreaker),
			value = $.data(el, "value") || $.fn.val.call([el]),
			isRadioCheck = radioCheck.test(el.type),
			write = !isRadioCheck || !! $(el).attr('checked'),
			lastPart;

		//console.log(parts);
		current = data;
		for ( var i = 0; i < parts.length - 1; i++ ) {
			if (!current[parts[i]] ) {
				current[parts[i]] = {};
			}
			current = current[parts[i]];

			//console.log(current);
		}

		lastPart = parts[parts.length - 1];
		if ( lastPart in current && type === "checkbox" ) {
			if (!$.isArray(current[lastPart]) ) {
				current[lastPart] = current[lastPart] === undefined ? [] : [current[lastPart]];
			}
			if ( write ) {
				current[lastPart].push(value);
			}
		} else if ( write || !current[lastPart] ) {
			current[lastPart] = write ? value : undefined;
		}
	});

	return data;
}

function save_shopsettings(form) {
	$(window).scrollTop(0);

	if($('.shopsettings .save-btn').html() == "Saving...") {
		return;
	}

	toggle_preloader_maincontent();
	$('#body-wrapper .overlay').eq(0).addClass('hide');
	$('#subcontent-wrapper').eq(0).removeClass('hide');

	$('.shopsettings .save-btn').html('Saving...');
	$('.form-submission-message').stop().hide();

	var form_data = {action:'saveform',isAjax:true,body_wrapper:true,subcontent_wrapper:true};
	if(form.attr('name') != "shipping-settings-form") {
		form.serializeArray().map(function(x){
			form_data[x.name] = x.value;
		});
	}
	// for shipping form only
	else {
		$('.shopsettings table tr.add-country .error-shipping').addClass('hide');
		var shipping_data = shopsetting_shipping_form_params();
		$.extend(form_data,shipping_data);
		
		form_data.readytoship = $.trim(form.find('select[name="readytoship"]').val());
		form_data.readytoshiptext = $.trim(form.find('textarea[name="readytoshiptext"]').val());
		
		if($.isEmptyObject(shipping_data)) {
			$('.shopsettings tr.error-shipping').removeClass('hide');
			toggle_preloader_maincontent();
			$('.shopsettings .save-btn').html('Save Changes');
			if($('.shopsettings .save-btn').hasClass('update-paypal-email')){
				$('.shopsettings .save-btn').html('Update');
			}

			$('.form-submission-message').removeClass('ss-success');
			$('.form-submission-message')
				.addClass('ss-error')
				.fadeIn(500, function() {
					$('.shopsettings .save-btn').html('Save Changes');
					if($('.shopsettings .save-btn').hasClass('update-paypal-email')){
						$('.shopsettings .save-btn').html('Update');
					}
				});
			return;
		}
		else {
			$('.shopsettings tr.error-shipping').addClass('hide');
		}			
	}

	if(form.attr('name') == "tax-settings-form") {
		var tax_addedstate_length = $('.shopsettings .tax-wrapper li').not('.addstate-wrapper').find('.added-cityzip-wrapper div').length;

		if(tax_addedstate_length == 0) {
			/*
			toggle_preloader_maincontent();
			$('.form-submission-message').removeClass('ss-success');
			$('.shopsettings .tax-wrapper li.addstate-wrapper .common-field-wrapper').addClass('error').find('.error').html('Please add at least one state.');
			$('.form-submission-message')
				.addClass('ss-error')
				.fadeIn(500, function() {
					$('.shopsettings .save-btn').html('Saving Changes');
				});
			return;
			*/
		}
	}
	//$('.shopsettings form').attr('disabled','disabled').find('button.save-btn').attr('disabled','disabled');

	switch(form.attr('name')) {
		case 'general-settings-form': 
		case 'shipping-settings-form':
		case 'policy-settings-form': 
		case 'tax-settings-form':
			SSAjax({
				data: form_data,
				url: form.attr('action'),
				success:function(response) {
					var response = $.parseJSON(response);

					if(response.status != "success") {
						if(form.attr('name') != "shipping-settings-form") {
							for(e in response.errors) {
								if(e == "address") {
									form.find('textarea[name="'+e+'"]').parent().addClass('error').find('.error').html(response.errors[e]);
								} else {
									form.find('input[name="'+e+'"]').parent().addClass('error').find('.error').html(response.errors[e]);
								}
							}
						}
						
							if(form_data.country_code == "US" && form_data.state_code == "") {
								$('.seller-state .error2').html(response.errors.state_code);
							} else if(form_data.country_code != "US" && form_data.state_name == "") {
							   $('.seller-state .error2').html(response.errors.state_name);
							}
							
							if(typeof response.errors.statecityzip != 'undefined'){
								$('.vendor-info-wrapper .error3').html(response.errors.statecityzip);
							}
							
						$('.form-submission-message').removeClass('ss-success');
						$('.form-submission-message')
							.addClass('ss-error')
							.fadeIn(500, function() {
								$('.shopsettings .save-btn').html('Save Changes');
								if($('.shopsettings .save-btn').hasClass('update-paypal-email')){
									$('.shopsettings .save-btn').html('Update');
								}
							});
						
					}
					else {
						$('.form-submission-message').removeClass('ss-error');
						$('.form-submission-message')
							.addClass('ss-success')
							.fadeIn(500, function() {
								$('.shopsettings .save-btn').html('Save Changes');
								if($('.shopsettings .save-btn').hasClass('update-paypal-email')){
									$('.shopsettings .save-btn').html('Update');
								}
							})
							.fadeOut(10000);

						if($('#header .userNav #claim-wallet-form').hasClass('hide') == false) {
							claim_walet_popup();
						}


						//
						if(form.attr('name') == "general-settings-form") {
							if(response.data.country_code == "US") {
								$('.left-submenu ul li.link.sm-taxes').removeClass('hide');
							}
							else {
								$('.left-submenu ul li.link.sm-taxes').addClass('hide');
							}

						}
					}

					toggle_preloader_maincontent();
					$('#body-wrapper .overlay').eq(0).addClass('hide');
					$('#subcontent-wrapper').eq(0).removeClass('hide');
				}
			});
			break;	
	}
}

function reloadPage(dataObj){
	var form_data = {};
	form_data.isAjax = true;
	$.extend(form_data,dataObj);
	$(form_data.preloader).show();
	if(typeof form_data.clear_wrapper !== 'undefined'){
		$(form_data.clear_wrapper).html('');
	}
	$(form_data.wrapper).html('');
	SSAjax({
		data: form_data,
		url: form_data.link,
		success: function(response){
			$(form_data.wrapper).html(response);
			$(form_data.preloader).hide();
			$(form_data.preloader + ' .shopstyxloader div').html('LOADING...');
		}
	});
}

function toggle_statezip_autocomplete(opts) {
	opts = typeof opts == "undefined" ? {} : opts;
	var defaults = {enabled:true,
					state_code: '',
					state: 'select#state_code',
					city: 'input#city',
					zip: 'input#zip',
					};
	opts = $.extend(true,defaults,opts);
	if(opts.state_code == "") {
		if($(opts.state).val() == "" || $(opts.state).length == 0) {
			return;
		}
		else {
			opts.state_code = $(opts.state).val();
		}
	}

	if(!opts.enabled) {
		//$(opts.city).autocomplete( "option", "disabled", true );
		$(opts.zip).autocomplete( "option", "disabled", true );
	}
	else {
		//$(opts.city).autocomplete("enabled");
		$(opts.city).autocomplete({
			//define callback to format results  
			source: function(req, add){  
				//pass request to server  
				$.getJSON("/autocomplete/US_city.html?state_code="+opts.state_code+"&callback=?", req, function(data) {  
						//create array for response objects  
						var suggestions = [];  
						//process response  
						$.each(data, function(i, val){  
						suggestions.push(val.name);  
					});  

					//pass array to callback  
					add(suggestions);  
				});  
			}, 
			select: function(e, ui) {  
				//create formatted friend  
				var city = ui.item.value; 
				$.get("/autocomplete/US_zip.html?state_code="+opts.state_code+"&city="+city+"&callback=?", function(data) {
					data = data.replace('?','').replace('(','').replace(')','');
					data = $.parseJSON(data);
					//console.log(data);
					//console.log(data.length);
					if(data.length == 1) {
						$(opts.zip).val(data[0].name);	
					}
					else {
						$(opts.zip).val('');	
					}
				});
			},  
			//define select handler  
			change: function() {  
			}  
		}); 
		
		//$(opts.zip).autocomplete("enable");
		$(opts.zip).autocomplete({  
			//define callback to format results  
			source: function(req, add){  
				//pass request to server  
				$.getJSON("/autocomplete/US_zip.html?state_code="+opts.state_code+"&city="+$(opts.city).val()+"&callback=?", req, function(data) {  
						//create array for response objects  
						var suggestions = [];  
						//process response  
						$.each(data, function(i, val){  
						suggestions.push(val.name);  
					});  
					//pass array to callback  
					add(suggestions);  
				});  
			},  
			select: function(e, ui) {  
				//create formatted friend  
				var zip = ui.item.value; 
				$.get("/autocomplete/US_city.html?state_code="+opts.state_code+"&zip="+zip+"&callback=?", function(data) {
					data = data.replace('?','').replace('(','').replace(')','');
					data = $.parseJSON(data);
					//console.log(data);
					//console.log(data.length);
					if(data.length == 1) {
						$(opts.city).val(data[0].name);	
					}
					else {
						$(opts.city).val('');	
					}
				});
			}
		}); 
		
	}
}

function HomeLoader(){
	var user_id = store.get('user_id'),
		token = store.get('token'),
		login_data = {post_data:1,
						token:token,
						user_id:user_id
					};

	// check here if user is logged In
	$.post(root + "checkuser.html",login_data,function(data){
		var _data = $.parseJSON(data);
		if( _data.result){
			store.set('display_name', _data.display_name);
			store.set('user_id', _data.user_id);
			
			$('#wrapper').html('');
			$('#loadbox-wrapper').removeClass('hide');
			var form_data = {};
			form_data.token = token;
			form_data.user_id = user_id;
			form_data.isAjax = true;
			form_data.isWrapper = true;
			$.post(location.href, form_data,function(response){
				$('#wrapper').html(response);
				$('#loadbox-wrapper').addClass('hide');
				$('#wrapper').removeClass('hide');
				$('#loadbox-wrapper').addClass('hide');
			});
			
		}
		else {
			store.remove('token');
			store.remove('user_id');
			store.remove('display_name');
			store.remove('remember');
			$('#wrapper').removeClass('hide');
			$('#loadbox-wrapper').addClass('hide');
		}
	});	
}

function upload_shop_product_image(input,filename) {
	var form = $('#product-editor-modal form');
	var form_data = _support_html5_uploadform(form,input);
	if(!form_data) {
		return false;
	}
	var pid = form.attr('pid');
	
	$.ajax({
		type: 'POST',
		data: form_data,
		cache: false,
		processData: false,
		contentType: false,
		url: "/admin/catalog_actions?action=upload&token="+store.get('token')+"&user_id="+store.get('user_id')+"&type=productimage&filename="+filename+"&product_id="+pid,
		success: function(response){
			var results = $.parseJSON(response);
			input.parents('li').removeClass('uploading').removeClass('add-image').attr('sort_order',results.sort_order);
			$('#product-editor-modal .image-wrapper .uploading-status').addClass('hide');
		}
	});
}

function check_allowed_image_filetype(file_mime,file_ext) {
	file_ext = file_ext.toLowerCase();
	if($.inArray(file_mime,["images/jpeg","images/jpg","images/png","images/gif"]) == -1 &&
		$.inArray(file_mime,["jpeg","jpg","png","gif"]) == -1
		) {
		return false;
	}

	return true;
}

function _support_html5_uploadform(form, inputfile) {
	var files = false;
	if (inputfile.length > 0 && inputfile[0].files) {
		files = inputfile[0].files;
	} 

	if (window.FormData && files && files.length > 0) {
		var form_data = new FormData();
		$.each(files, function(key, value) {
			form_data.append('userfile', value);
		});

		return form_data;
	}

	return false;
}

function popUp(URL, data) {
	var day = new Date();
	var id = day.getTime();
	data = "_social_sharing_";
	eval("page" + id + " = window.open(URL, '" + id + "', 'directories=0,toolbar=0,scrollbars=0,location=0,statusbar=0,menubar=0,resizable=0,width=1000,height=430,left=200,top=50');");
}

/*
 *
 */
function promote_product(data) {
	var opts = {type:'fb',
				url:'',
				text:'',
				desc:'',
				image:'',
				current_url:''
				};

	data = (typeof data == "undefined" || $.isEmptyObject(data)) ? {} : data;
	opts = $.extend(opts,data);
	if($.isEmptyObject(opts.desc) || opts.desc == "") {
		opts.desc = opts.text;
	}

	switch(opts.type) {
		default:
		case 'fb':
			url = 'https://www.facebook.com/dialog/feed?app_id=142470092447374&link='+opts.url+'&picture='+opts.image+'&name='+opts.text+'&caption='+opts.text+'&description='+opts.desc+'&redirect_uri='+opts.current_url;
			break;
		case 'tw':
			url = 'http://twitter.com/share?count=false&original_referer='+opts.current_url+'&via=shopstyx.com&text='+opts.text+'&url='+opts.url;
			break;
		case 'gplus':
			url = 'https://plus.google.com/share?url='+opts.current_url;
			break;
		case 'linkedin':
			url = 'http://www.linkedin.com/shareArticle?mini=true&url='+opts.url+'&title='+opts.text+'&summary='+opts.desc+'&source=shopstyx.com';
			break;
		case 'pt':
			url = 'http://pinterest.com/pin/create/button/?url='+opts.url+'&media='+opts.image+'&description='+opts.desc;
			break;
	}
	popUp("/go?r="+escape(url)+'&u='+opts.uid+'&pu='+opts.pu+'&s='+opts.s+'&c='+opts.c+'&p='+opts.p);
}

var t = this;
var defdataopts, scrollopts, dataopts;

function init_scroll_opts(){
	if (!dataopts) dataopts = {};
	if (!scrollopts) scrollopts = {};
	defdataopts = {	user_id: store.get('user_id'),
		token: store.get('token'),
		isAjax: true,
		body_wrapper: false,
		subcontent_wrapper: false
		};
}

function load_scroll_opts(vartouse){
	dataopts[vartouse] = {};
	scrollopts[vartouse] = {
		page: 1,
		url: ''
	};
	reset_scroll_data_opts(vartouse);
}

function reset_scroll_page(vartouse){
	if(typeof scrollopts[vartouse] != "undefined") {
		scrollopts[vartouse].page = 1;
	}
}

function reset_scroll_data_opts(vartouse){
	dataopts[vartouse] = defdataopts;
}

function extend_scroll_opts(dat,vartouse){
	$.extend(dataopts[vartouse], dat, true);
}

function infinite_scroll(scrolltarget, vstorage, params, urlrequest, whereinsert, iswindow){
	init_scroll_opts();
	load_scroll_opts(vstorage);
	scrollopts[vstorage].url = urlrequest;
	scrollopts[vstorage].iscontent_loaded = false;
	var hascontent = true;
	var iscontent_loaded = false;
	extend_scroll_opts(params,vstorage);
	var loadMoreContent = function() {
		var url;
		scrollopts[vstorage].page++;
		if(scrollopts[vstorage].url.indexOf("?") == -1) {
			url = scrollopts[vstorage].url+'?page='+scrollopts[vstorage].page;
		}
		else {
			url = scrollopts[vstorage].url+'&page='+scrollopts[vstorage].page;
		}
		
		SSAjax({
			data: dataopts[vstorage],
			url: url,
			success: function(response){
				scrollopts[vstorage].iscontent_loaded = false;
				whereinsert = $(whereinsert.selector);
				/*
				var ss = whereinsert.attr('class').split(' ');
				ss = "." + ss.join('.');
				whereinsert = $(ss);
				*/
				
				if ($.trim(response) != '') {
				  	whereinsert.append(response);
				}else{
					hascontent = false;
				}	
				whereinsert.parent().find('.infinite-scroll-loader').addClass('hide');
			}
		});
	}

	var trigger_scroll = function(){
		if (hascontent && typeof(scrollopts[vstorage]) != "undefined" && !scrollopts[vstorage].iscontent_loaded){
			whereinsert.parent().find('.infinite-scroll-loader').removeClass('hide');
			var stargheight = (iswindow)?$(document).height():$(scrolltarget)[0].scrollHeight;
	  		if (scrolltarget.scrollTop() + 300  >= stargheight - scrolltarget.height()) {
	  			scrollopts[vstorage].iscontent_loaded = true;
	    		loadMoreContent();
	  		}
	  	}
	}
	
	scrolltarget.scroll(trigger_scroll);
}

function unli_scroll(params, data){
	$(params.scrollTarget).slimScroll().bind('slimscroll', function(e, pos){
		if(pos == 'bottom'){
			if($(params.loader).hasClass('hide')){
				if(parseInt($(params.total_product).val()) >= parseInt($(params.display_limit).val())){
					$(params.loader).removeClass('hide');
					data.isAjax = true;
					data.page = $(params.page_id).val();
					SSAjax({
						data: data,
						url: params.urlRequest,
						success: function(response){
							$(params.insertTo).append(response);
							$(params.loader).addClass('hide');
						}
					});
				}
			}
		}
	});
}

function show_prev_pub_guide(){
	$('.guide-prevpub').css('opacity',0).removeClass('hide').animate({opacity: 1}, 300);
}

function hide_prev_pub_guide(){
	$('.guide-prevpub').animate({opacity: 0}, 300, function(){
		$('.guide-prevpub').addClass('hide');
	});
}

function show_design_guide(){
	$('.guide-design').css('opacity',0).removeClass('hide').animate({opacity: 1}, 300);
}

function hide_design_guide(){
	$('.guide-design').animate({opacity: 0}, 300, function(){
		$('.guide-design').addClass('hide');
	});
}

