var catlistid = 1;
var drgOptions = {
	appendTo: ".build-product-wrapper, .bc-wrapper",
	helper: "clone",
	cursor: "move",
	opacity: 0.6,
	zIndex: 10
};

var drpOptions = {
	hoverClass: "drop-hover",
	drop: function( event, ui ) {
		var $data = ui.draggable;
		if(! $data.hasClass('slimScrollBar') && ! $data.hasClass('b-prod-added')){
			var $drop_img = $data.find('.b-product-img').attr('data-bg'),
				$drop_plist_id = $data.find('.b-product-img').attr('data-prodlistid'),
				$pid = $data.find('.b-product-img').attr('data-pid'),
				$puid = $data.find('.b-product-img').attr('data-puid');
			
			if($('.tmp-plistid').val() != $drop_plist_id){
				$( this )
				.find( '.product-images' )
				.prepend( '<span data-pid="' + $pid + '" data-puid="' + $puid + '" class="p-image prodlistid' + $drop_plist_id + '" style="' + $drop_img + '"></span>' );
				
				var img_length = $(this).find('.p-image').length;
				if(img_length > 0){ 
					$(this).find('.category-del').remove(); 
				}
				if(img_length == 1){ 
					$(this).addClass('cat-link'); 
				}
				if(img_length > 1){ 
					$(this).addClass('more');
				}
				if(img_length > 5){
					var more = img_length - 5;
					$(this).find('.more-lbl').html('(+' + more + ' more)'); 
				}
				
				var plist = '<div class="category-products prodlistid' + $drop_plist_id + '" style="' + $drop_img + '"> \
								<span class="product-del" data-prodlistid="' + $drop_plist_id + '">&#215;</span> \
								<span class="earn-bottom"></span> \
								<span class="earn-top color3">Earn $180</span> \
							</div>';
				var clist_id = $(this).find('.cat-title').attr('data-catlistid'); 
				$('.oncat' + clist_id).find('.category-products-list').prepend( plist );
				$('.b-prod-w' + $drop_plist_id).addClass('b-prod-added');
			}
		}
	},
	tolerance: "pointer"
};

var drpOptions2 = {
	drop: function( event, ui ) {
		var $data = ui.draggable;
		if(! $data.hasClass('slimScrollBar') && ! $data.hasClass('b-prod-added')){
			var $drop_img = $data.find('.b-product-img').attr('data-bg'),
				$drop_plist_id = $data.find('.b-product-img').attr('data-prodlistid'),
				$pid = $data.find('.b-product-img').attr('data-pid'),
				$puid = $data.find('.b-product-img').attr('data-puid');
			
			if($('.tmp-plistid').val() != $drop_plist_id){	
				var plist = '<div class="category-products prodlistid' + $drop_plist_id + '" style="' + $drop_img + '"> \
								<span class="product-del" data-prodlistid="' + $drop_plist_id + '">&#215;</span> \
								<span class="earn-bottom"></span> \
								<span class="earn-top color3">Earn $180</span> \
							</div>';
				$(this).prepend( plist );
				var clist_id = $(this).parents('.category-products-wrapper').attr('data-catlistid');
				$('.category-list' + clist_id)
				.find( '.product-images' )
				.prepend( '<span data-pid="' + $pid + '" data-puid="' + $puid + '" class="p-image prodlistid' + $drop_plist_id + '" style="' + $drop_img + '"></span>' );
				
				var img_length = $('.category-list' + clist_id).find('.p-image').length;
				if(img_length > 0){ 
					$('.category-list' + clist_id).find('.category-del').remove(); 
				}
				if(img_length == 1){ 
					$('.category-list' + clist_id).addClass('cat-link'); 
				}
				if(img_length > 1){ 
					$('.category-list' + clist_id).addClass('more');
				}
				if(img_length > 5){
					var more = img_length - 5;
					$('.category-list' + clist_id).find('.more-lbl').html('(+' + more + ' more)'); 
				}
				
				$('.b-prod-w' + $drop_plist_id).addClass('b-prod-added');
			}
		}
		$(this).parents('.b-product-listing-wrapper').removeClass('drop-hover');
	},
	tolerance: "pointer",
	over: function( event, ui ) {
		$(this).parents('.b-product-listing-wrapper').addClass('drop-hover');
	},
	out: function( event, ui ) {
		$(this).parents('.b-product-listing-wrapper').removeClass('drop-hover');
	}
};


$(document).ready(function(){
	$( ".bc-left li, .b-contain-left li" ).draggable( drgOptions );
	//$( ".b-contain-left li" ).draggable( drgOptions );
	$( ".category-list" ).droppable( drpOptions );
	//http://api.jqueryui.com/droppable/
	$( ".add-new-cat-btn" ).droppable({
		hoverClass: "drop-hover",
		drop: function( event, ui ) {
			var $data = ui.draggable;
			if(! $data.hasClass('b-prod-added')){
				$( this )
				.parent( ".b-droppable-category" )
				.find( ".add-new-cat-btn" )
				.addClass( "hide" )
				.end()
				.find( ".add-new-cat-box" )
				.removeClass( "hide" );
				$('.add-new-cat-box input').focus();
				var $data = ui.draggable,
					$drop_img = $data.find('.b-product-img').attr('data-bg'),
					$drop_plist_id = $data.find('.b-product-img').attr('data-prodlistid');
					$drop_pid = $data.find('.b-product-img').attr('data-pid');
					$drop_puid = $data.find('.b-product-img').attr('data-puid');
				$('.tmp-img').html($drop_img);
				$('.tmp-plistid').val($drop_plist_id);
				$('.tmp-pid').val($drop_pid);
				$('.tmp-puid').val($drop_puid);
			}
		}
	});
	$(document).on('touchstart click', '#add-new-category-btn', function(){
		var catname = $.trim($('#add-new-category-inpt').val());
		if(catname != ''){
			var tmp_img = $('.tmp-img').html(),
				tmp_plistid = $('.tmp-plistid').val(),
				tmp_pid = $('.tmp-pid').val(),
				tmp_puid = $('.tmp-puid').val(),
				catlist = '<div class="category-list ui-droppable cat-link  category-list' + catlistid + '" data-catlistid="' + catlistid + '"> \
								<div class="b-cat-box1"></div> \
								<div class="b-cat-box2"></div> \
								<div class="b-cat-box3"></div> \
								<div class="b-cat-box4"> \
									<span class="cat-title color1" data-catlistid="' + catlistid + '">' + catname + '</span> \
									<div class="product-images"> \
										<span data-pid="' + tmp_pid + '" data-puid="' + tmp_puid + '" class="p-image prodlistid' + tmp_plistid + '" style="' + tmp_img + '"></span> \
									</div> \
									<span class="color1 more-lbl cat-link" data-catlistid="' + catlistid + '"></span> \
								</div> \
							</div>';
			$('#add-new-category-inpt').val('');
			$( catlist ).droppable( drpOptions ).prependTo( '.added-category-wrapper' );
			$('.add-new-cat-box').addClass('hide');
			$('.add-new-cat-btn').removeClass('hide');
			var prodlist = '<div class="category-products-wrapper hide oncat' + catlistid + '" data-catlistid="' + catlistid + '"> \
								<div class="category-name-wrapper"> \
									<div> \
										<span class="bgcolor3"></span> \
										<span class="bgcolor3"></span> \
										<span class="bgcolor3"></span> \
										<span class="bgcolor3"></span> \
									</div> \
									<h2 class="color3">' + catname + '</h2> \
								</div> \
								<div class="category-products-list"> \
									<div class="category-products prodlistid' + tmp_plistid + '" style="' + tmp_img + '"> \
										<span class="product-del" data-prodlistid="' + tmp_plistid + '">&#215;</span> \
										<span class="earn-bottom"></span> \
										<span class="earn-top color3">Earn $180</span> \
									</div> \
								</div> \
								<span class="backto-categories color3" href="javascript:;" data-catlistid="' + catlistid + '"><span class="back-icon"></span> Back</span> \
							</div>';
			$('.b-product-listing-wrapper').prepend( prodlist );
			$('.b-prod-w' + tmp_plistid).addClass('b-prod-added');
			$( ".oncat" + catlistid + " .category-products-list" ).droppable( drpOptions2 );
			catlistid++;
			if($('.category-list').length > 0){
				$('.build-promoter').removeClass('hide');
				$('.build-promoter2').removeClass('hide');
				$('.upload-for-vendor').slideUp(500);
				$('.tips-box').addClass('hide');
			}
		}
	});

	$(document).on('keydown', '#add-new-category-inpt', function(e){
		if(e.which == 13){
			var catname = $.trim($(this).val());
			if(catname != ''){
				var tmp_img = $('.tmp-img').html();
					tmp_plistid = $('.tmp-plistid').val();
					tmp_pid = $('.tmp-pid').val(),
					tmp_puid = $('.tmp-puid').val(),
					catlist = '<div class="category-list ui-droppable cat-link category-list' + catlistid + '" data-catlistid="' + catlistid + '"> \
									<div class="b-cat-box1"></div> \
									<div class="b-cat-box2"></div> \
									<div class="b-cat-box3"></div> \
									<div class="b-cat-box4"> \
										<span class="cat-title cat-link color1" data-catlistid="' + catlistid + '">' + catname + '</span> \
										<div class="product-images"> \
											<span data-pid="' + tmp_pid + '" data-puid="' + tmp_puid + '" class="p-image prodlistid' + tmp_plistid + '" style="' + tmp_img + '"></span> \
										</div> \
										<span class="color1 more-lbl cat-link" data-catlistid="' + catlistid + '"></span> \
									</div> \
								</div>';
				$(this).val('');
				$( catlist ).droppable( drpOptions ).prependTo( '.added-category-wrapper' );
				$('.add-new-cat-box').addClass('hide');
				$('.add-new-cat-btn').removeClass('hide');
				
				var prodlist = '<div class="category-products-wrapper hide oncat' + catlistid + '" data-catlistid="' + catlistid + '"> \
									<div class="category-name-wrapper"> \
										<div> \
											<span class="bgcolor3"></span> \
											<span class="bgcolor3"></span> \
											<span class="bgcolor3"></span> \
											<span class="bgcolor3"></span> \
										</div> \
										<h2 class="color3">' + catname + '</h2> \
									</div> \
									<div class="category-products-list"> \
										<div class="category-products prodlistid' + tmp_plistid + '" style="' + tmp_img + '"> \
											<span class="product-del" data-prodlistid="' + tmp_plistid + '">&#215;</span> \
											<span class="earn-bottom"></span> \
											<span class="earn-top color3">Earn $180</span> \
										</div> \
									</div> \
									<span class="backto-categories color3" href="javascript:;" data-catlistid="' + catlistid + '"><span class="back-icon"></span> Back</span> \
								</div>';
				$('.b-product-listing-wrapper').prepend( prodlist );
				$('.b-prod-w' + tmp_plistid).addClass('b-prod-added');
				$( ".oncat" + catlistid + " .category-products-list" ).droppable( drpOptions2 );
				catlistid++;
				if($('.category-list').length > 0){
					$('.build-promoter').removeClass('hide');
					$('.build-promoter2').removeClass('hide');
					$('.upload-for-vendor').slideUp(500);
					$('.tips-box').addClass('hide');
				}
			}
		}
	});
		
	$(document).on('click', '.product-del', function(){
		var cat_prod_wrapper_id = $(this).parents('.category-products-wrapper').attr('data-catlistid');
		var plist_wrapper = $(this).parents('.category-products-list');
		var plist_id = $(this).attr('data-prodlistid');
		$('.prodlistid' + plist_id).remove();
		$('.b-prod-w' + plist_id).removeClass('b-prod-added');
		var plist = plist_wrapper.find('.category-products').length;
		if(plist <= 0){ 
			$('.category-list' + cat_prod_wrapper_id).prepend('<span class="category-del">&#215;</span>'); 
			$('.category-list' + cat_prod_wrapper_id).removeClass('cat-link');
		}
		if(plist <= 1){
			$('.category-list' + cat_prod_wrapper_id).removeClass('more');
		}
		if(plist > 5){
			var more = plist - 5;
			$('.category-list' + cat_prod_wrapper_id).find('.more-lbl').html('(+' + more + ' more)'); 
		}else{
			$('.category-list' + cat_prod_wrapper_id).find('.more-lbl').html(''); 
		}
	});
	$(document).on('click', '.category-del', function(){
		$(this).parent('.category-list').remove();
		if($('.category-list').length < 1){
			$('.build-promoter').addClass('hide');
			$('.build-promoter2').addClass('hide');
			$('.tips-and-cmd h2').addClass('hide');
			$('.tips-box').removeClass('hide');
			$('.build-product-wrapper').animate({ marginLeft: '0' }, 300);
			$('.tips-and-cmd h2').addClass('hide');
		}
	});
	
	$(document).on('click', '.cat-link', function(){
		var clist_id = $(this).attr('data-catlistid');
		$('.b-category-wrapper').addClass('hide');
		$('.b-product-listing-wrapper').removeClass('hide');
		$('.oncat' + clist_id).removeClass('hide');
		$('.tips-and-cmd').addClass('hide');
	});
	
	$(document).on('touchstart click', '.more-btn', function(e){
		e.preventDefault();
		e.stopImmediatePropagation();
		$('.loader-image').removeClass('hide');
		var page_num = parseInt($('#h-current_page').val()) + 1;
		var search_text = $.trim($('#h-search_text').val());
		$('#h-search_text').remove();
		$('#h-total_pages').remove();
		$('#h-current_page').remove();
		var page = root + '?page=' + page_num;
			form_data = {},
			form_data.search_text = search_text;
			form_data.more = true;
			form_data.isPass = true;
			form_data.isAjax = true;
		
		if($(this).hasClass('page-catalog')){
			page = root + 'admin/build?page=catalog&pages=' + page_num;
			SSAjax({
				data: form_data,
				url: page,
				success: function(response){
					$('.b-contain-left-data ul').append(response);
					$('.category-products').each(function(){
						var prodlist_id = $(this).find('.product-del').attr('data-prodlistid');
						$('.b-prod-w' + prodlist_id).addClass('b-prod-added');
					})
					$( ".bc-left li, .b-contain-left li" ).draggable( drgOptions );
					var tpage = $('#tpage').val(),
						current_page = $('#h-current_page').val();
					if(tpage > current_page){
						$('.more-product').removeClass('hide'); 
					}else{
						$('.more-product').addClass('hide'); 
					}
					$('.loader-image').addClass('hide');
				}
			});
		}else{
			$.post(page, form_data,function(data){
				$('.b-contain-left-data ul').append(data);
				$('.category-products').each(function(){
					var prodlist_id = $(this).find('.product-del').attr('data-prodlistid');
					$('.b-prod-w' + prodlist_id).addClass('b-prod-added');
				})
				$( ".bc-left li, .b-contain-left li" ).draggable( drgOptions );
				var tpage = $('#tpage').val(),
					current_page = $('#h-current_page').val();
				if(tpage > current_page){
					$('.more-product').removeClass('hide'); 
				}else{
					$('.more-product').addClass('hide'); 
				}
				$('.loader-image').addClass('hide');
			});
		}
		
		
	
		return false;
	});
	
	$(document).on('keypress', '#search_product', function(e){
		e.stopImmediatePropagation();
		if (e.which == 13) {
			$('.more-product').addClass('hide'); 
			$(this).removeClass('input_error');
			var search_text = $.trim($(this).val());
			
			if(search_text == ""){
				$(this).addClass('input_error');
				return false;
			}
			
			var page = root;
				form_data = {},
				form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
				form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
				form_data.search_text = search_text;
				form_data.isPass = true;
				form_data.isAjax = true;

			$('#h-search_text').remove();
			$('#h-total_pages').remove();
			$('#h-current_page').remove();
			$('.loader-wrapper').show();

			if($(this).hasClass('page-catalog')){
				page = root + 'admin/build?page=catalog';
				SSAjax({
					data: form_data,
					url: page,
					success: function(response){
						$('.b-contain-left-data ul').html(response);
						$('.category-products').each(function(){
							var prodlist_id = $(this).find('.product-del').attr('data-prodlistid');
							$('.b-prod-w' + prodlist_id).addClass('b-prod-added');
						});
						$( ".bc-left li, .b-contain-left li" ).draggable( drgOptions );
						var total_page = $('#h-total_pages').val(),
							current_page = $('#h-current_page').val();
						if(total_page > current_page){ 
							$('.more-product').removeClass('hide'); 
						}else{
							$('.more-product').addClass('hide'); 
						}
						$('.loader-wrapper').hide();
					}
				});
			}else{
				$.post(page, form_data,function(data){
					$('.b-contain-left-data ul').html(data);
					$('.category-products').each(function(){
						var prodlist_id = $(this).find('.product-del').attr('data-prodlistid');
						$('.b-prod-w' + prodlist_id).addClass('b-prod-added');
					});
					$( ".bc-left li, .b-contain-left li" ).draggable( drgOptions );
					var total_page = $('#h-total_pages').val(),
						current_page = $('#h-current_page').val();
					if(total_page > current_page){ 
						$('.more-product').removeClass('hide'); 
					}else{
						$('.more-product').addClass('hide'); 
					}
					$('.loader-wrapper').hide();
				});
			}
			return false;
		}
	});
	
	$(document).on('touchstart click', '.search-btn', function(e){
		e.stopImmediatePropagation();
		$('.more-product').addClass('hide'); 
		$('#search_product').removeClass('input_error');
		var search_text = $.trim($('#search_product').val());
		
		if(search_text == ""){
			$('#search_product').addClass('input_error');
			return false;
		}
		
		var $this = $(this);
			page = root;
			form_data = {},
			form_data.search_text = search_text;
			form_data.isPass = true;
			form_data.isAjax = true;
			
		$('#h-search_text').remove();
		$('#h-total_pages').remove();
		$('#h-current_page').remove();
		$('.loader-wrapper').show();
		
		if($this.hasClass('page-catalog')){
			page = root + 'admin/build?page=catalog';
			SSAjax({
				data: form_data,
				url: page,
				success: function(response){
					$('.b-contain-left-data ul').html(response);
					$('.category-products').each(function(){
						var prodlist_id = $(this).find('.product-del').attr('data-prodlistid');
						$('.b-prod-w' + prodlist_id).addClass('b-prod-added');
					});
					$( ".bc-left li, .b-contain-left li" ).draggable( drgOptions );
					var total_page = $('#h-total_pages').val(),
						current_page = $('#h-current_page').val();
					if(total_page > current_page){ 
						$('.more-product').removeClass('hide'); 
					}else{
						$('.more-product').addClass('hide'); 
					}
					$('.loader-wrapper').hide();
				}
			});
		}else{
			$.post(page, form_data,function(data){
				$('.b-contain-left-data ul').html(data);
				$('.category-products').each(function(){
					var prodlist_id = $(this).find('.product-del').attr('data-prodlistid');
					$('.b-prod-w' + prodlist_id).addClass('b-prod-added');
				});
				$( ".bc-left li, .b-contain-left li" ).draggable( drgOptions );
				var total_page = $('#h-total_pages').val(),
					current_page = $('#h-current_page').val();
				if(total_page > current_page){ 
					$('.more-product').removeClass('hide'); 
				}else{
					$('.more-product').addClass('hide'); 
				}
				$('.loader-wrapper').hide();
			});
		}
		
		return false;
	});
	
	$(document).on('click', '.backto-categories', function(){
		var clist_id = $(this).attr('data-catlistid');
		$('.oncat' + clist_id).addClass('hide');
		$('.b-product-listing-wrapper').addClass('hide');
		$('.b-category-wrapper').removeClass('hide');
		$('.tips-and-cmd').removeClass('hide');
	});
	
	$('.b-contain-left-data').slimScroll({
		height: '474px',
		width: '716px',
		start: 'top',
		wheelStep : 2,
		disableFadeOut: true
	});
	
	$('.added-category-wrapper').slimScroll({
		height: '290px',
		width: '263px',
		start: 'top',
		wheelStep : 2,
		disableFadeOut: true
	});
	
	$(document).on('touchstart mouseover', '.category-products-list', function(){
		if(! $(this).parent('div').hasClass('slimScrollDiv')){
			$(this).slimScroll({
				height: '356px',
				width: '232px',
				start: 'top',
				wheelStep : 2,
				disableFadeOut: true
			});
		}
	});
});

function getPromoterData(){
	var category_data = '[';
	var c = 0;
	var cat_data = new Array();
	$('.category-list').each(function(){
		var $products = $(this).find('.p-image'),
		$category_name = $(this).find('.cat-title').html();
		cat_data[c] = '{"category_name":"'+$category_name+'","product_data":[';
		var product_data = new Array();
		i = 0;
		$products.each(function(){
			var $prod = $(this),
				$product_id = $prod.attr('data-pid');
				$product_user_id = $prod.attr('data-puid');
				product_data[i] = '{"product_id":"'+ $product_id +'","product_user_id":"'+ $product_user_id +'"}'
				i++;
		});
		product_data.join(',');
		cat_data[c] = cat_data[c] + product_data + ']}';
		c++;
	});
	cat_data.join(',');
	category_data = category_data + cat_data + ']';
	var promoter_data = $.parseJSON(category_data);
	store.set('promoter_data', promoter_data);
	
	// var dpromote = store.get('promoter_data');
	// $.each(dpromote, function(i,category){
		// console.log(category.category_name);
		// $.each(category.product_data, function(i,prod){
			// console.log('product_id' + prod.product_id);
			// console.log('product_user_id' + prod.product_user_id);
		// });
	// });
}