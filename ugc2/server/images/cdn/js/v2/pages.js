$(document).foundation();
$(document).ready(function(){
	
	$(document).on('click', '.pageform-modal-close', function(){
		closeCustomModal('#manage-page-modal');
	});
	
	$(document).on('click', '.add-new-page', function(event){
		event.stopImmediatePropagation();
		var settings = $.parseJSON($(this).attr('data-val'));
		var modal_id = '#manage-page-modal';
		$(modal_id + ' .mod-wrapper').html('');
		openCustomModal(modal_id);
		reloadAddEditPage(settings);
	});
	
	$(document).on('click', '.add-new-sub-page', function(event){
		event.stopImmediatePropagation();
		var settings = $.parseJSON($(this).attr('data-val'));
		var modal_id = '#manage-page-modal';
		$(modal_id + ' .mod-wrapper').html('');
		openCustomModal(modal_id);
		reloadAddEditPage(settings);
	});
	
	$(document).on('click', '.activate_page', function(event){
		event.stopImmediatePropagation();
		var form_data = {};
		form_data.isAjax = true;
		var settings = $.parseJSON($(this).attr('data-val'));
		$.extend(form_data,settings);

		if(form_data.process == 'deactivate'){
			$('.shopstyxloader div').html('DEACTIVATING...');
		}
		if(form_data.process == 'activate'){
			$('.shopstyxloader div').html('ACTIVATING...');
		}
		
		
		$('.manage-page-loader').removeClass('hide');
		$('.bg-overlay').removeClass('hide');
		SSAjax({
			data: form_data,
			url: "/admin/page_actions?action=activate_page",
			success: function(response){
				if(settings.show == "sub pages"){
					reloadManagePage({show:'sub pages',page_id:settings.parent_id, link:'/admin/page_actions?action=reload_pages', wrapper: '#sub-content-wrapper-shop-page', preloader: '.manage-page-loader'});
				}else{
					reloadManagePage({link:'/admin/page_actions?action=reload_pages', wrapper: '#sub-content-wrapper-shop-page', preloader: '.manage-page-loader'});
				}
			}
		});
	});
	
	$(document).on('click', '.edit_page', function(event){
		event.stopImmediatePropagation();
		var settings = $.parseJSON($(this).attr('data-val'));
		var modal_id = '#manage-page-modal';
		$(modal_id + ' .mod-wrapper').html('');
		openCustomModal(modal_id);
		reloadAddEditPage(settings);
	});
	
	$(document).on('click', '.delete_page', function(event){
		event.stopImmediatePropagation();
		var settings = $.parseJSON($(this).attr('data-val'));
		if (! confirm("Are you sure you want to delete " + settings.title + " ?")) {
			return false;
		}
		
		if(settings.total_subpage > 0){
			alert("Delete first the sub-pages before deleting the main page...");
			return false;
		}

		var form_data = {};
		form_data.isAjax = true;
		$.extend(form_data,settings);
		$('.shopstyxloader div').html('DELETING...');
		$('.manage-page-loader').removeClass('hide');
		$('.bg-overlay').removeClass('hide');
		SSAjax({
			data: form_data,
			url: "/admin/page_actions?action=delete_page",
			success: function(response){
				if(settings.show == "sub pages"){
					reloadManagePage({show:'sub pages',page_id:settings.parent_id, link:'/admin/page_actions?action=reload_pages', wrapper: '#sub-content-wrapper-shop-page', preloader: '.manage-page-loader'});
				}else{
					reloadManagePage({link:'/admin/page_actions?action=reload_pages', wrapper: '#sub-content-wrapper-shop-page', preloader: '.manage-page-loader'});
				}
			}
		});
	});
	
	$(document).on('click', '.del-page', function(event){
		event.stopImmediatePropagation();
		var settings = $.parseJSON($(this).attr('data-val'));
		if (! confirm("Are you sure you want to delete " + settings.title + " ?")) {
			return false;
		}
		
		if(settings.total_subpage > 0){
			alert("Delete first the sub-pages before deleting the main page...");
			return false;
		}
		
		var sett = $.parseJSON($('.thepage').attr('data-val'));
		parent_page_id = parseInt(sett.parent_id);
		var form_data = {};
		form_data.isAjax = true;
		$.extend(form_data,settings);
		$('.bg-overlay').removeClass('hide');
		SSAjax({
			data: form_data,
			url: "/admin/page_actions?action=delete_page",
			success: function(response){
				if(parent_page_id > 0){
					reloadManagePage({show:'sub pages',page_id: parent_page_id, link:'/admin/page_actions?action=reload_pages', wrapper: '#subcontent-wrapper', preloader: '.manage-page-loader'});
				}else{
					reloadManagePage({link:'/admin/page_actions?action=reload_pages', wrapper: '#subcontent-wrapper', preloader: '.manage-page-loader'});
				}
				$('.pageform-modal-close').trigger('click');
			}
		});
	});
	
	$(document).on('click', '.show_sub_pages', function(event){
		event.stopImmediatePropagation();
		$('.bg-overlay').removeClass('hide');
		reloadManagePage({show:'sub pages',page_id: $(this).attr('page_id'), link:'/admin/page_actions?action=reload_pages', wrapper: '#subcontent-wrapper', preloader: '.manage-page-loader'});
		return false;
	});
	
	$(document).on('click', '.backtomainpage', function(event){
		event.stopImmediatePropagation();
		$('.bg-overlay').removeClass('hide');
		reloadManagePage({link:'/admin/page_actions?action=reload_pages', wrapper: '#subcontent-wrapper', preloader: '.manage-page-loader'});
		return false;
	});
	
	$(document).on('click', '.publish-page', function(event){
		event.stopImmediatePropagation();
		$('.activated').val(1);
		$('.save_page_btn').trigger("click");
		return false;
	});
	$(document).on('click', '.draft-page', function(event){
		event.stopImmediatePropagation();
		$('.activated').val(0);
		$('.save_page_btn').trigger("click");
		return false;
	});
	$(document).on('click', '.deactivate-page', function(event){
		event.stopImmediatePropagation();
		$('.activated').val(0);
		$('.save_page_btn').trigger("click");
		$(this).addClass('activate-page').removeClass('deactivate-page').html('ACTIVATE');
		$('.common-publish').addClass('draft-page').removeClass('publish-page');
		return false;
	});
	$(document).on('click', '.activate-page', function(event){
		event.stopImmediatePropagation();
		$('.activated').val(2);
		$('.save_page_btn').trigger("click");
		$(this).addClass('deactivate-page').removeClass('activate-page').html('DEACTIVATE');
		$('.common-publish').addClass('publish-page').removeClass('draft-page');
		return false;
	});
	$(document).on('click', '.save_page_btn', function(event){
		event.stopImmediatePropagation();
		$(this).parents("form").trigger("submit");
		return false;
	});

	$(document).on('submit', '#savePageSettings', function(event){
		event.preventDefault();
		event.stopImmediatePropagation(); //console.log($('.activated').val());
		tinyMCE.triggerSave();
        $form = $(this);
		
		if($('.activated').val() == 0){
			$('#manage-page-modal .mod-loader-wrapper .shopstyxloader div').html('DEACTIVATING...');
		}
		if($('.activated').val() == 1){
			$('#manage-page-modal .mod-loader-wrapper .shopstyxloader div').html('UPDATING...');
		}
		if($('.activated').val() == 2){
			$('#manage-page-modal .mod-loader-wrapper .shopstyxloader div').html('ACTIVATING...');
			$('.activated').val(1);
		}
		
		$('#manage-page-modal .mod-loader-wrapper').show();
		
		var error_status = 0;
		$('#store_name').removeClass('input_error');
		
		url = $form.attr( 'action' );
		var form_data = {};
		
		form_data.store_id = 1;
		form_data.id = $.trim($form.find( 'input[name="id"]' ).val());
		form_data.title = $.trim($form.find( 'input[name="title"]' ).val());
		form_data.original_title = $.trim($form.find( 'input[name="original_title"]' ).val());
		form_data.system_generated = $.trim($form.find( 'input[name="system_generated"]' ).val());
		form_data.activated = $.trim($form.find( 'input[name="activated"]' ).val());
		form_data.content = $.trim($form.find( 'textarea[name="content"]' ).val());
		form_data.parent_id = $form.find( 'select[name="parent_page"]' ).val();
		form_data.allow_comments = $form.find( 'input:checkbox[name=allow_comments]:checked' ).val();
		form_data.enable_share = $form.find( 'input:checkbox[name=enable_share]:checked' ).val();
		form_data.show_email = $form.find( 'input:checkbox[name=show_email]:checked' ).val();
		var settings = $.parseJSON($('.thepage').attr('data-val'));
		form_data.parent_page_id = settings.parent_id;
		form_data.isAjax = true;
		$('.bg-overlay').removeClass('hide');	
		SSAjax({
			data: form_data,
			url: url,
			success: function(response){
				if(parseInt(form_data.parent_page_id) > 0){
					reloadManagePage({show:'sub pages',page_id:form_data.parent_page_id, link:'/admin/page_actions?action=reload_pages', wrapper: '#sub-content-wrapper-shop-page', preloader: '.manage-page-loader'});
				}else{
					reloadManagePage({link:'/admin/page_actions?action=reload_pages', wrapper: '#sub-content-wrapper-shop-page', preloader: '.manage-page-loader'});
				}
				$('#manage-page-modal .mod-loader-wrapper').hide();
			}
		});
		
		return false; 
    });

});

function reloadManagePage(dataObj){
	var form_data = {};
	form_data.isAjax = true;
	$.extend(form_data,dataObj);
	$(form_data.preloader).removeClass('hide');
	SSAjax({
		data: form_data,
		url: form_data.link,
		success: function(response){
			$(form_data.wrapper).html(response);
			$(form_data.preloader).addClass('hide');
			if ( $('#manage-page-modal').css('display') == 'none')
			{
			   $('body').css({'overflow':'inherit'});
			}
			$('.shopstyxloader div').html('LOADING...');
			$('.bg-overlay').addClass('hide');
		}
	});
}

function reloadAddEditPage(dataObj){
	var modal_id = '#manage-page-modal';
	$(modal_id + ' .mod-wrapper').html('');
	openCustomModal(modal_id);
	var $form = $(this),
		form_data = {};
	form_data.isAjax = true;
	$.extend(form_data,dataObj);
	if(typeof form_data.page_link !== "undefined"){
		page = form_data.page_link;
	}
	showModalLoader();
	SSAjax({
		data: form_data,
		url: "/admin/page_actions?action=load_page_form",
		success: function(response){
			$(modal_id + ' .mod-wrapper').html(response);
			hideModalLoader();
		}
	});
	
}