var image_counter = 0,
	total_img = 0,
	$search_text = '';
$(document).ready(function() {
	$(document).foundation();
	
	$(document).on('open', '#stickModal', function (e) {
		$(this).removeClass('stick-product-modal2');
		$(this).removeClass('stick-product-modal3');
		$(this).centerized();
		e.preventDefault(); e.stopImmediatePropagation();

		modal_class = '#stickModal';
		page = '/admin/stick_products?action=add';
		$(modal_class + ' .stick-wrapper').html('');
		$(modal_class + ' .stick-loader').removeClass('hide');

		var form_data = {};
		form_data.isAjax = true;
		SSAjax({
			data: form_data,
			url: page,
			success: function(response){
				$(modal_class + ' .stick-wrapper').html(response);
				$(modal_class + ' .stick-loader').addClass('hide');
			}
		});
	});
	
	$(document).on('open', '#stickAddModal', function (e) {
		$(this).centerized();
	});
	
	$(document).on('open', '#addSearchModal', function (e) {
		$(this).centerized();
		e.preventDefault(); e.stopImmediatePropagation();

		modal_class = '.stick-product-modal3';
		page = '/admin/stick_products?action=add_search';
		$(modal_class + ' .stick-wrapper').html('');
		$(modal_class + ' .stick-loader').removeClass('hide');

		var form_data = {};
		form_data.isAjax = true;
		form_data.search_text = $search_text;
		SSAjax({
			data: form_data,
			url: page,
			success: function(response){
				$(modal_class + ' .stick-wrapper').html(response);
				//$(modal_class + ' .stick-loader').addClass('hide');
			}
		});
	});
	
	$(document).on('click', '.add-product-close', function(){
		$('#stickModal').foundation('reveal', 'close');
		$('.reveal-modal-bg').remove();
	});
	$(document).on('click', '.search-product-close', function(){
		$('#addSearchModal').foundation('reveal', 'close');
		$('.reveal-modal-bg').remove();
	});
	$(document).on('click', '.add-search-step1-close', function(){
		$('.search-modals').addClass('hide');
		$('.add-search-step1').addClass('hide');
		$('.add-search-step1-content').html('');
	});
	$(document).on('click', '.add-search-step2-close', function(){
		if($('.add-search-step1').hasClass('hide')){
			$('.search-modals').addClass('hide');
		}
		$('.add-search-step2').addClass('hide');
		$('.add-search-step2-content').html('');
	});
	$(document).on('click', '.add-product-step1-close', function(){
		$('.add-product-step1-wrapper').addClass('hide');
		$('.add-product-step1-content').html('');
	});
	$(document).on('click', '.add-product-step2-close', function(){
		$('.add-product-step2-wrapper').addClass('hide');
		$('.add-product-step2-content').html('');
	});
	
	$(document).on('mouseover', '.product-url', function(){
		$('.url-field').removeClass('error');
	});
	
	$(document).on('submit', '#addUrlForm', function(e){
		e.preventDefault(); e.stopImmediatePropagation();
		
		var $form = $(this),
			url = $form.attr( 'action' );
			form_data = {};
		form_data.isAjax = true;
		form_data.url = $.trim($('.product-url').val());
		$('.stick-product-modal .stick-loader').removeClass('hide');
		$('.error2').addClass('hide');
		SSAjax({
			data: form_data,
			url: url,
			success: function(response){
				var _data = $.parseJSON(response),
					c = 0,
					image_counter = 0;
				if(_data.url_exist){
					var page = '/admin/stick_products?action=add_url_existing',
						form_data = {};
					form_data.product_site_name = _data.site_name;
					form_data.product_url = _data.web_url;
					form_data.product_name = _data.product_name;
					form_data.product_price = '$' + _data.price;
					form_data.product_image = _data.image;
					form_data.product_type = _data.product_type;
					form_data.isAjax = true;
					$('.stick-product-modal .stick-loader').removeClass('hide');
					$('.add-product-step2-wrapper').removeClass('hide');
					SSAjax({
						data: form_data,
						url: page,
						success: function(response){
							//$('.stick-product-modal .stick-wrapper').html(response);
							$('.add-product-step2-content').html(response);
							$('.stick-product-modal .stick-loader').addClass('hide');
						}
					});
				}
					
				if(! _data.url_exist){	
					if(_data.image != ''){
						$('.tmp-images').append('<li class="active" style="background-image: url(\' ' + _data.image + ' \')" ><input type="hidden" name="product_image" value="' + _data.image + '" /></li>');
						image_counter++; c++;
					}
					total_img = _data.images.length - 1;
					$.each(_data.images, function(i,item){
						var newImg = new Image();
						newImg.src = item;
						var height = newImg.height,
							width = newImg.width;
							
						$(newImg).load(function(){
							if(parseInt(newImg.width) >= 250 || parseInt(newImg.height) >= 250){
								if( c == 0 ){
									$('.tmp-images').append('<li class="active" style="background-image: url(\' ' + item + ' \')" ><input type="hidden" name="product_image" value="' + item + '" /></li>');
								}
								if( c > 0 ){
									$('.tmp-images').append('<li style="background-image: url(\' ' + item + ' \')" ><input type="hidden" name="product_image" value="' + item + '" /></li>');
								}
								image_counter++; c++;
								if(image_counter > 7){ total_img = 1; }
							}
							total_img--;
						});
					});
					
					function checkImg( ) {
						if(total_img <= 1){
							if(image_counter > 0){
								var imageList = $('.tmp-images').html(),
									page = '/admin/stick_products?action=new_url_step1';
								//$('.stick-product-modal .stick-wrapper').html('');
								$('.add-product-step1-wrapper').removeClass('hide');
								var form_data2 = {};
								form_data2.isAjax = true;
								SSAjax({
									data: form_data2,
									url: page,
									success: function(result){
										//$('.stick-product-modal .stick-wrapper').html(result);
										$('.add-product-step1-content').html(result);
										$('.stick-product-site_name').val(_data.site_name);
										$('.stick-product-url').val(_data.web_url);
										$('.stick-product-name').val(_data.product_name);
										$('.stick-product-price').val('$' + _data.price);
										$('.choose-img-content ul').html(imageList);
										$('.stick-product-modal .stick-loader').addClass('hide');
									}
								});
							}
							if(image_counter == 0){
								$('.error2').removeClass('hide');
								$('.stick-product-modal .stick-loader').addClass('hide');
							}
							image_counter = 0;
						}else{
							setTimeout( checkImg, 30 );
						}
					}
					checkImg();
				}
			}
		});
		
		return false;
	});
	
	$(document).on('touchstart, click', '.stick-for span', function(){
		$('.stick-for span').removeClass('selected');
		$(this).addClass('selected');
	});
	
	$(document).on('touchstart, click', '.choose-img-content ul li', function(){
		$('.choose-img-content ul li').removeClass('active');
		$(this).addClass('active');
	});
	
	$(document).on('touchstart, click', '.stick-add-btn, .sengine-stick-add-btn', function(e){
		e.preventDefault(); e.stopImmediatePropagation();
		var $this = $(this),
			error = 0;
		
		$('.stick-product-name').removeClass('error');
		$('.stick-product-price').removeClass('error');
		if($('.stick-product-name').val() == ''){
			$('.stick-product-name').addClass('error');
			error++;
		}
		if($('.stick-product-price').val() == ''){
			$('.stick-product-price').addClass('error');
			error++;
		}
		if($('.stick-product-price').val() == '$'){
			$('.stick-product-price').addClass('error');
			error++;
		}
		if(error){ return false; }
		var page = '/admin/stick_products?action=new_url_step2',
			form_data = {};
		form_data.product_site_name = $('.stick-product-site_name').val();
		form_data.product_url = $('.stick-product-url').val();
		form_data.product_name = $('.stick-product-name').val();
		form_data.product_price = $('.stick-product-price').val();
		form_data.product_image = $('.choose-img-content ul li.active input').val();
		form_data.product_type = $('.stick-for span.selected').attr('data');
		form_data.product_keyword = $('.get-search-text').val();
		form_data.isAjax = true;
		$('.stick-product-modal .stick-loader').removeClass('hide');
		$('.stick-product-modal3 .stick-loader').removeClass('hide');
		SSAjax({
			data: form_data,
			url: page,
			success: function(response){
				if($this.hasClass('sengine-stick-add-btn')){
					//$('.stick-product-modal3 .add-search-step1-content').html('');
					$('.tmp-images').html('');
					//$('.stick-product-modal3 .add-search-step1').addClass('hide');
					$('.stick-product-modal3 .add-search-step2').removeClass('hide');
					$('.stick-product-modal3 .add-search-step2-content').html(response);
					$('.stick-product-modal3 .stick-loader').addClass('hide');
					$('.add-search-step2 .stick-a-product').remove();
					$('.add-search-step2 .arrow-down').remove();
					$('.save-to-btn').removeClass('save-collection-btn');
					$('.save-to-btn').addClass('sengine-save-collection-btn');
				}else{
					$('.stick-product-modal .stick-wrapper').html(response);
					$('.stick-product-modal .stick-loader').addClass('hide');
				}
			}
		});
	});
	
	$(document).on('touchstart, click', '.new-collection-btn', function(){
		$(this).remove();
		$('.cw-a-content ul').append('<li class="collection-input"><textarea type="text" class="collection-name" name="collection_name" placeholder="New Collection Name"></textarea><span class="add-btn add-collection-btn"><span class="plus"></span></span></li>');
	});
	
	$(document).on('touchstart, click', '.add-collection-btn', function(){
		var collection_name = $.trim($('.collection-name').val());
		if(collection_name == ''){
			return false;
		}
		$('.collection-input').remove();
		$('.collection-box').removeClass('active'); //added script
		$('.cw-a-content ul').append('<li class="collection-box active"><div class="collection-txt">' + collection_name + '</div><input type="hidden" name="collection_id" value="new" /><input type="hidden" name="collection_name" value="' + collection_name + '" /></li>');
		$('.cw-a-content ul').append('<li class="collection-add-btn new-collection-btn"><span class="collection-plus"></span></li>');
	});
	
	$(document).on('touchstart, click', '.collection-box', function(){
		$('.collection-box').removeClass('active');
		$(this).addClass('active');
		// if($(this).hasClass('active')){
			// $(this).removeClass('active');
		// }else{
			// $(this).addClass('active');
		// }
	});
	
	$(document).on('touchstart, click', '.save-collection-btn, .sengine-save-collection-btn', function(e){
		e.preventDefault(); e.stopImmediatePropagation();
		$('.no-collection').html('');
		var page = '/admin/stick_products?action=save_collection',
			$this = $(this),
			form_data = {};
		form_data.product_site_name = $('.stick-product-site_name').val();
		form_data.product_url = $('.stick-product-url').val();
		form_data.product_name = $('.stick-product-name').val();
		form_data.product_price = $('.stick-product-price').val();
		form_data.product_image = $('.stick-product-image').val();
		form_data.product_type = $('.stick-product-type').val();
		form_data.product_keyword = $('.stick-product-keyword').val();
		form_data.product_description = $('.stick-product-description').val();
		form_data.added_first = $('.stick-product-exist').val();
		form_data.isAjax = true;
		
		var collection_id = [],  
			collection_name = [];  
		
		$('.cw-a-content ul li.active input[name="collection_id"]').each(function(){
			collection_id.push($(this).val()); 
		});
		$('.cw-a-content ul li.active input[name="collection_name"]').each(function(){
			collection_name.push($(this).val()); 
		});
		form_data.collection_id = collection_id;
		form_data.collection_name = collection_name;
		
		if (form_data.collection_id.length === 0) {
			$('.no-collection').html('Please add or select collection.');
			return false;
		}
		
		$('.stick-product-modal .stick-loader .shopstyxloader div').html('SAVING...');
		$('.stick-product-modal3 .stick-loader .shopstyxloader div').html('SAVING...');
		$('.stick-product-modal .stick-loader').removeClass('hide');
		$('.stick-product-modal3 .stick-loader').removeClass('hide');
		SSAjax({
			data: form_data,
			url: page,
			success: function(response){
				if($this.hasClass('sengine-save-collection-btn')){
					$('.stick-product-modal3 .stick-loader').addClass('hide');
					$('.stick-product-modal3 .stick-loader .shopstyxloader div').html('LOADING...');
					$('.stick-product-modal3 .search-modals').addClass('hide');
					$('.stick-product-modal3 .add-search-step2').addClass('hide');
				}else{
					$('.stick-product-modal .stick-loader').addClass('hide');
					$('.stick-product-modal .stick-loader .shopstyxloader div').html('LOADING...');
					$('#stickModal').foundation('reveal', 'close');
				}
			}
		});
	});
	
	
	$(document).on('submit', '#sengineForm', function(e){
		e.preventDefault(); e.stopImmediatePropagation();
		$search_text = $.trim($('.search-text').val());
		if($.trim($('.search-text').val()) == ''){
			return false;
		}
		$('#addSearchModal').foundation('reveal', 'open');
		
		// $('.error3').addClass('hide');
		// var $form = $(this),
			// url = $form.attr( 'action' );
			// form_data = {};
		// form_data.isAjax = true;
		// form_data.search_text = $.trim($('.search-text').val());
		// $('.search-results-content ul').html('');
		// $('.stick-product-modal .sengine-loader-box').removeClass('hide');
		// SSAjax({
			// data: form_data,
			// url: url,
			// success: function(response){
				// $('.search-results-content ul').html(response);
				// $('.stick-product-modal .sengine-loader-box').addClass('hide');
			// }
		// });
		// return false;
	});
	
	$(document).on('focus', '.search-text, .sengine-search-text', function(){
		$(this).val('');
	});
	
	
	$(document).on('submit', '#sengineForm2', function(e){
		e.preventDefault(); e.stopImmediatePropagation();
		$('.error3').addClass('hide');
		var $form = $(this),
			url = $form.attr( 'action' );
			//url = root + 'admin/stick_products?action=external_products';
			form_data = {};
		form_data.isAjax = true;
		form_data.search_text = $.trim($('.sengine-search-text').val());
		form_data.color = $.trim($form.find( 'input[name="color"]' ).val());
		form_data.site = $.trim($form.find( 'select[name="site"]' ).val());;
		if(form_data.search_text == ''){
			return false;
		}
		
		$('.search-results-content ul').html('');
		$('.stick-product-modal3 .stick-loader .shopstyxloader div').html('SEARCHING...');
		$('.stick-product-modal3 .stick-loader').removeClass('hide');
		SSAjax({
			data: form_data,
			url: url,
			success: function(response){
				$('.image-lists ul').html(response);
				$('.stick-product-modal3 .stick-loader').addClass('hide');
				$('.stick-product-modal3 .stick-loader .shopstyxloader div').html('LOADING...');
			}
		});
		return false;
	});
	
	$(document).on('touchstart click', '.colors-content ul li', function(){
		$('.colors-content ul li').removeClass('selected');
		$(this).addClass('selected');
		$('.color-option').val($(this).attr('data'));
		
		$('.error3').addClass('hide');
		var $form = $('#sengineForm2'),
			url = $form.attr( 'action' );
			form_data = {};
		form_data.isAjax = true;
		form_data.search_text = $.trim($('.sengine-search-text').val());
		form_data.color = $.trim($form.find( 'input[name="color"]' ).val());
		form_data.site = $.trim($form.find( 'select[name="site"]' ).val());;
		if(form_data.search_text == ''){
			return false;
		}
		
		$('.search-results-content ul').html('');
		$('.stick-product-modal3 .stick-loader .shopstyxloader div').html('SEARCHING...');
		$('.stick-product-modal3 .stick-loader').removeClass('hide');
		SSAjax({
			data: form_data,
			url: url,
			success: function(response){
				$('.image-lists ul').html(response);
				$('.stick-product-modal3 .stick-loader').addClass('hide');
				$('.stick-product-modal3 .stick-loader .shopstyxloader div').html('LOADING...');
			}
		});
		return false;
		
	});
	
	$(document).on('change', '.site-option', function(){
		$('.error3').addClass('hide');
		var $form = $('#sengineForm2'),
			url = $form.attr( 'action' );
			form_data = {};
		form_data.isAjax = true;
		form_data.search_text = $.trim($('.sengine-search-text').val());
		form_data.color = $.trim($form.find( 'input[name="color"]' ).val());
		form_data.site = $.trim($form.find( 'select[name="site"]' ).val());;
		if(form_data.search_text == ''){
			return false;
		}
		
		$('.search-results-content ul').html('');
		$('.stick-product-modal3 .stick-loader .shopstyxloader div').html('SEARCHING...');
		$('.stick-product-modal3 .stick-loader').removeClass('hide');
		SSAjax({
			data: form_data,
			url: url,
			success: function(response){
				$('.image-lists ul').html(response);
				$('.stick-product-modal3 .stick-loader').addClass('hide');
				$('.stick-product-modal3 .stick-loader .shopstyxloader div').html('LOADING...');
			}
		});
		return false;
		
	});
	
	$(document).on('touchstart click', '.search-results-content ul li', function(e){
		e.preventDefault(); e.stopImmediatePropagation();
		
		//var url = '/admin/scripter?action=product_urls';
		var url = '/admin/stick_products?action=external_single_product',
			form_data = {};
		form_data.isAjax = true;
		form_data.url = $.trim($(this).find('input').val());
		//form_data.new_product = true;
		form_data.product_image = $(this).find('img').attr('src');
		$('.stick-product-modal .stick-loader').removeClass('hide');
		$('.error3').addClass('hide');
		SSAjax({
			data: form_data,
			url: url,
			success: function(response){
				var _data = $.parseJSON(response),
					c = 0,
					image_counter = 0;
				if(_data.url_exist){
					var page = '/admin/stick_products?action=add_url_existing',
						form_data = {};
					form_data.product_site_name = _data.site_name;
					form_data.product_url = _data.web_url;
					form_data.product_name = _data.product_name;
					form_data.product_price = '$' + _data.price;
					form_data.product_image = _data.image;
					form_data.product_type = _data.product_type;
					form_data.product_keyword = _data.keyword;
					form_data.isAjax = true;
					$('.stick-product-modal .stick-loader').removeClass('hide');
					$('.add-product-step2-wrapper').removeClass('hide');
					SSAjax({
						data: form_data,
						url: page,
						success: function(response){
							//$('.stick-product-modal .stick-wrapper').html(response);
							$('.add-product-step2-content').html(response);
							$('.stick-product-modal .stick-loader').addClass('hide');
						}
					});
					return false
				}
				
				if(_data.image != ''){
					$('.tmp-images').append('<li class="active" style="background-image: url(\' ' + _data.image + ' \')" ><input type="hidden" name="product_image" value="' + _data.image + '" /></li>');
					image_counter++; c++;
				}
				total_img = _data.images.length - 1;
				$.each(_data.images, function(i,item){
					var newImg = new Image();
					newImg.src = item;
					var height = newImg.height,
						width = newImg.width;
						
					$(newImg).load(function(){
						if(parseInt(newImg.width) >= 250 || parseInt(newImg.height) >= 250){
							if( c == 0 ){
								$('.tmp-images').append('<li class="active" style="background-image: url(\' ' + item + ' \')" ><input type="hidden" name="product_image" value="' + item + '" /></li>');
							}
							if( c > 0 ){
								$('.tmp-images').append('<li style="background-image: url(\' ' + item + ' \')" ><input type="hidden" name="product_image" value="' + item + '" /></li>');
							}
							image_counter++; c++;
							if(image_counter > 7){ total_img = 1; }
						}
						total_img--;
					});
				});
				
				function checkImg( ) {
					if(total_img <= 1){
						if(image_counter > 0){
							var imageList = $('.tmp-images').html(),
								page = '/admin/stick_products?action=new_url_step1';
							$('.stick-product-modal .stick-wrapper').html('');
							var form_data2 = {};
							form_data2.isAjax = true;
							SSAjax({
								data: form_data2,
								url: page,
								success: function(result){
									$('.stick-product-modal .stick-wrapper').html(result);
									$('.stick-product-site_name').val(_data.site_name);
									$('.stick-product-url').val(_data.web_url);
									$('.stick-product-name').val(_data.product_name);
									$('.stick-product-price').val('$' + _data.price);
									$('.choose-img-content ul').html(imageList);
									$('.stick-product-modal .stick-loader').addClass('hide');
								}
							});
						}
						if(image_counter == 0){
							$('.error3').removeClass('hide');
							$('.stick-product-modal .stick-loader').addClass('hide');
						}
						image_counter = 0;
					}else{
						setTimeout( checkImg, 30 );
					}
				}
				checkImg();
				
			}
		});
		
		return false;
	});
	
	
	$(document).on('touchstart click', '.image-lists ul li', function(e){
		e.preventDefault(); e.stopImmediatePropagation();
		
		//var url = '/admin/scripter?action=product_urls';
		var url = '/admin/stick_products?action=external_single_product',
			form_data = {};
		form_data.isAjax = true;
		form_data.url = $.trim($(this).find('input').val());
		form_data.product_image = $(this).find('img').attr('src');
		$('.stick-product-modal3 .stick-loader').removeClass('hide');
		$('.error4').addClass('hide');
		SSAjax({
			data: form_data,
			url: url,
			success: function(response){
				var _data = $.parseJSON(response),
					c = 0,
					image_counter = 0;
				if(_data.url_exist){
					var page = '/admin/stick_products?action=add_url_existing',
						form_data = {};
					form_data.product_site_name = _data.site_name;
					form_data.product_url = _data.web_url;
					form_data.product_name = _data.product_name;
					form_data.product_price = '$' + _data.price;
					form_data.product_image = _data.image;
					form_data.product_type = _data.product_type;
					form_data.product_keyword = $('.get-search-text').val();
					form_data.isAjax = true;
					$('.stick-product-modal3 .stick-loader').removeClass('hide');
					SSAjax({
						data: form_data,
						url: page,
						success: function(response){
							$('.stick-product-modal3 .search-modals').removeClass('hide');
							$('.stick-product-modal3 .add-search-step2').removeClass('hide');
							$('.stick-product-modal3 .add-search-step2-content').html(response);
							$('.stick-product-modal3 .stick-loader').addClass('hide');
							$('.add-search-step2 .stick-a-product').remove();
							$('.add-search-step2 .arrow-down').remove();
							$('.save-to-btn').removeClass('save-collection-btn');
							$('.save-to-btn').addClass('sengine-save-collection-btn');
							
						}
					});
					return false
				}
				
				if(_data.image != ''){
					$('.tmp-images').append('<li class="active" style="background-image: url(\' ' + _data.image + ' \')" ><input type="hidden" name="product_image" value="' + _data.image + '" /></li>');
					image_counter++; c++;
				}
				total_img = _data.images.length - 1;
				$.each(_data.images, function(i,item){
					var newImg = new Image();
					newImg.src = item;
					var height = newImg.height,
						width = newImg.width;
						
					$(newImg).load(function(){
						if(parseInt(newImg.width) >= 250 || parseInt(newImg.height) >= 250){
							if( c == 0 ){
								$('.tmp-images').append('<li class="active" style="background-image: url(\' ' + item + ' \')" ><input type="hidden" name="product_image" value="' + item + '" /></li>');
							}
							if( c > 0 ){
								$('.tmp-images').append('<li style="background-image: url(\' ' + item + ' \')" ><input type="hidden" name="product_image" value="' + item + '" /></li>');
							}
							image_counter++; c++;
							if(image_counter > 7){ total_img = 1; }
						}
						total_img--;
					});
				});
				
				function checkImg( ) {
					if(total_img <= 1){
						if(image_counter > 0){
							var imageList = $('.tmp-images').html(),
								page = '/admin/stick_products?action=new_url_step1';
							$('.stick-product-modal3 .add-search-step1-content').html('');
							$('.tmp-images').html('');
							var form_data2 = {};
							form_data2.isAjax = true;
							SSAjax({
								data: form_data2,
								url: page,
								success: function(result){
									$('.stick-product-modal3 .search-modals').removeClass('hide');
									$('.stick-product-modal3 .add-search-step1').removeClass('hide');
									$('.stick-product-modal3 .add-search-step1-content').html(result);
									$('.stick-product-site_name').val(_data.site_name);
									$('.stick-product-url').val(_data.web_url);
									$('.stick-product-name').val(_data.product_name);
									$('.stick-product-price').val('$' + _data.price);
									$('.choose-img-content ul').html(imageList);
									$('.save-to-btn').removeClass('stick-add-btn');
									$('.save-to-btn').addClass('sengine-stick-add-btn');
									$('.add-search-step1 .stick-a-product').remove();
									$('.add-search-step1 .arrow-down').remove();
									$('.stick-product-modal3 .stick-loader').addClass('hide');
		
								}
							});
						}
						if(image_counter == 0){
							$('.error4').removeClass('hide');
							$('.stick-product-modal3 .stick-loader').addClass('hide');
						}
						image_counter = 0;
					}else{
						setTimeout( checkImg, 30 );
					}
				}
				checkImg();
				
			}
		});
		
		return false;
	});
	
	$(document).on('touchstart click', '.stick-content-a', function(){
		$('.add-search-step1, .add-search-step2, .search-modals').addClass('hide');
		$('.tmp-images').html('');
		$('.add-search-step1-content, .add-search-step2-content').html('');
	});
	
	$(document).on('close', '#stickModal, #addSearchModal, #stickAddModal', function (e) {
		e.preventDefault(); e.stopImmediatePropagation();
		$('.stick-wrapper').html('');
	});
	
	
	$(document).on('touchstart click', '.stick-prod-btn', function(e){
		e.preventDefault(); e.stopImmediatePropagation();
		$('#stickAddModal').foundation('reveal', 'open');
		//var url = '/admin/scripter?action=product_urls';
		var url = '/admin/stick_products?action=external_single_product',
			form_data = {};
		form_data.isAjax = true;
		form_data.url = $.trim($(this).find('input').val());
		//form_data.new_product = true;
		form_data.product_image = $(this).find('img').attr('src');
		$('.stick-product-add-modal .stick-loader').removeClass('hide');
		$('.error3').addClass('hide');
		$('.stick-product-add-modal .stick-wrapper').html('');
		SSAjax({
			data: form_data,
			url: url,
			success: function(response){
				var _data = $.parseJSON(response),
					c = 0,
					image_counter = 0;
				if(_data.url_exist){
					var page = '/admin/stick_products?action=add_url_existing',
						form_data = {};
					form_data.product_site_name = _data.site_name;
					form_data.product_url = _data.web_url;
					form_data.product_name = _data.product_name;
					form_data.product_price = '$' + _data.price;
					form_data.product_image = _data.image;
					form_data.product_type = _data.product_type;
					form_data.product_keyword = _data.keyword;
					form_data.isAjax = true;
					$('.stick-product-add-modal .stick-loader').removeClass('hide');
					$('.add-product-step2-wrapper').removeClass('hide');
					SSAjax({
						data: form_data,
						url: page,
						success: function(response){
							$('.stick-product-add-modal .stick-wrapper').html(response);
							$('.stick-product-add-modal .stick-wrapper').prepend('<span class="stick-close2 posted-close"></span>');
							$('.stick-product-add-modal .stick-loader').addClass('hide');
							$('.stick-product-add-modal .posted-lbl').addClass('hide');
						}
					});
					return false
				}
			}
		});
		
		return false;
	});
	
	$(document).on('touchstart click', '.posted-close', function(){
		$('#stickAddModal').foundation('reveal', 'close');
		$('.stick-product-add-modal .stick-wrapper').html('');
		$('.reveal-modal-bg').remove();
	});
	
	
	
	$(document).on('click', '.stick-mod-close', function(){
		$('.dashboard-modal').hide('slide', {direction: 'right'}, 200);
		$('body').css({'overflow':'inherit'});
	});
	
	$(document).on('click', '.view-product-url', function(e){
		e.preventDefault(); e.stopImmediatePropagation();
		$('.product-info-modal .mod-wrapper').html('');
		openModalUrl();
		var $form = $(this),
			form_data = {};
		if($(this).hasClass('product_featured')){
			form_data.featured = 1;
		}else{
			form_data.featured = 0;
		}
		var settings = $.parseJSON($(this).attr('data'));
		$.extend(form_data,settings);
		showModalLoader();
		$.post("/admin/catalog?page=product-url-details", form_data,function(response){
			$('.product-info-modal .mod-wrapper').html(response);
			var height = $('.modal-header').height(),
				window_height = $(window).height();
			window_height = window_height - height;
			hideModalLoader();
		});
		
	});
	
	$(document).on('click', '.product-sort-login li', function(e){
		e.preventDefault(); e.stopImmediatePropagation();
		if($(this).attr('data') == ""){ return false; }
		$('#wrapper').html('');
		$('#loadbox-wrapper').removeClass('hide');
		var form_data = {};
		form_data.display = $(this).attr('data');
		form_data.isAjax = true;
		SSAjax({
			data: form_data,
			url: location.href,
			success: function(response){
				$('#wrapper').html(response);
				$('#loadbox-wrapper').addClass('hide');
				$('#wrapper').removeClass('hide');
				$('#loadbox-wrapper').addClass('hide');
			}
		});
	});
	
	$(document).on('click', '.product-sort li', function(e){
		e.preventDefault(); e.stopImmediatePropagation();
		if($(this).attr('data') == ""){ return false; }
		$('#wrapper').html('');
		$('#loadbox-wrapper').removeClass('hide');
		var form_data = {};
		form_data.display = $(this).attr('data');
		form_data.isWrapper = true;
		$.post(location.href, form_data,function(response){
			$('#wrapper').html(response);
			$('#loadbox-wrapper').addClass('hide');
			$('#wrapper').removeClass('hide');
			$('#loadbox-wrapper').addClass('hide');
		});
		
	});
	
	$(document).on('keypress', '.search-p-c-s', function(e){
		e.stopImmediatePropagation();
		if (e.which == 13) {
			if($(this).val() == ""){ return false; }
			$('#wrapper').html('');
			$('#loadbox-wrapper').removeClass('hide');
			var form_data = {};
			form_data.search_text = $.trim($(this).val());
			form_data.isAjax = true;
			SSAjax({
				data: form_data,
				url: location.href,
				success: function(response){
					$('#wrapper').html(response);
					$('#loadbox-wrapper').addClass('hide');
					$('#wrapper').removeClass('hide');
					$('#loadbox-wrapper').addClass('hide');
				}
			});
		}
	});
	
	$(document).on('touchstart click', '.home-search-btn', function(e){
		e.stopImmediatePropagation();
		var search_text = $.trim($('.search-p-c-s').val());
		if(search_text == ""){ return false; }
		$('#wrapper').html('');
		$('#loadbox-wrapper').removeClass('hide');
		var form_data = {};
		form_data.search_text = search_text;
		form_data.isAjax = true;
		SSAjax({
			data: form_data,
			url: location.href,
			success: function(response){
				$('#wrapper').html(response);
				$('#loadbox-wrapper').addClass('hide');
				$('#wrapper').removeClass('hide');
				$('#loadbox-wrapper').addClass('hide');
			}
		});
	});
	
	$(document).on('keypress', '.home-urlproduct-search', function(e){
		e.stopImmediatePropagation();
		if (e.which == 13) {
			if($(this).val() == ""){ return false; }
			$('#wrapper').html('');
			$('#loadbox-wrapper').removeClass('hide');
			// $.ajax({
				// url: location.href,
				// type:'POST',
				// data: "search_text=" + $.trim($(this).val()),
				// success: function(response){
					// $('#wrapper').html(response);
					// $('#loadbox-wrapper').addClass('hide');
					// $('#wrapper').removeClass('hide');
					// $('#loadbox-wrapper').addClass('hide');
				// }
			// });
			
			var form_data = {};
			form_data.search_text = $.trim($(this).val());
			form_data.isWrapper = true;
			$.post(location.href, form_data,function(response){
				$('#wrapper').html(response);
				$('#loadbox-wrapper').addClass('hide');
				$('#wrapper').removeClass('hide');
				$('#loadbox-wrapper').addClass('hide');
			});
			
			
		}
	});
	
	var count = 2;
	$(window).scroll(function(){
		if($(window).scrollTop() == $(document).height() - $(window).height()){
			if(count == 0){ return false; }
			loadURLProducts(count);
			count++;
		}
	});
	
});

function openModalUrl(){
	var height = $(window).height();
	$('.dashboard-modal .mod-wrapper').css({'height': height});
	$('body').css({'overflow':'hidden'});
	$('.dashboard-modal').show('slide', {direction: 'right'}, 400);
}


function loadURLProducts(pageNumber){
	// $('a#inifiniteLoader').show('fast');
	$.ajax({
		url: root + "?page=" + pageNumber,
		type:'POST',
		data: "token=" + store.get('token') + "&user_id=" + store.get('user_id') + "&isAjax=" + true,
		success: function(response){
			if($.trim(response) == ''){ count = 0; return false; }
			//$('a#inifiniteLoader').hide('1000');
			$('.products-content ul').append(response);
		}
	});
	return false;
}