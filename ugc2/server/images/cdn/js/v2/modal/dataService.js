$.extend({
	parseJSON: function( data ) {
		if ( typeof data !== "string" || !data ) {
			return null;
		}
		data = jQuery.trim( data );
		if ( /^[\],:{}\s]*$/.test(data.replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g, "@")
			.replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, "]")
			.replace(/(?:^|:|,)(?:\s*\[)+/g, "")) ) {
			return window.JSON && window.JSON.parse ?
			window.JSON.parse( data ) :
			(new Function("return " + data))();
		}
	}
});

var dataService = function () {
    var urlBase = '/api/customers',
    root = '/',
    cloudUrl = '/cdn/',
    vid = CS.vid,
	
	popup = function(URL){
		day = new Date();
		id = day.getTime();
		return eval("page" + id + " = window.open(URL, '" + id + "', 'directories=0,toolbar=0,scrollbars=0,location=0,statusbar=0,menubar=0,resizable=0,width=520,height=364,left=200,top=50');");
	},
	
	resetform = function(){
		$('form input').val('');
		$('.email_error').html('');
		$('.field').removeClass('error');
		$('input').removeAttr("disabled");
		$('.message').html('');
		return;
	},
	
	loadSVG = function(){
		jQuery('img.svg').each(function(){
			var $img = jQuery(this);
			var imgID = $img.attr('id');
			var imgClass = $img.attr('class');
			var imgURL = $img.attr('src');

			jQuery.get(imgURL, function(data) {
				var $svg = jQuery(data).find('svg');
				if(typeof imgID !== 'undefined') {
					$svg = $svg.attr('id', imgID);
				}
				if(typeof imgClass !== 'undefined') {
					$svg = $svg.attr('class', imgClass+' replaced-svg');
				}
				$svg = $svg.removeAttr('xmlns:a');
				$img.replaceWith($svg);
			});
		});
		return;
	}
	
	getData = function (formData) {
        return $.ajax({
            url: formData.url,
            data: formData,
            type: 'POST'
        });
    };

    return {
		popup: popup,
		resetform: resetform,
		loadSVG: loadSVG,
		getData: getData
    };
}();