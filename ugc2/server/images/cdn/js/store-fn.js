$(document).ready(function(){
	$('.sc-product-wrapper').jScrollPane();
	$('.sp-product-wrapper').jScrollPane();
	$('.endp-product-wrapper').jScrollPane();
	$('.products_wrapper').jScrollPane();
	
	$('.template-list-wrapper').jScrollPane();
	$('.pages-wrapper').jScrollPane();

	$("div[rel*=styx_user_type]").CSModal({ top : 63, closeButton: ".modal_close" });

	$( "#tools" ).draggable();

	$(document.body).delegate('.menuz', 'click', function(){
		for(var c=1; c<6; c++){
			$('.menu'+c).addClass('menu'+c+'-style2');
			$('.menu'+c).removeClass('menu'+c+'-style1');
			$('.menu'+c).removeClass('menu'+c+'-style3');
		}
	});
	
	$(document.body).delegate('.x-btn', 'click', function(){
		$('.manage1').removeClass("menu_1");
		$('.manage2').removeClass("menu_2");
		$('.manage3').removeClass("menu_3");
		$('.manage3-pc').removeClass("menu_3");
		$('.manage4').removeClass("menu_4");
		$('.manage5').removeClass("menu_5");
		for(var c=1; c<6; c++){
			$('.menu'+c).addClass('menu'+c+'-style1');
			$('.menu'+c).removeClass('menu'+c+'-style2');
			$('.menu'+c).removeClass('menu'+c+'-style3');
		}
		$('.menu1').addClass('menu1-style3');
		$('.menu1').removeClass('menu1-style2');
		return false;
	});
	
	$(document.body).delegate('.menu1', 'click', function(){
		$('.manage2').removeClass("menu_2");
		$('.manage3').removeClass("menu_3");
		$('.manage3-pc').removeClass("menu_3");
		$('.manage4').removeClass("menu_4");
		$('.manage5').removeClass("menu_5");
		$(this).addClass('menu1-style3');
		$(this).removeClass('menu1-style2');
	});
	
	$(document.body).delegate('.menu2', 'click', function(){
		$('.manage2').addClass("menu_2");
		$('.manage3').removeClass("menu_3");
		$('.manage3-pc').removeClass("menu_3");
		$('.manage4').removeClass("menu_4");
		$('.manage5').removeClass("menu_5");
		
		$(this).addClass('menu2-style3');
		$(this).removeClass('menu2-style2');
		
		$('.sc-wrapper').removeClass('active');
		$('.sc').removeClass('active');
		//$(this).addClass('active');
		$('.sp-wrapper').addClass('active');
		$('.endp').removeClass('active');
		$('.endp-wrapper').removeClass('active');
	});
	
	$(document.body).delegate('.menu3', 'click', function(){
		$('.manage3').addClass("menu_3");
		$('.manage2').removeClass("menu_2");
		$('.manage4').removeClass("menu_4");
		$('.manage5').removeClass("menu_5");
		
		$(this).addClass('menu3-style3');
		$(this).removeClass('menu3-style2');
		
		//$(this).addClass('active');
		$('.sc-wrapper').addClass('active');
		$('.sp').removeClass('active');
		$('.sp-wrapper').removeClass('active');
		$('.endp-wrapper').removeClass('active');
		$('.endp').removeClass('active');
		if(categoriesLoadStatus <= 0){
			reloadCategoryListing();
			return false;
		}
	});
	
	$(document.body).delegate('.menu4', 'click', function(){
		$('.manage4').addClass("menu_4");
		$('.manage2').removeClass("menu_2");
		$('.manage3').removeClass("menu_3");
		$('.manage3-pc').removeClass("menu_3");
		$('.manage5').removeClass("menu_5");
		$(this).addClass('menu4-style3');
		$(this).removeClass('menu4-style2');
	});
	
	$(document.body).delegate('.menu5', 'click', function(){
		$('.manage5').addClass("menu_5");
		$('.manage2').removeClass("menu_2");
		$('.manage3').removeClass("menu_3");
		$('.manage3-pc').removeClass("menu_3");
		$('.manage4').removeClass("menu_4");
		$(this).addClass('menu5-style3');
		$(this).removeClass('menu5-style2');
		if(homepageLoadStatus <= 0){
			reloadHomepageSettings();
			return false;
		}
	});
	
	/* Vendor Settings */
	$(document.body).delegate('.menu-settings', 'click', function(e){
		e.stopImmediatePropagation();

		var pagetype;
		if($(this).hasClass('menu-vendor-details')) {
			pagetype = 'vendor';
		}else if($(this).hasClass('menu-shipping')) {
			pagetype = 'shipping';
		}else if($(this).hasClass('menu-return-policy')) {
			pagetype = 'return_policy';
		}else if($(this).hasClass('menu-get-paid')) {
			pagetype = 'get_paid';
		}
		else {
			pagetype = 'store';	
		}

		load_store_settings({'pagetype':pagetype});
	});
	
	$(document.body).delegate('.msub', 'click', function(){
		$('.msub').removeClass("active-tab");
		$(this).addClass("active-tab");
		$('.box-right').removeClass("store-product-box");
		var subid = $(this).attr("subid");
		$('.'+subid).addClass("store-product-box");
		//$('.sc-product-wrapper').trigger('click');
	});
	
	$(document.body).delegate('.primary-cs-menu', 'click', function(){
		$('.manage3-pc').removeClass('menu_3');
	});
	
	
	
	$(document.body).delegate('.sc-product-wrapper', 'mouseenter', function(){
		$(this).jScrollPane();
	});
	$(document.body).delegate('.sp-product-wrapper', 'mouseenter', function(){
		$(this).jScrollPane();
	});
	$(document.body).delegate('.endp-product-wrapper', 'mouseenter', function(){
		$(this).jScrollPane();
	});
	$(document.body).delegate('.products_wrapper', 'mouseenter', function(){
		$(this).jScrollPane();
	});
	
	$(document.body).delegate('.template-list-wrapper', 'mouseenter', function(){
		$(this).jScrollPane();
	});
	$(document.body).delegate('.pages-wrapper', 'mouseenter', function(){
		$(this).jScrollPane();
	});
	
	if (!$.browser.opera) {

		// select element styling
		$('select.sortby').each(function(){
			var title = $(this).attr('title');
			if( $('option:selected', this).val() != ''  ) title = $('option:selected',this).text();
			$(this)
				.css({'z-index':10,'opacity':0,'-khtml-appearance':'none'})
				.after('<div class="sortby">' + title + '</div>')
				.change(function(){
					val = $('option:selected',this).text();
					$(this).next().text(val);
					})
		});
	};
	
	
	
	$(".cornerpeel").hover(function() { //Hover Over
		$(".cornerpeel img , .message").stop()
		.animate({ //Expand the Message and the Hover Image
			width: '66px',
			height: '66px'
		}, 600);
	} , function() {
		$(".cornerpeel img").stop() //Hover Out Go Back To Original
		.animate({
			width: '40px',
			height: '40px'
		}, 200);
		$(".message").stop() //Hover Out Go Back To Original
		.animate({
			width: '40px',
			height: '40px'
		}, 180); //This should be a tad faster than the image because IE is known to glitch
	});
	
	jQuery('#product-carousel').jcarousel();
	

	$(document.body).delegate('#triggerPopup', 'click', function(){
		$('.advance-options').hide();
		$('.regular-option').show();
	});
	
	$(document.body).delegate('.aol', 'click', function(){
		$('.regular-option').hide(10, function(){
			$('.desc_enabled').val(1);
			$('.advance-options').slideDown(400);
		});
	});
	
	$(document.body).delegate('#triggerPopup, .rol', 'click', function(){
		$('.advance-options').slideUp(400, function() {
			$('.desc_enabled').val(0);
			$('.regular-option').show();
			if($('#form_type').val() == "add"){
				$('#description').val('');
			}
		});
	});
	
	$(document.body).delegate('#manage-categories', 'click', function(){
		$('.products-wrapper').removeClass("menu_3");
		$('.manage3-pc').addClass("menu_3");
		$('.sc').addClass('active');
		$('.sc-wrapper').addClass('active');
		$('.sp').removeClass('active');
		$('.sp-wrapper').removeClass('active');
		$('.endp').removeClass('active');
		$('.endp-wrapper').removeClass('active');
	});
	
	$(document.body).delegate('#view-my-products, .view_added_prod', 'click', function(){
		$('.products-wrapper').removeClass("menu_3");
		$('.manage3-pc').addClass("menu_3");
		$('.sp').addClass('active');
		$('.sp-wrapper').addClass('active');
		$('.sc').removeClass('active');
		$('.sc-wrapper').removeClass('active');
		$('.endp').removeClass('active');
		$('.endp-wrapper').removeClass('active');
	});
	
	// $(document.body).delegate('.sc', 'click', function(){
		// $(this).addClass('active');
		// $('.sc-wrapper').addClass('active');
		// $('.sp').removeClass('active');
		// $('.sp-wrapper').removeClass('active');
		// $('.endp-wrapper').removeClass('active');
		// $('.endp').removeClass('active');
	// });
	
	// $(document.body).delegate('.sp', 'click', function(){
		// $('.sc-wrapper').removeClass('active');
		// $('.sc').removeClass('active');
		// $(this).addClass('active');
		// $('.sp-wrapper').addClass('active');
		// $('.endp').removeClass('active');
		// $('.endp-wrapper').removeClass('active');
	// });
	
	
	$(document.body).delegate('.str', 'click', function(){
		$(this).addClass('active');
		$('.str-wrapper').addClass('active');
		$('.bill').removeClass('active');
		$('.bill-wrapper').removeClass('active');
		$('.ship-wrapper').removeClass('active');
		$('.ship').removeClass('active');
		$('.notf-wrapper').removeClass('active');
		$('.notf').removeClass('active');
		return false;
	});
	
	$(document.body).delegate('.bill', 'click', function(){
		$('.str-wrapper').removeClass('active');
		$('.str').removeClass('active');
		$(this).addClass('active');
		$('.bill-wrapper').addClass('active');
		$('.ship').removeClass('active');
		$('.ship-wrapper').removeClass('active');
		$('.notf').removeClass('active');
		$('.notf-wrapper').removeClass('active');
	});
	
	$(document.body).delegate('.ship', 'click', function(){
		$('.str-wrapper').removeClass('active');
		$('.str').removeClass('active');
		$('.bill').removeClass('active');
		$('.bill-wrapper').removeClass('active');
		$('.notf').removeClass('active');
		$('.notf-wrapper').removeClass('active');
		$(this).addClass('active');
		$('.ship-wrapper').addClass('active');
		return false;
	});
	
	$(document.body).delegate('.notf', 'click', function(){
		$('.str-wrapper').removeClass('active');
		$('.str').removeClass('active');
		$('.bill').removeClass('active');
		$('.bill-wrapper').removeClass('active');
		$('.ship').removeClass('active');
		$('.ship-wrapper').removeClass('active');
		$(this).addClass('active');
		$('.notf-wrapper').addClass('active');
		return false;
	});
	
	
	
	
	
	// $(document.body).delegate('.apbox-left', 'mouseenter', function(){
		// $('#popup_wrapper .see-demo-box').css({'left':'1px'});
	// });
	// $(document.body).delegate('.apbox-center', 'mouseenter', function(){
		// $('#popup_wrapper .see-demo-box').css({'left':'169px'});
	// });
	// $(document.body).delegate('.apbox-right', 'mouseenter', function(){
		// $('#popup_wrapper .see-demo-box').css({'left':'253px'});
	// });
	
	
	
	jQuery('#color-carousel').jcarousel();
	
	// function init() {
		// $('.color-picker').miniColors();
	// }
	// init();
	
});

function trimText (str) {
	str = str.replace(/^\s+/, '');
	for (var i = str.length - 1; i >= 0; i--) {
		if (/\S/.test(str.charAt(i))) {
			str = str.substring(0, i + 1);
			break;
		}
	}
	return str;
}

function load_productentry(dataObj) {
	var js;
	var thisFunc = this;
	var jsfile = 'js/store-product-fn.js';
	var el = $('.add_new_prod_s1');
	if(typeof el.data('js') === 'undefined') {
		el.data('js',new Array());
		var numtimesloaded = 0;
	}

	thisFunc.loadContent = function(dataObj) {
		
		data = {'token': store.get('token'), 'user_id': store.get('user_id'), 'store_id': store.get('store_id')};
		$.extend(data,dataObj);
		
		$.ajax({
			type: "POST",
			url: '/store_product/addproduct_content',
			data: data,
			success: function(data){
				var _data = $.parseJSON(data);
				if(!data){
					close_modal($('.productentry-wrapper'));
					csConfirm( 'Oops!', 
						'<div id="success-message-box">\
						 <p>Your session has expired, please login again.</p>\
						 </div>\
						',
						{Ok: null}, null, null,
						{ width:520, position:['center',120] }
					);
					$('.ui-dialog-titlebar').before('<div id="modalbox-colors-logo"><div class="one"></div><div class="two"></div><div class="three"></div><div class="four"></div><div class="five"></div></div>');
					return false;
				}
				$('.productentry-wrapper .box-content').html(data);
				$('.store_productentry').trigger('click');
				$('#addto').val($('#addtoval').val());
				$('#category_id').val($('#cid').val());
				$('#subcategory_id').val($('#scid').val());
				$('#category_type').val($('#ctype').val());
			}
		});
	};
	
	$('.store_productentry').trigger('click');
	//$('.productentry-wrapper .box-content').html('<div id="dialog-box"><div style="width: 100%; text-align: center; margin: 80px 0;"><img class="img-loader" src="' + cloud_url + 'images/dialog-ajax-loader.gif" /></div></div>');
	loadPreloader('.productentry-wrapper .box-content');
	
	if($.inArray(jsfile, el.data('js')) == -1) {
		$.when(
				/*
				$.getScript(cloud_url + 'js/vendor/jquery.ui.widget.js'),
				$.getScript(cloud_url + 'js/fileupload/tmpl.min.js'),
				$.getScript(cloud_url + 'js/fileupload/load-image.min.js'),
				$.getScript(cloud_url + 'js/fileupload/canvas-to-blob.min.js'),
				$.getScript(cloud_url + 'js/fileupload/jquery.iframe-transport.js'),
				$.getScript(cloud_url + 'js/fileupload/jquery.fileupload.js'),
				$.getScript(cloud_url + 'js/fileupload/jquery.fileupload-fp.js'),
				$.getScript(cloud_url + 'js/fileupload/jquery.fileupload-ui.js'),
				*/
				$.getScript(cloud_url + 'js/jquery.validationEngine-en.js'),
				$.getScript(cloud_url + 'js/jquery.validationEngine.js'),
				$.getScript(cloud_url + 'js/fineuploader/fileuploader.js'),
				$.getScript(cloud_url + 'js/jscolor.js'),
				$.getScript(cloud_url + jsfile)).then(
			function() {
				js = el.data('js');
				js.push(jsfile);
				el.data('js',js);
				thisFunc.loadContent(dataObj)
			}, 
			function() { 
				thisFunc.loadContent(dataObj);
				//alert('Unable to load js file for product entry section.'); 
			});
	}
	else {
		thisFunc.loadContent(dataObj);
	}

	return false;
}

function load_vendorentry() {
	var js;
	var thisFunc = this;
	var jsfile = 'js/store-product-fn.js';
	var el = $('.add_new_prod_s1');
	if(typeof el.data('js') === 'undefined') {
		el.data('js',new Array());
		var numtimesloaded = 0;
	}
}