
var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var logger = require('morgan');
var minify = require('express-minify');
var multer = require('multer');
var sqlConnect = require(__dirname + '/server/lib/mysql_connects/db_conf');
var	util = require('util');
var mySQLStuff = require(__dirname + '/server/lib/mysql_connects/mysql_convertions');
var nodemailer = require('nodemailer');
var transporter = nodemailer.createTransport();

var mongojs = require('mongojs');

var port = process.env.PORT || 8765;
GLOBAL.router = express.Router();

var startAPI = function()
{
	GLOBAL.locationString = "localhost";
	if(process.env.NODE_ENV=='development'){ //development or local machine
		console.log("setting up development");
		GLOBAL.imagepathinternal = "https://shopstyx.com/cdn/uploads/product_images"; //type=0
		GLOBAL.imagepathexternal = "https://shopstyx.com/cdn/uploads/external_products"; //type=4
		// GLOBAL.imagepathinternal = "http://192.168.56.101:"+port+"/images/cdn/uploads/product_images"; //type=0
		// GLOBAL.imagepathexternal = "http://192.168.56.101:"+port+"/images/cdn/uploads/external_products"; //type=4
		GLOBAL.http = "http://localhost:"+port;
		app.use(logger('dev'));

		var db_init = {
			host     : 'localhost',
			user     : 'root',
			password : '',
			database : ''
		};
	}else if(process.env.NODE_ENV=='production'){//production environment
		GLOBAL.locationString = "23.253.32.153";
		GLOBAL.imagepathinternal = "https://shopstyx.com/cdn/uploads/product_images"; //type=0
		GLOBAL.imagepathexternal = "https://shopstyx.com/cdn/uploads/external_products"; //type=4
		GLOBAL.http = "http://shopstyx.com:"+port;
		app.use(logger('combined'));

		var db_init = {
			host     : '3a7ff4b3d827722adc5ff9eaf30f56e25d924b46.rackspaceclouddb.com',
			user     : 'mymugzy',
			password : 'mymugzy!2009',
			database : ''
		};
	}else{//test environment
		GLOBAL.imagepathinternal = "https://shopstyx.com/cdn/uploads/product_images"; //type=0
		GLOBAL.imagepathexternal = "https://shopstyx.com/cdn/uploads/external_products"; //type=4
		GLOBAL.http = "http://23.253.32.153:"+port;
		app.use(logger('dev'));

		// var db_init = {
		// 	host     : '44fe862ed36373986601eac2094f12ad2d6c3c8c.rackspaceclouddb.com',
		// 	user     : 'mymugzy',
		// 	password : 'mymugzy!2009',
		// 	database : ''
		// };
		// var db_init = {
		// 	host     : '3a7ff4b3d827722adc5ff9eaf30f56e25d924b46.rackspaceclouddb.com',
		// 	user     : 'mymugzy',
		// 	password : 'mymugzy!2009',
		// 	database : ''
		// };
		var db_init = {
			host     : 'localhost',
			user     : 'mymugzy',
			password : 'mymugzy2009',
			database : ''
		};
	}//DB connection
	GLOBAL.http = "http://"+GLOBAL.locationString+":"+port;
	GLOBAL.mongodb = mongojs('ugcUser:s#0pst!cks@'+GLOBAL.locationString+'/ugc', ['ugcPhotostyx','ugcVideostyx']);
	//app.use(minify({cache: __dirname + '/cache'}));
	app.use(express.static(__dirname + '/public'));// set the static files location /public
	app.use(bodyParser.json()); 							// parse application/json
	app.use(bodyParser.urlencoded({extended:true}));		// parse application/x-www-form-urlencoded
	app.use(multer({ dest: './uploads/',
		rename: function (fieldname, filename,req,res) {
			// return filename.replace(/\W+/g, '');//.toLowerCase() + Date.now()
			// console.log("res");
			// console.log(res);
			// console.log("req");
			// console.log(req);
			// console.log("fieldname");
			// console.log(fieldname); req.body.model
			// var newFilename = req.body.model.filename;
			
			// newFilename = req.body.model.user.user_id + "_" + req.body.model.user.store_id + req.body.model.advertInfo.name+req.files.extension;
			// newFilename = newFilename.replace(/\W+/g, '').toLowerCase();
			var data = JSON.parse(req.body.model);
			console.log(data.filename_ext);
			return data.filename;
		} 
  	}).single('filename'));// parse multipart/form-data

	sqlConnect.mysqlConnect(db_init);//set mysqlConnections
	//set the template engine
	app.engine('.html',require('ejs').__express);
	//set the template engine's template folder
	app.set('views',__dirname+'/public');
	//automatically set the extensions for all server side res.renders
	app.set('view engine','html');

	//load the routes
	require('./server/routes')(GLOBAL.router);

	//CORs Registration
	//configure CORs
	app.all('*', function(req, res, next) {
		// res.header("Access-Control-Allow-Origin", "*");
		// res.header("Access-Control-Allow-Headers", "X-Requested-With");
		// next();
		res.header('Access-Control-Allow-Origin', '*');
	    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
	    res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');

	    // intercept OPTIONS method
	    if ('OPTIONS' == req.method) {
	      res.send(200);
	    }
	    else {
	      next();
	    }
	});

	// REGISTER OUR ROUTES -------------------------------
	// all of our routes will be from root /
	app.use('/', GLOBAL.router);
	//start our server
	app.listen(port);
	console.log('starting server at port '+port);
	console.log(process.env.NODE_ENV+' mode running');

	GLOBAL.sendEmail = function(msg)
	{
		var message = msg;
		console.log(util.inspect(msg,{showHidden: false, depth: null}));
		var dataStruct = {
			"newSimpleDate":'',
			"newUnixDate":''
		};
		dataStruct.newSimpleDate = mySQLStuff.MySQLDateTimeNOW(dataStruct);
		if(typeof(msg)=='object')
		{
			msg['date']=[];
			msg['date_simple']=[];
			msg.date=dataStruct.newUnixDate;
			msg.date_simple=dataStruct.newSimpleDate;
			message = JSON.stringify(msg);
		}
		var mailOptions = {
		    from: 'ugcservices@shopstyx.com', // sender address
		    to: 'andrewjohnyoung@gmail.com', // list of receivers
		    subject: 'Status Error', // Subject line
		    text: message // plaintext body
		    //html: '<b>Hello world ✔</b>' // html body
		};

		// send mail with defined transport object
		transporter.sendMail(mailOptions, function(error, info){
		    if(error){
		        console.log(error);
		    }else{
		        console.log('Message sent: ' + info.response);
		    }
		});
	}
	GLOBAL.sendError = function(msg,res)
	{
		if(msg.status=="Error" && msg.message!="400 page not found")
		{
			setImmediate(GLOBAL.sendEmail,msg);
		}else{
			console.log(util.inspect(msg,{showHidden: false, depth: null}));
		}
		res.json(msg);
	}
}

module.exports = {
	startAPI:startAPI
}