(function(){
	var dataServices = angular.module('fileUploadServices',[]);

	dataServices.service('fileUploadProcedures',['$http',function($http){
		

		return {
			fileRead:function(sharedEditorData,target,output,$scope){
	            tag_count=0;
	            var input = target;

	            var reader = new FileReader();
	            reader.onload = function(){
	                var dataURL = reader.result;
	                //var output = document.getElementById('output');
	                if(output == null)
	                {
	                	output= document.getElementById('mediaContainer');
	                }
	                output.src = dataURL;
	                output.onload=function(){
	                	var targetItem = {
	                		height:parseInt(this.height),
	                		width:parseInt(this.width)
	                	};
	                	console.log("fileRead target");
	                	console.log(targetItem);
	                	$scope.$emit("fileLoaded",targetItem);
	                };
	            };
	            reader.readAsDataURL(input.files[0]);
	            //extract filename,
	            var filename_arr = target.value.split('\\');
	            var rem_ext = filename_arr[filename_arr.length - 1];
	            sharedEditorData.saveBgFile(rem_ext);
	            rem_ext = rem_ext.split('.');
	            var filename = rem_ext[0];
	            filename_extention = rem_ext[rem_ext.length-1];
	            document.getElementById('filename').value = filename.replace(/\W/g, '');

	            // if(sharedEditorData.getAllowSave()==false)
	            // {
	            //     sharedEditorData.changeAllowSave(true);
	            //     $scope.$digest();//to trigger a check for the change in value...   
	            // }
	        }, //fileUpload : function(sharedEditorData,target)
			fileUpload : function(sharedEditorData,state,$scope) {
	            //sharedEditorData.reindexTagInformation();
	            //tag_count=sharedEditorData.getTagCount();
	            var files = sharedEditorData.getFiles();
	            var newFilename = user_id + "_" + store_id + "_" + sharedEditorData.getAdvertInfo().name;
	            newFilename = newFilename.replace(/\W+/g, '').toLowerCase();
	            //newFilename = newFilename+"."+files[0].type.split("/")[1]; //no longer needed
	            var modelData = {
	            	currentTags:JSON.parse(JSON.stringify(sharedEditorData.getAllTags())),
	            	advertInfo:JSON.parse(JSON.stringify(sharedEditorData.getAdvertInfo())),
	            	photoStyxColors:JSON.parse(JSON.stringify(sharedEditorData.getColors())),
	            	filename:newFilename,
	            	filename_ext:filename_extention,
	            	user:{
	            		user_id:user_id,
	            		store_id:store_id
	            	}
	            };
	            //sharedEditorData.getTagInformation();
	            if(state==undefined || state=='')
	            {
	                state='POST';
	            }
	            $http({
	                method: state,
	                url: GlobalHost+'/'+store_id+'/'+user_id+'/'+store_id+'/save/photostyx/',
	                //IMPORTANT!!! You might think this should be set to 'multipart/form-data' 
	                // but this is not true because when we are sending up files the request 
	                // needs to include a 'boundary' parameter which identifies the boundary 
	                // name between parts in this multi-part request and setting the Content-type 
	                // manually will not set this boundary parameter. For whatever reason, 
	                // setting the Content-type to 'false' will force the request to automatically
	                // populate the headers properly including the boundary parameter.
	                headers: { 'Content-Type': undefined },
	                //This method will allow us to change how the data is sent up to the server
	                // for which we'll need to encapsulate the model data in 'FormData'
	                transformRequest: function (data) {
	                    var formData = new FormData();
	                    //need to convert our json object to a string version of json otherwise
	                    // the browser will do a 'toString()' on the object which will result 
	                    // in the value '[Object object]' on the server.
	                    //formData.append("model", angular.toJson(data.model));//I am assuming that data = $scope, since we're not really using scope I'll push the shared data component
	                    formData.append("model", angular.toJson(modelData));
	                    //now add all of the assigned files
	                    
	                    for (var i = 0; i < files.length; i++) {
	                        //add each file to the form data and iteratively name them
	                        formData.append("file" + i, files[i]);
	                    }
	                    return formData;
	                },
	                //Create an object that contains the model and files which will be transformed
	                // in the above transformRequest method
	                //data: { model: $scope.model, files: $scope.files }
	                data: { model: modelData, files: files }
	            }).
	            success(function (data, status, headers, config) {
	                if(status==200)
	                {
	                    if(data.status=="Error")
	                    {
	                        if(data.message == "File Already Exists")
	                        {
	                            var choice = confirm("Advertisement already Exists.\nDo you wish to overwrite?");
	                            if(choice == true)
	                            {
	                                $scope.exposed.saveAdvert('PUT');
	                            }
	                        }else{
	                            alert(data.message+"\n\n"+data.description);
	                        }
	                    }else{
	                        sharedEditorData.generateEmbedCode(data.docs[0]._id);//not yet created
	                        sharedEditorData.setsavedState(true);
	                        alert("Saved Advertisement");
	                        //$scope.clearAdvertisement();
	                    }
	                    // sharedEditorData.pullSavedAdvertisement($scope.reflectData);
	                    // $scope.$digest();
	                }else{
	                    alert("Advertisement NOT SAVED!");
	                }
	                // $scope.clearAdvertisement();
	                // $scope.reconstructAdvert(sharedEditorData.getTagInformation());
	            }).
	            error(function (data, status, headers, config) {
	                alert("Failed communication to Server!");
	            });
	        }
		};
	}]);

})();