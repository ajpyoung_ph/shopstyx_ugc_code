(function(){
	var PhotoStyx = angular.module('mainController',['ngAnimate','ui.bootstrap','colorpicker.module','ngDragDrop','stringManilpulationService','productInfoServices','fileUploadServices','myDataServices','commonWidgets','commonContainers','advertisementContainers']);
    //'PhotoStyxPartialControllers',,'decisionMakersServices',,'photostyxDirectives',



	PhotoStyx.controller('mainPageController',['$scope','$http','sharedEditorData','decisionMakersProcedures','fileUploadProcedures',function($scope, $http,sharedEditorData,decisionMakersProcedures,fileUploadProcedures){
        //,'sharedEditorData','productInfoProcedures'
        //, sharedEditorData,productInfoProcedures
        var getSelectedRes = function(){
            return sharedEditorData.getSelectedRes();
        };
        var getColors = function(){
            return sharedEditorData.getColors();
        };
        var panelAdSize = function(width,type,target)
        {
            return decisionMakersProcedures.panelAdSize(width,type,target);
        };
        var getCreateStatus = function(){
            return sharedEditorData.getCreateStatus();
        };
        var getManagePhotoStyx = function(){
            return sharedEditorData.getManagePhotoStyx();
        };
        var opacityMenu = function(opacity_level,event)
        {
            ////console.log(event.type);
            $('#menuBtn').css('opacity',parseFloat(opacity_level));
            $('#addProductBtn').css('opacity',parseFloat(opacity_level));
        };
        var opacityTarget = function(opacity_level,elementName)
        {
            $('#'+elementName).css('opacity',parseFloat(opacity_level));
        };
        var opacityThis = function(opacity_level,event)
        {
            $(event.target).css('opacity',parseFloat(opacity_level));
        };
        var getPanelStatus = function()
        {
            return sharedEditorData.getPanelStatus();
        };
        var getborderstatus = function()
        {
            return sharedEditorData.getborderstatus();
        };
        var getshadowstatus = function()
        {
            return sharedEditorData.getshadowstatus();
        };
        var getPreviewCard = function(){
            return sharedEditorData.getPreviewCard();
        };
        var setPanelStatus = function(type,boolean_value,event)
        {
            var set=false;
            if(event!=null)
            {
                set = decisionMakersProcedures.setPanelStatusDecisions(event);
            }
            if(set==false)
            {
                // //console.log(event.type);
                // //console.log(event);
                sharedEditorData.setPanelStatus(type,boolean_value);
            }
            //decisionMakersProcedures.unlimitedScroll(type,boolean_value,$scope);
        };
        var getDepartmentList=function()
        {
            return sharedEditorData.getDepartmentList();
        };
        var getProductList = function()
        {
            return sharedEditorData.getProductList();
        };
        var convertCurr = function(currString)
        {
            return decisionMakersProcedures.currencySign(currString);
            
        };
        var salePrice = function(prodData)
        {
            return decisionMakersProcedures.salePrice(prodData);
        };
        var cancelTag = function()
        {
            saveTagInformation = false;
            sharedEditorData.setPanelStatus('tagTypeSelection',false);
            restoreTagDefault();
        };
        var restoreTagDefault = function()
        {
            var mycurrselected = $("#bottomaddtypeselectorholder .addtypeselectorSelectedtag");
            mycurrselected.removeClass("addtypeselectorSelectedtag");
            mycurrselected.addClass("addtypeselectorUnselectedtag");
            $('#pricetag').removeClass("addtypeselectorUnselectedtag");
            $('#pricetag').removeClass("addtypeselectorSelectedtag");
            $('#pricetag').addClass("addtypeselectorSelectedtag");
            //restore
        };
        var saveAdvert = function(method){
            fileUploadProcedures.fileUpload(sharedEditorData,method,$scope);
        };
        var openFile = function(target){
            $("#actualfile").off( "change" );
            sharedEditorData.setManagePhotoStyx(true);
            if(video==false)
            {
                fileUploadProcedures.fileRead(sharedEditorData,target,document.getElementById('mediaContainer'),$scope);
            }else{
                //place html5 video here
                fileUploadProcedures.fileRead(sharedEditorData,target,document.getElementById('mediaContainer'),$scope);
            }
            
            $scope.$digest();
        };
        var openFileChoose = function(){
            $('#actualfile').val("");
            $('#actualfile').on('change',function(){
                openFile(document.getElementById('actualfile'));
            });
            $('#actualfile').click();
        };
        var showAdvert = function()
        {
            return show;
        };
        var getAllTags = function()
        {
            ////console.log("tag length:"+sharedEditorData.getAllTags().length);
            if((sharedEditorData.getAllTags()).length>0)
            {
                console.log(sharedEditorData.getAllTags());
            }
            return sharedEditorData.getAllTags();
        }
        var getTagTypes = function()
        {
            return sharedEditorData.getTagTypes();
        }
        function dropEvent(event)
        {
            ////console.log("dropped");
            ////console.log(event);
            var positionData = {
                clientX:event.clientX,
                clientY:event.clientY,
                offsetX:event.offsetX,
                offsetY:event.offsetY,
                pageX:event.pageX,
                pageY:event.pageY,
                screenX:event.screenX,
                screenY:event.screenY
            };
            ////console.log(positionData);
            saveTagInformation = true;
            sharedEditorData.saveCurrentProductTagPosition(positionData);
            sharedEditorData.saveCurrentTagType($("#bottomaddtypeselectorholder .addtypeselectorSelectedtag").attr("id"));
            sharedEditorData.setPanelStatus('tagTypeSelection',true);
        };
        globaldropEvent=dropEvent;
        //$scope.videoState = video;
        $scope.exposed = {
            getSelectedRes:getSelectedRes,
            getColors:getColors,
            panelAdSize:panelAdSize,
            getCreateStatus:getCreateStatus,
            getManagePhotoStyx:getManagePhotoStyx,
            opacityMenu:opacityMenu,
            opacityTarget:opacityTarget,
            opacityThis:opacityThis,
            getPanelStatus:getPanelStatus,
            getborderstatus:getborderstatus,
            getshadowstatus:getshadowstatus,
            getPreviewCard:getPreviewCard,
            setPanelStatus:setPanelStatus,
            getDepartmentList:getDepartmentList,
            getProductList:getProductList,
            convertCurr:convertCurr,
            salePrice:salePrice,
            cancelTag:cancelTag,
            restoreTagDefault:restoreTagDefault,
            saveAdvert:saveAdvert,
            openFile:openFile,
            openFileChoose:openFileChoose,
            showAdvert:showAdvert,
            getAllTags:getAllTags,
            getTagTypes:getTagTypes
        }

        var editTag = function(event,index)
        {
            saveTagInformation = false;
            cancelTag();
            tt_HideInit();
            lasteditIndex=index;
            ////console.log("editing tag #"+index);
            setPanelStatus('manageProductAds',false,null);
            //setClickedData to getTagInfo(index).clickedData;
            selecttagtype(sharedEditorData.getTagInfo(index).clickedData.lastDataTagType);//lastDataTagType
            sharedEditorData.setClickedData(sharedEditorData.getTagInfo(index).clickedData);
            sharedEditorData.setPanelStatus('tagTypeSelection',true);
        };
        var removeTag = function(event,index)
        {
            if(sharedEditorData.getdeletingTagFlag()==false)
            {
                sharedEditorData.setdeletingTagFlag();
                lasteditIndex=-1;
                saveTagInformation = false;
                cancelTag();
                //console.log("removing tag #"+index);
                tt_HideInit();
                sharedEditorData.removeFromListOfTags(index);
                formTag();
            }
        };

        $scope.$on("editTag",editTag);
        $scope.$on("removeTag",removeTag);
        $scope.$on("cancelTag",cancelTag);
	}]);

	PhotoStyx.filter('limitStyxText', function () {
        return function (value, wordwise, max, tail) {
            if (!value) return '';

            max = parseInt(max, 10);
            if (!max) return value;
            if (value.length <= max) return value;

            value = value.substr(0, max);
            if (wordwise) {
                var lastspace = value.lastIndexOf(' ');
                if (lastspace != -1) {
                    value = value.substr(0, lastspace);
                }
            }

            return value + (tail || ' …');
        };
    });
})();