(function(){

	var routeController = angular.module('routeController',['ngRoute','mainController']);

	routeController.config(['$routeProvider',
		function($routeProvider) {
			$routeProvider
			.when('/:token/:user_id/:store_id/photostyx/create/', {
				templateUrl: 'partials/phone-list.html',
				controller: 'PhoneListCtrl'
			})
			.when('/phones/:phoneId', {
				templateUrl: 'partials/phone-detail.html',
				controller: 'PhoneDetailCtrl'
			});
			// .otherwise({
			// 	redirectTo: '/phones'
			// });
		}
	]);
})();