(function(){
	var commonContainers = angular.module('commonContainers',['myDataServices','decisionMakersServices','fileUploadServices']);

	commonContainers.directive('createPhotoStyxEntry',function(){
		return{
			restrict:'E',//E as an Element or A as a Attribute
			templateUrl:"/lib/partials/template/containers/createPhotoStyxEntry.html",
			scope:{

			},
			controller:function($scope,sharedEditorData,decisionMakersProcedures)
			{
				$scope.videoState = function(){
		            return sharedEditorData.videoState();
		        }
				$scope.initCreate = function()
				{
					if(sharedEditorData.getAdvertName()!='')
					{
						decisionMakersProcedures.decideDoneInPage0(sharedEditorData,$scope);
						//sharedEditorData.setCreateStatus(true);
						//alert("showCreate : "+sharedEditorData.getAdvertName());
					}else{
						sharedEditorData.setCreateStatus(false);
						alert("Please enter a proper advertisement name/title");
					}
				}
				$scope.clearEverything = function()
				{
					$scope.$broadcast('clearAdvInfo');//send event to children
					//$scope.$emit('someEvent', [1,2,3]);//send event above to parents
				}
				$scope.previouspageDecide = function()
				{
					if(sharedEditorData.getPreviousPage()==sharedEditorData.getCurrentPageIndex(2))
					{
						return "Update";
					}else{
						return "Create";
					}
				}
			}
		}
	});
	commonContainers.directive('loadMedia',function(){
		return{
			restrict:'E',//E as an Element or A as a Attribute
			templateUrl:"/lib/partials/template/containers/loadMedia.html",
			scope:{
				exposed:'='
			},
			controller:function($scope, $element, $attrs, $transclude,sharedEditorData){
				$scope.videoState = function(){
		            return sharedEditorData.videoState();
		        }
			}
		}
	});
	commonContainers.directive('managePhotoStyx',function(){
		return{
			restrict:'E',//E as an Element or A as a Attribute
			templateUrl:"/lib/partials/template/containers/managePhotoStyx.html"
		}
	});
	commonContainers.directive('overallmenu',function(){
		return{
			restrict:'E',//E as an Element or A as a Attribute
			templateUrl:"/lib/partials/template/containers/overallmenu.html",
			transclude:true,
			scope:{
				exposed:'='
			},
			controller:function($scope, $element, $attrs, $transclude){
				
			}
		}
	});
	commonContainers.directive('manageProductsAds',function(){
		return{
			restrict:'E',//E as an Element or A as a Attribute
			templateUrl:"/lib/partials/template/containers/manageProductsAds.html",
			scope:{
				exposed:'='
			},
			controller:function($scope, $element, $attrs, $transclude,sharedEditorData){
				var getAllTags = function()
		        {
		        	return sharedEditorData.getAllTags();
		        }
			}
		}
	});

})();