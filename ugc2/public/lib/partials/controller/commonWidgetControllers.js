(function(){
	var commonWidgets = angular.module('commonWidgets',['myDataServices','decisionMakersServices','fileUploadServices','productInfoServices','stringManilpulationService']);

	
	commonWidgets.directive('resolutionPicker',function(){
		return{
			restrict:'E',//E as an Element or A as a Attribute
			templateUrl:"/lib/partials/template/widgets/resolutionPicker.html",
			scope:{
				initialize:'@'
			},
			controller:function($scope,sharedEditorData){
				if(show==false)
				{
					sharedEditorData.setSelectedRes(sharedEditorData.getResolutionSetIndex(parseInt($scope.initialize)));
				}
				$scope.selectedRes=sharedEditorData.getSelectedRes();
				
				$scope.getResolutionList = sharedEditorData.getResolutionSet();
				
				$scope.setSelectedRes = function(data){
					sharedEditorData.setSelectedRes(data);
					$scope.selectedRes=sharedEditorData.getSelectedRes();
					sharedEditorData.modifyCreateWindow();
				}
				$scope.getSelectedRes = function(){
					return sharedEditorData.getSelectedRes();
				}
				var clearMe = function(){
					if(typeof(GlobalAdvertInfo)=='undefined')
					{
						$scope.selectedRes = sharedEditorData.getResolutionSetIndex(0);
						sharedEditorData.setSelectedRes($scope.selectedRes);
						sharedEditorData.modifyCreateWindow();
					}else{
						sharedEditorData.setAdvertInfo(GlobalAdvertInfo);
						setSelectedRes(GlobalAdvertInfo.selectedRes);
						$("#titlePhotoStyx").val(GlobalAdvertInfo.name);
						sharedEditorData.setCreateStatus(true);
						sharedEditorData.setManagePhotoStyx(true);
						sharedEditorData.setPreviewCard(false);
						sharedEditorData.setPanelStatus('manageColors',false);
						sharedEditorData.setPanelStatus('overallMenu',true);
						sharedEditorData.setPanelStatus('backButton',false);
					}
				}
				$scope.$on('clearAdvInfo',clearMe);
			}
		}
	});

	commonWidgets.directive('advertisementName',function(){
		return{
			restrict:'E',
			template:'<input placeholder="Title" ng-keyup="setAdvertName($event)">',
			scope:{
				style:'@',
				callback:'='
			},
			controller:function($scope, $element, $attrs, $transclude,sharedEditorData,decisionMakersProcedures){
				//console.log($element[0]);
				if($scope.style!='' && $scope.style!=null && typeof($scope.style)&&'undefined')
				{
					try{
						var jsonStyle = $scope.style;
						jsonStyle = jsonStyle.replace(";",",");
						jsonStyle = JSON.parse("{"+jsonStyle+"}");
						for (var key in jsonStyle) {
						    if (jsonStyle.hasOwnProperty(key)) {
						        console.log(key + " = " + jsonStyle[key]);
						        $element[0].childNodes[0].style[key]=jsonStyle[key];
						    }
						}
					}catch(err){
						//do nothing
					}
				}
				
				$scope.setAdvertName=function(event){
					sharedEditorData.setAdvertName(event.target.value);
					if(event.which==13)
					{
						$scope.callback();
					}
				}
				var clearMe = function(){
					//console.log($element[0].childNodes[0]);
					$element[0].childNodes[0].value='';
					if(typeof(GlobalAdvertInfo)=='undefined')
					{
						$element[0].childNodes[0].value='';
					}else{
						$element[0].childNodes[0].value=GlobalAdvertInfo.name;
					}
				}
				$scope.$on('clearAdvInfo',clearMe);
			}
		}
	});

	commonWidgets.directive('hiddenUploadFile',function(){
		return{
			restrict:'E',
			template:'<input type="file" accept="{{mediaTypeToLoad}}" id="actualfile" style="visibility: hidden;" file-Upload>',//<ng-include src="getFileUploadTemplate()"/>
			controller:function($scope, $element, $attrs, $transclude,sharedEditorData){
				if(video==true)
				{
					$scope.mediaTypeToLoad="video/mp4,video/webm,video/ogg";
				}else{
					$scope.mediaTypeToLoad="image/*";
				}
				$scope.$on("fileSelected", function (event, args) {
		            $scope.$apply(function () {            
		                //add the file object to the scope's files collection
		                sharedEditorData.pushFiles(args.file);
		            });
		        });
			}
		}
	});
	commonWidgets.directive('fileUpload', function () {
	    return {
	        scope: true,        //create a new scope
	        link: function (scope, el, attrs) {
	            el.bind('change', function (event) {
	                var files = event.target.files;
	                //iterate files since 'multiple' may be specified on the element
	                for (var i = 0;i<files.length;i++) {
	                    //emit event upward
	                    scope.$emit("fileSelected", { file: files[i] });
	                }                                       
	            });
	        }
	    };
	});
	commonWidgets.directive('mediaDisplay',function(){
		return{
			restrict:'E',
			template:'<ng-include src="getDisplayMediaType()" onload="finishedLoadingMediaContainer()"/>',//media-display
			controller:function($scope, $element, $attrs, $transclude,sharedEditorData,fileUploadProcedures){
				var fileInfo = sharedEditorData.getFiles();
				console.log(fileInfo);
				$scope.getDisplayMediaType = function(){
					if(video==false)
					{
						return '/lib/partials/template/widgets/imageDisplay.html';
					}else{
						return '/lib/partials/template/widgets/videoDisplay.html';
					}
				}
				
				var updateImageCSS = function(evt,data)
				{
					//console.log(data);
					// width:100%;
					// height: auto;
					var sel = sharedEditorData.getSelectedRes();
					if(video==false)
					{
						if(parseInt(data.width)>parseInt(data.height))
						{
							$("#mediaContainer").css('width',"100%");
							$("#mediaContainer").css('height',"auto");
							
							var targetPadding = (parseInt(sel.height)/2)-(parseInt($("#mediaContainer").height())/2);
							var targetcss = 'padding-top';
						}else{
							$("#mediaContainer").css('height',"100%");
							$("#mediaContainer").css('width',"auto");
							
							var targetPadding = (parseInt(sel.width)/2)-(parseInt($("#mediaContainer").width())/2);
							var targetcss = 'padding-left';
						}
						$("#mediaContainer").css('display',"block");
						$("#mediaContainer").css('margin-left',"auto");
						$("#mediaContainer").css('margin-right',"auto");
						$("#mediaContainer").css(targetcss,targetPadding+"px");
					}else{
						$("#mediaContainer").css('width',sel.width+"px");
						$("#mediaContainer").css('height',sel.height+"px");
					}
					
				};
				// output.onload = function(){
				$scope.finishedLoadingMediaContainer = function(){
					if(video==false)
					{
						$("#mediaContainer").one("load", function() {
							var targetItem = {
		                		height:parseInt(this.height),
		                		width:parseInt(this.width)
		                	};
		                	console.log("jquery mediaContainer");
		                	console.log(targetItem);
		                	updateImageCSS(null,targetItem);
						}).each(function() {
							if(this.complete) $(this).load(); //checks if loaded from cache
						});
					}else{
						var myvideo = document.getElementById('mediaContainer');
						myvideo.addEventListener('loadeddata', function() {
						   // Video is loaded and can be played
		                	updateImageCSS(null);
						}, false);
					}
				}
				
				$scope.$on('fileLoaded',updateImageCSS);//fileUploadProcedures.fileRead(sharedEditorData,document.getElementById('actualfile'),document.getElementById('photoImageDisplay'),$scope);
				// $scope.triggerLoad = function(){
				// 	fileUploadProcedures.fileRead(sharedEditorData,document.getElementById('actualfile'),document.getElementById('photoImageDisplay'),$scope);
				// }
			}
		}
	});
	
	commonWidgets.directive('rightsideMenu',function(){
		return{
			restrict:'E',
			templateUrl:"/lib/partials/template/widgets/rightsideMenu.html",//<ng-include src="getFileUploadTemplate()"/>,
			// scope:{},
			controller:function($scope, $element, $attrs, $transclude,sharedEditorData){
				
			}
		}
	});
	commonWidgets.directive('leftsideMenu',function(){
		return{
			restrict:'E',
			templateUrl:"/lib/partials/template/widgets/leftsideMenu.html",//<ng-include src="getFileUploadTemplate()"/>,
			// scope:{},
			controller:function($scope, $element, $attrs, $transclude){
				
			}
		}
	});
	commonWidgets.directive('managePhotoStyxColors',function(){
		return{
			restrict:'E',
			templateUrl:"/lib/partials/template/widgets/managePhotoStyxColors.html",//<ng-include src="getFileUploadTemplate()"/>,
			scope:{
				exposed:'='
			},
			controller:function($scope, $element, $attrs, $transclude,sharedEditorData,decisionMakersProcedures){
				$scope.setColors=function(type,color){
					sharedEditorData.setColors(type,color);
					switch(type)
					{
						case 'bordercolor':
							$('#photoOverLayer').css("border","solid 10px "+sharedEditorData.getColors().bordercolor);
							break;
						case 'shadowcolor':
							$('#photoOverLayer').css('box-shadow','1px 1px 10px 1px '+sharedEditorData.getColors().shadowcolor);
							$('#photoOverLayer').css('-webkit-box-shadow','1px 1px 10px 1px '+sharedEditorData.getColors().shadowcolor);
							$('#photoOverLayer').css('-moz-box-shadow','1px 1px 10px 1px '+sharedEditorData.getColors().shadowcolor);
							break;
						case 'color1selected':
						case 'color2selected':	
						case 'color3selected':
							$scope.setPreviewCard(true);
							break;
					}
					
				}
				
				$scope.changeState = function(targetString,partnerString)
				{
					decisionMakersProcedures.changeState(targetString,partnerString,sharedEditorData);
					//$scope.$digest();
				}
				$scope.setPreviewCard = function(boolean_value)
				{
					sharedEditorData.setPreviewCard(boolean_value);
				}
				$scope.color1selected = (sharedEditorData.getColors()).color1;
		        $scope.color2selected = (sharedEditorData.getColors()).color2;
		        $scope.color3selected = (sharedEditorData.getColors()).color3;
		        $scope.shadowcolorselected = (sharedEditorData.getColors()).shadowcolor;
		        $scope.bordercolorselected = (sharedEditorData.getColors()).bordercolor;
			},
			transclude:true
		}
	});
	commonWidgets.directive('previewcard',function(){
		return{
			restrict:'E',
			templateUrl:"/lib/partials/template/widgets/previewcard.html",//<ng-include src="getFileUploadTemplate()"/>,
			scope:{
				setCard:'=',
				exposed:'='
			},
			controller:function($scope, $element, $attrs, $transclude){
				$scope.setPreviewCard = function(boolean_value)
				{
					setCard(boolean_value);
				}
				$scope.sampletext = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam maximus ipsum et arcu sodales convallis. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aliquam sollicitudin condimentum tortor, a bibendum nulla finibus a.";
			}
		}
	});
	commonWidgets.directive('addproduct',function(){
		return{
			restrict:'E',
			templateUrl:"/lib/partials/template/widgets/addproduct.html",//<ng-include src="getFileUploadTemplate()"/>,
			scope:{
				exposed:'='
			},
			controller:function($scope, $element, $attrs, $transclude,sharedEditorData,productInfoProcedures){
				$scope.setSearchName = function(event)
				{
					if(event.keyCode==13)
					{
						sharedEditorData.setSearchName(name);
						initiateSearchProducts();
					}
				}
				$scope.setSearchCat = function(event,index)
				{
					// //console.log(event);
					var cat = $(event.target).text();
					sharedEditorData.setSearchCat(cat);
					$scope.initiateSearchProducts();
				}
				$scope.initiateSearchProducts = function()
				{
					// if(JSON.stringify(sharedEditorData.getProductList())=='{}')
					// {
						sharedEditorData.setCurrentProductPage(0);
					//}
					var name = $('#prodNameSearch').val();
					if(typeof(name)=='undefined')
					{
						name='';
					}
					sharedEditorData.setSearchName(name);
					productInfoProcedures.getAllProducts(sharedEditorData);
				}
				$scope.getProductList = function()
				{
					return sharedEditorData.getProductList();
				}
		        var increasePage=function()
				{
					var currentPage = sharedEditorData.getCurrentProductPage();
					currentPage=currentPage+1;
					sharedEditorData.setCurrentProductPage(currentPage);
					productInfoProcedures.getAllProducts(sharedEditorData);
				}
				var checkScrollProducts = function()
				{
					//console.log('executing productListingHolder');
					if($('#productListingHolder').scrollTop() + $('#productListingHolder').innerHeight() >= $('#productListingHolder')[0].scrollHeight - 100)
					{
						//console.log('reached the bottom');
						if(sharedEditorData.getLoadingProductList()==false)
						{
							sharedEditorData.setLoadingProductList();
							increasePage();
						}
					}		
				}
				if(show==false)
		        {
		        	productInfoProcedures.getAllCategories(sharedEditorData);
		        	$scope.initiateSearchProducts();
		        	$('#productListingHolder').on("scroll",checkScrollProducts);
		        }
			}
		}
	});
	commonWidgets.directive('productlistItem',function(){
		return{
			restrict:'E',
			templateUrl:"/lib/partials/template/widgets/productlistItem.html",//<ng-include src="getFileUploadTemplate()"/>,
			// scope:{
			// 	exposed:'=',
			// 	getProductList:'='
			// },
			controller:function($scope, $element, $attrs, $transclude,sharedEditorData){
				//console.log($attrs);
				$scope.saveCurrentProduct = function(index)
				{
					sharedEditorData.saveCurrentProduct(index);
					////console.log(sharedEditorData.getCurrentProduct());
				}
			}
		}
	});
	commonWidgets.directive('addtypeselector',function(){
		return{
			restrict:'E',
			templateUrl:"/lib/partials/template/widgets/addtypeselector.html",//<ng-include src="getFileUploadTemplate()"/>,
			scope:{
				exposed:'='
			},
			controller:function($scope, $element, $attrs, $transclude,sharedEditorData,decisionMakersProcedures,stringMan){
				//console.log($attrs);
				$scope.getCurrentProduct = function()
				{
					return sharedEditorData.getCurrentProduct();
				}
				$scope.getTagTypes = function()
				{
					return sharedEditorData.getTagTypes();
				}
				$scope.selecttagtype = function(tagName)
				{
					var mycurrselected = $("#bottomaddtypeselectorholder .addtypeselectorSelectedtag");
					mycurrselected.removeClass("addtypeselectorSelectedtag");
					mycurrselected.addClass("addtypeselectorUnselectedtag")
					$('#'+tagName).removeClass("addtypeselectorUnselectedtag");
					$('#'+tagName).addClass("addtypeselectorSelectedtag");
					sharedEditorData.saveCurrentTagType(tagName);
				}
				$scope.formTag = function()
				{
					tt_HideInit();
					if(saveTagInformation)
					{
						var clickedData = JSON.stringify(sharedEditorData.getClickedData());
						var photoStyxColors = JSON.stringify(sharedEditorData.getColors());
						var collectedContent = {
							clickedData:JSON.parse(clickedData)
						};
						sharedEditorData.pushToListOfTags(JSON.stringify(collectedContent));
					}else{
						if(lasteditIndex>-1)
						{
							var clickedData = sharedEditorData.getTagInfo(lasteditIndex).clickedData;
							clickedData.lastDataTagType = sharedEditorData.getCurrentTagType();
							sharedEditorData.updateClickedDataIndex(lasteditIndex,clickedData);
						}
					}			
					//decisionMakersProcedures.createTag(sharedEditorData,stringMan,$compile,$scope,$sce);
					$scope.$emit("cancelTag");
					setTimeout(sharedEditorData.cleardeletingTagFlag,1000);
				}
			}
		}
	});
	commonWidgets.directive('manageProductAdsList',function(){
		return{
			restrict:'E',
			templateUrl:"/lib/partials/template/widgets/manageProductAdsList.html",//<ng-include src="getFileUploadTemplate()"/>,
			// scope:{
			// 	exposed:'=',
			// 	getProductList:'='
			// },
			controller:function($scope, $element, $attrs, $transclude,sharedEditorData){
				//console.log($attrs);
				$scope.showManageProductEditor = function(id)
		        {
		        	//console.log('show '+id);
		        	$("#"+id).show();
		        }
		        $scope.hideManageProductEditor = function(id)
		        {
		        	//console.log('hide '+id);
		        	$("#"+id).hide();
		        }
		        $scope.editTag=function(index)
		        {
		        	$scope.$emit("editTag",index);
		        }
		        $scope.removeTag=function(index)
		        {
		        	$scope.$emit("removeTag",index);
		        }
			}
		}
	});
	commonWidgets.directive('menuBtnSubMenuHolder',function(){
		return{
			restrict:'E',
			templateUrl:"/lib/partials/template/widgets/menuBtnSubMenuHolder.html",//<ng-include src="getFileUploadTemplate()"/>,
			// scope:{
			// 	exposed:'=',
			// 	getProductList:'='
			// },
			controller:function($scope, $element, $attrs, $transclude,sharedEditorData){
				//console.log($attrs);
				$scope.returnToPage0 = function()
				{
					sharedEditorData.returnToPage0();
				}
			}
		}
	});

})();