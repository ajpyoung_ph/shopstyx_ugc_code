(function(){
	var advertisementContainers = angular.module('advertisementContainers',['myDataServices','myDataServices']);

	advertisementContainers.directive('advertisementTags',function(){
		return{
			restrict:'E',//E as an Element or A as a Attribute
			template:'<ng-include src="getAdvertisementType($index,advertData,exposed)"/>',
			// scope:{
			// 	exposed:'=',
			// 	advertData:'='
			// },
			controller:function($scope, $element, $attrs, $transclude,sharedEditorData){
				// $scope.widgetType = sharedEditorData.getTagInfo(parseInt($attrs['index']));
				// console.log($scope.widgetType);
				$scope.currencySign = function(currString)
				{
					if(typeof(currString)!='undefined')
					{
						switch(currString.toLowerCase())
						{
							case 'yen':
								return '¥';
								break;
							case 'pound':
								return '£';
								break;
							case 'usd':
							default:
								return '$';
								break;
						}	
					}else{
						return '';
					}
				}
				$scope.filterText = function (value, wordwise, max, tail) {
		            if (!value) return '';

		            max = parseInt(max, 10);
		            if (!max) return value;
		            if (value.length <= max) return value;

		            value = value.substr(0, max);
		            if (wordwise) {
		                var lastspace = value.lastIndexOf(' ');
		                if (lastspace != -1) {
		                    value = value.substr(0, lastspace);
		                }
		            }

		            return value + (tail || ' …');
		        }
		        $scope.salePrice = function(prodData)
				{
					if(typeof(prodData.sale_price)!='undefined')
					{
						if(parseInt(prodData.sale_price)!=0 && isNaN(prodData.sale_price)==false)
						{
							return $scope.currencySign(prodData.currency)+prodData.sale_price;
						}else{
							return '';
						}
					}
					if(typeof(prodData.clickedData)!='undefined')
					{
						if(typeof(prodData.clickedData.lastClickedData)!='undefined')
						{
							if(typeof(prodData.clickedData.lastClickedData.sale_price)!='undefined')
							{
								if(parseInt(prodData.clickedData.lastClickedData.sale_price)!=0 && isNaN(prodData.clickedData.lastClickedData.sale_price)==false)
								{
									return $scope.currencySign(prodData.clickedData.lastClickedData.currency)+prodData.clickedData.lastClickedData.sale_price;
								}else{
									return '';
								}
							}
						}
					}
					return '';
					
				}
				$scope.editTag=function(index)
				{
					$scope.$emit("editTag",index);
				}
				$scope.removeTag=function(index)
				{
					$scope.$emit("removeTag",index);
				}
				$scope.showme=function(index,show)
				{
					if(show==1)
					{
						$('#subtag'+index).css('visibility', 'visible');
					}else{
						$('#subtag'+index).css('visibility', 'hidden');
					}
				}
				$scope.getColors=function()
				{
					return sharedEditorData.getColors();
				}
				$scope.getAdvertisementType=function(index,advertData,exposed)
				{
					// console.log(index);
					// console.log(advertData);
					// console.log("exposed");
					// console.log(exposed);
					// console.log(filterText);
					$scope.filterText=filterText;
					switch((advertData.clickedData.lastDataTagType).toLowerCase())
					{
						case (exposed.getTagTypes()[1]).toLowerCase():
							//rightsidetag
							break;
						case (exposed.getTagTypes()[2]).toLowerCase():
							//leftsidetag
							break;
						case (exposed.getTagTypes()[3]).toLowerCase():
							//bottomsidetag
							break;
						case (exposed.getTagTypes()[0]).toLowerCase():
						default:
							return '/lib/partials/template/adverts/advertisementPriceTag.html';
							break;
					}
				}
				$scope.finishedLoadingMediaContainer=function($scope,index)
				{
					console.log("ad finished loading");
					//console.log($scope.widgetType);
				}
			}
		}
	});

})();