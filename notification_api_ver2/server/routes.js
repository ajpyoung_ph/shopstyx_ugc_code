var saveRecord = require('./lib/CRUD/post/saveNotificationRecord');
var verifyAccess = require('./lib/securityChecks');
var mySQLStuff = require('./lib/mysql_connects/mysql_convertions');
var readNotification = require('./lib/CRUD/get/readNotification');
var markRead = require('./lib/CRUD/put/updateNotificationAsRead');
var readGenChats = require('./lib/CRUD/get/getGenChatHistory');
var markReadChat = require('./lib/CRUD/put/updateChatsAsRead');
var readAdvertisementLogs = require('./lib/CRUD/get/readAdvertisementLogs');
var getUnreadCount = require('./lib/CRUD/get/getUnReadCount');
var getListOfPeople = require('./lib/CRUD/get/getListOfPeopleChatted');
var saveAccountSettings = require('./lib/CRUD/post/saveAccountSettings');
var readAccountSettings = require('./lib/CRUD/get/readAccountSettings');
var updateAccountSettings = require('./lib/CRUD/put/updateAccountSettings');
var pmx = require('pmx');
var probe = pmx.probe();
 
var meter = probe.meter({
  name    : 'req/min',
  seconds : 60
});

module.exports = function(router){

	router.route('/:token/:user_id/notification/log/')
		.post(function(req,res){
			validate(req,res,saveRecord.saveNotification);//ok - no array
		});
	router.route('/:token/:user_id/get/latest/notifications/now/:number_of_records/:page_number/')
		.get(function(req,res){
			validate(req,res,readNotification.readALLNotificationsNOCHECK);//ok - no array
		});
	router.route('/:token/:user_id/get/latest/notifications/now/_id/:number_of_records/:page_number/:_id/')
		.get(function(req,res){
			//console.log('inside 1');
			validate(req,res,readNotification.readALLNotificationsAFTERIDNOCHECK);//ok - no array
		});
	router.route('/:token/:user_id/get/latest/notifications/checks/:number_of_records/:page_number/')
		.get(function(req,res){
			//console.log('inside 2');
			validate(req,res,readNotification.readALLNotificationsWITHCHECK);//ok - no array
		});
	router.route('/:token/:user_id/get/latest/notifications/checks/_id/:number_of_records/:page_number/:_id/')
		.get(function(req,res){
			validate(req,res,readNotification.readALLNotificationsAFTERIDWITHCHECK);//ok - no array
		});
	//mark notification as read
	router.route('/:token/:user_id/mark/read/:recID/')
		.post(function(req,res){
			validate(req,res,markRead.markRecordRead)
		});
	router.route('/:token/:user_id/mark/read/all/notifications/')
		.post(function(req,res){
			validate(req,res,markRead.markALLRecordRead)
		});
	//Get latest messages per user
	router.route('/:token/:user_id/get/latest/chat/all/now/user_id/:target_user_id')
		.get(function(req,res){
			validate(req,res,readGenChats.readALLChatsForSpecificUserNOCHECKS_modifiedUserID);//10 second delete array in place with force delete at end of process
		});
	router.route('/:token/:user_id/get/latest/chat/all/now/user_id/specific/:number_of_records/:page_number/:target_user_id')
		.get(function(req,res){
			validate(req,res,readGenChats.readALLChatsForUser);//10 second delete array in place with force delete at end of process
		});
	router.route('/:token/:user_id/get/latest/chat/all/now/user_id/date/specific/:number_of_records/:page_number/:target_user_id/:datetime')
		.get(function(req,res){
			validate(req,res,readGenChats.readALLChatsForUserAfterDate);
		});
	router.route('/:token/:user_id/get/latest/chat/all/now/:number_of_records/:page_number/')
		.get(function(req,res){
			validate(req,res,readGenChats.readALLChatsForSpecificUserNOCHECKS_modified);
		});
	router.route('/:token/:user_id/get/latest/chat/all/now/by_date/:number_of_records/:page_number/:date_start/')
		.get(function(req,res){
			validate(req,res,readGenChats.readALLChatsForSpecificUserAFTERIDNOCHECKS);
		});
	//get generic chat thread by user
	router.route('/:token/:user_id/get/latest/chat/generic/user_id/now/:number_of_records/:page_number/:target_user_id/')
		.get(function(req,res){
			validate(req,res,readGenChats.readALLGenericChatsForSpecificUserAndTargetUserNOCHECKS);
		});
	router.route('/:token/:user_id/get/latest/chat/generic/user_id/_id/now/:number_of_records/:page_number/:target_user_id/:_id')
		.get(function(req,res){
			validate(req,res,readGenChats.readGenericChatsAfterID);
		});
	//get product chat thread by user
	router.route('/:token/:user_id/get/latest/chat/product/user_id/now/:number_of_records/:page_number/:target_user_id/:product_id/:store_id/:store_user_id/')
		.get(function(req,res){
			validate(req,res,readGenChats.readSpecificProductChatsNOCHECKS);
		});
	router.route('/:token/:user_id/get/latest/chat/product/user_id/_id/now/:number_of_records/:page_number/:target_user_id/:product_id/:store_id/:store_user_id/:_id/')
		.get(function(req,res){
			validate(req,res,readGenChats.readProductChatsAfterID);
		});
	router.route('/:token/:user_id/get/latest/chat/product/user_id/unixdate/now/:number_of_records/:page_number/:target_user_id/:product_id/:store_id/:store_user_id/:unixdate/')
		.get(function(req,res){
			validate(req,res,readGenChats.readProductChatsAfterUnixDate);
		});
	//get order chat thread by user
	router.route('/:token/:user_id/get/latest/chat/order/now/:number_of_records/:page_number/:target_user_id/:order_id/')
		.get(function(req,res){
			validate(req,res,readGenChats.readSpecificOrderChatsNOCHECKS);
		});
	router.route('/:token/:user_id/get/latest/chat/order/user_id/_id/now/:number_of_records/:page_number/:target_user_id/:order_id/:_id')
		.get(function(req,res){
			validate(req,res,readGenChats.readOrderChatsAfterID);
		});
	//get store chat thread by user
	router.route('/:token/:user_id/get/latest/chat/store/user_id/now/:number_of_records/:page_number/:target_user_id/:claimed_by/:store_id/:store_user_id/')
		.get(function(req,res){
			validate(req,res,readGenChats.readSpecificStoreChatsNOCHECKS);
		});
	router.route('/:token/:user_id/get/latest/chat/store/user_id/_id/now/:number_of_records/:page_number/:target_user_id/:claimed_by/:store_id/:store_user_id/:_id/')
		.get(function(req,res){
			validate(req,res,readGenChats.readStoreChatsAfterID);
		});
	//mark chats as read
	router.route('/:token/:user_id/mark/read/generic/chat/:target_user_id')
		.post(function(req,res){
			validate(req,res,markReadChat.markGenChatRead);
		});
	router.route('/:token/:user_id/mark/read/product/chat/:target_user_id/:product_id/:store_id/:store_user_id/')
		.post(function(req,res){
			validate(req,res,markReadChat.markProdChatRead);
		});
	router.route('/:token/:user_id/mark/read/order/chat/:target_user_id/:order_id')
		.post(function(req,res){
			validate(req,res,markReadChat.markOrderChatRead);
		});
	router.route('/:token/:user_id/mark/read/store/chat/:target_user_id/:claimed_by/:store_id/:store_user_id/')
		.post(function(req,res){
			validate(req,res,markReadChat.markStoreChatRead);
		});
	//get unread count
	router.route('/:token/:user_id/get/unread/count/generic/chat/:target_user_id')
		.get(function(req,res){
			validate(req,res,getUnreadCount.genericchatSpecificUser);
		});
	router.route('/:token/:user_id/get/unread/count/generic/chat/:target_user_id/:source_user_id')
		.get(function(req,res){
			validate(req,res,getUnreadCount.genericchatSpecificUser);
		});
	router.route('/:token/:user_id/get/unread/count/product/chat/:target_user_id/:product_id/:store_id/:collection_id')
		.get(function(req,res){
			validate(req,res,getUnreadCount.productchatSpecificUser);
		});
	router.route('/:token/:user_id/get/unread/count/order/chat/:target_user_id/:order_id')
		.get(function(req,res){
			validate(req,res,getUnreadCount.orderchatSpecificUser);
		});
	router.route('/:token/:user_id/get/unread/number/chat/order_id/now/:order_id/')
		.get(function(req,res){
			validate(req,res,getUnreadCount.orderchatSpecificOrderIDOnly);
		});
	router.route('/:token/:user_id/get/unread/count/notifications/')
		.get(function(req,res){
			validate(req,res,getUnreadCount.notificationsUnread);
		});
	router.route('/:token/:user_id/get/unread/all/chats/')
		.get(function(req,res){
			validate(req,res,getUnreadCount.allUnreadChats);
		});
	//getListOfPeople
	router.route('/:token/:user_id/get/list/people/chats/:number_of_records/:page_number')
		.get(function(req,res){
			validate(req,res,getListOfPeople.allPeeps);
		});
	//get number of chats in thread
	router.route('/:token/:user_id/get/quantity/messages/')
		.get(function(req,res){
			validate(req,res,readGenChats.reqChatCount);
		});

	//ADVERTISEMENT LOGS
	router.route('/:token/:user_id/get/endorser/sharing/split/product/:product_id/:store_id/:store_user_id/')
		.get(function(req,res){
			validate(req,res,readAdvertisementLogs.getProductViews)
		});

	//Account Settings
	router.route('/:token/:user_id/save/emailblock/')
		.post(function(req,res){
			validate(req,res,saveAccountSettings.saveEmailBlock);
		});

	router.route('/:token/:user_id/query/emailblock/:notification_type/')
		.get(function(req,res){
			validate(req,res,readAccountSettings.readEmailBlock);
		});

	router.route('/:token/:user_id/unsubscribe/emailblock/')
		.post(function(req,res){
			validate(req,res,updateAccountSettings.updateEmailBlock);
		});


	router.route('*')
		.get(function(req,res){
			msg = {
				status:"Error",
				message:"400 page not found",
				description: ""
			};
			GLOBAL.sendError(msg,res);
		})
		.post(function(req,res){
			msg = {
				status:"Error",
				message:"400 page not found",
				description: ""
			};
			GLOBAL.sendError(msg,res);
		})
		.put(function(req,res){
			msg = {
				status:"Error",
				message:"400 page not found",
				description: ""
			};
			GLOBAL.sendError(msg,res);
		})
		.delete(function(req,res){
			msg = {
				status:"Error",
				message:"400 page not found",
				description: ""
			};
			GLOBAL.sendError(msg,res);
		});
}

function validate(req,res,callback)
{
	var dataStruct = {
		"user_id":parseInt(req.params.user_id),
		"token":req.params.token,
		"req": req,
		"res": res,
		"newSimpleDate":'',
		"newUnixDate":'',
		"timezone":"America/Los_Angeles"
	};
	var newSimpleDate = mySQLStuff.MySQLDateTimeNOW(dataStruct);
	dataStruct.newSimpleDate = newSimpleDate;
	meter.mark();
	//currently we have store_id and user_id fixed
	if(process.env.NODE_ENV=='development')// && parseInt(req.params.token) == 0)
	{
		callback(dataStruct);
	}else{
		verifyAccess.checkToken(dataStruct,callback);
	}
}