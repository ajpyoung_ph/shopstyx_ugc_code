var genChatTable = function (id1,id2)
{
	return ["chat_"+id1+"_to_"+id2,"chat_"+id2+"_to_"+id1];
}

var prodChatTable = function(id1,id2,prodID,storeID,storeUserID,variantID)
{
	return ["product_chat_"+id1+"_to_"+id2+"_about_"+prodID+"_"+storeID+"_"+storeUserID+"_"+variantID,"product_chat_"+id2+"_to_"+id1+"_about_"+prodID+"_"+storeID+"_"+storeUserID+"_"+variantID];
}

var orderChatTable = function(id1,id2,orderID)
{
	return ["order_chat_"+id1+"_to_"+id2+"_about_"+orderID.toString(),"order_chat_"+id2+"_to_"+id1+"_about_"+orderID.toString()];
}
module.exports = {
	genChatTable:genChatTable,
	prodChatTable:prodChatTable,
	orderChatTable:orderChatTable
}