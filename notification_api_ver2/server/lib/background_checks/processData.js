var mysql_string_cnv = require('../mysql_connects/mysql_convertions');
var retry_max = 9;
var events = require('events');
var emitter = new events.EventEmitter();
var scheduler = require('node-schedule');
var verbHandler = require('../eventHandler/verbHandler');
var mongojs = require('mongojs');

GLOBAL.processTracker = [];

var processData = function()
{
	try{
		/*var query = { 
				$or:[
					{process_marker:1}
					,{process_marker:2}
				]
			};*/
		var notificationCollection = GLOBAL.notificationCollection;
		var query = {
			process_marker:1
		};

		notificationCollection.find(query,function(err, docs){
			if(err==null)
			{
				if(docs.length>0)
				{
					docs.forEach(function(doc){
						setImmediate(verbHandler.notificationEventSwitcher,doc);
					});
				}else{
					var err = {
						status:'[processData] Background Status',
						query:query,
						result:"NO RECORDS FOUND"
					};
					console.log(err);
				}
			}else{
				throw err;
			}
		});

	}catch(err){
		msg = {
			status:"Error",
			message:"[processData] Error Processing Data",
			description: err
		};
		setImmediate(GLOBAL.sendEmail,msg);
	}
}

var single_processData = function(sdoc,notificationCollection)
{
	try{
		var docs = [];
		docs.push(sdoc);
		console.log("no err found");
		if(docs.length>0)
		{
			console.log("got "+docs.length+" documents");
			docs.forEach(function(doc){
				console.log("calling verbHandler");
				setImmediate(verbHandler.notificationEventSwitcher,doc);
			});
		}else{
			var err = {
				status:'[single_processData] Background Status',
				query:query,
				result:"NO RECORDS FOUND"
			};
			setImmediate(GLOBAL.sendEmail,err);
		}

	}catch(err){
		console.log("captured error");
		msg = {
			status:"Error",
			message:"[single_processData] Error Processing Data",
			description: err
		};
		setImmediate(GLOBAL.sendEmail,msg);
	}
}

module.exports = {
	processData:processData,
	single_processData:single_processData
}