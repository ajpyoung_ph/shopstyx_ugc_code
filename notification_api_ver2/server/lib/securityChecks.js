var conv = require(__dirname + '/mysql_connects/mysql_convertions');
var checkToken = function(dataStruct,callback)
{
	if(isNaN(dataStruct.user_id)==false)
	{
		dataStruct.token = conv.mysql_real_escape_string(dataStruct.token);
		var query = "SELECT * FROM cs_tokens WHERE user_id="+dataStruct.user_id+" AND token='"+dataStruct.token+"';";
		//console.log(query);
		GLOBAL.db_cs_common.query(query,function(err,rows, fields){
			var msg={};
			// err={
			// 	code:'PROTOCOL_ENQUEUE_AFTER_FATAL_ERROR'
			// };
			if(err==null)
			{
				if(rows.length>0)
				{
					callback(dataStruct);
				}else{
					msg = {
						status:"Error",
						message:"[checkToken]You are not permitted to use this service",
						description:"Credentials are not valid"
					};
					try{
						dataStruct.res.json(msg);
						console.log(msg);
					}catch(err){
						console.log("Error Sending Server response");
						console.log(err);
						console.log(dataStruct.res);
						//dataStruct.res.json(msg);
					}
				}
			}else{
				console.log("Error Accessing DB");
				console.log(err);
				msg = {
					status:"Error",
					message:"[checkToken]Error Accessing DB",
					description:err,
					query:query
				};
				GLOBAL.db_cs_common.emit('error',err);
				dataStruct.res.json(msg);
				console.log(msg);
			}
		});
	}else{
		msg = {
			status:"Error",
			message:"[checkToken]You are not permitted to use this service",
			description:"Credentials are not valid"
		};
		try{
			dataStruct.res.json(msg);
			console.log(msg);
		}catch(err){
			console.log("Error Sending Server response");
			console.log(err);
			console.log(dataStruct.res);
			//dataStruct.res.json(msg);
		}
	}
}

module.exports = {
	checkToken:checkToken
}