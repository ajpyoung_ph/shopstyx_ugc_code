/*
API will also automatically create entries for followers when:

'verb':’add-product’ - API will check if current user has followers and create notification entries based on structure seen above
‘verb’:’added-new-collection’
‘verb’:‘opened-new-store’
‘verb’:‘claimed-store’
‘verb’:‘removed-product’
‘verb’:’removed-collection’
‘verb’:’removed-store’
‘verb’:’chat’
‘verb’:’product-chat’
‘verb’:’modified-product’
‘verb’:’modified-collection’
‘verb’:’modified-store’

*/
var util = require('util');
var mySQLStuff = require('../mysql_connects/mysql_convertions');

var notificationEventSwitcher =  function(dataStruct)
{
	try{
		if(typeof(dataStruct.type)=='undefined')
		{
			dataStruct['type']=[];
			dataStruct.type='statistic';
		}

		switch((dataStruct.verb).toLowerCase())
		{
			case 'add-product-external':
				dataStruct['collectionUser']=[];
				dataStruct.collectionUser=true;
				addUserFollowers(dataStruct);
				// notify user followers
				break;
			case 'add-product-internal':
				dataStruct['collectionUser']=[];
				dataStruct.collectionUser=true;
				dataStruct['shopUser']=[];
				dataStruct.shopUser=true;
				addUserFollowers(dataStruct);
				break;
			case 'added-new-collection':
				addUserFollowers(dataStruct);
				// notify user followers
				break;
			case 'opened-new-store':
				dataStruct['shopUser']=[];
				dataStruct.shopUser=true;
				addUserFollowers(dataStruct);
				//notify user followers
				break;
			case 'claimed-store':
				dataStruct['shopUser']=[];
				dataStruct.shopUser=true;
				addUserFollowers(dataStruct);
				break;
			case 'removed-product':
				addUserFollowers(dataStruct);
				break;
			case 'removed-collection':
				dataStruct['collectionUser']=[];
				dataStruct.collectionUser=true;
				addUserFollowers(dataStruct);
				break;
			case 'removed-store':
				dataStruct['shopUser']=[];
				dataStruct.shopUser=true;
				addUserFollowers(dataStruct);
				break;
			
			case 'modified-product':
				addUserFollowers(dataStruct);
				break;
			case 'modified-collection':
				dataStruct['collectionUser']=[];
				dataStruct.collectionUser=true;
				addUserFollowers(dataStruct);
				break;
			case 'modified-store':
				dataStruct['shopUser']=[];
				dataStruct.shopUser=true;
				addUserFollowers(dataStruct);
				break;
			case 'shared-advertisement-product':
			case 'shared-advertisement-collection':	
			case 'chat':
			case 'product-chat':
				//no record in notification table
				break;
			case 'follow-user':
			case 'ordered-item'://should have extra_information.order_id and to_info
			case 'shipped-item'://should have extra_information.order_id and to_info
			default:
				saveRecord(dataStruct);
				break;
		}
		
	}catch(err){
		msg = {
			status:"Background Error",
			message:"[notificationEventSwitcher] Error Processing Data",
			description: err
		};
		setImmediate(GLOBAL.sendEmail,msg);
	}
}

function addUserFollowers(dataStruct)
{
	try{
		var follower_ids = [];
		var collectionUser=false;
		if(typeof(dataStruct['collectionUser'])!='undefined')
		{
			collectionUser=true;
			delete dataStruct.collectionUser;
		}

		var shopUser=false;
		if(typeof(dataStruct['shopUser'])!='undefined')
		{
			shopUser=true;
			delete dataStruct.shopUser;
		}
		
		// if((dataStruct.verb).toLowerCase()=='add-product-internal')
		// {
		// 	if(parseInt(dataStruct.extra_information.user_info.user_id)!=0)
		// 	{
		// 		var SELECTuser_id = dataStruct.extra_information.user_info.user_id;
		// 	}else{
		// 		var SELECTuser_id = dataStruct.extra_information.shop_info.claimed_by;
		// 	}
			
		// }else{
			var SELECTuser_id = dataStruct.from_info.user_id;
		//}
		var query = "SELECT * FROM cs_users_followers WHERE user_id="+SELECTuser_id+" ORDER BY follower_id ASC;";	
		GLOBAL.db_cs_common.query(query,function(err,rows, fields){
			if(err==null)
			{
				if(typeof(dataStruct['server_generated_information']) == 'undefined')
				{
					dataStruct['server_generated_information']=[];
					dataStruct.server_generated_information = {};
				}
				
				if(typeof(dataStruct.server_generated_information['follower_list']) == 'undefined')
				{
					dataStruct.server_generated_information['follower_list'] = [];	
				}
				rows.forEach(function(doc){
					var follower = {
						user_id:parseInt(doc.follower_id),
						read:false
					};
					dataStruct.server_generated_information.follower_list.push(follower);
					follower_ids.push(parseInt(doc.follower_id));
				});
				if(collectionUser)
				{
					setImmediate(addCollectionFollowers,dataStruct);
				}else if(shopUser){
					setImmediate(addStoreFollowers,dataStruct);
				}else{
					console.log("saved from addUserFollowers")
					setImmediate(saveRecord,dataStruct);
				}
				trueIndexTable(follower_ids);
			}else{
				var desc={
					err:err,
					rows:rows,
					fields:fields,
					query:query
				};
				msg = {
					status:"Background Error",
					message:"[addUserFollowers] Error Processing Data",
					description: desc
				};
				setImmediate(GLOBAL.sendEmail,msg);
			}
		});
	}catch(err){
		markStructAsProcess99(dataStruct);
		msg = {
			status:"Background Error",
			message:"[addUserFollowers] Error Processing Data",
			description: err
		};
		setImmediate(GLOBAL.sendEmail,msg);
	}
	
}
function addCollectionFollowers(dataStruct)
{
	try{
		var follower_ids = [];
		var collection_id = '';
		var skip=false;
		switch((dataStruct.verb).toLowerCase())
		{
			case 'add-product-internal':
			case 'add-product-external':
			case 'removed-product':
			case 'modified-product':
				// collection_id = dataStruct.extra_information.product_info.collection_id;
				// break;
			case 'added-new-collection':
			case 'removed-collection':
			case 'modified-collection':
				try{
					collection_id = dataStruct.extra_information.collection_info.collection_id;	
				}catch(err){
					skip=true;
				}
				break;

		}
		if(skip==false)
		{
			var query = "SELECT * FROM cs_users_collections_followers WHERE user_id="+dataStruct.from_info.user_id+" AND collection_id="+collection_id+" ORDER BY follower_id ASC;";
			GLOBAL.db_cs_common.query(query,function(err,rows,fields){
				if(err==null)
				{
					if(typeof(dataStruct['server_generated_information']) == 'undefined')
					{
						dataStruct['server_generated_information']=[];
						dataStruct.server_generated_information = {};
					}
					
					if(typeof(dataStruct.server_generated_information['follower_list']) == 'undefined')
					{
						dataStruct.server_generated_information['follower_list'] = [];	
					}
					rows.forEach(function(doc){
						var addME = true;
						var follower = {
							user_id:parseInt(doc.follower_id),
							read:false
						};
						for(var x=0;x < dataStruct.server_generated_information.follower_list.length; x++)
						{
							if(parseInt(doc.follower_id) == parseInt(dataStruct.server_generated_information.follower_list[x].user_id))
							{
								addME = false;
								break;
							}
						}
						if(addME)
						{
							dataStruct.server_generated_information.follower_list.push(follower);
							follower_ids.push(parseInt(doc.follower_id));
						}
					});
					console.log("saved from addCollectionFollowers")
					setImmediate(saveRecord,dataStruct);
					setImmediate(trueIndexTable,follower_ids);
				}else{
					var desc={
						err:err,
						rows:rows,
						fields:fields,
						query:query
					};
					msg = {
						status:"Background Error",
						message:"[addCollectionFollowers] Error Processing Data",
						description: desc
					};
					setImmediate(GLOBAL.sendEmail,msg);
				}
			});
		}else{
			console.log("skipped from Collection Followers")
			setImmediate(saveRecord,dataStruct);
			//setImmediate(trueIndexTable,follower_ids);
		}
		
	}catch(err){
		markStructAsProcess99(dataStruct);
		msg = {
			status:"Background Error",
			message:"[addCollectionFollowers] Error Processing Data",
			description: err
		};
		setImmediate(GLOBAL.sendEmail,msg);
	}
}
function addStoreFollowers(dataStruct)
{
	try{
		var follower_ids = [];
		var query = "SELECT * FROM cs_users_collections_followers WHERE user_id="+dataStruct.from_info.user_id+" AND store_id="+dataStruct.extra_information.store_info.store_id+" ORDER BY follower_id ASC;";
		GLOBAL.db_cs_common.query(query,function(err,rows,fields){
			if(err==null)
			{
				if(typeof(dataStruct['server_generated_information']) == 'undefined')
				{
					dataStruct['server_generated_information']=[];
					dataStruct.server_generated_information = {};
				}
				
				if(typeof(dataStruct.server_generated_information['follower_list']) == 'undefined')
				{
					dataStruct.server_generated_information['follower_list'] = [];	
				}
				rows.forEach(function(doc){
					var addME = true;
					var follower = {
						user_id:parseInt(doc.follower_id),
						read:false
					};
					for(var x=0;x < dataStruct.server_generated_information.follower_list.length; x++)
					{
						if(parseInt(doc.follower_id) == parseInt(dataStruct.server_generated_information.follower_list[x].user_id))
						{
							addME = false;
							break;
						}
					}
					if(addME)
					{
						dataStruct.server_generated_information.follower_list.push(follower);
						follower_ids.push(parseInt(doc.follower_id));
					}
				});
				console.log("saved from addStoreFollowers");
				setImmediate(saveRecord,dataStruct);
				setImmediate(trueIndexTable,follower_ids);
			}else{
				var desc={
					err:err,
					rows:rows,
					fields:fields,
					query:query
				};
				msg = {
					status:"Background Error",
					message:"[addCollectionFollowers] Error Processing Data",
					description: desc
				};
				setImmediate(GLOBAL.sendEmail,msg);
			}
		});
	}catch(err){
		markStructAsProcess99(dataStruct);
		msg = {
			status:"Background Error",
			message:"[addCollectionFollowers] Error Processing Data",
			description: err
		};
		setImmediate(GLOBAL.sendEmail,msg);
	}
}
var saveRecord = function(dataStruct)
{
	try{
		var notificationCollection = GLOBAL.notificationCollection;
		var query = {
			_id:dataStruct._id
		};
		delete dataStruct._id;
		dataStruct.process_marker=2;
		notificationCollection.update(query,{'$set': dataStruct },function(err,doc){
			if(err==null){
				msg = {
					status:"Success",
					message:"[saveRecord] Information Saved",
					data:dataStruct
				};
				console.log(util.inspect(msg,{showHidden: false, depth: null}));
			}else{
				msg = {
					status:"Error",
					message:"Error Saving Data",
					description: err
				};
				setImmediate(GLOBAL.sendEmail,msg);
			}
		});
		var trueIndexQuery = {
			"user_id":dataStruct.from_info.user_id
		};
		var trueIndexDataStruct = {
			"newSimpleDate":'',
			"newUnixDate":'',
			"timezone":"America/Los_Angeles"
		};
		mySQLStuff.MySQLDateTimeNOW(trueIndexDataStruct);
		var from_info_struct = {
			"user_id" : dataStruct.from_info.user_id,
		    "read" : false,
		    "newSimpleDate" : trueIndexDataStruct.newSimpleDate,
		    "newUnixDate" : parseInt(trueIndexDataStruct.newUnixDate)
		};
		GLOBAL.trueIndexCollection.update(query,{"$set":from_info_struct},{"upsert":true});
		try{
			var trueIndexQuery = {
				"user_id":dataStruct.to_info.user_id
			};
			var from_info_struct = {
				"user_id" : dataStruct.to_info.user_id,
			    "read" : false,
			    "newSimpleDate" : trueIndexDataStruct.newSimpleDate,
			    "newUnixDate" : parseInt(trueIndexDataStruct.newUnixDate)
			};
			GLOBAL.trueIndexCollection.update(query,{"$set":from_info_struct},{"upsert":true});
		}catch(err){
			//do nothing
		}
		
	}catch(err){
		msg = {
			status:"Background Error",
			message:"[saveRecord] Error Processing Data",
			description: err
		};
		setImmediate(GLOBAL.sendEmail,msg);
	}
}
var trueIndexTable = function(list_of_ids)
{
	try{

		list_of_ids.forEach(function(id){
			var dataStruct = {
				"user_id":parseInt(id),
				"read":false,
				"newSimpleDate":'',
				"newUnixDate":''
			};
			var newSimpleDate = mySQLStuff.MySQLDateTimeNOW(dataStruct);
			dataStruct.newSimpleDate = newSimpleDate;
			setImmediate(updateTrueIndexTable,dataStruct,parseInt(id));
		});
	}catch(err){
		msg = {
			status:"Background Error",
			message:"[trueIndexTable] Error Processing Data",
			description: err
		};
		setImmediate(GLOBAL.sendEmail,msg);
	}
}
function updateTrueIndexTable(dataStruct,user_id)
{
	try{
		var trueIndexCollection = GLOBAL.mongodb.collection('trueIndexCollection');
		var query = {
			"user_id":parseInt(user_id)
		};
		trueIndexCollection.update(query,{$set:dataStruct},{upsert:true},function(err,doc){
			if(err==null)
			{
				if(doc.n>0 && doc.ok==true)
				{

					var msg = {
						status:"Success",
						message:"[updateTrueIndexTable] table successfully updated"
						// doc:doc,
						// err:err,
						// query:query,
						// dataStruct:dataStruct
					};
					console.log(msg)	
				}else{
					var desc = {
						err:err,
						doc:doc,
						query:query,
						dataStruct:dataStruct
					};
					msg = {
						status:"Background Error",
						message:"[updateTrueIndexTable] Error Processing Data",
						description: desc
					};
					setImmediate(GLOBAL.sendEmail,msg);
				}				
			}else{
				var desc = {
					err:err,
					doc:doc,
					query:query,
					dataStruct:dataStruct
				};
				msg = {
					status:"Background Error",
					message:"[updateTrueIndexTable] Error Processing Data",
					description: desc
				};
				setImmediate(GLOBAL.sendEmail,msg);
			}
		});
	}catch(err){
		msg = {
			status:"Background Error",
			message:"[updateTrueIndexTable] Error Processing Data",
			description: err
		};
		setImmediate(GLOBAL.sendEmail,msg);
	}
}
var markStructAsProcess99=function(dataStruct)
{
	var notificationCollection = GLOBAL.notificationCollection;
	var query = {
			_id:dataStruct._id
		};
	notificationCollection.update(query,{$set:{"process_marker":99}},function(err,doc){
		if(err!=null)
		{
			var msg = {
				err:err,
				doc:doc,
				message:"Failed to mark 99",
				query:query
			};
			console.log(util.inspect(msg,{showHidden: false, depth: null}));
		}
	});
}
module.exports = {
	notificationEventSwitcher:notificationEventSwitcher,
	trueIndexTable:trueIndexTable
}