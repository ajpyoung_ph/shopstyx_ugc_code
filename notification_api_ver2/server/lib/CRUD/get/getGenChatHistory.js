var jsonStuff = require('../../jsonModifiers/jsonLib');
var mongojs = require('mongojs');
var idGen = require('../../generateLatest/idGenerator');
var pmx = require('pmx');
var probe = pmx.probe();
 
var globalArrayCounter = probe.counter({
  name : 'ArraysLive'
});


GLOBAL.LatestChatCollector = [];
GLOBAL.LatestChatCollectorCounter = [];
function collectAllLatestChat(doc,maxLength,session_id,dataStruct)
{
	try{
		if(typeof(GLOBAL.LatestChatCollector[session_id])=='undefined')
		{
			GLOBAL.LatestChatCollector[session_id]=[];
			setTimeout(idGen.deleteMe, 10000,GLOBAL.LatestChatCollector,session_id);
			//globalArrayCounter.inc();
		}
		if(typeof(GLOBAL.LatestChatCollectorCounter[session_id])=='undefined')
		{
			GLOBAL.LatestChatCollectorCounter[session_id]=0;
			setTimeout(idGen.deleteMe, 10000,GLOBAL.LatestChatCollectorCounter,session_id);
			//globalArrayCounter.inc();
		}
		if(doc!='')
		{
			GLOBAL.LatestChatCollector[session_id].push(doc);
		}
		GLOBAL.LatestChatCollectorCounter[session_id]=GLOBAL.LatestChatCollectorCounter[session_id]+1;
		if(GLOBAL.LatestChatCollectorCounter[session_id]==maxLength)
		{
			var msg = {
				status:"Success",
				message:"Data Found",
				data: GLOBAL.LatestChatCollector[session_id]
			};
			// setTimeout(idGen.deleteMe, 5000,GLOBAL.LatestChatCollectorCounter,session_id);
			// setTimeout(idGen.deleteMe, 5000,GLOBAL.LatestChatCollector,session_id);
			try{
				delete GLOBAL.LatestChatCollectorCounter[session_id];
				//globalArrayCounter.dec();
				delete GLOBAL.LatestChatCollector[session_id];
				//globalArrayCounter.dec();
			}catch(err){
				//do nothing here
			}
			
			GLOBAL.sendError(msg,dataStruct.res);
		}
	}catch(err){
		if(typeof(GLOBAL.LatestChatCollector[session_id])=='undefined')
		{
			GLOBAL.LatestChatCollector[session_id]=[];
			setTimeout(idGen.deleteMe, 10000,GLOBAL.LatestChatCollector,session_id);
			//globalArrayCounter.inc();
		}
		if(typeof(GLOBAL.LatestChatCollectorCounter[session_id])=='undefined')
		{
			GLOBAL.LatestChatCollectorCounter[session_id]=0;
			setTimeout(idGen.deleteMe, 10000,GLOBAL.LatestChatCollectorCounter,session_id);
			//globalArrayCounter.inc();
		}
		GLOBAL.LatestChatCollectorCounter[session_id]=GLOBAL.LatestChatCollectorCounter[session_id]+1;
		var msg = {
			status:"Background Error",
			message:"[collectAllLatestChat] Error Processing Data",
			description: err
		};
		GLOBAL.sendEmail(msg);
		if(GLOBAL.LatestChatCollectorCounter[session_id]==maxLength)
		{
			var msg = {
				status:"Success",
				message:"Data Found",
				data: GLOBAL.LatestChatCollector[session_id]
			};
			// setTimeout(idGen.deleteMe, 5000,GLOBAL.LatestChatCollectorCounter,session_id);
			// setTimeout(idGen.deleteMe, 5000,GLOBAL.LatestChatCollector,session_id);
			try{
				delete GLOBAL.LatestChatCollectorCounter[session_id];
				//globalArrayCounter.dec();
				delete GLOBAL.LatestChatCollector[session_id];
				//globalArrayCounter.dec();
			}catch(err){
				//do nothing here
			}
			
			GLOBAL.sendError(msg,dataStruct.res);
		}
	}
}
GLOBAL.chatListCounterCollectorHolder = [];
GLOBAL.chatListCounterCollectorCounter = [];
function chatListCounterCollector(verbVal,replyStruct,dataStruct,session_id,maxType)
{
	try{
		if(typeof(GLOBAL.chatListCounterCollectorHolder[session_id])=='undefined')
		{
			GLOBAL.chatListCounterCollectorHolder[session_id]=[];
			setTimeout(idGen.deleteMe, 10000,GLOBAL.chatListCounterCollectorHolder,session_id);
			//globalArrayCounter.inc();
		}
		if(typeof(GLOBAL.chatListCounterCollectorCounter[session_id])=='undefined')
		{
			GLOBAL.chatListCounterCollectorCounter[session_id]=[];
			GLOBAL.chatListCounterCollectorCounter[session_id]=0;
			setTimeout(idGen.deleteMe, 10000,GLOBAL.chatListCounterCollectorCounter,session_id);
			//globalArrayCounter.inc();
		}
		if(replyStruct!='')
		{
			GLOBAL.chatListCounterCollectorHolder[session_id].push(replyStruct);
			GLOBAL.chatListCounterCollectorCounter[session_id]=GLOBAL.chatListCounterCollectorCounter[session_id]+1;
		}
		if(GLOBAL.chatListCounterCollectorCounter[session_id]==maxType)
		{
			var msg = {
				status:"Success",
				message:"[chatListCounterCollector] Data Collected",
				data:GLOBAL.chatListCounterCollectorHolder[session_id]
			};
			// setTimeout(idGen.deleteMe, 5000,GLOBAL.chatListCounterCollectorHolder,session_id);
			// setTimeout(idGen.deleteMe, 5000,GLOBAL.chatListCounterCollectorCounter,session_id);
			try{
				delete GLOBAL.chatListCounterCollectorHolder[session_id];
				//globalArrayCounter.dec();
				delete GLOBAL.chatListCounterCollectorCounter[session_id];
				//globalArrayCounter.dec();
			}catch(err){
				//do nothing here
			}
			
			GLOBAL.sendError(msg,dataStruct.res);
		}
	}catch(err){
		if(typeof(GLOBAL.chatListCounterCollectorCounter[session_id])=='undefined')
		{
			GLOBAL.chatListCounterCollectorCounter[session_id]=[];
			GLOBAL.chatListCounterCollectorCounter[session_id]=0;
			setTimeout(idGen.deleteMe, 10000,GLOBAL.chatListCounterCollectorCounter,session_id);
			//globalArrayCounter.inc();
		}
		if(typeof(GLOBAL.chatListCounterCollectorHolder[session_id])=='undefined')
		{
			GLOBAL.chatListCounterCollectorHolder[session_id]=[];
			setTimeout(idGen.deleteMe, 10000,GLOBAL.chatListCounterCollectorHolder,session_id);
			//globalArrayCounter.inc();
		}
		GLOBAL.chatListCounterCollectorCounter[session_id]=GLOBAL.chatListCounterCollectorCounter[session_id]+1;
		var msg = {
			status:"Background Error",
			message:"[chatListCounterCollector] Error Finding Data",
			description: err
		};
		GLOBAL.sendEmail(msg);
		if(GLOBAL.chatListCounterCollectorCounter[session_id]==maxType)
		{
			var msg = {
				status:"Success",
				message:"[chatListCounterCollector] Data Collected",
				data:GLOBAL.chatListCounterCollectorHolder[session_id]
			};
			// setTimeout(idGen.deleteMe, 5000,GLOBAL.chatListCounterCollectorHolder,session_id);
			// setTimeout(idGen.deleteMe, 5000,GLOBAL.chatListCounterCollectorCounter,session_id);
			try{
				delete GLOBAL.chatListCounterCollectorHolder[session_id];
				//globalArrayCounter.dec();
				delete GLOBAL.chatListCounterCollectorCounter[session_id];
				//globalArrayCounter.dec();
			}catch(err){
				//do nothing here
			}
			
			GLOBAL.sendError(msg,dataStruct.res);
		}
	}
}
function specificChatListCounter(verbVal,replyStruct,dataStruct,session_id,maxType,query)
{
	try{
		if(replyStruct!='')
		{
			var myquery = '{"$and":[{"verb":"'+verbVal+'"},{"server_generated_read_tags.read'+dataStruct.user_id+'":false}]}';
			myquery=eval("("+myquery+")");
			GLOBAL.chatCollection.count(myquery,function(err,docs){
				if(err==null)
				{
					var myreplyStruct = {
						"unread":docs,
						"chatdata":replyStruct.chatdata
					};
					//console.log(myreplyStruct);
					chatListCounterCollector(verbVal,myreplyStruct,dataStruct,session_id,maxType);
				}else{
					var desc = {
						err:err,
						docs:docs,
						query:query
					};
					var msg = {
						status:"Background Error",
						message:"[specificChatListCounter] Error Finding Count Data",
						description: desc
					};
					chatListCounterCollector(verbVal,replyStruct,dataStruct,session_id,maxType);
					GLOBAL.sendEmail(msg);
				}
			});
		}else{
			chatListCounterCollector(verbVal,replyStruct,dataStruct,session_id,maxType);
		}
	}catch(err){
		var msg = {
			status:"Background Error",
			message:"[specificChatListCounter] Error Finding Data",
			description: err
		};
		chatListCounterCollector(verbVal,replyStruct,dataStruct,session_id,maxType);
		GLOBAL.sendEmail(msg);
	}
}

var readALLChatsForSpecificUserNOCHECKS_modified = function(dataStruct)
{
	try{
		//get all pairs of source_id from the chatIndex
		///:token/:user_id/get/latest/chat/all/now/:number_of_records/:page_number/
		var session_id = idGen.latest_shopstyx_order_id(dataStruct);
		var limit = parseInt(dataStruct.req.params.number_of_records);
		var skip = limit * parseInt(dataStruct.req.params.page_number);
		var query = {
			"users.user_id":dataStruct.user_id
		};
		if(typeof(dataStruct.query)!='undefined')
		{
			query = dataStruct.query;
		}
		GLOBAL.chatIndex.find(query,{"users":1}).sort({"date":-1}).skip(skip).limit(limit,function(err,docs){
			if(err==null)
			{
				var list_of_ids = [];
				var maxLength=docs.length;
				docs.forEach(function(singleDoc){
					singleDoc.users.forEach(function(user_info){
						if(parseInt(user_info.user_id)!=parseInt(dataStruct.user_id))
						{
							list_of_ids.push(user_info.user_id);
						}
					});
				});
				list_of_ids.forEach(function(target_user_id){
					var singleQuery = {
						"$or":[
							{
								"$and":[
									{"from_info.user_id":dataStruct.user_id},
									{"to_info.user_id":parseInt(target_user_id)}
								]
							},
							{
								"$and":[
									{"to_info.user_id":dataStruct.user_id},
									{"from_info.user_id":parseInt(target_user_id)}
								]
							}
						]
					};
					GLOBAL.chatCollection.find(singleQuery).sort({"date":-1}).limit(1,function(err,docs){
						if(err==null)
						{
							if(docs.length>0)
							{
								docs[0]['talking_to']=[];
								docs[0]['talking_to']=target_user_id;
								var doc = docs[0];
							}else{
								var doc = '';
							}
							collectAllLatestChat(doc,maxLength,session_id,dataStruct);
						}else{
							var desc = {
								err:err,
								docs:docs,
								query:query
							};
							var msg = {
								status:"Error",
								message:"[readALLChatsForSpecificUserNOCHECKS_modified] Error Accessing Data",
								description: desc
							};
							GLOBAL.sendEmail(msg);
						}
					});
				});
			}else{
				var desc = {
					err:err,
					docs:docs,
					query:query
				};
				var msg = {
					status:"Error",
					message:"[readALLChatsForSpecificUserNOCHECKS_modified] Error Accessing Data",
					description: desc
				};
				GLOBAL.sendError(msg,dataStruct.res);
			}
		});
	}catch(err){
		var msg = {
			status:"Error",
			message:"[readALLChatsForSpecificUserNOCHECKS_modified] Error Processing Data",
			description: err
		};
		GLOBAL.sendError(msg,dataStruct.res);
	}
}

var readALLChatsForSpecificUserNOCHECKS_modifiedUserID = function(dataStruct)
{
	try{
		var verbList = ["chat","order-chat","product-chat"];
		var maxType = verbList.length;
		var session_id = idGen.latest_shopstyx_order_id(dataStruct);
		verbList.forEach(function(verbVal){
			var query = {
				"$and":[
					{
						"$or":[
							{
								"$and":[
									{
										"from_info.user_id":parseInt(dataStruct.user_id)
									},
									{
										"to_info.user_id":parseInt(dataStruct.req.params.target_user_id)
									}
								]
							},
							{
								"$and":[
									{
										"to_info.user_id":parseInt(dataStruct.user_id)
									},
									{
										"from_info.user_id":parseInt(dataStruct.req.params.target_user_id)
									}
								]
							}
						]
					},
					{
						"verb":verbVal
					}
				]
			};

			GLOBAL.chatCollection.find(query).sort({"date":-1}).limit(1,function(err,docs){
				if(err==null)
				{
					var mychatdata = docs[0];
					mychatdata['talking_to']=parseInt(dataStruct.req.params.target_user_id);
					var replyStruct = {
						chatdata:mychatdata
					};
					setImmediate(specificChatListCounter,verbVal,replyStruct,dataStruct,session_id,maxType,query);
				}else{
					var desc = {
						err:err,
						docs:docs,
						query:query
					};
					var msg = {
						status:"Background Error",
						message:"[readALLChatsForSpecificUserNOCHECKS_modifiedUserID] Error Finding Data",
						description: desc
					};
					var replyStruct = '';
					setImmediate(specificChatListCounter,verbVal,replyStruct,dataStruct,session_id,maxType,query);
					GLOBAL.sendEmail(msg);
				}
			});
		});
	}catch(err){
		var msg = {
			status:"Error",
			message:"[readALLChatsForSpecificUserNOCHECKS_modifiedUserID] Error Processing Data",
			description: err
		};
		GLOBAL.sendError(msg,dataStruct.res);
	}
}

var readALLChatsForSpecificUserNOCHECKS = function(dataStruct)
{
	try{
		///:token/:user_id/get/latest/chat/all/now/:number_of_records/:page_number/
		var res = dataStruct.res;
		var req = dataStruct.req;

		var source_user_id = dataStruct.user_id;
		var number_of_records = parseInt(req.params.number_of_records);
		var page_number = parseInt(req.params.page_number);
		var skip = number_of_records * page_number;

		var query = {
			"$and":[
				{
					"$or":[
						{"from_info.user_id":dataStruct.user_id},
						{"to_info.user_id":dataStruct.user_id}
					]
				}
			]
		};
		if(typeof(dataStruct.query)!='undefined')
		{
			query = dataStruct.query;
		}
		GLOBAL.chatCollection.find(query).limit(page_number).skip(skip).sort({"date":-1},function(err,docs){
			if(err==null)
			{
				var msg = {
					status:"Success",
					message:"Found Data",
					data:[]
				};
				if(docs.length>0)
				{
					//sort data
					docs.sort(jsonStuff.sortby("date"));
					//insert talking_to
					var newDocs = [];
					docs.forEach(function(singledocument){
						singledocument['talking_to']=[];
						if(singledocument.from_info.user_id == source_user_id)
						{
							singledocument.talking_to=singledocument.to_info.user_id;
						}else{
							singledocument.talking_to=singledocument.from_info.user_id;
						}
						newDocs.push(singledocument);
					});
					msg.data = newDocs;
				}
				GLOBAL.sendError(msg,dataStruct.res);
			}else{
				var desc = {
					err:err,
					docs:docs,
					query:query
				};
				var msg = {
					status:"Error",
					message:"[readALLChatsForSpecificUserNOCHECKS] Error Finding Data",
					description: query
				};
				GLOBAL.sendError(msg,dataStruct.res);
			}
		});
	}catch(err){
		var msg = {
			status:"Error",
			message:"[readALLChatsForSpecificUserNOCHECKS] Error Processing Data",
			description: err
		};
		GLOBAL.sendError(msg,dataStruct.res);
	}
}

var readALLChatsForSpecificUserAFTERIDNOCHECKS = function(dataStruct)
{
	try{
		///:token/:user_id/get/latest/chat/all/now/by_date/:number_of_records/:page_number/:date_start/
		// var query = {
		// 	"$and":[
		// 		{
		// 			"$or":[
		// 				{"from_info.user_id":dataStruct.user_id},
		// 				{"to_info.user_id":dataStruct.user_id},
		// 			]
		// 		},
		// 		{"date":{"$gt":parseInt(dataStruct.req.params.date_start)}}
		// 	]
		// };
		var query = {
			"$and":[
				{"users.user_id":dataStruct.user_id},
				{"date":{"$gt":parseInt(dataStruct.req.params.date_start)}}
			]
		};
		dataStruct.query = [];
		dataStruct.query = query;

		readALLChatsForSpecificUserNOCHECKS_modified(dataStruct);
	}catch(err){
		var msg = {
			status:"Error",
			message:"[readALLChatsForSpecificUserAFTERIDNOCHECKS] Error Processing Data",
			description: err
		};
		GLOBAL.sendError(msg,dataStruct.res);
	}
}

var readALLGenericChatsForSpecificUserAndTargetUserNOCHECKS = function(dataStruct)
{
	try{
		///:token/:user_id/get/latest/chat/generic/user_id/now/:number_of_records/:page_number/:target_user_id/
		
		var query = {
			"$and":[
				{
					"$or":[
						{
							"$and":[
								{"from_info.user_id":dataStruct.user_id},
								{"to_info.user_id":parseInt(dataStruct.req.params.target_user_id)}
							]
						},
						{
							"$and":[
								{"to_info.user_id":dataStruct.user_id},
								{"from_info.user_id":parseInt(dataStruct.req.params.target_user_id)}
							]
						}
					]
				},
				{"verb":"chat"}
			]
		};
		dataStruct.query = [];
		dataStruct.query = query;

		readALLChatsForSpecificUserNOCHECKS(dataStruct);
	}catch(err){
		var msg = {
			status:"Error",
			message:"[readALLGenericChatsForSpecificUserAndTargetUserNOCHECKS] Error Processing Data",
			description: err
		};
		GLOBAL.sendError(msg,dataStruct.res);
	}
}

var readGenericChatsAfterID = function(dataStruct)
{
	try{
		///:token/:user_id/get/latest/chat/generic/user_id/_id/now/:number_of_records/:page_number/:target_user_id/:_id
		var res = dataStruct.res;
		var req = dataStruct.req;
		var target_rec_id = req.params._id;

		var query = {
			"_id":mongojs.ObjectId(target_rec_id.toString())
		};

		GLOBAL.chatCollection.findOne(query,function(err,doc){
			if(err==null)
			{
				if(doc!=null)
				{
					var query = {
						"$and":[
							{
								"$or":[
									{
										"$and":[
											{"from_info.user_id":dataStruct.user_id},
											{"to_info.user_id":parseInt(dataStruct.req.params.target_user_id)}
										]
									},
									{
										"$and":[
											{"to_info.user_id":dataStruct.user_id},
											{"from_info.user_id":parseInt(dataStruct.req.params.target_user_id)}
										]
									}
								]								
							},
							{"date":{"$gt":parseInt(doc.date)}},
							{"verb":"chat"}
						]
					};
					dataStruct.query = [];
					dataStruct.query = query;
					readALLChatsForSpecificUserNOCHECKS(dataStruct);
				}else{
					var msg = {
					status:"Success",
					message:"[readGenericChatsAfterID] No records found",
					data:[]
				};
				GLOBAL.sendError(msg,dataStruct.res);
				}
			}else{
				var desc = {
					err:err,
					doc:doc,
					query:query
				};
				var msg = {
					status:"Error",
					message:"[readGenericChatsAfterID] Error Processing Data",
					description: err
				};
				GLOBAL.sendError(msg,dataStruct.res);
			}
		});
	}catch(err){
		var msg = {
			status:"Error",
			message:"[readGenericChatsAfterID] Error Processing Data",
			description: err
		};
		GLOBAL.sendError(msg,dataStruct.res);
	}
}

var readSpecificProductChatsNOCHECKS=function(dataStruct)
{
	try{
		///:token/:user_id/get/latest/chat/product/user_id/now/:number_of_records/:page_number/:target_user_id/:product_id/:store_id/:store_user_id/
		var res = dataStruct.res;
		var req = dataStruct.req;

		var query = {
			"$and":[
				{
					"$or":[
						{
							"$and":[
								{"from_info.user_id":dataStruct.user_id},
								{"to_info.user_id":parseInt(req.params.target_user_id)}
							]
						},
						{
							"$and":[
								{"to_info.user_id":dataStruct.user_id},
								{"from_info.user_id":parseInt(req.params.target_user_id)}
							]
						}
					]
				},				
				{
					"extra_information.product_info.product_id":parseInt(req.params.product_id)
				},
				{
					"extra_information.product_info.store_id":parseInt(req.params.store_id)
				},
				{
					"extra_information.product_info.store_user_id":parseInt(req.params.store_user_id)
				},
				{
					"verb":"product-chat"
				}
			]
		};
		dataStruct.query=[];
		dataStruct.query=query;
		readALLChatsForSpecificUserNOCHECKS(dataStruct);
	}catch(err){
		var msg = {
			status:"Error",
			message:"[readSpecificProductChatsNOCHECKS] Error Processing Data",
			description: err
		};
		GLOBAL.sendError(msg,dataStruct.res);
	}
}

var readProductChatsAfterUnixDate = function(dataStruct)
{
	try{
		///:token/:user_id/get/latest/chat/product/user_id/_id/now/:number_of_records/:page_number/:target_user_id/:product_id/:store_id/:store_user_id/:unixdate/
		var res = dataStruct.res;
		var req = dataStruct.req;
		var targetDate = parseInt(req.params.unixdate);

		var query = {
			"$and":[
				{
					"$or":[
						{
							"$and":[
								{"from_info.user_id":dataStruct.user_id},
								{"to_info.user_id":parseInt(dataStruct.req.params.target_user_id)}
							]
						},
						{
							"$and":[
								{"to_info.user_id":dataStruct.user_id},
								{"from_info.user_id":parseInt(dataStruct.req.params.target_user_id)}
							]
						}
					]
				},
				{"date":{"$gt":parseInt(doc.date)}},
				{"verb":"product-chat"},
				{"extra_information.product_info.product_id":parseInt(req.params.product_id)},
				{"extra_information.product_info.store_id":parseInt(req.params.store_id)},
				{"extra_information.product_info.store_user_id":parseInt(req.params.store_user_id)}
			]
		};
		dataStruct.query = [];
		dataStruct.query = query;
		readALLChatsForSpecificUserNOCHECKS(dataStruct);
	}catch(err){
		var msg = {
			status:"Error",
			message:"[readProductChatsAfterUnixDate] Error Processing Data",
			description: err
		};
		GLOBAL.sendError(msg,dataStruct.res);
	}
}
var readProductChatsAfterID = function(dataStruct)
{
	try{
		///:token/:user_id/get/latest/chat/product/user_id/_id/now/:number_of_records/:page_number/:target_user_id/:product_id/:store_id/:store_user_id/:_id/
		var res = dataStruct.res;
		var req = dataStruct.req;
		var target_rec_id = req.params._id;

		var query = {
			"$and":[
				{
					"_id":mongojs.ObjectId(target_rec_id.toString())
				},
				{
					"verb":"product-chat"
				}
			]
		};

		GLOBAL.chatCollection.findOne(query,function(err,doc){
			if(err==null)
			{
				if(doc!=null)
				{
					var query = {
						"$and":[
							{
								"$or":[
									{
										"$and":[
											{"from_info.user_id":dataStruct.user_id},
											{"to_info.user_id":parseInt(dataStruct.req.params.target_user_id)}
										]
									},
									{
										"$and":[
											{"to_info.user_id":dataStruct.user_id},
											{"from_info.user_id":parseInt(dataStruct.req.params.target_user_id)}
										]
									}
								]
							},
							{"date":{"$gt":parseInt(doc.date)}},
							{"verb":"product-chat"},
							{"extra_information.product_info.product_id":parseInt(req.params.product_id)},
							{"extra_information.product_info.store_id":parseInt(req.params.store_id)},
							{"extra_information.product_info.store_user_id":parseInt(req.params.store_user_id)}
						]
					};
					dataStruct.query = [];
					dataStruct.query = query;
					readALLChatsForSpecificUserNOCHECKS(dataStruct);
				}else{
					var msg = {
						status:"Success",
						message:"[readProductChatsAfterID] No records found",
						data:[]
					};
					GLOBAL.sendError(msg,dataStruct.res);
				}
			}else{
				var desc = {
					err:err,
					doc:doc,
					query:query
				};
				var msg = {
					status:"Error",
					message:"[readProductChatsAfterID] Error Processing Data",
					description: err
				};
				GLOBAL.sendError(msg,dataStruct.res);
			}
		});
	}catch(err){
		var msg = {
			status:"Error",
			message:"[readProductChatsAfterID] Error Processing Data",
			description: err
		};
		GLOBAL.sendError(msg,dataStruct.res);
	}
}

var readSpecificOrderChatsNOCHECKS = function(dataStruct)
{
	try{
		///:token/:user_id/get/latest/chat/order/now/:number_of_records/:page_number/:target_user_id/:order_id/
		var query = {
			"$and":[
				{"verb":"order-chat"},
				{
					"extra_information.order_info.order_id":dataStruct.req.params.order_id
				},
				{
					"$or":[
						{
							"$and":[
								{"from_info.user_id":dataStruct.user_id},
								{"to_info.user_id":parseInt(dataStruct.req.params.target_user_id)}
							]
						},
						{
							"$and":[
								{"to_info.user_id":dataStruct.user_id},
								{"from_info.user_id":parseInt(dataStruct.req.params.target_user_id)}
							]
						}
					]
				}
			]
		};
		dataStruct.query = [];
		dataStruct.query = query;
		readALLChatsForSpecificUserNOCHECKS(dataStruct);
	}catch(err){
		var msg = {
			status:"Error",
			message:"[readSpecificOrderChatsNOCHECKS] Error Processing Data",
			description: err
		};
		GLOBAL.sendError(msg,dataStruct.res);
	}
}

var readOrderChatsAfterID = function(dataStruct)
{
	try{
		///:token/:user_id/get/latest/chat/order/user_id/_id/now/:number_of_records/:page_number/:target_user_id/:order_id/:_id
		var query = {
			"_id":mongojs.ObjectId((dataStruct.req.params._id).toString())
		};
		GLOBAL.chatCollection.findOne(query,function(err,doc){
			if(err==null)
			{
				if(doc!=null)
				{
					query = {
						"$and":[
							{
								"$or":[
									{
										"$and":[
											{"from_info.user_id":dataStruct.user_id},
											{"to_info.user_id":parseInt(dataStruct.req.params.target_user_id)}
										]
									},
									{
										"$and":[
											{"to_info.user_id":dataStruct.user_id},
											{"from_info.user_id":parseInt(dataStruct.req.params.target_user_id)}
										]
									}
								]
							},
							{"date":{"$gt":doc.date}},
							{"verb":"order-chat"},
							{"extra_information.order_info.order_id":dataStruct.req.params.order_id}
						]
					};
					dataStruct.query = [];
					dataStruct.query = query;
					readALLChatsForSpecificUserNOCHECKS(dataStruct);
				}else{
					var msg = {
						status:"Success",
						message:"[readOrderChatsAfterID] No Records Found",
						data:[]
					};
					GLOBAL.sendError(msg,dataStruct.res);
				}
			}else{
				var desc = {
					err:err,
					doc:doc,
					query:query
				};
				var msg = {
					status:"Error",
					message:"[readOrderChatsAfterID] Error Processing Data",
					description: desc
				};
				GLOBAL.sendError(msg,dataStruct.res);
			}
		});
	}catch(err){
		var msg = {
			status:"Error",
			message:"[readOrderChatsAfterID] Error Processing Data",
			description: err
		};
		GLOBAL.sendError(msg,dataStruct.res);
	}
}
var readSpecificStoreChatsNOCHECKS=function(dataStruct)
{
	try{
		///:token/:user_id/get/latest/chat/product/user_id/now/:number_of_records/:page_number/:target_user_id/:product_id/:store_id/:store_user_id/
		var res = dataStruct.res;
		var req = dataStruct.req;

		var query = {
			"$and":[
				{
					"$or":[
						{
							"$and":[
								{"from_info.user_id":dataStruct.user_id},
								{"to_info.user_id":parseInt(req.params.target_user_id)}
							]
						},
						{
							"$and":[
								{"to_info.user_id":dataStruct.user_id},
								{"from_info.user_id":parseInt(req.params.target_user_id)}
							]
						}
					]
				},				
				{
					"extra_information.store_info.claimed_by":parseInt(req.params.claimed_by)
				},
				{
					"extra_information.store_info.store_id":parseInt(req.params.store_id)
				},
				{
					"extra_information.store_info.store_user_id":parseInt(req.params.store_user_id)
				},
				{
					"verb":"store-chat"
				}
			]
		};
		dataStruct.query=[];
		dataStruct.query=query;
		readALLChatsForSpecificUserNOCHECKS(dataStruct);
	}catch(err){
		var msg = {
			status:"Error",
			message:"[readSpecificStoreChatsNOCHECKS] Error Processing Data",
			description: err
		};
		GLOBAL.sendError(msg,dataStruct.res);
	}
}
var readStoreChatsAfterID = function(dataStruct)
{
	try{
		///:token/:user_id/get/latest/chat/product/user_id/_id/now/:number_of_records/:page_number/:target_user_id/:product_id/:store_id/:store_user_id/:_id/
		var res = dataStruct.res;
		var req = dataStruct.req;
		var target_rec_id = req.params._id;

		var query = {
			"$and":[
				{
					"_id":mongojs.ObjectId(target_rec_id.toString())
				},
				{
					"verb":"store-chat"
				}
			]
		};

		GLOBAL.chatCollection.findOne(query,function(err,doc){
			if(err==null)
			{
				if(doc!=null)
				{
					var query = {
						"$and":[
							{
								"$or":[
									{
										"$and":[
											{"from_info.user_id":dataStruct.user_id},
											{"to_info.user_id":parseInt(dataStruct.req.params.target_user_id)}
										]
									},
									{
										"$and":[
											{"to_info.user_id":dataStruct.user_id},
											{"from_info.user_id":parseInt(dataStruct.req.params.target_user_id)}
										]
									}
								]
							},
							{"date":{"$gt":parseInt(doc.date)}},
							{"verb":"product-chat"},
							{"extra_information.store_info.claimed_by":parseInt(req.params.claimed_by)},
							{"extra_information.store_info.store_id":parseInt(req.params.store_id)},
							{"extra_information.store_info.store_user_id":parseInt(req.params.store_user_id)}
						]
					};
					dataStruct.query = [];
					dataStruct.query = query;
					readALLChatsForSpecificUserNOCHECKS(dataStruct);
				}else{
					var msg = {
						status:"Success",
						message:"[readStoreChatsAfterID] No records found",
						data:[]
					};
					GLOBAL.sendError(msg,dataStruct.res);
				}
			}else{
				var desc = {
					err:err,
					doc:doc,
					query:query
				};
				var msg = {
					status:"Error",
					message:"[readStoreChatsAfterID] Error Processing Data",
					description: err
				};
				GLOBAL.sendError(msg,dataStruct.res);
			}
		});
	}catch(err){
		var msg = {
			status:"Error",
			message:"[readStoreChatsAfterID] Error Processing Data",
			description: err
		};
		GLOBAL.sendError(msg,dataStruct.res);
	}
}

GLOBAL.readAllChatsForUserCollectorHolder=[];
GLOBAL.readAllChatsForUserCollectorHolderCounter=[];
GLOBAL.readAllChatsForUserCollectormaxTypeHolder=[];
GLOBAL.readAllChatsForUserCollectorForceReplyHolder=[];
function readAllChatsForUserCollector(doc,maxType,session_id,dataStruct)
{
	if(typeof(GLOBAL.readAllChatsForUserCollectorHolder[session_id])=='undefined')
	{
		GLOBAL.readAllChatsForUserCollectorHolder[session_id]=[];
		setTimeout(idGen.deleteMe, 10000,GLOBAL.readAllChatsForUserCollectorHolder,session_id);
		//globalArrayCounter.inc();
	}

	if(typeof(GLOBAL.readAllChatsForUserCollectorHolderCounter[session_id])=='undefined')
	{
		GLOBAL.readAllChatsForUserCollectorHolderCounter[session_id]=[];
		GLOBAL.readAllChatsForUserCollectorHolderCounter[session_id]=0;
		GLOBAL.readAllChatsForUserCollectormaxTypeHolder[session_id]=[];
		GLOBAL.readAllChatsForUserCollectormaxTypeHolder[session_id]=maxType;

		setTimeout(idGen.deleteMe, 10000,GLOBAL.readAllChatsForUserCollectorHolderCounter,session_id);
		//globalArrayCounter.inc();
		setTimeout(idGen.deleteMe, 10000,GLOBAL.readAllChatsForUserCollectormaxTypeHolder,session_id);
		//globalArrayCounter.inc();
	}
	if(doc!='')
	{
		if(doc!=null)
		{
			GLOBAL.readAllChatsForUserCollectorHolder[session_id].push(doc);
		}
		
	}
	GLOBAL.readAllChatsForUserCollectorHolderCounter[session_id]=GLOBAL.readAllChatsForUserCollectorHolderCounter[session_id]+1;
	if(GLOBAL.readAllChatsForUserCollectormaxTypeHolder[session_id]<maxType)
	{
		GLOBAL.readAllChatsForUserCollectormaxTypeHolder[session_id]=maxType;
	}else{
		maxType=GLOBAL.readAllChatsForUserCollectormaxTypeHolder[session_id];
	}
	// if(maxType<GLOBAL.readAllChatsForUserCollectormaxTypeHolder[session_id])
	// {
	// 	maxType=GLOBAL.readAllChatsForUserCollectormaxTypeHolder[session_id];
	// }
	console.log("maxType->"+maxType+" session_id_count->"+GLOBAL.readAllChatsForUserCollectorHolderCounter[session_id]);
	console.log(GLOBAL.readAllChatsForUserCollectorHolder[session_id]);
	if(GLOBAL.readAllChatsForUserCollectorHolderCounter[session_id]==1)
	{
		//force reply even if not finished
		GLOBAL.readAllChatsForUserCollectorForceReplyHolder[session_id]=[];
		GLOBAL.readAllChatsForUserCollectorForceReplyHolder[session_id]=setTimeout(forceReply,1000,session_id,maxType,dataStruct);
		setTimeout(idGen.deleteMe, 10000,GLOBAL.readAllChatsForUserCollectorForceReplyHolder,session_id);
	}
	if(maxType==GLOBAL.readAllChatsForUserCollectorHolderCounter[session_id] && maxType!=1)
	{
		try{
			clearTimeout(GLOBAL.readAllChatsForUserCollectorForceReplyHolder[session_id]);
		}catch(err){
			//do nothing
		}
		
		//sort the data
		try{
			GLOBAL.readAllChatsForUserCollectorHolder[session_id].sort(jsonStuff.sortby("date"));	
		}catch(err){
			console.log("[readAllChatsForUserCollector] data sort error");
			console.log(err);
		}
		
		var msg = {
			status:"Success",
			message:"[readAllChatsForUserCollector] Returning Requested Data",
			data:GLOBAL.readAllChatsForUserCollectorHolder[session_id],
			maxType:maxType,
			counter:GLOBAL.readAllChatsForUserCollectorHolderCounter[session_id],
			length:GLOBAL.readAllChatsForUserCollectorHolder[session_id].length

		};
		// setTimeout(idGen.deleteMe, 5000,GLOBAL.readAllChatsForUserCollectorHolder,session_id);
		// setTimeout(idGen.deleteMe, 5000,GLOBAL.readAllChatsForUserCollectorHolderCounter,session_id);
		// setTimeout(idGen.deleteMe, 5000,GLOBAL.readAllChatsForUserCollectormaxTypeHolder,session_id);
		try{
			delete GLOBAL.readAllChatsForUserCollectorHolder[session_id];
		}catch(err){
			//do nothing
		}
		try{
			delete GLOBAL.readAllChatsForUserCollectorHolderCounter[session_id];
		}catch(err){
			//do nothing
		}
		try{
			delete GLOBAL.readAllChatsForUserCollectormaxTypeHolder[session_id];
		}catch(err){
			//do nothing
		}
		try{
			delete GLOBAL.readAllChatsForUserCollectorForceReplyHolder[session_id];
		}catch(err){
			//do nothing
		}
			//globalArrayCounter.dec();
			
			//globalArrayCounter.dec();
			
			//globalArrayCounter.dec();
			
		try{
			GLOBAL.sendError(msg,dataStruct.res);
		}catch(err){
			//do nothing
		}
		
	}
}

function forceReply(session_id,maxType,dataStruct)
{
	//sort the data
	try{
		GLOBAL.readAllChatsForUserCollectorHolder[session_id].sort(jsonStuff.sortby("date"));	
	}catch(err){
		console.log("[readAllChatsForUserCollector] data sort error");
		console.log(err);
	}
	
	var msg = {
		status:"Success",
		message:"[readAllChatsForUserCollector] Returning Requested Data",
		data:GLOBAL.readAllChatsForUserCollectorHolder[session_id],
		maxType:maxType,
		counter:GLOBAL.readAllChatsForUserCollectorHolderCounter[session_id],
		length:GLOBAL.readAllChatsForUserCollectorHolder[session_id].length

	};
	// setTimeout(idGen.deleteMe, 5000,GLOBAL.readAllChatsForUserCollectorHolder,session_id);
	// setTimeout(idGen.deleteMe, 5000,GLOBAL.readAllChatsForUserCollectorHolderCounter,session_id);
	// setTimeout(idGen.deleteMe, 5000,GLOBAL.readAllChatsForUserCollectormaxTypeHolder,session_id);
	try{
		delete GLOBAL.readAllChatsForUserCollectorHolder[session_id];
	}catch(err){
		//do nothing
	}
	try{
		delete GLOBAL.readAllChatsForUserCollectorHolderCounter[session_id];
	}catch(err){
		//do nothing
	}
	try{
		delete GLOBAL.readAllChatsForUserCollectormaxTypeHolder[session_id];
	}catch(err){
		//do nothing
	}
	try{
		delete GLOBAL.readAllChatsForUserCollectorForceReplyHolder[session_id];
	}catch(err){
		//do nothing
	}
		//globalArrayCounter.dec();
		
		//globalArrayCounter.dec();
		
		//globalArrayCounter.dec();
		
	try{
		GLOBAL.sendError(msg,dataStruct.res);
	}catch(err){
		//do nothing
	}
}
var readALLChatsForUser = function(dataStruct)
{
	try{
		///:token/:user_id/get/latest/chat/all/now/user_id/specific/:number_of_records/:page_number/:target_user_id
		//collect all possible chats made by user
		var session_id = idGen.latest_shopstyx_order_id(dataStruct);
		var maxType = 3;//chat - uniquely one, store-chat - uniquely one, (product-chat - multiple, order-chat = multiple) - counted as one so total is 3
		var maxIndexType = 2;//used for the collection of product-chat and order-chat ... so total 2

		if(parseInt(dataStruct.req.params.page_number)>0)
		{
			var typeArray = ['product-chat','order-chat'];
		}else{
			var typeArray = ['chat','product-chat','order-chat','store-chat'];
		}
		
		var query = {
			"$and":[
				{"users.user_id":dataStruct.user_id},
				{"users.user_id":parseInt(dataStruct.req.params.target_user_id)},

			]
		};

		typeArray.forEach(function(verb){
			switch(verb)
			{
				case 'chat':
					var query = {
						"$and":[
							{
								"$or":[
									{
										"$and":[
											{"from_info.user_id":dataStruct.user_id},
											{"to_info.user_id":parseInt(dataStruct.req.params.target_user_id)}
										]
									},
									{
										"$and":[
											{"to_info.user_id":dataStruct.user_id},
											{"from_info.user_id":parseInt(dataStruct.req.params.target_user_id)}
										]
									}
								]
							},
							{
								"verb":verb
							}
						]
					};
					setImmediate(readALLChatsForUserQuery,dataStruct,query,maxType,session_id);
					break;
				case 'product-chat':
					//inquire how many product-chats dataStruct.user_id & parseInt(dataStruct.req.params.target_user_id) has
					var chatIndex = GLOBAL.prodchatIndex;
					readALLChatsForUserChatIndexQuery(dataStruct,query,maxType,session_id,chatIndex,maxIndexType);
					break;
				case 'order-chat':
					
					var chatIndex = GLOBAL.orderchatIndex;
					readALLChatsForUserChatIndexQuery(dataStruct,query,maxType,session_id,chatIndex,maxIndexType);
					break;
				case 'store-chat':
					
					var query = {
						"$and":[
							{
								"$or":[
									{
										"$and":[
											{"from_info.user_id":dataStruct.user_id},
											{"to_info.user_id":parseInt(dataStruct.req.params.target_user_id)}
										]
									},
									{
										"$and":[
											{"to_info.user_id":dataStruct.user_id},
											{"from_info.user_id":parseInt(dataStruct.req.params.target_user_id)}
										]
									}
								]
							},
							{
								"verb":verb
							}
						]
					};
					setImmediate(readALLChatsForUserQuery,dataStruct,query,maxType,session_id);
					break;
			}
			
		});
	}catch(err){
		var msg = {
			status:"Error",
			message:"[readALLChatsForUser] Error Processing Data",
			description: err
		};
		GLOBAL.sendError(msg,dataStruct.res);
	}
}
function readALLChatsForUserQuery(dataStruct,query,maxType,session_id)
{
	if(typeof(dataStruct.date_start)!='undefined')
	{
		var holder = {};
		var queryID = "date";
		holder[queryID]={"$gt":dataStruct.date_start};
		query["$and"].push(holder);
	}
	GLOBAL.chatCollection.find(query).sort({"date":-1}).limit(1,function(err,docs){
		if(err==null)
		{
			var doc=docs[0];
			try{
				doc['talking_to']=[];
				doc['talking_to']=parseInt(dataStruct.req.params.target_user_id);
			}catch(err){

			}			
			readAllChatsForUserCollector(docs[0],maxType,session_id,dataStruct);
		}else{
			readAllChatsForUserCollector('',maxType,session_id,dataStruct);
		}
	});
}
GLOBAL.chatIndexCollectorHolder=[];
GLOBAL.chatIndexCollectorCounter=[];
function chatIndexCollector(dataStruct,query,maxType,session_id,chatIndex,docs,maxIndexType)
{
	if(typeof(GLOBAL.chatIndexCollectorHolder[session_id])=='undefined')
	{
		GLOBAL.chatIndexCollectorHolder[session_id]=[];
		setTimeout(idGen.deleteMe, 10000,GLOBAL.chatIndexCollectorHolder,session_id);
		//globalArrayCounter.inc();
	}
	if(typeof(GLOBAL.chatIndexCollectorCounter[session_id])=='undefined')
	{
		GLOBAL.chatIndexCollectorCounter[session_id]=[];
		GLOBAL.chatIndexCollectorCounter[session_id]=0;
		setTimeout(idGen.deleteMe, 10000,GLOBAL.chatIndexCollectorCounter,session_id);
		//globalArrayCounter.inc();
	}
	if(docs!='')
	{
		docs.forEach(function(doc){
			GLOBAL.chatIndexCollectorHolder[session_id].push(doc);
		});
	}
	GLOBAL.chatIndexCollectorCounter[session_id]=GLOBAL.chatIndexCollectorCounter[session_id]+1;
	//console.log("maxIndexType ->"+maxIndexType+" session_id_count ->"+GLOBAL.chatIndexCollectorCounter[session_id]);
	if(maxIndexType==GLOBAL.chatIndexCollectorCounter[session_id])
	{
		///:token/:user_id/get/latest/chat/all/now/user_id/specific/:number_of_records/:page_number/:target_user_id
		//sort data
		GLOBAL.chatIndexCollectorHolder[session_id].sort(jsonStuff.sortby("date"));
		var skip = parseInt(dataStruct.req.params.number_of_records)*parseInt(dataStruct.req.params.page_number);
		var limit = parseInt(dataStruct.req.params.number_of_records);

		if(GLOBAL.chatIndexCollectorHolder[session_id].length>parseInt(dataStruct.req.params.number_of_records))
		{
			limit = parseInt(dataStruct.req.params.number_of_records);
		}else{
			limit = GLOBAL.chatIndexCollectorHolder[session_id].length;
		}
		if((skip+parseInt(dataStruct.req.params.number_of_records))>parseInt(dataStruct.req.params.number_of_records))
		{
			limit = skip+parseInt(dataStruct.req.params.number_of_records);
		}
		if(skip == 0 && limit != 0)
		{
			var newLimit = limit -1;//because of chat and store-chat
			maxType=maxType+newLimit;
		}else{
			if(limit>parseInt(dataStruct.req.params.number_of_records))
			{
				maxType = parseInt(dataStruct.req.params.number_of_records);
				if(maxType>GLOBAL.chatIndexCollectorHolder[session_id].length)
				{
					maxType=GLOBAL.chatIndexCollectorHolder[session_id].length;
				}
			}else if(limit!=0){
				maxType = limit;
			}
		}
		
		if(skip<GLOBAL.chatIndexCollectorHolder[session_id].length && limit !=0)
		{
			if(limit>GLOBAL.chatIndexCollectorHolder[session_id].length)
			{
				limit = GLOBAL.chatIndexCollectorHolder[session_id].length;
				maxType = limit - skip;
			}
			for(var x = skip;x<limit;x++)
			{
				if(typeof(GLOBAL.chatIndexCollectorHolder[session_id][x]['product_id'])=='undefined')
				{
					var verb = 'order-chat';
				}else{
					var verb = 'product-chat';
				}
				
				//console.log(verb);
				switch(verb.toLowerCase())
				{
					case 'product-chat':
						var query = {
							"$and":[
								{
									"$or":[
										{
											"$and":[
												{"from_info.user_id":dataStruct.user_id},
												{"to_info.user_id":parseInt(dataStruct.req.params.target_user_id)}
											]
										},
										{
											"$and":[
												{"to_info.user_id":dataStruct.user_id},
												{"from_info.user_id":parseInt(dataStruct.req.params.target_user_id)}
											]
										}
									]
								},
								{
									"verb":verb
								},
								{
									"extra_information.product_info.product_id":GLOBAL.chatIndexCollectorHolder[session_id][x]['product_id']
								},
								{
									"extra_information.product_info.store_id":GLOBAL.chatIndexCollectorHolder[session_id][x]['store_id']
								},
								{
									"extra_information.product_info.store_user_id":GLOBAL.chatIndexCollectorHolder[session_id][x]['store_user_id']
								}
							]
						}
						setImmediate(readALLChatsForUserQuery,dataStruct,query,maxType,session_id);
						break;
					case 'order-chat':
						var query = {
							"$and":[
								{
									"$or":[
										{
											"$and":[
												{"from_info.user_id":dataStruct.user_id},
												{"to_info.user_id":parseInt(dataStruct.req.params.target_user_id)}
											]
										},
										{
											"$and":[
												{"to_info.user_id":dataStruct.user_id},
												{"from_info.user_id":parseInt(dataStruct.req.params.target_user_id)}
											]
										}
									]
								},
								{
									"verb":verb
								},
								{
									"extra_information.order_info.order_id":GLOBAL.chatIndexCollectorHolder[session_id][x]['shopstyx_order_id']
								}
							]
						}
						setImmediate(readALLChatsForUserQuery,dataStruct,query,maxType,session_id);
						break;
				}
			}
		}else{
			maxType=1;
			readAllChatsForUserCollector('',maxType,session_id,dataStruct);
		}
		// setTimeout(idGen.deleteMe, 5000,GLOBAL.chatIndexCollectorHolder,session_id);
		// setTimeout(idGen.deleteMe, 5000,GLOBAL.chatIndexCollectorCounter,session_id);
		try{
			delete GLOBAL.chatIndexCollectorHolder[session_id];
			//globalArrayCounter.dec();
			delete GLOBAL.chatIndexCollectorCounter[session_id];
			//globalArrayCounter.dec();
		}catch(err){
			//do nothing here
		}
		
	}
}
function readALLChatsForUserChatIndexQuery(dataStruct,query,maxType,session_id,chatIndex,maxIndexType)
{
	if(typeof(dataStruct.date_start)!='undefined')
	{
		var holder = {};
		var queryID = "date";
		holder[queryID]={"$gt":dataStruct.date_start};
		query["$and"].push(holder);
	}
	chatIndex.find(query).sort({"date":-1},function(err,docs){
		if(err==null)
		{
			chatIndexCollector(dataStruct,query,maxType,session_id,chatIndex,docs,maxIndexType);
		}else{
			chatIndexCollector(dataStruct,query,maxType,session_id,chatIndex,'',maxIndexType);
		}
	});
}
var readALLChatsForUserAfterDate=function(dataStruct)
{
	try{
		///:token/:user_id/get/latest/chat/all/now/user_id/date/specific/:number_of_records/:page_number/:target_user_id/:datetime
		
		dataStruct.date_start = [];
		dataStruct.date_start = parseInt(dataStruct.req.params.datetime);
		readALLChatsForUser(dataStruct);
	}catch(err){
		var msg = {
			status:"Error",
			message:"[readALLChatsForUser] Error Processing Data",
			description: err
		};
		GLOBAL.sendError(msg,dataStruct.res);
	}
}
var reqChatCount=function(dataStruct)
{
	try{
		var res = dataStruct.res;
		var req = dataStruct.req;
		var dataJSON = req.body.data;
		if(typeof(dataJSON)=='undefined')
		{
			dataJSON = req.headers.data;
		}
		if(typeof(dataJSON)=='undefined')
		{
			dataJSON = req.query.data;
		}
		if(typeof(dataJSON)!="object")
		{
			dataJSON = eval("("+dataJSON+")");
		}
		var target_user_id=parseInt(dataJSON.target_user_id);
		var searchable = ['chat','product-chat','order-chat','store-chat'];
		if(searchable.indexOf(dataJSON.verb.toLowerCase())==-1)
		{
			var msg = {
				status:"Error",
				message:"You are not allowed to use this API in the manner you requested",
				description:dataJSON
			};
			GLOBAL.sendError(msg,dataStruct.res);
		}else{
			delete dataJSON.target_user_id;
			var query = {
				"$and":[
					dataJSON,
					{
						"$or":[
							{
								"$and":[
									{"from_info.user_id":dataStruct.user_id},
									{"to_info.user_id":target_user_id}
								]
							},
							{
								"$and":[
									{"from_info.user_id":target_user_id},
									{"to_info.user_id":dataStruct.user_id}
								]
							}
						]
					}
				]
			}
			GLOBAL.chatCollection.count(query,function(err,docs){
				if(err==null)
				{
					var msg = {
						status:"Success",
						message:"Found Data",
						data:[]
					};
					var mydata = {
						verb:dataJSON.verb,
						count:docs,
						talking_to:target_user_id
					};
					msg.data.push(mydata);
					GLOBAL.sendError(msg,dataStruct.res);
				}else{
					var desc = {
						err:err,
						docs:docs,
						query:dataStruct.query
					};
					var msg = {
						status:"Error",
						message:"[reqChatCount] Error Accessing DB",
						description: desc
					};
					GLOBAL.sendError(msg,dataStruct.res);
				}
			});
		}
	}catch(err){
		var msg = {
			status:"Error",
			message:"[reqChatCount] Error Processing Data",
			description: err
		};
		GLOBAL.sendError(msg,dataStruct.res);
	}
}
module.exports = {
	readALLChatsForSpecificUserNOCHECKS_modified:readALLChatsForSpecificUserNOCHECKS_modified,
	readALLChatsForSpecificUserNOCHECKS_modifiedUserID:readALLChatsForSpecificUserNOCHECKS_modifiedUserID,
	readALLChatsForSpecificUserNOCHECKS:readALLChatsForSpecificUserNOCHECKS,
	readALLChatsForSpecificUserAFTERIDNOCHECKS:readALLChatsForSpecificUserAFTERIDNOCHECKS,
	readALLGenericChatsForSpecificUserAndTargetUserNOCHECKS:readALLGenericChatsForSpecificUserAndTargetUserNOCHECKS,
	readGenericChatsAfterID:readGenericChatsAfterID,
	readSpecificProductChatsNOCHECKS:readSpecificProductChatsNOCHECKS,
	readProductChatsAfterID:readProductChatsAfterID,
	readSpecificOrderChatsNOCHECKS:readSpecificOrderChatsNOCHECKS,
	readOrderChatsAfterID:readOrderChatsAfterID,
	readALLChatsForUser:readALLChatsForUser,
	readALLChatsForUserAfterDate:readALLChatsForUserAfterDate,
	readProductChatsAfterUnixDate:readProductChatsAfterUnixDate,
	readSpecificStoreChatsNOCHECKS:readSpecificStoreChatsNOCHECKS,
	readStoreChatsAfterID:readStoreChatsAfterID,
	reqChatCount:reqChatCount
};