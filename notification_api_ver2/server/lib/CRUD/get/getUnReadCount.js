
var genericChatCounterQuery = function(dataStruct)
{
	try{
		
		GLOBAL.chatCollection.count(dataStruct.query,function(err,docs){
			if(err==null)
			{
				var msg = {
					status:"Success",
					message:"Found Data",
					data:[]
				};
				var mydata = {
					unread:docs
				};
				msg.data.push(mydata);
				GLOBAL.sendError(msg,dataStruct.res);
			}else{
				var desc = {
					err:err,
					docs:docs,
					query:dataStruct.query
				};
				var msg = {
					status:"Error",
					message:"[genericchatSpecificUser] Error Accessing DB",
					description: desc
				};
				GLOBAL.sendError(msg,dataStruct.res);
			}
		});
	}catch(err){
		var msg = {
			status:"Error",
			message:"[genericchatSpecificUser] Error Processing Data",
			description: err
		};
		GLOBAL.sendError(msg,dataStruct.res);
	}
}
var genericchatSpecificUser = function(dataStruct)
{
	try{
		///:token/:user_id/get/unread/count/generic/chat/:target_user_id
		if(typeof(dataStruct.req.params.source_user_id)=='undefined')
		{
			var query = '{"$and":[{"server_generated_read_tags.read'+parseInt(dataStruct.user_id)+'": false},{"server_generated_read_tags.read'+parseInt(dataStruct.req.params.target_user_id)+'": {"$exists": true}},{"verb":"chat"}]}';	
		}else{
			var query = '{"$and":[{"server_generated_read_tags.read'+parseInt(dataStruct.req.params.source_user_id)+'": false},{"server_generated_read_tags.read'+parseInt(dataStruct.req.params.target_user_id)+'": {"$exists": true}},{"verb":"chat"}]}';
		}
		
		query = eval("("+query+")");
		dataStruct['query']=[];
		dataStruct.query=query;
		genericChatCounterQuery(dataStruct);
	}catch(err){
		var msg = {
			status:"Error",
			message:"[genericchatSpecificUser] Error Processing Data",
			description: err
		};
		GLOBAL.sendError(msg,dataStruct.res);
	}
}

var productchatSpecificUser = function(dataStruct)
{
	try{
		///:token/:user_id/get/unread/count/product/chat/:target_user_id/:product_id/:store_id/:collection_id
		var query = '{"$and":[{"server_generated_read_tags.read'+parseInt(dataStruct.user_id)+'": false},{"server_generated_read_tags.read'+parseInt(dataStruct.req.params.target_user_id)+'": {"$exists": true}},{"verb":"product-chat"},{"extra_information.product_info.product_id":'+parseInt(dataStruct.req.params.product_id)+'},{"extra_information.product_info.store_id":'+parseInt(dataStruct.req.params.store_id)+'},{"extra_information.product_info.collection_id":'+parseInt(dataStruct.req.params.collection_id)+'}]}';
		query = eval("("+query+")");
		dataStruct['query']=[];
		dataStruct.query=query;
		genericChatCounterQuery(dataStruct);
	}catch(err){
		var msg = {
			status:"Error",
			message:"[productchatSpecificUser] Error Processing Data",
			description: err
		};
		GLOBAL.sendError(msg,dataStruct.res);
	}
}

var orderchatSpecificUser = function(dataStruct)
{
	try{
		///:token/:user_id/get/unread/count/product/chat/:target_user_id/:order_id
		var query = '{"$and":[{"server_generated_read_tags.read'+parseInt(dataStruct.user_id)+'": false},{"server_generated_read_tags.read'+parseInt(dataStruct.req.params.target_user_id)+'": {"$exists": true}},{"verb":"order-chat"},{"extra_information.order_info.order_id":"'+dataStruct.req.params.order_id+'"}]}';
		query = eval("("+query+")");
		dataStruct['query']=[];
		dataStruct.query=query;
		genericChatCounterQuery(dataStruct);
	}catch(err){
		var msg = {
			status:"Error",
			message:"[orderchatSpecificUser] Error Processing Data",
			description: err
		};
		GLOBAL.sendError(msg,dataStruct.res);
	}
}

var orderchatSpecificOrderIDOnly = function(dataStruct)
{
	try{
		///:token/:user_id/get/unread/number/chat/order_id/now/:order_id/
		var query = '{"$and":[{"server_generated_read_tags.read'+parseInt(dataStruct.user_id)+'": false},{"verb":"order-chat"},{"extra_information.order_info.order_id":"'+dataStruct.req.params.order_id+'"}]}';
		query = eval("("+query+")");
		dataStruct['query']=[];
		dataStruct.query=query;
		genericChatCounterQuery(dataStruct);
	}catch(err){
		var msg = {
			status:"Error",
			message:"[orderchatSpecificOrderIDOnly] Error Processing Data",
			description: err
		};
		GLOBAL.sendError(msg,dataStruct.res);
	}
}

var notificationsUnread = function(dataStruct)
{
	try{
		///:token/:user_id/get/unread/count/notifications/
		var query = {
			"$or":[
				{
					"$and":[
						{"from_info.user_id":dataStruct.user_id},
						{"from_info.read":false}
					]
				},
				{
					"$and":[
						{"to_info.user_id":dataStruct.user_id},
						{"to_info.read":false}
					]
				}
			]
		};
		GLOBAL.notificationCollection.count(query,function(err,docs){
			if(err==null)
			{
				var msg = {
					status:"Success",
					message:"Found Data",
					data:[]
				};
				var mydata = {
					unread:docs
				};
				msg.data.push(mydata);
				GLOBAL.sendError(msg,dataStruct.res);
			}else{
				var desc = {
					err:err,
					docs:docs,
					query:query
				};
				var msg = {
					status:"Error",
					message:"[genericchatSpecificUser] Error Accessing DB",
					description: desc
				};
				GLOBAL.sendError(msg,dataStruct.res);
			}
		});
	}catch(err){
		var msg = {
			status:"Error",
			message:"[notificationsUnread] Error Processing Data",
			description: err
		};
		GLOBAL.sendError(msg,dataStruct.res);
	}
}

var allUnreadChats = function(dataStruct)
{
	try{
		///:token/:user_id/get/unread/count/generic/chat/:target_user_id
		var query = '{"$and":[{"server_generated_read_tags.read'+parseInt(dataStruct.user_id)+'": false}]}';
		query = eval("("+query+")");
		dataStruct['query']=[];
		dataStruct.query=query;
		genericChatCounterQuery(dataStruct);
	}catch(err){
		var msg = {
			status:"Error",
			message:"[allUnreadChats] Error Processing Data",
			description: err
		};
		GLOBAL.sendError(msg,dataStruct.res);
	}
}

module.exports = {
	genericchatSpecificUser:genericchatSpecificUser,
	productchatSpecificUser:productchatSpecificUser,
	orderchatSpecificUser:orderchatSpecificUser,
	orderchatSpecificOrderIDOnly:orderchatSpecificOrderIDOnly,
	notificationsUnread:notificationsUnread,
	allUnreadChats:allUnreadChats
}