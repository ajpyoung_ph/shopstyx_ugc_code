var getProductViews = function(dataStruct)
{
	try{
		///:token/:user_id/get/endorser/sharing/split/product/:product_id/:store_id/:collection_id/:variant_id/:product_owner_user_id/:product_owner_shop_id/
		var res = dataStruct.res;
		var req = dataStruct.req;
		//<product_id>/<product_owner_user_id>/<product_owner_shop_id>/
		var product_id = req.params.product_id;
		var store_user_id = req.params.store_user_id;
		var store_id = req.params.store_id;

		var query = {
			"from_info.user_id":parseInt(dataStruct.user_id),
			"extra_information.product_info.product_id":parseInt(product_id),
			"extra_information.product_info.store_user_id":parseInt(store_user_id),
			"extra_information.product_info.store_id":parseInt(store_id)
		};
		// 	"extra_information.product_info.variant_id":parseInt(req.params.variant_id)
		// };
		//console.log(query);
		GLOBAL.viewedAdvertisedProducts.find(query).sort({"date":-1}).limit(1,function(err,docs){
			if(err==null)
			{
				var reply = {
					status:"Success",
					message:"",
					data:[]
				};
				if(docs.length > 0)
				{
					reply.message = "Data found";
					reply.data=docs;
				}else{
					reply.message = "NO Data found";
				}
				GLOBAL.sendError(reply,dataStruct.res);
			}else{
				var msg = {
					status:"Error",
					message:"[getProductViews] Error Processing Data",
					description: err
				};
				GLOBAL.sendError(msg,dataStruct.res);
			}
		});
	}catch(err){
		var msg = {
			status:"Error",
			message:"[getProductViews] Error Processing Data",
			description: err
		};
		GLOBAL.sendError(msg,dataStruct.res);
	}
}

module.exports = {
	getProductViews:getProductViews
}