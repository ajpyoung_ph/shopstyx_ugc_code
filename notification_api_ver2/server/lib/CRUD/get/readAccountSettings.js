var util = require('util');


var readEmailBlock = function(dataStruct)
{
	///:token/:user_id/query/emailblock/:notification_type/
	var res = dataStruct.res;
	var req = dataStruct.req;
	var notification_type = req.params.notification_type;
	notification_type=notification_type.toUpperCase();

	if(notification_type.toUpperCase()=="ALL")
	{
		readALLEmailBlock(dataStruct);
	}else{
		query = {
			"$and":[
				{"user_id":dataStruct.user_id},
				{"notificationBlock":notification_type}
			]
		};

		GLOBAL.accountSetting.findOne(query,function(err,doc){
			if(err==null)
			{
				if(doc!=null)
				{
					/*
						{
						    "status": "Success",
						    "message": <short success message>,
						    "data": "blocked"
						}
					*/
					var msg = {
						status:"Success",
						message:"[readEmailBlock] Account Setting Found",
						data : "blocked"
					};
					GLOBAL.sendError(msg,dataStruct.res);
				}else{
					var msg = {
						status:"Success",
						message:"[readEmailBlock] Account Setting Found",
						data : "not blocked"
					};
					GLOBAL.sendError(msg,dataStruct.res);
				}
			}else{
				msg = {
					status:"Error",
					message:"[saveEmailBlock] Error Processing Data",
					code:1,
					description: err
				};
				GLOBAL.sendError(msg,dataStruct.res);
			}
		});
	}	
};

function readALLEmailBlock(dataStruct)
{
	///:token/:user_id/query/emailblock/:notification_type/
	var res = dataStruct.res;
	var req = dataStruct.req;
	var notification_type = req.params.notification_type;
	notification_type=notification_type.toUpperCase();

	query = {
		"user_id":dataStruct.user_id
	};

	GLOBAL.accountSetting.findOne(query,function(err,doc){
		if(err==null)
		{
			if(doc!=null)
			{
				console.log("whole doc:");
				console.log(util.inspect(doc,{showHidden: false, depth: null}));
				console.log("notificationBlock READ:");
				console.log(doc.notificationBlock);
				var msg = {
					status:"Success",
					message:"[readEmailBlock] Account Setting Found",
					data : doc.notificationBlock.slice()
				};
			}else{
				var msg = {
					status:"Success",
					message:"[readEmailBlock] Account Setting Found",
					data : []
				};
			}
			GLOBAL.sendError(msg,dataStruct.res);
		}else{
			msg = {
				status:"Error",
				message:"[readALLEmailBlock] Error Processing Data",
				code:1,
				description: err
			};
			GLOBAL.sendError(msg,dataStruct.res);
		}
	});
}
module.exports = {
	readEmailBlock:readEmailBlock
};