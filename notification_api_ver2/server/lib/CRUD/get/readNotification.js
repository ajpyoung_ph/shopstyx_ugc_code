var events = require('events');
var emitter = new events.EventEmitter();
var idGen = require('../../generateLatest/idGenerator');
var mongojs = require('mongojs');
var tabName = require('../../helpers/chatTableNameCreator');
var util = require('util');
var jsonStuff = require('../../jsonModifiers/jsonLib');

GLOBAL.collectorHolder = [];
GLOBAL.chatHolder=[];

GLOBAL.FollowerListVerbs = ["add-product-internal","add-product-external","added-new-collection","opened-new-store","claimed-store","removed-product","removed-collection","removed-store","modified-product","modified-collection","modified-store"];
GLOBAL.FollowedByUserVariable = ["follow-user","follow-shop","follow-collection"];
GLOBAL.OrderedItemsVariable = ["ordered-item","shipped-item","cancelled-order","cancelled-item","order-completed","order-payment-failed","order-payment-success","rewards-received"];

var pmx = require('pmx');
var probe = pmx.probe();
 
var globalArrayCounter = probe.counter({
  name : 'ArraysLive'
});

var CollectionComplete = function(doc,max_types,dataStruct,session_id){
	var res = dataStruct.res;
	var req = dataStruct.req;
	try{
		//console.log('CollectionComplete called');
		if(typeof(GLOBAL.collectorHolder[session_id])=='undefined')
		{
			GLOBAL.collectorHolder[session_id]=[];	
			setTimeout(idGen.deleteMe, 10000,GLOBAL.collectorHolder,session_id);
			//globalArrayCounter.inc();
		}
		GLOBAL.collectorHolder[session_id].push(doc);
		//console.log('CollectionComplete '+GLOBAL.collectorHolder[session_id].length+' ::: '+max_types);
		if(GLOBAL.collectorHolder[session_id].length == max_types)
		{
			var holder = [];
			GLOBAL.collectorHolder[session_id].forEach(function(docs){
				docs.forEach(function(doc_sub){
					holder.push(doc_sub);
				});
			});
			holder.sort(jsonStuff.sortby("date"));
			
			var msg = {
				status:"Success",
				message: "Returning requested data",
				data:holder
			};
			GLOBAL.sendError(msg,res);
			// setTimeout(idGen.deleteMe, 5000,GLOBAL.collectorHolder,session_id);
			// setTimeout(idGen.deleteMe, 5000,GLOBAL.chatHolder,session_id);
			try{
				delete GLOBAL.collectorHolder[session_id];
				//globalArrayCounter.dec();
				delete GLOBAL.chatHolder[session_id];
				//globalArrayCounter.dec();
			}catch(err){
				//do nothing here
			}
			
		}
		if(typeof(GLOBAL.collectorHolder[session_id])!='undefined')
		{
			if(GLOBAL.collectorHolder[session_id].length > max_types)
			{
				try{
					// setTimeout(idGen.deleteMe, 5000,GLOBAL.collectorHolder,session_id);
					// setTimeout(idGen.deleteMe, 5000,GLOBAL.chatHolder,session_id);
					delete GLOBAL.collectorHolder[session_id];
					//globalArrayCounter.dec();
					delete GLOBAL.chatHolder[session_id];
					//globalArrayCounter.dec();
				}catch(err){
					console.log(err);
				}
			}
		}
	}catch(err){
		msg = {
			status:"Error",
			message:"[CollectionComplete] Error Processing Data",
			description: err
		};
		GLOBAL.sendError(msg,dataStruct.res);
	}
}

var genericReply = function(docs,dataStruct){
	var res = dataStruct.res;
	var req = dataStruct.req;
	try{
		docs.sort(jsonStuff.sortby("date"));
		var msg = {
			status:"Success",
			message: "Returning requested data/No new Data to show",
			data:docs
		};
		GLOBAL.sendError(msg,res);
	}catch(err){
		msg = {
			status:"Error",
			message:"[genericReply] Error Processing Data",
			description: err
		};
		setImmediate(GLOBAL.sendEmail,msg);
	}
}

emitter.on('chatNotificationCollector',function(doc,max_items,chatTable,session_id,dataStruct){
	var res = dataStruct.res;
	var req = dataStruct.req;
	try{
		docs.sort(jsonStuff.sortby("date"));
		var msg = {
			status:"Success",
			message: "Returning requested data",
			data:docs
		};
		GLOBAL.sendError(msg,res);
	}catch(err){
		msg = {
			status:"Error",
			message:"[genericReply] Error Processing Data",
			description: err
		};
		setImmediate(GLOBAL.sendEmail,msg);
	}
});

var readALLNotificationsNOCHECK = function(dataStruct)
{
	try{
		var max_types = 4;//number of setImmediates execution; needs to increase if you increase the number of setImediate executions
		var session_id = idGen.latest_shopstyx_order_id(dataStruct);
		setImmediate(ver2_READALLNOTIFICATION,dataStruct);
		// setImmediate(getNotification,dataStruct,CollectionComplete,session_id,max_types);
		// setImmediate(getFollowedByUser,dataStruct,CollectionComplete,session_id,max_types);
		// setImmediate(getOrderedItems,dataStruct,CollectionComplete,session_id,max_types);
		// setImmediate(getFollowersListVerbs,dataStruct,CollectionComplete,session_id,max_types);
		//setImmediate(getShippedItems,dataStruct,CollectionComplete,session_id,max_types);
		// setImmediate(getGenericChat,dataStruct,CollectionComplete,'ChatIndex',session_id,max_types);
		// setImmediate(getGenericChat,dataStruct,CollectionComplete,'OrderChatIndex',session_id,max_types);
		// setImmediate(getGenericChat,dataStruct,CollectionComplete,'ProductChatIndex',session_id,max_types);
	}catch(err){
		msg = {
			status:"Error",
			message:"[readALLNotificationsNOCHECK] Error Processing Data",
			description: err
		};
		setImmediate(GLOBAL.sendEmail,msg);
	}
}
var readALLNotificationsAFTERIDNOCHECK = function(dataStruct)
{
		try{
		var max_types = 4;//number of setImmediates execution; needs to increase if you increase the number of setImediate executions
		var session_id = idGen.latest_shopstyx_order_id(dataStruct);
		var req = dataStruct.req;
		var notificationCollection = GLOBAL.notificationCollection;
		var query = {
			"_id":mongojs.ObjectId((req.params._id).toString())
		};
		notificationCollection.findOne(query,function(err,doc){
			if(err==null)
			{
				if(doc!=null)
				{
					dataStruct['date_start']=[];
					dataStruct.date_start=doc.date;
					//console.log("date: "+doc.date);
					setImmediate(ver2_READALLNOTIFICATION,dataStruct);
					// setImmediate(getNotification,dataStruct,CollectionComplete,session_id,max_types);
					// setImmediate(getFollowedByUser,dataStruct,CollectionComplete,session_id,max_types);
					// setImmediate(getOrderedItems,dataStruct,CollectionComplete,session_id,max_types);
					// setImmediate(getFollowersListVerbs,dataStruct,CollectionComplete,session_id,max_types);
					//setImmediate(getShippedItems,dataStruct,CollectionComplete,session_id,max_types);
					// setImmediate(getGenericChat,dataStruct,CollectionComplete,'ChatIndex',session_id,max_types);
					// setImmediate(getGenericChat,dataStruct,CollectionComplete,'OrderChatIndex',session_id,max_types);
					// setImmediate(getGenericChat,dataStruct,CollectionComplete,'ProductChatIndex',session_id,max_types);
				}else{
					var desc = {
						err:err,
						doc:doc
					};
					msg = {
						status:"Success",
						message:"[readALLNotificationsAFTERIDNOCHECK] No Record Found",
						description: desc
					};
					console.log(util.inspect(msg,{showHidden: false, depth: null}));
					//setImmediate(GLOBAL.sendEmail,msg);
				}
			}else{
				var desc = {
					err:err,
					doc:doc
				};
				msg = {
					status:"Error",
					message:"[readALLNotificationsAFTERIDNOCHECK] Error Finding Data",
					description: desc
				};
				setImmediate(GLOBAL.sendEmail,msg);
			}
		});
	}catch(err){
		msg = {
			status:"Error",
			message:"[readALLNotificationsAFTERIDNOCHECK] Error Processing Data",
			description: err
		};
		setImmediate(GLOBAL.sendEmail,msg);
	}
}
var readALLNotificationsWITHCHECK = function(dataStruct)
{
	try{
		checkIfTheresNewData(dataStruct,readALLNotificationsNOCHECK);
	}catch(err){
		msg = {
			status:"Error",
			message:"[readALLNotificationsWITHCHECK] Error Processing Data",
			description: err
		};
		setImmediate(GLOBAL.sendEmail,msg);
	}
}
var readALLNotificationsAFTERIDWITHCHECK = function(dataStruct)
{
	try{
		checkIfTheresNewData(dataStruct,readALLNotificationsAFTERIDNOCHECK);
	}catch(err){
		msg = {
			status:"Error",
			message:"[readALLNotificationsAFTERIDWITHCHECK] Error Processing Data",
			description: err
		};
		setImmediate(GLOBAL.sendEmail,msg);
	}
}
function getNotification(dataStruct,callback,session_id,max_types)
{
	try{
		var res = dataStruct.res;
		var req = dataStruct.req;
		//:number_of_records/:page_number
		var number_of_records = parseInt(req.params.number_of_records);
		var page_number = parseInt(req.params.page_number);
		var source_user_id = parseInt(req.params.user_id);
		var skip_value = number_of_records * page_number;

		var notificationCollection = GLOBAL.notificationCollection;
		var verbskip = [];
		GLOBAL.OrderedItemsVariable.forEach(function(verb){
			verbskip.push(verb);
		});
		GLOBAL.FollowedByUserVariable.forEach(function(verb){
			verbskip.push(verb);
		});
		GLOBAL.FollowerListVerbs.forEach(function(verb){
			verbskip.push(verb);
		});
		if(typeof(dataStruct.date_start)=='undefined')
		{
			
			var query = {
				$and:[{
					$or:[
						{"from_info.user_id":parseInt(source_user_id)},
						{"to_info.user_id":parseInt(source_user_id)},
						{"server_generated_information.follower_list.user_id":parseInt(source_user_id)}
					]},{
						$and:[
							{"type":{$exists:true}},
							{"type":"immediate"}
						]
					},{
						"verb":{$nin:verbskip}
					}
				]
			};
		}else{
			var query = {
				$and:[{
					$or:[
						{"from_info.user_id":parseInt(source_user_id)},
						{"to_info.user_id":parseInt(source_user_id)},
						{"server_generated_information.follower_list.user_id":parseInt(source_user_id)}
					]},{
						$and:[
							{"type":{$exists:true}},
							{"type":"immediate"}
						]
					},{
						"verb":{$nin:verbskip}
					},{
						"date":{$lt:dataStruct.date_start}
					}
				]
			};
		}
		//console.log(util.inspect(query,{showHidden: false, depth: null}));
		notificationCollection.find(query).sort({"date":-1}).limit(number_of_records).skip(skip_value,function(err,docs){
			if(err==null)
			{
				setImmediate(callback,docs,max_types,dataStruct,session_id);
			}else{
				var desc = {
					err:err,
					docs:docs,
					query:query
				};
				docs = [];
				setImmediate(callback,docs,max_types,dataStruct,session_id);
				var msg = {
					status:"Error",
					message:"[getNotification] Error Processing Data",
					description: desc
				};
				setImmediate(GLOBAL.sendEmail,msg);
			}
		});
	}catch(err){
		var msg = {
			status:"Error",
			message:"[getNotification] Error Processing Data",
			description: err
		};
		setImmediate(GLOBAL.sendEmail,msg);
	}
}

var getFollowedByUser = function(dataStruct,callback,session_id,max_types)
{
	try{
		var res = dataStruct.res;
		var req = dataStruct.req;
		//:number_of_records/:page_number
		var number_of_records = parseInt(req.params.number_of_records);
		var page_number = parseInt(req.params.page_number);
		var source_user_id = parseInt(req.params.user_id);
		var skip_value = number_of_records * page_number;

		var notificationCollection = GLOBAL.notificationCollection;
		if(typeof(dataStruct.date_start)=='undefined')
		{
			var query = {
				$and:[{
						"extra_information.user_info.user_id":source_user_id
					},{
						$and:[
							{"type":{$exists:true}},
							{"type":"immediate"}
						]
					},{
						"verb":{"$in":GLOBAL.FollowedByUserVariable}
					}
				]
			};
		}else{
			var query = {
				$and:[{
						"extra_information.user_info.user_id":source_user_id
					},{
						$and:[
							{"type":{$exists:true}},
							{"type":"immediate"}
						]
					},{
						"verb":{"$in":GLOBAL.FollowedByUserVariable}
					},{
						"date":{$lt:dataStruct.date_start}
					}
				]
			};
		}
		//console.log(util.inspect(query,{showHidden: false, depth: null}));
		notificationCollection.find(query).sort({"date":-1}).limit(number_of_records).skip(skip_value,function(err,docs){
			if(err==null)
			{
				setImmediate(callback,docs,max_types,dataStruct,session_id);
			}else{
				var desc = {
					err:err,
					docs:docs,
					query:notification_query
				};
				docs = [];
				setImmediate(callback,docs,max_types,dataStruct,session_id);
				var msg = {
					status:"Error",
					message:"[getFollowedByUser] Error Processing Data",
					description: desc
				};
				setImmediate(GLOBAL.sendEmail,msg);
			}
		});
	}catch(err){
		var msg = {
			status:"Error",
			message:"[getFollowedByUser] Error Processing Data",
			description: err
		};
		setImmediate(GLOBAL.sendEmail,msg);
	}
}

var getOrderedItems = function(dataStruct,callback,session_id,max_types)
{
	try{
		var res = dataStruct.res;
		var req = dataStruct.req;
		//:number_of_records/:page_number
		var number_of_records = parseInt(req.params.number_of_records);
		var page_number = parseInt(req.params.page_number);
		var source_user_id = parseInt(req.params.user_id);
		var skip_value = number_of_records * page_number;

		var notificationCollection = GLOBAL.notificationCollection;
		if(typeof(dataStruct.date_start)=='undefined')
		{
			var query = {
				$and:[{
						"to_info.user_id":source_user_id
					},{
						$and:[
							{"type":{$exists:true}},
							{"type":"immediate"}
						]
					},{
						"verb":{"$in":GLOBAL.OrderedItemsVariable}
					}
				]
			};
		}else{
			var query = {
				$and:[{
						"to_info.user_id":source_user_id
					},{
						$and:[
							{"type":{$exists:true}},
							{"type":"immediate"}
						]
					},{
						"verb":{"$in":GLOBAL.OrderedItemsVariable}
					},{
						"date":{$lt:dataStruct.date_start}
					}
				]
			};
		}
		//console.log(util.inspect(query,{showHidden: false, depth: null}));
		notificationCollection.find(query).sort({"date":-1}).limit(number_of_records).skip(skip_value,function(err,docs){
			if(err==null)
			{
				setImmediate(callback,docs,max_types,dataStruct,session_id);
			}else{
				var desc = {
					err:err,
					docs:docs,
					query:notification_query
				};
				docs = [];
				setImmediate(callback,docs,max_types,dataStruct,session_id);
				var msg = {
					status:"Error",
					message:"[getFollowedByUser] Error Processing Data",
					description: desc
				};
				setImmediate(GLOBAL.sendEmail,msg);
			}
		});
	}catch(err){
		var msg = {
			status:"Error",
			message:"[getFollowedByUser] Error Processing Data",
			description: err
		};
		setImmediate(GLOBAL.sendEmail,msg);
	}
}

var getFollowersListVerbs = function(dataStruct,callback,session_id,max_types)
{
	try{
		var res = dataStruct.res;
		var req = dataStruct.req;
		//:number_of_records/:page_number
		var number_of_records = parseInt(req.params.number_of_records);
		var page_number = parseInt(req.params.page_number);
		var source_user_id = parseInt(req.params.user_id);
		var skip_value = number_of_records * page_number;

		var notificationCollection = GLOBAL.notificationCollection;
		if(typeof(dataStruct.date_start)=='undefined')
		{
			var query = {
				$and:[{
						"server_generated_information.follower_list.user_id":source_user_id
					},
					{
						$or:[
							{"server_generated_information.follower_list.read":false},
							{"server_generated_information.follower_list.read":{$exists:false}}
						]
					},
					{
						$and:[
							{"type":{$exists:true}},
							{"type":"immediate"}
						]
					},{
						"verb":{"$in":GLOBAL.FollowerListVerbs}
					}
				]
			};
		}else{
			var query = {
				$and:[{
						"server_generated_information.follower_list.user_id":source_user_id
					},
					{
						$or:[
							{"server_generated_information.follower_list.read":false},
							{"server_generated_information.follower_list.read":{$exists:false}}
						]
					},
					{
						$and:[
							{"type":{$exists:true}},
							{"type":"immediate"}
						]
					},{
						"verb":{"$in":GLOBAL.FollowerListVerbs}
					},{
						"date":{$lt:dataStruct.date_start}
					}
				]
			};
		}
		//console.log(util.inspect(query,{showHidden: false, depth: null}));
		notificationCollection.find(query).sort({"date":-1}).limit(number_of_records).skip(skip_value,function(err,docs){
			if(err==null)
			{
				setImmediate(callback,docs,max_types,dataStruct,session_id);
			}else{
				var desc = {
					err:err,
					docs:docs,
					query:notification_query
				};
				docs = [];
				setImmediate(callback,docs,max_types,dataStruct,session_id);
				var msg = {
					status:"Error",
					message:"[getFollowersListVerbs] Error Processing Data",
					description: desc
				};
				setImmediate(GLOBAL.sendEmail,msg);
			}
		});
	}catch(err){
		var msg = {
			status:"Error",
			message:"[getFollowersListVerbs] Error Processing Data",
			description: err
		};
		setImmediate(GLOBAL.sendEmail,msg);
	}
}

var getShippedItems = function(dataStruct,callback,session_id,max_types)
{
	try{
		var res = dataStruct.res;
		var req = dataStruct.req;
		//:number_of_records/:page_number
		var number_of_records = parseInt(req.params.number_of_records);
		var page_number = parseInt(req.params.page_number);
		var source_user_id = parseInt(req.params.user_id);
		var skip_value = number_of_records * page_number;

		var notificationCollection = GLOBAL.notificationCollection;
		//inform client
		if(typeof(dataStruct.date_start)=='undefined')
		{
			var query = {
				$and:[{
						"to_info.user_id":source_user_id
					},{
						$and:[
							{"type":{$exists:true}},
							{"type":"immediate"}
						]
					},{
						"verb":"shipped-item"
					}
				]
			};
		}else{
			var query = {
				$and:[{
						"to_info.user_id":source_user_id
					},{
						$and:[
							{"type":{$exists:true}},
							{"type":"immediate"}
						]
					},{
						"verb":"shipped-item"
					},{
						"date":{$gt:dataStruct.date_start}
					}
				]
			};
		}
		notificationCollection.find(query).sort({"date":-1}).limit(number_of_records).skip(skip_value,function(err,docs){
			if(err==null)
			{
				setImmediate(callback,docs,max_types,dataStruct,session_id);
			}else{
				var desc = {
					err:err,
					docs:docs,
					query:notification_query
				};
				docs = [];
				setImmediate(callback,docs,max_types,dataStruct,session_id);
				var msg = {
					status:"Error",
					message:"[getFollowedByUser] Error Processing Data",
					description: desc
				};
				setImmediate(GLOBAL.sendEmail,msg);
			}
		});
	}catch(err){
		var msg = {
			status:"Error",
			message:"[getFollowedByUser] Error Processing Data",
			description: err
		};
		setImmediate(GLOBAL.sendEmail,msg);
	}
}

var getGenericChat = function(dataStruct,callback,collectionName,session_id,max_types)
{
	try{
		var res = dataStruct.res;
		var req = dataStruct.req;
		//:number_of_records/:page_number
		var number_of_records = parseInt(req.params.number_of_records);
		var page_number = parseInt(req.params.page_number);
		var source_user_id = parseInt(req.params.user_id);
		var skip_value = number_of_records * page_number;

		var chatIndex = GLOBAL.mongodb.collection(collectionName);
		if(typeof(dataStruct.date_start)=='undefined')
		{
			var query = {
				$and:[{"users.user_id":parseInt(source_user_id)}]
			};
		}else{
			var query = {
				$and:[
					{"users.user_id":parseInt(source_user_id)},
					{"date":{$gt:dataStruct.date_start}}
				]
			};
		}

		chatIndex.find(query).sort({"date":-1}).limit(number_of_records).skip(skip_value,function(err,docs){
			if(err==null)
			{
				if(docs.length>0)
				{
					//chatTableCollector=[];
					var totalchats = docs.length;
					docs.forEach(function(singleDoc){
						switch(collectionName)
						{
							case 'OrderChatIndex':
								var tableArr = tabName.orderChatTable(singleDoc.users[0].user_id,singleDoc.users[1].user_id,singleDoc.order_id);
								break;
							case 'ProductChatIndex':
								var collection_id = 0;
								if(typeof(singleDoc.collection_id)!='undefined')
								{
									collection_id=parseInt(singleDoc.collection_id);
								}
								//prodChatTable = function(id1,id2,prodID,storeID,collectionID,variantID)
								var tableArr = tabName.prodChatTable(singleDoc.users[0].user_id,singleDoc.users[1].user_id,singleDoc.product_id,singleDoc.store_id,collection_id,singleDoc.variant_id);
								break;
							case 'ChatIndex':
								var tableArr = tabName.genChatTable(singleDoc.users[0].user_id,singleDoc.users[1].user_id);
								break;
						}
						var chatTable1 = tableArr[0];
						var chatTable2 = tableArr[1];
						var target_id = parseInt(singleDoc.users[0].user_id);
						if(target_id==source_user_id)
						{
							target_id = parseInt(singleDoc.users[1].user_id);
						}
						GLOBAL.mongodb.getCollectionNames(function(err,retdocs){
							if(err==null)
							{
								var found=false;
								if(retdocs.indexOf(chatTable1)!=-1)
								{
									var genericChat = GLOBAL.mongodb.collection(chatTable1);
									found=true;
									//console.log("chatTable = "+chatTable1);
								}
								if(retdocs.indexOf(chatTable2)!=-1)
								{
									var genericChat = GLOBAL.mongodb.collection(chatTable2);	
									found=true;
									//console.log("chatTable = "+chatTable2);
								}
								if(found==false)
								{
									var genericChat = GLOBAL.mongodb.collection(chatTable1);
									//console.log("chatTable = "+chatTable1);
								}
								//chatTableCollector.push(genericChat);
								setImmediate(readLatestChatRecord,genericChat,totalchats,parseInt(source_user_id),session_id,dataStruct,max_types,callback,collectionName,target_id);
							}else{
								var desc = {
									source:"[getGenericChat] Error pulling Collection Names",
									err:err,
									docs:retdocs
								};
								//setImmediate(readLatestChatRecord,chatTable1,docs.length,parseInt(source_user_id),session_id);
								var msg = {
									status:"Error",
									message:"[getGenericChat] Error Processing Data",
									description: desc
								};
								setImmediate(GLOBAL.sendEmail,msg);
							}
						});
					});
					
				}else{
					setImmediate(callback,docs,max_types,dataStruct,session_id);
				}
			}else{
				var desc = {
					err:err,
					docs:docs,
					query:query,
					target:collectionName
				};
				docs = [];
				setImmediate(callback,docs,max_types,dataStruct,session_id);
				var msg = {
					status:"Error",
					message:"[getGenericChat] Error Processing Data",
					description: desc
				};
				setImmediate(GLOBAL.sendEmail,msg);
			}
		});
	}catch(err){
		var msg = {
			status:"Error",
			message:"[getGenericChat] Error Processing Data",
			description: err
		};
		setImmediate(GLOBAL.sendEmail,msg);
	}
}

var checkIfTheresNewData = function(dataStruct,callback)
{
	try{
		var res = dataStruct.res;
		var req = dataStruct.req;
		
		var source_user_id = parseInt(req.params.user_id);
		
		var trueIndexCollection = GLOBAL.trueIndexCollection;
		var query = {
			"user_id":parseInt(source_user_id),
			"read":false
		};
		trueIndexCollection.findOne(query,function(err,doc){
			if(err==null)
			{
				if(doc!=null)
				{
					callback(dataStruct);
				}else{
					var docs = [];
					setImmediate(genericReply,docs,dataStruct);
				}
			}else{
				var msg = {
					status:"Error",
					message:"[checkIfTheresNewData] Error Processing Data",
					description: err
				};
			}
		});
	}catch(err){
		var msg = {
			status:"Error",
			message:"[checkIfTheresNewData] Error Processing Data",
			description: err
		};
		setImmediate(GLOBAL.sendEmail,msg);
	}
}

function readLatestChatRecord(collectionName,max_items,source_user_id,session_id,dataStruct,max_types,callback,chatTypeIndexCollectionName,target_id)
{
	try{
		// var targetCollection = collectionName.toString();
		// console.log('targetCollection = '+targetCollection);
		//var chatCollection = GLOBAL.mongodb.collection(targetCollection);
		if(typeof(dataStruct.date_start)=='undefined')
		{
			var query = {
				$or:[
						{
							"from_info.user_id":parseInt(source_user_id)
						},
						{
							"to_info.user_id":parseInt(source_user_id)	
						}
					]
			};
		}else{
			var query = {
				$and:[
					{
						$or:[
							{
								"from_info.user_id":parseInt(source_user_id)
							},
							{
								"to_info.user_id":parseInt(source_user_id)	
							}
						]
					},{
						"date":{$gt:dataStruct.date_start}
					}
				]
			};
		}
		
		if(typeof(GLOBAL.chatHolder[session_id]) == 'undefined')
		{
			GLOBAL.chatHolder[session_id]=[];

			setTimeout(idGen.deleteMe, 10000,GLOBAL.chatHolder,session_id);
			//globalArrayCounter.inc();
		}
		if(typeof(GLOBAL.chatHolder[session_id]['counter'])=='undefined')
		{
			GLOBAL.chatHolder[session_id]['counter']=[];
			GLOBAL.chatHolder[session_id]['counter']=0;	
		}
		if(typeof(GLOBAL.chatHolder[session_id][chatTypeIndexCollectionName]) == 'undefined')
		{
			GLOBAL.chatHolder[session_id][chatTypeIndexCollectionName]=[];
		}
		//console.log("query");
		//console.log(query);
		collectionName.find(query).sort({"date":-1}).limit(1,function(err,doc){
			if(err==null)
			{
				var docs=[];
				if(doc.length>0)
				{
					doc[0]['talking_to']=[];
					doc[0]['talking_to']=target_id;
					GLOBAL.chatHolder[session_id][chatTypeIndexCollectionName].push(doc[0]);
					GLOBAL.chatHolder[session_id]['counter']=GLOBAL.chatHolder[session_id]['counter']+1;
					//console.log(GLOBAL.chatHolder[session_id]['counter']+" :: "+max_items)
					if(GLOBAL.chatHolder[session_id]['counter'] >= max_items)
					{
						var docs = GLOBAL.chatHolder[session_id][chatTypeIndexCollectionName];
						setImmediate(callback,docs,max_types,dataStruct,session_id);
					}
				}else{
					GLOBAL.chatHolder[session_id]['counter']=GLOBAL.chatHolder[session_id]['counter']+1;
					if(GLOBAL.chatHolder[session_id]['counter'] >= max_items)
					{
						if(typeof(GLOBAL.chatHolder[session_id][chatTypeIndexCollectionName]) == 'undefined')
						{
							GLOBAL.chatHolder[session_id][chatTypeIndexCollectionName]=[];
						}
						var docs = GLOBAL.chatHolder[session_id][chatTypeIndexCollectionName];
						setImmediate(callback,docs,max_types,dataStruct,session_id);
					}
					var desc = {
						err:err,
						doc:doc,
						query:query
					};
					var msg = {
						status:"Success",
						message:"[readLatestChatRecord] No Document Found",
						description: desc
					};
					//console.log("doc");
					//console.log(doc);
					//console.log("query");
					//console.log(query);
					//setImmediate(GLOBAL.sendEmail,msg);
				}
			}
		});
	}catch(err){
		var msg = {
			status:"Error",
			message:"[readLatestChatRecord] Error Processing Data",
			description: err
		};
		setImmediate(GLOBAL.sendEmail,msg);
	}
	
}
function ver2_READALLNOTIFICATION(dataStruct)
{
	try{
		//console.log('reading ver2');
		var res = dataStruct.res;
		var req = dataStruct.req;
		//:number_of_records/:page_number
		var number_of_records = parseInt(req.params.number_of_records);
		var page_number = parseInt(req.params.page_number);
		try{
			var source_user_id = parseInt(req.params.user_id);
		}catch(err){
			console.log(err);
			var source_user_id = parseInt(dataStruct.user_id);
		}
		
		var skip_value = number_of_records * page_number;

		var verbskip = [];
		GLOBAL.OrderedItemsVariable.forEach(function(verb){
			verbskip.push(verb);
		});
		GLOBAL.FollowedByUserVariable.forEach(function(verb){
			verbskip.push(verb);
		});
		GLOBAL.FollowerListVerbs.forEach(function(verb){
			verbskip.push(verb);
		});

		if(typeof(dataStruct.date_start)=='undefined')
		{
			
			var query = {
				$or:
				[
					{$and:[{
						$or:[
							{"from_info.user_id":parseInt(source_user_id)},
							{"to_info.user_id":parseInt(source_user_id)},
							{"server_generated_information.follower_list.user_id":parseInt(source_user_id)}
						]},{
							$and:[
								{"type":{$exists:true}},
								{"type":"immediate"}
							]
						},{
							"verb":{$nin:verbskip}
						}
					]},
					{$and:[{
						$or:[
							{"from_info.user_id":parseInt(source_user_id)},
							{"to_info.user_id":parseInt(source_user_id)},
							{"server_generated_information.follower_list.user_id":parseInt(source_user_id)}
						]},{
							$and:[
								{"type":{$exists:true}},
								{"type":"immediate"}
							]
						},{
							"extra_information.order_info.is_robot":1
						}
					]},
					{$and:[{
							"extra_information.user_info.user_id":source_user_id
						},{
							$and:[
								{"type":{$exists:true}},
								{"type":"immediate"}
							]
						},{
							"verb":{"$in":GLOBAL.FollowedByUserVariable}
						}
					]},
					{$and:[{
							"to_info.user_id":source_user_id
						},{
							$and:[
								{"type":{$exists:true}},
								{"type":"immediate"}
							]
						},{
							"verb":{"$in":GLOBAL.OrderedItemsVariable}
						}
					]},
					{$and:[{
							"server_generated_information.follower_list.user_id":source_user_id
						},
						// {
						// 	$or:[
						// 		{"server_generated_information.follower_list.read":false},
						// 		{"server_generated_information.follower_list.read":{$exists:false}}
						// 	]
						// },
						{
							$and:[
								{"type":{$exists:true}},
								{"type":"immediate"}
							]
						},{
							"verb":{"$in":GLOBAL.FollowerListVerbs}
						}
					]}
				]
			};
		}else{
			var query = {
				$or:
				[
					{$and:[{
						$or:[
							{"from_info.user_id":parseInt(source_user_id)},
							{"to_info.user_id":parseInt(source_user_id)},
							{"server_generated_information.follower_list.user_id":parseInt(source_user_id)}
						]},{
							$and:[
								{"type":{$exists:true}},
								{"type":"immediate"}
							]
						},{
							"verb":{$nin:verbskip}
						},{
							"date":{$gt:dataStruct.date_start}
						}
					]},
					{$and:[{
						$or:[
							{"from_info.user_id":parseInt(source_user_id)},
							{"to_info.user_id":parseInt(source_user_id)},
							{"server_generated_information.follower_list.user_id":parseInt(source_user_id)}
						]},{
							$and:[
								{"type":{$exists:true}},
								{"type":"immediate"}
							]
						},{
							"extra_information.order_info.is_robot":1
						},{
							"date":{$gt:dataStruct.date_start}
						}
					]},
					{$and:[{
							"extra_information.user_info.user_id":source_user_id
						},{
							$and:[
								{"type":{$exists:true}},
								{"type":"immediate"}
							]
						},{
							"verb":{"$in":GLOBAL.FollowedByUserVariable}
						},{
							"date":{$gt:dataStruct.date_start}
						}
					]},
					{$and:[{
							"to_info.user_id":source_user_id
						},{
							$and:[
								{"type":{$exists:true}},
								{"type":"immediate"}
							]
						},{
							"verb":{"$in":GLOBAL.OrderedItemsVariable}
						},{
							"date":{$gt:dataStruct.date_start}
						}
					]},
					{$and:[{
							"server_generated_information.follower_list.user_id":source_user_id
						},
						{
							$or:[
								{"server_generated_information.follower_list.read":false},
								{"server_generated_information.follower_list.read":{$exists:false}}
							]
						},
						{
							$and:[
								{"type":{$exists:true}},
								{"type":"immediate"}
							]
						},{
							"verb":{"$in":GLOBAL.FollowerListVerbs}
						},{
							"date":{$gt:dataStruct.date_start}
						}
					]}
				]
			};
		}

		notificationCollection.find(query).sort({"date":-1}).limit(number_of_records).skip(skip_value,function(err,docs){
			if(err==null)
			{
				docs.sort(jsonStuff.sortby("date"));
				var msg = {
					status:"Success",
					message: "Returning requested data",
					data:docs,
					query:query
				};
				GLOBAL.sendError(msg,res);
			}else{
				var desc = {
					err:err,
					docs:docs,
					query:query
				};
				//docs = [];
				//setImmediate(callback,docs,max_types,dataStruct,session_id);
				var msg = {
					status:"Error",
					message:"[ver2_READALLNOTIFICATION] Error Finding Data",
					description: desc
				};
				GLOBAL.sendError(msg,res);
			}
		});

	}catch(err){
		var msg = {
			status:"Error",
			message:"[ver2_READALLNOTIFICATION] Error Processing Data",
			description: err
		};
		GLOBAL.sendError(msg,dataStruct.res);
	}
}
module.exports = {
	readALLNotificationsNOCHECK:readALLNotificationsNOCHECK,
	readALLNotificationsWITHCHECK:readALLNotificationsWITHCHECK,
	checkIfTheresNewData:checkIfTheresNewData,
	getGenericChat:getGenericChat,
	CollectionComplete:CollectionComplete,
	genericReply:genericReply,
	readALLNotificationsAFTERIDWITHCHECK:readALLNotificationsAFTERIDWITHCHECK,
	readALLNotificationsAFTERIDNOCHECK:readALLNotificationsAFTERIDNOCHECK
}