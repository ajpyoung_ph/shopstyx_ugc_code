var idGen = require('../../generateLatest/idGenerator');
var pmx = require('pmx');
var probe = pmx.probe();
 
var globalArrayCounter = probe.counter({
  name : 'ArraysLive'
});

GLOBAL.peepTracker = [];
GLOBAL.peepCounter = [];
var chatCollector = function(doc,maxTypes,dataStruct,sessionID)
{
	try{
		if(typeof(GLOBAL.peepTracker[sessionID])=='undefined')
		{
			GLOBAL.peepTracker[sessionID]=[];
			setTimeout(idGen.deleteMe, 10000,GLOBAL.peepTracker,sessionID);
			//globalArrayCounter.inc();
		}
		if(typeof(GLOBAL.peepCounter[sessionID])=='undefined')
		{
			GLOBAL.peepCounter[sessionID]=[];
			GLOBAL.peepCounter[sessionID]=0;
			
			setTimeout(idGen.deleteMe, 10000,GLOBAL.peepCounter,sessionID);
			//globalArrayCounter.inc();
		}
		GLOBAL.peepTracker[sessionID].push(doc);
		GLOBAL.peepCounter[sessionID] = GLOBAL.peepCounter[sessionID] + 1;
		if(GLOBAL.peepCounter[sessionID]==maxTypes)
		{
			//sort the data 
			GLOBAL.peepTracker[sessionID].sort(function(b,a){
			      if( a.chatdata.date > b.chatdata.date){
			          return 1;
			      }else if( a.chatdata.date < b.chatdata.date ){
			          return -1;
			      }
			      return 0;
			   });
			var msg = {
				status:"Success",
				message:"[chatCollector] List found",
				data: GLOBAL.peepTracker[sessionID]
			};
			// setTimeout(idGen.deleteMe, 5000,GLOBAL.peepTracker,sessionID);
			// setTimeout(idGen.deleteMe, 5000,GLOBAL.peepCounter,sessionID);
			try{
				delete GLOBAL.peepTracker[sessionID];
				//globalArrayCounter.dec();
				delete GLOBAL.peepCounter[sessionID];
				//globalArrayCounter.dec();
			}catch(err){
				//do nothing here
			}
			
			GLOBAL.sendError(msg,dataStruct.res);
		}
	}catch(err){
		var msg = {
			status:"Error",
			message:"[chatCollector] Error Processing Data",
			description: err
		};
		GLOBAL.sendError(msg,dataStruct.res);
	}
}
var peepCollector = function(doc,maxTypes,dataStruct,sessionID)
{
	try{
		if(doc!='')
		{
			// console.log(doc);
			// console.log("****************************");
			// console.log("requery");
			/*
				doc = {
						id:target_id, unread:docs
					}
			*/
			var query = {
				"$or":[
					{
						"$and":[
							{
								"from_info.user_id":parseInt(dataStruct.user_id)
							},
							{
								"to_info.user_id":parseInt(doc.id)
							}
						]
					},
					{
						"$and":[
							{
								"to_info.user_id":parseInt(dataStruct.user_id)
							},
							{
								"from_info.user_id":parseInt(doc.id)
							}
						]
					}
				]
			};
			GLOBAL.chatCollection.find(query).sort({"date":-1}).limit(1,function(err,docs){
				if(err==null)
				{
					// var mydoc = {
					// 	id:doc.id,
					// 	unread:doc.unread,
					// 	chatdata:docs[0]
					// };
					//console.log("chat:");
					//console.log(docs[0]);
					//console.log(mydoc);
					var mydoc=doc;
					mydoc['chatdata']=docs[0];
					chatCollector(mydoc,maxTypes,dataStruct,sessionID);
				}else{
					var mydoc = doc;
					chatCollector(doc,maxTypes,dataStruct,sessionID);
				}
			});
		}else{
			//console.log("no chat data found");
			var mydoc = doc;
			chatCollector(doc,maxTypes,dataStruct,sessionID);
		}
		
	}catch(err){
		var msg = {
			status:"Background Error",
			message:"[peepCollector] Error Processing Data",
			description: err
		};
		var mydoc = doc;
		chatCollector(mydoc,maxTypes,dataStruct,sessionID)
		GLOBAL.sendEmail(msg);
	}
}
var allPeeps = function(dataStruct)
{
	try{
		///:token/:user_id/get/list/people/chats/:number_of_records/:page_number
		var query = {
			"users.user_id":dataStruct.user_id
		};
		var limit = parseInt(dataStruct.req.params.number_of_records);
		var skip = parseInt(dataStruct.req.params.page_number) * limit;
		var sessionID = idGen.latest_shopstyx_order_id(dataStruct);

		GLOBAL.chatIndex.find(query,{"users":1}).sort({"date":-1}).skip(skip).limit(limit,function(err,docs){
			if(err==null)
			{
				if(docs.length>0)
				{
					var listOfIDs = [];
					var maxTypes = docs.length;
					docs.forEach(function(mylist){
						if(parseInt(mylist.users[0].user_id)!=dataStruct.user_id)
						{
							listOfIDs.push(parseInt(mylist.users[0].user_id));
						}else{
							listOfIDs.push(parseInt(mylist.users[1].user_id));
						}
					});
					processUnread(listOfIDs,sessionID,maxTypes,dataStruct);
				}else{
					var msg = {
						status:"Success",
						message:"No more data found",
						data:[]
					};
					GLOBAL.sendError(msg,dataStruct.res);
				}
			}else{
				var desc = {
					err:err,
					docs:docs,
					query:query
				};
				var msg = {
					status:"Error",
					message:"[allPeeps] Error Accessing DB",
					description: desc
				};
				GLOBAL.sendError(msg,dataStruct.res);
			}
		});
	}catch(err){
		var msg = {
			status:"Error",
			message:"[allPeeps] Error Processing Data",
			description: err
		};
		GLOBAL.sendError(msg,dataStruct.res);
	}
}

function processUnread(listOfIDs,sessionID,maxTypes,dataStruct)
{
	listOfIDs.forEach(function(target_id){
		var query = '{"$and":[{"server_generated_read_tags.read'+parseInt(dataStruct.user_id)+'": false},{"server_generated_read_tags.read'+parseInt(target_id)+'": {"$exists": true}}]}';
		//console.log(query);
		query = eval("("+query+")");
		setImmediate(executeUnreadQuery,listOfIDs,sessionID,maxTypes,dataStruct,query,target_id);
	});
}

function executeUnreadQuery(listOfIDs,sessionID,maxTypes,dataStruct,query,target_id)
{
	GLOBAL.chatCollection.count(query,function(err,docs){
		if(err==null)
		{
			var mydata = {
				id:target_id, unread:docs
			};
			//console.log(mydata);
			//peepCollector(mydata,maxTypes,dataStruct,sessionID);
			readALLchatCount(mydata,maxTypes,dataStruct,sessionID,target_id);
		}else{
			var desc = {
				err:err,
				docs:docs,
				query:dataStruct.query
			};
			var msg = {
				status:"Background Error",
				message:"[processUnread] Error Accessing DB",
				description: desc
			};
			var mydata = '';
			//setImmediate(peepCollector,mydata,maxTypes,dataStruct,sessionID);
			readALLchatCount(mydata,maxTypes,dataStruct,sessionID,target_id);
			GLOBAL.sendEmail(msg);
		}
	});
}

function readALLchatCount(mydata,maxTypes,dataStruct,sessionID,target_id)
{
	var verbArray=["chat","product-chat","order-chat","store-chat"];
	var count =0;	
	setImmediate(collectChatTypeCount,mydata,maxTypes,dataStruct,sessionID,target_id,count,verbArray);
}
function collectChatTypeCount(mydata,maxTypes,dataStruct,sessionID,target_id,count,verbArray)
{
	if(count==4 || count > 4)
	{
		// console.log("==========================");
		// console.log(mydata);
		peepCollector(mydata,maxTypes,dataStruct,sessionID);
	}else{
		if(mydata=='')
		{
			mydata={
				id:target_id, 
				unread:0,
			}
		}
		var myVar=verbArray[count];
		switch(myVar)
		{
			case 'store-chat':
				var query = '[{"$match": {"$and": [{"verb": "store-chat"},{"server_generated_read_tags.read'+parseInt(dataStruct.user_id)+'": {"$exists": true}},{"server_generated_read_tags.read'+parseInt(target_id)+'": {"$exists": true}}]}},{"$group": {"_id": {"store_id": "$extra_information.store_info.store_id","store_user_id": "$extra_information.store_info.store_user_id","claimed_by": "$extra_information.store_info.claimed_by"},"count": {"$sum": 1}}}]';
				query = eval("("+query+")");
				GLOBAL.chatCollection.aggregate(query,function(err,docs){
					if(err==null)
					{
						mydata[myVar]=docs;
						count=count+1;
					}else{
						mydata[myVar]=[];
						count=count+1;
					}
					setImmediate(collectChatTypeCount,mydata,maxTypes,dataStruct,sessionID,target_id,count,verbArray);
				});
				break;
			case 'order-chat':
				var query = '[{"$match": {"$and": [{"verb": "order-chat"},{"server_generated_read_tags.read'+parseInt(dataStruct.user_id)+'": {"$exists": true}},{"server_generated_read_tags.read'+parseInt(target_id)+'": {"$exists": true}}]}},{"$group": {"_id": {"order_id": "$extra_information.order_info.order_id","customer_order_number": "$extra_information.order_info.customer_order_number","seller_transaction_number": "$extra_information.order_info.seller_transaction_number"},"count": {"$sum": 1}}}]';
				//console.log(query);
				query = eval("("+query+")");
				GLOBAL.chatCollection.aggregate(query,function(err,docs){
					if(err==null)
					{
						mydata[myVar]=docs;
						count=count+1;
					}else{
						mydata[myVar]=[];
						count=count+1;
					}
					setImmediate(collectChatTypeCount,mydata,maxTypes,dataStruct,sessionID,target_id,count,verbArray);
				});
				break;
			case 'product-chat':
				var query = '[{"$match": {"$and": [{"verb": "product-chat"},{"server_generated_read_tags.read'+parseInt(dataStruct.user_id)+'": {"$exists": true}},{"server_generated_read_tags.read'+parseInt(target_id)+'": {"$exists": true}}]}},{"$group": {"_id": {"store_user_id": "$extra_information.product_info.store_user_id","store_id": "$extra_information.product_info.store_id","product_id": "$extra_information.product_info.product_id"},"count": {"$sum": 1}}}]';
				query = eval("("+query+")");
				GLOBAL.chatCollection.aggregate(query,function(err,docs){
					if(err==null)
					{
						mydata[myVar]=docs;
						count=count+1;
					}else{
						mydata[myVar]=[];
						count=count+1;
					}
					setImmediate(collectChatTypeCount,mydata,maxTypes,dataStruct,sessionID,target_id,count,verbArray);
				});
				break;
			case 'chat':
			default:
				var query = '{"$and": [{"server_generated_read_tags.read'+parseInt(dataStruct.user_id)+'": {"$exists": true}},{"server_generated_read_tags.read'+parseInt(target_id)+'": {"$exists": true}},{"verb":"'+verbArray[count]+'"}]}';

				//console.log(query);
				query = eval("("+query+")");
				GLOBAL.chatCollection.count(query,function(err,docs){
					if(err==null)
					{
						
						mydata[myVar]=docs;
						count=count+1;
						
					}else{
						
						mydata[myVar]=0;
						count=count+1;
						
					}
					setImmediate(collectChatTypeCount,mydata,maxTypes,dataStruct,sessionID,target_id,count,verbArray);			
				});
				break;
		}
		
	}
}

module.exports = {
	allPeeps:allPeeps
}