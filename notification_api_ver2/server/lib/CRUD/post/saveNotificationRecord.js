var mySQLStuff = require('../../mysql_connects/mysql_convertions');
var data_integrity = require('../../background_checks/processData');
var verbHandler = require('../../eventHandler/verbHandler');
var notificationCollection = GLOBAL.notificationCollection;
var tabName = require('../../helpers/chatTableNameCreator');
var mongojs = require('mongojs');
var util = require('util');


var saveNotification = function(dataStruct)
{
	try{
		var res = dataStruct.res;
		var req = dataStruct.req;
		var dataJSON = req.body.data;

		if(typeof(dataJSON)=="string")
		{
			dataJSON = eval("("+dataJSON+")");
		}
		if(typeof(dataJSON.type)=='undefined')
		{
			dataJSON['type']=[];
			dataJSON.type='statistic';
		}
		switch((dataJSON.verb).toLowerCase())
		{
			case 'shared-advertisement-product':
			case 'shared-advertisement-collection':
			case 'viewed-advertisement-product':
			case 'viewed-advertisement-collection':
				saveAdvertisement(dataStruct);
				break;
			case 'order-chat':
			case 'product-chat':
			case 'chat':
			case 'store-chat':
				saveGenericChat(dataStruct);
				break;
			default:
				saveGenericNotification(dataStruct);
				break;
		}
		
	}catch(err){
		msg = {
			status:"Error",
			message:"[saveNotification] Error Processing Data",
			description: err
		};
		GLOBAL.sendError(msg,dataStruct.res);
	}
}
var saveAdvertisement = function(dataStruct)
{
	try{
		
		var msg = {
			status:"Success",
			message:"Advertisement Log Saved"
		};
		//msg.data.push(doc);
		GLOBAL.sendError(msg,dataStruct.res);

		var res = dataStruct.res;
		var req = dataStruct.req;
		var dataJSON = req.body.data;

		dataJSON = eval("("+dataJSON+")");
		
		dataJSON['date']=[];
		dataJSON['date']=dataStruct.newUnixDate;
		dataJSON['date_simple']=[];
		dataJSON['date_simple']=dataStruct.newSimpleDate;
		dataJSON['date_timezone']=[];
		dataJSON['date_timezone']=dataStruct.timezone;
		var advertCollection = '';

		switch((dataJSON.verb).toLowerCase())
		{
			case 'shared-advertisement-product':
			case 'shared-advertisement-collection':
				advertCollection = GLOBAL.sharedAdvertisement;
				break;
			case 'viewed-advertisement-product':
				advertCollection = GLOBAL.viewedAdvertisedProducts;
				break;
			case 'viewed-advertisement-collection':
				advertCollection = GLOBAL.viewedAdvertisedCollections;
		}

		// advertCollection.insert(dataJSON,function(err,doc){
		// 	if(err==null)
		// 	{
		// 		var msg = {
		// 			status:"Success",
		// 			message:"Advertisement Log Saved",
		// 			data : []
		// 		};
		// 		msg.data.push(doc);
		// 		GLOBAL.sendError(msg,dataStruct.res);
		// 	}else{
		// 		var desc = {
		// 			err:err,
		// 			doc:doc
		// 		};
		// 		var msg = {
		// 			status:"Error",
		// 			message:"[saveAdvertisement] Error Processing Data",
		// 			description: desc
		// 		};
		// 		GLOBAL.sendError(msg,dataStruct.res);
		// 	}
		// });
		
		var searchQuery = {
			"from_info.user_id":parseInt(dataJSON.from_info.user_id),
			"extra_information.product_info.product_id":parseInt(dataJSON.extra_information.product_info.product_id),
			"extra_information.product_info.store_user_id":parseInt(dataJSON.extra_information.product_info.store_user_id),
			"extra_information.product_info.store_id":parseInt(dataJSON.extra_information.product_info.store_id),
			"extra_information.product_info.endorser_id":parseInt(dataJSON.extra_information.product_info.endorser_id)
		};
		if(parseInt(dataJSON.from_info.user_id) == parseInt(dataJSON.extra_information.product_info.endorser_id) || parseInt(dataJSON.from_info.user_id) == parseInt(dataJSON.extra_information.product_info.store_user_id) || parseInt(dataJSON.extra_information.product_info.endorser_id)==0)
		{
			//do not update or save
			
		}else{
			//console.log(util.inspect(searchQuery,{showHidden: false, depth: null}));
			advertCollection.update(searchQuery,{"$set":dataJSON},{"upsert":true},function(err,docs){
				if(err==null)
				{
					// var msg = {
					// 	status:"Success",
					// 	message:"Advertisement Log Saved"
					// };
					// //msg.data.push(doc);
					// GLOBAL.sendError(msg,dataStruct.res);
				}else{
					var desc = {
						err:err,
						doc:doc
					};
					var msg = {
						status:"Error",
						message:"[saveAdvertisement] Error Processing Data",
						description: desc
					};
					GLOBAL.sendEmail(msg);//,dataStruct.res);
				}
			});
		}
		
	}catch(err){
		var msg = {
			status:"Error",
			message:"[saveAdvertisement] Error Processing Data",
			description: err
		};
		GLOBAL.sendEmail(msg);//,dataStruct.res);
	}
}
var saveGenericNotification = function(dataStruct)
{
	try{
		var res = dataStruct.res;
		var req = dataStruct.req;
		var dataJSON = req.body.data;
		
		
		dataJSON = eval("("+dataJSON+")");
		
		dataJSON['process_marker']=[];
		dataJSON['process_marker']=1;
		dataJSON['date']=[];
		dataJSON['date']=dataStruct.newUnixDate;
		dataJSON['date_simple']=[];
		dataJSON['date_simple']=dataStruct.newSimpleDate;

		notificationCollection.insert(dataJSON,function(err,doc){
			if(err==null){
				msg = {
					status:"Success",
					message:"[saveGenericNotification] Information Saved",
					data : []
				};
				msg.data.push(doc);
				GLOBAL.sendError(msg,dataStruct.res);
				//console.log("finding the document again");
				notificationCollection.findOne(dataJSON,function(err,doc){
					if(err==null)
					{
						//console.log("found document");
						var id = doc._id.toString();
						////console.log("calling single_processData with "+id);
						setImmediate(data_integrity.single_processData,doc,notificationCollection);
						//console.log("end of function saveGenericNotification");
					}else{
						var desc = {
							err:err,
							doc:doc,
							data:dataJSON
						}
						msg = {
							status:"Error",
							message:"[saveGenericNotification] Error Processing Data",
							description: desc
						};
						GLOBAL.sendError(msg,dataStruct.res);
					}
				});
			}else{
				var desc = {
					err:err,
					doc:doc,
					data:dataJSON
				}
				msg = {
					status:"Error",
					message:"[saveGenericNotification] Error Processing Data",
					description: desc
				};
				GLOBAL.sendError(msg,dataStruct.res);
			}
		});
	}catch(err){
		msg = {
			status:"Error",
			message:"[saveGenericNotification] Error Processing Data",
			description: err
		};
		GLOBAL.sendError(msg,dataStruct.res);
	}
}

var saveGenericChat = function(dataStruct)
{
	try{
		var res = dataStruct.res;
		var req = dataStruct.req;
		var dataJSON = req.body.data;

		dataJSON = eval("("+dataJSON+")");
		
		dataJSON['date']=[];
		dataJSON['date']=dataStruct.newUnixDate;
		dataJSON['date_simple']=[];
		dataJSON['date_simple']=dataStruct.newSimpleDate;
		dataJSON['server_generated_read_tags']=[];
		var dataJSON_server_generated_read_tagsString = '{"read'+dataJSON.from_info.user_id+'":true,"read'+dataJSON.to_info.user_id+'":false}';
		dataJSON_server_generated_read_tagsString=eval("("+dataJSON_server_generated_read_tagsString+")");
		dataJSON['server_generated_read_tags']=dataJSON_server_generated_read_tagsString;

		var chatIndexData = {
		    "date":dataStruct.newUnixDate,
		    "date_simple":dataStruct.newSimpleDate,
		    "users":[
		        {
		            "user_id":dataJSON.from_info.user_id
		        },
		        {
		            "user_id":dataJSON.to_info.user_id
		        }
		    ]
		};
		var verbJSONstruct = '{"read'+dataJSON.from_info.user_id+'":true,"read'+dataJSON.to_info.user_id+'":false,"date_modified":'+dataStruct.newUnixDate+',"date_modified_simple":"'+dataStruct.newSimpleDate+'"}';
		verbJSONstruct=eval("("+verbJSONstruct+")");
		chatIndexData[dataJSON.verb]=[];
		chatIndexData[dataJSON.verb]=verbJSONstruct;
		
		switch((dataJSON.verb).toLowerCase())
		{
			case 'order-chat':				
				var ExtraChatIndexData = {
				    "users":[
				        {
				            "user_id":dataJSON.from_info.user_id
				        },
				        {
				            "user_id":dataJSON.to_info.user_id
				        }
				    ],
				    "date":dataStruct.newUnixDate,
		    		"date_simple":dataStruct.newSimpleDate,
				    "shopstyx_order_id":(dataJSON.extra_information.order_info.order_id).toString(),
				    "shopstyx_order_number":parseInt(dataJSON.extra_information.order_info.customer_order_number),
				    "shopstyx_transaction_id":(dataJSON.extra_information.order_info.seller_transaction_number).toString(),
				    "server_generated_read_tags" : {}
				}
				var queryExtraIndex = {
					"$and":[
						{"users.user_id":dataJSON.from_info.user_id},
						{"users.user_id":dataJSON.to_info.user_id},
						{"shopstyx_order_id":(dataJSON.extra_information.order_info.order_id).toString()},
				    	{"shopstyx_order_number":parseInt(dataJSON.extra_information.order_info.customer_order_number)},
				    	{"shopstyx_transaction_id":(dataJSON.extra_information.order_info.seller_transaction_number).toString()}
					]
				};
				var ExtraChatIndexCollection = GLOBAL.orderchatIndex;
				break;
			case 'product-chat':
				var ExtraChatIndexData = {
				    "users":[
				        {
				            "user_id":dataJSON.from_info.user_id
				        },
				        {
				            "user_id":dataJSON.to_info.user_id
				        }
				    ],
				    "date":dataStruct.newUnixDate,
		    		"date_simple":dataStruct.newSimpleDate,
		    		"product_id":parseInt(dataJSON.extra_information.product_info.product_id),
					"store_id":parseInt(dataJSON.extra_information.product_info.store_id),
					"store_user_id":parseInt(dataJSON.extra_information.product_info.store_user_id),
				    "server_generated_read_tags" : {}
				};
				var queryExtraIndex = {
					"$and":[
						{"users.user_id":dataJSON.from_info.user_id},
						{"users.user_id":dataJSON.to_info.user_id},
						{"product_id":parseInt(dataJSON.extra_information.product_info.product_id)},
						{"store_id":parseInt(dataJSON.extra_information.product_info.store_id)},
						{"store_user_id":parseInt(dataJSON.extra_information.product_info.store_user_id)}
					]
				};
				var ExtraChatIndexCollection = GLOBAL.prodchatIndex;
				break;
			case 'store-chat':
				var ExtraChatIndexData = {
				    "users":[
				        {
				            "user_id":dataJSON.from_info.user_id
				        },
				        {
				            "user_id":dataJSON.to_info.user_id
				        }
				    ],
				    "date":dataStruct.newUnixDate,
		    		"date_simple":dataStruct.newSimpleDate,
		    		"store_id":parseInt(dataJSON.extra_information.store_info.store_id),
					"store_user_id":parseInt(dataJSON.extra_information.store_info.store_user_id),
					"claimed_by":parseInt(dataJSON.extra_information.store_info.claimed_by),
				    "server_generated_read_tags" : {}
				};
				var queryExtraIndex = {
					"$and":[
						{"users.user_id":dataJSON.from_info.user_id},
						{"users.user_id":dataJSON.to_info.user_id},
						{"claimed_by":parseInt(dataJSON.extra_information.store_info.claimed_by)},
						{"store_id":parseInt(dataJSON.extra_information.store_info.store_id)},
						{"store_user_id":parseInt(dataJSON.extra_information.store_info.store_user_id)}
					]
				};
				var ExtraChatIndexCollection = GLOBAL.storechatIndex;
				break;
			case 'chat':
			default:
				var ExtraChatIndexData = 'undefined';
				break;
		}
		if(ExtraChatIndexData!='undefined')
		{
			var server_generated_read_tagsString = '{"read'+dataJSON.from_info.user_id+'":true,"read'+dataJSON.to_info.user_id+'":false}';
			server_generated_read_tagsString = eval("("+server_generated_read_tagsString+")");
			ExtraChatIndexData.server_generated_read_tags = server_generated_read_tagsString;
			setImmediate(saveExtraIndex,ExtraChatIndexCollection,ExtraChatIndexData,queryExtraIndex,dataJSON,dataStruct);
		}
		setImmediate(saveChatIndex,chatIndexData,dataJSON,dataStruct);
		setImmediate(saveChatData,dataJSON,dataStruct);
	}catch(err){
		msg = {
			status:"Error",
			message:"[saveGenericChat] Error Processing Data",
			description: err
		};
		GLOBAL.sendError(msg,dataStruct.res);
	}
}

function saveExtraIndex(ExtraChatIndexCollection,ExtraChatIndexData,queryExtraIndex,dataJSON,dataStruct)
{
	try{
		ExtraChatIndexCollection.findOne(queryExtraIndex,function(err,doc){
			if(err==null)
			{
				if(doc!=null)
				{
					//update
					var updateQuery = {
						"_id":mongojs.ObjectId((doc._id).toString())
					};
					delete ExtraChatIndexData.users; //so that we won't cause MongoDB to have an error in trying to update the users.user_id array
					ExtraChatIndexCollection.update(updateQuery,{$set:ExtraChatIndexData},function(err,docs){
						if(err!=null)
						{
							var desc = {
								err:err,
								docs:docs,
								query:updateQuery
							};
							var msg = {
								status:"Error",
								message:"[saveExtraIndex] Error Updating Data",
								description: desc
							};
							GLOBAL.sendEmail(msg);
						}
					});
				}else{
					//insert
					ExtraChatIndexCollection.insert(ExtraChatIndexData,function(err,docs){
						if(err!=null)
						{
							var desc = {
								err:err,
								docs:docs,
								query:updateQuery
							};
							var msg = {
								status:"Error",
								message:"[saveExtraIndex] Error Inserting Data",
								description: desc
							};
							GLOBAL.sendEmail(msg);
						}
					});
				}
			}else{
				var desc = {
					err:err,
					docs:docs,
					query:queryExtraIndex
				};
				var msg = {
					status:"Error",
					message:"[saveExtraIndex] Error Finding Data",
					description: desc
				};
				GLOBAL.sendEmail(msg);
			}
		});
	}catch(err){
		var msg = {
			status:"Error",
			message:"[saveExtraIndex] Error Processing Data",
			description: err
		};
		GLOBAL.sendEmail(msg);
	}
}

function saveChatIndex(chatIndexData,dataJSON,dataStruct)
{
	try{
		var query = {
			"$and":[
				{"users.user_id":dataJSON.from_info.user_id},
				{"users.user_id":dataJSON.to_info.user_id}
			]
		};
		//util.inspect(query,{showHidden: false, depth: null});
		GLOBAL.chatIndex.findOne(query,function(err,doc){
			if(err==null)
			{
				if(doc!=null)
				{
					//update
					var updateQuery={
						"_id":mongojs.ObjectId((doc._id).toString())
					};
					delete chatIndexData.users;
					GLOBAL.chatIndex.update(updateQuery,{$set:chatIndexData},function(err,docs){
						if(err!=null)
						{
							var desc = {
								err:err,
								docs:docs
							};
							var msg = {
								status:"Error",
								message:"[saveChatIndex] Error Updating Data",
								description: desc
							};
							GLOBAL.sendEmail(msg);
						}
					});
				}else{
					//insert
					GLOBAL.chatIndex.insert(chatIndexData,function(err,docs){
						if(err!=null)
						{
							var desc = {
								err:err,
								docs:docs
							};
							var msg = {
								status:"Error",
								message:"[saveChatIndex] Error Inserting Data",
								description: desc
							};
							GLOBAL.sendEmail(msg);
						}
					});
				}
			}else{
				var desc = {
					err:err,
					doc:doc,
					query:query
				};
				var msg = {
					status:"Error",
					message:"[saveChatIndex] Error Finding Data",
					description: desc
				};
				GLOBAL.sendEmail(msg);
			}
		});
	}catch(err){
		var msg = {
			status:"Error",
			message:"[saveChatIndex] Error Processing Data",
			description: err
		};
		GLOBAL.sendEmail(msg);
	}
}

function saveChatData(dataJSON,dataStruct)
{
	try{
		GLOBAL.chatCollection.insert(dataJSON,function(err,doc){
			if(err!=null)
			{
				var desc = {
					err:err,
					docs:docs
				};
				var msg = {
					status:"Error",
					message:"[saveChatData] Error Processing Data",
					description: desc
				};
				GLOBAL.sendError(msg,dataStruct.res);
			}else{
				var msg = {
					status:"Success",
					message:"Chat record saved",
					data:[]
				};
				msg.data.push(doc);
				GLOBAL.sendError(msg,dataStruct.res);
			}
		});
	}catch(err){
		msg = {
			status:"Error",
			message:"[saveChatData] Error Processing Data",
			description: err
		};
		GLOBAL.sendError(msg,dataStruct.res);
	}
}


module.exports = {
	saveNotification:saveNotification
}