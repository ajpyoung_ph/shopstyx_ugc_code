
var saveEmailBlock = function(dataStruct)
{
	try{
		var res = dataStruct.res;
		var req = dataStruct.req;
		var dataJSON = req.body.data;

		if(typeof(dataJSON)=="string")
		{
			dataJSON = eval("("+dataJSON+")");
		}
		
		/*
			DATA: JSON data
			{
				"type" : "Feature",
				"properties" : {},
				"geometry" : {
				                 "type" : "Point",
				                 "coordinates" : [ 
				            		              123.8897752761841, (longitude; I think)
				            		              10.31576371999243 (latitude; I think)
				        	                 ]
				},
				ip_address: String, (this ip_address will be converted to type:feature->geometry),
			    user_id:INT, (user_id of the person who owns the config
				notificationBlock:[
			                               PA_NP,PA_UF,PA_FC
			                          ]
			}
		*/
		var query = {
			"user_id":parseInt(dataJSON.user_id)
		};
		if(typeof(dataJSON.notificationBlock)!='object')
		{
			dataJSON.notificationBlock=[];
		}
		GLOBAL.accountSetting.findOne(query,function(err,doc){
			if(err==null)
			{
				if(doc!=null)
				{
					modifyEmailBlock(dataJSON,dataStruct);
				}else{
					insertEmailBlock(dataJSON,dataStruct);
				}
			}else{
				msg = {
					status:"Error",
					message:"[saveEmailBlock] Error Processing Data",
					code:2,
					description: err
				};
				GLOBAL.sendError(msg,dataStruct.res);
			}
		});
		
	}catch(err){
		msg = {
			status:"Error",
			message:"[saveEmailBlock] Error Processing Data",
			code:1,
			description: err
		};
		GLOBAL.sendError(msg,dataStruct.res);
	}
};

function modifyEmailBlock(dataJSON,dataStruct)
{
	var query = {
		"user_id":parseInt(dataJSON.user_id)
	};
	var updateEmailBlock = {
		"notificationBlock":dataJSON.notificationBlock.slice()
	};
	GLOBAL.accountSetting.update(query,{$set:updateEmailBlock},function(err,docs){
		if(err==null)
		{
			var msg = {
				status:"Success",
				message:"[modifyEmailBlock] Account Setting Saved",
				data : []
			};
			msg.data.push(docs);
			GLOBAL.sendError(msg,dataStruct.res);
		}else{
			var desc = {
				err:err,
				docs:docs,
				query:queryChatRecords
			};
			var msg = {
				status:"Error",
				message:"[modifyEmailBlock] Error Processing Data",
				description: desc
			};
			GLOBAL.sendEmail(msg);
		}
	});
}

function insertEmailBlock(dataJSON,dataStruct)
{
	if(typeof(dataJSON.user_id)!="number")
	{
		dataJSON.user_id=parseInt(dataJSON.user_id);
	}
	GLOBAL.accountSetting.insert(dataJSON,function(err,doc){
		if(err==null)
		{
			var msg = {
				status:"Success",
				message:"[insertEmailBlock] Account Setting Saved",
				data : []
			};
			msg.data.push(doc);
			GLOBAL.sendError(msg,dataStruct.res);
		}else{
			var desc = {
				err:err,
				doc:doc
			};
			var msg = {
				status:"Error",
				message:"[insertEmailBlock] Error Processing Data",
				description: desc
			};
			GLOBAL.sendError(msg,dataStruct.res);
		}
	});
}

module.exports = {
	saveEmailBlock:saveEmailBlock
};