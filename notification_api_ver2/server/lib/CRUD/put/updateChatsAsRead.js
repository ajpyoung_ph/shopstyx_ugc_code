var mongojs = require('mongojs');

var markGenChatRead = function(dataStruct)
{
	try{
		///:token/:user_id/mark/read/generic/chat/:target_user_id
		//chatRecords
		//chatIndex

		// ,
		// 		{
		// 			"$or":[
		// 				{"to_info.user_id":dataStruct.user_id},
		// 				{"to_info.user_id":parseInt(dataStruct.req.params.target_user_id)},
		// 			]
		// 		}
		var msg = {
			status:"Success",
			message:"Marking Chat as Read"
		};
		GLOBAL.sendError(msg,dataStruct.res);
		var queryChatRecords = {
			"$and":[
				{
					"from_info.user_id":parseInt(dataStruct.user_id)
				},
				{
					"to_info.user_id":parseInt(dataStruct.req.params.target_user_id)
				},
				{
					"verb":"chat"
				}
			]
		};
		if(typeof(dataStruct.queryChatRecords)!='undefined')
		{
			queryChatRecords=dataStruct.queryChatRecords;
		}
		var setChatRecords='{"server_generated_read_tags.read'+dataStruct.user_id+'":true,"from_info.read":true}';
		setChatRecords=eval("("+setChatRecords+")");
		GLOBAL.chatCollection.update(queryChatRecords,{$set:setChatRecords},{multi:true},function(err,docs){
			if(err!=null)
			{
				var desc = {
					err:err,
					docs:docs,
					query:queryChatRecords
				};
				var msg = {
					status:"Error",
					message:"[markGenChatRead] Error Updating Generic Chat Stage 1",
					description: desc
				};
				GLOBAL.sendEmail(msg);
			}
		});

		var queryChatRecords2 = {
			"$and":[
				{
					"from_info.user_id":parseInt(dataStruct.req.params.target_user_id)
				},
				{
					"to_info.user_id":parseInt(dataStruct.user_id)
				},
				{
					"verb":"chat"
				}
			]
		};
		if(typeof(dataStruct.queryChatRecords2)!='undefined')
		{
			queryChatRecords2=dataStruct.queryChatRecords2;
		}
		var setChatRecords2='{"server_generated_read_tags.read'+dataStruct.user_id+'":true,"to_info.read":true}';
		setChatRecords2=eval("("+setChatRecords2+")");
		GLOBAL.chatCollection.update(queryChatRecords2,{$set:setChatRecords2},{multi:true},function(err,docs){
			if(err!=null)
			{
				var desc = {
					err:err,
					docs:docs,
					query:queryChatRecords
				};
				var msg = {
					status:"Error",
					message:"[markGenChatRead] Error Updating Generic Chat Stage 2",
					description: desc
				};
				GLOBAL.sendEmail(msg);
			}
		});

		var queryChatIndex = {
			"$and":[
				{
					"users.user_id":parseInt(dataStruct.req.params.target_user_id)
				},
				{
					"users.user_id":parseInt(dataStruct.user_id)	
				}
			]
		};
		var setChatIndex = '{"chat.read'+dataStruct.user_id+'":true}';
		if(typeof(dataStruct.setChatIndex)!='undefined')
		{
			setChatIndex=dataStruct.setChatIndex;
		}
		setChatIndex = eval("("+setChatIndex+")");
		GLOBAL.chatIndex.update(queryChatIndex,{$set:setChatIndex},function(err,docs){
			if(err!=null)
			{
				var desc = {
					err:err,
					docs:docs,
					query:queryChatRecords
				};
				var msg = {
					status:"Error",
					message:"[markGenChatRead] Error Updating Generic Chat Stage 2",
					description: desc
				};
				GLOBAL.sendEmail(msg);
			}
		});
	}catch(err){
		var msg = {
			status:"Error",
			message:"[markGenChatRead] Error Processing Data",
			description: err
		};
		GLOBAL.sendEmail(msg);
	}
}

var markProdChatRead = function(dataStruct)
{
	try{
		//:token/:user_id/mark/read/product/chat/:target_user_id/:product_id/:store_id/:store_user_id/
		//chatRecords
		//chatIndex
		//prodchatIndex
		dataStruct.queryChatRecords=[];
		dataStruct.queryChatRecords={
			"$and":[
				{
					"from_info.user_id":parseInt(dataStruct.user_id)
				},
				{
					"to_info.user_id":parseInt(dataStruct.req.params.target_user_id)
				},
				{
					"verb":"product-chat"
				},
				{
					"extra_information.product_info.product_id":parseInt(dataStruct.req.params.product_id)
				},
				{
					"extra_information.product_info.store_id":parseInt(dataStruct.req.params.store_id)
				},
				{
					"extra_information.product_info.store_user_id":parseInt(dataStruct.req.params.store_user_id)
				}
			]
		};
		dataStruct.queryChatRecords2=[];
		dataStruct.queryChatRecords2={
			"$and":[
				{
					"from_info.user_id":parseInt(dataStruct.req.params.target_user_id)
				},
				{
					"to_info.user_id":parseInt(dataStruct.user_id)
				},
				{
					"verb":"product-chat"
				},
				{
					"extra_information.product_info.product_id":parseInt(dataStruct.req.params.product_id)
				},
				{
					"extra_information.product_info.store_id":parseInt(dataStruct.req.params.store_id)
				},
				{
					"extra_information.product_info.store_user_id":parseInt(dataStruct.req.params.store_user_id)
				}
			]
		};
		dataStruct.setChatIndex=[];
		dataStruct.setChatIndex = '{"product-chat.read'+dataStruct.user_id+'":true}';
		markGenChatRead(dataStruct);
		var productChatIndexQuery = {
			"$and":[
				{
					"users.user_id":parseInt(dataStruct.req.params.target_user_id)
				},
				{
					"users.user_id":parseInt(dataStruct.user_id)
				}
			]
		};
		var prodSet = '{"server_generated_read_tags.read'+dataStruct.user_id+'":true}';
		prodSet = eval("("+prodSet+")");

		GLOBAL.prodchatIndex.update(productChatIndexQuery,{$set:prodSet},function(err,docs){
			if(err!=null)
			{
				var desc = {
					err:err,
					docs:docs,
					query:queryChatRecords
				};
				var msg = {
					status:"Error",
					message:"[markProdChatRead] Error Updating Product Index",
					description: desc
				};
				GLOBAL.sendEmail(msg);
			}
		});
	}catch(err){
		var msg = {
			status:"Error",
			message:"[markProdChatRead] Error Processing Data",
			description: err
		};
		GLOBAL.sendEmail(msg);
	}
}

var markOrderChatRead = function(dataStruct)
{
	try{
		///:token/:user_id/mark/read/order/chat/:target_user_id/:order_id
		//chatRecords
		//chatIndex
		//prodchatIndex
		dataStruct.queryChatRecords=[];
		dataStruct.queryChatRecords={
			"$and":[
				{
					"from_info.user_id":parseInt(dataStruct.user_id)
				},
				{
					"to_info.user_id":parseInt(dataStruct.req.params.target_user_id)
				},
				{
					"verb":"order-chat"
				}
			]
		};
		dataStruct.queryChatRecords2=[];
		dataStruct.queryChatRecords2={
			"$and":[
				{
					"from_info.user_id":parseInt(dataStruct.req.params.target_user_id)
				},
				{
					"to_info.user_id":parseInt(dataStruct.user_id)
				},
				{
					"verb":"order-chat"
				}
			]
		};
		dataStruct.setChatIndex=[];
		dataStruct.setChatIndex = '{"order-chat.read'+dataStruct.user_id+'":true}';
		markGenChatRead(dataStruct);
		var orderChatIndexQuery = {
			"$and":[
				{
					"users.user_id":parseInt(dataStruct.req.params.target_user_id)
				},
				{
					"users.user_id":parseInt(dataStruct.user_id)
				}
			]
		};
		var orderSet = '{"server_generated_read_tags.read'+dataStruct.user_id+'":true}';
		orderSet = eval("("+orderSet+")");

		GLOBAL.orderchatIndex.update(orderChatIndexQuery,{$set:orderSet},function(err,docs){
			if(err!=null)
			{
				var desc = {
					err:err,
					docs:docs,
					query:queryChatRecords
				};
				var msg = {
					status:"Error",
					message:"[markOrderChatRead] Error Updating Product Index",
					description: desc
				};
				GLOBAL.sendEmail(msg);
			}
		});
	}catch(err){
		var msg = {
			status:"Error",
			message:"[markOrderChatRead] Error Updating Product Index",
			description: err
		};
		GLOBAL.sendEmail(msg);
	}
}

var markStoreChatRead = function(dataStruct)
{
	try{
		//:token/:user_id/mark/read/store/chat/:target_user_id/:claimed_by/:store_id/:store_user_id/
		//chatRecords
		//chatIndex
		//prodchatIndex
		dataStruct.queryChatRecords=[];
		dataStruct.queryChatRecords={
			"$and":[
				{
					"from_info.user_id":parseInt(dataStruct.user_id)
				},
				{
					"to_info.user_id":parseInt(dataStruct.req.params.target_user_id)
				},
				{
					"verb":"store-chat"
				},
				{
					"extra_information.store_info.claimed_by":parseInt(dataStruct.req.params.claimed_by)
				},
				{
					"extra_information.store_info.store_id":parseInt(dataStruct.req.params.store_id)
				},
				{
					"extra_information.store_info.store_user_id":parseInt(dataStruct.req.params.store_user_id)
				}
			]
		};
		dataStruct.queryChatRecords2=[];
		dataStruct.queryChatRecords2={
			"$and":[
				{
					"from_info.user_id":parseInt(dataStruct.req.params.target_user_id)
				},
				{
					"to_info.user_id":parseInt(dataStruct.user_id)
				},
				{
					"verb":"store-chat"
				},
				{
					"extra_information.store_info.claimed_by":parseInt(dataStruct.req.params.claimed_by)
				},
				{
					"extra_information.store_info.store_id":parseInt(dataStruct.req.params.store_id)
				},
				{
					"extra_information.store_info.store_user_id":parseInt(dataStruct.req.params.store_user_id)
				}
			]
		};
		dataStruct.setChatIndex=[];
		dataStruct.setChatIndex = '{"store-chat.read'+dataStruct.user_id+'":true}';
		markGenChatRead(dataStruct);
		var storeChatIndexQuery = {
			"$and":[
				{
					"users.user_id":parseInt(dataStruct.req.params.target_user_id)
				},
				{
					"users.user_id":parseInt(dataStruct.user_id)
				}
			]
		};
		var storeSet = '{"server_generated_read_tags.read'+dataStruct.user_id+'":true}';
		storeSet = eval("("+storeSet+")");

		GLOBAL.storechatIndex.update(storeChatIndexQuery,{$set:storeSet},function(err,docs){
			if(err!=null)
			{
				var desc = {
					err:err,
					docs:docs,
					query:queryChatRecords
				};
				var msg = {
					status:"Error",
					message:"[markStoreChatRead] Error Updating Product Index",
					description: desc
				};
				GLOBAL.sendEmail(msg);
			}
		});
	}catch(err){
		var msg = {
			status:"Error",
			message:"[markStoreChatRead] Error Processing Data",
			description: err
		};
		GLOBAL.sendEmail(msg);
	}
}

module.exports = {
	markGenChatRead:markGenChatRead,
	markProdChatRead:markProdChatRead,
	markOrderChatRead:markOrderChatRead,
	markStoreChatRead:markStoreChatRead
}