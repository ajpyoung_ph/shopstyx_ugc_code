var mongojs = require('mongojs');
var util = require('util');


var markRecordRead = function(dataStruct)
{

	try{
		var res = dataStruct.res;
		var req = dataStruct.req;
		
		// var mongojs = require('mongojs');
		var objID = mongojs.ObjectId((req.params.recID).toString());
		var source_user_id = parseInt(req.params.user_id);


		var query = {
			"_id":objID,
			"from_info.user_id":parseInt(source_user_id),
			"from_info.read":false
		};
		
		var setVal = {
			$set:{"from_info.read":true}
		};
		console.log(query);
		console.log(setVal);
		GLOBAL.notificationCollection.update(query,setVal,function(err1,docs1){
			if(err1!=null)
			{
				var desc = {
					query:query,
					setVal:setVal,
					docs:docs1,
					err:err1
				}
				var msg = {
					status:"Background Error",
					message:"[markRecordRead] error marking read doc1",
					description:desc
				};
				GLOBAL.sendEmail(msg);//,dataStruct.res);
			}
		});

		var query2 = {
			"_id":objID,
			"to_info.user_id":parseInt(source_user_id),
			"to_info.read":false
		}
		
		var setVal2 = {
			$set:{"to_info.read":false}	
		};
		console.log(query2);
		console.log(setVal2);
		GLOBAL.notificationCollection.update(query2,setVal2,function(err2,docs2){
			if(err2!=null)
			{
				var desc = {
					query:query2,
					setVal2:setVal2,
					docs:docs2,
					err:err2
				}
				var msg = {
					status:"Background Error",
					message:"[markRecordRead] error marking read doc2",
					description:desc
				};
				GLOBAL.sendEmail(msg);//,dataStruct.res);
			}
		});

		var query3 = {
			"_id":objID,
			"server_generated_information.follower_list":{
				$elemMatch:{
					$and:[
						{"user_id":parseInt(source_user_id)},
						{"read":false}
					]
				}
			}
		};

		var setVal3 = {
			$set:{"server_generated_information.follower_list.$.read":false}
		};
		console.log(query3);
		console.log(setVal3);
		GLOBAL.notificationCollection.update(query3,setVal3,function(err3,docs3){
			if(err3!=null)
			{
				var desc = {
					query:query3,
					setVal3:setVal3,
					docs:docs3,
					err:err3
				}
				var msg = {
					status:"Background Error",
					message:"[markRecordRead] error marking read doc3",
					description:desc
				};
				GLOBAL.sendEmail(msg);//,dataStruct.res);
			}
		});
		
		var msg = {
			status:"Success",
			message:"Record marked as Read"
		};
		GLOBAL.sendError(msg,dataStruct.res);	
	}catch(err){
		
		var msg = {
			status:"Error",
			message:"[markRecordRead] Error Processing Data",
			desc:JSON.stringify(err)
		};
		GLOBAL.sendError(msg,dataStruct.res);
	}
}
// var markRecordRead = function(dataStruct)
// {
// 	try{
// 		var res = dataStruct.res;
// 		var req = dataStruct.req;
// 		var notificationCollection = GLOBAL.notificationCollection;
// 		// var mongojs = require('mongojs');
// 		var objID = mongojs.ObjectId((req.params.recID).toString());
// 		var source_user_id = parseInt(req.params.user_id);
// 		search notification table
// 		var query = {
// 			$and:[{"_id":objID},
// 				{
// 					$or:[
// 						{

// 							$and:[
// 								{"from_info.user_id":parseInt(source_user_id)},
// 								{"from_info.read":false}
// 							],
// 							$and:[
// 								{"to_info.user_id":parseInt(source_user_id)},
// 								{"to_info.read":false}
// 							],
// 							$and:[
// 								{"server_generated_information.follower_list.user_id":parseInt(source_user_id)},
// 								{"server_generated_information.follower_list.read":false}
// 							]
// 						}
// 					]
// 				}
// 			]
// 		};

// 		notificationCollection.findOne(query,function(err,doc){
// 			if(err==null)
// 			{
// 				if(doc!=null)
// 				{
// 					var setVar = {};
// 					var query = {};
// 					var found = false;
// 					if(parseInt(doc.from_info.user_id) == parseInt(source_user_id) && found == false)
// 					{
// 						setVar = {"from_info.read":true};
// 						query = {"$and":[
// 							{"_id":objID},
// 							{"from_info.user_id":parseInt(source_user_id)}
// 						]};
// 						found = true;
// 					}

// 					if(parseInt(doc.to_info.user_id) ==  parseInt(source_user_id) && found == false)
// 					{
// 						setVar = {"to_info.read":true};
// 						query = {"$and":[
// 							{"_id":objID},
// 							{"to_info.user_id":parseInt(source_user_id)}
// 						]};
// 						found = true;
// 					}
// 					if(found==false)
// 					{
// 						//check server_generated_information.follower_list
// 						for(var x = 0;x < doc.server_generated_information.follower_list.length; x++)
// 						{
// 							var temp_id = doc.server_generated_information.follower_list[x].user_id;
// 							if(parseInt(temp_id) == parseInt(source_user_id))
// 							{
// 								setVar = {"server_generated_information.follower_list.$.read":true};
// 								query = {"$and":[
// 									{"_id":objID},
// 									{"server_generated_information.follower_list.user_id":parseInt(source_user_id)}
// 								]};
// 								found=true;
// 								break;
// 							}
// 						}
// 					}
// 					if(found==true)
// 					{
// 						notificationCollection.update(query,{$set:setVar},function(err,doc){
// 							if(err==null)
// 							{
// 								if(doc.n>0)
// 								{
// 									var msg = {
// 										status:"Success",
// 										message:"Record marked as Read"
// 									};
// 									GLOBAL.sendError(msg,dataStruct.res);
// 								}else{
// 									var desc = {
// 										err:err,
// 										doc:doc,
// 										query:query,
// 										additional_info: "No Record Updated"
// 									};
// 									var msg = {
// 										status:"Error",
// 										message:"[markRecordRead] Error Processing Data",
// 										description: desc
// 									};
// 									GLOBAL.sendError(msg,dataStruct.res);
// 								}
// 							}else{
// 								var desc = {
// 									err:err,
// 									doc:doc,
// 									query:query,
// 									additional_info: "No Record Found"
// 								};
// 								var msg = {
// 									status:"Error",
// 									message:"[markRecordRead] Error Processing Data",
// 									description: desc
// 								};
// 								GLOBAL.sendError(msg,dataStruct.res);
// 							}
// 						})
// 					}else{
// 						var desc = {
// 							err:err,
// 							doc:doc,
// 							query:query,
// 							additional_info: "No Record Found"
// 						};
// 						var msg = {
// 							status:"Error",
// 							message:"[markRecordRead] Error Processing Data",
// 							description: desc
// 						};
// 						GLOBAL.sendError(msg,dataStruct.res);
// 					}
// 				}else{
// 					var desc = {
// 						err:err,
// 						doc:doc,
// 						query:query,
// 						additional_info: "No Record Found"
// 					};
// 					var msg = {
// 						status:"Error",
// 						message:"[markRecordRead] Error Processing Data",
// 						description: desc
// 					};
// 					GLOBAL.sendError(msg,dataStruct.res);
// 				}
// 			}else{
// 				var desc = {
// 					err:err,
// 					doc:doc,
// 					query:query
// 				};
// 				var msg = {
// 					status:"Error",
// 					message:"[markRecordRead] Error Processing Data",
// 					description: desc
// 				};
// 				GLOBAL.sendError(msg,dataStruct.res);
// 			}
// 		});
// 	}catch(err){
// 		var msg = {
// 			status:"Error",
// 			message:"[markRecordRead] Error Processing Data",
// 			description: err
// 		};
// 		GLOBAL.sendError(msg,dataStruct.res);
// 	}
// }


var markALLRecordRead = function(dataStruct)
{
	try{
		var res = dataStruct.res;
		var req = dataStruct.req;
		//var notificationCollection = GLOBAL.notificationCollection;
		var source_user_id = parseInt(dataStruct.user_id);
		//search notification table
		var query = {
			$and:[
            	{"from_info.user_id":parseInt(source_user_id)},
            	{"from_info.read":false},
            	{"type":"immediate"}
            ]
		};
		var setVal = {
			$set:{"from_info.read":true}
		};
		GLOBAL.notificationCollection.update(query,setVal,function(err,docs){
			if(err!=null)
			{
				var desc = {
					query:query,
					setVal:setVal,
					docs:docs,
					err:err
				}
				var msg = {
					status:"Background Error",
					message:"[markALLRecordRead] error marking read doc",
					description:desc
				};
				GLOBAL.sendEmail(msg);//,dataStruct.res);
			}
		});

		var query2 = {
			 $and:[
            	{"to_info.user_id":parseInt(source_user_id)},
            	{"to_info.read":false},
            	{"type":"immediate"}
            ]
		};
		var setVal2 = {
			$set:{"to_info.read":true}
		};
		GLOBAL.notificationCollection.update(query2,setVal2,function(err2,docs2){
			if(err2!=null)
			{
				var desc = {
					query:query2,
					setVal:setVal2,
					docs:docs2,
					err:err2
				}
				var msg = {
					status:"Background Error",
					message:"[markALLRecordRead] error marking read doc2",
					description:desc
				};
				GLOBAL.sendEmail(msg);//,dataStruct.res);
			}
		});

		var query3 = {
			"type":"immediate",
			"server_generated_information.follower_list":{
				$elemMatch:{
					$and:[
						{"user_id":parseInt(source_user_id)},
						{"read":false}
					]
				}
			}
		}
		var setVal3 = {
			$set:{"server_generated_information.follower_list.$.read":true}
		}
		GLOBAL.notificationCollection.update(query3,setVal3,function(err3,docs3){
			if(err3!=null)
			{
				var desc = {
					query:query3,
					setVal:setVal3,
					docs:docs3,
					err:err3
				}
				var msg = {
					status:"Background Error",
					message:"[markALLRecordRead] error marking read doc3",
					description:desc
				};
				GLOBAL.sendEmail(msg);//,dataStruct.res);
			}
		});

		var query4 = {
			$and:[
        		{
        			$or:[
        				{"verb":"follow-user"},
        				{"verb":"follow-shop"},
        				{"verb":"follow-collection"}
        			]
        		},
        		{
        			"extra_information.user_info.user_id":parseInt(source_user_id)
        		},
        		{
        			$or:[
            			{"extra_information.user_info.read":{$exists:false}},
            			{"extra_information.user_info.read":false}
        			]
        		},
        		{"type":"immediate"}
        	]
		};
		var setVal4 = {
			$set:{"extra_information.user_info.read":true}
		};
		GLOBAL.notificationCollection.update(query4,setVal4,function(err4,docs4){
			if(err4!=null)
			{
				var desc = {
					query:query4,
					setVal:setVal4,
					docs:docs4,
					err:err4
				}
				var msg = {
					status:"Background Error",
					message:"[markALLRecordRead] error marking read doc4",
					description:desc
				};
				GLOBAL.sendEmail(msg);//,dataStruct.res);
			}
		});

		var msg = {
			status:"Success",
			message:"[markALLRecordRead] Record marked as Read"
		};
		GLOBAL.sendError(msg,dataStruct.res);
	}catch(errALL){
		var desc = {
			query:query,
			query2:query2,
			query3:query3,
			query4:query4,
			setVal:setVal,
			setVal2:setVal2,
			setVal3:setVal3,
			setVal4:setVal4,
			source_user_id:source_user_id
		};
		var msg = {
			status:"Error",
			message:"[markALLRecordRead] Error Processing Data",
			description: errALL,
			addInfo:desc
		
		};
		GLOBAL.sendError(msg,dataStruct.res);		
	}
}

// var markALLRecordRead = function(dataStruct)
// {
// 	try{
// 		var res = dataStruct.res;
// 		var req = dataStruct.req;
// 		var notificationCollection = GLOBAL.notificationCollection;
// 		var source_user_id = parseInt(dataStruct.user_id);
// 		//search notification table
// 		var query = {
// 			$and:[
// 				{
// 					$or:[
// 			            {
// 			                $and:[
// 			                	{"from_info.user_id":parseInt(source_user_id)},
// 			                	{"from_info.read":false}
// 			                ]
// 			            },
// 			            {
// 			                $and:[
// 			                	{"to_info.user_id":parseInt(source_user_id)},
// 			                	{"to_info.read":false}
// 			                ]
// 			            },
// 			            {
// 			                $and:[
// 			                	{"server_generated_information.follower_list.user_id":parseInt(source_user_id)},
// 			                	{"server_generated_information.follower_list.read":false}
// 			                ]
// 			            },
// 			            {
// 			            	$and:[
// 			            		{
// 			            			$or:[
// 			            				{"verb":"follow-user"},
// 			            				{"verb":"follow-shop"},
// 			            				{"verb":"follow-collection"}
// 			            			]
// 			            		},
// 			            		{
// 			            			"extra_information.user_info.user_id":parseInt(source_user_id)
// 			            		},
// 			            		{
// 			            			$or:[
// 				            			{"extra_information.user_info.read":{$exists:false}},
// 				            			{"extra_information.user_info.read":false}
// 			            			]
// 			            		}
// 			            	]
// 			            }
// 			        ]
// 				},
// 				{
// 					"type":"immediate"
// 				}
// 			]
// 		};

// 		notificationCollection.find(query,function(err,docs){
// 			if(err==null)
// 			{
// 				if(docs.length>0)
// 				{
// 					msg = {
// 						status:"Success",
// 						message:"Records marked as Read"
// 					};
// 					GLOBAL.sendError(msg,dataStruct.res);
// 					docs.forEach(function(doc){
// 						var setVar = {};
// 						var query = {};
// 						var found = false;
						
// 						var objID = mongojs.ObjectId((doc._id).toString());
// 						if(parseInt(doc.from_info.user_id) == parseInt(source_user_id) && found == false)
// 						{
// 							setVar = {"from_info.read":true};
// 							query = {"$and":[
// 								{"_id":objID},
// 								{"from_info.user_id":parseInt(source_user_id)}
// 							]};
// 							found = true;
// 						}
// 						try{
// 							if(parseInt(doc.to_info.user_id) ==  parseInt(source_user_id) && found == false)
// 							{
// 								setVar = {"to_info.read":true};

// 								query = {"$and":[
// 									{"_id":objID},
// 									{"to_info.user_id":parseInt(source_user_id)}
// 								]};
// 								found = true;
// 							}
// 						}catch(err){
// 							//do nothing
// 							console.log(util.inspect(err,{showHidden: false, depth: null}));
// 						}
						
// 						try{
// 							if(doc.verb=="follow-user"||doc.verb=="follow-shop"||doc.verb=="follow-collection")
// 							{
// 								setVar = {"extra_information.user_info.read":true};
// 								query={"$and":[
// 									{"_id":objID},
// 									{"extra_information.user_info.user_id":parseInt(source_user_id)}
// 								]};
// 								found = true;
// 							}
// 						}catch(err){
// 							//do nothing
// 						}
// 						if(found==false)
// 						{
// 							try
// 							{
// 								//check server_generated_information.follower_list
// 								for(var x = 0;x < doc.server_generated_information.follower_list.length; x++)
// 								{
// 									if(typeof(doc.server_generated_information.follower_list[x].user_id)!='undefined')
// 									{
// 										var temp_id = doc.server_generated_information.follower_list[x].user_id;
// 										if(parseInt(temp_id) == parseInt(source_user_id))
// 										{
// 											setVar = {"server_generated_information.follower_list.$.read":true};
// 											query = {"$and":[
// 												{"_id":objID},
// 												{"server_generated_information.follower_list.user_id":parseInt(source_user_id)}
// 											]};
// 											found=true;
// 											break;
// 										}
// 									}
// 								}
// 							}catch(err){
// 								//do nothing
// 								console.log(util.inspect(err,{showHidden: false, depth: null}));
// 							}
// 						}
// 						if(found==true)
// 						{
// 							notificationCollection.update(query,{$set:setVar},function(err2,doc2){
// 								if(err2==null)
// 								{
// 									if(doc2.n>0)
// 									{
// 										msg = {
// 											status:"Success",
// 											message:"Record marked as Read"
// 										};
// 										console.log(msg);
// 									}else{
// 										var desc = {
// 											err:err2,
// 											doc:doc2,
// 											query:query,
// 											additional_info: "No Record Updated"
// 										};
// 										var msg = {
// 											status:"Success",
// 											message:"[markALLRecordRead] No More Records to Update",
// 											description: desc
// 										};
// 										//GLOBAL.sendEmail(msg);
// 									}
// 								}else{
// 									var desc = {
// 										err:err2,
// 										doc:doc2,
// 										query:query,
// 										additional_info: "No Record Updated"
// 									};
// 									var msg = {
// 										status:"Error",
// 										message:"[markALLRecordRead] Error Processing Data",
// 										description: desc
// 									};
// 									console.log(msg);
// 									//GLOBAL.sendEmail(msg);
// 								}
// 							})
// 						}else{
// 							// var desc = {
// 							// 	err:err,
// 							// 	doc:doc,
// 							// 	query:query,
// 							// 	additional_info: "No Record Found"
// 							// };
// 							// var msg = {
// 							// 	status:"Error",
// 							// 	message:"[markALLRecordRead] Error Processing Data",
// 							// 	description: desc
// 							// };
// 							// console.log(util.inspect(msg,{showHidden: false, depth: null}));
// 						}
// 					});
					
// 				}else{
// 					var desc = {
// 						err:err,
// 						doc:docs,
// 						query:query,
// 						additional_info: "No Record Found"
// 					};
// 					var msg = {
// 						status:"Success",
// 						message:"[markALLRecordRead] No More Data marked as Unread",
// 						description: desc
// 					};
// 					//console.log(msg);
// 					GLOBAL.sendError(msg,dataStruct.res);
// 				}
// 			}else{
// 				var desc = {
// 					err:err,
// 					doc:docs,
// 					query:query
// 				};
// 				var msg = {
// 					status:"Error",
// 					message:"[markALLRecordRead] Error Processing Data",
// 					description: desc
// 				};
// 				GLOBAL.sendError(msg,dataStruct.res);
// 			}
// 		});
// 	}catch(err){
// 		var msg = {
// 			status:"Error",
// 			message:"[markALLRecordRead] Error Processing Data",
// 			description: err
// 		};
// 		GLOBAL.sendError(msg,dataStruct.res);
// 	}
// }
module.exports = {
	markRecordRead:markRecordRead,
	markALLRecordRead:markALLRecordRead
}