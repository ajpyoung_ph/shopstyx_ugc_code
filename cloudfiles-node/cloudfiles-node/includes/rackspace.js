/*
 * rackspace.js = wrapper around the pkgcloud openstack with rackspace as a provider
 * Implementation app.js:
 *	// Loading rackspace modules
 *	var rackspace = require('./includes/rackspace');
 *	var app = express();
 *
 *  Before the app.use('/', routes) line:
 *  app.use(function(req,res,next){
 *  	req.rackspace = rackspace;
 *  	next();
 *  });
 */

if(process.env.NODE_ENV == 'production'){
	var rackvars = {
		username: 'clint.canada',
		apiKey: '91b1570c1e8f49319ae7610fbacc0eb8',
		region: 'ORD',
		container: 'UGC',
		useInternal: true
	};
} else {
	var rackvars = {
		username: 'clint.canada',
		apiKey: '91b1570c1e8f49319ae7610fbacc0eb8',
		region: 'ORD',
		container: 'UGC',
		useInternal: false
	}
};

var client = require('pkgcloud').storage.createClient({
	provider: 'rackspace',
	username: rackvars.username,
	apiKey: rackvars.apiKey,
	region: rackvars.region,
	useInternal: rackvars.useInternal
});

var rackspace = {
	client: client,
	container: rackvars.container,
	fs: require('fs'),
	init: function(){
		console.info('running init');
		var client = this.client;
		var container = this.container;
		console.info('check if exists '+container);
		// We will check if container exists, if not, create it
		client.getContainer(container, function(err, container) { 
			if(err){
				// No container exists, create one - this is a hack
				if(process.env.NODE_ENV == 'production'){
					var nme = "UGC";
				} else {
					var nme = "UGC";
				}
				var cont = {
					name: nme,
					cdnEnabled: true
				}
				console.info('creating '+nme);
				client.createContainer(cont, function(err, container) { 
					// Stub
				});
			} else { 
				console.info(JSON.stringify(container));
			}
		});
	},
	// This is to upload a file (no need to make pseudo folders for the remote file, i.e. /uploads/1/image.jpg)
	uploadfile: function(localfile,remotefile){
		var container = this.container;
		// Let us get size of file
		var fs = this.fs;
		var stats = fs.statSync(localfile);

		if(!stats){
			return false;
		}

		var filesize = stats['size'];
		var client = this.client;


		var options = {
			container: container,
			remote: remotefile,
			size: filesize
		}
		var readStream = fs.createReadStream(localfile);
		var writeStream = client.upload({
			container: container,
			remote: remotefile
		});
		readStream.pipe(writeStream);
		return writeStream; // This will return an emitter .on('success',function(file)) or .on('error',function(err))
	},
	// This is just a delete file function with no return on error
	deletefile: function(remotefile){
		var container = this.container;
		var client = this.client;
		client.removeFile(container, remotefile, function(err, result) { });
	}
}

module.exports = rackspace;