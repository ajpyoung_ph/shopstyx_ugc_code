var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res) {
  var client = req.rackspace;
  var result = client.uploadfile('/Users/clintcanada/Documents/test.txt','/onetwo/test.txt');
  result.on('success',function(file){
  	console.info('uploaded file');
  	res.send("Uploaded File");
  });
  result.on('error',function(err){
  	console.info(JSON.stringify(err));
  	res.send("Didn't upload file");
  })
});

router.get('/delete',function(req, res){
	var client = req.rackspace;
	client.deletefile('/onetwo/test.txt');
	res.send('file deleted');
})

module.exports = router;
