// inital setup
var pmx = require('pmx');
//Global Exception Logging
pmx.init();
var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var logger = require('morgan'); //logger
GLOBAL.data_integrity = require(__dirname+'/server/lib/background_checks/recheck_unprocessed_Data');
var events = require('events'),
	util = require('util');
var nodemailer = require('nodemailer');

// create reusable transporter object using SMTP transport
// var transporter = nodemailer.createTransport({
//     service: 'Gmail',
//     auth: {
//         user: 'andrewjohnyoung@gmail.com',
//         pass: 'k!#g0fk!#g$'
//     }
// });
var transporter = nodemailer.createTransport();
var probe = pmx.probe();
 
var shutdown_counter = probe.counter({
  name : 'transaction shutdown'
});
	
var startAPI = function()
{
	//var minify = require('express-minify');
	//var multer = require('multer'); //handes multi-part forms
	var mysqlStuff = require('./server/lib/mysql_connects/mysql_convertions');
	var sqlConnect = require(__dirname + '/server/lib/mysql_connects/db_conf');
	//var compress = require('compression');
	GLOBAL.router = express.Router();
	var port = process.env.PORT || 8766;
	var mongojs = require('mongojs');
	GLOBAL.saved_images_location = '/images';//"/images/:shop_id/:user_id/:filename"
	// configuration ===============================================================
	process.env.NODE_ENV=process.env.NODE_ENV.trim();

	if(process.env.NODE_ENV=='development'){ //development or local machine
		GLOBAL.locationString="localhost";//transactionUser:s#0pst!cks@ :27017
		GLOBAL.mongodb = mongojs('transactionUser:s#0pst!cks@'+GLOBAL.locationString+'/shopstyx_transactions', ['transactionRecords','disputeRecords']);
		//GLOBAL.mongodb = mongojs('transactionUser:s#0pst!cks@104.130.206.240:27017,104.130.162.34:27017,104.239.135.106:27017/shopstyx_transactions?replicaSet=styx01', ['transactionRecords','disputeRecords']);
		//app.use(compress({threshold: 256}));
		//app.use(minify({cache: __dirname + '/cache'}));
		app.use(express.static(__dirname + '/public')); 		// set the static files location /public/img will be /img for users
		app.use(logger('combined')); 						// log every request to the console
		app.use(bodyParser.json()); 							// parse application/json
		app.use(bodyParser.urlencoded({extended:true}));		// parse application/x-www-form-urlencoded
		// app.use(multer({ dest: './uploads/',
		// 	rename: function (fieldname, filename) {
		// 		return filename.replace(/\W+/g, '');//.toLowerCase() + Date.now()
		// 	} 
	 //  	}));				// parse multipart/form-data
		var db_init = {
			host     : 'localhost',
			user     : 'root',
			password : '',
			database : ''
		};
	}else if(process.env.NODE_ENV=='localdev'){
		GLOBAL.locationString="localhost";
		//mongodb config
		GLOBAL.http = "http://"+GLOBAL.locationString+":"+port;
		
		GLOBAL.mongodb = mongojs('transactionUser:s#0pst!cks@'+GLOBAL.locationString+'/shopstyx_transactions', ['transactionRecords','disputeRecords']);

		//app.use(compress({threshold: 256}));
		//app.use(minify({cache: __dirname + '/cache'}));
		app.use(express.static(__dirname + '/public')); 		// set the static files location /public/img will be /img for users
		app.use(logger('combined')); 						// log every request to the console
		app.use(bodyParser.json()); 							// parse application/json
		app.use(bodyParser.urlencoded({extended:true}));		// parse application/x-www-form-urlencoded
		// app.use(multer({ dest: './uploads/',
		// 	rename: function (fieldname, filename) {
		// 		return filename.replace(/\W+/g, '');//.toLowerCase() + Date.now()
		// 	}
		//  	}));				// parse multipart/form-data
		var db_init = {
			host     : 'localhost',
			user     : 'root',
			password : '3##@#u3!',
			database : ''
		};
	}else if(process.env.NODE_ENV=='staging'){
		//app.use(compress({threshold: 256}));
		GLOBAL.locationString="localhost";
		//mongodb config
		GLOBAL.http = "http://"+GLOBAL.locationString+":"+port;
		GLOBAL.mongodb = mongojs('transactionUser:s#0pst!cks@'+GLOBAL.locationString+'/shopstyx_transactions', ['transactionRecords','disputeRecords'], {authMechanism: 'ScramSHA1'});
		

		//app.use(minify({cache: __dirname + '/cache'}));
		app.use(express.static(__dirname + '/public')); 		// set the static files location /public/img will be /img for users
		app.use(logger('combined')); 								// log every request to the console - default settings
		app.use(bodyParser.json()); 							// parse application/json
		app.use(bodyParser.urlencoded({extended:true}));		// parse application/x-www-form-urlencoded
		// app.use(multer({ dest: './uploads/',
		// 	rename: function (fieldname, filename) {
		// 		//return filename.replace(/\W+/g, '-').toLowerCase() + Date.now()
		// 		return filename.replace(/\W+/g, '');//.toLowerCase() + Date.now()
		// 	} 
	 //  	}));				// parse multipart/form-data
		var db_init = {
			host     : 'localhost',
			user     : 'mymugzy',
			password : 'mymugzy2009',
			database : ''
		};
	}else{ //test or production machine
		console.log("setting up production")
		GLOBAL.locationString="23.253.32.153";
		GLOBAL.mongodb = mongojs('transactionUser:s#0pst!cks@104.130.206.240:27017,104.130.162.34:27017,104.239.135.106:27017/shopstyx_transactions?replicaSet=styx01', ['transactionRecords','disputeRecords'], {authMechanism: 'ScramSHA1'});
		//GLOBAL.http = "http://localhost:"+port;
		//app.use(minify({cache: __dirname + '/cache'}));
		app.use(express.static(__dirname + '/public')); 		// set the static files location /public/img will be /img for users
		app.use(logger('combined')); 						// log every request to the console
		app.use(bodyParser.json()); 							// parse application/json
		app.use(bodyParser.urlencoded({extended:true}));		// parse application/x-www-form-urlencoded
		// app.use(multer({ dest: './uploads/',
		// 	rename: function (fieldname, filename) {
		// 		//return filename.replace(/\W+/g, '-').toLowerCase() + Date.now()
		// 		return filename.replace(/\W+/g, '');//.toLowerCase() + Date.now()
		// 	} 
	 //  	}));				// parse multipart/form-data
		// var db_init = {
		// 	host     : '44fe862ed36373986601eac2094f12ad2d6c3c8c.rackspaceclouddb.com',
		// 	user     : 'mymugzy',
		// 	password : 'mymugzy!2009',
		// 	database : ''
		// };
		var db_init = {
			host     : '104.239.220.252',
			user     : 'mymugzy',
			password : 'mymugzy!2009',
			database : ''
		};
	} 
	
	
	GLOBAL.sendEmail = function(msg)
	{
		var message = msg;
		var dataStruct = {
			"new_unix_timestamp":'',
			"new_simple_timestamp":''
		};
		dataStruct.new_simple_timestamp = mysqlStuff.MySQLDateTimeNOW2(dataStruct);
		if(typeof(msg)=='object')
		{
			msg['date']=[];
			msg['date_simple']=[];
			msg.date=dataStruct.new_unix_timestamp;
			msg.date_simple=dataStruct.new_simple_timestamp;
			var err = new Error(JSON.stringify(msg));
   			pmx.notify(err);
			message = JSON.stringify(msg);
		}
		console.log(util.inspect(msg,{showHidden: false, depth: null}));
		var mailOptions = {
		    from: 'nodeservices@shopstyx.com', // sender address
		    to: 'andrewjohnyoung@gmail.com', // list of receivers
		    subject: 'Transaction API Status Error', // Subject line
		    text: message // plaintext body
		    //html: '<b>Hello world ✔</b>' // html body
		};

		// send mail with defined transport object
		transporter.sendMail(mailOptions, function(error, info){
		    if(error){
		        console.log(error);
		    }else{
		        console.log('Message sent: ' + info.response);
		    }
		});
	};
	GLOBAL.sendError = function(msg,res)
	{
		if(msg.status=="Error" && msg.message!="400 page not found")
		{
			GLOBAL.sendEmail(msg);
		}
		console.log(util.inspect(msg,{showHidden: false, depth: null}));
		res.json(msg);
	};
	sqlConnect.mysqlConnect(db_init);
	//set the template engine added something
	//app.engine('.html',require('ejs').__express);
	//set the template engine's template folder
	//app.set('views',__dirname+'/public');
	//automatically set the extensions for all server side res.renders
	//app.set('view engine','html');

	//load the routes
	require('./server/routes')(GLOBAL.router);


	//CORs Registration
	//configure CORs
	app.all('*', function(req, res, next) {
		// res.header("Access-Control-Allow-Origin", "*");
		// res.header("Access-Control-Allow-Headers", "X-Requested-With");
		// next();
		res.header('Access-Control-Allow-Origin', '*');
	    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
	    res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');

	    // intercept OPTIONS method
	    if ('OPTIONS' == req.method) {
	      res.send(200);
	    }
	    else {
	      next();
	    }
	});
	//graceful reload for PM2 manager
	function close_all_connections(){
		GLOBAL.mongodb.close();
		GLOBAL.db_cs_common.end();
	}
	app.on('message', function(msg) {  
	  if (msg === 'shutdown') {
	  	shutdown_counter.inc();
	    close_all_connections();
	    //delete_logs();
	    GLOBAL.server._connections=0;
	    GLOBAL.server.close(function(){
	    	console.log("closing server");
	    });
	    process.exit(0);
	  }
	});
	
	// REGISTER OUR ROUTES -------------------------------
	// all of our routes will be from root /
	if(process.env.NODE_ENV.toLowerCase()=='development')
	{
		app.use('/', GLOBAL.router);
	}else{
		app.use('/api/transaction/', GLOBAL.router);
	}
	// Add the error middleware at the end (after route declaration)
    app.use(pmx.expressErrorHandler());
	// app.use(function(err, req, res, next){
	// 	msg = {
	// 		status:"Error",
	// 		message:"400 page not found",
	// 		description: err.stack
	// 	};
	// 	GLOBAL.sendError(msg,res);
	//   // console.error(err.stack);
	//   // res.status(500).json('Something broke!');
	// });
	//start our server
	GLOBAL.server = app.listen(port);
	console.log('starting server at port '+port);
	console.log(process.env.NODE_ENV+' mode running');
	GLOBAL.checker_running = false;
	GLOBAL.data_integrity.check_unprocessed_data();
};

module.exports = {
	startAPI:startAPI
};