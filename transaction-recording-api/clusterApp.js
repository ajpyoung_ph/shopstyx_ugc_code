// inital setup

var startup = require(__dirname+'/commonApp');

var cluster = require('cluster');
var mysqlTime = require(__dirname+'/server/lib/mysql_connects/mysql_convertions');

if (cluster.isMaster) {

  console.log("Master pid: " + process.pid);

  var numberOfRequests = 0;

  var numCPUs = require('os').cpus().length;
  for (var i = 0; i < numCPUs; i++) {
    cluster.fork();
  }

  Object.keys(cluster.workers).forEach(function(id) {
    console.log('creating process with id = ' + cluster.workers[id].process.pid);

    //getting message
    cluster.workers[id].on('message', function messageHandler(msg) {
      if (msg.cmd && msg.cmd == 'notifyRequest') {
        numberOfRequests += 1;
      }
      console.log(mysqlTime.MySQLDateTimeNOW());
      console.log("Getting message from process : ", msg.procId);
    });

    //Getting worker online
    cluster.workers[id].on('online', function online()
    {
      console.log(mysqlTime.MySQLDateTimeNOW());
      console.log("Worker pid: " + cluster.workers[id].process.pid + " is online");
    });

    //printing the listening port
    cluster.workers[id].on('listening', function online(address)
    {
      console.log(mysqlTime.MySQLDateTimeNOW());
      console.log("Listening on port + " , address.port);
    });

    //Catching errors
    cluster.workers[id].on('exit', function(code, signal) {
      if( signal ) {
        console.log(mysqlTime.MySQLDateTimeNOW());
        console.log("worker was killed by signal: "+signal);
      } else if( code !== 0 ) {
        console.log(mysqlTime.MySQLDateTimeNOW());
        console.log("worker exited with error code: "+code);
      } else {
        console.log(mysqlTime.MySQLDateTimeNOW());
        console.log("worker success!");
      }
      cluster.fork(); //create a new worker once a worker exits
    });
  });

  //Printing number of requests
  // setInterval(function(){
  //   console.log("Handled " + numberOfRequests + " requests");
  // }, 3000);

} else {

  startup.startAPI();
  // process.on('uncaughtException', function(){
  //     console.log(err);
  //     //Send some notification about the error  
  //     process.exit(1);
  // });
}