var uuid = require('node-uuid');

var latest_shopstyx_order_id = function(dataStruct)
{
	//requirement user_id
	var user_id = dataStruct.user_id;
	var uuid1 = uuid.v1();
	return user_id+"-"+uuid1;
}

var latest_merchant_shopstyx_transaction_id = function(dataStruct)
{
	//requirement user_id
	//shop_id
	//shop_user_id
	var uuid1 = uuid.v1();
	return dataStruct.shop_id+"-"+dataStruct.shop_user_id+"-"+dataStruct.user_id+"-"+uuid1;
}

var deleteMe = function(variableRep,sessionVar)
{
	try{
		delete variableRep[sessionVar];
	}catch(err){
		//do nothing here
	}
	//console.log(GLOBAL.testvar[session_id]);
	//console.log(variableRep[sessionVar]);
	//delete variableRep[sessionVar];
	//console.log(variableRep[sessionVar]);
	//console.log(GLOBAL.testvar[session_id]);
	//console.log(GLOBAL.testvar[session_id2]);
}
module.exports = {
	latest_shopstyx_order_id:latest_shopstyx_order_id,
	latest_merchant_shopstyx_transaction_id:latest_merchant_shopstyx_transaction_id,
	deleteMe:deleteMe
}