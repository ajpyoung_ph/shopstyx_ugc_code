var mysql_string_cnv = require('../mysql_connects/mysql_convertions');
var retry_max = 9;
// var events = require('events');
// var emitter = new events.EventEmitter();
var scheduler = require('node-schedule');
var util = require('util');
var mongojs = require('mongojs');
var idGen = require('../generateLatest/idGenerator');

GLOBAL.processTracker = [];
var checkTOTALTracker = function(doc,processType)
{
	var shopstyx_order_id=doc.transaction_info.shopstyx_order_id;
	
	//try access GLOBAL.processTracker[shopstyx_order_id]
	if(typeof(GLOBAL.processTracker[shopstyx_order_id])=='undefined')
	{
		GLOBAL.processTracker[shopstyx_order_id]=[];
		setTimeout(idGen.deleteMe, 10000,GLOBAL.processTracker,shopstyx_order_id);
	}
	if(typeof(GLOBAL.processTracker[shopstyx_order_id][processType])=='undefined')
	{
		GLOBAL.processTracker[shopstyx_order_id][processType]=[];
	}
	GLOBAL.processTracker[shopstyx_order_id][processType]=true;
	var true_counter = 0;
	var max_counter = 2;//the number of processes that will call this event
	for(var x = 1; x < max_counter+1; x++)
	{
		try{
			var flag=false;
			if(GLOBAL.processTracker[shopstyx_order_id][x]==true)
			{
				true_counter++;
			}
			if(true_counter>=max_counter)
			{
				flag=true;
				try{
					delete GLOBAL.processTracker[shopstyx_order_id];
				}catch(err){
					//do nothing here
				}
				//delete GLOBAL.processTracker[shopstyx_order_id];
			}
		}catch(err){
			//no GLOBAL.processTracker[x] found
			flag=false;
			console.log(x+" is missing for "+shopstyx_order_id);
		}
	}
	if(flag)
	{
		//update process_marker = 3
		var query = {"transaction_info.shopstyx_order_id":shopstyx_order_id}
		GLOBAL.mongodb.transactionRecords.update(query,{'$set': {"process_marker":3} },{upsert:true},function(err,doc){
				if(err==null)
				{
					GLOBAL.checker_running = false;
					check_unprocessed_data();
				}else{
					msg = {
						status:"Background Process Error",
						message:"Error Processing Transaction Data",
						description:err
					};
					console.log(util.inspect(msg,{showHidden: false, depth: null}));
					GLOBAL.checker_running = false;
					//die loop for debugging
				}
		});
	}
	
}

var check_unprocessed_data = function()
{
	// if(GLOBAL.checker_running == false)
	// {
		try{
			GLOBAL.checker_running = true;
			//query single dataset with process_marker=1
			var query = { 
				$or:[
					{process_marker:1}
					,{process_marker:2}
				],
				"transaction_status":{$nin:["Deleted","deleted","DELETED"]}
			};
			GLOBAL.mongodb.transactionRecords.find(query,function(err, docs){
				if(err==null)
				{
					if(docs.length>0)
					{
						//process data

						process_Data(docs);
					}else{
						msg={
							status:"Background Process Done",
							message: "No more records found with process_marker=1",
							description: "Ending Background Process Loop"
						};
						console.log(msg);
						//console.log(docs);
						GLOBAL.checker_running = false;
					}
				}else{
					GLOBAL.checker_running = false;
					msg = {
						status:"Error",
						message:"[check_unprocessed_data] Error Processing Transaction Data",
						description:err
					};
					console.log(util.inspect(msg,{showHidden: false, depth: null}));
				}
			});
		}catch(err){
			msg = {
				status:"Error",
				message:"[check_unprocessed_data] Error Processing Transaction Data",
				description:err
			};
			console.log(util.inspect(msg,{showHidden: false, depth: null}));
			GLOBAL.checker_running = false;
		}
	// }else{
	// 	msg = {
	// 		status:"Background Process single thread already running",
	// 		message:"check_unprocessed_data instance already running",
	// 		description:"limiting background checking to one thread only"
	// 	};
	// 	console.log(msg);
	// }
}

var check_unprocessed_data_single = function(id)
{
	// if(GLOBAL.checker_running == false)
	// {
		try{
			var mongojs = require('mongojs');
			var objID = mongojs.ObjectId(id.toString());
			GLOBAL.checker_running = true;
			//query single dataset with process_marker=1
			// var query = { 
			// 	$or:[
			// 		{process_marker:1}
			// 		,{process_marker:2}
			// 	]
			// };
			var query = {
				_id:objID
			};
			GLOBAL.mongodb.transactionRecords.findOne(query,function(err, doc){
				if(err==null)
				{
					if(doc!=null)
					{
						var docs = [];
						docs.push(doc);
						//process data
						process_Data(docs);
					}else{
						msg={
							status:"Background Process Done",
							message: "finished processing single document",
							description: "Ending Processing of document"
						};
						console.log(msg);
						//console.log(doc);
						GLOBAL.checker_running = false;
					}
				}else{
					GLOBAL.checker_running = false;
					msg = {
						status:"Error",
						message:"[check_unprocessed_data_single] Error Processing Transaction Data",
						description:err
					};
					console.log(util.inspect(msg,{showHidden: false, depth: null}));
				}
			});
		}catch(err){
			msg = {
				status:"Error",
				message:"[check_unprocessed_data_single] Error Processing Transaction Data",
				description:err
			};
			console.log(util.inspect(msg,{showHidden: false, depth: null}));
			GLOBAL.checker_running = false;
		}
	// }else{
	// 	msg = {
	// 		status:"Background Process single thread already running",
	// 		message:"check_unprocessed_data instance already running",
	// 		description:"limiting background checking to one thread only"
	// 	};
	// 	console.log(msg);
	// }
}
var process_Data = function(docs)
{
	//console.log("called again");
	/*
		cs_merchant_customers
		cs_merchant_promoters
		cs_merchant_promoters_products
	*/
	//set process_marker = 2
	for(var x = 0;x<docs.length;x++)
	{
		if(typeof(docs[x]._id) != 'undefined')
		{
			delete docs[x]._id;
		}
		var query = docs[x];
		GLOBAL.mongodb.transactionRecords.update(query,{'$set': {"process_marker":2} },{upsert:true},function(err,doc){
			if(err==null){
				query.process_marker=2;
				setImmediate(breakdown_to_cs_merchant_customers,query,0);
				//setImmediate(breakdown_to_cs_merchant_promoters,query,0);
				//setImmediate(breakdown_to_cs_merchant_promoters_products,query,0);
				setImmediate(breakdown_to_cs_users_orders,query,0);
				//setImmediate(generate_transactionID_to_cs_merchant_order_ids,query,0);
			}else{
				msg = {
					status:"Error",
					message:"[process_Data] Error Processing Transaction Data",
					description:err
				};
				console.log(util.inspect(msg,{showHidden: false, depth: null}));
				GLOBAL.checker_running = false;
			}
		});
	}
}

var breakdown_to_cs_merchant_customers = function(doc,retry)
{

	try{
		//breakdown docs
		var customerId = doc.user_id;
		
		doc.transaction_info.bought_items.forEach(function(merchantsInfo){
			var merchant_data = {
				merchant_id : merchantsInfo.store_user_id,
				merchants_store_id : merchantsInfo.store_id
			};
			var query = "SELECT EXISTS(SELECT 1 FROM cs_merchant_customers WHERE merchant_id="+merchant_data.merchant_id+" AND store_id="+merchant_data.merchants_store_id+" AND customer_id="+customerId+");";
			GLOBAL.db_cs_common.query(query,function(err,rows, fields){
				if(err==null)
				{
					if(rows[0][fields[0].name]>0)
					{
						//already exists
						var nowDate = mysql_string_cnv.MySQLDateTimeNOW();
						query = "UPDATE cs_merchant_customers SET date_last_ordered='"+nowDate+"' WHERE merchant_id="+merchant_data.merchant_id+" AND store_id="+merchant_data.merchants_store_id+" AND customer_id="+customerId+";";
						GLOBAL.db_cs_common.query(query,function(err,rows,fields){
							if(err==null)
							{
								//emitter.emit('checkTOTALTracker',doc,1);
								checkTOTALTracker(doc,1);
							}else{
								//check_mysql_err(err,breakdown_to_cs_merchant_customers(doc,retry));
								check_mysql_err(err,doc,retry,query,breakdown_to_cs_merchant_customers);
								// msg = {
								// 	status:"MySQL ERROR",
								// 	message:"Error Executing MySQL query",
								// 	description:err,
								// 	query:query
								// };
								// console.log(msg);
							}
						});
					}else{
						//not exist and INSERT record
						var nowDate = mysql_string_cnv.MySQLDateTimeNOW();
						query = "INSERT INTO cs_merchant_customers (merchant_id,store_id,customer_id, date_added, date_last_ordered) VALUES ("+merchant_data.merchant_id+","+merchant_data.merchants_store_id+","+customerId+",'"+nowDate+"',"+doc.transaction_info.purchase_date+");";
						GLOBAL.db_cs_common.query(query,function(err,rows,fields){
							if(err==null)
							{
								//insert ok
								//emitter.emit('checkTOTALTracker',doc,1);
								checkTOTALTracker(doc,1);
							}else{
								//check_mysql_err(err,breakdown_to_cs_merchant_customers(doc,retry));
								check_mysql_err(err,doc,retry,query,breakdown_to_cs_merchant_customers);
								// msg = {
								// 	status:"MySQL ERROR",
								// 	message:"Error Executing MySQL query",
								// 	description:err,
								// 	query:query
								// };
								// console.log(msg);
							}
						});
					}
				}else{
					console.log("error found "+query);
					// check_mysql_err(err,breakdown_to_cs_merchant_customers(doc,retry));
					check_mysql_err(err,doc,retry,query,breakdown_to_cs_merchant_customers);
					// msg = {
					// 	status:"MySQL ERROR",
					// 	message:"Error Executing MySQL query",
					// 	description:err,
					// 	query:query
					// };
					// console.log(msg);
				}
			});	
		});
		
	}catch(err){
		msg = {
			status:"Background Process Error",
			message:"[breakdown_to_cs_merchant_customers] Error Processing Transaction Data",
			description:err
		};
		GLOBAL.sendEmail(msg);
		console.log(util.inspect(msg,{showHidden: false, depth: null}));
		GLOBAL.checker_running = false;
	}
	
}
var breakdown_to_cs_merchant_promoters = function(doc,retry)
{
	try{
		var customerId = doc.user_id;
		doc.transaction_info.bought_items.forEach(function(merchantsInfo){
			var merchant_data = {
				merchant_id : merchantsInfo.store_user_id,
				merchants_store_id : merchantsInfo.store_id
			};
			merchantsInfo.item_purchased.forEach(function(purchasedItem){
				var item_id = purchasedItem.product_id;
				purchasedItem.endorsers.forEach(function(itemEndorsers){
					var promoterId = itemEndorsers.user_id;
					if(promoterId == '' || promoterId == null)
					{
						promoterId=customerId;
					}
					//check if endorser already exists
					var query = "SELECT EXISTS(SELECT 1 FROM cs_merchant_promoters WHERE merchant_id="+merchant_data.merchant_id+" AND store_id="+merchant_data.merchants_store_id+" AND promoter_id="+promoterId+");";
					GLOBAL.db_cs_common.query(query,function(err,rows,fields){
						if(err==null)
						{
							if(rows[0][fields[0].name]>0)
							{
								//record already exists
								//emitter.emit('checkTOTALTracker',doc,2);
								checkTOTALTracker(doc,2);
							}else{
								//not exist and INSERT record
								//var nowDate = mysql_string_cnv.MySQLDateTimeNOW();
								// `cs_merchant_promoters`.`merchant_id`,
								// `cs_merchant_promoters`.`store_id`,
								// `cs_merchant_promoters`.`promoter_id`,
								// `cs_merchant_promoters`.`total_earned`,
								// `cs_merchant_promoters`.`total_product`,
								// `cs_merchant_promoters`.`date_latest_promoted`
								query = "INSERT INTO cs_merchant_promoters (merchant_id,store_id,promoter_id) VALUES ("+merchant_data.merchant_id+","+merchant_data.merchants_store_id+","+promoterId+");";
								GLOBAL.db_cs_common.query(query,function(err,rows,fields){
										if(err==null)
										{
											//emitter.emit('checkTOTALTracker',doc,2);
											checkTOTALTracker(doc,2);
										}else{
											// check_mysql_err(err,breakdown_to_cs_merchant_promoters(doc,retry));
											check_mysql_err(err,doc,retry,query,breakdown_to_cs_merchant_promoters);
											// msg = {
											// 	status:"MySQL ERROR",
											// 	message:"Error Executing MySQL query",
											// 	description:err,
											// 	query:query
											// };
											// console.log(msg);
										}
									});
							}
						}else{
							// check_mysql_err(err,breakdown_to_cs_merchant_promoters(doc,retry));
							check_mysql_err(err,doc,retry,query,breakdown_to_cs_merchant_promoters);
							// msg = {
							// 	status:"MySQL ERROR",
							// 	message:"Error Executing MySQL query",
							// 	description:err,
							// 	query:query
							// };
							// console.log(msg);
						}
					});
				});
			});
		});
	}catch(err){
		msg = {
			status:"Background Process Error",
			message:"[breakdown_to_cs_merchant_promoters] Error Processing Transaction Data",
			description:err
		};
		GLOBAL.sendEmail(msg);
		console.log(util.inspect(msg,{showHidden: false, depth: null}));
		GLOBAL.checker_running = false;
	}
}
var breakdown_to_cs_merchant_promoters_products = function(doc,retry)
{
	// `cs_merchant_promoters_products`.`merchant_id`,
	// `cs_merchant_promoters_products`.`product_store_id`,
	// `cs_merchant_promoters_products`.`product_id`,
	// `cs_merchant_promoters_products`.`promoter_id`,
	// `cs_merchant_promoters_products`.`last_date_earned`,
	// `cs_merchant_promoters_products`.`total_sales`,
	// `cs_merchant_promoters_products`.`total_earned`
    try{
    	var customerId = doc.user_id;
		doc.transaction_info.bought_items.forEach(function(merchantsInfo){
			var merchant_data = {
				merchant_id : merchantsInfo.store_user_id,
				merchants_store_id : merchantsInfo.store_id
			};
			merchantsInfo.item_purchased.forEach(function(purchasedItem){
				var item_id = purchasedItem.product_id;
				purchasedItem.endorsers.forEach(function(itemEndorsers){
					var promoterId = itemEndorsers.user_id;
					//find if merchant_id, product_store_id, product_id, and promoter_id combination already exists 
					if(promoterId==null || promoterId=='')
					{
						promoterId=customerId;
					}
					var query = "SELECT EXISTS(SELECT 1 FROM cs_merchant_promoters_products WHERE merchant_id="+merchant_data.merchant_id+" AND product_store_id="+merchant_data.merchants_store_id+" AND product_id="+item_id+" AND promoter_id="+promoterId+");";
					GLOBAL.db_cs_common.query(query,function(err,rows,fields){
						if(err==null)
						{
							if(rows[0][fields[0].name]>0)
							{
								//already exists
								//emitter.emit('checkTOTALTracker',doc,3);
								checkTOTALTracker(doc,3);
							}else{
								//insert
								query = "INSERT INTO cs_merchant_promoters_products (merchant_id,product_store_id,product_id,promoter_id) VALUES ("+merchant_data.merchant_id+","+merchant_data.merchants_store_id+","+item_id+","+promoterId+");";
								GLOBAL.db_cs_common.query(query,function(err,rows,fields){
										if(err==null)
										{
											//emitter.emit('checkTOTALTracker',doc,3);
											checkTOTALTracker(doc,3);
										}else{
											//check_mysql_err(err,breakdown_to_cs_merchant_promoters_products(doc,retry));
											check_mysql_err(err,doc,retry,query,breakdown_to_cs_merchant_promoters_products);
											// msg = {
											// 	status:"MySQL ERROR",
											// 	message:"Error Executing MySQL query",
											// 	description:err,
											// 	query:query
											// };
											// console.log(msg);
										}
									});
							}
						}else{
							// check_mysql_err(err,breakdown_to_cs_merchant_promoters_products(doc,retry));
							check_mysql_err(err,doc,retry,query,breakdown_to_cs_merchant_promoters_products);
							// msg = {
							// 	status:"MySQL ERROR",
							// 	message:"Error Executing MySQL query",
							// 	description:err,
							// 	query:query
							// };
							// console.log(msg);
						}
					});
				});
			});
		});
    }catch(err){
    	msg = {
			status:"Background Process Error",
			message:"[breakdown_to_cs_merchant_promoters_products] Error Processing Transaction Data",
			description:err
		};
		GLOBAL.sendEmail(msg);
		console.log(util.inspect(msg,{showHidden: false, depth: null}));
		GLOBAL.checker_running = false;
    }
}
var breakdown_to_cs_users_orders = function(doc,retry)
{
	try{
		var total_items = doc.transaction_info.bought_items.length;
		var counter = 0;
		doc.transaction_info.bought_items.forEach(function(bought_items){
			/*
			`cs_users_orders`.`user_id`,
			transaction_info
		    `cs_users_orders`.`shopstyx_order_id`,
		    `cs_users_orders`.`shopstyx_order_number`,
		    `cs_users_orders`.`purchase_date`,
		    `cs_users_orders`.`purchase_date_simple`,
		    `cs_users_orders`.`payment_type`,
			transaction_info.bought_items
		    `cs_users_orders`.`store_id`,
		    `cs_users_orders`.`store_user_id`,
		    `cs_users_orders`.`store_claimed_by`,
		    `cs_users_orders`.`shopstyx_transaction_id`,
		    `cs_users_orders`.`shopstyx_transaction_number`,
		    `cs_users_orders`.`per_merchant_total_order_price`,
		    `cs_users_orders`.`per_merchant_total_product_price`,
		    `cs_users_orders`.`per_merchant_total_shipping`,
		    `cs_users_orders`.`per_merchant_total_tax`,
		    `cs_users_orders`.`per_merchant_total_shopstyx_fee`,
		    `cs_users_orders`.`per_merchant_total_endorser_earning`
			*/
			var user_id = doc.user_id;

			var shopstyx_order_id = doc.transaction_info.shopstyx_order_id;
			var shopstyx_order_number = doc.transaction_info.shopstyx_order_number;
			var purchase_date = doc.transaction_info.purchase_date;
			var purchase_date_simple = doc.transaction_info.purchase_date_simple;
			var payment_type = doc.transaction_info.payment_type;

			var store_id = bought_items.store_id;
			var store_user_id = bought_items.store_user_id;
			var store_claimed_by = bought_items.store_claimed_by;
			var shopstyx_transaction_id = bought_items.shopstyx_transaction_id;
			var shopstyx_transaction_number = parseInt(bought_items.shopstyx_transaction_number);
			var overall_transaction_status = bought_items.overall_transaction_status;
			//var shopstyx_transaction_number = bought_items.shopstyx_transaction_number;
			var per_merchant_total_order_price = bought_items.per_merchant_total_order_price;
			var per_merchant_total_product_price = bought_items.per_merchant_total_product_price;
			var per_merchant_total_shipping = bought_items.per_merchant_total_shipping;
			var per_merchant_total_tax = bought_items.per_merchant_total_tax;
			var per_merchant_total_shopstyx_fee = bought_items.per_merchant_total_shopstyx_fee;
			var per_merchant_total_endorser_earning = bought_items.per_merchant_total_endorser_earning;
			var readytoship = parseInt(bought_items.readytoship.readytoship_days);
			var readytoshiptext = bought_items.readytoship.readytoshiptext;

			var findquery = "SELECT * FROM cs_users_orders WHERE user_id="+user_id+" AND shopstyx_order_id=\""+shopstyx_order_id+"\" AND shopstyx_order_number="+shopstyx_order_number+" AND purchase_date="+purchase_date+" AND purchase_date_simple=\""+purchase_date_simple+"\" AND payment_type=\""+payment_type+"\" AND store_id="+store_id+" AND store_user_id="+store_user_id+" AND store_claimed_by="+store_claimed_by+" AND shopstyx_transaction_id=\""+shopstyx_transaction_id+"\" AND overall_transaction_status=\""+overall_transaction_status+"\" AND per_merchant_total_order_price="+per_merchant_total_order_price+" AND per_merchant_total_product_price="+per_merchant_total_product_price+" AND per_merchant_total_shipping="+per_merchant_total_shipping+" AND per_merchant_total_tax="+per_merchant_total_tax+" AND per_merchant_total_shopstyx_fee="+per_merchant_total_shopstyx_fee+" AND per_merchant_total_endorser_earning="+per_merchant_total_endorser_earning+" AND readytoship="+readytoship+" AND readytoshiptext='"+readytoshiptext+"';";

			GLOBAL.db_cs_common.query(findquery,function(err,rows,fields){
				if(err==null)
				{
					if(rows.length>0)
					{
						counter = counter + 1;
						if(counter >= total_items)
						{
							//emitter.emit('checkTOTALTracker',doc,4);
							checkTOTALTracker(doc,2);
						}
					}else{
						var query = "INSERT INTO cs_users_orders (user_id,shopstyx_order_id,shopstyx_order_number,purchase_date,purchase_date_simple,payment_type,store_id,store_user_id,store_claimed_by,shopstyx_transaction_id,shopstyx_transaction_number,overall_transaction_status,per_merchant_total_order_price,per_merchant_total_product_price,per_merchant_total_shipping,per_merchant_total_tax,per_merchant_total_shopstyx_fee,per_merchant_total_endorser_earning,readytoship,readytoshiptext) VALUES ("+user_id+",\""+shopstyx_order_id+"\","+shopstyx_order_number+","+purchase_date+",\""+purchase_date_simple+"\",\""+payment_type+"\","+store_id+","+store_user_id+","+store_claimed_by+",\""+shopstyx_transaction_id+"\","+shopstyx_transaction_number+",\""+overall_transaction_status+"\","+per_merchant_total_order_price+","+per_merchant_total_product_price+","+per_merchant_total_shipping+","+per_merchant_total_tax+","+per_merchant_total_shopstyx_fee+","+per_merchant_total_endorser_earning+","+readytoship+",'"+readytoshiptext+"');";

						GLOBAL.db_cs_common.query(query,function(err,rows,fields){
							if(err==null)
							{
								counter = counter +1;
								if(counter >= total_items)
								{
									//emitter.emit('checkTOTALTracker',doc,4);
									//checkTOTALTracker(doc,4);
									checkTOTALTracker(doc,2);
								}			
							}else{
								//do nothing but add to counter
								counter = counter + 1;
								if(counter >= total_items)
								{
									//emitter.emit('checkTOTALTracker',doc,4);
									//checkTOTALTracker(doc,4);
									checkTOTALTracker(doc,2);
								}
								msg = {
									status:"Background Process Error",
									message:"[breakdown_to_cs_users_orders] Error Processing Transaction Data",
									description:err,
									rows:rows,
									fields:fields,
									query:query
								};
								GLOBAL.sendEmail(msg);
								console.log(util.inspect(msg,{showHidden: false, depth: null}));
								GLOBAL.checker_running = false;
							}
						});
					}
				}else{
					counter = counter + 1;
					if(counter >= total_items)
					{
						//emitter.emit('checkTOTALTracker',doc,4);
						//checkTOTALTracker(doc,4);
						checkTOTALTracker(doc,2);
					}
					msg = {
						status:"Background Process Error",
						message:"[breakdown_to_cs_users_orders] Error Finding Transaction Data",
						description:err,
						rows:rows,
						fields:fields,
						query:findquery
					};
					GLOBAL.sendEmail(msg);
					console.log(util.inspect(msg,{showHidden: false, depth: null}));
					GLOBAL.checker_running = false;
				}
			});
		});

		
	}catch(err){
		msg = {
			status:"Background Process Error",
			message:"[breakdown_to_cs_users_orders] Error Processing Transaction Data",
			description:err
		};
		GLOBAL.sendEmail(msg);
		console.log(util.inspect(msg,{showHidden: false, depth: null}));
		GLOBAL.checker_running = false;
	}
}
var generate_transactionID_to_cs_merchant_order_ids = function(doc,retry)
{
	try{
		var total_items = doc.transaction_info.bought_items.length;
		var counter=0;
		for(var x=0;x<doc.transaction_info.bought_items.length;x++)
		{
			executeProcess(x,total_items,counter,doc);
		}
	}catch(err){
		msg = {
			status:"Background Process Error",
			message:"[generate_transactionID_to_cs_merchant_order_ids] Error Processing Transaction Data",
			description:err
		};
		GLOBAL.sendEmail(msg);
		console.log(util.inspect(msg,{showHidden: false, depth: null}));
		GLOBAL.checker_running = false;
	}
}
function executeProcess(x,total_items,counter,doc)
{
	try{
		var query = "SELECT * FROM cs_merchant_order_ids WHERE shopstyx_transaction_id='"+doc.transaction_info.bought_items[x].shopstyx_transaction_id+"' LIMIT 1;";
		GLOBAL.db_cs_common.query(query,function(err,rows,fields){
			if(err==null)
			{
				if(rows.length>0)
				{
					//get the shopstyx_transaction_id
					if(typeof(doc.transaction_info.bought_items[x].shopstyx_transaction_id)!='undefined')
					{
						console.log(x+"||"+total_items);
						doc.transaction_info.bought_items[x].shopstyx_transaction_number = [];
						doc.transaction_info.bought_items[x].shopstyx_transaction_number = rows[0]["id"];
						if(x==total_items-1)
						{
							console.log("saved saveID");
							saveID(doc);
							//checkTOTALTracker(doc,3);
						}
					}
					
				}else{
					if(typeof(doc.transaction_info.bought_items[x].shopstyx_transaction_id)!='undefined')
					{
						console.log(x+"||"+total_items);
						var query2 = "INSERT INTO `cs_merchant_order_ids` SET shopstyx_transaction_id = '"+doc.transaction_info.bought_items[x].shopstyx_transaction_id+"';";
						GLOBAL.db_cs_common.query(query2,function(err,rows,fields){
							if(err==null)
							{
								
								var order_id = parseInt(rows.insertId);
								doc.transaction_info.bought_items[x].shopstyx_transaction_number = [];
								doc.transaction_info.bought_items[x].shopstyx_transaction_number = order_id;
								if(x==total_items-1)
								{
									console.log("saved saveID");
									saveID(doc);
									//checkTOTALTracker(doc,3);
								}
							}else{
								var desc = {
									err:err,
									rows:rows,
									fields:fields,
									query:query
								};
								var msg = {
									status:"Background Process Error",
									message:"[executeIDGeneration] Error INSERTING executeIDGeneration",
									description:desc
								};
								GLOBAL.sendEmail(msg);
								console.log(util.inspect(msg,{showHidden: false, depth: null}));
								GLOBAL.checker_running = false;
							}
						});
					}else{
						console.log(x+"||"+total_items);
					}
				}
			}else{
				var desc = {
					err:err,
					rows:rows,
					fields:fields,
					query:query
				};
				var msg = {
					status:"Background Process Error",
					message:"[executeIDGeneration] Error Executing executeIDGeneration",
					description:desc
				};
				GLOBAL.sendEmail(msg);
				console.log(util.inspect(msg,{showHidden: false, depth: null}));
				GLOBAL.checker_running = false;
			}
		});
	}catch(err){
		msg = {
			status:"Background Process Error",
			message:"[executeProcess] Error Processing Transaction Data",
			description:err
		};
		GLOBAL.sendEmail(msg);
		console.log(util.inspect(msg,{showHidden: false, depth: null}));
		GLOBAL.checker_running = false;
	}
	
}

function saveID(doc)
{
	try{
		var shopstyx_order_id=doc.transaction_info.shopstyx_order_id;
		if(typeof(doc._id)!='undefined')
		{
			var query = {"_id":mongojs.ObjectId((doc._id).toString())};
			delete doc._id;
		}else{
			var query = {"transaction_info.shopstyx_order_id":shopstyx_order_id};	
		}
		
		GLOBAL.mongodb.transactionRecords.update(query,{'$set': doc },{upsert:true},function(err,doc2){
				if(err==null)
				{
					checkTOTALTracker(doc,3);
				}else{
					msg = {
						status:"Background Process Error",
						message:"[saveID]Error Updating Transaction Data",
						description:err
					};
					console.log(util.inspect(msg,{showHidden: false, depth: null}));
					GLOBAL.checker_running = false;
					//die loop for debugging
				}
		});
	}catch(err){
		msg = {
			status:"Background Process Error",
			message:"[saveID] Error Processing Transaction Data",
			description:err
		};
		GLOBAL.sendEmail(msg);
		console.log(util.inspect(msg,{showHidden: false, depth: null}));
		GLOBAL.checker_running = false;
	}
}
var check_mysql_err = function(err,doc,retry,query,cronjob_exec)
{
	// var caller = arguments.callee.caller.name;
	// console.log("caller :"+caller);
	try{
		var switchVar = err.code;
		console.log(switchVar);
		switch(switchVar.toUpperCase())
		{
			case 'PROTOCOL_CONNECTION_LOST':
				//retry again 15 minutes from now
				var oldDateObj = new Date();
				var diff = 15; //15 minutes
				var newDateObj = new Date(oldDateObj.getTime() + diff*60000);
				var newCron = scheduler.scheduleJob(newDateObj,cronjob_exec);
				break;
			case 'ER_PARSE_ERROR':
			default:
				//log error to STD_OUt
				msg = {
					status:"Background Process Error",
					message:"[check_mysql_err] Error Processing Transaction Data",
					description:err,
					query:query
				};
				//email to andrew and clint
				GLOBAL.sendEmail(msg);
				console.log(util.inspect(msg,{showHidden: false, depth: null}));
				GLOBAL.checker_running = false;
				break;
		}
	}catch(error){
		msg = {
			status:"Background Process Error",
			message:"[check_mysql_err] Error Processing Transaction Data",
			description:err,
			query:query,
			secondary_description:error
		};
		//email to andrew and clint
		GLOBAL.sendEmail(msg);
		console.log(util.inspect(msg,{showHidden: false, depth: null}));
		GLOBAL.checker_running = false;
	}
	
}
module.exports = {
	check_unprocessed_data:check_unprocessed_data,
	check_unprocessed_data_single:check_unprocessed_data_single
}