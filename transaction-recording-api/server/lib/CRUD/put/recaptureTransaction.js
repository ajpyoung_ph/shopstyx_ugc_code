var ship_single_product = function(dataStruct)
{
	try{
		var res = dataStruct.res;
		var req = dataStruct.req;
		var dataJSON = req.body.data;
		if(typeof(dataJSON)=="string")
		{
			dataJSON = eval("("+dataJSON+")");
		}
		var serverDate = (new Date()).getTime() / 1000;
		var query = {
				'transaction_info.shopstyx_order_id':req.params.shopstyx_order_id, 
				'user_id':dataStruct.user_id,
				'transaction_info.bought_items.store_id':parseInt(req.params.store_id),
				'transaction_info.bought_items.shopstyx_transaction_id':req.params.shopstyx_transaction_id,
				'transaction_info.bought_items.store_user_id':parseInt(req.params.store_user_id),
				"transaction_status":{$nin:["Deleted","deleted","DELETED"]}
			};
		//console.log(query);
		GLOBAL.mongodb.transactionRecords.find(query,function(err,doc){
				if(err==null)
				{
					if(doc.length>0)
					{
						//console.log(doc);
						//parse through transaction_info.bought_items
						//var newJSON = find_bought_items_index(doc,dataJSON,dataStruct);
						var targetProductJSON = find_bought_items_index(doc,dataJSON,dataStruct);						

						//modify document and remove old item purchased
						GLOBAL.mongodb.transactionRecords.update(query,{
							$currentDate:{
								"transaction_info.bought_items.$.date_modified":{ $type: "timestamp" }
							},$push:{
								"transaction_info.bought_items.$.payment_transaction_recapture_history":dataJSON
							},$pull:{"transaction_info.bought_items.$.item_purchased":targetProductJSON}
						},function(err,doc2){
							if(err==null){
								// add "new" product information
								var newDate = (new Date()).getTime() / 1000;
								var modifiedProductJSON = targetProductJSON;
								modifiedProductJSON.order_status.status_state="Shipped";
								modifiedProductJSON.order_status.order_cancelled_date = newDate;
								GLOBAL.mongodb.transactionRecords.update(query,{
									$push:{
										"transaction_info.bought_items.$.item_purchased":modifiedProductJSON
									}
								},function(err,doc3){
									if(err==null)
									{
										msg = {
											status:"Success",
											message:"Information Saved"
										};
										GLOBAL.sendError(msg,res);
									}else{
										msg = {
											status:"Error",
											message:"[ship_single_product] Error Processing Update Data",
											description:err
										}
										GLOBAL.sendError(msg,dataStruct.res);
									}
								});
							}else{
								msg = {
									status:"Error",
									message:"[ship_single_product] Error Processing Update Data",
									description:err
								}
								GLOBAL.sendError(msg,dataStruct.res);
							}
						});
					}else{
						msg = {
							status:"Error",
							message:"[ship_single_product] Data not found in system",
							description: "Cannot Cancel Order that does not exist in the system"
						};
						GLOBAL.sendError(msg,res);
					}
					
				}else{
					msg = {
						status:"Error",
						message:"[ship_single_product] Error Processing Update Data",
						description:err
					}
					GLOBAL.sendError(msg,dataStruct.res);
				}
			});
	}catch(err){
		msg = {
			status:"Error",
			message:"[ship_single_product] Error Processing Update Data",
			description:err
		}
		GLOBAL.sendError(msg,dataStruct.res);
	}
}


var updateRecapture = function(dataStruct)
{
	try{
		var res = dataStruct.res;
		var req = dataStruct.req;
		var dataJSON = req.body.data;
		if(typeof(dataJSON)=="string")
		{
			dataJSON = eval("("+dataJSON+")");
		}
		var serverDate = (new Date()).getTime() / 1000;
		var query = {
				'transaction_info.shopstyx_order_id':req.params.shopstyx_order_id, 
				'user_id':dataStruct.user_id,
				'transaction_info.bought_items.store_id':parseInt(req.params.store_id),
				'transaction_info.bought_items.shopstyx_transaction_id':req.params.shopstyx_transaction_id,
				'transaction_info.bought_items.store_user_id':parseInt(req.params.store_user_id),
				"transaction_status":{$nin:["Deleted","deleted","DELETED"]}
			};
		//console.log(query);
		GLOBAL.mongodb.transactionRecords.find(query,function(err,doc){
				if(err==null)
				{
					if(doc.length>0)
					{
						//console.log(doc);
						//parse through transaction_info.bought_items
						//var newJSON = find_bought_items_index(doc,dataJSON,dataStruct);
						var targetProductJSON = find_bought_items_index(doc,dataJSON,dataStruct);						

						//modify document and remove old item purchased
						GLOBAL.mongodb.transactionRecords.update(query,{
							$currentDate:{
								"transaction_info.bought_items.$.date_modified":{ $type: "timestamp" }
							},$push:{
								"transaction_info.bought_items.$.payment_transaction_recapture_history":dataJSON
							},$pull:{"transaction_info.bought_items.$.item_purchased":targetProductJSON}
						},function(err,doc2){
							if(err==null){
								// add "new" product information
								var newDate = (new Date()).getTime() / 1000;
								var modifiedProductJSON = targetProductJSON;
								modifiedProductJSON.order_status.status_state="Cancelled";
								modifiedProductJSON.order_status.order_cancelled_date = newDate;
								GLOBAL.mongodb.transactionRecords.update(query,{
									$push:{
										"transaction_info.bought_items.$.item_purchased":modifiedProductJSON
									}
								},function(err,doc3){
									if(err==null)
									{
										msg = {
											status:"Success",
											message:"Information Saved"
										};
										GLOBAL.sendError(msg,res);
									}else{
										msg = {
											status:"Error",
											message:"[updateRecapture] Error Processing Update Data",
											description:err
										}
										GLOBAL.sendError(msg,dataStruct.res);
									}
								});
							}else{
								msg = {
									status:"Error",
									message:"[updateRecapture] Error Processing Update Data",
									description:err
								}
								GLOBAL.sendError(msg,dataStruct.res);
							}
						});
					}else{
						msg = {
							status:"Error",
							message:"[updateRecapture] Data not found in system",
							description: "Cannot Cancel Order that does not exist in the system"
						};
						GLOBAL.sendError(msg,res);
					}
					
				}else{
					msg = {
						status:"Error",
						message:"[updateRecapture] Error Processing Update Data",
						description:err
					}
					GLOBAL.sendError(msg,dataStruct.res);
				}
			});
	}catch(err){
		msg = {
			status:"Error",
			message:"[updateRecapture] Error Processing Update Data",
			description:err
		}
		GLOBAL.sendError(msg,dataStruct.res);
	}
}

var cancel_all_orders_by_merchant = function(dataStruct)
{
	/*
	/transaction/cancel/order/all/<token>/<user_id>/<shopstyx_order_id>/<store_id>/<store_user_id>/<shopstyx_transaction_id>/
	*/
	try{
		var res = dataStruct.res;
		var req = dataStruct.req;
		var serverDate = (new Date()).getTime() / 1000;
		var query = {
				'transaction_info.shopstyx_order_id':req.params.shopstyx_order_id, 
				'user_id':dataStruct.user_id,
				'transaction_info.bought_items.store_id':parseInt(req.params.store_id),
				'transaction_info.bought_items.shopstyx_transaction_id':req.params.shopstyx_transaction_id,
				'transaction_info.bought_items.store_user_id':parseInt(req.params.store_user_id),
				"transaction_status":{$nin:["Deleted","deleted","DELETED"]}
			};
		//console.log(query);
		GLOBAL.mongodb.transactionRecords.find(query,function(err,doc){
			if(err==null)
			{
				//console.log(doc);
				//parse through transaction_info.bought_items
				var oldDoc = doc;
				var newItemsPurchasedJSON = cancel_entire_order_by_merchant(doc,dataStruct);
				GLOBAL.mongodb.transactionRecords.update(query,{
							$currentDate:{
								"transaction_info.bought_items.$.date_modified":{ $type: "timestamp" }
							},
							$set:{
								"transaction_info.bought_items.$.item_purchased":newItemsPurchasedJSON
							}
						},function(err,doc){
						if(err==null){
							msg = {
								status:"Success",
								message:"Orders Cancelled",
								data:newItemsPurchasedJSON
							};
							GLOBAL.sendError(msg,res);
						}else{
							msg = {
								status:"Error",
								message:"[cancel_all_orders_by_merchant] Error Cancelling Order",
								description:err
							}
							GLOBAL.sendError(msg,dataStruct.res);
						}
				});
			}else{
				msg = {
					status:"Error",
					message:"[cancel_all_orders_by_merchant] Error Cancelling Order",
					description:err
				}
				GLOBAL.sendError(msg,dataStruct.res);
			}
		});
	}catch(err){
		msg = {
			status:"Error",
			message:"[cancel_all_orders_by_merchant] Error Cancelling Order",
			description:err
		}
		GLOBAL.sendError(msg,dataStruct.res);
	}
	
}

var cancel_all_orders_by_shopstyx_order_id = function(dataStruct)
{
	/*/:token/:user_id/transaction/cancel/order/all/:shopstyx_order_id/*/
	try{
		var res = dataStruct.res;
		var req = dataStruct.req;
		var serverDate = (new Date()).getTime() / 1000;
		var query = {
				'transaction_info.shopstyx_order_id':req.params.shopstyx_order_id, 
				'user_id':dataStruct.user_id,
				"transaction_status":{$nin:["Deleted","deleted","DELETED"]}
			};
		//console.log(query);
		GLOBAL.mongodb.transactionRecords.find(query,function(err,doc){
			if(err==null)
			{
				//console.log(doc);
				if(doc.length>0)
				{
					var newBoughtItemsJSON = cancel_entire_order_by_shopstyx_order_id(doc,dataStruct);
					GLOBAL.mongodb.transactionRecords.update(query,{
							$set:{
								"transaction_info.bought_items":newBoughtItemsJSON
							}
						},function(err,doc){
							if(err==null){
								msg = {
									status:"Success",
									message:"Orders Cancelled",
									data:newBoughtItemsJSON
								};
								GLOBAL.sendError(msg,res);
							}else{
								msg = {
									status:"Error",
									message:"[cancel_all_orders_by_shopstyx_order_id] Error Processing Order Cancel",
									description:err
								}
								GLOBAL.sendError(msg,dataStruct.res);
							}
						});
				}else{
					msg = {
						status:"Error",
						message:"[cancel_all_orders_by_shopstyx_order_id] Data not found in system",
						description: "Cannot Cancel Order that does not exist in the system"
					};
					GLOBAL.sendError(msg,res);
				}
			}else{
				msg = {
					status:"Error",
					message:"[cancel_all_orders_by_shopstyx_order_id] Error Processing Order Cancel",
					description:err
				}
				GLOBAL.sendError(msg,dataStruct.res);
			}
		});
	}catch(err){
		msg = {
			status:"Error",
			message:"[cancel_all_orders_by_shopstyx_order_id] Error Processing Order Cancel",
			description:err
		}
		GLOBAL.sendError(msg,dataStruct.res);
	}
}

function find_bought_items_index(doc,dataJSON,dataStruct)
{
	try{
		//find new recap
		var res = dataStruct.res;
		var req = dataStruct.req;
		var orders = doc[0].transaction_info.bought_items;
		var orderIndex = 0;
		for(var x = 0; x<orders.length;x++)
		{
			if( orders[x].store_id == parseInt(req.params.store_id) &&
				orders[x].shopstyx_transaction_id == req.params.shopstyx_transaction_id &&
				orders[x].store_user_id == parseInt(req.params.store_user_id))
			{
				orderIndex = x;
				//orders[x].payment_transaction_recapture_history.push(dataJSON);
				break;
			}

		}
		//modify date_modified
		var newDate = (new Date()).getTime() / 1000;
		//orders[orderIndex].date_modified=newDate;
		//modify status
		var items = orders[orderIndex].item_purchased;
		var itemIndex = 0;
		for(var y = 0;y<items.length;y++)
		{
			if(items[y].product_id == parseInt(req.params.product_id))
			{
				itemIndex=y;
				break;
				// items[y].order_status.status_state="Cancelled";
				// items[y].order_status.order_cancelled_date = newDate;
			}
		}
		
		//console.log(items[y]);
		return items[y];
	}catch(err){
		msg = {
			status:"Error",
			message:"[find_bought_items_index] Error Processing Update Data",
			description:err
		}
		GLOBAL.sendError(msg,dataStruct.res);
	}
}

function cancel_entire_order_by_merchant(doc,dataStruct)
{
	try{
		var res = dataStruct.res;
		var req = dataStruct.req;
		var orders = doc[0].transaction_info.bought_items;
		var orderIndex = 0;
		for(var x = 0; x<orders.length;x++)
		{
			if( orders[x].store_id == parseInt(req.params.store_id) &&
				orders[x].shopstyx_transaction_id == req.params.shopstyx_transaction_id &&
				orders[x].store_user_id == parseInt(req.params.store_user_id))
			{
				orderIndex = x;
				//orders[x].payment_transaction_recapture_history.push(dataJSON);
				break;
			}

		}
		//modify date_modified
		var newDate = (new Date()).getTime() / 1000;
		//orders[orderIndex].date_modified=newDate;
		//modify status
		var items = orders[orderIndex].item_purchased;
		var itemIndex = 0;
		for(var y = 0;y<items.length;y++)
		{
			items[y].order_status.status_state="Cancelled";
			items[y].order_status.order_cancelled_date = newDate;
		}
		
		//console.log(items);
		return items;
	}catch(err){
		msg = {
			status:"Error",
			message:"[cancel_entire_order_by_merchant] Error Processing Update Data",
			description:err
		}
		GLOBAL.sendError(msg,dataStruct.res);
	}
}

function cancel_entire_order_by_shopstyx_order_id(doc,dataStruct)
{
	try{
		var res = dataStruct.res;
		var req = dataStruct.req;
		var orders = doc[0].transaction_info.bought_items;
		var orderIndex = 0;
		//modify date_modified
		var newDate = (new Date()).getTime() / 1000;
		for(var x = 0; x<orders.length;x++)
		{
			//modify status
			// var items = orders[x].item_purchased;
			// var itemIndex = 0;
			orders[x].date_modified=newDate;
			for(var y = 0;y<orders[x].item_purchased.length;y++)
			{
				orders[x].item_purchased[y].order_status.status_state="Cancelled";
				orders[x].item_purchased[y].order_status.order_cancelled_date = newDate;
			}
		}
		//console.log(orders);
		return orders;
	}catch(err){
		msg = {
			status:"Error",
			message:"[cancel_entire_order_by_shopstyx_order_id] Error Processing Update Data",
			description:err
		}
		GLOBAL.sendError(msg,dataStruct.res);
	}
}
module.exports = {
	updateRecapture:updateRecapture,
	cancel_all_orders_by_merchant:cancel_all_orders_by_merchant,
	cancel_all_orders_by_shopstyx_order_id:cancel_all_orders_by_shopstyx_order_id,
	ship_single_product:ship_single_product
}