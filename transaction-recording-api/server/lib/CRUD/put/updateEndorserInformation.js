var setPaid = function(dataStruct)
{
	try{
		//:endorser_user_id/:shopstyx_transaction_id/:product_id
		var res = dataStruct.res;
		var req = dataStruct.req;
		var query = {
			"transaction_info.bought_items.shopstyx_transaction_id":req.params.shopstyx_transaction_id,
			"transaction_info.bought_items.item_purchased.product_id":parseInt(req.params.product_id),
			"transaction_info.bought_items.item_purchased.endorsers.user_id":parseInt(req.params.endorser_user_id),
			"transaction_status":{$nin:["Deleted","deleted","DELETED"]}
		};
		//console.log(query);
		GLOBAL.mongodb.transactionRecords.find(query,function(err,doc){
			if(err==null)
			{
				if(doc.length>0)
				{
					set_Single_Endorser_Payment_True(query,doc,dataStruct);
				}else{
					msg = {
						status:"Error",
						message:"[setPaid] Data not found in system",
						description: "Data is non-existent in our Database"
					};
					GLOBAL.sendError(msg,res);
				}
			}else{
				msg = {
					status:"Error",
					message:"[setPaid] Error Processing Data",
					description: err
				};
				GLOBAL.sendError(msg,res);
			}
		});
	}catch(err){
		msg = {
			status:"Error",
			message:"[setPaid] Error Processing Data",
			description: err
		};
		GLOBAL.sendError(msg,res);
	}
}
function set_Single_Endorser_Payment_True(query,doc,dataStruct)
{
	try
	{
		var data = doc[0];
		var res = dataStruct.res;
		var req = dataStruct.req;
		//
		var item_purchased_index = 0;
		var endorsers_index = 0;
		var bought_items_index = 0;
		var bought_items = data.transaction_info.bought_items;
		
		var breakme=false;
		//find item_purchased_index and endorsers_index
		for(var x = 0;x<bought_items.length;x++)
		{
			bought_items_index=x;
			var item_purchased = bought_items[x].item_purchased;
			for(var y = 0;y<item_purchased.length;y++)
			{
				var endorsers = item_purchased[y].endorsers;
				if(item_purchased[y].product_id==req.params.product_id)
				{
					item_purchased_index=y;
					for(var z = 0;z<endorsers.length;z++)
					{
						if(endorsers[z].user_id==req.params.endorser_user_id)
						{
							endorsers_index=z;
							breakme=true;
							break;
						}
						if(breakme)
						{
							break;
						}
					}
				}
				if(breakme)
				{
					break;
				}
			}
			if(breakme)
			{
				break;
			}
		}
		if(breakme)
		{
			//setup $set to modify paid_status to true
			var setModifier = { $set: {} };
			setModifier.$set['transaction_info.bought_items.'+bought_items_index+'.item_purchased.'+item_purchased_index+'.endorsers.'+endorsers_index+'.paid_status'] = true;
			//console.log(query);
			//console.log(setModifier);
			GLOBAL.mongodb.transactionRecords.update(query, setModifier, function(err,doc){
				if(err==null)
				{
					//console.log(doc);
					msg = {
						status:"Success",
						message:"Payout Successfully updated"
					};
					GLOBAL.sendError(msg,res);
				}else{
					msg = {
						status:"Error",
						message:"[set_Single_Endorser_Payment_True] Error Processing Data",
						description: err
					};
					GLOBAL.sendError(msg,res);
				}
			});
		}else{
			msg = {
				status:"Error",
				message:"[set_Single_Endorser_Payment_True] Error Finding Data to update",
				description: "Parameters does not coincide with a valid data in the system"
			};
			GLOBAL.sendError(msg,res);
		}
	}catch(err){
		msg = {
			status:"Error",
			message:"[set_Single_Endorser_Payment_True] Error Processing Data",
			description: err
		};
		GLOBAL.sendError(msg,res);
	}
}
module.exports = {
	setPaid:setPaid
}