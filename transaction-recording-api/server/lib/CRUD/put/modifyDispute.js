var util = require('util');
var mongojs = require('mongojs');

var modifyItem_purchased = function(dataStruct)
{
	try{
		var res = dataStruct.res;
		var req = dataStruct.req;
		var dataJSON = req.body.data;

		var msg = {
			status:"Success",
			message:"[modifyItem_purchased] Update Recieved"
		};
		GLOBAL.sendError(msg,dataStruct.res);

		//console.log("checking dataJSON");
		//console.log(dataJSON);
		if(typeof(dataJSON)=="string")
		{
			dataJSON = eval("("+dataJSON+")");
		}

		
		var maxValue = dataJSON.item_purchased.length;
		for(var x=0;x<dataJSON.item_purchased.length;x++)
		{
			if(typeof(dataJSON.item_purchased[x]['date_modified'])=='undefined')
			{
				dataJSON.item_purchased[x]['date_modified']=[];
			}
			if(typeof(dataJSON.item_purchased[x]['date_modified_simple'])=='undefined')
			{
				dataJSON.item_purchased[x]['date_modified_simple']=[];
			}
			dataJSON.item_purchased[x].date_modified=dataStruct.new_unix_timestamp;
			dataJSON.item_purchased[x].date_modified_simple=dataStruct.new_simple_timestamp;
			
			var query = {
				"$and":[
					{"shopstyx_transaction_id":req.params.shopstyx_transaction_id},
					{
						"item_purchased":{
							"$elemMatch":{
								"$and":[
									{"product_id":dataJSON.item_purchased[x]['product_id']},
									{"variant_id":dataJSON.item_purchased[x]['variant_id']}
								]
							}
						}
					}
				]
			};
			var setVal = '{';
			var setBody ='';
			var varContainer = dataJSON.item_purchased[x];
			var product_id = dataJSON.item_purchased[x]['product_id'];
			var variant_id = dataJSON.item_purchased[x]['variant_id'];
			if(typeof(dataJSON.item_purchased[x].files)=='undefined')
			{
				dataJSON.item_purchased[x]['files']=[];
			}
			var fileLength = dataJSON.item_purchased[x].files.length;
			delete varContainer.product_id;
			delete varContainer.variant_id;

			for (var keyName in varContainer) {
		        //alert(' name=' + keyName + ' value=' + varContainer[keyName]);
		        if(isNaN(varContainer[keyName]))
		        {
		        	if(typeof(varContainer[keyName])!='object')
		        	{
		        		setBody = setBody + '"item_purchased.\$.'+keyName+'":"'+varContainer[keyName]+'",';
		        		//setBody = setBody + '"item_purchased.'+y+'.'+keyName+'":"'+varContainer[keyName]+'",';
		        	}else{
		        		//setBody = setBody + '"item_purchased.\$.'+keyName+'":'+JSON.stringify(varContainer[keyName])+',';
		        		//setBody = setBody + '"item_purchased.'+y+'.'+keyName+'":'+JSON.stringify(varContainer[keyName])+',';
		        	}
		        	
		        }else{
		        	setBody = setBody + '"item_purchased.\$.'+keyName+'":'+varContainer[keyName]+',';
		        	//setBody = setBody + '"item_purchased.'+y+'.'+keyName+'":'+varContainer[keyName]+',';
		        }
		    }
		    setVal = setVal + setBody + '}';
		    //console.log("String Set => "+set);
		    setVal = eval("("+setVal+")");
			dataJSON.item_purchased[x]['product_id']=product_id;
			dataJSON.item_purchased[x]['variant_id']=variant_id;
			if(fileLength==0)
			{
				dataJSON.item_purchased[x].files=[];
			}

			GLOBAL.mongodb.disputeRecords.update(query,{$set:setVal,$addToSet:{"item_purchased.$.files":{$each:dataJSON.item_purchased[x].files}}},function(err,docs){
				if(err!=null)
				{
					var desc = {
						err:err,
						docs:docs,
						query:query
					};
					var msg = {
						status:"Error",
						message:"[modifyItem_purchased] Error Updating DB",
						description:err
					};
					GLOBAL.sendEmail(msg);//,dataStruct.res);
				}else{
					var desc={
						err:err,
						docs:docs,
						query:query
					};
					var msg = {
						status:"Success",
						message:"[modifyItem_purchased] Update Completed",
						description:desc
					};
					console.log(util.inspect(msg,{showHidden: false, depth: null}));
					//GLOBAL.sendEmail(msg);//,dataStruct.res);
				}
			});
			//console.log(util.inspect(setVal,{showHidden: false, depth: null}));
			//setImmediate(updateDisputeItemPurchasedDetails,query,setVal,fileLength,dataJSON.item_purchased[x],req.params.shopstyx_transaction_id,dataStruct);//,y,myDocument);


			//updateDisputeItemFindIndex(varContainer,setBody,setVal,product_id,variant_id,query,fileLength,dataJSON,x,req,dataStruct);
	        
		}

	}catch(err){
		var msg = {
			status:"Error",
			message:"[modifyItem_purchased] Error Processing Dispute Data",
			description:err
		};
		GLOBAL.sendError(msg,dataStruct.res);
	}
}

function updateDisputeItemFindIndex(varContainer,setBody,setVal,product_id,variant_id,query,fileLength,dataJSON,x,req,dataStruct)
{
	var findDocument = {
		"$and":[
			{"shopstyx_transaction_id":req.params.shopstyx_transaction_id},
			{"item_purchased.product_id":product_id},
			{"item_purchased.variant_id":variant_id}
		]
	};

	GLOBAL.mongodb.disputeRecords.find(findDocument,function(err,docs){
		if(err==null)
		{
			var myDocument = docs[0];
			for(var y=0;y<myDocument.item_purchased.length;y++)
			{
				if(	myDocument.item_purchased[y].product_id == product_id &&  
					myDocument.item_purchased[y].variant_id == variant_id )
				{
					break;
				}
			}
			for (var keyName in varContainer) {
		        //alert(' name=' + keyName + ' value=' + varContainer[keyName]);
		        if(isNaN(varContainer[keyName]))
		        {
		        	if(typeof(varContainer[keyName])!='object')
		        	{
		        		//setBody = setBody + '"item_purchased.\$.'+keyName+'":"'+varContainer[keyName]+'",';
		        		setBody = setBody + '"item_purchased.'+y+'.'+keyName+'":"'+varContainer[keyName]+'",';
		        	}else{
		        		//setBody = setBody + '"item_purchased.\$.'+keyName+'":'+JSON.stringify(varContainer[keyName])+',';
		        		setBody = setBody + '"item_purchased.'+y+'.'+keyName+'":'+JSON.stringify(varContainer[keyName])+',';
		        	}
		        	
		        }else{
		        	//setBody = setBody + '"item_purchased.\$.'+keyName+'":'+varContainer[keyName]+',';
		        	setBody = setBody + '"item_purchased.'+y+'.'+keyName+'":'+varContainer[keyName]+',';
		        }
		    }
		    setVal = setVal + setBody + '}';
		    //console.log("String Set => "+set);
		    setVal = eval("("+setVal+")");
			dataJSON.item_purchased[x]['product_id']=product_id;
			dataJSON.item_purchased[x]['variant_id']=variant_id;
			//console.log(util.inspect(setVal,{showHidden: false, depth: null}));
			setImmediate(updateDisputeItemPurchasedDetails,query,setVal,fileLength,dataJSON.item_purchased[x],req.params.shopstyx_transaction_id,dataStruct,y,myDocument);
		}else{
			var desc = {
				err:err,
				docs:docs,
				query:findDocument
			};
			var msg = {
				status:"Error",
				message:"[updateDisputeItemFindIndex] Error finding document in DB",
				description:err
			};
			GLOBAL.sendEmail(msg);
		}
	});
}

function updateDisputeItemPurchasedDetails(query,setVal,fileLength,item_purchased,shopstyx_transaction_id,dataStruct)//,y,myDocument)
{
	if(fileLength==0)
	{
		GLOBAL.mongodb.disputeRecords.update(query,{$set:setVal},function(err,docs){
			if(err!=null)
			{
				var desc = {
					err:err,
					docs:docs,
					query:query
				};
				var msg = {
					status:"Error",
					message:"[updateDisputeItemPurchasedDetails] Error Updating DB",
					description:err
				};
				GLOBAL.sendEmail(msg);//,dataStruct.res);
			}else{
				var desc={
					err:err,
					docs:docs,
					query:query
				};
				var msg = {
					status:"Success",
					message:"[updateDisputeItemPurchasedDetails] Update Completed",
					description:desc
				};
				console.log(util.inspect(msg,{showHidden: false, depth: null}));
				//GLOBAL.sendEmail(msg);//,dataStruct.res);
			}
		});
	}else{
		updateFiles(query,setVal,fileLength,item_purchased,shopstyx_transaction_id,dataStruct);//,y,myDocument);
	}
}

function updateFiles(query,setVal,fileLength,item_purchased,shopstyx_transaction_id,dataStruct,index_y_marked,myDocument)
{

	for(var x=0;x<myDocument.item_purchased.length;x++)
	{
		if((item_purchased.product_id == myDocument.item_purchased[x].product_id) && (item_purchased.variant_id == myDocument.item_purchased[x].variant_id))
		{
			if(typeof(myDocument.item_purchased[x].files)!='undefined')
			{
				for(var y=0;y<myDocument.item_purchased[x].files.length;y++)
				{
					setVal["item_purchased."+index_y_marked+".files"].push(myDocument.item_purchased[x].files[y]);
				}
			}
			break;
		}
	}

	setImmediate(updateDisputeItemPurchasedDetails,query,setVal,0,item_purchased,shopstyx_transaction_id,dataStruct,index_y_marked,myDocument)
	//var fileArray = setVal["item_purchased.$.files"];
	//find the document first
	// GLOBAL.mongodb.disputeRecords.find(query,function(err,docs){
	// 	if(err==null)
	// 	{
	// 		if(docs.length>0)
	// 		{
	// 			//var target_item_purchased = docs[0].item_purchased;
	// 			// docs[0].item_purchased.forEach(function(single_item){
	// 			// 	// {"item_purchased.product_id":dataJSON.item_purchased[x]['product_id']},
	// 			// 	// {"item_purchased.variant_id":dataJSON.item_purchased[x]['variant_id']}
	// 			// 	if((item_purchased.product_id == single_item.product_id) && (item_purchased.variant_id == single_item.variant_id))
	// 			// 	{
	// 			// 		single_item.files.forEach(function(file){
	// 			// 			setVal["item_purchased.$.files"].push(file);
	// 			// 		});
	// 			// 	}
	// 			// });
	// 			for(var x=0;x<docs[0].item_purchased.length;x++)
	// 			{
	// 				if((item_purchased.product_id == docs[0].item_purchased[x].product_id) && (item_purchased.variant_id == docs[0].item_purchased[x].variant_id))
	// 				{
	// 					if(typeof(docs[0].item_purchased[x].files)!='undefined')
	// 					{
	// 						for(var y=0;y<docs[0].item_purchased[x].files.length;y++)
	// 						{
	// 							setVal["item_purchased."+index_y_marked+".files"].push(docs[0].item_purchased[x].files[y]);
	// 						}
	// 					}
	// 					break;
	// 				}
	// 			}
	// 		}
			
	// 		setImmediate(updateDisputeItemPurchasedDetails,query,setVal,0,item_purchased,shopstyx_transaction_id,dataStruct,index_y_marked,myDocument);
	// 	}else{
	// 		var desc = {
	// 			err:err,
	// 			docs:docs,
	// 			query:query
	// 		};
	// 		var msg = {
	// 			status:"Error",
	// 			message:"[updateFiles] Error finding record to update in DB",
	// 			description:err
	// 		};
	// 		GLOBAL.sendEmail(msg);//,dataStruct.res);
	// 	}
	// });
}

var updateDisputeStatus = function (dataStruct,shopstyx_transaction_id,status)
{
	//update all "pending" to status
	var query = {
		"shopstyx_transaction_id":shopstyx_transaction_id
	}
	GLOBAL.mongodb.disputeRecords.find(query,function(err,docs){
		if(err==null)
		{
			var targetJSON = docs[0];
			for(var x=0;x<targetJSON.item_purchased.length;x++)
			{
				if((targetJSON.item_purchased[x].status).toLowerCase()=='pending')
				{
					// targetJSON.item_purchased[x].status = status;
					// targetJSON.item_purchased[x]['date_modified'] = dataStruct.new_unix_timestamp;	
					// targetJSON.item_purchased[x]['date_modified_simple'] = dataStruct.new_simple_timestamp;
					dataStruct.req.params['product_id2'] = targetJSON.item_purchased[x].product_id;
					dataStruct.req.params['variant_id2'] = targetJSON.item_purchased[x].variant_id;
					updateDisputeStatusSingle(dataStruct,shopstyx_transaction_id,status);
				}
			}
			delete targetJSON._id;
			GLOBAL.mongodb.disputeRecords.remove(query,1,function(err,docs){
				GLOBAL.mongodb.disputeRecords.insert(targetJSON);
			});
		}else{
			var desc = {
				err:err,
				docs:docs,
				query:query
			};
			var msg = {
				status:"Background Error",
				message:"[updateFiles] Error finding record to update in DB",
				description:err
			};
			GLOBAL.sendEmail(msg);
		}
	});
}
var updateDisputeStatusSingle = function (dataStruct,shopstyx_transaction_id,status)
{
	///:token/:user_id/transaction/log/update/record/order/per/merchant/per/item/status_state/dispute/:disputeStatus/:shopstyx_order_id/:shopstyx_transaction_id/:product_id/:variant_id/:is_robot/
	//update all "pending" to status
	var query = {
		"$and":[
			{"shopstyx_transaction_id":shopstyx_transaction_id},
			{
				"item_purchased":{
					"$elemMatch":{
						"$and":[
							{"product_id":parseInt(dataStruct.req.params.product_id)},
							{"variant_id":parseInt(dataStruct.req.params.variant_id)}
						]
					}
				}
			}
		]
	};
	if(typeof(dataStruct.req.params.product_id2)!='undefined')
	{
		query = {
			"$and":[
				{"shopstyx_transaction_id":shopstyx_transaction_id},
				{
					"item_purchased":{
						"$elemMatch":{
							"$and":[
								{"product_id":parseInt(dataStruct.req.params.product_id2)},
								{"variant_id":parseInt(dataStruct.req.params.variant_id2)}
							]
						}
					}
				}
			]
		};
	}
	var setVal = {
		"$set":{"item_purchased.$.status":status}
	};
	GLOBAL.mongodb.disputeRecords.update(query,setVal,function(err,docs){
		if(err!=null)
		{
			var desc = {
				err:err,
				docs:docs,
				query:query
			};
			var msg = {
				status:"Background Error",
				message:"[updateDisputeStatusSingle] Error updating record",
				description:err
			};
			GLOBAL.sendEmail(msg);
		}
	});
	// var query = {
	// 	"$and":[
	// 		{"shopstyx_transaction_id":shopstyx_transaction_id},
	// 		{"item_purchased.product_id":parseInt(dataStruct.req.params.product_id)},
	// 		{"item_purchased.variant_id":parseInt(dataStruct.req.params.variant_id)}
	// 	]
	// }
	// if(typeof(dataStruct.req.params.product_id2)!='undefined')
	// {
	// 	query = {
	// 		"$and":[
	// 			{"shopstyx_transaction_id":shopstyx_transaction_id},
	// 			{"item_purchased.product_id":parseInt(dataStruct.req.params.product_id2)},
	// 			{"item_purchased.variant_id":parseInt(dataStruct.req.params.variant_id2)}
	// 		]
	// 	}
	// }
	// GLOBAL.mongodb.disputeRecords.find(query,function(err,docs){
	// 	if(err==null)
	// 	{
	// 		// console.log("found data");
	// 		// console.log(docs);
	// 		var data = docs[0];
	// 		// console.log("data assignment");
	// 		// console.log(data);
	// 		var item_purchased = data.item_purchased;
	// 		// console.log("item_purchased");
	// 		// console.log(item_purchased);
	// 		var target_item = "";
	// 		for(var x = 0;x<item_purchased.length;x++)
	// 		{
	// 			if(typeof(dataStruct.req.params.product_id2)!='undefined')
	// 			{
	// 				if(item_purchased[x].product_id == parseInt(dataStruct.req.params.product_id2) && item_purchased[x].variant_id == parseInt(dataStruct.req.params.variant_id2))
	// 				{
	// 					console.log(item_purchased[x]);
	// 					target_item=JSON.stringify(item_purchased[x]);
	// 					break;
	// 				}
	// 			}else{
	// 				if(item_purchased[x].product_id == parseInt(dataStruct.req.params.product_id) && item_purchased[x].variant_id == parseInt(dataStruct.req.params.variant_id))
	// 				{
	// 					console.log(item_purchased[x]);
	// 					target_item=JSON.stringify(item_purchased[x]);
	// 					break;
	// 				}
	// 			}
				

	// 		}
	// 		var updateQuery = {
	// 			"_id":mongojs.ObjectId(data._id.toString())
	// 		};
	// 		var setValString = '{"item_purchased.'+x+'.status":"'+status+'"}';
	// 		var setVal = JSON.parse(setValString);

	// 		GLOBAL.mongodb.disputeRecords.update(updateQuery,{"$set":setVal},function(err,docs){
	// 			if(err!=null)
	// 			{
	// 				var desc = {
	// 					err:err,
	// 					docs:docs,
	// 					query:updateQuery
	// 				};
	// 				var msg = {
	// 					status:"Background Error",
	// 					message:"[updateDisputeStatusSingle] Error updating record",
	// 					description:err
	// 				};
	// 				GLOBAL.sendEmail(msg);
	// 			}
	// 		});
			// console.log("target_item");
			// console.log(target_item);
			// target_item=JSON.parse(target_item);
			// var pullVal = {
			// 	"$pull":{"item_purchased":target_item}
			// };
			// var pullQuery = {
			// 	"_id":mongojs.ObjectId(data._id.toString())
			// };
			// GLOBAL.mongodb.disputeRecords.update(pullQuery,pullVal);
			
			// target_item.status=status;
			// var pushVal = {
			// 	"$push":{"item_purchased":target_item}
			// };
			// var pushQuery = {
			// 	"_id":mongojs.ObjectId(data._id.toString())
			// };
			// GLOBAL.mongodb.disputeRecords.update(query,pushVal);

			// GLOBAL.mongodb.disputeRecords.update(pullQuery,pullVal,function(err,docs){
			// 	if(err==null)
			// 	{
			// 		target_item.status=status;
			// 		var pushVal = {
			// 			"$push":{"item_purchased":target_item}
			// 		};
			// 		var pushQuery = {
			// 			"_id":mongojs.ObjectId(data._id.toString())
			// 		};
			// 		GLOBAL.mongodb.disputeRecords.update(query,pushVal);
			// 	}
			// });
	// 	}else{
	// 		var desc = {
	// 			err:err,
	// 			docs:docs,
	// 			query:query
	// 		};
	// 		var msg = {
	// 			status:"Background Error",
	// 			message:"[updateDisputeStatusSingle] Error finding record to update in DB",
	// 			description:err
	// 		};
	// 		GLOBAL.sendEmail(msg);
	// 	}
	// });
}
module.exports = {
	modifyItem_purchased:modifyItem_purchased,
	updateDisputeStatus:updateDisputeStatus,
	updateDisputeStatusSingle:updateDisputeStatusSingle
}