var updatePaypal = function(dataStruct)
{
	try{
		var res = dataStruct.res;
		var req = dataStruct.req;
		var dataJSON = req.body.data;

		if(typeof(dataJSON)=="string")
		{
			dataJSON = eval("("+dataJSON+")");
		}
		var query = { 
			"transaction_info.shopstyx_order_id" : dataJSON.shopstyx_order_id 
		};

		GLOBAL.mongodb.transactionRecords.find(query,function(err,docs){
			if(err==null)
			{
				if(docs.length>0)
				{
					for(var x = 0;x<docs[0].transaction_info.bought_items.length;x++)
					{
						for(var y = 0;y<dataJSON.bought_items.length;y++)
						{
							if(docs[0].transaction_info.bought_items[x].shopstyx_transaction_id == dataJSON.bought_items[y].shopstyx_transaction_id)
							{
								if(typeof(docs[0].transaction_info.bought_items[x].payment_transaction_id)=='undefined')
								{
									docs[0].transaction_info.bought_items[x]['payment_transaction_id']=[];
								}
								if(typeof(docs[0].transaction_info.bought_items[x].payment_transaction_date)=='undefined')
								{
									docs[0].transaction_info.bought_items[x]['payment_transaction_date']=[];
								}
								docs[0].transaction_info.bought_items[x].payment_transaction_id = dataJSON.bought_items[y].payment_transaction_id;
								docs[0].transaction_info.bought_items[x].payment_transaction_date = dataJSON.bought_items[y].payment_transaction_date;
								break;
							}
						}
					}
					var bought_items = docs[0].transaction_info.bought_items;
					GLOBAL.mongodb.transactionRecords.update(query,{ $set: {'transaction_info.bought_items': bought_items} },{upsert:false},function(err,docs){
						if(err==null)
						{
							//please test if transaction_info.shopstyx_order_id is nonExistent
							msg = {
								status:"Success",
								message:"Successfully Updated Purchase Transaction ID"
							}
							GLOBAL.sendError(msg,dataStruct.res);
						}else{
							var desc = {
								err:err,
								query:query,
								find:2
							};
							msg = {
								status:"Error",
								message:"[updatePaypal] Error Processing Updating Transaction Data",
								description:err
							}
							GLOBAL.sendError(msg,dataStruct.res);
						}
					});
				}else{
					msg = {
						status:"Error",
						message:"[updatePaypal] Transaction Record not found"
					}
					GLOBAL.sendError(msg,dataStruct.res);
				}
			}else{
				var desc = {
					err:err,
					query:query,
					find:1
				};
				msg = {
					status:"Error",
					message:"[updatePaypal] Error Processing Updating Transaction Data",
					description:desc
				}
				GLOBAL.sendError(msg,dataStruct.res);
			}
		});

	}catch(err){
		msg = {
			status:"Error",
			message:"[updatePaypal] Error Processing Updating Transaction Data",
			description:err
		}
		GLOBAL.sendError(msg,dataStruct.res);
	}
}

module.exports = {
	updatePaypal:updatePaypal
}