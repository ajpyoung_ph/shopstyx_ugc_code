var util = require('util');
var mongojs = require('mongojs');
var modifyDispute = require(__dirname+"/modifyDispute");

var updateTransaction = function(dataStruct)
{
	try{
		var res = dataStruct.res;
		var req = dataStruct.req;
		var dataJSON = req.body.data;
		if(typeof(dataJSON)=="string")
		{
			dataJSON = eval("("+dataJSON+")");
		}
		var shopstyx_order_id = dataJSON.shopstyx_order_id;
		var transaction_status = (dataJSON.transaction_status).toLowerCase();

		var query = {
			"transaction_info.shopstyx_order_id":shopstyx_order_id
		};
		GLOBAL.mongodb.transactionRecords.update(query,{$set:{"transaction_status":transaction_status}},function(err,doc){
			if(err==null)
			{
				if(doc.n>0)
				{
					var msg = {
						status:"Success",
						message:"Information Saved"
					};
					GLOBAL.sendError(msg,res);
				}else{
					var desc = {
						err:err,
						doc:doc,
						query:query,
						additional_info:"Transaction Record not found"
					};
					var msg = {
						status:"Error",
						message:"[updateTransaction] Error Processing Data",
						description: desc
					};
					GLOBAL.sendError(msg,dataStruct.res);
				}
			}else{
				var desc = {
					err:err,
					doc:doc,
					query:query
				};
				var msg = {
					status:"Error",
					message:"[updateTransaction] Error Processing Data",
					description: desc
				};
				GLOBAL.sendError(msg,dataStruct.res);
			}
		});
	}catch(err){
		var msg = {
			status:"Error",
			message:"[updateTransaction] Error Processing Data",
			description: err
		};
		GLOBAL.sendError(msg,dataStruct.res);
	}
}

var updateTransactionOverallperMerchant = function(dataStruct)
{
	try{
		var res = dataStruct.res;
		var req = dataStruct.req;
		var dataJSON = req.body.data;
		if(typeof(dataJSON)=="string")
		{
			dataJSON = eval("("+dataJSON+")");
		}
		var shopstyx_order_id = dataJSON.shopstyx_order_id;
		var bought_items = dataJSON.bought_items;

		var query = {
			"transaction_info.shopstyx_order_id":shopstyx_order_id
		};
		
		//var newDate = (new Date()).getTime()/1000;
		GLOBAL.mongodb.transactionRecords.findOne(query,function(err,doc){
			if(err==null)
			{
				if(doc!=null)
				{
					var doc_id = doc._id;
					delete doc._id;
					var origDoc = doc;
					for(var x=0;x<bought_items.length;x++)
					{
						for(var y=0;y<doc.transaction_info.bought_items.length;y++)
						{
							if(bought_items[x].shopstyx_transaction_id == doc.transaction_info.bought_items[y].shopstyx_transaction_id)
							{
								doc.transaction_info.bought_items[y].date_modified = dataStruct.new_unix_timestamp;
								doc.transaction_info.bought_items[y].overall_transaction_status =bought_items[x].overall_transaction_status;
								// if(typeof(bought_items[x].date_shipped)!='undefined')
								// {
								// 	doc.transaction_info.bought_items[y]['date_shipped']=[];
								// 	doc.transaction_info.bought_items[y]['date_shipped_simple']=[];
									//doc.transaction_info.bought_items[y].date_shipped=bought_items[x].date_shipped;
								//}
								var is_robot=0;
								if(typeof(bought_items[x].is_robot_resolved)!='undefined')
								{
									doc.transaction_info.bought_items[y]['is_robot_resolved']=bought_items[x].is_robot_resolved;
									doc.transaction_info.bought_items[x].item_purchased[y].order_status['is_robot_disputed']
									is_robot=bought_items[x].is_robot_resolved;
								}
								if(typeof(bought_items[x].is_robot_refunded)!='undefined')
								{
									doc.transaction_info.bought_items[y]['is_robot_refunded']=bought_items[x].is_robot_refunded;
									is_robot=bought_items[x].is_robot_refunded;
								}
								if(typeof(bought_items[x].is_robot_completed)!='undefined')
								{
									doc.transaction_info.bought_items[y]['is_robot_completed']=bought_items[x].is_robot_completed;
									is_robot=bought_items[x].is_robot_completed;
								}
								if(typeof(bought_items[x].is_robot_void)!='undefined')
								{
									doc.transaction_info.bought_items[y]['is_robot_void']=bought_items[x].is_robot_void;
									is_robot=bought_items[x].is_robot_void;
								}
								if(typeof(bought_items[x].date_completed)!='undefined')
								{
									doc.transaction_info.bought_items[y]['date_completed']=[];
									doc.transaction_info.bought_items[y].date_completed=bought_items[x].date_completed;
								}
								//Disputed/Refunded/Resolved
								switch((bought_items[x].overall_transaction_status).toLowerCase())
								{
									case 'paymentsuccessful':
										doc.transaction_info.bought_items[y]["payment_successful_date"]=[];
										doc.transaction_info.bought_items[y]["payment_successful_date_simple"]=[];
										doc.transaction_info.bought_items[y].payment_successful_date=dataStruct.new_unix_timestamp;
										doc.transaction_info.bought_items[y].payment_successful_date_simple=dataStruct.new_simple_timestamp;
										break;
									case 'shipped':
										doc.transaction_info.bought_items[y]['date_shipped']=[];
										doc.transaction_info.bought_items[y]['date_shipped_simple']=[];
										doc.transaction_info.bought_items[y].date_shipped=dataStruct.new_unix_timestamp;
										doc.transaction_info.bought_items[y].date_shipped_simple=dataStruct.new_simple_timestamp;
										break;
									case 'disputed':
										doc.transaction_info.bought_items[y]['date_disputed']=[];
										doc.transaction_info.bought_items[y]['date_disputed_simple']=[];
										doc.transaction_info.bought_items[y].date_disputed=dataStruct.new_unix_timestamp;
										doc.transaction_info.bought_items[y].date_disputed_simple=dataStruct.new_simple_timestamp;
										break;
									case 'refunded':
										doc.transaction_info.bought_items[y]['date_refunded']=[];
										doc.transaction_info.bought_items[y]['date_refunded_simple']=[];
										doc.transaction_info.bought_items[y].date_refunded=dataStruct.new_unix_timestamp;
										doc.transaction_info.bought_items[y].date_refunded_simple=dataStruct.new_simple_timestamp;
										//modifyDispute.updateDisputeStatus(dataStruct,doc.transaction_info.bought_items[y].shopstyx_transaction_id,"Refunded");
										break;
									case 'resolved':
										doc.transaction_info.bought_items[y]['date_resolved']=[];
										doc.transaction_info.bought_items[y]['date_resolved_simple']=[];
										doc.transaction_info.bought_items[y].date_resolved=dataStruct.new_unix_timestamp;
										doc.transaction_info.bought_items[y].date_resolved_simple=dataStruct.new_simple_timestamp;
										//modifyDispute.updateDisputeStatus(dataStruct,doc.transaction_info.bought_items[y].shopstyx_transaction_id,"Resolved");
										break;
									case 'completed':
										doc.transaction_info.bought_items[y]['date_completed']=[];
										doc.transaction_info.bought_items[y]['date_completed_simple']=[];
										doc.transaction_info.bought_items[y].date_completed=dataStruct.new_unix_timestamp;
										doc.transaction_info.bought_items[y].date_completed_simple=dataStruct.new_simple_timestamp;
										update_cs_users_orders_completed(dataStruct,doc.transaction_info.bought_items[y],doc);
										break;
								}
								doc.transaction_info.bought_items[y]['date_modified']=[];
								doc.transaction_info.bought_items[y]['date_modified_simple']=[];
								doc.transaction_info.bought_items[y].date_modified=dataStruct.new_unix_timestamp;
								doc.transaction_info.bought_items[y].date_modified_simple=dataStruct.new_simple_timestamp;
								var modified=false;
								var forceBreak=false;
								var completedFlag=false;
								for(var z=0;z<doc.transaction_info.bought_items[y].item_purchased.length;z++)
								{
									
									switch((bought_items[x].overall_transaction_status).toLowerCase())
									{
										case 'paymentfailed':
											doc.transaction_info.bought_items[y]['paymentfailed_reason']=[];
											doc.transaction_info.bought_items[y].paymentfailed_reason=bought_items[x].paymentfailed_reason;
											forceBreak=true;
											break;
										case 'void':
											//check if current state is void
											if((doc.transaction_info.bought_items[y].item_purchased[z].order_status.status_state).toLowerCase()=='void')
											{
												restore_per_merchant(doc,y,z);
											}
											//void all items
											doc.transaction_info.bought_items[y].item_purchased[z].order_status['status_state']=[];
											doc.transaction_info.bought_items[y].item_purchased[z].order_status.status_state='Void';
											//should do this once lang unta
											// if(z==0)
											// {
												doc.transaction_info.bought_items[y]['voiding_reason']=[];
												doc.transaction_info.bought_items[y].voiding_reason=bought_items[x].voiding_reason;
											// }
											modified=true;
											break;
										case 'shipped':
											//shipped only pending items
											if((doc.transaction_info.bought_items[y].item_purchased[z].order_status.status_state).toLowerCase()=='pending')
											{
												doc.transaction_info.bought_items[y].item_purchased[z].order_status.status_state=bought_items[x].overall_transaction_status;
												modified=true;
											}
											//dapat do this once lang
											// if(z==0)
											// {
												doc.transaction_info.bought_items[y]['shipping_info']=[];
												doc.transaction_info.bought_items[y].shipping_info=bought_items[x].shipping_info;
											// }
											break;
										case 'disputed':
											doc.transaction_info.bought_items[y].item_purchased[z].order_status['status_state']=[];
											
											doc.transaction_info.bought_items[y].item_purchased[z].order_status.status_state=bought_items[x].overall_transaction_status;

											doc.transaction_info.bought_items[y].item_purchased[z].order_status['order_disputed_date']=[];
											doc.transaction_info.bought_items[y].item_purchased[z].order_status['order_disputed_date_simple']=[];
											
											doc.transaction_info.bought_items[y].item_purchased[z].order_status.order_disputed_date=dataStruct.new_unix_timestamp;
											doc.transaction_info.bought_items[y].item_purchased[z].order_status.order_disputed_date_simple=dataStruct.new_simple_timestamp;
											modified=true;
											break;
										case 'refunded':
											//check if current state is refunded
											if((doc.transaction_info.bought_items[y].item_purchased[z].order_status.status_state).toLowerCase()=='refunded')
											{
												restore_per_merchant(doc,y,z);
											}else{
												//mark refunded items that are not yet refunded
												doc.transaction_info.bought_items[y].item_purchased[z].order_status['order_dispute_refunded_date']=[];
												doc.transaction_info.bought_items[y].item_purchased[z].order_status['order_dispute_refunded_date_simple']=[];
												
												doc.transaction_info.bought_items[y].item_purchased[z].order_status.order_dispute_refunded_date=dataStruct.new_unix_timestamp;
												doc.transaction_info.bought_items[y].item_purchased[z].order_status.order_dispute_refunded_date_simple=dataStruct.new_simple_timestamp;
												doc.transaction_info.bought_items[y].item_purchased[z].order_status['status_state']=[];
												doc.transaction_info.bought_items[y].item_purchased[z].order_status.status_state=doc.transaction_info.bought_items[y].item_purchased[z].order_status.status_state;
												doc.transaction_info.bought_items[y].item_purchased[z].order_status['is_robot_refunded']=is_robot;
											}
											modified=true;
											break;
										case 'resolved':
											doc.transaction_info.bought_items[y].item_purchased[z].order_status['status_state']=[];
											
											doc.transaction_info.bought_items[y].item_purchased[z].order_status.status_state=bought_items[x].overall_transaction_status;

											doc.transaction_info.bought_items[y].item_purchased[z].order_status['order_resolved_date']=[];
											doc.transaction_info.bought_items[y].item_purchased[z].order_status['order_resolved_date_simple']=[];
											
											doc.transaction_info.bought_items[y].item_purchased[z].order_status.order_resolved_date=dataStruct.new_unix_timestamp;
											doc.transaction_info.bought_items[y].item_purchased[z].order_status.order_resolved_date_simple=dataStruct.new_simple_timestamp;
											doc.transaction_info.bought_items[y].item_purchased[z].order_status['is_robot_resolved']=is_robot;
											modified=true;
											break;
										case 'completed':
											//completed only shipped items
											if((doc.transaction_info.bought_items[y].item_purchased[z].order_status.status_state).toLowerCase()=='shipped')
											{
												doc.transaction_info.bought_items[y].item_purchased[z].order_status.status_state=bought_items[x].overall_transaction_status;
												doc.transaction_info.bought_items[y].item_purchased[z].order_status['is_robot_completed']=is_robot;

												for(var a=0;a<doc.transaction_info.bought_items[y].item_purchased[z].endorsers.length;a++)
												{
													doc.transaction_info.bought_items[y].item_purchased[z].endorsers[a].paid_status = true;
													doc.transaction_info.bought_items[y].item_purchased[z].endorsers[a]['date_paid'] = [];
													doc.transaction_info.bought_items[y].item_purchased[z].endorsers[a].date_paid = dataStruct.new_unix_timestamp;
													doc.transaction_info.bought_items[y].item_purchased[z].endorsers[a]['date_paid_simple'] = [];
													doc.transaction_info.bought_items[y].item_purchased[z].endorsers[a].date_paid_simple = dataStruct.new_simple_timestamp;

												}
												completedFlag=true;
												modified=true;
											}
											break;
										default:
											forceBreak=true;
											break;
									}
									if(typeof(bought_items[x].is_robot_resolved)!='undefined')
									{
										doc.transaction_info.bought_items[y].item_purchased[z].order_status['is_robot_resolved']=bought_items[x].is_robot_resolved;
										modified=true;
									}
									if(typeof(bought_items[x].is_robot_refunded)!='undefined')
									{
										doc.transaction_info.bought_items[y].item_purchased[z].order_status['is_robot_refunded']=bought_items[x].is_robot_refunded;
										modified=true;
									}
									if(typeof(bought_items[x].is_robot_completed)!='undefined')
									{
										doc.transaction_info.bought_items[y].item_purchased[z].order_status['is_robot_completed']=bought_items[x].is_robot_completed;
										modified=true;
									}
									if(typeof(bought_items[x].is_robot_void)!='undefined')
									{
										doc.transaction_info.bought_items[y].item_purchased[z].order_status['is_robot_void']=bought_items[x].is_robot_void;
										modified=true;
									}
									if(modified==true)
									{
										doc.transaction_info.bought_items[y].item_purchased[z].order_status['date_modified']=[];
										doc.transaction_info.bought_items[y].item_purchased[z].order_status.date_modified=dataStruct.new_unix_timestamp;
										doc.transaction_info.bought_items[y].item_purchased[z].order_status['date_modified_simple']=[];
										doc.transaction_info.bought_items[y].item_purchased[z].order_status.date_modified_simple=dataStruct.new_simple_timestamp;
									}
									if(forceBreak==true)
									{
										break;
									}
								}
								update_cs_merchant_customers((bought_items[x].overall_transaction_status).toLowerCase(), doc.transaction_info.bought_items[y], doc.transaction_info.bought_items[y].per_merchant_total_order_price,doc.user_id);
								break; 
							}
						}
					}
					
					GLOBAL.mongodb.transactionRecords.update(query,{$set:doc},function(err,retdoc){
						if(err==null)
						{
							if(retdoc.n>0)
							{
								var msg = {
									status:"Success",
									message:"Information Saved"
								};
								// if(completedFlag==true)
								// {
									msg['data']=[];
									msg.data.push(doc);
								// }
								GLOBAL.sendError(msg,res);
							}else{
								var desc = {
									err:err,
									doc:retdoc,
									query:origDoc,
									additional_info:"Transaction Record not updated"
								};
								var msg = {
									status:"Error",
									message:"[updateTransactionOverallperMerchant] Error Processing Data",
									description: desc
								};
								GLOBAL.sendError(msg,dataStruct.res);
							}
						}else{
							var desc = {
								err:err,
								doc:retdoc,
								query:origDoc,
								additional_info:"Transaction Record not updated"
							};
							var msg = {
								status:"Error",
								message:"[updateTransactionOverallperMerchant] Error Processing Data",
								description: desc
							};
							GLOBAL.sendError(msg,dataStruct.res);
						}
					});
					updateMySQL_overall_transaction_status(dataStruct,doc);
				}else{
					var desc = {
						err:err,
						doc:doc,
						query:query,
						additional_info:"Transaction Record not found"
					};
					var msg = {
						status:"Error",
						message:"[updateTransactionOverallperMerchant] Error Processing Data",
						description: desc
					};
					GLOBAL.sendError(msg,dataStruct.res);
				}
			}else{
				var desc = {
					err:err,
					doc:doc,
					query:query
				};
				var msg = {
					status:"Error",
					message:"[updateTransactionOverallperMerchant] Error Processing Data",
					description: desc
				};
				GLOBAL.sendError(msg,dataStruct.res);
			}
		});
	}catch(err){
		var msg = {
			status:"Error",
			message:"[updateTransactionOverallperMerchant] Error Processing Data",
			description: err
		};
		GLOBAL.sendError(msg,dataStruct.res);
	}
}

var update_cs_users_orders_completed = function(dataStruct,bought_item,dataJSON)
{
	try{
		var res = dataStruct.res;
		var req = dataStruct.req;
		var shopstyx_order_id = dataJSON.transaction_info.shopstyx_order_id;
		var shopstyx_transaction_id = bought_item.shopstyx_transaction_id;
		var query = "UPDATE cs_users_orders SET completed_date_simple='"+bought_item.date_completed_simple+"', completed_date="+bought_item.date_completed+" WHERE shopstyx_transaction_id='"+shopstyx_transaction_id+"';";
		GLOBAL.db_cs_common.query(query,function(err,rows,fields){
			if(err!=null)
			{
				errorFound=true;
				var desc = {
					err:err,
					rows:rows,
					fields:fields,
					query:query
				};
				var msg = {
					status:"[Background Process] Error",
					message:"[update_cs_users_orders_completed] Error Updating Data",
					description:desc
				};
				console.log(util.inspect(msg,{showHidden: false, depth: null}));
				GLOBAL.sendEmail(msg);
			}
		});
	}catch(err){
		var msg = {
			status:"Error",
			message:"[update_cs_users_orders_completed] Error Processing Data",
			description: err
		};
		GLOBAL.sendError(msg,dataStruct.res);
	}
}
var updateMySQL_overall_transaction_status = function(dataStruct,dataJSON)
{
	try{
		var res = dataStruct.res;
		var req = dataStruct.req;
		// var dataJSON = req.body.data;
		// if(typeof(dataJSON)=="string")
		// {
		// 	dataJSON = eval("("+dataJSON+")");
		// }
		var shopstyx_order_id = '';
		var bought_items = '';
		var is_robot=0;
		var is_robot_param=0;
		if(typeof(dataJSON.shopstyx_order_id)!='undefined')
		{
			shopstyx_order_id = dataJSON.shopstyx_order_id;
			bought_items = dataJSON.bought_items;
		}
		if(typeof(dataJSON.transaction_info.shopstyx_order_id)!='undefined')
		{
			shopstyx_order_id = dataJSON.transaction_info.shopstyx_order_id;
			bought_items = dataJSON.transaction_info.bought_items;
		}
		if(typeof(dataStruct.req.params.is_robot)!='undefined')
		{
			is_robot_param=parseInt(dataStruct.req.params.is_robot);
		}
		var errorFound = false;
		bought_items.forEach(function(bought_item){
			var shopstyx_transaction_id = bought_item.shopstyx_transaction_id;
			var overall_transaction_status = bought_item.overall_transaction_status;

			var per_merchant_total_order_price = bought_item.per_merchant_total_order_price;
		    var per_merchant_total_product_price = bought_item.per_merchant_total_product_price;
		    var per_merchant_total_shipping = bought_item.per_merchant_total_shipping;
		    var per_merchant_total_tax = bought_item.per_merchant_total_tax;
		    var per_merchant_total_shopstyx_fee = bought_item.per_merchant_total_shopstyx_fee;
		    var per_merchant_total_endorser_earning = bought_item.per_merchant_total_endorser_earning;

			var query = "UPDATE cs_users_orders SET overall_transaction_status='"+overall_transaction_status+"', per_merchant_total_order_price="+per_merchant_total_order_price+", per_merchant_total_product_price="+per_merchant_total_product_price+", per_merchant_total_shipping="+per_merchant_total_shipping+", per_merchant_total_tax="+per_merchant_total_tax+", per_merchant_total_shopstyx_fee="+per_merchant_total_shopstyx_fee+", per_merchant_total_endorser_earning="+per_merchant_total_endorser_earning;
			if(overall_transaction_status.toLowerCase()=='shipped')
			{
				query = query + ", shipping_date="+dataStruct.new_unix_timestamp+", shipping_date_simple='"+dataStruct.new_simple_timestamp+"'";
			}
			//from individual item update
			//doc.transaction_info.bought_items[x]['is_robot_resolved']
			
			//from overall API
			if(typeof(bought_item['is_robot_resolved'])!='undefined')
			{
				if(is_robot_param==0)
				{
					query = query + ", is_robot_resolved="+bought_item['is_robot_resolved'];	
				}else{
					query = query + ", is_robot_resolved="+is_robot_param;
				}
				
				is_robot=1;
			}
			if(typeof(bought_item['is_robot_refunded'])!='undefined')
			{
				if(is_robot_param==0)
				{
					query = query + ", is_robot_refunded="+bought_item['is_robot_refunded'];
				}else{
					query = query + ", is_robot_refunded="+is_robot_param;
				}
				
				is_robot=1
			}
			if(typeof(bought_item['is_robot_completed'])!='undefined')
			{
				if(is_robot_param==0)
				{
					query = query + ", is_robot_completed="+bought_item['is_robot_completed'];
				}else{
					query = query + ", is_robot_completed="+is_robot_param;
				}
				is_robot=1
			}
			if(typeof(bought_item['is_robot_void'])!='undefined')
			{
				if(is_robot_param==0)
				{
					query = query + ", is_robot_void="+bought_item['is_robot_void'];
				}else{
					query = query + ", is_robot_void="+is_robot_param;
				}
				is_robot=1
			}
			if(typeof(bought_item['payment_successful_date_simple'])!='undefined')
			{
				query = query + ", payment_successful_date="+bought_item['payment_successful_date'];
				query = query + ", payment_successful_date_simple='"+bought_item['payment_successful_date_simple']+"'";
			}
			query = query + ", robot_updated="+is_robot;

			var query = query+" WHERE shopstyx_transaction_id='"+shopstyx_transaction_id+"';";
			//GLOBAL.sendEmail("query executed : "+query);

			GLOBAL.db_cs_common.query(query,function(err,rows,fields){
				if(err!=null)
				{
					errorFound=true;
					var desc = {
						err:err,
						rows:rows,
						fields:fields,
						query:query
					};
					var msg = {
						status:"[Background Process] Error",
						message:"[updateMySQL_overall_transaction_status] Error Updating Data",
						description:desc
					};
					console.log(util.inspect(msg,{showHidden: false, depth: null}));
					GLOBAL.sendEmail(msg);
				}
			});
		});
		if(errorFound==true)
		{
			setTimeout(updateMySQL_overall_transaction_status,30000,dataStruct);
		}
		
	}catch(err){
		var msg = {
			status:"[Background Process] Error",
			message:"[updateMySQL_overall_transaction_status] Error Processing Data",
			description:err
		};
		console.log(util.inspect(msg,{showHidden: false, depth: null}));
		GLOBAL.sendEmail(msg);
	}
}
var update_cs_merchant_customers = function(status_state,bought_items,price,customer_id)
{
	try{
		//((doc.transaction_info.bought_items[y].item_purchased[z].order_status.status_state).toLowerCase(), doc.transaction_info.bought_items[y], doc.transaction_info.bought_items[y].per_merchant_total_order_price);
		var merchant_id = bought_items.store_user_id;
		var store_id = bought_items.store_id;
		switch(status_state)
		{
			case 'refunded':
			case 'void':
				var query = "UPDATE cs_merchant_customers SET `ordered_amount_cancelled` = `ordered_amount_cancelled` + "+price+", `total_cancelled_products` = `total_cancelled_products` + 1 WHERE merchant_id="+merchant_id+" AND store_id="+store_id+" AND customer_id="+customer_id+";";
				break;
			case 'completed':
				var query = "UPDATE cs_merchant_customers SET `ordered_amount_shipped` = `ordered_amount_shipped` + "+price+", `total_shipped_products` = `total_shipped_products` + 1 WHERE merchant_id="+merchant_id+" AND store_id="+store_id+" AND customer_id="+customer_id+";";
				break;
			default:
				break;
		}
		//console.log(query);
		if(typeof(query)!='undefined')
		{
			//console.log("execute query");
			GLOBAL.db_cs_common.query(query,function(err,rows,fields){
				if(err!=null)
				{
					errorFound=true;
					var desc = {
						err:err,
						rows:rows,
						fields:fields,
						query:query
					};
					var msg = {
						status:"[Background Process] Error",
						message:"[update_cs_merchant_customers] Error Updating Data",
						description:desc
					};
					console.log(util.inspect(msg,{showHidden: false, depth: null}));
					GLOBAL.sendEmail(msg);
				}
			});
		}
	}catch(err){
		var msg = {
			status:"[Background Process] Error",
			message:"[update_cs_merchant_customers] Error Processing Data",
			description:err
		};
		console.log(util.inspect(msg,{showHidden: false, depth: null}));
		GLOBAL.sendEmail(msg);
	}
}
var updatePerItemVoid = function(dataStruct)
{
	try{
		///:token/:user_id/transaction/log/update/record/order/per/merchant/per/item/status_state/void/:shopstyx_order_id/:shopstyx_transaction_id/:product_id/
		var res = dataStruct.res;
		var req = dataStruct.req;
		var shopstyx_order_id = req.params.shopstyx_order_id;
		var shopstyx_transaction_id = req.params.shopstyx_transaction_id;
		var product_id = parseInt(req.params.product_id);
		var variant_id = parseInt(req.params.variant_id);
		var query = {
			"transaction_info.shopstyx_order_id":shopstyx_order_id,
			"transaction_info.bought_items.shopstyx_transaction_id":shopstyx_transaction_id,
			"transaction_info.bought_items.item_purchased.product_id":product_id
		};
		GLOBAL.mongodb.transactionRecords.findOne(query,function(err,doc){
			if(err==null)
			{
				if(doc!=null)
				{
					var found=false;
					for(var x=0;x<doc.transaction_info.bought_items.length;x++)
					{
						if(shopstyx_transaction_id==doc.transaction_info.bought_items[x].shopstyx_transaction_id)
						{
							var VoidProductCounter = 0;
							for(var y=0;y<doc.transaction_info.bought_items[x].item_purchased.length;y++)
							{
								
								if((doc.transaction_info.bought_items[x].item_purchased[y].order_status.status_state).toLowerCase()=='void')
								{
									VoidProductCounter++;
								}
								if(product_id==doc.transaction_info.bought_items[x].item_purchased[y].product_id && variant_id==doc.transaction_info.bought_items[x].item_purchased[y].variant_id)
								{
									found=true;
									doc.transaction_info.bought_items[x].item_purchased[y].order_status.status_state='Void';
									doc.transaction_info.bought_items[x].item_purchased[y].order_status['date_modified']=[];
									doc.transaction_info.bought_items[x].item_purchased[y].order_status['date_modified_simple']=[];
									doc.transaction_info.bought_items[x].item_purchased[y].order_status.date_modified=dataStruct.new_unix_timestamp;
									doc.transaction_info.bought_items[x].item_purchased[y].order_status.date_modified_simple=dataStruct.new_simple_timestamp;
									VoidProductCounter++;
									//recalculate 
									/*
									per_merchant_total_order_price:Float,
									per_merchant_total_product_price: Float,
									per_merchant_total_shipping: Float,
									per_merchant_total_tax: Float,
									per_merchant_total_shopstyx_fee: Float,
									per_merchant_total_endorser_earning: Float,

									total_product_price: Float ((actual_price + additional_price) * quantity),
									total_product_tax: Float,
									total_product_shipping_cost: Float,
									total_product_order_price: Float ((actual_price + additional_price _ tax) * quantity) + shipping_cost,
									total_shopstyx_earning: Float (total_order_price - (total_order_price * shopstyx_fee) This will be the amount that the shopstyx will receiving
									total_endroser_earning: Float,
									total_merchant_revenue: Float (this is the amount that the merchant will receiving,

									*/
									subtract_per_merchant(doc,x,y)

								}
								if(VoidProductCounter==doc.transaction_info.bought_items[x].item_purchased.length)
								{
									doc.transaction_info.bought_items[x].overall_transaction_status='Void';
									update_cs_merchant_customers((doc.transaction_info.bought_items[x].overall_transaction_status).toLowerCase(), doc.transaction_info.bought_items[x], doc.transaction_info.bought_items[x].per_merchant_total_order_price,doc.user_id);
									for(var z=0;z<doc.transaction_info.bought_items[x].item_purchased.length;z++)
									{
										restore_per_merchant(doc,x,z)
									}
								}
							}
						}
						if(found==true)
						{
							break;
						}
					}

					if(found==true)
					{
						GLOBAL.mongodb.transactionRecords.update(query,{$set:doc},function(err,docs){
							if(err==null)
							{
								if(docs.n>0)
								{
									var msg = {
										status:"Success",
										message:"Product Status Updated"
									};
									GLOBAL.sendError(msg,dataStruct.res);
								}else{
									var desc = {
										err:err,
										docs:docs,
										query:query
									};
									var msg = {
										status:"Error",
										message:"[updateTransactionOverallperMerchant] Product Status Not Updated",
										description: desc
									};
									GLOBAL.sendError(msg,dataStruct.res);
								}
							}else{
								var desc = {
									err:err,
									docs:docs,
									query:query
								};
								var msg = {
									status:"Error",
									message:"[updateTransactionOverallperMerchant] Product Status Not Updated",
									description: desc
								};
								GLOBAL.sendError(msg,dataStruct.res);
							}
						});
						updateMySQL_overall_transaction_status(dataStruct,doc);
					}else{
						var desc = {
							err:err,
							docs:docs,
							query:query
						};
						var msg = {
							status:"Error",
							message:"[updateTransactionOverallperMerchant] Product and Variant not found",
							description: desc
						};
						GLOBAL.sendError(msg,dataStruct.res);
					}
					
				}else{
					var desc = {
						err:err,
						doc:doc,
						query:query
					};
					var msg = {
						status:"Error",
						message:"[updateTransactionOverallperMerchant] Product Not Found",
						description: desc
					};
					GLOBAL.sendError(msg,dataStruct.res);
				}
			}else{
				var desc = {
					err:err,
					doc:doc
				};
				var msg = {
					status:"Error",
					message:"[updateTransactionOverallperMerchant] Error Processing Data",
					description: desc
				};
				GLOBAL.sendError(msg,dataStruct.res);
			}
		});
	}catch(err){
		var msg = {
			status:"Error",
			message:"[updateTransactionOverallperMerchant] Error Processing Data",
			description: err
		};
		GLOBAL.sendError(msg,dataStruct.res);
	}
}
var updateShippingInfo = function(dataStruct)
{
	try{
		var res = dataStruct.res;
		var req = dataStruct.req;
		var dataJSON = req.body.data;
		if(typeof(dataJSON)=="string")
		{
			dataJSON = eval("("+dataJSON+")");
		}
		var shopstyx_order_id = dataJSON.shopstyx_order_id;
		var bought_items = dataJSON.bought_items;

		var query = {
			"transaction_info.shopstyx_order_id":shopstyx_order_id
		};
		GLOBAL.mongodb.transactionRecords.findOne(query,function(err,doc){
			if(err==null)
			{
				if(doc!=null)
				{
					for(var y = 0;y<bought_items.length;y++)
					{
						for(var x = 0;x<doc.transaction_info.bought_items.length;x++)
						{
							if(doc.transaction_info.bought_items[x].shopstyx_transaction_id == bought_items[y].shopstyx_transaction_id)
							{
								doc.transaction_info.bought_items[x].shipping_info = bought_items[y].shipping_info;
								break;
							}
						}
					}
					GLOBAL.mongodb.transactionRecords.update(query,{$set:doc},function(err,retdoc){
						if(err==null)
						{
							if(retdoc.n>0)
							{
								var msg = {
									status:"Success",
									message:"Product Status Updated"
								};
								GLOBAL.sendError(msg,dataStruct.res);
							}else{
								var desc = {
									err:err,
									doc:retdoc
								};
								var msg = {
									status:"Error",
									message:"[updateShippingInfo] No Record Updated",
									description: desc
								};
								GLOBAL.sendError(msg,dataStruct.res);
							}
						}else{
							var desc = {
								err:err,
								doc:retdoc
							};
							var msg = {
								status:"Error",
								message:"[updateShippingInfo] Error Updating Record",
								description: desc
							};
							GLOBAL.sendError(msg,dataStruct.res);
						}
					});
				}else{
					var desc = {
						err:err,
						doc:doc
					};
					var msg = {
						status:"Error",
						message:"[updateShippingInfo] Error Finding Data to update",
						description: desc
					};
					GLOBAL.sendError(msg,dataStruct.res);
				}
			}else{
				var desc = {
					err:err,
					doc:doc
				};
				var msg = {
					status:"Error",
					message:"[updateShippingInfo] Error Processing Data",
					description: desc
				};
				GLOBAL.sendError(msg,dataStruct.res);
			}
		});
	}catch(err){
		var msg = {
			status:"Error",
			message:"[updateShippingInfo] Error Processing Data",
			description: err
		};
		GLOBAL.sendError(msg,dataStruct.res);
	}
}
var updateIsEmailedTrue = function(dataStruct)
{
	try{
		var res = dataStruct.res;
		var req = dataStruct.req;
		var shopstyx_order_id = req.params.shopstyx_order_id;
		var query = {
			"transaction_info.shopstyx_order_id":shopstyx_order_id,
			"user_id":dataStruct.user_id
		};
		GLOBAL.mongodb.transactionRecords.update(query,{$set:{"is_emailed":true}},function(err,docs){
			if(err==null)
			{
				if(docs.n>0)
				{
					var msg = {
						status:"Success",
						message:"Record updated",
						description: desc
					};
					GLOBAL.sendError(msg,dataStruct.res);
				}else{
					var desc = {
						err:err,
						docs:docs
					};
					var msg = {
						status:"Error",
						message:"[updateIsEmailedTrue] Could not find document",
						description: desc
					};
					GLOBAL.sendError(msg,dataStruct.res);
				}
			}else{
				var desc = {
					err:err,
					docs:docs
				};
				var msg = {
					status:"Error",
					message:"[updateIsEmailedTrue] Error Processing Data",
					description: desc
				};
				GLOBAL.sendError(msg,dataStruct.res);
			}
		})
	}catch(err){
		var msg = {
			status:"Error",
			message:"[updateIsEmailedTrue] Error Processing Data",
			description: err
		};
		GLOBAL.sendError(msg,dataStruct.res);
	}
}

var updateTransactionStatusDisputePerItem = function(dataStruct)
{
	try{
		///:token/:user_id/transaction/log/update/record/order/per/merchant/per/item/status_state/dispute/:disputeStatus/:shopstyx_order_id/:shopstyx_transaction_id/:product_id/:variant_id/

		//per product = resolved, overall = ???
		//per product = refunded, overall = ???
		//per product = disputed, overall = disputed

		//all per product = disputed, overall = disputed
		//all per product = refunded, overall = refunded
		//all per product = resolved, overall = completed
		var is_robot = parseInt(dataStruct.req.params.is_robot);
		if(isNaN(is_robot))
		{
			is_robot=0;
		}
		var query = {
			"$and":[
				// {
				// 	"user_id":parseInt(dataStruct.user_id)
				// },
				{
					"transaction_info.shopstyx_order_id":dataStruct.req.params.shopstyx_order_id
				},
				{
					"transaction_info.bought_items.shopstyx_transaction_id":dataStruct.req.params.shopstyx_transaction_id
				}
			]
		}
		GLOBAL.mongodb.transactionRecords.findOne(query,function(err,doc){
			if(err==null)
			{
				if(doc!=null)
				{
					///:token/:user_id/transaction/log/update/record/order/per/merchant/per/item/status_state/dispute/:disputeStatus/:shopstyx_order_id/:shopstyx_transaction_id/:product_id/:variant_id/
					var bought_items = JSON.parse(JSON.stringify(doc.transaction_info.bought_items));
					var completed_array = ["completed","refunded","resolved"]
					var ovarall_modified = false;
					//all per product = disputed, overall = disputed
					//all per product = refunded, overall = refunded
					//all per product = resolved, overall = completed
					// if(typeof(bought_items.length)=='undefined')
					// {
					// 	bought_items=[];
					// 	bought_items.push(JSON.parse(JSON.stringify(doc.transaction_info.bought_items)));
					// }
					for(var x = 0; x < bought_items.length;x++)
					{
						
						var modified = false;
						// console.log("comparing1:")
						// console.log(bought_items[x].shopstyx_transaction_id+" == "+dataStruct.req.params.shopstyx_transaction_id);
						// console.log("=====================================================================");
						if(bought_items[x].shopstyx_transaction_id==dataStruct.req.params.shopstyx_transaction_id)
						{
							//console.log("compare1 done");order_status
							var item_purchased = JSON.parse(JSON.stringify(bought_items[x].item_purchased));
							var completedVariableCounter = 0;
							var refundedCounter = 0;
							var resolvedCounter = 0;
							var cancelledCounter = 0;
							var shippedCounter = 0;
							// console.log("item_purchased.length "+item_purchased.length);
							// console.log(item_purchased);
							// console.log(bought_items[x]);
							// if(typeof(item_purchased.length)=='undefined')
							// {
							// 	item_purchased=[];
							// 	item_purchased.push(JSON.parse(JSON.stringify(bought_items[x].item_purchased)));
							// }
							// console.log(item_purchased);
							for(var y = 0;y < item_purchased.length;y++)
							{
								console.log("comparing2:")
								console.log(item_purchased[y].product_id+" == "+dataStruct.req.params.product_id);
								console.log(item_purchased[y].variant_id+" == "+dataStruct.req.params.variant_id);
								console.log("=====================================================================");
								if(item_purchased[y].product_id==dataStruct.req.params.product_id && item_purchased[y].variant_id==dataStruct.req.params.variant_id)
								{
									modified=true;
									console.log("modified = true");
									doc.transaction_info.bought_items[x].item_purchased[y].order_status['status_state']=[];
									doc.transaction_info.bought_items[x].item_purchased[y].order_status['date_modified']=[];
									doc.transaction_info.bought_items[x].item_purchased[y].order_status['date_modified_simple']=[];
									

									doc.transaction_info.bought_items[x].item_purchased[y].order_status.status_state=dataStruct.req.params.disputeStatus;
									doc.transaction_info.bought_items[x].item_purchased[y].order_status.date_modified=dataStruct.new_unix_timestamp;
									doc.transaction_info.bought_items[x].item_purchased[y].order_status.date_modified_simple=dataStruct.new_simple_timestamp;
									

									switch((dataStruct.req.params.disputeStatus).toLowerCase())
									{
										case 'disputed':
											//date_disputed date_disputed_simple
											doc.transaction_info.bought_items[x]['date_disputed']=[];
											doc.transaction_info.bought_items[x]['date_disputed_simple']=[];
											doc.transaction_info.bought_items[x]['date_disputed']=dataStruct.new_unix_timestamp;
											doc.transaction_info.bought_items[x]['date_disputed_simple']=dataStruct.new_simple_timestamp;
											//doc.transaction_info.bought_items[x].overall_transaction_status
											doc.transaction_info.bought_items[x].overall_transaction_status=dataStruct.req.params.disputeStatus;
											
											doc.transaction_info.bought_items[x]
											doc.transaction_info.bought_items[x].item_purchased[y].order_status['order_disputed_date']=[];
											doc.transaction_info.bought_items[x].item_purchased[y].order_status['order_disputed_date_simple']=[];
											doc.transaction_info.bought_items[x].item_purchased[y].order_status['is_robot_disputed']=[];
											doc.transaction_info.bought_items[x].item_purchased[y].order_status['is_robot_disputed']=is_robot;
											
											doc.transaction_info.bought_items[x].item_purchased[y].order_status.order_disputed_date=dataStruct.new_unix_timestamp;
											doc.transaction_info.bought_items[x].item_purchased[y].order_status.order_disputed_date_simple=dataStruct.new_simple_timestamp;

											//mark overall as disputed also
											doc.transaction_info.bought_items[x].overall_transaction_status=dataStruct.req.params.disputeStatus;

											break;
										case 'resolved':
											doc.transaction_info.bought_items[x].item_purchased[y].order_status['order_dispute_resolved_date']=[];
											doc.transaction_info.bought_items[x].item_purchased[y].order_status['order_dispute_resolved_date_simple']=[];

											doc.transaction_info.bought_items[x].item_purchased[y].order_status['is_robot_resolved']=[];
											doc.transaction_info.bought_items[x].item_purchased[y].order_status['is_robot_resolved']=is_robot;
											
											doc.transaction_info.bought_items[x].item_purchased[y].order_status.order_dispute_resolved_date=dataStruct.new_unix_timestamp;
											doc.transaction_info.bought_items[x].item_purchased[y].order_status.order_dispute_resolved_date_simple=dataStruct.new_simple_timestamp;
											modifyDispute.updateDisputeStatusSingle(dataStruct,dataStruct.req.params.shopstyx_transaction_id,dataStruct.req.params.disputeStatus);
											break;

										case 'refunded':
											// if((doc.transaction_info.bought_items[x].item_purchased[y].order_status.status_state).toLowerCase()!='refunded')
											// {
												doc.transaction_info.bought_items[x].item_purchased[y].order_status['order_dispute_refunded_date']=[];
												doc.transaction_info.bought_items[x].item_purchased[y].order_status['order_dispute_refunded_date_simple']=[];

												doc.transaction_info.bought_items[x].item_purchased[y].order_status['is_robot_refunded']=[];
												doc.transaction_info.bought_items[x].item_purchased[y].order_status['is_robot_refunded']=is_robot;
												
												doc.transaction_info.bought_items[x].item_purchased[y].order_status.order_dispute_refunded_date=dataStruct.new_unix_timestamp;
												doc.transaction_info.bought_items[x].item_purchased[y].order_status.order_dispute_refunded_date_simple=dataStruct.new_simple_timestamp;
												modifyDispute.updateDisputeStatusSingle(dataStruct,dataStruct.req.params.shopstyx_transaction_id,dataStruct.req.params.disputeStatus);
												//subtract value from per_merchant
												subtract_per_merchant(doc,x,y);
											//}
											
											break;

										case 'completed':
											doc.transaction_info.bought_items[x].item_purchased[y].order_status['order_completed_date']=[];
											doc.transaction_info.bought_items[x].item_purchased[y].order_status['order_completed_date_simple']=[];

											doc.transaction_info.bought_items[x].item_purchased[y].order_status['is_robot_completed']=[];
											doc.transaction_info.bought_items[x].item_purchased[y].order_status['is_robot_completed']=is_robot;
											
											doc.transaction_info.bought_items[x].item_purchased[y].order_status.order_completed_date=dataStruct.new_unix_timestamp;
											doc.transaction_info.bought_items[x].item_purchased[y].order_status.order_completed_date_simple=dataStruct.new_simple_timestamp;
											break;

									}
								}
								//check item_purchased.order_status
								if(completed_array.indexOf((doc.transaction_info.bought_items[x].item_purchased[y].order_status.status_state).toLowerCase())!=-1 )//dataStruct.req.params.disputeStatus
								{
									completedVariableCounter=completedVariableCounter+1;
								}
								if((doc.transaction_info.bought_items[x].item_purchased[y].order_status.status_state).toLowerCase()=='shipped')
								{
									shippedCounter = shippedCounter+1;
								}
								if((doc.transaction_info.bought_items[x].item_purchased[y].order_status.status_state).toLowerCase()=='void')
								{
									cancelledCounter = cancelledCounter+1;
								}
								if((doc.transaction_info.bought_items[x].item_purchased[y].order_status.status_state).toLowerCase()=='refunded')
								{
									refundedCounter = refundedCounter+1;
								}
								if((doc.transaction_info.bought_items[x].item_purchased[y].order_status.status_state).toLowerCase()=='resolved')
								{
									resolvedCounter=resolvedCounter+1;
								}
								if((resolvedCounter == item_purchased.length) || ((resolvedCounter+cancelledCounter) == item_purchased.length))
								{
									doc.transaction_info.bought_items[x].overall_transaction_status='Resolved';
									doc.transaction_info.bought_items[x]['date_refunded']=dataStruct.new_unix_timestamp;
									doc.transaction_info.bought_items[x]['date_refunded_simple']=dataStruct.new_simple_timestamp;
									doc.transaction_info.bought_items[x]['is_robot_resolved']=is_robot;
									//modifyDispute.updateDisputeStatus(dataStruct,dataStruct.req.params.shopstyx_transaction_id,"Resolved");
								}else if((refundedCounter==item_purchased.length) || ((refundedCounter+cancelledCounter) == item_purchased.length)){
									doc.transaction_info.bought_items[x].overall_transaction_status='Refunded';
									doc.transaction_info.bought_items[x]['date_resolved']=dataStruct.new_unix_timestamp;
									doc.transaction_info.bought_items[x]['date_resolved_simple']=dataStruct.new_simple_timestamp;
									doc.transaction_info.bought_items[x]['is_robot_refunded']=is_robot;
									update_cs_merchant_customers((doc.transaction_info.bought_items[x].overall_transaction_status).toLowerCase(), doc.transaction_info.bought_items[x], doc.transaction_info.bought_items[x].per_merchant_total_order_price,doc.user_id);
									//restore item values to per_merchant
									for(var z=0;z<item_purchased.length;z++)
									{
										restore_per_merchant(doc,x,z);
									}
									//update dispute table to Refunded
									//I need shopstyx_transaction_id
									//item_purchased[x]['product_id']
									//item_purchased[x]['variant_id']
									//modifyDispute.updateDisputeStatus(dataStruct,dataStruct.req.params.shopstyx_transaction_id,"Refunded");

								}else if(completedVariableCounter==item_purchased.length){
									doc.transaction_info.bought_items[x].overall_transaction_status='Resolved';
								}else if( (parseInt(item_purchased.length) == parseInt(shippedCounter+cancelledCounter+refundedCounter+resolvedCounter)) && shippedCounter>0 ){
									doc.transaction_info.bought_items[x].overall_transaction_status='Shipped';
								}
							}
						}
						if(modified)
						{
							doc.transaction_info.bought_items[x]['date_modified']=[];
							doc.transaction_info.bought_items[x]['date_modified_simple']=[];
							doc.transaction_info.bought_items[x]['date_modified']=dataStruct.new_unix_timestamp;
							doc.transaction_info.bought_items[x]['date_modified_simple']=dataStruct.new_simple_timestamp;
							ovarall_modified=true;
						}
					}
					var updateQuery={
						"_id":mongojs.ObjectId((doc._id).toString())
					};
					delete doc._id;
					if(ovarall_modified)
					{
						GLOBAL.mongodb.transactionRecords.update(updateQuery,{$set:doc},function(err,docs){
							if(err!=null)
							{
								var desc = {
									err:err,
									docs:docs,
									query:updateQuery
								};
								var msg = {
									status:"Error",
									message:"[updateTransactionStatusDisputePerItem] Transaction Record NOT Updated",
									description: desc
								};
								GLOBAL.sendError(msg,dataStruct.res);
							}else{
								var msg = {
									status:"Success",
									message:"[updateTransactionStatusDisputePerItem] Transaction Record Updated"
								};
								GLOBAL.sendError(msg,dataStruct.res);
							}
						});
						updateMySQL_overall_transaction_status(dataStruct,doc);
					}else{
						var msg = {
							status:"Success",
							message:"[updateTransactionStatusDisputePerItem] NO RECORD FOUND WITH THOSE PARAMETERS",
							description: "no modifications made",
							code:1

						};
						GLOBAL.sendError(msg,dataStruct.res);
					}
					
				}else{
					var desc = {
						query:query,
						params:dataStruct.req.params
					};
					var msg = {
						status:"Success",
						message:"[updateTransactionStatusDisputePerItem] NO RECORD FOUND WITH THOSE PARAMETERS",
						description: desc,
						code:2
					};
					GLOBAL.sendError(msg,dataStruct.res);
				}
			}else{
				var desc = {
					err:err,
					doc:doc,
					query:query
				};
				var msg = {
					status:"Error",
					message:"[updateTransactionStatusDisputePerItem] Error Accessing DB",
					description: desc
				};
				GLOBAL.sendError(msg,dataStruct.res);
			}
		});
	}catch(err){
		var msg = {
			status:"Error",
			message:"[updateTransactionStatusDisputePerItem] Error Processing Data",
			description: err
		};
		GLOBAL.sendError(msg,dataStruct.res);
	}
}

function subtract_per_merchant(doc,x,y)
{
	doc.transaction_info.bought_items[x].per_merchant_total_order_price = doc.transaction_info.bought_items[x].per_merchant_total_order_price - doc.transaction_info.bought_items[x].item_purchased[y].total_product_order_price;
	doc.transaction_info.bought_items[x].per_merchant_total_product_price = doc.transaction_info.bought_items[x].per_merchant_total_product_price - doc.transaction_info.bought_items[x].item_purchased[y].total_product_price;
	doc.transaction_info.bought_items[x].per_merchant_total_shipping = doc.transaction_info.bought_items[x].per_merchant_total_shipping - doc.transaction_info.bought_items[x].item_purchased[y].total_product_shipping_cost;
	doc.transaction_info.bought_items[x].per_merchant_total_tax = doc.transaction_info.bought_items[x].per_merchant_total_tax - doc.transaction_info.bought_items[x].item_purchased[y].total_product_tax;
	doc.transaction_info.bought_items[x].per_merchant_total_shopstyx_fee = doc.transaction_info.bought_items[x].per_merchant_total_shopstyx_fee - doc.transaction_info.bought_items[x].item_purchased[y].total_shopstyx_earning;
	doc.transaction_info.bought_items[x].per_merchant_total_endorser_earning = doc.transaction_info.bought_items[x].per_merchant_total_endorser_earning - doc.transaction_info.bought_items[x].item_purchased[y].total_endroser_earning;
}

function restore_per_merchant(doc,x,z)
{
	doc.transaction_info.bought_items[x].per_merchant_total_order_price = doc.transaction_info.bought_items[x].per_merchant_total_order_price + doc.transaction_info.bought_items[x].item_purchased[z].total_product_order_price;
	doc.transaction_info.bought_items[x].per_merchant_total_product_price = doc.transaction_info.bought_items[x].per_merchant_total_product_price + doc.transaction_info.bought_items[x].item_purchased[z].total_product_price;
	doc.transaction_info.bought_items[x].per_merchant_total_shipping = doc.transaction_info.bought_items[x].per_merchant_total_shipping + doc.transaction_info.bought_items[x].item_purchased[z].total_product_shipping_cost;
	doc.transaction_info.bought_items[x].per_merchant_total_tax = doc.transaction_info.bought_items[x].per_merchant_total_tax + doc.transaction_info.bought_items[x].item_purchased[z].total_product_tax;
	doc.transaction_info.bought_items[x].per_merchant_total_shopstyx_fee = doc.transaction_info.bought_items[x].per_merchant_total_shopstyx_fee + doc.transaction_info.bought_items[x].item_purchased[z].total_shopstyx_earning;
	doc.transaction_info.bought_items[x].per_merchant_total_endorser_earning = doc.transaction_info.bought_items[x].per_merchant_total_endorser_earning + doc.transaction_info.bought_items[x].item_purchased[z].total_endroser_earning;
}
module.exports = {
	updateTransaction:updateTransaction,
	updateTransactionOverallperMerchant:updateTransactionOverallperMerchant,
	updatePerItemVoid:updatePerItemVoid,
	updateShippingInfo:updateShippingInfo,
	updateIsEmailedTrue:updateIsEmailedTrue,
	updateTransactionStatusDisputePerItem:updateTransactionStatusDisputePerItem
}