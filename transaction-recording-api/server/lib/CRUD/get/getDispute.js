

var getDispute = function(dataStruct)
{
	try{
		///:token/:user_id/transaction/dispute/log/get/dispute/:shopstyx_transaction_id
		var query = {
			"shopstyx_transaction_id":dataStruct.req.params.shopstyx_transaction_id
		};
		GLOBAL.mongodb.disputeRecords.findOne(query,function(err,doc){
			if(err==null)
			{
				var msg = {
					status:"Success",
					message:"Data Found",
					data:[]
				};
				if(doc!=null)
				{
					msg.data.push(doc);
				}
				GLOBAL.sendError(msg,dataStruct.res);
			}else{
				var desc = {
					err:err,
					doc:doc,
					query:query
				};
				var msg = {
					status:"Error",
					message:"[getDispute] Error Processing Data",
					description:desc
				};
				GLOBAL.sendError(msg,dataStruct.res);
			}
		});
	}catch(err){
		var msg = {
			status:"Error",
			message:"[getDispute] Error Processing Data",
			description:err
		};
		GLOBAL.sendError(msg,dataStruct.res);
	}
}

module.exports = {
	getDispute:getDispute
}