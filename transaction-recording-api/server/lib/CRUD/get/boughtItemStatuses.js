
var findCustomStatusInBoughtItems = function(dataStruct)
{
	try{
		var res = dataStruct.res;
		var req = dataStruct.req;
		var dataJSON = req.body.data;
		if(typeof(dataJSON)=='undefined')
		{
			dataJSON = req.headers.data;
		}else if(typeof(dataJSON)=='undefined')
		{
			dataJSON = req.query.data;
		}
		if(typeof(dataJSON)!="object")
		{
			dataJSON = eval("("+dataJSON+")");
		}

		var query = {
		    "user_id": parseInt(dataJSON.user_id),
		    "transaction_status": dataJSON.transaction_status,
		    "transaction_info.payment_type": dataJSON.transaction_info.payment_type,
		    "transaction_info.UUID": dataJSON.transaction_info.UUID,
		    "bought_items.overall_transaction_status": dataJSON['bought_items.overall_transaction_status']
		}
		GLOBAL.mongodb.transactionRecords.findOne(query,function(err,doc){
			if(err==null)
			{
				if(doc!=null)
				{
					var reply = {
						status:"Success",
						message:"data found",
						data:[
							{status:true}
						]
					};
					GLOBAL.sendError(reply,dataStruct.res);
				}else{
					var reply = {
						status:"Success",
						message:"No Record Found",
						data:[
							{status:false}
						]
					};
					GLOBAL.sendError(reply,dataStruct.res);
				}
			}else{
				var desc = {
					err:err,
					doc:doc
				}
				var msg = {
					status:"Error",
					message:"[findPendingStatusInBoughtItems] Error Processing Data",
					description: desc
				};
				GLOBAL.sendError(msg,dataStruct.res);
			}
		})
	}catch(err){
		var msg = {
			status:"Error",
			message:"[findPendingStatusInBoughtItems] Error Processing Data",
			description: err
		};
		GLOBAL.sendError(msg,dataStruct.res);
	}
}

module.exports = {
	findCustomStatusInBoughtItems:findCustomStatusInBoughtItems
}