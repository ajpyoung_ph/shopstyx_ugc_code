var util = require('util');

var shopstyx_order_id = function(dataStruct)
{
	try{
		var res = dataStruct.res;
		var req = dataStruct.req;
		var query = {
			"transaction_info.shopstyx_order_id":req.params.shopstyx_order_id,
			"transaction_status":{$nin:["Deleted","deleted","DELETED"]}
		};
		//console.log(query);
		GLOBAL.mongodb.transactionRecords.find(query,function(err,doc){
			if(err==null)
			{
				if(doc.length>0)
				{
					msg = {
						status:"Success",
						message:"Data Found in System",
						data:doc
					};
					GLOBAL.sendError(msg,dataStruct.res);
				}else{
					msg = {
						status:"Error",
						message:"[shopstyx_order_id] Data not found in system",
						description: "Data is non-existent in our Database"
					};
					GLOBAL.sendError(msg,dataStruct.res);
				}
			}else{
				msg = {
					status:"Error",
					message:"[shopstyx_order_id] Error Processing Data",
					description: err
				};
				GLOBAL.sendError(msg,dataStruct.res);
			}
		});
	}catch(err){
		msg = {
			status:"Error",
			message:"[shopstyx_order_id] Error Processing Data",
			description: err
		};
		GLOBAL.sendError(msg,dataStruct.res);
	}
}

var shopstyxTransactionID = function(dataStruct)
{
	try{
		var res = dataStruct.res;
		var req = dataStruct.req;
		var query = {
			"transaction_info.bought_items.shopstyx_transaction_id":req.params.shopstyx_transaction_id,
			"transaction_status":{$nin:["Deleted","deleted","DELETED"]}
		};
		//console.log(query);
		GLOBAL.mongodb.transactionRecords.find(query,function(err,doc){
			if(err==null)
			{
				if(doc.length>0)
				{
					var newJSON = pull_shopstyx_transaction_id(doc,dataStruct);

					msg = {
						status:"Success",
						message:"Data Found in System",
						data:[
							newJSON
						]
					};
					GLOBAL.sendError(msg,dataStruct.res);
				}else{
					msg = {
						status:"Success",
						message:"[shopstyxTransactionID] Data not found in system",
						description: "Data is non-existent in our Database",
						data:[]
					};
					GLOBAL.sendError(msg,dataStruct.res);
				}
			}else{
				msg = {
					status:"Error",
					message:"[shopstyxTransactionID] Error Processing Data",
					description: err
				};
				GLOBAL.sendError(msg,dataStruct.res);
			}
		});
	}catch(err){
		msg = {
			status:"Error",
			message:"[shopstyxTransactionID] Error Processing Data",
			description: err
		};
		GLOBAL.sendError(msg,dataStruct.res);
	}
}

var payment_transaction_id = function(dataStruct)
{
	try{
		var res = dataStruct.res;
		var req = dataStruct.req;
		var query = {
			"transaction_info.bought_items.shopstyx_transaction_id":req.params.shopstyx_transaction_id,
			"transaction_status":{$nin:["Deleted","deleted","DELETED"]}
		};
		//console.log(query);
		GLOBAL.mongodb.transactionRecords.find(query,function(err,doc){
			if(err==null)
			{
				if(doc.length>0)
				{
					var newJSON = pull_payment_transaction_id(doc,dataStruct);

					msg = {
						status:"Success",
						message:"Data Found in System",
						data:[
							newJSON
						]
					};
					GLOBAL.sendError(msg,dataStruct.res);
				}else{
					msg = {
						status:"Error",
						message:"[payment_transaction_id] Data not found in system",
						description: "Data is non-existent in our Database"
					};
					GLOBAL.sendError(msg,dataStruct.res);
				}
			}else{
				msg = {
					status:"Error",
					message:"[payment_transaction_id] Error Processing Data",
					description: err
				};
				GLOBAL.sendError(msg,dataStruct.res);
			}
		});
	}catch(err){
		msg = {
			status:"Error",
			message:"[payment_transaction_id] Error Processing Data",
			description: err
		};
		GLOBAL.sendError(msg,dataStruct.res);
	}
}
var transactions_by_user_and_purchase_date = function(dataStruct)
{
	try{
		var res = dataStruct.res;
		var req = dataStruct.req;
		var start = parseFloat(req.params.unix_date_start);
		var end = parseFloat(req.params.unix_date_end);
		var query = {
			"user_id":parseInt(req.params.user_id),
			"transaction_info.purchase_date": {$gte: start, $lt: end},
			"transaction_status":{$nin:["Deleted","deleted","DELETED"]}
		};
		//console.log(query);
		GLOBAL.mongodb.transactionRecords.find(query,function(err,doc){
			if(err==null)
			{
				if(doc.length>0)
				{
					msg = {
						status:"Success",
						message:"Data Found in System",
						data:doc
					};
					GLOBAL.sendError(msg,dataStruct.res);
				}else{
					msg = {
						status:"Error",
						message:"[transactions_by_user_and_purchase_date] Data not found in system",
						description: "Data is non-existent in our Database"
					};
					GLOBAL.sendError(msg,dataStruct.res);
				}
			}else{
				msg = {
					status:"Error",
					message:"[transactions_by_user_and_purchase_date] Error Processing Data",
					description: err
				};
				GLOBAL.sendError(msg,dataStruct.res);
			}
		});
	}catch(err){
		msg = {
			status:"Error",
			message:"[transactions_by_user_and_purchase_date] Error Processing Data",
			description: err
		};
		GLOBAL.sendError(msg,dataStruct.res);
	}
}
var transactions_by_merchant_id_and_purchase_date = function(dataStruct)
{
	try{
		var res = dataStruct.res;
		var req = dataStruct.req;
		var start = parseFloat(req.params.unix_date_start);
		var end = parseFloat(req.params.unix_date_end);
		var query = {
			"transaction_info.bought_items.store_id":parseInt(req.params.store_id),
			"transaction_info.bought_items.store_user_id":parseInt(req.params.store_user_id),
			"transaction_info.purchase_date": {$gte: start, $lt: end},
			"transaction_status":{$nin:["Deleted","deleted","DELETED"]}
		};
		//console.log(query);
		GLOBAL.mongodb.transactionRecords.find(query,function(err,doc){
			if(err==null)
			{
				if(doc.length>0)
				{
					var newJSON = format_transactions_by_merchant_id_and_purchase_date(doc,dataStruct);
					msg = {
						status:"Success",
						message:"Data Found in System",
						data:[
							newJSON
						]
					};
					GLOBAL.sendError(msg,dataStruct.res);
				}else{
					msg = {
						status:"Error",
						message:"[transactions_by_merchant_id_and_purchase_date] Data not found in system",
						description: "Data is non-existent in our Database"
					};
					GLOBAL.sendError(msg,dataStruct.res);
				}
			}else{
				msg = {
					status:"Error",
					message:"[transactions_by_merchant_id_and_purchase_date] Error Processing Data",
					description: err
				};
				GLOBAL.sendError(msg,dataStruct.res);
			}
		});
	}catch(err){
		msg = {
			status:"Error",
			message:"[transactions_by_merchant_id_and_purchase_date] Error Processing Data",
			description: err
		};
		GLOBAL.sendError(msg,dataStruct.res);
	}
}
var finished_payout_by_endorser_id_and_purchase_date = function(dataStruct)
{
	try{
		var res = dataStruct.res;
		var req = dataStruct.req;
		var start = parseFloat(req.params.unix_date_start);
		var end = parseFloat(req.params.unix_date_end);
		var query = {
			"transaction_info.bought_items.item_purchased.endorsers.user_id":parseInt(req.params.endorser_user_id),
			"transaction_info.bought_items.item_purchased.endorsers.paid_status":true,
			"transaction_info.purchase_date": {$gte: start, $lt: end},
			"transaction_status":{$nin:["Deleted","deleted","DELETED"]}
		};
		//console.log(query);
		GLOBAL.mongodb.transactionRecords.find(query,function(err,doc){
			if(err==null)
			{
				if(doc.length>0)
				{
					var newJSON = format_finished_payout_by_endorser_id_and_purchase_date(doc,dataStruct);
					msg = {
						status:"Success",
						message:"Data Found in System",
						data:[
							newJSON
						]
					};
					GLOBAL.sendError(msg,dataStruct.res);
				}else{
					msg = {
						status:"Error",
						message:"[transactions_by_merchant_id_and_purchase_date] Data not found in system",
						description: "Data is non-existent in our Database"
					};
					GLOBAL.sendError(msg,dataStruct.res);
				}
			}else{
				msg = {
					status:"Error",
					message:"[transactions_by_merchant_id_and_purchase_date] Error Processing Data",
					description: err
				};
				GLOBAL.sendError(msg,dataStruct.res);
			}
		});
	}catch(err){
		msg = {
			status:"Error",
			message:"[transactions_by_merchant_id_and_purchase_date] Error Processing Data",
			description: err
		};
		GLOBAL.sendError(msg,dataStruct.res);
	}
}
var pending_payout_by_endorser_id_and_purchase_date = function(dataStruct)
{
	try{
		var res = dataStruct.res;
		var req = dataStruct.req;
		var start = parseFloat(req.params.unix_date_start);
		var end = parseFloat(req.params.unix_date_end);
		var query = {
			"transaction_info.bought_items.item_purchased.endorsers.user_id":parseInt(req.params.endorser_user_id),
			"transaction_info.bought_items.item_purchased.endorsers.paid_status":false,
			"transaction_info.purchase_date": {$gte: start, $lt: end},
			"transaction_status":{$nin:["Deleted","deleted","DELETED"]}
		};
		//console.log(query);
		GLOBAL.mongodb.transactionRecords.find(query,function(err,doc){
			if(err==null)
			{
				if(doc.length>0)
				{
					var newJSON = format_pending_payout_by_endorser_id_and_purchase_date(doc,dataStruct);
					msg = {
						status:"Success",
						message:"Data Found in System",
						data:[
							newJSON
						]
					};
					GLOBAL.sendError(msg,dataStruct.res);
				}else{
					msg = {
						status:"Error",
						message:"[pending_payout_by_endorser_id_and_purchase_date] Data not found in system",
						description: "Data is non-existent in our Database"
					};
					GLOBAL.sendError(msg,dataStruct.res);
				}
			}else{
				msg = {
					status:"Error",
					message:"[pending_payout_by_endorser_id_and_purchase_date] Error Processing Data",
					description: err
				};
				GLOBAL.sendError(msg,dataStruct.res);
			}
		});
	}catch(err){
		msg = {
			status:"Error",
			message:"[pending_payout_by_endorser_id_and_purchase_date] Error Processing Data",
			description: err
		};
		GLOBAL.sendError(msg,dataStruct.res);
	}
}
function pull_shopstyx_transaction_id(doc,dataStruct)
{
	var newJSON = doc[0];
	var target = doc[0].transaction_info.bought_items;
	//return only the bought_items that is related to the shopstyx_transaction_id
	var xIndex = 0;
	for(var x = 0; x<target.length;x++)
	{
		if(target[x].shopstyx_transaction_id == dataStruct.req.params.shopstyx_transaction_id)
		{
			xIndex = x;
			break;
		}
	}
	newJSON.transaction_info.bought_items = [];
	newJSON.transaction_info.bought_items.push(target[xIndex]);
	return newJSON;
}
function pull_payment_transaction_id(doc,dataStruct)
{
	var newJSON = doc[0];
	var target = doc[0].transaction_info.bought_items;
	//return only the bought_items that is related to the shopstyx_transaction_id
	var xIndex = 0;
	for(var x = 0; x<target.length;x++)
	{
		if(target[x].payment_transaction_id == dataStruct.req.params.payment_transaction_id)
		{
			xIndex = x;
			break;
		}
	}
	newJSON.transaction_info.bought_items = [];
	newJSON.transaction_info.bought_items.push(target[xIndex]);
	return newJSON;
}
function format_transactions_by_merchant_id_and_purchase_date(doc,dataStruct)
{
	var value_push = false;
	var newJSON = {
		"store_id": parseInt(doc[0].transaction_info.bought_items[0].store_id),
		"store_user_id": parseInt(doc[0].transaction_info.bought_items[0].store_user_id),
		"store_url": doc[0].transaction_info.checkout_location_info.store_url,
		"buyers_info":[]
	};
	var origJSON = doc;
	for(var x=0;x<doc.length;x++)
	{
		var target = doc[x].transaction_info.bought_items;
		//clean document to only have the merchant's items
		for(var y=0;y<target.length;y++)
		{
			if(target[y].store_user_id == dataStruct.req.params.store_user_id && target[y].store_id == dataStruct.req.params.store_id)
			{
				var temp = target[y];
				doc[x].transaction_info.bought_items=[];
				doc[x].transaction_info.bought_items.push(temp);
				value_push=true;
				break;
			}
		}
		if(value_push)
		{
			newJSON.buyers_info.push(doc[x]);
		}
	}
	return newJSON;
}
function format_finished_payout_by_endorser_id_and_purchase_date(doc,dataStruct)
{
	var newJSON = {
		user_id:parseInt(dataStruct.req.params.endorser_user_id),
		endorsement_info:[]
	};
	/*
	shopstyx_transaction_id:String or Long_Int/Big_Int,
	paypal_payment_transaction_id:String,
	purchase_date:Date,
	amount_earned:Float,
	user_id:Long_Int/Big_Int,
	endorser_type:”promoter”/”direct_buyer”,
	amount_earned:Float,
	paid_status:Boolean


	"transaction_info.bought_items.item_purchased.endorsers.user_id":req.params.endorser_user_id,
			"transaction_info.bought_items.item_purchased.endorsers.paid_status"
	*/
	var origJSON = doc;
	for(var x=0;x<doc.length;x++)
	{
		var value_push = false;
		var target = doc[x].transaction_info.bought_items;
		var collected_data = {
			shopstyx_transaction_id:'',
			payment_transaction_id:'',
			purchase_date:doc[x].transaction_info.purchase_date_simple,
			purchase_date_unix:doc[x].transaction_info.purchase_date,
			product_id:'',
			amount_earned:'',
			user_id:'',
			endorser_type:'',
			amount_earned:'',
			paid_status:''
		};

		//clean document to only have the merchant's items
		for(var y=0;y<target.length;y++)
		{
			/*collected_data = {
				shopstyx_transaction_id:target[y].shopstyx_transaction_id,
				payment_transaction_id:target[y].payment_transaction_id,
				purchase_date:doc[x].transaction_info.purchase_date_simple,
				amount_earned:target[y].item_purchased[z].endorsers[a].amount_earned,
				user_id:target[y].item_purchased[z].endorsers[a].user_id,
				endorser_type:target[y].item_purchased[z].endorsers[a].endorser_type,
				amount_earned:target[y].item_purchased[z].endorsers[a].amount_earned,
				paid_status:target[y].item_purchased[z].endorsers[a].paid_status,
			};*/
			collected_data.shopstyx_transaction_id=target[y].shopstyx_transaction_id;
			collected_data.payment_transaction_id=target[y].payment_transaction_id;
			var item_purchased = target[y].item_purchased;
			for(var z=0;z<item_purchased.length;z++)
			{
				var endorsers = target[y].item_purchased[z].endorsers;
				for(var a=0;a<endorsers.length;a++)
				{
					if(parseInt(endorsers[a].user_id)==parseInt(newJSON.user_id) && endorsers[a].paid_status==true)
					{
						collected_data.amount_earned=endorsers[a].amount_earned;
						collected_data.user_id=parseInt(endorsers[a].user_id);
						collected_data.endorser_type=endorsers[a].endorser_type;
						collected_data.amount_earned=endorsers[a].amount_earned;
						collected_data.paid_status=endorsers[a].paid_status;
						collected_data.product_id=parseInt(target[y].item_purchased[z].product_id);
						value_push=true;
					}
				}
			}
		}
		if(value_push)
		{
			newJSON.endorsement_info.push(collected_data);
		}
	}
	return newJSON;
}

function format_pending_payout_by_endorser_id_and_purchase_date(doc,dataStruct)
{
	var newJSON = {
		user_id:parseInt(dataStruct.req.params.endorser_user_id),
		endorsement_info:[]
	};

	var origJSON = doc;
	for(var x=0;x<doc.length;x++)
	{
		var value_push = false;
		var target = doc[x].transaction_info.bought_items;
		var collected_data = {
			shopstyx_transaction_id:'',
			payment_transaction_id:'',
			purchase_date:doc[x].transaction_info.purchase_date_simple,
			purchase_date_unix:doc[x].transaction_info.purchase_date,
			product_id:'',
			amount_earned:'',
			user_id:'',
			endorser_type:'',
			amount_earned:'',
			paid_status:''
		};

		//clean document to only have the merchant's items
		for(var y=0;y<target.length;y++)
		{
			collected_data.shopstyx_transaction_id=target[y].shopstyx_transaction_id;
			collected_data.payment_transaction_id=target[y].payment_transaction_id;
			var item_purchased = target[y].item_purchased;
			for(var z=0;z<item_purchased.length;z++)
			{
				var endorsers = target[y].item_purchased[z].endorsers;
				for(var a=0;a<endorsers.length;a++)
				{
					if(parseInt(endorsers[a].user_id)==parseInt(newJSON.user_id) && endorsers[a].paid_status==false)
					{
						collected_data.amount_earned=endorsers[a].amount_earned;
						collected_data.user_id=parseInt(endorsers[a].user_id);
						collected_data.endorser_type=endorsers[a].endorser_type;
						collected_data.amount_earned=endorsers[a].amount_earned;
						collected_data.paid_status=endorsers[a].paid_status;
						collected_data.product_id=parseInt(target[y].item_purchased[z].product_id);
						value_push=true;
						break;
					}
				}
			}
		}
		if(value_push)
		{
			newJSON.endorsement_info.push(collected_data);
		}
	}
	return newJSON;
}

var getOverall_transaction_status = function (dataStruct)
{
	try{
		var res = dataStruct.res;
		var req = dataStruct.req;
		var dataJSON = req.body.data;
		if(typeof(dataJSON)=='undefined')
		{
			dataJSON = req.headers.data;
		}else if(typeof(dataJSON)=='undefined')
		{
			dataJSON = req.query.data;
		}
		if(typeof(dataJSON)!="object")
		{
			dataJSON = eval("("+dataJSON+")");
		}
		var shopstyx_order_id = dataJSON.shopstyx_order_id;
		var bought_items = dataJSON.bought_items;
		var query = {
			"transaction_info.shopstyx_order_id":shopstyx_order_id
		};
		GLOBAL.mongodb.transactionRecords.findOne(query,function(err,doc){
			if(err==null)
			{
				if(doc!=null)
				{
					for(var x = 0;x<bought_items.length;x++)
					{
						for(var y = 0;y<doc.transaction_info.bought_items.length;y++)
						{
							if(bought_items[x].shopstyx_transaction_id == doc.transaction_info.bought_items[y].shopstyx_transaction_id)
							{
								dataJSON.bought_items[x]['overall_transaction_status']=[];
								dataJSON.bought_items[x].overall_transaction_status=doc.transaction_info.bought_items[y].overall_transaction_status;
								break;
							}
						}
					}
					var data = [];
					data.push(dataJSON);
					var msg = {
						status:"Success",
						message:"Data Found",
						data: data
					};
					GLOBAL.sendError(msg,dataStruct.res);
				}else{
					var desc = {
						err:err,
						doc:doc
					};
					var msg = {
						status:"Error",
						message:"[getOverall_transaction_status] Error Data not found",
						description: desc
					};
					GLOBAL.sendError(msg,dataStruct.res);
				}
			}else{
				var msg = {
					status:"Error",
					message:"[getOverall_transaction_status] Error Data not found",
					description: err
				};
				GLOBAL.sendError(msg,dataStruct.res);
			}
		});

	}catch(err){
		var msg = {
			status:"Error",
			message:"[getOverall_transaction_status] Error Processing Data",
			description: err
		};
		GLOBAL.sendError(msg,dataStruct.res);
	}
}

var getALLMerchantTransactions = function(dataStruct)
{
	try{
		var res = dataStruct.res;
		var req = dataStruct.req;
		var dataJSON = req.body.data;
		if(typeof(dataJSON)=='undefined')
		{
			dataJSON = req.headers.data;
		}
		if(typeof(dataJSON)=='undefined')
		{
			dataJSON = req.query.data;
		}
		if(typeof(dataJSON)!="object")
		{
			dataJSON = eval("("+dataJSON+")");
		}
		//console.log(dataJSON);
		var merchant_id = parseInt(dataJSON.store_user_id);
		var store_id = parseInt(dataJSON.store_id);
		var number_of_records = parseInt(dataJSON.number_of_records);
		var skip = parseInt(number_of_records) * parseInt(dataJSON.page);
		// {
		// 	sort:”date” (date/shipto/status/order_total),
		// 	sort_type:”ASC” (ASC/DESC),
		// 	number_of_records:INT,
		// 	page:INT, (starting at zero)
		// 	"transaction_info.bought_items.overall_transaction_status": [
		//             "Pending",
		//             "Completed",
		//             "Void"
		//         ]

		// }
		switch((dataJSON.sort).toLowerCase())
		{
			case 'order_total'://
				if(dataJSON.sort_type=="DESC")
				{
					var sort = {"transaction_info.bought_items.per_merchant_total_order_price":-1}
				}else{
					var sort = {"transaction_info.bought_items.per_merchant_total_order_price":1};
				}
				break;
			case 'status'://
				if(dataJSON.sort_type=="DESC")
				{
					var sort = {"transaction_info.bought_items.overall_transaction_status":-1}
				}else{
					var sort = {"transaction_info.bought_items.overall_transaction_status":1};
				}
				break;
			case 'shipto':
				if(dataJSON.sort_type=="DESC")
				{
					var sort = {"transaction_info.shipping_address.FirstName":-1};
				}else{
					var sort = {"transaction_info.shipping_address.FirstName":1};
				}
				break;
			case 'date':
				if(dataJSON.sort_type=="DESC")
				{
					var sort = {"transaction_info.purchase_date":-1};
				}else{
					var sort = {"transaction_info.purchase_date":1};
				}
				break;
			case 'order_num':
				if(dataJSON.sort_type=="DESC")
				{
					var sort = {"transaction_info.transaction_info.bought_items.shopstyx_transaction_number":-1};
				}else{
					var sort = {"transaction_info.transaction_info.bought_items.shopstyx_transaction_number":1};
				}
				break;
			default:
				var sort = {"transaction_info.purchase_date":-1};
				break;
		}
		if(typeof(dataJSON.date_start)!='undefined')
		{
			if(dataJSON.overall_transaction_status.length==0)
			{
				var query = {
					$and:[
					{"transaction_info.bought_items.store_user_id":merchant_id},
					{"transaction_info.bought_items.store_id":store_id},
					{"transaction_info.purchase_date": {$gte: dataJSON.date_start, $lt: dataJSON.date_end}}
					]
				};
			}else{
				var query = {
					$and:[
					{"transaction_info.bought_items.store_user_id":merchant_id},
					{"transaction_info.bought_items.store_id":store_id},
					{"transaction_info.bought_items.overall_transaction_status":{$in:dataJSON.overall_transaction_status}},
					{"transaction_info.purchase_date": {$gte: dataJSON.date_start, $lt: dataJSON.date_end}}
					]
				};
			}
		}else{
			if(dataJSON.overall_transaction_status.length==0)
			{
				var query = {
					$and:[
					{"transaction_info.bought_items.store_user_id":merchant_id},
					{"transaction_info.bought_items.store_id":store_id}
					]
				};
			}else{
				var query = {
					$and:[
					{"transaction_info.bought_items.store_user_id":merchant_id},
					{"transaction_info.bought_items.store_id":store_id},
					{"transaction_info.bought_items.overall_transaction_status":{$in:dataJSON.overall_transaction_status}}
					]
				};
			}
		}
		//console.log(query);
		GLOBAL.mongodb.transactionRecords.find(query).skip(skip).limit(number_of_records).sort(sort,function(err,docs){
			if(err==null)
			{
				if(docs.length>0)
				{
					//eliminate all records that are not equal to mechant_id
					var holderALLdocs = [];
					docs.forEach(function(doc){
						//var bought_items = doc.transaction_info.bought_items
						var newBoughtItems = [];
						for(var x=0;x < doc.transaction_info.bought_items.length;x++)
						{
							var checkOverAll = 1;
							if(dataJSON.overall_transaction_status.length>0)
							{
								checkOverAll=dataJSON.overall_transaction_status.indexOf(doc.transaction_info.bought_items[x].overall_transaction_status);
							}
							if(doc.transaction_info.bought_items[x].store_user_id == merchant_id && doc.transaction_info.bought_items[x].store_id == store_id && checkOverAll!=-1)
							{
								newBoughtItems.push(doc.transaction_info.bought_items[x])
							}
						}
						doc.transaction_info.bought_items = newBoughtItems;
						holderALLdocs.push(doc);
					});
					var msg = {
						status:"Success",
						message:"Data Found",
						data: holderALLdocs
					};
					GLOBAL.sendError(msg,dataStruct.res);
				}else{
					var msg = {
						status:"Success",
						message:"NO Data Found",
						data: []
					};
					GLOBAL.sendError(msg,dataStruct.res);
				}
			}else{
				var desc = {
					err:err,
					docs:docs
				};
				var msg = {
					status:"Error",
					message:"[getALLMerchantTransactions] Error Processing Data",
					description: err
				};
				GLOBAL.sendError(msg,dataStruct.res);
			}
		})
	}catch(err){
		var msg = {
			status:"Error",
			message:"[getALLMerchantTransactions] Error Processing Data",
			description: err
		};
		GLOBAL.sendError(msg,dataStruct.res);
	}
}

var getALLCustomerTransactions = function(dataStruct)
{
	try{
		var res = dataStruct.res;
		var req = dataStruct.req;
		var dataJSON = req.body.data;
		if(typeof(dataJSON)=='undefined')
		{
			dataJSON = req.headers.data;
		}
		if(typeof(dataJSON)=='undefined')
		{
			dataJSON = req.query.data;
		}
		if(typeof(dataJSON)!="object")
		{
			dataJSON = eval("("+dataJSON+")");
		}
		//console.log(dataJSON);
		var customer_id = parseInt(dataStruct.user_id);
		var number_of_records = parseInt(dataJSON.number_of_records);
		var skip = parseInt(number_of_records) * parseInt(dataJSON.page);
		// {
		// 	sort:”date” (date/shipto/status/order_total),
		// 	sort_type:”ASC” (ASC/DESC),
		// 	number_of_records:INT,
		// 	page:INT, (starting at zero)
		// 	"transaction_info.bought_items.overall_transaction_status": [
		//             "Pending",
		//             "Completed",
		//             "Void"
		//         ]

		// }
		switch((dataJSON.sort).toLowerCase())
		{
			case 'order_total'://
				if(dataJSON.sort_type=="DESC")
				{
					var sort = " ORDER BY per_merchant_total_order_price DESC ";
				}else{
					var sort = " ORDER BY per_merchant_total_order_price ASC ";
				}
				break;
			case 'status'://
				if(dataJSON.sort_type=="DESC")
				{
					var sort = " ORDER BY overall_transaction_status DESC ";
				}else{
					var sort = " ORDER BY overall_transaction_status ASC ";
				}
				break;
			case 'date':
				if(dataJSON.sort_type=="DESC")
				{
					var sort = " ORDER BY purchase_date DESC ";
				}else{
					var sort = " ORDER BY purchase_date ASC ";
				}
				break;
			case 'order_num':
				if(dataJSON.sort_type=="DESC")
				{
					var sort = " ORDER BY shopstyx_order_number DESC ";
				}else{
					var sort = " ORDER BY shopstyx_order_number DESC ";
				}
				break;
			default:
				var sort = " ORDER BY purchase_date DESC ";
				break;
		}
		// var query = {
		// 	$and:[{"user_id":customer_id},
		// 	{"transaction_info.bought_items.overall_transaction_status":{$in:dataJSON.overall_transaction_status}}]
		// };
		var overall_transaction_statusIN = '';
		if(dataJSON.overall_transaction_status.length>0)
		{
			dataJSON.overall_transaction_status.forEach(function(stateData){
				overall_transaction_statusIN = overall_transaction_statusIN +"'"+ stateData +"',";
			});
			//remove the last comma ','
			overall_transaction_statusIN = overall_transaction_statusIN.substring(0, overall_transaction_statusIN.length - 1);
		}
		var query = "SELECT * FROM cs_users_orders WHERE user_id="+customer_id;
		if(overall_transaction_statusIN!='')
		{
			query = query +" AND ( overall_transaction_status IN ("+overall_transaction_statusIN+"))";
		}
		if(typeof(dataJSON.date_start)!='undefined')
		{
			query = query + " AND (purchase_date BETWEEN "+dataJSON.date_start+" AND "+dataJSON.date_end+")";
		}
		query = query + sort;
		//console.log(util.inspect(query,{showHidden: false, depth: null}));
		GLOBAL.db_cs_common.query(query,function(err,rows,fields){
			if(err==null)
			{
				if(rows.length>0)
				{
					var msg = {
						status:"Success",
						message:"Data Found",
						data: rows
					};
					GLOBAL.sendError(msg,dataStruct.res);
				}else{
					var msg = {
						status:"Success",
						message:"Data Found",
						data: []
					};
					GLOBAL.sendError(msg,dataStruct.res);
				}
			}else{
				var desc = {
					err:err,
					rows:rows,
					fields:fields,
					query:query
				};
				var msg = {
					status:"Error",
					message:"[getOverall_transaction_status] Error Processing Data",
					description: desc
				};
				GLOBAL.sendError(msg,dataStruct.res);
			}
		});
	}catch(err){
		var msg = {
			status:"Error",
			message:"[getOverall_transaction_status] Error Processing Data",
			description: err
		};
		GLOBAL.sendError(msg,dataStruct.res);
	}
}

var totalOrdersTrans = function(dataStruct)
{
	try{
		var access=1;
		var res = dataStruct.res;
		var req = dataStruct.req;
		var dataJSON = req.body.data;
		if(typeof(dataJSON)=='undefined')
		{
			dataJSON = req.headers.data;
		}
		if(typeof(dataJSON)=='undefined')
		{
			dataJSON = req.query.data;
		}
		if(typeof(dataJSON)!="object")
		{
			dataJSON = eval("("+dataJSON+")");
		}
		if(typeof(req.params.type)=='undefined')
		{
			var msg = {
				status:"Error",
				message:"[totalOrdersTrans] Error Processing Data",
				description: "Illegal Params"
			};
			GLOBAL.sendError(msg,dataStruct.res);
		}else{
			switch((req.params.type).toLowerCase())
			{
				case 'buyer':
					var query = {
						"user_id":parseInt(req.params.target_id)
					};
					break;
				case 'seller'://
					var query = {
						"$or":[
	                        {"transaction_info.bought_items.store_claimed_by": parseInt(req.params.target_id)},
	                        {"transaction_info.bought_items.store_user_id": parseInt(req.params.target_id)}
	                    ]
					};
					break;
				default:
					var msg = {
						status:"Error",
						message:"[totalOrdersTrans] Error Processing Data",
						description: "Illegal DataType"
					};
					GLOBAL.sendError(msg,dataStruct.res);
					access=0;
					break;
			}
			if(access==1)
			{
				//console.log(query);
				GLOBAL.mongodb.transactionRecords.count(query,function(err,docs){
					var msg = {
						status:"Success",
						message:"[totalOrdersTrans] Data Found kuno",
						data:[]
					};
					if(err==null)
					{
						//msg.data['count']=docs;
						var data = {
							count : docs
						};
						msg.data.push(data);
						// console.log("00000000000000000");
						// console.log(msg);
					}else{
						msg.status="Error";
						msg.message="[totalOrdersTrans] Data NOT Found";
						//msg.data['count']=0;
						var data = {
							count:0
						};
						msg.data.push(data)
					}
					//msg.data['query']=query;
					//console.log(msg);
					GLOBAL.sendError(msg,dataStruct.res);
					
				});
			}
		}		
	}catch(err){
		var msg = {
			status:"Error",
			message:"[totalOrdersTrans] Error Processing Data",
			description: err
		};
		GLOBAL.sendError(msg,dataStruct.res);
	}
	
}

module.exports = {
	shopstyx_order_id:shopstyx_order_id,
	shopstyxTransactionID:shopstyxTransactionID,
	payment_transaction_id:payment_transaction_id,
	transactions_by_user_and_purchase_date:transactions_by_user_and_purchase_date,
	transactions_by_merchant_id_and_purchase_date:transactions_by_merchant_id_and_purchase_date,
	finished_payout_by_endorser_id_and_purchase_date:finished_payout_by_endorser_id_and_purchase_date,
	pending_payout_by_endorser_id_and_purchase_date:pending_payout_by_endorser_id_and_purchase_date,
	getOverall_transaction_status:getOverall_transaction_status,
	getALLMerchantTransactions:getALLMerchantTransactions,
	getALLCustomerTransactions:getALLCustomerTransactions,
	totalOrdersTrans:totalOrdersTrans
}