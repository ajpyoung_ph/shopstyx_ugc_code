var mongojs = require('mongojs');


var saveDispute = function(dataStruct)
{
	try{
		var res = dataStruct.res;
		var req = dataStruct.req;
		var dataJSON = req.body.data;

		//console.log("checking dataJSON");
		//console.log(dataJSON);
		if(typeof(dataJSON)=="string")
		{
			dataJSON = eval("("+dataJSON+")");
		}
		if(typeof(dataJSON['process_marker'])=='undefined')
		{
			dataJSON['process_marker']=[];
		}
		dataJSON.process_marker=3;
		updateDates(dataStruct,dataJSON);

		//this is not a direct save
		//check if dispute is existing before updating/inserting
		var findquery = {
			"$and":[
				{
					"shopstyx_transaction_id":dataJSON.shopstyx_transaction_id
				},
				{
					"shopstyx_transaction_number":dataJSON.shopstyx_transaction_number
				}
			]
		};
		GLOBAL.mongodb.disputeRecords.findOne(findquery,function(err,doc){
			if(err==null)
			{
				if(doc!=null)
				{
					//update
					processData(doc,dataJSON,dataStruct);
				}else{
					//insert
					GLOBAL.mongodb.disputeRecords.insert(dataJSON,function(err,doc){
						if(err==null)
						{
							var msg = {
								status:"Success",
								message:"[saveDispute] Saved Dispute Data",
								data:[]
							};
							msg.data.push(doc);
							GLOBAL.sendError(msg,dataStruct.res);
						}else{
							var desc = {
								err:err,
								doc:doc,
								query:dataJSON
							};
							var msg = {
								status:"Error",
								message:"[saveDispute] Error Inserting Dispute Data",
								description:desc
							};
							GLOBAL.sendError(msg,dataStruct.res);
						}
					});
				}
				
			}else{
				var desc = {
					err:err,
					doc:doc,
					query:findquery
				};
				var msg = {
					status:"Error",
					message:"[saveDispute] Error Processing Dispute Data",
					description:desc
				};
				GLOBAL.sendError(msg,dataStruct.res);
			}
		});
	}catch(err){
		var msg = {
			status:"Error",
			message:"[saveDispute] Error Processing Dispute Data",
			description:err
		};
		GLOBAL.sendError(msg,dataStruct.res);
	}
}

function updateDates(dataStruct,dataJSON)
{
	//update dates
	if(typeof(dataJSON.date)=='undefined')
	{
		dataJSON['date']=[];
	}
	if(typeof(dataJSON.date_simple)=='undefined')
	{
		dataJSON['date_simple']=[];
	}
	dataJSON.date=dataStruct.new_unix_timestamp;
	dataJSON.date_simple=dataStruct.new_simple_timestamp;
	for(var x = 0;x<dataJSON.item_purchased.length;x++)
	{
		if(typeof(dataJSON.item_purchased[x]['date_modified'])=='undefined')
		{
			dataJSON.item_purchased[x]['date_modified']=[];
		}
		if(typeof(dataJSON.item_purchased[x]['date_modified_simple'])=='undefined')
		{
			dataJSON.item_purchased[x]['date_modified_simple']=[];
		}
		if(typeof(dataJSON.item_purchased[x]['date_created'])=='undefined')
		{
			dataJSON.item_purchased[x]['date_created']=[];
		}
		if(typeof(dataJSON.item_purchased[x]['date_created_simple'])=='undefined')
		{
			dataJSON.item_purchased[x]['date_created_simple']=[];
		}
		dataJSON.item_purchased[x].date_modified=dataStruct.new_unix_timestamp;
		dataJSON.item_purchased[x].date_modified_simple=dataStruct.new_simple_timestamp;
		dataJSON.item_purchased[x].date_created=dataStruct.new_unix_timestamp;
		dataJSON.item_purchased[x].date_created_simple=dataStruct.new_simple_timestamp;
	}
}

function processData(doc,dataJSON,dataStruct)
{
	try{
		//check if dataJSON.item_purchased.product_id and dataJSON.item_purchased.variant_id is equal to doc.item_purchased.product_id and doc.item_purchased.variant_id
		var matchCounter = 0;
		var matchCounterIndex = [];
		for(var x = 0; x < doc.item_purchased.length; x++)
		{
			for(var y = 0;y < dataJSON.item_purchased.length;y++)
			{
				if((dataJSON.item_purchased[y].product_id == doc.item_purchased[x].product_id) && (dataJSON.item_purchased[y].variant_id == doc.item_purchased[x].variant_id))
				{
					var fileHolder=[];
					var fileExists=false;
					//check if doc has files
					if(typeof(doc.item_purchased[x].files)!='undefined')
					{
						//check if files have items in it
						if(doc.item_purchased[x].files.length>0)
						{
							for(var z=0;z<doc.item_purchased[x].files.length;z++)
							{
								fileHolder.push(doc.item_purchased[x].files[z]);
								fileExists=true;
							}
						}
					}
					doc.item_purchased[x] = dataJSON.item_purchased[y];
					if(fileExists)
					{
						//transfer the files to doc
						for(var z=0;z<fileHolder.length;z++)
						{
							doc.item_purchased[x].files.push(fileHolder[z]);
						}
					}
					matchCounterIndex[matchCounter]=y;
					matchCounter++;
					break;
				}
			}
		}
		if(matchCounter!=dataJSON.item_purchased.length)
		{
			//there must be data that needs to be inserted
			for(var y = 0;y < dataJSON.item_purchased.length;y++)
			{
				if(matchCounterIndex.indexOf(y)==-1)
				{

					doc.item_purchased.push(dataJSON.item_purchased[y]);
				}
			}
		}
		//move items in dataJSON to doc
		// dataJSON.item_purchased.forEach(function(item){
		// 	doc.item_purchased.push(item);
		// });
		var id = (doc._id).toString();
		var query = {
			"_id":mongojs.ObjectId((doc._id).toString())
		};
		delete doc._id;
		GLOBAL.mongodb.disputeRecords.update(query,{$set:doc},{upsert:false},function(err,docs){
			if(err==null)
			{
				var msg = {
					status:"Success",
					message:"[processData] Saved Dispute Data",
					data:[]
				};
				doc["_id"]=[];
				doc["_id"]=id;
				msg.data.push(doc);
				GLOBAL.sendError(msg,dataStruct.res);
			}else{
				var desc = {
					err:err,
					docs:docs,
					query:query
				};
				var msg = {
					status:"Error",
					message:"[processData] Error Updating Dispute Data",
					description:desc
				};
				GLOBAL.sendError(msg,dataStruct.res);
			}
		});
	}catch(err){
		
		var msg = {
			status:"Error",
			message:"[processData] Error Processing Dispute Data",
			description:JSON.stringify(err)
		};
		GLOBAL.sendError(msg,dataStruct.res);
	}
}
module.exports = {
	saveDispute:saveDispute
}