var id_generator = require('../../generateLatest/idGenerator');
var util = require('util');

var start = function(dataStruct)
{
	try{
		var res = dataStruct.res;
		var req = dataStruct.req;
		//console.log(req.body);
		var dataJSON = req.body.data;
		var newDate = dataStruct.new_unix_timestamp;
		//console.log("checking dataJSON");
		//console.log(dataJSON);
		if(typeof(dataJSON)=="string")
		{
			dataJSON = eval("("+dataJSON+")");
		}
		// console.log("dataJSON valid");
		// console.log(dataJSON);
		// if(typeof(dataJSON.transaction_info.purchase_date)=='undefined')
		// {
		// 	dataJSON.transaction_info['purchase_date']=[];
		// }
		// if(typeof(dataJSON.transaction_info.purchase_date_simple)=='undefined')
		// {
		// 	dataJSON.transaction_info['purchase_date_simple']=[];
		// }
		// dataJSON.transaction_info.purchase_date=dataStruct.new_unix_timestamp;
		// dataJSON.transaction_info.purchase_date_simple=dataStruct.new_simple_timestamp;
		// dataJSON['process_marker']=[];
		// dataJSON.transaction_info.shopstyx_order_id = id_generator.latest_shopstyx_order_id(dataStruct);
		// var reply_struct = {
		// 	"shopstyx_order_id":dataJSON.transaction_info.shopstyx_order_id,
		// 	"shopstyx_order_number":'',
		// 	"bought_items":[]
		// };
		// dataJSON.transaction_info.bought_items.forEach(function(bought_items){
		// 	dataStruct['shop_id']=bought_items.store_id;
		// 	dataStruct['shop_user_id']=parseInt(bought_items.store_user_id);
			
		// 	bought_items.shopstyx_transaction_id = id_generator.latest_merchant_shopstyx_transaction_id(dataStruct);
			// bought_items.date_modified = newDate;	
			// var data_reply = {
			// 	shopstyx_transaction_id:bought_items.shopstyx_transaction_id,
			// 	store_id: bought_items.store_id,
			// 	store_user_id: bought_items.store_user_id,
			// 	store_claimed_by: bought_items.store_claimed_by,
			// 	payment_transaction_id:"",
			// 	payment_transaction_date:"",
			// 	per_merchant_total_order_price:parseFloat(bought_items.per_merchant_total_order_price),
			// 	shopstyx_transaction_number:parseInt(bought_items.shopstyx_transaction_number)
			// };
			// reply_struct.bought_items.push(data_reply);
		// });
		generate_transactionID_to_cs_merchant_order_ids(dataJSON,dataStruct);
	}catch(err){
		var msg = {
			status:"Error",
			message:"[start] Error Processing Saving Data",
			description:err
		}
		GLOBAL.sendError(msg,dataStruct.res);
	}
}
var saveID = function(dataJSON,dataStruct,reply_struct)
{
	try{
		var res = dataStruct.res;
		var req = dataStruct.req;
		
		var newDate = dataStruct.new_unix_timestamp;
		//console.log("checking dataJSON");
		//console.log(dataJSON);

		// console.log("dataJSON valid");
		// console.log(dataJSON);
		if(typeof(dataJSON.is_emailed)=='undefined')
		{
			dataJSON["is_emailed"]=[];
			dataJSON.is_emailed=false;
		}
		// if(typeof(dataJSON.transaction_info.purchase_date)=='undefined')
		// {
		// 	dataJSON.transaction_info['purchase_date']=[];
		// }
		// if(typeof(dataJSON.transaction_info.purchase_date_simple)=='undefined')
		// {
		// 	dataJSON.transaction_info['purchase_date_simple']=[];
		// }
		// dataJSON.transaction_info.purchase_date=dataStruct.new_unix_timestamp;
		// dataJSON.transaction_info.purchase_date_simple=dataStruct.new_simple_timestamp;
		dataJSON['process_marker']=[];
		dataJSON['timezone']=[];
		dataJSON.timezone=dataStruct.timezone;
		// dataJSON.transaction_info.shopstyx_order_id = id_generator.latest_shopstyx_order_id(dataStruct);
		// var reply_struct = {
		// 	"shopstyx_order_id":dataJSON.transaction_info.shopstyx_order_id,
		// 	"shopstyx_order_number":'',
		// 	"bought_items":[]
		// };
		// dataJSON.transaction_info.bought_items.forEach(function(bought_items){
			// dataStruct['shop_id']=bought_items.store_id;
			// dataStruct['shop_user_id']=parseInt(bought_items.store_user_id);
			
			// bought_items.shopstyx_transaction_id = id_generator.latest_merchant_shopstyx_transaction_id(dataStruct);
		// 	bought_items.date_modified = newDate;	
		// 	var data_reply = {
		// 		shopstyx_transaction_id:bought_items.shopstyx_transaction_id,
		// 		store_id: bought_items.store_id,
		// 		store_user_id: bought_items.store_user_id,
		// 		store_claimed_by: bought_items.store_claimed_by,
		// 		payment_transaction_id:"",
		// 		payment_transaction_date:"",
		// 		per_merchant_total_order_price:parseFloat(bought_items.per_merchant_total_order_price),
		// 		shopstyx_transaction_number:parseInt(bought_items.shopstyx_transaction_number)
		// 	};
		// 	reply_struct.bought_items.push(data_reply);
		// });
		dataJSON['process_marker']=1;
		//console.log(dataJSON);
		// GLOBAL.mongodb.transactionRecords.update({'transaction_info.shopstyx_order_id':dataJSON.transaction_info.shopstyx_order_id, 'user_id':dataJSON.user_id},
		// 	{'$set': dataJSON },
		// 	{upsert:true},function(err,doc){
		// 		if(err==null){
		// 			msg = {
		// 				status:"Success",
		// 				message:"Information Saved"
		// 			};
		// 			GLOBAL.sendError(msg,res);
		// 		}else{
		// 			msg = {
		// 				status:"Error",
		// 				message:"Error Saving Data",
		// 				description: err
		// 			};
		// 			GLOBAL.sendError(msg,res);
		// 		}
		// });
		//search latest shopstyx_order_number
		var query2 = {
			"user_id":dataJSON.user_id
		};
		reply_struct['user_id']=[];
		reply_struct['user_id']=dataJSON.user_id;
		GLOBAL.mongodb.transactionRecords.find(query2).sort({"transaction_info.purchase_date":-1}).limit(1,function(err,doc){
			if(err==null)
			{
				if(doc.length>0)
				{
					try{
						var shopstyx_order_number = doc[0].transaction_info.shopstyx_order_number;
					}catch(err){
						var shopstyx_order_number = 0;
					}
				}else{
					var shopstyx_order_number = 0;
				}
				if(typeof(dataJSON.transaction_info.shopstyx_order_number)=='undefined')
				{
					dataJSON.transaction_info['shopstyx_order_number']=[];
				}
				if(typeof(shopstyx_order_number)=='undefined')
				{
					shopstyx_order_number=0;
				}
				dataJSON.transaction_info.shopstyx_order_number = parseInt(shopstyx_order_number) + parseInt(1);
				reply_struct.shopstyx_order_number = dataJSON.transaction_info.shopstyx_order_number;
				GLOBAL.mongodb.transactionRecords.insert(dataJSON,function(err,doc){
					if(err==null){
						msg = {
							status:"Success",
							message:"Information Saved",
							data:[]
						};
						msg.data.push(reply_struct);
						GLOBAL.sendError(msg,res);
						var id = doc._id.toString();
						setImmediate(GLOBAL.data_integrity.check_unprocessed_data_single,id);
					}else{
						var msg = {
							status:"Error",
							message:"[saveID] Error Processing Saving Data",
							description:err
						}
						GLOBAL.sendError(msg,dataStruct.res);
					}
				});
			}else{
				var msg = {
					status:"Error",
					message:"[saveID] Error Processing Saving Data",
					query:query2,
					description:err
				}
				GLOBAL.sendError(msg,dataStruct.res);
			}
		});
	}catch(err){
		var msg = {
			status:"Error",
			message:"[saveID] Error Processing Saving Data",
			description:err
		}
		GLOBAL.sendError(msg,dataStruct.res);
	}
}

var generate_transactionID_to_cs_merchant_order_ids = function(doc,dataStruct)
{
	try{
		var total_items = doc.transaction_info.bought_items.length;
		var counter=0;
		var newDate = dataStruct.new_unix_timestamp;

		doc.transaction_info.shopstyx_order_id = id_generator.latest_shopstyx_order_id(dataStruct);
		if(typeof(doc.transaction_info.purchase_date)=='undefined')
		{
			doc.transaction_info['purchase_date']=[];
		}
		if(typeof(doc.transaction_info.purchase_date_simple)=='undefined')
		{
			doc.transaction_info['purchase_date_simple']=[];
		}
		doc.transaction_info.purchase_date=dataStruct.new_unix_timestamp;
		doc.transaction_info.purchase_date_simple=dataStruct.new_simple_timestamp;
		var reply_struct = {
			"shopstyx_order_id":doc.transaction_info.shopstyx_order_id,
			"shopstyx_order_number":'',
			"purchase_date":doc.transaction_info.purchase_date,
			"purchase_date_simple":doc.transaction_info.purchase_date_simple,
			"bought_items":[]
		};
		for(var x=0;x<doc.transaction_info.bought_items.length;x++)
		{
			dataStruct['shop_id']=doc.transaction_info.bought_items[x].store_id;
			dataStruct['shop_user_id']=parseInt(doc.transaction_info.bought_items[x].store_user_id);
			
			doc.transaction_info.bought_items[x].shopstyx_transaction_id = id_generator.latest_merchant_shopstyx_transaction_id(dataStruct);

			doc.transaction_info.bought_items[x].date_modified = newDate;
			//check if bought_items[x].item_purchased is an array
			console.log("=============== length ===============");
			console.log(doc.transaction_info.bought_items[x].item_purchased.length);
			if(typeof(doc.transaction_info.bought_items[x].item_purchased.length)=='undefined')
			{
				console.log("============= check struct ==========");
				console.log(doc.transaction_info.bought_items[x].item_purchased);
				var valueHolder = JSON.stringify(doc.transaction_info.bought_items[x].item_purchased);
				doc.transaction_info.bought_items[x].item_purchased=[];
				doc.transaction_info.bought_items[x].item_purchased.push(JSON.parse(valueHolder));
				// console.log("initial item_purchased converted");
				// console.log(doc.transaction_info.bought_items[x].item_purchased[0]);
				//test if you can access item_purchased[0].product_id
				if(typeof(doc.transaction_info.bought_items[x].item_purchased[0].product_id)=='undefined')
				{
					var arrayHolder = [];
					for (index in doc.transaction_info.bought_items[x].item_purchased)
					{
						for(key in doc.transaction_info.bought_items[x].item_purchased[index])
						{
							valueHolder = doc.transaction_info.bought_items[x].item_purchased[index][key];
							valueHolder = JSON.parse(JSON.stringify(valueHolder));
							arrayHolder.push(valueHolder);
						}
					}
					doc.transaction_info.bought_items[x].item_purchased=[];
					for (var counter=0;counter<arrayHolder.length;counter++)
					{
						doc.transaction_info.bought_items[x].item_purchased.push(JSON.parse(JSON.stringify(arrayHolder[counter])));
					}
				}
				console.log("new item_purchased");
				console.log(doc.transaction_info.bought_items[x].item_purchased[0]);
			}	
			
			executeProcess(x,total_items,counter,doc,dataStruct,reply_struct);
		}
	}catch(err){
		msg = {
			status:"Error",
			message:"[generate_transactionID_to_cs_merchant_order_ids] Error Processing Transaction Data",
			description:err
		};
		GLOBAL.sendError(msg,dataStruct.res);
		console.log(util.inspect(msg,{showHidden: false, depth: null}));
		GLOBAL.checker_running = false;
	}
}
function executeProcess(x,total_items,counter,doc,dataStruct,reply_struct)
{
	try{

		var query = "SELECT * FROM cs_merchant_order_ids WHERE shopstyx_transaction_id='"+doc.transaction_info.bought_items[x].shopstyx_transaction_id+"' LIMIT 1;";
		GLOBAL.db_cs_common.query(query,function(err,rows,fields){
			if(err==null)
			{
				if(rows.length>0)
				{
					//get the shopstyx_transaction_id
					if(typeof(doc.transaction_info.bought_items[x].shopstyx_transaction_id)!='undefined')
					{
						//console.log(x+"||"+total_items);
						doc.transaction_info.bought_items[x].shopstyx_transaction_number = [];
						doc.transaction_info.bought_items[x].shopstyx_transaction_number = rows[0]["id"];
						populate_reply_struct(reply_struct,doc,x);
						if(x==total_items-1)
						{
							//console.log("saved saveID");
							saveID(doc,dataStruct,reply_struct);
							//checkTOTALTracker(doc,3);
						}
					}
					
				}else{
					if(typeof(doc.transaction_info.bought_items[x].shopstyx_transaction_id)!='undefined')
					{
						//console.log(x+"||"+total_items);
						var query2 = "INSERT INTO `cs_merchant_order_ids` SET shopstyx_transaction_id = '"+doc.transaction_info.bought_items[x].shopstyx_transaction_id+"';";
						GLOBAL.db_cs_common.query(query2,function(err,rows,fields){
							if(err==null)
							{
								
								var order_id = parseInt(rows.insertId);
								doc.transaction_info.bought_items[x].shopstyx_transaction_number = [];
								doc.transaction_info.bought_items[x].shopstyx_transaction_number = order_id;
								populate_reply_struct(reply_struct,doc,x);
								
								if(x==total_items-1)
								{
									//console.log("saved saveID");
									saveID(doc,dataStruct,reply_struct);
									//checkTOTALTracker(doc,3);
								}
							}else{
								var desc = {
									err:err,
									rows:rows,
									fields:fields,
									query:query
								};
								var msg = {
									status:"Error",
									message:"[executeIDGeneration] Error INSERTING executeIDGeneration",
									description:desc
								};
								GLOBAL.sendError(msg,dataStruct.res);
								//console.log(util.inspect(msg,{showHidden: false, depth: null}));
								GLOBAL.checker_running = false;
							}
						});
					}else{
						//console.log(x+"||"+total_items);
					}
				}
			}else{
				var desc = {
					err:err,
					rows:rows,
					fields:fields,
					query:query
				};
				var msg = {
					status:"Background Process Error",
					message:"[executeIDGeneration] Error Executing executeIDGeneration",
					description:desc
				};
				GLOBAL.sendError(msg,dataStruct.res);
				console.log(util.inspect(msg,{showHidden: false, depth: null}));
				GLOBAL.checker_running = false;
			}
		});
	}catch(err){
		msg = {
			status:"Background Process Error",
			message:"[executeProcess] Error Processing Transaction Data",
			description:err
		};
		GLOBAL.sendEmail(msg);
		console.log(util.inspect(msg,{showHidden: false, depth: null}));
		GLOBAL.checker_running = false;
	}
	
}

function populate_reply_struct(reply_struct,doc,x)
{
	var data_reply = {
		shopstyx_transaction_id:doc.transaction_info.bought_items[x].shopstyx_transaction_id,
		store_id: doc.transaction_info.bought_items[x].store_id,
		store_user_id: doc.transaction_info.bought_items[x].store_user_id,
		store_claimed_by: doc.transaction_info.bought_items[x].store_claimed_by,
		payment_transaction_id:"",
		payment_transaction_date:"",
		per_merchant_total_order_price:parseFloat(doc.transaction_info.bought_items[x].per_merchant_total_order_price),
		shopstyx_transaction_number:parseInt(doc.transaction_info.bought_items[x].shopstyx_transaction_number)
	};
	reply_struct.bought_items.push(data_reply);
}
module.exports = {
	start:start
}