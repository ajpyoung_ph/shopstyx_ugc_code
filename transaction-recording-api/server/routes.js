var saveRecord = require('./lib/CRUD/post/saveTransactionRecord');
var saveDispute = require('./lib/CRUD/post/saveDisputeRecord');

var updateTransactionData = require('./lib/CRUD/put/updateTransactionData');
var updateRecaptureData = require('./lib/CRUD/put/recaptureTransaction');
var updateEndorser = require('./lib/CRUD/put/updateEndorserInformation');
var readData = require('./lib/CRUD/get/readData');
var verifyAccess = require('./lib/securityChecks');
var modifyTransactionStatus = require('./lib/CRUD/put/deleteTransaction');
var boughtItemStatuses = require('./lib/CRUD/get/boughtItemStatuses');
var mysqlStuff = require('./lib/mysql_connects/mysql_convertions');

var modifyDispute = require('./lib/CRUD/put/modifyDispute');
var getDispute = require('./lib/CRUD/get/getDispute');
var pmx = require('pmx');
var probe = pmx.probe();
 
var meter = probe.meter({
  name    : 'req/min',
  seconds : 60
});

module.exports = function(router){

	router.route('/:token/:user_id/transaction/log/')
		.post(function(req,res){
			validate(req,res,saveRecord.start);
		});
	router.route('/:token/:user_id/transaction/log/update/paypal/transaction/id/')
		.post(function(req,res){
			validate(req,res,updateTransactionData.updatePaypal);
		});
	router.route('/:token/:user_id/transaction/shipped/:shopstyx_order_id/:store_id/:store_user_id/:shopstyx_transaction_id/:product_id/')
		.post(function(req,res){
			validate(req,res,updateRecaptureData.ship_single_product);
		});
	router.route('/:token/:user_id/transaction/recapture/:shopstyx_order_id/:store_id/:store_user_id/:shopstyx_transaction_id/:product_id/')
		.post(function(req,res){
			validate(req,res,updateRecaptureData.updateRecapture);
		});
	router.route('/:token/:user_id/transaction/cancel/order/all/:shopstyx_order_id/')
		.post(function(req,res){
			validate(req,res,updateRecaptureData.cancel_all_orders_by_shopstyx_order_id);
		});
	router.route('/:token/:user_id/transaction/cancel/order/all/:shopstyx_order_id/:store_id/:store_user_id/:shopstyx_transaction_id/')
		.post(function(req,res){
			validate(req,res,updateRecaptureData.cancel_all_orders_by_merchant);
		});

	router.route('/:token/:user_id/transaction/get/shopstyx_order_id/:shopstyx_order_id/')
		.get(function(req,res){
			validate(req,res,readData.shopstyx_order_id);
		});
	router.route('/:token/:user_id/transaction/get/shopstyx_transaction_id/:shopstyx_transaction_id')
		.get(function(req,res){
			validate(req,res,readData.shopstyxTransactionID);
		});
	router.route('/:token/:user_id/transaction/get/payment_transaction_id/:payment_transaction_id')
		.get(function(req,res){
			validate(req,res,readData.payment_transaction_id);
		});
	router.route('/:token/:user_id/transaction/get/all/purchase_date/:unix_date_start/:unix_date_end')
		.get(function(req,res){
			validate(req,res,readData.transactions_by_user_and_purchase_date);
		});
	router.route('/:token/:user_id/transaction/get/merchant/all/purchase_date/:store_user_id/:store_id/:unix_date_start/:unix_date_end')
		.get(function(req,res){
			validate(req,res,readData.transactions_by_merchant_id_and_purchase_date);
		});
	router.route('/:token/:user_id/transaction/get/endorser/payout/done/purchase_date/:endorser_user_id/:unix_date_start/:unix_date_end')
		.get(function(req,res){
			validate(req,res,readData.finished_payout_by_endorser_id_and_purchase_date);
		});
	router.route('/:token/:user_id/transaction/log/endorser/payout/true/:endorser_user_id/:shopstyx_transaction_id/:product_id')
		.post(function(req,res){
			validate(req,res,updateEndorser.setPaid);
		});
	router.route('/:token/:user_id/transaction/get/endorser/payout/pending/date/:endorser_user_id/:unix_date_start/:unix_date_end')
		.get(function(req,res){
			validate(req,res,readData.pending_payout_by_endorser_id_and_purchase_date);
		});
	router.route('/:token/:user_id/transaction/log/update/record/order/transaction/status/')
		.post(function(req,res){
			validate(req,res,modifyTransactionStatus.updateTransaction);
		});
	router.route('/:token/:user_id/transaction/log/update/record/order/per/merchant/transaction/status/')
		.post(function(req,res){
			validate(req,res,modifyTransactionStatus.updateTransactionOverallperMerchant);
		});
	router.route('/:token/:user_id/transaction/log/update/record/order/per/merchant/per/item/status_state/void/:shopstyx_order_id/:shopstyx_transaction_id/:product_id/:variant_id/')
		.post(function(req,res){
			validate(req,res,modifyTransactionStatus.updatePerItemVoid);
		});
	
	router.route('/:token/:user_id/transaction/log/update/record/order/per/merchant/per/item/status_state/dispute/:disputeStatus/:shopstyx_order_id/:shopstyx_transaction_id/:product_id/:variant_id/:is_robot/')
		.post(function(req,res){
			validate(req,res,modifyTransactionStatus.updateTransactionStatusDisputePerItem);
			//dispute,resolved,refunded(recalculate like void)
		});
	
	router.route('/:token/:user_id/transaction/log/get/record/order/per/merchant/transaction/status/')
		.get(function(req,res){
			validate(req,res,readData.getOverall_transaction_status);
		});
	router.route('/:token/:user_id/transaction/log/get/bought_item_status/')
		.get(function(req,res){
			validate(req,res,boughtItemStatuses.findCustomStatusInBoughtItems);
		});
	router.route('/:token/:user_id/transaction/log/all/merchant/transaction_list/')
		.get(function(req,res){
			validate(req,res,readData.getALLMerchantTransactions);
		});
	router.route('/:token/:user_id/transaction/log/all/customer/transaction_list/')
		.get(function(req,res){
			validate(req,res,readData.getALLCustomerTransactions);
		});
	router.route('/:token/:user_id/transaction/log/update/record/shipping_info/per/merchant/')
		.post(function(req,res){
			validate(req,res,modifyTransactionStatus.updateShippingInfo);
		});
	router.route('/:token/:user_id/transaction/log/update/is_emailed/true/:shopstyx_order_id/')
		.post(function(req,res){
			validate(req,res,modifyTransactionStatus.updateIsEmailedTrue);
		});


	// dispute logs
	//<source URL>/<token>/<user_id>/transaction/dispute/log/
	router.route('/:token/:user_id/transaction/dispute/log/')
		.post(function(req,res){
			validate(req,res,saveDispute.saveDispute);
		});
	//GET 
	router.route('/:token/:user_id/transaction/dispute/log/get/dispute/:shopstyx_transaction_id')
		.get(function(req,res){
			validate(req,res,getDispute.getDispute);
		});
	router.route('/:token/:user_id/transaction/log/get/orders/total/:type/:target_id')
		.get(function(req,res){
			validate(req,res,readData.totalOrdersTrans);
		});
	//PUT
	//modify item_purchased
	router.route('/:token/:user_id/transaction/dispute/log/update/item_purchased/:shopstyx_transaction_id')
		.post(function(req,res){
			validate(req,res,modifyDispute.modifyItem_purchased);
		});

	router.route('*')
		.get(function(req,res){
			msg = {
				status:"Error",
				message:"400 page not found",
				description: ""
			};
			GLOBAL.sendError(msg,res);
		})
		.post(function(req,res){
			msg = {
				status:"Error",
				message:"400 page not found",
				description: ""
			};
			GLOBAL.sendError(msg,res);
		})
		.put(function(req,res){
			msg = {
				status:"Error",
				message:"400 page not found",
				description: ""
			};
			GLOBAL.sendError(msg,res);
		})
		.delete(function(req,res){
			msg = {
				status:"Error",
				message:"400 page not found",
				description: ""
			};
			GLOBAL.sendError(msg,res);
		});

}

function validate(req,res,callback)
{
	try{
		var dataStruct = {
			"user_id":parseInt(req.params.user_id),
			"token":req.params.token,
			"req": req,
			"res": res,
			"new_unix_timestamp":'',
			"new_simple_timestamp":'',
			"timezone":"America/Los_Angeles"
		};
		dataStruct.new_simple_timestamp = mysqlStuff.MySQLDateTimeNOW2(dataStruct);
		meter.mark();
		//currently we have store_id and user_id fixed
		if(process.env.NODE_ENV=='development')// && parseInt(req.params.token) == 0)
		{
			callback(dataStruct);
		}else{
			verifyAccess.checkToken(dataStruct,callback);
		}
	}catch(err){
		var msg = {
			status:"Error",
			message:"[getOverall_transaction_status] Error Processing Data",
			description: err
		};
		GLOBAL.sendError(msg,dataStruct.res);
	}
}