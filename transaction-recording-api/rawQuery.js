var util = require('util');
var mongojs = require('mongojs');

GLOBAL.locationString = "104.130.206.240:27017,104.130.162.34:27017,104.239.135.106:27017";
var trailingArg = '?replicaSet=styx01';


GLOBAL.mongodbTransaction = mongojs('transactionUser:s#0pst!cks@'+GLOBAL.locationString+'/shopstyx_transactions'+trailingArg, ['transactionRecords','disputeRecords'], {authMechanism: 'ScramSHA1'});
GLOBAL.mongodbNotification = mongojs('notificationUser:s#0pst!cks@'+GLOBAL.locationString+'/shopstyx_notification'+trailingArg,['sharedAdvertisement','viewedAdvertisedProducts','viewedAdvertisedCollections','notificationRecords','chatRecords','chatIndex','prodchatIndex','orderchatIndex','storechatIndex','trueIndexCollection'], {authMechanism: 'ScramSHA1'}); 

console.log("Querying table "+process.argv[2]);
switch((process.argv[2]).toLowerCase())
{
	case "transactionrecords":
		var target = GLOBAL.mongodbTransaction.transactionRecords;
		break;
	case "disputerecords":
		var target = GLOBAL.mongodbTransaction.disputeRecords;
		break;
	case "sharedadvertisement":
		var target = GLOBAL.mongodbNotification.sharedAdvertisement;
		break;
	case "viewedadvertisedProducts":
		var target = GLOBAL.mongodbNotification.viewedAdvertisedProducts;
		break;
	case "viewedadvertisedCollections":
		var target = GLOBAL.mongodbNotification.viewedAdvertisedCollections;
		break;
	case "notificationrecords":
		var target = GLOBAL.mongodbNotification.notificationRecords;
		break;
	case "chatrecords":
		var target = GLOBAL.mongodbNotification.chatRecords;
		break;
	case "chatindex":
		var target = GLOBAL.mongodbNotification.chatIndex;
		break;
	case "prodchatindex":
		var target = GLOBAL.mongodbNotification.prodchatIndex;
		break;
	case "orderchatindex":
		var target = GLOBAL.mongodbNotification.orderchatIndex;
		break;
	case "storechatindex":
		var target = GLOBAL.mongodbNotification.storechatIndex;
		break;
	case "trueindexcollection":
		var target = GLOBAL.mongodbNotification.trueIndexCollection;
		break;
}
var query = process.argv[3];

console.log("query:");
console.log(query);

if(typeof(query)!="object")
{
	query = JSON.parse(query);
}
if(typeof(query._id)!='undefined')
{
	query._id=mongojs.ObjectId(query._id.toString());
}
target.find(query,function(err,data){
	if(err==null)
	{
		console.log(util.inspect(data,{showHidden: false, depth: null}));
	}else{
		console.log(util.inspect(err,{showHidden: false, depth: null}));
	}
});