// inital setup

var express = require('express');
var app = express();
var bodyParser = require('body-parser');
var logger = require('morgan');
var minify = require('express-minify');
var multer = require('multer');

var sqlConnect = require(__dirname + '/server/mysql_connects/db_conf');
//var compress = require('compression');
GLOBAL.router = express.Router();
var port = process.env.PORT || 8765;
GLOBAL.db_location = 'mapDB';
GLOBAL.http = "http://localhost:"+port;
GLOBAL.saved_images_location = '/images';//"/images/:shop_id/:user_id/:filename"
// configuration ===============================================================


if(process.env.NODE_ENV=='development'){
	GLOBAL.imagepathinternal = "https://shopstyx.com/cdn/uploads/product_images"; //type=0
	GLOBAL.imagepathexternal = "https://shopstyx.com/cdn/uploads/external_products"; //type=4
	GLOBAL.http = "http://localhost:"+port;
	//app.use(compress({threshold: 256}));
	//app.use(minify({cache: __dirname + '/cache'}));
	app.use(express.static(__dirname + '/public')); 		// set the static files location /public/img will be /img for users
	app.use(logger('combined')); 						// log every request to the console
	app.use(bodyParser.json()); 							// parse application/json
	app.use(bodyParser.urlencoded({extended:true}));		// parse application/x-www-form-urlencoded
	app.use(multer({ dest: './uploads/',
		rename: function (fieldname, filename) {
			return filename.replace(/\W+/g, '');//.toLowerCase() + Date.now()
		} 
  	}));				// parse multipart/form-data
	var db_init = {
		host     : 'localhost',
		user     : 'root',
		password : '',
		database : ''
	};
	sqlConnect.mysqlConnect(db_init);
}else if(process.env.NODE_ENV=='production'){
	//app.use(compress({threshold: 256}));
	GLOBAL.imagepathinternal = "https://shopstyx.com/cdn/uploads/product_images";
	GLOBAL.imagepathexternal = "https://shopstyx.com/cdn/uploads/external_products";
	GLOBAL.http = "http://shopstyx.com:"+port;
	app.use(minify({cache: __dirname + '/cache'}));
	app.use(express.static(__dirname + '/public')); 		// set the static files location /public/img will be /img for users
	app.use(logger()); 								// log every request to the console - default settings
	app.use(bodyParser.json()); 							// parse application/json
	app.use(bodyParser.urlencoded({extended:true}));		// parse application/x-www-form-urlencoded
	app.use(multer({ dest: './uploads/',
		rename: function (fieldname, filename) {
			//return filename.replace(/\W+/g, '-').toLowerCase() + Date.now()
			return filename.replace(/\W+/g, '');//.toLowerCase() + Date.now()
		} 
  	}));				// parse multipart/form-data
	var db_init = {
		host     : '3a7ff4b3d827722adc5ff9eaf30f56e25d924b46.rackspaceclouddb.com',
		user     : 'mymugzy',
		password : 'mymugzy!2009',
		database : ''
	};
	sqlConnect.mysqlConnect(db_init);
}else{
	GLOBAL.imagepathinternal = "https://shopstyx.com/cdn/uploads/product_images";
	GLOBAL.imagepathexternal = "https://shopstyx.com/cdn/uploads/external_products";
	GLOBAL.http = "http://104.130.66.55:"+port;
	//GLOBAL.http = "http://localhost:"+port;
	//app.use(minify({cache: __dirname + '/cache'}));
	app.use(express.static(__dirname + '/public')); 		// set the static files location /public/img will be /img for users
	app.use(logger('combined')); 						// log every request to the console
	app.use(bodyParser.json()); 							// parse application/json
	app.use(bodyParser.urlencoded({extended:true}));		// parse application/x-www-form-urlencoded
	app.use(multer({ dest: './uploads/',
		rename: function (fieldname, filename) {
			//return filename.replace(/\W+/g, '-').toLowerCase() + Date.now()
			return filename.replace(/\W+/g, '');//.toLowerCase() + Date.now()
		} 
  	}));				// parse multipart/form-data
	// var db_init = {
	// 	host     : '44fe862ed36373986601eac2094f12ad2d6c3c8c.rackspaceclouddb.com',
	// 	user     : 'mymugzy',
	// 	password : 'mymugzy!2009',
	// 	database : ''
	// };
	var db_init = {
		host     : '3a7ff4b3d827722adc5ff9eaf30f56e25d924b46.rackspaceclouddb.com',
		user     : 'mymugzy',
		password : 'mymugzy!2009',
		database : ''
	};
	sqlConnect.mysqlConnect(db_init);
}
//set the template engine
app.engine('.html',require('ejs').__express);
//set the template engine's template folder
app.set('views',__dirname+'/public');
//automatically set the extensions for all server side res.renders
app.set('view engine','html');

//load the routes
require('./server/routes')(GLOBAL.router);


//CORs Registration
//configure CORs
app.all('*', function(req, res, next) {
	// res.header("Access-Control-Allow-Origin", "*");
	// res.header("Access-Control-Allow-Headers", "X-Requested-With");
	// next();
	res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');

    // intercept OPTIONS method
    if ('OPTIONS' == req.method) {
      res.send(200);
    }
    else {
      next();
    }
});

// REGISTER OUR ROUTES -------------------------------
// all of our routes will be from root /
app.use('/', GLOBAL.router);

//start our server
app.listen(port);
console.log('starting server at port '+port);