(function(){
	var dataServices = angular.module('myDataServices',[]);

	dataServices.service('sharedEditorData',['$http',function($http){
		var lists = {
			fullList:{},
			adsList:{}
		};
		var triggers = {
			allowSave:false,
			createdEmbedCode:false
		};
		var clickedData = {
			lastClickedData:{}
		};
		var savedTags = {
			tags:[],
			files:[],
			bgfile:''
		};
		var savedAdverts = {};
		
		return {
			setFullList:function(data){
				lists.fullList = data;
			},
			getFullList:function(){
				return lists.fullList;
			},
			getFullListIndex:function(index){
				return lists.fullList[index];
			},
			clearFullList:function(){
				lists.fullList = {};
			},
			setAdList:function(data){
				lists.adsList=data;
			},
			getAdsList:function(){
				return lists.adsList;
			},
			clearAdsList:function(){
				lists.adsList={};
			},
			changeAllowSave:function(data){
				if(data===true || data===false)
				{
					triggers.allowSave=data;
				}else{
					triggers.allowSave=false;
				}
			},
			getAllowSave:function(){
				return triggers.allowSave;
			},
			saveCurrentClickedProduct:function(data){
				clickedData.lastClickedData=data;
			},
			getCurrentClickedProduct:function(){
				return clickedData.lastClickedData;
			},
			pushTagInformation:function(data){
				savedTags.tags.push(data);
			},
			removeTagInformation:function(index){
				savedTags.tags[index]='';
			},
			clearTagInformation:function(){
				savedTags.tags=[];
			},
			reindexTagInformation:function(){
				var temp=[];
				savedTags.tags.forEach(function(data){
					if(data!='')
					{
						temp.push(data);
					}
				});
				savedTags.tags=temp;
			},
			getTagInformation:function(){
				return savedTags.tags;
			},
			getTagCount:function(){
				return savedTags.tags.length;
			},
			saveEmbedCodeStatus:function(data){
				if(data===true || data===false)
				{
					triggers.createdEmbedCode=data;
				}else{
					triggers.createdEmbedCode=false;
				}
			},
			getEmbedCodeFlag:function(){
				return triggers.createdEmbedCode;
			},
			pushFiles:function(data){
				savedTags.files.push(data);
			},
			getFiles:function(){
				return savedTags.files;
			},
			clearFiles:function(){
				savedTags.files=[];
			},
			saveBgFile:function(data){
				savedTags.bgfile=data;
			},
			getBgFile:function(){
				return savedTags.bgfile;
			},
			clearBgFile:function(){
				savedTags.bgfile=''
			},
			pullSavedAdvertisement:function(callback){
				var target = {
		            method:"GET",
		            url:GlobalHost+'/photostyx/allsaved/'+store_id+'/'+user_id
		        };
		        var fullList = $http(target);
				fullList.success(function(data, status, headers, config) {
		            savedAdverts = data;
		            callback();
		        });
		        fullList.error(function(data, status, headers, config) {
		            alert("Cannot access main Server for Product List!\ntry reloading the page");
		        });
			},
			getSavedAdvertisement:function(){
				return savedAdverts;
			},
			getSpecificSavedAdvertisement:function(x){
				return savedAdverts[x];
			},
			addslashes:function(str) 
			{
			  //  discuss at: http://phpjs.org/functions/addslashes/
			  // original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
			  // improved by: Ates Goral (http://magnetiq.com)
			  // improved by: marrtins
			  // improved by: Nate
			  // improved by: Onno Marsman
			  // improved by: Brett Zamir (http://brett-zamir.me)
			  // improved by: Oskar Larsson Högfeldt (http://oskar-lh.name/)
			  //    input by: Denny Wardhana
			  //   example 1: addslashes("kevin's birthday");
			  //   returns 1: "kevin\\'s birthday"

			  return (str + '')
			    .replace(/[\\"']/g, '\\$&')
			    .replace(/\u0000/g, '\\0');
			},
			stripslashes:function(str) 
			{
			  //       discuss at: http://phpjs.org/functions/stripslashes/
			  //      original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
			  //      improved by: Ates Goral (http://magnetiq.com)
			  //      improved by: marrtins
			  //      improved by: rezna
			  //         fixed by: Mick@el
			  //      bugfixed by: Onno Marsman
			  //      bugfixed by: Brett Zamir (http://brett-zamir.me)
			  //         input by: Rick Waldron
			  //         input by: Brant Messenger (http://www.brantmessenger.com/)
			  // reimplemented by: Brett Zamir (http://brett-zamir.me)
			  //        example 1: stripslashes('Kevin\'s code');
			  //        returns 1: "Kevin's code"
			  //        example 2: stripslashes('Kevin\\\'s code');
			  //        returns 2: "Kevin\'s code"

			  return (str + '')
			    .replace(/\\(.?)/g, function(s, n1) {
			      switch (n1) {
			        case '\\':
			          return '\\';
			        case '0':
			          return '\u0000';
			        case '':
			          return '';
			        default:
			          return n1;
			      }
			    });
			},
			mysql_real_escape_string:function(str) 
			{
			    return str.replace(/[\0\x08\x09\x1a\n\r"'\\\%]/g, function (char) {
			        switch (char) {
			            case "\0":
			                return "\\0";
			            case "\x08":
			                return "\\b";
			            case "\x09":
			                return "\\t";
			            case "\x1a":
			                return "\\z";
			            case "\n":
			                return "\\n";
			            case "\r":
			                return "\\r";
			            case "\"":
			            case "'":
			            case "\\":
			            case "%":
			                return "\\"+char; // prepends a backslash to backslash, percent,
			                                  // and double/single quotes
			        }
			    });
			}
		};
	}]);
})();