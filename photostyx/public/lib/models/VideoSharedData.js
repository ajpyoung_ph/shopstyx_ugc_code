(function(){
	var dataServices = angular.module('VideoSharedServices',['stringManilpulationService']);

	dataServices.service('VideoSharedEditorData',['$http','stringMan',function($http,stringMan){
		var clickedData = {
			lastClickedData:{}
		};
		var lists = {
			fullList:{},
			adsList:{}
		};
		var flags = {
			allowSave:false
		};
		var playerData = {
			progress:0,
			width:750
		};
		var VidAdsDataTags = {};
		return{
			setFullList:function(data){
				lists.fullList = data;
			},
			getFullList:function(){
				return lists.fullList;
			},
			getFullListIndex:function(index){
				return lists.fullList[index];
			},
			clearFullList:function(){
				lists.fullList = {};
			},
			setAdList:function(data){
				lists.adsList=data;
			},
			getAdsList:function(){
				return lists.adsList;
			},
			clearAdsList:function(){
				lists.adsList={};
			},
			setAllowSave:function(data){
				if(data!=true || data!=false)
				{
					flags.allowSave=false;
				}else{
					flags.allowSave=data;
				}
			},
			getAllowSave:function(){
				return flags.allowSave;
			},
			saveCurrentClickedProduct:function(data){
				clickedData.lastClickedData=data;
			},
			getCurrentClickedProduct:function(){
				return clickedData.lastClickedData;
			},
			setVideoPlayerProgress:function(percentage){
				playerData.progress = percentage;
			},
			getVideoPlayerProgress:function(){
				return playerData.progress;
			},
			clearVideoPlayerProgress:function(){
				playerData.progress=0;
			},
			pushVidTags:function(data){
				VidAdsDataTags.push(data);
			},
			clearVidTags:function(){
				VidAdsDataTags = {};
			},
			getAllVidTags:function(){
				return VidAdsDataTags;
			},
			getOneVidTags:function(index){
				return VidAdsDataTags[index];
			},
			setVidWidth:function(width){
				var videoplayer = document.getElementById('output');
				var playpausebutton = document.getElementById('vidplayerplaypause');
				//progress bars
				var progressbargroup = document.getElementById('progressbargroup');
				var playerstatusbar = document.getElementById('playerstatusbar');
				var playerSliderBar = document.getElementById('playerslider');
				videoplayer.style.maxWidth=width+'px';
				var newProgressLength = videoplayer.offsetWidth - playpausebutton.offsetWidth;
				progressbargroup.style.maxWidth = newProgressLength;
				playerstatusbar.style.maxWidth = newProgressLength;
				playerSliderBar.style.maxWidth = newProgressLength;
				playerData.width = videoplayer.offsetWidth;
			},
			getVidWidth:function(width){
				return playerData.width;
			},
			insertAdvertTimeBar:function(starttime,endtime,totaltime,widthpx,index){
				var onePxTime = totaltime/widthpx;
				var startpx = Math.floor(starttime/onePxTime);
				var endpx = Math.floor(endtime/onePxTime);
				var numpixels = endpx - startpx;
				var string = '<div name="vidMarker" id="vidMarker'+index+'" style="width:'+numpixels+'px;height:20px;position:absolute;left:'+startpx+'px;top:0px;background-color:rgba(255,255,0,0.5);"></div>';
				console.log(string);
				return string;
			},
			insertPriceTags:function(event,currentClickedProduct,tag_count){
				var prod_desc = stringMan.mysql_real_escape_string(currentClickedProduct.product_desc);
            	var prod_name = stringMan.mysql_real_escape_string(currentClickedProduct.product_name);
				var string = '<div style="position: absolute; top: '+event.layerY+'px; left: '+event.layerX+'px;" id="tag'+tag_count+'" name="tag_item"  context-menu="testfunction();" data-target="rightclickcontext'+tag_count+'" ng-class="{ \'highlight\': highlight, \'expanded\' : expanded }"><a href="'+currentClickedProduct.url+'" onmouseover="Tip(\'<span><b></b>'+prod_name+'<br/>'+currentClickedProduct.price+' '+currentClickedProduct.currency+'<br/><br/>'+prod_desc+'</span>\',HEIGHT,-300,WIDTH,-250,JUMPHORZ,true,JUMPVERT,true,BALLOON,true,BALLOONSTEMOFFSET,-30,OFFSETX,-15,ABOVE,true);" onmouseout="UnTip();" target="_blank"><img src="'+GlobalHost+'/images/pricetag.png"/></a></div><div class="dropdown position-fixed" id="rightclickcontext'+tag_count+'" name="rightclickcontext"><ul class="dropdown-menu" role="menu"><li><a class="pointer" role="menuitem" tabindex="1" ng-click="deleteTag($event,$data,'+tag_count+')">Delete Tag</a></li></ul></div>';
				return string;
			},
			clearAdvertisementTagsDisplay : function()
	        {
	            while(document.querySelector( '[name="tag_item"]' )!=null)
	            {
	                var target = angular.element( document.querySelector( '[name="tag_item"]' ) );
	                target.remove();
	            }
	            while(document.querySelector( '[name="rightclickcontext"]' )!=null)
	            {
	                var targetmenu = angular.element( document.querySelector('[name="rightclickcontext"]'));
	                targetmenu.remove();
	            }
	            while(document.querySelector( '[name="vidMarker"]' )!=null)
	            {
	                var targetyellow = angular.element( document.querySelector('[name="vidMarker"]'));
	                targetyellow.remove();
	            }
	        }

		};
	}]);
})();