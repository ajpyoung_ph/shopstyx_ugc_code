(function(){
	var dataServices = angular.module('pullProductInformation',[]);

	dataServices.service('pullProdProccess',['$http',function($http){
		return {
			getProduct:function(dataStruct,sharedData)
			{
				var GlobalHost = dataStruct.GlobalHost;
				var store_id = dataStruct.store_id;
				var user_id = dataStruct.user_id;

				var target = {
					method:"GET",
					url:GlobalHost+'/storeproducts/all/'+store_id+'/'+user_id
				}
				var fullList = $http(target);
				// scope.serverData={
				// 	fullList : {},
				// 	advertList : {}
				// };
				fullList.success(function(data, status, headers, config) {
					//scope.serverData.fullList = data;
					sharedData.setFullList(data);
				});
				fullList.error(function(data, status, headers, config) {
					alert("Cannot access main Server for Product List!\ntry reloading the page");
				});
			}
		};
	}]);
})();