(function(){
	var PhotoStyx = angular.module('mainController',['productDisplayPartialControllers','productDisplayEventDirectives','pullProductInformation','myDataServices','ui.bootstrap','ngDragDrop','ng-context-menu']);
	//GlobalHost = 'http://localhost:8765';
    // user_id = 0;
    // store_id = 0;
    //http://localhost:8765/storeproducts/all/0/0
	tag_count = 0;
    PhotoStyx.controller('mainPageController',['$scope','$http','sharedEditorData','pullProdProccess',function($scope, $http, sharedEditorData,pullProdProccess){
        
  //       var target = {
  //           method:"GET",
  //           url:GlobalHost+'/storeproducts/all/'+store_id+'/'+user_id
  //       }
		// var fullList = $http(target);
		// $scope.serverData={
  //           fullList : {},
  //           advertList : {}
  //       };
		// fullList.success(function(data, status, headers, config) {
  //           $scope.serverData.fullList = data;
  //       });
  //       fullList.error(function(data, status, headers, config) {
  //           alert("Cannot access main Server for Product List!\ntry reloading the page");
  //       });
        var dataStruct = {
            "GlobalHost":GlobalHost,
            "user_id":user_id,
            "store_id":store_id
        };
        pullProdProccess.getProduct(dataStruct,sharedEditorData);
       
        var getFullList = function()
        {
            return sharedEditorData.getFullList();
        };
        var getAdsList = function()
        {
            return sharedEditorData.getAdsList();
        };
        $scope.reflectData=function()
        {
            return sharedEditorData.getSavedAdvertisement();
        };
        $scope.exposed = {
            getFullList:getFullList,
            getAdsList:getAdsList
        };
        sharedEditorData.changeAllowSave(false);
        
        var digestme = function()
        {
            reflectData();
            $scope.$digest();
        }
        sharedEditorData.pullSavedAdvertisement($scope.reflectData);
        
        //capture clicked item value
        $scope.getData=function(index,event)
        {
           //sharedEditorData.saveCurrentClickedProduct($scope.serverData.fullList[index]);
           sharedEditorData.saveCurrentClickedProduct(sharedEditorData.getFullListIndex(index));
           $scope.$digest();
        }
        //drop success
        $scope.dropSuccessHandler=function(index,event)
        {
            //get mouse location
            //place price tag
            //generate hover
            console.log(event);
        }
        //custom file select event capture
        //listen for the file selected event
        $scope.$on("fileSelected", function (event, args) {
            $scope.$apply(function () {            
                //add the file object to the scope's files collection
                sharedEditorData.pushFiles(args.file);
            });
        });
	}]);

    PhotoStyx.controller('editorController',['$scope','$http','$timeout','$compile','$window','sharedEditorData',function($scope,$http,$timeout,$compile,$window,sharedEditorData){

        $scope.modeValues ={
            edit:true
        };
        $scope.allowSave = function()
        {
            return sharedEditorData.getAllowSave();
        }
        $scope.showData = function()
        {
            return sharedEditorData.getCurrentClickedProduct();
        }
        $scope.generatedEmbed=function()
        {
            return sharedEditorData.getEmbedCodeFlag();
        }
        $scope.trimvalue = function(target)
        {
            var string = target.val();
            var newString = string.replace(/\W/g, '');
            target.val(newString);
            $scope.updateEmbed();
        }
        $scope.updateEmbed = function()
        {
            $timeout(updateEmbedValue(), 2000);
        }
        $scope.getSavedData = function(index,event)
        {
            var data = sharedEditorData.getSpecificSavedAdvertisement(index);
            reconstructAdvert(data);
        }
        $scope.alertme = function(string)
        {
            $window.alert(string);
        }
        $scope.deleteTag = function(event,data,index)
        {
            document.getElementById('tag'+index).remove();
            document.getElementById('rightclickcontext'+index).remove();
            sharedEditorData.removeTagInformation(index);
            console.log(sharedEditorData.getTagInformation());
        }
        $scope.UnTip = function()
        {
            $window.UnTip();
        }
        $scope.moveTag = function(targetDiv, event)
        {
            $scope.closeCallback();
            moveEvent.status=true;
            console.log(event);
        }
        
        var updateEmbedValue = function()
        {
            // if(document.getElementById('output').width==0 || document.getElementById('output').height==0)
            // {
                
            // }
            var string = '<iframe src="'+GlobalHost+'/photostyx/regen/'+store_id+'/'+user_id+'/'+document.getElementById('filename').value+'" height="'+document.getElementById('output').height+'px" width="'+document.getElementById('output').width+'px" scrolling="no"></iframe>';
            document.getElementById("embedCode").size = string.length;
            document.getElementById('embedCode').value = string;
        }
        var reconstructTags = function(data)
        {
            if(data.dataStruct!=undefined)
            {
                var mydata = data.dataStruct;
            }else{
                var mydata = data;
                sharedEditorData.clearTagInformation();
            }
            tag_count=0;
            mydata.forEach(function(tagdata){
                sharedEditorData.pushTagInformation(tagdata);
                //var target = angular.element( document.querySelector( '#compoundImage' ) );
                var prod_desc = sharedEditorData.mysql_real_escape_string(tagdata.ProductInformation.product_desc);
                var prod_name = sharedEditorData.mysql_real_escape_string(tagdata.ProductInformation.product_name);
                //var priceTag ='<div style="position: absolute; top: '+tagdata.layerY+'px; left: '+tagdata.layerX+'px;" name="tag_item"><a href="'+tagdata.ProductInformation.url+'" onmouseover="Tip(\'<span><b></b>'+prod_name+'<br/>'+tagdata.ProductInformation.price+' '+tagdata.ProductInformation.currency+'<br/><br/>'+prod_desc+'</span>\',HEIGHT,-300,WIDTH,-250,JUMPHORZ,true,JUMPVERT,true);" onmouseout="UnTip();" ng-right-click="alert(\'right click\');" target="_blank"><img src="'+GlobalHost+'/images/pricetag.png"/></a></div>';
                var priceTag = '<div style="position: absolute; top: '+tagdata.layerY+'px; left: '+tagdata.layerX+'px;" id="tag'+tag_count+'" name="tag_item" context-menu="UnTip();" data-target="rightclickcontext'+tag_count+'" ng-class="{ \'highlight\': highlight, \'expanded\' : expanded }"><a href="'+tagdata.ProductInformation.url+'" onmouseover="Tip(\'<span><b></b>'+prod_name+'<br/>'+tagdata.ProductInformation.price+' '+tagdata.ProductInformation.currency+'<br/><br/>'+prod_desc+'</span>\',HEIGHT,-300,WIDTH,-250,JUMPHORZ,true,JUMPVERT,true,BALLOON,true,BALLOONSTEMOFFSET,-30,OFFSETX,-15,ABOVE,true);" onmouseout="UnTip();" target="_blank"><img src="'+GlobalHost+'/images/pricetag.png"/></a></div><div class="dropdown position-fixed" id="rightclickcontext'+tag_count+'" name="rightclickcontext"><ul class="dropdown-menu" role="menu"><li><a class="pointer" role="menuitem" tabindex="1" ng-click="deleteTag($event,$data,'+tag_count+')">Delete Tag</a></li></ul></div>';
                
                var linkFn = $compile(priceTag);
                var content = linkFn($scope);

                angular.element(document.getElementById('compoundImage')).append(content);
                tag_count=tag_count+1;
                //angular.element(document.getElementById('space-for-buttons')).append($compile("<div><button class='btn btn-default' data-alert="+scope.count+">Show alert #"+scope.count+"</button></div>")(scope));
            });
        }
        var reconstructAdvert = function(data)
        {
            //initialize environment
            tag_count=0;
            sharedEditorData.changeAllowSave(true);
            $scope.clearAdvertisement();
            document.getElementById('output').src = data.sourceloc+data.bgfile;
            document.getElementById('filename').value = data.filename;
            document.getElementById('filename').readonly = 'readonly';
            
            reconstructTags(data);

            console.log("finished reconstruction");
            console.log(sharedEditorData.getTagInformation());

            $scope.updateEmbed();
            if(sharedEditorData.getAllowSave()==false)
            {
                sharedEditorData.changeAllowSave(true);
                $scope.$digest();//to trigger a check for the change in value...   
            }
        }
        $scope.openFile = function(target){
            tag_count=0;
            var input = target;

            var reader = new FileReader();
            reader.onload = function(){
                var dataURL = reader.result;
                var output = document.getElementById('output');
                output.src = dataURL;
            };
            reader.readAsDataURL(input.files[0]);
            //extract filename
            var filename_arr = target.value.split('\\');
            var rem_ext = filename_arr[filename_arr.length - 1];
            sharedEditorData.saveBgFile(rem_ext);
            rem_ext = rem_ext.split('.');
            var filename = rem_ext[0] + "_advertisement";
            document.getElementById('filename').value = filename.replace(/\W/g, '');

            if(sharedEditorData.getAllowSave()==false)
            {
                sharedEditorData.changeAllowSave(true);
                $scope.$digest();//to trigger a check for the change in value...   
            }
        }
        $scope.onDrop = function(event,data)
        {
            sharedEditorData.reindexTagInformation();
            tag_count=sharedEditorData.getTagCount();
            clearAdvertisementTags();
            reconstructTags(sharedEditorData.getTagInformation());
            var currentClickedProduct = sharedEditorData.getCurrentClickedProduct();
            var currBgFile = sharedEditorData.getBgFile();
            var target = angular.element( document.querySelector( '#compoundImage' ) );
            //var priceTag = '<div style="position: absolute; top: '+event.y+'px; left: '+event.x+'px;" class="right-tip" data-tips="'+currentClickedProduct.product_name+' - '+currentClickedProduct.price+' '+currentClickedProduct.currency+'"><img src="'+GlobalHost+'/images/pricetag.png"/></div>';
            //var priceTag ='<div style="position: absolute; top: '+event.layerY+'px; left: '+event.layerX+'px;" name="tag_item"><a href="'+currentClickedProduct.url+'" class="cooltooltip" target="_blank"><img src="'+GlobalHost+'/images/pricetag.png"/><span><b></b>'+currentClickedProduct.product_name+'<br/>'+currentClickedProduct.price+' '+currentClickedProduct.currency+'<br/><br/>'+currentClickedProduct.product_desc+'</span></a>';
            var prod_desc = sharedEditorData.mysql_real_escape_string(currentClickedProduct.product_desc);
            var prod_name = sharedEditorData.mysql_real_escape_string(currentClickedProduct.product_name);
            var priceTag ='<div style="position: absolute; top: '+event.layerY+'px; left: '+event.layerX+'px;" id="tag'+tag_count+'" name="tag_item"  context-menu="testfunction();" data-target="rightclickcontext'+tag_count+'" ng-class="{ \'highlight\': highlight, \'expanded\' : expanded }"><a href="'+currentClickedProduct.url+'" onmouseover="Tip(\'<span><b></b>'+prod_name+'<br/>'+currentClickedProduct.price+' '+currentClickedProduct.currency+'<br/><br/>'+prod_desc+'</span>\',HEIGHT,-300,WIDTH,-250,JUMPHORZ,true,JUMPVERT,true,BALLOON,true,BALLOONSTEMOFFSET,-30,OFFSETX,-15,ABOVE,true);" onmouseout="UnTip();" target="_blank"><img src="'+GlobalHost+'/images/pricetag.png"/></a></div><div class="dropdown position-fixed" id="rightclickcontext'+tag_count+'" name="rightclickcontext"><ul class="dropdown-menu" role="menu"><li><a class="pointer" role="menuitem" tabindex="1" ng-click="deleteTag($event,$data,'+tag_count+')">Delete Tag</a></li></ul></div>';
            
            var linkFn = $compile(priceTag);
            var content = linkFn($scope);
            //class="top-tip" data-tips="Go to bavotasan.com"
            //layerX/layerY
            //offsetX/offsetY
            //screenX/screenY
            //x/y
            tag_count=tag_count+1;
            var tagInformation={
                layerX:event.layerX,
                layerY:event.layerY,
                offsetX:event.offsetX,
                offsetY:event.offsetY,
                screenX:event.screenX,
                screenY:event.screenY,
                x:event.x,
                y:event.y,
                imgHeight:document.getElementById('output').height,
                imgWidth:document.getElementById('output').width,
                bgfile:currBgFile,
                ProductInformation: currentClickedProduct
            }
            sharedEditorData.pushTagInformation(tagInformation);
            target.append(content);
            // console.log(data);
            // console.log(event);
            // add price tag.png onto the page
            // tag this with the mouse down information
            $scope.updateEmbed();
            console.log(sharedEditorData.getTagInformation());
        }
        var clearAdvertisementTags = function()
        {
            while(document.querySelector( '[name="tag_item"]' )!=null)
            {
                var target = angular.element( document.querySelector( '[name="tag_item"]' ) );
                target.remove();
            }
            while(document.querySelector( '[name="rightclickcontext"]' )!=null)
            {
                var targetmenu = angular.element( document.querySelector('[name="rightclickcontext"]'));
                targetmenu.remove();
            }
        }
        $scope.clearAdvertisement = function()
        {
            document.getElementById('output').src='';
            document.getElementById('filename').value='';
            document.getElementById('embedCode').value='';
            clearAdvertisementTags();
            sharedEditorData.clearBgFile();
            sharedEditorData.clearTagInformation();
            sharedEditorData.changeAllowSave(false);
            sharedEditorData.saveEmbedCodeStatus(false);
            tag_count=0;
        }

        $scope.saveAdvertisement = function(state) {
            sharedEditorData.reindexTagInformation();
            tag_count=sharedEditorData.getTagCount();
            var files = sharedEditorData.getFiles();
            var modelData = sharedEditorData.getTagInformation();
            if(state==undefined || state=='')
            {
                state='POST';
            }
            $http({
                method: state,
                url: GlobalHost+'/photostyx/'+store_id+'/'+user_id+'/'+document.getElementById('filename').value,
                //IMPORTANT!!! You might think this should be set to 'multipart/form-data' 
                // but this is not true because when we are sending up files the request 
                // needs to include a 'boundary' parameter which identifies the boundary 
                // name between parts in this multi-part request and setting the Content-type 
                // manually will not set this boundary parameter. For whatever reason, 
                // setting the Content-type to 'false' will force the request to automatically
                // populate the headers properly including the boundary parameter.
                headers: { 'Content-Type': undefined },
                //This method will allow us to change how the data is sent up to the server
                // for which we'll need to encapsulate the model data in 'FormData'
                transformRequest: function (data) {
                    var formData = new FormData();
                    //need to convert our json object to a string version of json otherwise
                    // the browser will do a 'toString()' on the object which will result 
                    // in the value '[Object object]' on the server.
                    //formData.append("model", angular.toJson(data.model));//I am assuming that data = $scope, since we're not really using scope I'll push the shared data component
                    formData.append("model", angular.toJson(modelData));
                    //now add all of the assigned files
                    
                    for (var i = 0; i < files.length; i++) {
                        //add each file to the form data and iteratively name them
                        formData.append("file" + i, files[i]);
                    }
                    return formData;
                },
                //Create an object that contains the model and files which will be transformed
                // in the above transformRequest method
                //data: { model: $scope.model, files: $scope.files }
                data: { model: modelData, files: files }
            }).
            success(function (data, status, headers, config) {
                if(status==200)
                {
                    if(data.status=="Error")
                    {
                        if(data.message == "File Already Exists")
                        {
                            var choice = confirm("Filename specified already Exists.\nDo you wish to overwrite?");
                            if(choice == true)
                            {
                                $scope.saveAdvertisement('PUT');
                            }
                        }else{
                            alert(data.message+"\n\n"+data.description);
                        }
                    }else{
                        sharedEditorData.saveEmbedCodeStatus(true);
                        alert("Saved Advertisement");
                        //$scope.clearAdvertisement();
                    }
                    sharedEditorData.pullSavedAdvertisement($scope.reflectData);
                    $scope.$digest();
                }else{
                    alert("Advertisement NOT SAVED!");
                }
                $scope.clearAdvertisement();
                $scope.reconstructAdvert(sharedEditorData.getTagInformation());
            }).
            error(function (data, status, headers, config) {
                alert("Failed communication to Server!");
            });
        };
    }]);
    PhotoStyx.filter('limitStyxText', function () {
        return function (value, wordwise, max, tail) {
            if (!value) return '';

            max = parseInt(max, 10);
            if (!max) return value;
            if (value.length <= max) return value;

            value = value.substr(0, max);
            if (wordwise) {
                var lastspace = value.lastIndexOf(' ');
                if (lastspace != -1) {
                    value = value.substr(0, lastspace);
                }
            }

            return value + (tail || ' …');
        };
    });
})();