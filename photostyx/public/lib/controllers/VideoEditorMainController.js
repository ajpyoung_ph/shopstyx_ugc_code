(function(){

	var VidStyx = angular.module('VideoEditorMainController',['productDisplayPartialControllers','productDisplayEventDirectives','pullProductInformation','VideoSharedServices','stringManilpulationService','ui.bootstrap','ngDragDrop','ng-context-menu']);

	tag_count=0;
	movie_initialized=false;

	VidStyx.controller('videomainPageController',['pullProdProccess','VideoSharedEditorData','stringMan','$scope',function(pullProdProccess,VideoSharedEditorData,stringMan,$scope){

		var dataStruct = {
			"GlobalHost":GlobalHost,
			"user_id":user_id,
			"store_id":store_id
		};
		
		pullProdProccess.getProduct(dataStruct,VideoSharedEditorData);
		var getFullList = function()
		{
			return VideoSharedEditorData.getFullList();
		}
		var getAdsList = function()
		{
			return VideoSharedEditorData.getAdsList();
		}
		
		$scope.exposed = {
			getFullList:getFullList,
			getAdsList:getAdsList
		};
		$scope.getData=function(index,event)
        {
           //sharedEditorData.saveCurrentClickedProduct($scope.serverData.fullList[index]);
           VideoSharedEditorData.saveCurrentClickedProduct(VideoSharedEditorData.getFullListIndex(index));
        }
	}]);

	VidStyx.controller('VideoEditorController',['VideoSharedEditorData','stringMan','$scope',function(VideoSharedEditorData,stringMan,$scope){
		// $scope.play_value=function(){
		// 	return VideoSharedEditorData.getVideoPlayerProgress();
		// }
		var openFile = function(target)
		{
			tag_count=0;
			if(movie_initialized==false)
			{
				initializeVideo();
				initialVideoDropZone();
			}
            var input = target;

            var reader = new FileReader();
            reader.onload = function(){
                var dataURL = reader.result;
                var output = document.getElementById('output');
                output.src = dataURL;
            };
            reader.onprogress = function(event){
            	//console.log(event);
            	if(event.lengthComputable==true)
            	{
            		$scope.exposed.movieLoadDataProgressBar = parseFloat(event.loaded/event.total);
            	}else{
            		$scope.exposed.movieLoadDataProgressBar = "Done";
            	}
            	//angular.element(document.getElementById('movieloadprogressbar'))
            }
            reader.onloadend = function(event){
            	console.log(event);
            }
            reader.readAsDataURL(input.files[0]);
            movie_initialized=true;
		}
		var changewidth = function(target)
		{
			console.log(target);
			VideoSharedEditorData.setVidWidth(target.value);
		}
		$scope.exposed = {
			openFile : openFile,
			trimvalue : stringMan.trimvalue,
			allowSave : VideoSharedEditorData.getAllowSave,
			movieLoadDataProgressBar:0,
			changewidth:changewidth
		};
		var initializeVideo = function(){
			videoplayer = document.getElementById('output');
			playpausebutton = document.getElementById('vidplayerplaypause');
			//progress bars
			//loadingProgressBar = document.getElementById('loaded');
			//playingProgressBar = document.getElementById('playerstatusbar');
			progressbargroup = document.getElementById('progressbargroup');
			playerstatusbar = document.getElementById('playerstatusbar');
			playerSliderBar = document.getElementById('playerslider');

			// Hide the browser's default controls
			videoplayer.controls = false;

			//setup our events
			videoplayer.addEventListener('timeupdate', updateProgressBar, false);//update all 3 progress bars
			playerslider.addEventListener('change',changeVidPosition,false);
			playerslider.addEventListener('input',changeVidPosition,false);

		};
		var initialVideoDropZone=function(){
			draglayer = document.getElementById('videoDropLayer');
		}
		var updateProgressBar = function()
		{
			var percentage = Math.floor((100 / videoplayer.duration) * videoplayer.currentTime);
			// loadingProgressBar.value = percentage;
			// playingProgressBar.value = percentage;
			playerSliderBar.value = percentage;
			// playingProgressBar.innerHTML = percentage + '% played';
			VideoSharedEditorData.setVideoPlayerProgress(percentage);
			//check if ad should be in view
			var adlist = VideoSharedEditorData.getAllVidTags();
			if((typeof adlist)=="object")
			{
				if(adlist.length!=undefined)
				{
					
				}
			}
			$scope.$digest();
		};
		var changeVidPosition = function()
		{
			//alert(playerSliderBar.value);
			// percentage = (curr / total)*100
			// (percentage/100)*total = curr
			var percentage_local = playerSliderBar.value;
			var currentTime = videoplayer.duration*(percentage_local/100);
			videoplayer.currentTime=currentTime;
			playerSliderBar.value = percentage_local;
			VideoSharedEditorData.setVideoPlayerProgress(percentage_local);
			$scope.$digest();
		};
	}]);
	
	VidStyx.controller('VideoPlayerController',['VideoSharedEditorData','stringMan','$scope','$compile',function(VideoSharedEditorData,stringMan,$scope,$compile){

		var playVideo = function(target){
			document.getElementById('output').play();
			// draglayer.width = videoplayer.offsetWidth;
			// draglayer.height = videoplayer.height;
			document.getElementById('progressbargroup').width = videoplayer.offsetWidth;
		}
		$scope.play_value=function(){
			return VideoSharedEditorData.getVideoPlayerProgress();
		}
		var onDrop = function(event,data)
		{
			//pause video
			videoplayer.pause();
			//get current time
			var endtime = videoplayer.currentTime+10;
			if(endtime>videoplayer.duration)
			{
				endtime = videoplayer.duration;
			}
			var currentProduct = VideoSharedEditorData.getCurrentClickedProduct();
			var pricetagDiv = VideoSharedEditorData.insertPriceTags(event,currentProduct,tag_count,"priceTag");
			var linkFn = $compile(pricetagDiv);
            var content = linkFn($scope);
            var target = angular.element(document.querySelector( '#pricetagLists' ));
			target.append(content);
			var pricetagInformation = {
				"event":event,
				"productInformation":currentProduct,
				"type":"priceTag"
			};

			var vidWidth = VideoSharedEditorData.getVidWidth();
			var yellowVidMarker = VideoSharedEditorData.insertAdvertTimeBar(videoplayer.currentTime,endtime,videoplayer.duration,vidWidth,tag_count);
			linkFn = $compile(yellowVidMarker);
            content = linkFn($scope);
            target = angular.element(document.querySelector( '#positionLayer' ));
			target.append(content);
			tag_count=tag_count+1;
			var yellowVidMarkerInformation={
				"startTime":videoplayer.currentTime,
				"endTime":endtime,
				"totalTime":videoplayer.duration,
				"vidWidth":vidWidth
			};
			//save tag information
			data = {
				"pricetagInformation":pricetagInformation,
				"yellowVidMarkerInformation":yellowVidMarkerInformation
			};
			VideoSharedEditorData.pushVidTags(data);
		}
		var reconstructTags = function(data)
		{
			//this assumes the data has been cleaned and reindexed
			//clear ads
			VideoSharedEditorData.clearAdvertisementTagsDisplay();
			//reset counter
			tag_count=0;
			//display tags and types of tags
			data.forEach(function(items){
				var event = items.pricetagInformation.event;
				var currentProduct = items.pricetagInformation.productInformation;
				var type = items.pricetagInformation.type;
				var pricetagDiv = VideoSharedEditorData.insertPriceTags(event,currentProduct,tag_count,type);
				var linkFn = $compile(pricetagDiv);
	            var content = linkFn($scope);
	            var target = angular.element(document.querySelector( '#pricetagLists' ));
				target.append(content);

				var vidWidth = items.yellowVidMarkerInformation.vidWidth;
				var yellowVidMarker = VideoSharedEditorData.insertAdvertTimeBar(items.yellowVidMarkerInformation.startTime,items.yellowVidMarkerInformation.endTime,totalTime,items.yellowVidMarkerInformation.vidWidth,tag_count);
				linkFn = $compile(yellowVidMarker);
	            content = linkFn($scope);
	            target = angular.element(document.querySelector( '#positionLayer' ));
				target.append(content);

				tag_count=tag_count+1;
			});
		}
		$scope.exposed = {
			playVideo:playVideo,
			onDrop:onDrop
		};
	}]);

	VidStyx.filter('limitStyxText', function () {
        return function (value, wordwise, max, tail) {
            if (!value) return '';

            //max = parseInt(max, 10);
            if (!max) return value;
            if (value.length <= max) return value;

            value = value.substr(0, max);
            if (wordwise) {
                var lastspace = value.lastIndexOf(' ');
                if (lastspace != -1) {
                    value = value.substr(0, lastspace);
                }
            }

            return value + (tail || ' …');
        };
    });
})();