(function(){
	var mydirectives = angular.module('productDisplayPartialControllers',[]);

	mydirectives.directive('listOfproducts',function(){
		return{
			restrict:'E',//E as an Element or A as a Attribute
			templateUrl:"./lib/partials/listOfProducts.html"
		}
	});
	mydirectives.directive('editorArea',function(){
		return{
			restrict:'E',
			templateUrl:"./lib/partials/editorArea.html"
		}
	});
	mydirectives.directive('listOfsavedadvertisement',function(){
		return{
			restrict:'E',
			templateUrl:"./lib/partials/listOfsavedadvertisement.html"
		}
	});
	mydirectives.directive('videoTagEditor',function(){
		return{
			restrict:'E',
			templateUrl:"./lib/partials/videoTagEditor.html"
		}
	});
	mydirectives.directive('player',function(){
		return{
			restrict:'E',
			templateUrl:"./lib/partials/player.html"
		}
	});
	mydirectives.directive('videoProgressBar',function(){
		return{
			restrict:'E',
			templateUrl:"./lib/partials/videoProgressBar.html"
		}
	});
})();