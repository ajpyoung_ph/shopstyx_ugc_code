var fs = require('fs');
var sh = require(__dirname+'/commonFunctions/stringHandling.js');
var styxService = require(__dirname+'/getProcedures/getServiceStyx');
var getProducts = require(__dirname+'/getProcedures/getProducts');
var saveAdvertising = require(__dirname+'/postProcedures/postAdvertisement');
var updateAdvertising = require(__dirname+'/putProcedures/putAdvertisement');
var reconstructPhotoStyx = require (__dirname+'/getProcedures/reconstructPhotoStyx');
// all routes from the server
//standard response to all resources is a JSON document
var envStruct = {
	environment : "GlobalHost = '"+GLOBAL.http+"';user_id = 0;store_id = 0;",
	filename:"environment.js"
};

module.exports = function(router){
	//set our router for default root 
	// router.route(/^\/.+\.(html|php|htm)/i)
	// 	//set up our promise functions
	// 	.get(function(req, res) {
	// 		//server the demo landing page
	// 		// generateEnvironment(envStruct);
	// 		// res.sendfile('./public/mystartpage.html'); // so this really doesn't have to be index.html
	// 		if(parseInt(req.params.store_id) == 0)
	// 		{
	// 			//demo mode... fix to store_id 2 and user_id 0
	// 			dataStruct.store_id = 2;
	// 			dataStruct.user_id = 0;
	// 			styxService.photoStyxStart(dataStruct);
	// 		}else{

	// 		}
	// 	});
	// router.route('/photostyx')
	// 	//set up our promise functions
	// 	.get(function(req, res) {
	// 		//server the demo landing page
	// 		// generateEnvironment(envStruct);
	// 		// res.sendfile('./public/mystartpage.html'); // so this really doesn't have to be index.html
	// 		var dataStruct={
	// 			"req":req,
	// 			"res":res,
	// 			"store_id":req.params.store_id,
	// 			"user_id":req.params.user_id,
	// 			"token":req.params.token
	// 		};
	// 		if(parseInt(req.params.store_id) == 0)
	// 		{
	// 			//demo mode... fix to store_id 2 and user_id 0
	// 			dataStruct.store_id = 2;
	// 			dataStruct.user_id = 0;
	// 			styxService.photoStyxStart(dataStruct);
	// 		}else{
	// 			validateUser(dataStruct,styxService.photoStyxStart);
	// 		}
	// 	});
	router.route('/photostyx/editor/:store_id/:user_id/:token')
		.get(function(req,res,params){
			// envStruct.environment = "GlobalHost = '"+GLOBAL.http+"';user_id = "+req.params.user_id+";store_id = "+req.params.store_id+";",
			// generateEnvironment(envStruct);
			// res.sendfile('./public/mystartpage.html'); // so this really doesn't have to be index.html
			var dataStruct={
				"req":req,
				"res":res,
				"store_id":req.params.store_id,
				"user_id":req.params.user_id,
				"token":req.params.token
			};
			if(parseInt(req.params.store_id) == 0)
			{
				//demo mode... fix to store_id 2 and user_id 0
				dataStruct.store_id = 2;
				dataStruct.user_id = 0;
				styxService.photoStyxStart(dataStruct);
			}else{
				validateUser(dataStruct,styxService.photoStyxStart);			
			}
		});
	router.route('/photostyx/editor/:store_id/:user_id/*')
		.get(function(req,res){
			//console.log(req);
			try{
				if(req.params[0]!=undefined)
				{
					res.sendfile('./public/'+req.params[0]);
				}
			}catch(err){
				var msg = {
					status:"Error",
					message:"You are not permitted to use this service",
					description:"Credentials are not valid"
				};
				res.json(msg);
			}
		});
	router.route('/images/:image')
		.get(function(req,res){
			// var img = fs.readFileSync(__dirname + '/images/'+req.params.image);
			// var ext = req.params.image.split(".");
			// res.writeHead(200, {'Content-Type': 'image/'+ext[ext.length] });
			//res.end(img, 'binary');
			//res.end(img);
			res.sendfile('./server/images/'+req.params.image);
		});
	router.route('/images/:dir1/:dir2/:image')
		.get(function(req,res){
			// var img = fs.readFileSync(__dirname + '/images/'+req.params.image);
			// var ext = req.params.image.split(".");
			// res.writeHead(200, {'Content-Type': 'image/'+ext[ext.length] });
			//res.end(img, 'binary');
			//res.end(img);
			res.sendfile('./server/images/'+req.params.dir1+'/'+req.params.dir2+'/'+req.params.image);
		});
	router.route('/images/:store_id/:user_id/:token/:image')
		.get(function(req,res){
			//verify login token
			//if valid then
			res.sendfile('./server/images/'+req.params.store_id+'/'+req.params.user_id+'/'+req.params.image);
		});
	router.route('/storeproducts/all/:store_id/:user_id')
		.get(function(req,res){
			var dataStruct = {
				"store_id":parseInt(req.params.store_id),
				"user_id":parseInt(req.params.user_id),
				"req": req,
				"res": res
			};
			//currently we have store_id and user_id fixed
			if(parseInt(req.params.store_id) == 0)
			{
				//demo mode... fix to store_id 2 and user_id 0
				dataStruct.store_id = 2;
				dataStruct.user_id = 0;
				getProducts.pullAllproducts(dataStruct);
			}else{
				getProducts.pullAllproducts(dataStruct);
			}
		});
	router.route('/photostyx/allsaved/:store_id/:user_id')
		.get(function(req,res){
			var dataStruct = {
				"store_id":parseInt(req.params.store_id),
				"user_id":parseInt(req.params.user_id),
				"req": req,
				"res": res
			};
			//currently we have store_id and user_id fixed
			if(parseInt(req.params.store_id) == 0)
			{
				//demo mode... fix to store_id 2 and user_id 0
				dataStruct.store_id = 2;
				dataStruct.user_id = 0;
			}
			getProducts.pullAllsavedPhotoStyx(dataStruct);
		});
	router.route('/photostyx/:store_id/:user_id/:filename')
		.get(function(req,res){//read single photostyx record
			var dataStruct = {
				"store_id":parseInt(req.params.store_id),
				"user_id":parseInt(req.params.user_id),
				"filename":req.params.filename,
				"req": req,
				"res": res
			};
			//currently we have store_id and user_id fixed
			if(parseInt(req.params.store_id) == 0)
			{
				//demo mode... fix to store_id 2 and user_id 0
				dataStruct.store_id = 2;
				dataStruct.user_id = 0;
			}
			var msg = {
				status:"Error",
				message:"Not Supported Service",
				description:"Service not yet created for consumption"
			};
			dataStruct.res.json(msg);
		})
		.post(function(req,res){ //insert
			var dataStruct = {
				"store_id":parseInt(req.params.store_id),
				"user_id":parseInt(req.params.user_id),
				"filename":req.params.filename,
				"req": req,
				"res": res
			};
			//currently we have store_id and user_id fixed
			if(parseInt(req.params.store_id) == 0)
			{
				//demo mode... fix to store_id 2 and user_id 0
				dataStruct.store_id = 2;
				dataStruct.user_id = 0;
			}
			saveAdvertising.start(dataStruct);
		})
		.put(function(req,res){ //update
			var dataStruct = {
				"store_id":parseInt(req.params.store_id),
				"user_id":parseInt(req.params.user_id),
				"filename":req.params.filename,
				"req": req,
				"res": res
			};
			//currently we have store_id and user_id fixed
			if(parseInt(req.params.store_id) == 0)
			{
				//demo mode... fix to store_id 2 and user_id 0
				dataStruct.store_id = 2;
				dataStruct.user_id = 0;
			}
			updateAdvertising.start(dataStruct);
		});
	router.route('/photostyx/regen/:store_id/:user_id/:filename')
		.get(function(req,res){
			var dataStruct={
				"req":req,
				"res":res,
				"store_id":req.params.store_id,
				"user_id":req.params.user_id,
				"filename":req.params.filename
			};
			if(parseInt(req.params.store_id) == 0)
			{
				//demo mode... fix to store_id 2 and user_id 0
				dataStruct.store_id = 2;
				dataStruct.user_id = 0;
			}
			reconstructPhotoStyx.start(dataStruct);
		});
	router.route('/videostyx/:store_id/:user_id/:token')
		.get(function(req,res){
			var dataStruct={
				"req":req,
				"res":res,
				"store_id":req.params.store_id,
				"user_id":req.params.user_id,
				"token":req.params.token
			};
			if(parseInt(req.params.store_id) == 0)
			{
				//demo mode... fix to store_id 2 and user_id 0
				dataStruct.store_id = 2;
				dataStruct.user_id = 0;
				styxService.videoStyxStart(dataStruct);
			}
			validateUser(dataStruct,styxService.videoStyxStart);
		});
	router.route('/videostyx/:store_id/:user_id/*')
		.get(function(req,res){
			try{
				if(req.params[0]!=undefined)
				{
					res.sendfile('./public/'+req.params[0]);
				}
			}catch(err){
				var msg = {
					status:"Error",
					message:"You are not permitted to use this service",
					description:"Credentials are not valid"
				};
				res.json(msg);
			}
		})

}

function validateUser(dataStruct,callback)
{
	//if token exists then valid else reject access
	var query = "SELECT * FROM cs_tokens WHERE user_id="+dataStruct.user_id+" AND token='"+dataStruct.token+"';";
	console.log(query);
	GLOBAL.db_cs_common.query(query,function(err,rows, fields){
		var msg={};
		if(err==null)
		{
			if(rows>0)
			{
				callback(dataStruct);
			}else{
				msg = {
					status:"Error",
					message:"You are not permitted to use this service",
					description:"Credentials are not valid"
				};
				try{
					dataStruct.res.json(msg);
				}catch(err){
					console.log("Error Sending Server response");
					console.log(err);
					console.log(dataStruct.res);
					//dataStruct.res.json(msg);
				}
			}
		}else{
			console.log("Error Accessing DB");
			console.log(err);
			msg = {
				status:"Error",
				message:"Error Accessing DB",
				description:err
			};
			dataStruct.res.json(msg);
		}
	});
}

// function generateEnvironment(dataStruct)
// {
// 	var data = dataStruct.environment;
// 	var filename = dataStruct.filename;
// 	var targetDir = {
// 		dirname : __dirname,
// 		splitvar : "server"
// 	};
// 	absPath = sh.getDirectory(targetDir);
// 	absPath = absPath+"public/lib/vendor/config/";
// 	fs.writeFileSync(absPath+filename, data);
// }