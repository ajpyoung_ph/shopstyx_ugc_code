var reconPhotoStyx = require(__dirname+'/reconstructPhotoStyx');

var videoStyxStart = function(dataStruct)
{
	console.log('VideoStyxStart');
	var payload = {
		GlobalHost:GLOBAL.http,
		user_id:dataStruct.user_id,
		store_id:dataStruct.user_id
	};
	console.log("payload");
	console.log(payload);
	dataStruct.res.render('videoAdvert',payload);
};

var photoStyxStart = function(dataStruct)
{
	console.log('PhotoStyxStart');
	var payload = {
		GlobalHost:GLOBAL.http,
		user_id:dataStruct.user_id,
		store_id:dataStruct.user_id
	};
	console.log("payload");
	console.log(payload);
	dataStruct.res.render('mystartpage',payload);
};
module.exports = {
	"videoStyxStart":videoStyxStart,
	"photoStyxStart":photoStyxStart
};