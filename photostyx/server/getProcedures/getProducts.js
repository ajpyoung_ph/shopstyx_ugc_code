var sh = require('../commonFunctions/stringHandling');


module.exports.pullAllproducts = function(dataStruct){

	var query = "SELECT * FROM cs_products WHERE store_id = '"+dataStruct.store_id+"' AND user_id = '"+dataStruct.user_id+"' ORDER BY product_name ASC";
	console.log(query);
	GLOBAL.db_cs_common.query(query,function(err,rows, fields){
		if(err==null)
		{
			console.log(rows);
			dataStruct.rows=rows;
			dataStruct.fields = fields;
			dataStruct.counter=0;
			dataStruct.async_counter=0;
			dataStruct.AllProducts = [];
			parseThroughProducts(dataStruct);		
		}else{
			console.log("Error in pulling products pullAllproducts");
			console.log(err);
			var msg = {
				status:"Error",
				message:"Error in pulling products",
				description:err
			}
			dataStruct.res.json(msg);
		}
	});
}

function parseThroughProducts(dataStruct)
{
	//console.log("counter : "+dataStruct.counter);
	for(var x=dataStruct.counter;x<dataStruct.rows.length;x++)
	{
		data = dataStruct.rows[x];
	//rows.forEach(function(data){
		// https://shopstyx.com/prod/prod_id-store_id/seo_url
		// if user_id > 0
		// https://shopstyx.com/prod/prod_id-store_id-user_id/seo_url
		var async_counter=0;
		var targetURL = '';
		var query2 = "SELECT * FROM cs_product_images WHERE store_id = '"+dataStruct.store_id+"' AND user_id = '"+dataStruct.user_id+"' AND product_id = '"+data.product_id+"'";
		console.log(query2);
		if(parseInt(data.user_id)>0)
		{
			targetURL = 'https://shopstyx.com/prod/'+data.product_id+'-'+data.store_id+'-'+data.user_id+'/'+data.seo_url;
		}else{
			targetURL = 'https://shopstyx.com/prod/'+data.product_id+'-'+data.store_id+'/'+data.seo_url;
		}
		dataStruct.counter = x+1;
		dataStruct.query2 = query2;
		dataStruct.targetURL = targetURL;
		dataStruct.data = data;
		search_image(dataStruct);
		break;
	}
}

function search_image(dataStruct)
{
	GLOBAL.db_cs_common.query(dataStruct.query2,function(err,rows2, fields){
		if(err==null)
		{
			var path='';
			if(parseInt(dataStruct.data.type)==4)
			{
				path = GLOBAL.imagepathexternal;
			}else{
				path = GLOBAL.imagepathinternal;
			}
			var DataCollection = {
				"price" :dataStruct.data.price,
				"currency" : dataStruct.data.currency,
				"color" : dataStruct.data.color,
				"product_name" : dataStruct.data.product_name,
				"product_desc" : dataStruct.data.product_desc,
				"external_tags" : dataStruct.data.external_tags,
				"product_id": dataStruct.data.product_id,
				"url" :dataStruct.targetURL,
				"image":path+"/"+rows2[0].image_large
			};
			console.log(DataCollection);
			dataStruct.AllProducts.push(DataCollection);
		}else{
			//skip that data then
			console.log(err);
			//no need to respond to client
		}
		dataStruct.async_counter++;
		if(dataStruct.async_counter==dataStruct.rows.length)
		{
			dataStruct.res.json(dataStruct.AllProducts);
		}else{
			parseThroughProducts(dataStruct);
		}
	});
}

module.exports.pullAllsavedPhotoStyx=function(dataStruct)
{
	var query = "SELECT * FROM cs_photostyx WHERE store_id = '"+dataStruct.store_id+"' AND user_id = '"+dataStruct.user_id+"' ORDER BY filename ASC";
	console.log(query);
	GLOBAL.db_cs_user_ads.query(query,function(err,rows, fields){
		if(err==null)
		{
			//json encode all dataStruct in rows
			var mydataALL =[];
			rows.forEach(function(data){
				var mydata = {};
				mydata = data;
				data = JSON.parse(mydata.dataStruct);
				mybgfile = sh.cleanFilename(data[0].bgfile);
				mydata.bgfile = "/images/"+dataStruct.user_id+"/"+dataStruct.store_id+"/"+mybgfile;
				mydata.dataStruct = JSON.parse(mydata.dataStruct);
				mydata.sourceloc = GLOBAL.http;
				mydataALL.push(mydata);
			});
			dataStruct.res.json(mydataALL);
		}else{
			var msg = {
				status:"Error",
				message:"Error in pulling saved advertisements",
				description:err
			};
			console.log(msg);
			dataStruct.res.json(msg);
		}
	});
}