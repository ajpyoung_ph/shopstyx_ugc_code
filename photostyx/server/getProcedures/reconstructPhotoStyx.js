
var sh = require('../commonFunctions/stringHandling');

module.exports.start = function(dataStruct){
	var query = 'SELECT * FROM cs_photostyx WHERE user_id="'+dataStruct.user_id+'" AND store_id="'+dataStruct.store_id+'" AND filename="'+dataStruct.filename+'";';
	console.log(query);
	GLOBAL.db_cs_user_ads.query(query,function(err,rows, fields){
		if(err==null)
		{
			var dataStructDB = JSON.parse(rows[0].dataStruct);
			//var filename = dataStructDB[0].bgfile.split(".");
			var bgfile = sh.cleanFilename(dataStructDB[0].bgfile);//filename[0].replace(/\W+/g, '')+"."+filename[1];
			for(var x=0;x<dataStructDB.length;x++)
			{
				dataStructDB[x].ProductInformation.product_desc=sh.mysql_real_escape_string(dataStructDB[x].ProductInformation.product_desc);
				dataStructDB[x].ProductInformation.product_name=sh.mysql_real_escape_string(dataStructDB[x].ProductInformation.product_name);
			}
			var payload = {
				data:dataStructDB,
				filepath:GLOBAL.http+GLOBAL.saved_images_location+"/"+dataStruct.user_id+"/"+dataStruct.store_id,
				bgfilename:bgfile,
				httpPath:GLOBAL.http,
				imgHeight:dataStructDB[0].imgHeight,
				imgWidth:dataStructDB[0].imgWidth
			};
			console.log("payload");
			console.log(payload);
			console.log("ProductInformation");
			console.log(payload.data[0].ProductInformation);
			dataStruct.res.render('photostyxRecon',payload);
		}else{
			var msg = {
				status:"Error",
				message:"Error Accessing Database",
				description:err
			}
			dataStruct.res.json(msg);
		}
	});
}