var fs = require('fs');
var mkdirp = require('mkdirp');

var moveFile = function(dataStruct,callback)
{
	/*
	dataStruct.moveFile.src = sourcefilename with fullpath
	dataStruct.moveFile.dest = {
		filename: new filename,
		path: new full path
	}
	*/
	mkdirp(dataStruct.moveFile.dest.path, function (err) {
	    if(err==null)
	    {
	    	if(fs.existsSync(dataStruct.moveFile.src))
			{
				var source = fs.createReadStream(dataStruct.moveFile.src);
				var dest = fs.createWriteStream(dataStruct.moveFile.dest.path+"/"+dataStruct.moveFile.dest.filename);
				// if(!fs.existsSync(dataStruct.moveFile.dest.path)){
				//     fs.mkdirSync(dataStruct.moveFile.dest.path,0x644);
				// }

				source.pipe(dest);
				source.on('end', function() { 
					eraseFile(dataStruct,callback)
				});
				source.on('error', function(err) { 
					console.log("Error Moving File");
					console.log(err);
					dataStruct.res.json(err);
				});
			}else{
				eraseFile(dataStruct,callback);
			}
	    }else{
	    	console.log()
	    }
	});
}

var eraseFile = function(dataStruct,callback)
{
	fs.existsSync(dataStruct.moveFile.src)
	{
		fs.unlink(dataStruct.moveFile.src, function (err) {
		  if(err==null){
		  	console.log("Deleted tmp file "+dataStruct.moveFile.src);
		  }else{
		  	console.log("error in deleting tmp file "+dataStruct.moveFile.src);
		  }
		});
	}
	callback(dataStruct);
}

module.exports = {
	moveFile:moveFile,
	eraseFile:eraseFile
}