var cf = require('../commonFunctions/fileHandling');

module.exports.start = function(dataStruct){
	
	console.log(dataStruct);
	
	/*
	dataStruct.req.files= { 
	  	file0: { 
	    encoding: "7bit"
		extension: "jpg"
		fieldname: "file0"
		mimetype: "image/jpeg"
		name: "08_advance-image_add-product-internal1412988600473.jpg"
		originalname: "08_advance - image_Add Product - internal.jpg"
		path: "uploads/08_advance-image_add-product-internal1412988600473.jpg"
		size: 255537
		truncated: false
	  } 
	dataStruct.req.body={
	model: "[{"layerX":176,"layerY":179,"offsetX":176,"offsetY":153,"screenX":476,"screenY":280,"x":476,"y":179,"ProductInformation":{"price":9.9,"currency":"EUR","color":"#FFFFFF","product_name":"4 spoons Help! assorted colors","product_desc":"Take a five minute break, choose the color that most 'you like teaspoons Help! and enjoy your favorite tea or coffee. The teaspoons Help! With their colors will give a little extra 'flavor to your drink. \n4 teaspoons The package includes the following colors: gray, blue, black and white.\n\nSpecifications Length 13 cm Materials Plastic","external_tags":"shop,acquista online,design,promozioni,sconti,compra,lovethesign,prodotti,lovethesign.com,novità,idee regalo,icone design,arredo casa,Tavola&Cucina,Under 50€,Posate,Speciali,Ankul Assavaviboonpan,Propaganda","product_id":34,"url":"https://shopstyx.com/prod/34-2/4-spoons-help-assorted-colors","image":"https://shopstyx.com/cdn/uploads/external_products/20140822/34/140870950253f7337e14665754763601.jpg"}},{"layerX":122,"layerY":97,"offsetX":122,"offsetY":71,"screenX":422,"screenY":198,"x":422,"y":97,"ProductInformation":{"price":137.6,"currency":"USD","color":"#DE483E","product_name":"Aika wall clock","product_desc":"Aika means time in Finnish: this small pendulum wall clock, born in the same family as Tiuku, is actually simply perfect to mark everyday time.","external_tags":"Accessories,Products,Clocks,Ari Kanerva,Covo","product_id":3,"url":"https://shopstyx.com/prod/3-2/aika-wall-clock","image":"https://shopstyx.com/cdn/uploads/external_products/20140807/3/140741495553e372ab63367046392538.png"}}]"
	}

	dataStruct = {
				"store_id":parseInt(req.params.store_id),
				"user_id":parseInt(req.params.user_id),
				"filename":req.params.filename,
				"req": req,
				"res": res
			}
	*/
	//check if filename already exists
	var query = "SELECT filename FROM cs_photostyx WHERE user_id = '"+dataStruct.user_id+"' AND store_id = '"+dataStruct.store_id+"' AND filename = '"+dataStruct.filename+"'; ";
	GLOBAL.db_cs_user_ads.query(query,function(err,rows, fields){
		if(err==null)
		{
			if(rows.length>0)
			{
				fileExist(dataStruct);
			}else{
				var uploadDirectory = __dirname ;
				//get the root based on server folder
				var uploadDirArr = uploadDirectory.split('server');
				uploadDirectory = uploadDirArr[0];
				var mydata = JSON.parse(dataStruct.req.body.model)
				var filename = mydata[0].bgfile;
				var filename_arr = filename.split(".");
				filename = filename_arr[0].replace(/\W+/g, '');
				filename = filename+'.'+filename_arr[1];
				dataStruct.moveFile={
					src : uploadDirectory+'uploads/'+filename,
					dest : {
						filename: filename,
						path: uploadDirectory+"server/images/"+dataStruct.user_id+"/"+dataStruct.store_id
					}
				};
				//dataStruct.filename=filename;
				//insertToDB(dataStruct);
				cf.moveFile(dataStruct,insertToDB);
			}
		}else{
			var msg = {
				status:"Error",
				message:"Could not connect to PhotoStyx Server",
				description: err
			}
			dataStruct.res.json(msg);
		}
	});
	//if not exist then insert to database else return error
}

function insertToDB(dataStruct)
{
	var query = "INSERT INTO cs_photostyx SET user_id = '"+dataStruct.user_id+"', store_id = '"+dataStruct.store_id+"', filename = '"+dataStruct.filename+"', dataStruct = "+JSON.stringify(dataStruct.req.body.model)+"; ";
	console.log(query);
	GLOBAL.db_cs_user_ads.query(query,function(err,rows, fields){
		if(err==null)
		{
			var msg = {
				status:"Ok",
				message:"Saved "+dataStruct.filename,
				description: ""
			}
		}else{
			var msg = {
				status:"Error",
				message:"Could not save PhotoStyx",
				description: err
			}
		}
		dataStruct.res.json(msg);
	});
}
function fileExist(dataStruct)
{
	dataStruct.res.json({status:"Error",message:"File Already Exists",description:"File is already in our system, please change filename"});
}