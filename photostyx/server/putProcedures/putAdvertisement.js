var cf = require('../commonFunctions/fileHandling');

module.exports.start = function(dataStruct){
	//debugger;
	console.log(dataStruct);
	//check if filename already exists
	var query = "SELECT filename FROM cs_photostyx WHERE user_id = '"+dataStruct.user_id+"' AND store_id = '"+dataStruct.store_id+"' AND filename = '"+dataStruct.filename+"'; ";
	GLOBAL.db_cs_user_ads.query(query,function(err,rows, fields){
		if(err==null)
		{
			if(rows.length>0)
			{
				var uploadDirectory = __dirname ;
				//get the root based on server folder
				var uploadDirArr = uploadDirectory.split('server');
				uploadDirectory = uploadDirArr[0];
				var mydata = JSON.parse(dataStruct.req.body.model)
				try{
					var filename = mydata[0].bgfile;
					var filename_arr = filename.split(".");
					filename = filename_arr[0].replace(/\W+/g, '');
					filename = filename+'.'+filename_arr[1];
					dataStruct.moveFile={
						src : uploadDirectory+'uploads/'+filename,
						dest : {
							filename: filename,
							path: uploadDirectory+"server/images"
						}
					};
					//dataStruct.filename=filename;
					//insertToDB(dataStruct);
					cf.moveFile(dataStruct,updateDB);
				}catch(err){
					console.log(err);
					//this means that there is no bgfile and therefore
					updateDB(dataStruct);
				}
				
			}else{
				var msg = {
					status:"Error",
					message: "Could not update Advertisement",
					description: "Advertisement could not be found in the system"
				};
				dataStruct.res.json(msg);
			}
		}else{
			var msg = {
				status:"Error",
				message:"Could not connect to PhotoStyx Server",
				description: err
			}
			dataStruct.res.json(msg);
		}
	});
	//if not exist then insert to database else return error
	//if not exist then error else update record
}

function updateDB(dataStruct)
{
	var query = "UPDATE cs_photostyx SET user_id = '"+dataStruct.user_id+"', store_id = '"+dataStruct.store_id+"', filename = '"+dataStruct.filename+"', dataStruct = "+JSON.stringify(dataStruct.req.body.model)+"WHERE user_id = '"+dataStruct.user_id+"' AND store_id = '"+dataStruct.store_id+"' AND filename = '"+dataStruct.filename+"';";
	console.log(query);
	GLOBAL.db_cs_user_ads.query(query,function(err,rows, fields){
		if(err==null)
		{
			var msg = {
				status:"Ok",
				message:"Saved "+dataStruct.filename,
				description: ""
			}
		}else{
			var msg = {
				status:"Error",
				message:"Could not save PhotoStyx",
				description: err
			}
		}
		dataStruct.res.json(msg);
	});
}