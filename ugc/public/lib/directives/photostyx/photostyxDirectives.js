(function(){
	var eventDirective = angular.module('photostyxDirectives',[]);

	// eventDirective.directive('customOnChange', function() {
	//   'use strict';

	//   return {
	//     restrict: "A",
	//     link: function (scope, element, attrs) {
	//       var onChangeFunc = element.scope()[attrs.customOnChange];
	//       element.bind('change', onChangeFunc);
	//     }
	//   };
	// });
	eventDirective.directive('fileUpload', function () {
	    return {
	        scope: true,        //create a new scope
	        link: function (scope, el, attrs) {
	            el.bind('change', function (event) {
	                var files = event.target.files;
	                //iterate files since 'multiple' may be specified on the element
	                for (var i = 0;i<files.length;i++) {
	                    //emit event upward
	                    scope.$emit("fileSelected", { file: files[i] });
	                }                                       
	            });
	        }
	    };
	});
	// eventDirective.directive('ngRightClick', function($parse) {
	//     return function(scope, element, attrs) {
	//         var fn = $parse(attrs.ngRightClick);
	//         element.bind('contextmenu', function(event) {
	//         	event.preventDefault();
	//             fn(scope, {$event:event});
	//             // scope.$apply(function() {
	//             //     event.preventDefault();
	//             //     fn(scope, {$event:event});
	//             // });
	//         });
	//     };
	// });

})();


