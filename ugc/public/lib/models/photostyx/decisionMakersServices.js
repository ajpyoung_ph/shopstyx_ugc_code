(function(){
	var dataServices = angular.module('decisionMakersServices',[]);

	dataServices.service('decisionMakersProcedures',['$http',function($http){
		var setPanelStatusDecisions=function(event){
			var set = false;
			if(event.fromElement!=null && event.toElement!=null)
			{
				if( (event.fromElement.id=='menuBtn' && event.toElement.id=='menuBtnIcon') || (event.toElement.id=='menuBtn' && event.fromElement.id=='menuBtnIcon') )
				{
					return true;
					//do nothing;
				}
				if( (event.fromElement.id=='menuBtnSubMenuHolder' && event.toElement.id=='menuBtn') || (event.toElement.id=='menuBtnSubMenuHolder' && event.fromElement.id=='menuBtn') )
				{
					return true;
					//do nothing
				}
				if( (event.fromElement.id=='menuBtnSubMenu' && event.toElement.id=='menuBtnSubMenuHolder') || (event.toElement.id=='menuBtnSubMenu' && event.fromElement.id=='menuBtnSubMenuHolder') )
				{
					return true;
				}
				if( (event.fromElement.id=='menuBtnSubMenu' && event.toElement.id=='menuBtnIcon') || (event.toElement.id=='menuBtnSubMenu' && event.fromElement.id=='menuBtnIcon'))
				{
					return true;
				}
			}
			
			//console.log("set = "+set);
			return set;
		}
		var changeState=function(targetString,partnerString,sharedEditorData){
			var target = $(targetString);
			var partner = $(partnerString);
			if(!target.hasClass("selectedRadio"))
			{
				target.addClass("selectedRadio");
				target.removeClass("unselectedRadio");
				partner.removeClass("selectedRadio");
				partner.addClass("unselectedRadio");
			}
			switch(targetString)
			{
				case '#borderNoCircle':
					$('#photoOverLayer').css("border","");
					sharedEditorData.setborder(false);
					break;
				case '#borderYesCircle':
					$('#photoOverLayer').css("border","solid 10px "+sharedEditorData.getColors().bordercolor);
					sharedEditorData.setborder(true);
					break;
				case '#shadowNoCircle':
					$('#photoOverLayer').css('box-shadow','');
					sharedEditorData.setshadow(false);
					break;
				case '#shadowYesCircle':
					$('#photoOverLayer').css('box-shadow','1px 1px 10px 1px '+sharedEditorData.getColors().shadowcolor);
					$('#photoOverLayer').css('-webkit-box-shadow','1px 1px 10px 1px '+sharedEditorData.getColors().shadowcolor);
					$('#photoOverLayer').css('-moz-box-shadow','1px 1px 10px 1px '+sharedEditorData.getColors().shadowcolor);

					/*
					-webkit-box-shadow: 1px 1px 10px 1px #888888;
					-moz-box-shadow: 1px 1px 10px 1px #888888;
					box-shadow: 1px 1px 10px 1px #888888;
					*/
					sharedEditorData.setshadow(true);
					break;
			}
		}
		var decideDoneInPage0=function(sharedEditorData)
		{
			if(sharedEditorData.getPreviousPage()==sharedEditorData.getCurrentPageIndex(2))
			{
				sharedEditorData.setCreateStatus(true);
				sharedEditorData.setManagePhotoStyx(true);
			}else{
				sharedEditorData.setCreateStatus(true);
			}
		}
		var unlimitedScroll=function(type,boolean_value,scope)
		{
			switch(type.toLowerCase())
			{
				case 'addproduct':
					switch(boolean_value)
					{
						case true:
							angular.element('#productListingHolder').on('scroll',scope.exposed.checkScrollProducts());
							break;
						case false:
							//$('#productListingHolder').off('scroll');
							break;
						default:
							break;
					}
				default:
					break;
			}
		}
		var currencySign = function(currString)
		{
			if(typeof(currString)!='undefined')
			{
				switch(currString.toLowerCase())
				{
					case 'yen':
						return '¥';
						break;
					case 'pound':
						return '£';
						break;
					case 'usd':
					default:
						return '$';
						break;
				}	
			}else{
				return '';
			}
		}
		var salePrice = function(prodData)
		{
			if(typeof(prodData.sale_price)!='undefined')
			{
				if(parseInt(prodData.sale_price)!=0 && isNaN(prodData.sale_price)==false)
				{
					return currencySign(prodData.currency)+prodData.sale_price;
				}else{
					return '';
				}
			}
			if(typeof(prodData.clickedData)!='undefined')
			{
				if(typeof(prodData.clickedData.lastClickedData)!='undefined')
				{
					if(typeof(prodData.clickedData.lastClickedData.sale_price)!='undefined')
					{
						if(parseInt(prodData.clickedData.lastClickedData.sale_price)!=0 && isNaN(prodData.clickedData.lastClickedData.sale_price)==false)
						{
							return currencySign(prodData.clickedData.lastClickedData.currency)+prodData.clickedData.lastClickedData.sale_price;
						}else{
							return '';
						}
					}
				}
			}
			return '';
			
		}
		var filterText = function (value, wordwise, max, tail) {
            if (!value) return '';

            max = parseInt(max, 10);
            if (!max) return value;
            if (value.length <= max) return value;

            value = value.substr(0, max);
            if (wordwise) {
                var lastspace = value.lastIndexOf(' ');
                if (lastspace != -1) {
                    value = value.substr(0, lastspace);
                }
            }

            return value + (tail || ' …');
        };
        var floatingCardReturn = function(filterText,currentIndex,clickedData,currencySign,salePrice,$scope)
        {
        	if(show==false)
        	{
        		return '<div id=\'subtag'+currentIndex+'\' style=\'height:181px;width:252px;\'><div style=\'top:0px:right:0px;color:#00A3D9\'><div style=\'left:0px;position:absolute;cursor:pointer;\' onclick=\'tt_HideInit();\'>			<i onclick=\'tt_HideInit();\' class=\'fa fa-times fa-lg hvr-buzz-out\' style=\'cursor:pointer;\'></i></div></div><div class=\'floatingCardContainer2\' ><div class=\'floatingCardContainer2UpperPart\'><div style=\'position:absolute;top:0px;left:0px;width:162px;height:90px;padding: 5px 15px 5px 10px;line-height: 16px;\'><span style=\'font-family:Arial;font-size:14px;\' class=\'color1\'><b>'+filterText(clickedData.lastClickedData.product_name,true,58,'...')+'</b></span></div><div  style=\'position:absolute;top:0px;right:0px;\'><img src=\''+clickedData.lastClickedData.image+'\' style=\'height:90px;width:90px\' alt=\'no image\'></div></div><div style=\'position:absolute;top:100px;left:0px;height:44px;width:252px\'><div style=\'position:absolute;top:0px;left:10px;;\' class=\'color3\'><span style=\'display:inline-block;font-family:Arial;font-size:14px\'><b>'+salePrice(clickedData.lastClickedData)+'</b></span><span style=\'display:inline-block;font-family:Arial;font-size:10px;text-decoration:line-through\'>'+currencySign(clickedData.lastClickedData.currency)+clickedData.lastClickedData.base_price+'</span></div><div style=\'position:absolute;top:0px;right:10px;\'><span><span onClick=\'editTagAjax('+currentIndex+')\' class=\'hvr-wobble-top hvr-underline-from-center color3\' style=\'cursor:pointer;\'>Edit</span> | <span onClick=\'removeTagAjax('+currentIndex+');\' class=\'hvr-wobble-top hvr-underline-from-center color3\' style=\'cursor:pointer;\'> Remove</span></span></div></div><div class=\'floatingCardContainer2DashedLine\'></div><div style=\'position:absolute;top:130px;left:0px;width:252px;height:37px;\' class=\'color3\'><span style=\'display:inline-block;padding: 0px 10px 0px 5px;font-family:Arial;font-size:12px;\'>Noel56KL + 32 more sticked this</span></div></div></div>';
        	}else{
        		/*
					<div id="shoppingCardHolder" style="position:absolute;top:0px;right:10px;">
						<i class="fa fa-shopping-cart fa-lg hvr-buzz-out" style="color:{{(exposed.getColors()).color2}};cursor:pointer;"></i>
					</div>
					<div id="sendHolder" style="position:absolute;top:0px;right:50px;">
						<i class="fa fa-paper-plane fa-lg hvr-buzz-out" style="color:{{(exposed.getColors()).color2}};cursor:pointer;"></i>
					</div>
					<div id="plusHolder" style="position:absolute;top:0px;right:90px;">
						<i class="fa fa-plus fa-lg hvr-buzz-out" style="color:{{(exposed.getColors()).color2}};cursor:pointer;"></i>
					</div>
        		*/
        		return '<div id=\'subtag'+currentIndex+'\' style=\'height:181px;width:252px;\'><div style=\'top:0px:right:0px;color:#00A3D9\'><div style=\'left:0px;position:absolute;cursor:pointer;\' onclick=\'tt_HideInit();\'>			<i onclick=\'tt_HideInit();\' class=\'fa fa-times fa-lg hvr-buzz-out\' style=\'cursor:pointer;\'></i></div></div><div class=\'floatingCardContainer2\' ><div class=\'floatingCardContainer2UpperPart\'><div style=\'position:absolute;top:0px;left:0px;width:162px;height:90px;padding: 5px 15px 5px 10px;line-height: 16px;\'><span style=\'font-family:Arial;font-size:14px;\' class=\'color1\'><b>'+filterText(clickedData.lastClickedData.product_name,true,58,'...')+'</b></span></div><div  style=\'position:absolute;top:0px;right:0px;\'><img src=\''+clickedData.lastClickedData.image+'\' style=\'height:90px;width:90px\' alt=\'no image\'></div></div><div style=\'position:absolute;top:100px;left:0px;height:44px;width:252px\'><div style=\'position:absolute;top:0px;left:10px;;\' class=\'color3\'><span style=\'display:inline-block;font-family:Arial;font-size:14px\'><b>'+salePrice(clickedData.lastClickedData)+'</b></span><span style=\'display:inline-block;font-family:Arial;font-size:10px;text-decoration:line-through\'>'+currencySign(clickedData.lastClickedData.currency)+clickedData.lastClickedData.base_price+'</span></div><div style=\'position:absolute;top:0px;right:10px;\'><i class=\'fa fa-shopping-cart fa-lg hvr-buzz-out color2\' style=\'cursor:pointer;\'></i></div><div  style=\'position:absolute;top:0px;right:50px;\'><i class=\'fa fa-paper-plane fa-lg hvr-buzz-out color2\' style=\'cursor:pointer;\'></i></div><div style=\'position:absolute;top:0px;right:90px;\'><i class=\'fa fa-plus fa-lg hvr-buzz-out color2\' style=\'cursor:pointer;\'></i></div><div style=\'position:absolute;top:130px;left:0px;width:252px;height:37px;\' class=\'color3\'><span style=\'display:inline-block;padding: 0px 10px 0px 5px;font-family:Arial;font-size:12px;\'>Noel56KL + 32 more sticked this</span></div></div></div>';
        	}
        	
        };
        var rightsidetagReturn = function(filterText,currentIndex,clickedData,currencySign,salePrice,$scope)
        {
        	if(show==false)
        	{
        		return '<div id="tag'+currentIndex+'" name="shopstyxTag" class="sideAdPanel" style="right:0px;top:0px;position:absolute"><img src="'+clickedData.lastClickedData.image+'" align="middle"><div name="lowerAdHolder"><div name="priceHolder"><span name="price">'+salePrice(clickedData.lastClickedData)+'</span><span name="discount">'+currencySign(clickedData.lastClickedData.currency)+clickedData.lastClickedData.base_price+'</span></div><div name="adtitleHolder"><span name="adtitle">'+filterText(clickedData.lastClickedData.product_name,true,44,'...')+'</span></div><div name="collectionHolder"><span name="collectionname">'+filterText(clickedData.lastClickedData.collection_name,true,30,'...')+'</span></div><div name="actionbarHolder"><div style=\'position:absolute;top:0px;right:10px;\'><span><span onClick=\'editTagAjax('+currentIndex+')\' class=\'hvr-wobble-top hvr-underline-from-center color3\' style=\'cursor:pointer;\'>Edit</span> | <span onClick=\'removeTagAjax('+currentIndex+');\' class=\'hvr-wobble-top hvr-underline-from-center color3\' style=\'cursor:pointer;\'> Remove</span></span></div></div></div></div>';
        	}else{
        		/*
					<div name="actionbarHolder">
						<div class="actionbox" name="addtofavorites">
							<i class="fa fa-plus fa-lg hvr-buzz-out"></i>
						</div>
						<div class="actionbox" name="sendmessage">
							<i class="fa fa-paper-plane fa-lg hvr-buzz-out"></i>
						</div>
						<div class="actionbox" name="shoppingcart">
							<i class="fa fa-shopping-cart fa-lg hvr-buzz-out"></i>
						</div>
					</div>
        		*/
        		return '<div id="tag'+currentIndex+'" name="shopstyxTag" class="sideAdPanel" style="right:0px;top:0px;position:absolute"><img src="'+clickedData.lastClickedData.image+'" align="middle"><div name="lowerAdHolder"><div name="priceHolder"><span name="price">'+salePrice(clickedData.lastClickedData)+'</span><span name="discount">'+currencySign(clickedData.lastClickedData.currency)+clickedData.lastClickedData.base_price+'</span></div><div name="adtitleHolder"><span name="adtitle">'+filterText(clickedData.lastClickedData.product_name,true,44,'...')+'</span></div><div name="collectionHolder"><span name="collectionname">'+filterText(clickedData.lastClickedData.collection_name,true,30,'...')+'</span></div><div name="actionbarHolder"><div class="actionbox hvr-fade2" name="addtofavorites" style="cursor:pointer;"><i class="fa fa-plus fa-lg hvr-buzz-out"></i></div><div class="actionbox hvr-fade2" name="sendmessage" style="cursor:pointer;"><i class="fa fa-paper-plane fa-lg hvr-buzz-out"></i></div><div class="actionbox hvr-fade2" name="shoppingcart" style="cursor:pointer;"><i class="fa fa-shopping-cart fa-lg hvr-buzz-out"></i></div></div></div></div>';

        	}
        };
        var leftsidetagReturn = function(filterText,currentIndex,clickedData,currencySign,salePrice,$scope)
        {
        	if(show==false)
        	{
        		return '<div id="tag'+currentIndex+'" name="shopstyxTag" class="sideAdPanel" style="left:0px;top:0px;position:absolute"><img src="'+clickedData.lastClickedData.image+'" align="middle"><div name="lowerAdHolder"><div name="priceHolder"><span name="price">'+salePrice(clickedData.lastClickedData)+'</span><span name="discount">'+currencySign(clickedData.lastClickedData.currency)+clickedData.lastClickedData.base_price+'</span></div><div name="adtitleHolder"><span name="adtitle">'+filterText(clickedData.lastClickedData.product_name,true,44,'...')+'</span></div><div name="collectionHolder"><span name="collectionname">'+filterText(clickedData.lastClickedData.collection_name,true,30,'...')+'</span></div><div name="actionbarHolder"><div style=\'position:absolute;top:0px;right:10px;\'><span><span onClick=\'editTagAjax('+currentIndex+')\' class=\'hvr-wobble-top hvr-underline-from-center color3\' style=\'cursor:pointer;\'>Edit</span> | <span onClick=\'removeTagAjax('+currentIndex+');\' class=\'hvr-wobble-top hvr-underline-from-center color3\' style=\'cursor:pointer;\'> Remove</span></span></div></div></div></div>';
        	}else{
        		return '<div id="tag'+currentIndex+'" name="shopstyxTag" class="sideAdPanel" style="left:0px;top:0px;position:absolute"><img src="'+clickedData.lastClickedData.image+'" align="middle"><div name="lowerAdHolder"><div name="priceHolder"><span name="price">'+salePrice(clickedData.lastClickedData)+'</span><span name="discount">'+currencySign(clickedData.lastClickedData.currency)+clickedData.lastClickedData.base_price+'</span></div><div name="adtitleHolder"><span name="adtitle">'+filterText(clickedData.lastClickedData.product_name,true,44,'...')+'</span></div><div name="collectionHolder"><span name="collectionname">'+filterText(clickedData.lastClickedData.collection_name,true,30,'...')+'</span></div><div name="actionbarHolder"><div class="actionbox hvr-fade2" name="addtofavorites" style="cursor:pointer;"><i class="fa fa-plus fa-lg hvr-buzz-out"></i></div><div class="actionbox hvr-fade2" name="sendmessage" style="cursor:pointer;"><i class="fa fa-paper-plane fa-lg hvr-buzz-out"></i></div><div class="actionbox hvr-fade2" name="shoppingcart" style="cursor:pointer;"><i class="fa fa-shopping-cart fa-lg hvr-buzz-out"></i></div></div></div></div>';
        	}
        };
        var bottomsidetagReturn = function(filterText,currentIndex,clickedData,currencySign,salePrice,$scope)
        {
        	if(show==false)
        	{
        		return '<div id="tag'+currentIndex+'" class=\'bottomAd\' name=\'shopstyxTag\'><img src=\''+clickedData.lastClickedData.image+'\' align=\'middle\'><div name=\'botttomAdTopHolder\'><div name=\'titleHolder\'><span class=\'color2\'>'+filterText(clickedData.lastClickedData.product_name,true,44,'...')+'</span></div><div name=\'controllerHolder\'><div name=\'otherholders\' style=\'position:absolute;top:0px;right:10px;\'><span> <span onClick=\'editTagAjax('+currentIndex+')\' class=\'hvr-wobble-top hvr-underline-from-center\' style=\'cursor:pointer;\'>Edit</span> | <span onClick=\'removeTagAjax('+currentIndex+')\' class=\'hvr-wobble-top hvr-underline-from-center\' style=\'cursor:pointer;\'> Remove</span></span></div></div><div name=\'closeme\'><i class=\'fa fa-times fa-lg hvr-buzz-out\' style=\'cursor:pointer;\'></i></div></div><div name=\'bottomAdBottomHolder\'><div name=\'collectionNameHolder\'><span>'+filterText(clickedData.lastClickedData.collection_name,true,30,'...')+'</span></div><div name=\'priceHolder\'><span name=\'price\'>'+salePrice(clickedData.lastClickedData)+'</span><span name=\'discount\'>'+currencySign(clickedData.lastClickedData.currency)+clickedData.lastClickedData.base_price+'</span></div></div></div>';
        	}else{
        		return '<div id="tag'+currentIndex+'" class=\'bottomAd\' name=\'shopstyxTag\'><img src=\''+clickedData.lastClickedData.image+'\' align=\'middle\'><div name=\'botttomAdTopHolder\'><div name=\'titleHolder\'><span class=\'color2\'>'+filterText(clickedData.lastClickedData.product_name,true,44,'...')+'</span></div><div name="controllerHolder"><div name="holders" style="position:absolute;top:0px;right:10px;" class="hvr-fade2"><i class="fa fa-shopping-cart fa-lg hvr-buzz-out" style="cursor:pointer; top:11px;"></i></div><div name="holders" style="position:absolute;top:0px;right:50px;" class="hvr-fade2"><i class="fa fa-paper-plane fa-lg hvr-buzz-out" style="cursor:pointer; top:11px;"></i></div><div name="holders" style="position:absolute;top:0px;right:90px;" class="hvr-fade2"><i class="fa fa-plus fa-lg hvr-buzz-out" style="cursor:pointer;top: 12px;left: 11px;"></i></div></div><div name=\'closeme\'><i class=\'fa fa-times fa-lg hvr-buzz-out\' style=\'cursor:pointer;\'></i></div></div><div name=\'bottomAdBottomHolder\'><div name=\'collectionNameHolder\'><span>'+filterText(clickedData.lastClickedData.collection_name,true,30,'...')+'</span></div><div name=\'priceHolder\'><span name=\'price\'>'+salePrice(clickedData.lastClickedData)+'</span><span name=\'discount\'>'+currencySign(clickedData.lastClickedData.currency)+clickedData.lastClickedData.base_price+'</span></div></div></div>';
        	}
        }
		var createTag=function(sharedEditorData,stringManipulationProcedures,$compile,$scope,$sce)
		{
			//delete all current tags
			while(document.querySelector( '[name="shopstyxTag"]' )!=null)
            {
                var target = angular.element( document.querySelector( '[name="shopstyxTag"]' ) );
                target.remove();
            }
            
			//for loop sharedEditorData.currentTags
			for(var x = 0;x<sharedEditorData.getLengthListOfTags();x++)
			{
				// var clickedData = sharedEditorData.getClickedData();
				// clickedData = {
				// 	lastClickedData:{},
				// 	lastDataPosition:{},
				// 	lastDataTagType:{}
				// };
				var clickedData = sharedEditorData.getTagInfo(x).clickedData;
				var photoStyxColors = sharedEditorData.getColors();
				// photoStyxColors = {
				// 	color1:'#FFFFFF',
				// 	color2:'#00A3D9',
				// 	color3:'#000000',
				// 	border:false,
				// 	shadow:false,
				// 	shadowcolor:'#AAAAAA',
				// 	bordercolor:'#FFFFFF'
				// };
				//var currentIndex = sharedEditorData.getLengthListOfTags();
				var currentIndex = x;
				switch((clickedData.lastDataTagType).toLowerCase())
				{
					case (sharedEditorData.getTagTypes()[0]).toLowerCase():
						//pricetag
						var floatingCard = floatingCardReturn(filterText,currentIndex,clickedData,currencySign,salePrice,$scope);
						var tag = '<div id="tag'+currentIndex+'" name="shopstyxTag" style="position:absolute;top:'+clickedData.lastDataPosition.pageY+'px;left:'+clickedData.lastDataPosition.pageX+'px;" onmouseout="UnTip();" onmouseover="Tip(\''+stringManipulationProcedures.mysql_real_escape_string(floatingCard)+'\', FADEIN, 300, FADEOUT, 300, STICKY, 1);"><img src="/images/pricetag.png"></div>';
						break;
					case (sharedEditorData.getTagTypes()[1]).toLowerCase():
						//rightsidetag
						var tag = rightsidetagReturn(filterText,currentIndex,clickedData,currencySign,salePrice,$scope);
						break;
					case (sharedEditorData.getTagTypes()[2]).toLowerCase():
						//leftsidetag
						var tag = leftsidetagReturn(filterText,currentIndex,clickedData,currencySign,salePrice,$scope);
						break;
					case (sharedEditorData.getTagTypes()[3]).toLowerCase():
						//bottomsidetag
						var tag = bottomsidetagReturn(filterText,currentIndex,clickedData,currencySign,salePrice,$scope);
						break;
					default:
						break;

				}
				//tag = $sce.trustAsHtml(tag);
				var linkFn = $compile(tag);
	            var content = linkFn($scope);

	            //var content = compile(tag)(scope);

	            //angular.element(document.getElementById('photoOverLayer')).append(content);
	            $('#photoOverLayer').append(content);
	            //collect content

	            // var collectedContent = {
	            // 	clickedData:clickedData,
	            // 	photoStyxColors:photoStyxColors
	            // };
	            // sharedEditorData.pushToListOfTags(collectedContent);
			}
		}
		var panelAdSize = function(width,type,target)
		{
			/*
			sideAdPanel
			800x600 - 262
			720x540 - 
			640x480 - 
			480x360 - 230
			*/
			/*
			img
			800x600 - 260x197
			720x540 - 
			640x480 - 
			480x360 - 228x172
			*/
			value = 0;
			if(parseInt(width)<480 && type=='sideAdPanel' && target=='width')
			{
				value=230;
			}else if(parseInt(width)==480 && type=='img' && target=='width')
			{
				value=228;
			}else if(parseInt(width)==480 && type=='img' && target=='height'){
				value=172;
			}else if(type=='img' && target=='width'){
				value=260;
			}else if(type=='img' && target=='height'){
				value=197;
			}else if(type=='sideAdPanel' && target=='width'){
				value=262;
			}
			return value;
		}
		return{
			setPanelStatusDecisions:setPanelStatusDecisions,
			changeState:changeState,
			decideDoneInPage0:decideDoneInPage0,
			unlimitedScroll:unlimitedScroll,
			currencySign:currencySign,
			createTag:createTag,
			salePrice:salePrice,
			panelAdSize:panelAdSize
		};
	}]);

})();