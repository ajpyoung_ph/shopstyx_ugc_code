(function(){
	var dataServices = angular.module('myDataServices',[]);

	dataServices.service('sharedEditorData',['$http',function($http){
		
		var clickedData = {
			lastClickedData:{},
			lastDataPosition:{},
			lastDataTagType:{}
		};
		var savedTags = {
			tags:[],
			files:[],
			bgfile:''
		};
		var currentTags=[];

		var LoadingProductList = false;
		var currentPage = '';
		var previousPage = '';
		var differentPages = ['startPage','chooseImagePage','managePhotoStyxPage'];
		var createStatus = false;
		var managePhotoStyx = false;
		var AdvertInfo = {
			name:'',
			selectedRes:{}
		};
		//var selectedRes = {};
		var tagTypes = ['pricetag','rightsidetag','leftsidetag','bottomsidetag'];
		var resolutionSet = [
			{'text':'800x600','width':800,'height':600},
			{'text':'720x540','width':720,'height':540},
			{'text':'640x480','width':640,'height':480},
			{'text':'480x360','width':480,'height':360}
		];
		var photoStyxColors = {
			color1:'#FFFFFF',
			color2:'#00A3D9',
			color3:'#000000',
			border:false,
			shadow:false,
			shadowcolor:'#AAAAAA',
			bordercolor:'#FFFFFF'
		};
		var previewCard = false;
		var panelStatus = {
			backButton:true,
			overallMenu:false,
			menuBtnSubMenu:false,
			manageColors:true,
			manageAddProduct:false,
			addProduct:false,
			tagTypeSelection:false
		};
		var savedState = false;
		var productListPage = 0;
		var prodSearch = {
			prodName:'',
			prodColor:'',
			prodDepartment:'Everything'
		};
		var productList = [];
		var prodDepartmentList = [];
		var deletingTagFlag = false;
		var embed_url = '';

		// ============ procedures ================ //

		var restorePanelStatus=function()
		{
			panelStatus = {
				backButton:true,
				overallMenu:false,
				menuBtnSubMenu:false,
				manageColors:true,
				manageAddProduct:false,
				addProduct:false,
				tagTypeSelection:false

			};
		};
		var getCreateStatus=function(){
			return createStatus;
		};
		var setCreateStatus=function(boolean_value){
			if(typeof(boolean_value)!='boolean')
			{
				boolean_value=false;
			}
			createStatus=boolean_value;
			if(boolean_value)
			{
				previousPage=differentPages[0];
				currentPage=differentPages[1];
			}else{
				currentPage=differentPages[0];
				if(previousPage!='')
				{
					previousPage=differentPages[1];
				}
			}
		};
		var getManagePhotoStyx=function(){
			return managePhotoStyx;
		};
		var	setManagePhotoStyx=function(boolean_value){
			if(typeof(boolean_value)!='boolean')
			{
				boolean_value=false;
			}
			managePhotoStyx=boolean_value;
			if(boolean_value)
			{
				previousPage=differentPages[1];
				currentPage=differentPages[2];
			}else{
				previousPage=differentPages[2];
				currentPage=differentPages[1];
			}
		};
		var getCurrentPage=function(){
			return currentPage;
		};
		var getCurrentPageIndex=function(index){
			return differentPages[index];
		};
		var getPreviousPage=function(){
			return previousPage;
		};
		var getResolutionSetIndex=function(index){
			return resolutionSet[index];
		};
		var setSelectedRes=function(data){
			AdvertInfo.selectedRes = data;
		};
		var getSelectedRes=function(){
			if(JSON.stringify(AdvertInfo.selectedRes)==JSON.stringify({}))
			{
				AdvertInfo.selectedRes = resolutionSet[0];
			}
			return AdvertInfo.selectedRes;
		};
		var setAdvertInfo = function(data){
			AdvertInfo = JSON.parse(JSON.stringify(data));
		}
		var getResolutionSet=function(){
			return resolutionSet;
		};
		var modifyCreateWindow=function(data){
			var prevHeight = parseInt($('#mainbody').css("height"));
			var prevWidth = parseInt($('#mainbody').css("width"));
			var animation = 300;
			var animation2 = {duration: 1000, easing: 'easeOutBounce'};
			// $('#mainbody').css("height",$scope.selectedRes.height);
			// $('#mainbody').css("width",$scope.selectedRes.width);
			$('#mainbody').animate({'width':AdvertInfo.selectedRes.width+'px','height':AdvertInfo.selectedRes.height+'px'}, 1000,'easeOutBounce');

			if(parseInt(AdvertInfo.selectedRes.height)==360)
			{
				$('#createEntryContainer').animate({"top":"90px","left":"40px"}, 1000,'easeOutBounce') 
				// $('#createEntryContainer').css("top","90px");
				// $('#createEntryContainer').css("left","90px");//width
				//$('#createEntryContainer').css("width","400px");
			}
			if(parseInt(AdvertInfo.selectedRes.height)==480)
			{
				$('#createEntryContainer').animate({"top":"140px","left":"150px"}, 1000,'easeOutBounce') 
				// $('#createEntryContainer').css("top","140px");
				// $('#createEntryContainer').css("left","150px");//width
				//$('#createEntryContainer').css("width","400px");
			}
			if(parseInt(AdvertInfo.selectedRes.height)==540)
			{
				$('#createEntryContainer').animate({"top":"180px","left":"190px"}, 1000,'easeOutBounce') 
				// $('#createEntryContainer').css("top","180px");
				// $('#createEntryContainer').css("left","190px");//width
				//$('#createEntryContainer').css("width","400px");
			}
			if(parseInt(AdvertInfo.selectedRes.height)==600)
			{
				$('#createEntryContainer').animate({"top":"200px","left":"200px"}, 1000,'easeOutBounce') 
				// $('#createEntryContainer').css("top","200px");
				// $('#createEntryContainer').css("left","200px");//width
				//$('#createEntryContainer').css("width","400px");
			}
		};
		var setAdvertName=function(name){
			AdvertInfo.name=name;
		};
		var getAdvertName=function(){
			return AdvertInfo.name;
		};
		var getAdvertInfo=function(){
			return AdvertInfo;
		};
		var saveBgFile=function(data){
			savedTags.bgfile=data;
		};
		var getBgFile=function(){
			return savedTags.bgfile;
		};
		var clearBgFile=function(){
			savedTags.bgfile=''
		};
		var getColors=function(){
			return photoStyxColors;
		};
		var setColors=function(key,value)
		{
			if(typeof(photoStyxColors[key])=='undefined')
			{
				photoStyxColors[key]=[];	
			}
			photoStyxColors[key]=value;
		};
		var setborder=function(boolean_value)
		{
			if(typeof(boolean_value)!='boolean')
			{
				boolean_value=false;
			}
			setColors('border',boolean_value);
		};
		var getborderstatus=function()
		{
			return photoStyxColors.border;
		};
		var setshadow=function(boolean_value)
		{
			if(typeof(boolean_value)!='boolean')
			{
				boolean_value=false;
			}
			setColors('shadow',boolean_value);
		};
		var getshadowstatus=function()
		{
			return photoStyxColors.shadow;
		};
		var getPreviewCard=function(){
			return previewCard;
		};
		var setPreviewCard=function(boolean_value)
		{
			if(typeof(boolean_value)!='boolean')
			{
				boolean_value=false;
			}
			previewCard=boolean_value;
		};
		var getPanelStatus=function(){
			//console.log(panelStatus);
			return panelStatus;
		};
		var setPanelStatus=function(type,boolean_value)
		{
			if(typeof(panelStatus[type]) == 'undefined')
			{
				panelStatus[type]=[];
			}
			if(typeof(boolean_value)!='boolean')
			{
				boolean_value=false;
			}
			panelStatus[type]=boolean_value;
		};
		
		var setsavedState=function(boolean_value)
		{
			if(typeof(boolean_value)!='boolean')
			{
				boolean_value=false;
			}
			savedState=boolean_value;
		};
		var getsavedState=function()
		{
			return savedState;
		};
		var returnToPage0=function()
		{
			restorePanelStatus();
			createStatus=false;
			previousPage=getCurrentPageIndex(2);
		};
		var getCurrentProductPage=function()
		{
			return productListPage;
		};
		var setCurrentProductPage=function(page)
		{
			if(isNaN(page))
			{
				productListPage=0;
			}else{
				productListPage=page;
			}
		}
		var setSearchColor=function(hexcolor)
		{
			prodSearch.prodColor=hexcolor;
		}
		var setSearchName=function(name)
		{
			prodSearch.prodName=name;
		}
		var setSearchCat=function(cat)
		{
			prodSearch.prodDepartment=cat;
		}
		var getSearchProd=function()
		{
			return prodSearch;
		}
		var getDepartmentList=function()
		{
			return prodDepartmentList;
		}
		var setDepartmentList=function(data)
		{
			prodDepartmentList=[
				{"department_name":"Everything"}
			];
			data.forEach(function(singleData)
			{
				prodDepartmentList.push(singleData);
			});
			//prodDepartmentList=data;
		}
		var setProductList=function(dataList)
		{
			if(productListPage==0)
			{
				productList=[];
			}
			dataList.forEach(function(data){
				productList.push(data);
			});
		}
		var getProductList=function()
		{
			return productList;
		}
		var saveCurrentProduct=function(index)
		{
			clickedData.lastClickedData = [];
			clickedData.lastClickedData = productList[index];
		};
		var getCurrentProduct=function()
		{
			return clickedData.lastClickedData;
		};
		var saveCurrentProductTagPosition=function(data)
		{
			clickedData.lastDataPosition=data;
		};
		var getCurrentProductTagPosition=function()
		{
			return clickedData.lastDataPosition;
		};
		var saveCurrentTagType=function(type)
		{
			clickedData.lastDataTagType=type;
		};
		var getCurrentTagType = function()
		{
			return clickedData.lastDataTagType;
		};
		var getClickedData = function()
		{
			return clickedData;
		};
		var setClickedData = function(target_clickedData)
		{
			clickedData = target_clickedData;
		};
		var getTagTypes = function()
		{
			return tagTypes;
		};
		var pushToListOfTags = function(data)
		{
			currentTags.push(JSON.parse(data));
		};
		var removeFromListOfTags = function(index)
		{
			delete currentTags[index];
			var th1 = JSON.stringify(currentTags);
			var th2 = JSON.parse(th1);
			currentTags = [];
			for(var x=0;x<th2.length;x++)
			{
				if(typeof(th2[x])=='object' && th2[x]!=null)
				{
					currentTags.push(th2[x]);
				}
			}
		};
		var clearListOfTags = function()
		{
			currentTags = [];
		};
		var getLengthListOfTags = function()
		{
			return currentTags.length;
		};
		var getTagInfo = function(index)
		{
			return currentTags[index];
		};
		var getAllTags = function()
		{
			return currentTags;
		};
		var setLoadingProductList = function()
		{
			LoadingProductList = true;
		};
		var getLoadingProductList = function()
		{
			return LoadingProductList;
		};
		var clearLoadingProductList = function()
		{
			LoadingProductList = false;
		};
		var getdeletingTagFlag = function()
		{
			return deletingTagFlag;
		};
		var setdeletingTagFlag = function()
		{
			deletingTagFlag = true;
		};
		var cleardeletingTagFlag = function()
		{
			deletingTagFlag = false;
		};
		var updateClickedDataIndex = function(index,clickedData)
		{
			var myclickedData = JSON.stringify(clickedData);
			myclickedData = JSON.parse(myclickedData);
			currentTags[index].clickedData = myclickedData;
		};
		var pushFiles=function(data){
			savedTags.files.push(data);
		};
		var getFiles=function(){
			return savedTags.files;
		};
		var generateEmbedCode = function(uuid){
			embed_url = GlobalHost+'/'+user_id+'/'+store_id+'/show/photostyx/'+uuid;
		};
		var getEmbedCode = function(){
			return embed_url;
		};
		var populateData = function(data,sharedEditorData,decisionMakersProcedures,stringManipulationProcedures,$compile,$scope,$sce){
			//need to populate
			//currentTags
			//AdvertInfo
			//photoStyxColors
			//currentPage
			//previousPage
			//(global) filename_ext
			
			//console.log(data);
			//{"_id":"5564285bc7ea3ce371175b29","currentTags":[{"clickedData":{"lastClickedData":{"external_product_url":"","external_tags":"","product_name":"Extreme Micro-Drone 2.0 w/ Camera","product_desc":"The Micro Drone 2.0 is equipped with an aerial camera so you can capture photos and video while flipping through the skies. This crazy maneuvering remote-controlled Micro Drone 2.0 can do it all!\n\nCarries an included camera w/ a swiveling lens to capture all angles\nPre-programmed algorithm to perform 360-degree flips and stabilize back to flying position\nUSB rechargeable battery\nRange up to 400 feet (2.4GHZ transmitter)\nWeighs just over an ounce\nDurable for flying indoors and out\nIncludes 4 spare blades and a mini screwdriver for attaching the camera","currency":"USD","color":"#111111","department_name":"Gadgets","base_price":139,"sale_price":74.99,"image":"https://shopstyx.com/cdn/uploads/product_images/2/20150414/2/21428999587552ccda3901fa020943096_small300.jpg","collection_name":"Interesting Stuff"},"lastDataPosition":{"clientX":437,"clientY":254,"offsetX":157,"offsetY":88,"pageX":437,"pageY":254,"screenX":437,"screenY":339},"lastDataTagType":"pricetag"}},{"clickedData":{"lastClickedData":{"external_product_url":"","external_tags":"","product_name":"OM/ONE Levitating Bluetooth Speaker","product_desc":"The OM/ONE is the world’s first levitating Bluetooth speaker, and it absolutely doesn’t disappoint. The unique levitating design of the OM/ONE allows it to direct all of its amplification energy into the driver to create full, crisp, and clear sound (a full 105 decibels of output with a 3 watt RMS amplifier to be exact).\n\n\n\nWorks with any device equipped with Bluetooth 4.0\nCrystal clear sound matched with beautiful and innovative design\nUse the OM/ONE as an awesome, floating speakerphone\nSuper portable, pick up the OM/ONE speaker orb and take it anywhere\nUp to 15 hours of continuous play w/ its long-lasting battery","currency":"USD","color":"#111111","department_name":"Gadgets","base_price":199,"sale_price":190,"image":"https://shopstyx.com/cdn/uploads/product_images/2/20150414/3/21428999685552cce05c24cd017035701_small300.jpg","collection_name":"Interesting Stuff"},"lastDataPosition":{"clientX":503,"clientY":248,"offsetX":92,"offsetY":96,"pageX":503,"pageY":248,"screenX":503,"screenY":333},"lastDataTagType":"rightsidetag"}}],"advertInfo":{"name":"test advertisement","selectedRes":{"text":"800x600","width":800,"height":600}},"photoStyxColors":{"color1":"#FFFFFF","color2":"#00A3D9","color3":"#000000","border":false,"shadow":false,"shadowcolor":"#AAAAAA","bordercolor":"#FFFFFF"},"filename":"2_0_testadvertisement","filename_ext":"jpg","user":{"user_id":2,"store_id":0}}

			currentTags = JSON.parse(JSON.stringify(data.currentTags));
			AdvertInfo = JSON.parse(JSON.stringify(data.advertInfo));
			GlobalAdvertInfo = JSON.parse(JSON.stringify(AdvertInfo));
			photoStyxColors = JSON.parse(JSON.stringify(data.photoStyxColors));
			filename_extention = data.filename_ext;
			$("#titlePhotoStyx").val(AdvertInfo.name);
			//preselect resolution
			$scope.exposed.setSelectedRes(AdvertInfo.selectedRes);
			var imgURL = GlobalHost+"/cdn/uploads/advertisement/photostyx/"+user_id+"/"+store_id+"/"+data.filename+"."+data.filename_ext;
			modifyCreateWindow(null);
			console.log(imgURL);
			$("#photoImageDisplay").attr("src",imgURL);
			setCreateStatus(true);
			setManagePhotoStyx(true);
			setPreviewCard(false);
			setPanelStatus('manageColors',false);
			setPanelStatus('overallMenu',true);
			setPanelStatus('backButton',false);
			decisionMakersProcedures.createTag(sharedEditorData,stringManipulationProcedures,$compile,$scope,$sce);
			$scope.$digest();
		};
		return {
			getCreateStatus:getCreateStatus,
			setCreateStatus:setCreateStatus,
			getManagePhotoStyx:getManagePhotoStyx,
			setManagePhotoStyx:setManagePhotoStyx,
			getCurrentPage:getCurrentPage,
			getCurrentPageIndex:getCurrentPageIndex,
			getPreviousPage:getPreviousPage,
			getResolutionSetIndex:getResolutionSetIndex,
			setSelectedRes:setSelectedRes,
			getSelectedRes:getSelectedRes,
			setAdvertInfo:setAdvertInfo,
			getResolutionSet:getResolutionSet,
			modifyCreateWindow:modifyCreateWindow,
			setAdvertName:setAdvertName,
			getAdvertName:getAdvertName,
			getAdvertInfo:getAdvertInfo,
			saveBgFile:saveBgFile,
			getBgFile:getBgFile,
			clearBgFile:clearBgFile,
			getColors:getColors,
			setColors:setColors,
			setborder:setborder,
			getborderstatus:getborderstatus,
			setshadow:setshadow,
			getshadowstatus:getshadowstatus,
			getPreviewCard:getPreviewCard,
			setPreviewCard:setPreviewCard,
			getPanelStatus:getPanelStatus,
			setPanelStatus:setPanelStatus,
			restorePanelStatus:restorePanelStatus,
			setsavedState:setsavedState,
			getsavedState:getsavedState,
			returnToPage0:returnToPage0,
			getCurrentProductPage:getCurrentProductPage,
			setCurrentProductPage:setCurrentProductPage,
			setSearchColor:setSearchColor,
			setSearchName:setSearchName,
			setSearchCat:setSearchCat,
			getSearchProd:getSearchProd,
			getDepartmentList:getDepartmentList,
			setDepartmentList:setDepartmentList,
			setProductList:setProductList,
			getProductList:getProductList,
			saveCurrentProduct:saveCurrentProduct,
			getCurrentProduct:getCurrentProduct,
			saveCurrentProductTagPosition:saveCurrentProductTagPosition,
			getCurrentProductTagPosition:getCurrentProductTagPosition,
			saveCurrentTagType:saveCurrentTagType,
			getCurrentTagType:getCurrentTagType,
			getClickedData:getClickedData,
			setClickedData:setClickedData,
			getTagTypes:getTagTypes,
			pushToListOfTags:pushToListOfTags,
			removeFromListOfTags:removeFromListOfTags,
			clearListOfTags:clearListOfTags,
			getLengthListOfTags:getLengthListOfTags,
			getTagInfo:getTagInfo,
			getAllTags:getAllTags,
			setLoadingProductList:setLoadingProductList,
			getLoadingProductList:getLoadingProductList,
			clearLoadingProductList:clearLoadingProductList,
			getdeletingTagFlag:getdeletingTagFlag,
			setdeletingTagFlag:setdeletingTagFlag,
			cleardeletingTagFlag:cleardeletingTagFlag,
			updateClickedDataIndex:updateClickedDataIndex,
			pushFiles:pushFiles,
			getFiles:getFiles,
			generateEmbedCode:generateEmbedCode,
			getEmbedCode:getEmbedCode,
			populateData:populateData
		};
	}]);

})();