(function(){
	var dataServices = angular.module('stringManipulation',[]);


	dataServices.service('stringManipulationProcedures',['$http',function($http){

		var zeroFill=function( number, width )
		{
		  width -= number.toString().length;
		  if ( width > 0 )
		  {
		    return new Array( width + (/\./.test( number ) ? 2 : 1) ).join( '0' ) + number;
		  }
		  return number + ""; // always return a string
		}
		var trimvalue=function(target)
        {
            var string = target.val();
            var newString = string.replace(/\W/g, '');
            return newString;
        }
        var addslashes=function(str) 
		{
		  //  discuss at: http://phpjs.org/functions/addslashes/
		  // original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
		  // improved by: Ates Goral (http://magnetiq.com)
		  // improved by: marrtins
		  // improved by: Nate
		  // improved by: Onno Marsman
		  // improved by: Brett Zamir (http://brett-zamir.me)
		  // improved by: Oskar Larsson Högfeldt (http://oskar-lh.name/)
		  //    input by: Denny Wardhana
		  //   example 1: addslashes("kevin's birthday");
		  //   returns 1: "kevin\\'s birthday"

		  return (str + '')
		    .replace(/[\\"']/g, '\\$&')
		    .replace(/\u0000/g, '\\0');
		}
		var stripslashes=function(str) 
		{
		  //       discuss at: http://phpjs.org/functions/stripslashes/
		  //      original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
		  //      improved by: Ates Goral (http://magnetiq.com)
		  //      improved by: marrtins
		  //      improved by: rezna
		  //         fixed by: Mick@el
		  //      bugfixed by: Onno Marsman
		  //      bugfixed by: Brett Zamir (http://brett-zamir.me)
		  //         input by: Rick Waldron
		  //         input by: Brant Messenger (http://www.brantmessenger.com/)
		  // reimplemented by: Brett Zamir (http://brett-zamir.me)
		  //        example 1: stripslashes('Kevin\'s code');
		  //        returns 1: "Kevin's code"
		  //        example 2: stripslashes('Kevin\\\'s code');
		  //        returns 2: "Kevin\'s code"

		  return (str + '')
		    .replace(/\\(.?)/g, function(s, n1) {
		      switch (n1) {
		        case '\\':
		          return '\\';
		        case '0':
		          return '\u0000';
		        case '':
		          return '';
		        default:
		          return n1;
		      }
		    });
		}
		var mysql_real_escape_string=function(str) 
		{
		    return str.replace(/[\0\x08\x09\x1a\n\r"'\\\%]/g, function (char) {
		        switch (char) {
		            case "\0":
		                return "\\0";
		            case "\x08":
		                return "\\b";
		            case "\x09":
		                return "\\t";
		            case "\x1a":
		                return "\\z";
		            case "\n":
		                return "\\n";
		            case "\r":
		                return "\\r";
		            case "\"":
		            case "'":
		            case "\\":
		            case "%":
		                return "\\"+char; // prepends a backslash to backslash, percent,
		                                  // and double/single quotes
		        }
		    });
		}
		return{
			zeroFill:zeroFill,
			trimvalue:trimvalue,
			addslashes:addslashes,
			stripslashes:stripslashes,
			mysql_real_escape_string:mysql_real_escape_string
		}
	}]);
})();