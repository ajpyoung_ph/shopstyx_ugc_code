(function(){
	var dataServices = angular.module('productInfoServices',[]);

	dataServices.service('productInfoProcedures',['$http',function($http){
		var getAllProducts = function(sharedEditorData){
			///:token/:user_id/:store_id/get/product_list/:colortype/:department_name/:product_name/:number_of_records/:page_number/
			/*
			var prodSearch = {
				prodName:'',
				prodColor:'',
				prodDepartment:'Everything'
			};
			*/
			var allInfo = sharedEditorData.getSearchProd();
			prodColor=allInfo.prodColor;
			prodDepartment=allInfo.prodDepartment;
			prodName=allInfo.prodName;
			if(allInfo.prodDepartment=='Everything')
			{
				prodDepartment='*';
			}
			if(allInfo.prodColor=='')
			{
				prodColor='*';
			}
			if(allInfo.prodName=='')
			{
				prodName='*';
			}
			pullProductList(prodColor,prodDepartment,prodName,sharedEditorData.getCurrentProductPage(),sharedEditorData);
			
		}
		function pullProductList(prodColor,prodDepartment,prodName,currentPage,sharedEditorData)
		{
			var url_value = GlobalHost+'/'+current_token+'/'+user_id+'/'+store_id+'/get/product_list/'+encodeURIComponent(prodColor)+'/'+encodeURIComponent(prodDepartment)+'/'+encodeURIComponent(prodName)+'/9/'+currentPage;
			console.log("url:");
			console.log(url_value);
			var target = {
	            method:"GET",
	            url:url_value
	        };///:token/:user_id/:store_id/product_list/:number_of_records/:page_number/
	        var fullList = $http(target);
			fullList.success(function(data, status, headers, config) {
				sharedEditorData.clearLoadingProductList();
	            if(data.status=='Success')
	            {
	            	sharedEditorData.setProductList(data.data);
	            	if(data.data.length==0)
	 				{
	 					currentPage=currentPage-1;
	 					sharedEditorData.setCurrentProductPage(currentPage);
	 				}
	            }else{
	            	console.log(data);
	            	alert("An Error Occurred while accessing the server");
	            }
	        });
	        fullList.error(function(data, status, headers, config) {
	        	sharedEditorData.clearLoadingProductList();
	            alert("Cannot access main Server for Product List!\ntry reloading the page");
	            console.log("Status: "+status);
	            console.log("config: "+config);
	            console.log("headers: "+headers);
	            console.log("data: "+data);
	        });
		}
		var getAllCategories = function(sharedEditorData){
			///:token/:user_id/get/all/departments/
			var target = {
	            method:"GET",
	            url:GlobalHost+'/'+current_token+'/'+user_id+'/get/all/departments/'
	        };
	        var fullList = $http(target);
			fullList.success(function(data, status, headers, config) {
	            //console.log(data);
	            if(data.status=='Success')
	            {
	            	sharedEditorData.setDepartmentList(data.data);
	            }else{
	            	console.log(data);
	            	alert("An Error Occurred while accessing the server");
	            }
	        });
	        fullList.error(function(data, status, headers, config) {
	            alert("Cannot access main Server for Product List!\ntry reloading the page");
	        });
		};
		var getEditData = function(id,sharedEditorData,decisionMakersProcedures,stringManipulationProcedures,$compile,$scope,$sce)
		{
			if(show==false)
			{
				var target = {
		            method:"GET",
		            url:GlobalHost+'/'+current_token+'/'+user_id+'/get/advertisement/'+id
		        };
			}else{
				var target = {
		            method:"GET",
		            url:GlobalHost+'/'+user_id+'/show/advertisement/'+id
		        };
			}
			
	        var fullList = $http(target);
	        sharedEditorData.generateEmbedCode(id);
			fullList.success(function(data, status, headers, config) {
	            //console.log(data);
	            if(data.status=='Success')
	            {
	            	if(environment == 'staging')
	            	{
	            		setTimeout(sharedEditorData.populateData, 4000,data.data,sharedEditorData,decisionMakersProcedures,stringManipulationProcedures,$compile,$scope,$sce);
	            	}else{
	            		sharedEditorData.populateData(data.data,sharedEditorData,decisionMakersProcedures,stringManipulationProcedures,$compile,$scope,$sce);	
	            	}
	            	
	            }else{
	            	console.log(data);
	            	alert("An Error Occurred while accessing the server");
	            }
	        });
	        fullList.error(function(data, status, headers, config) {
	            alert("Cannot access main Server for Product List!\ntry reloading the page");
	        });
		}
		return{
			getAllProducts:getAllProducts,
			getAllCategories:getAllCategories,
			getEditData:getEditData
		};
	}]);
})();


