(function(){
	var mydirectives = angular.module('PhotoStyxPartialControllers',[]);

	mydirectives.directive('createProduct',function(){
		return{
			restrict:'E',//E as an Element or A as a Attribute
			templateUrl:"/lib/partials/photostyx/createPhotoStyxEntry.html"
		}
	});
	mydirectives.directive('loadPhoto',function(){
		return{
			restrict:'E',//E as an Element or A as a Attribute
			templateUrl:"/lib/partials/photostyx/loadPhoto.html"
		}
	});
	mydirectives.directive('managePhotoStyx',function(){
		return{
			restrict:'E',//E as an Element or A as a Attribute
			templateUrl:"/lib/partials/photostyx/managePhotoStyx.html"
		}
	});
	mydirectives.directive('backbuttonBlue',function(){
		return{
			restrict:'E',//E as an Element or A as a Attribute
			templateUrl:"/lib/partials/photostyx/backbuttonBlue.html"
		}
	});
	mydirectives.directive('managePhotoStyxColors',function(){
		return{
			restrict:'E',//E as an Element or A as a Attribute
			templateUrl:"/lib/partials/photostyx/managePhotoStyxColors.html"
		}
	});
	mydirectives.directive('previewcard',function(){
		return{
			restrict:'E',//E as an Element or A as a Attribute
			templateUrl:"/lib/partials/photostyx/previewcard.html"
		}
	});
	mydirectives.directive('previewcardEdit',function(){
		return{
			restrict:'E',//E as an Element or A as a Attribute
			templateUrl:"/lib/partials/photostyx/previewcardEdit.html"
		}
	});
	mydirectives.directive('previewBottomAdPanel',function(){
		return{
			restrict:'E',//E as an Element or A as a Attribute
			templateUrl:"/lib/partials/photostyx/previewBottomAdPanel.html"
		}
	});
	mydirectives.directive('previewBottomAdPanelEdit',function(){
		return{
			restrict:'E',//E as an Element or A as a Attribute
			templateUrl:"/lib/partials/photostyx/previewBottomAdPanelEdit.html"
		}
	});
	mydirectives.directive('overallMenu',function(){
		return{
			restrict:'E',//E as an Element or A as a Attribute
			templateUrl:"/lib/partials/photostyx/overallMenu.html"
		}
	});
	mydirectives.directive('menuBtnSubMenuHolder',function(){
		return{
			restrict:'E',//E as an Element or A as a Attribute
			templateUrl:"/lib/partials/photostyx/menuBtnSubMenuHolder.html"
		}
	});
	mydirectives.directive('productColorPicker',function(){
		return{
			restrict:'E',//E as an Element or A as a Attribute
			templateUrl:"/lib/partials/photostyx/productColorPicker.html"
		}
	});
	mydirectives.directive('addproduct',function(){
		return{
			restrict:'E',//E as an Element or A as a Attribute
			templateUrl:"/lib/partials/photostyx/addproduct.html"
		}
	});
	mydirectives.directive('productlistItem',function(){
		return{
			restrict:'E',//E as an Element or A as a Attribute
			templateUrl:"/lib/partials/photostyx/productlistItem.html"
		}
	});
	mydirectives.directive('addproducteverythingbtn',function(){
		return{
			restrict:'E',//E as an Element or A as a Attribute
			templateUrl:"/lib/partials/photostyx/addproducteverythingbtn.html"
		}
	});
	mydirectives.directive('addtypeselector',function(){
		return{
			restrict:'E',//E as an Element or A as a Attribute
			templateUrl:"/lib/partials/photostyx/addtypeselector.html"
		}
	});
	mydirectives.directive('previewSideAdPanel',function(){
		return{
			restrict:'E',//E as an Element or A as a Attribute
			templateUrl:"/lib/partials/photostyx/previewSideAdPanel.html"
		}
	});
	mydirectives.directive('manageProductAds',function(){
		return{
			restrict:'E',//E as an Element or A as a Attribute
			templateUrl:"/lib/partials/photostyx/manageProductAds.html"
		}
	});
	mydirectives.directive('manageProductAdsList',function(){
		return{
			restrict:'E',//E as an Element or A as a Attribute
			templateUrl:"/lib/partials/photostyx/manageProductAdsList.html"
		}
	});
})();