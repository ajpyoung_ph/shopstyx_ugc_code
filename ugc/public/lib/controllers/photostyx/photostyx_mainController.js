(function(){
	var PhotoStyx = angular.module('PhotoStyxMainController',['ngAnimate','ui.bootstrap','colorpicker.module','myDataServices','PhotoStyxPartialControllers','fileUploadServices','decisionMakersServices','stringManipulation','productInfoServices','photostyxDirectives','ngDragDrop']);
	// if(environment != 'development')
	// {
		// PhotoStyx.config(['$compileProvider', function ($compileProvider) {
		// 	// disable debug info
		// 	$compileProvider.debugInfoEnabled(false);
		// }]);
	//}
	globaldragEvent={};
	globaldropEvent={};
	PhotoStyx.controller('CreatePartialController',['$scope','$http','$compile','$document','$location','$sce','sharedEditorData','fileUploadProcedures','decisionMakersProcedures','stringManipulationProcedures','productInfoProcedures',function($scope,$http,$compile,$document,$location,$sce,sharedEditorData,fileUploadProcedures,decisionMakersProcedures,stringManipulationProcedures,productInfoProcedures){
		
		var getCreateStatus = function(){
			return sharedEditorData.getCreateStatus();
		}
		var getResolutionList = function(){
			return sharedEditorData.getResolutionSet();
		}
		var getSelectedRes = function(){
			return sharedEditorData.getSelectedRes();
		}
		var setSelectedRes = function(data){
			sharedEditorData.setSelectedRes(data);
			$scope.selectedRes=sharedEditorData.getSelectedRes();
			sharedEditorData.modifyCreateWindow();
		}
		var setAdvertName = function(event){
			//sharedEditorData.setAdvertName()
			////console.log(event);
			sharedEditorData.setAdvertName($('#titlePhotoStyx').val());
			if(event.which==13)
			{
				initCreate();
			}
		}
		var initCreate = function(){
			if(sharedEditorData.getAdvertName()!='')
			{
				decisionMakersProcedures.decideDoneInPage0(sharedEditorData);
				//sharedEditorData.setCreateStatus(true);
				//alert("showCreate : "+sharedEditorData.getAdvertName());
			}else{
				sharedEditorData.setCreateStatus(false);
				alert("Please enter a proper advertisement name/title");
			}
		}
		var clearEverything = function(){
			if(typeof(GlobalAdvertInfo)=='undefined')
			{
				$scope.selectedRes = sharedEditorData.getResolutionSetIndex(0);
				sharedEditorData.setSelectedRes($scope.selectedRes);
				sharedEditorData.modifyCreateWindow();
				$('#titlePhotoStyx').val('');
			}else{
				sharedEditorData.setAdvertInfo(GlobalAdvertInfo);
				setSelectedRes(GlobalAdvertInfo.selectedRes);
				$("#titlePhotoStyx").val(GlobalAdvertInfo.name);
				sharedEditorData.setCreateStatus(true);
				sharedEditorData.setManagePhotoStyx(true);
				sharedEditorData.setPreviewCard(false);
				sharedEditorData.setPanelStatus('manageColors',false);
				sharedEditorData.setPanelStatus('overallMenu',true);
				sharedEditorData.setPanelStatus('backButton',false);
			}
		}
		var getManagePhotoStyx = function(){
			return sharedEditorData.getManagePhotoStyx();
		}
		var goBackToStart = function(){
			switch(sharedEditorData.getCurrentPage())
			{
				case sharedEditorData.getCurrentPageIndex(2):
					sharedEditorData.setManagePhotoStyx(false);
					break;
				case sharedEditorData.getCurrentPageIndex(1):
				default:
					sharedEditorData.setCreateStatus(false);
					sharedEditorData.setManagePhotoStyx(false);
					break;

			}
			sharedEditorData.restorePanelStatus();
			$scope.$digest();
		}
		var openFile = function(target){
			$("#actualfile").off( "change" );
			sharedEditorData.setManagePhotoStyx(true);
			fileUploadProcedures.fileRead(sharedEditorData,target,document.getElementById('photoImageDisplay'));
			$scope.$digest();
		};
		var saveAdvert = function(method){
			fileUploadProcedures.fileUpload(sharedEditorData,method,$scope);
		};
		var openFileChoose = function(){
			$('#actualfile').val("");
			$('#actualfile').on('change',function(){
	        	openFile(document.getElementById('actualfile'));
	        });
			$('#actualfile').click();
		};
		var getColors = function(){
			return sharedEditorData.getColors();
		};
		var setColors = function(type,color){
			sharedEditorData.setColors(type,color);
			switch(type)
			{
				case 'bordercolor':
					$('#photoOverLayer').css("border","solid 10px "+sharedEditorData.getColors().bordercolor);
					break;
				case 'shadowcolor':
					$('#photoOverLayer').css('box-shadow','1px 1px 10px 1px '+sharedEditorData.getColors().shadowcolor);
					$('#photoOverLayer').css('-webkit-box-shadow','1px 1px 10px 1px '+sharedEditorData.getColors().shadowcolor);
					$('#photoOverLayer').css('-moz-box-shadow','1px 1px 10px 1px '+sharedEditorData.getColors().shadowcolor);
					break;
				case 'color1selected':
				case 'color2selected':	
				case 'color3selected':
					setPreviewCard(true);
					break;
			}
			
		}
		var getPreviewCard = function(){
			return sharedEditorData.getPreviewCard();
		}
		var setPreviewCard = function(boolean_value)
		{
			sharedEditorData.setPreviewCard(boolean_value);
		}
		var getPanelStatus = function()
		{
			return sharedEditorData.getPanelStatus();
		}
		var setPanelStatus = function(type,boolean_value,event)
		{
			var set=false;
			if(event!=null)
			{
				set = decisionMakersProcedures.setPanelStatusDecisions(event);
			}
			if(set==false)
			{
				// //console.log(event.type);
				// //console.log(event);
				sharedEditorData.setPanelStatus(type,boolean_value);
			}
			//decisionMakersProcedures.unlimitedScroll(type,boolean_value,$scope);
		}
		var changeState = function(targetString,partnerString)
		{
			decisionMakersProcedures.changeState(targetString,partnerString,sharedEditorData);
			//$scope.$digest();
		}
		var opacityMenu = function(opacity_level,event)
		{
			////console.log(event.type);
			$('#menuBtn').css('opacity',parseFloat(opacity_level));
			$('#addProductBtn').css('opacity',parseFloat(opacity_level));
		}
		var opacityTarget = function(opacity_level,elementName)
		{
			$('#'+elementName).css('opacity',parseFloat(opacity_level));
		}
		var opacityThis = function(opacity_level,event)
		{
			$(event.target).css('opacity',parseFloat(opacity_level));
		}
		var embedCodeMsg = function()
		{
			if(sharedEditorData.getsavedState())
			{
				return sharedEditorData.getEmbedCode();
			}else{
				return "Please Save this project first";
			}
		}
		var decideDoneInPage0 = function()
		{
			decisionMakersProcedures.decideDoneInPage0();
		}
		var returnToPage0 = function()
		{
			sharedEditorData.returnToPage0();
		}
		var previouspageDecide = function()
		{
			if(sharedEditorData.getPreviousPage()==sharedEditorData.getCurrentPageIndex(2))
			{
				return "Update";
			}else{
				return "Create";
			}
		}
		var getborderstatus = function()
		{
			return sharedEditorData.getborderstatus();
		}
		var getshadowstatus = function()
		{
			return sharedEditorData.getshadowstatus();
		}
		var setSearchColor = function(event)
		{
			if(event!='')
			{
				var colorSearchRGB = $(event.target).css("background-color");
				colorSearchRGB = colorSearchRGB.match(/^rgb\((\d+),\s*(\d+),\s*(\d+)\)$/);
	 			var colorSearchHEX = "#" + stringManipulationProcedures.zeroFill((parseInt(colorSearchRGB[1])).toString(16),2) + stringManipulationProcedures.zeroFill((parseInt(colorSearchRGB[2])).toString(16),2) + stringManipulationProcedures.zeroFill((parseInt(colorSearchRGB[3])).toString(16),2);
			}else{
				var colorSearchHEX = event;
			}
			
 			////console.log(colorSearchHEX);
 			sharedEditorData.setSearchColor(colorSearchHEX);
 			initiateSearchProducts();
		}

		var setSearchName = function(event)
		{
			if(event.keyCode==13)
			{
				sharedEditorData.setSearchName(name);
				initiateSearchProducts();
			}
		}
		var setSearchCat = function(event,index)
		{
			// //console.log(event);
			var cat = $(event.target).text();
			sharedEditorData.setSearchCat(cat);
			initiateSearchProducts();
		}
		var initiateSearchProducts = function()
		{
			// if(JSON.stringify(sharedEditorData.getProductList())=='{}')
			// {
				sharedEditorData.setCurrentProductPage(0);
			//}
			var name = $('#prodNameSearch').val();
			if(typeof(name)=='undefined')
			{
				name='';
			}
			sharedEditorData.setSearchName(name);
			productInfoProcedures.getAllProducts(sharedEditorData);
		}
		var increasePage=function()
		{
			var currentPage = sharedEditorData.getCurrentProductPage();
			currentPage=currentPage+1;
			sharedEditorData.setCurrentProductPage(currentPage);
			productInfoProcedures.getAllProducts(sharedEditorData);
		}
		var checkScrollProducts = function()
		{
			//console.log('executing productListingHolder');
			if($('#productListingHolder').scrollTop() + $('#productListingHolder').innerHeight() >= $('#productListingHolder')[0].scrollHeight - 100)
			{
				//console.log('reached the bottom');
				if(sharedEditorData.getLoadingProductList()==false)
				{
					sharedEditorData.setLoadingProductList();
					increasePage();
				}
			}		
		}
		var getSearchProd = function()
		{
			return sharedEditorData.getSearchProd();
		}
		var getDepartmentList=function()
		{
			return sharedEditorData.getDepartmentList();
		}
		var getProductList = function()
		{
			return sharedEditorData.getProductList();
		}
		var convertCurr = function(currString)
		{
			return decisionMakersProcedures.currencySign(currString);
			
		}
		var salePrice = function(prodData)
		{
			return decisionMakersProcedures.salePrice(prodData);
		}
		
		var dropEvent = function(event)
		{
			////console.log("dropped");
			////console.log(event);
			var positionData = {
				clientX:event.clientX,
				clientY:event.clientY,
				offsetX:event.offsetX,
				offsetY:event.offsetY,
				pageX:event.pageX,
				pageY:event.pageY,
				screenX:event.screenX,
				screenY:event.screenY
			};
			////console.log(positionData);
			saveTagInformation = true;
			sharedEditorData.saveCurrentProductTagPosition(positionData);
			sharedEditorData.saveCurrentTagType($("#bottomaddtypeselectorholder .addtypeselectorSelectedtag").attr("id"));
			sharedEditorData.setPanelStatus('tagTypeSelection',true);
		}
		
		var saveCurrentProduct = function(index)
		{
			sharedEditorData.saveCurrentProduct(index);
			////console.log(sharedEditorData.getCurrentProduct());
		}
		var getCurrentProduct = function()
		{
			return sharedEditorData.getCurrentProduct();
		}
		var saveTagInformation = false;
		var lasteditIndex = 0;
		var selecttagtype = function(tagName)
		{
			var mycurrselected = $("#bottomaddtypeselectorholder .addtypeselectorSelectedtag");
			mycurrselected.removeClass("addtypeselectorSelectedtag");
			mycurrselected.addClass("addtypeselectorUnselectedtag")
			$('#'+tagName).removeClass("addtypeselectorUnselectedtag");
			$('#'+tagName).addClass("addtypeselectorSelectedtag");
			sharedEditorData.saveCurrentTagType(tagName);
		}
		var formTag = function()
		{
			tt_HideInit();
			if(saveTagInformation)
			{
				var clickedData = JSON.stringify(sharedEditorData.getClickedData());
				var photoStyxColors = JSON.stringify(sharedEditorData.getColors());
				var collectedContent = {
					clickedData:JSON.parse(clickedData)
				};
				sharedEditorData.pushToListOfTags(JSON.stringify(collectedContent));
			}else{
				if(lasteditIndex>-1)
				{
					var clickedData = sharedEditorData.getTagInfo(lasteditIndex).clickedData;
					clickedData.lastDataTagType = sharedEditorData.getCurrentTagType();
					sharedEditorData.updateClickedDataIndex(lasteditIndex,clickedData);
				}
			}			
			decisionMakersProcedures.createTag(sharedEditorData,stringManipulationProcedures,$compile,$scope,$sce);
			cancelTag();
			setTimeout(sharedEditorData.cleardeletingTagFlag,1000);
		}
		var cancelTag = function()
		{
			saveTagInformation = false;
			sharedEditorData.setPanelStatus('tagTypeSelection',false);
			restoreTagDefault();
		}
		var editTag = function(index)
		{
			saveTagInformation = false;
			cancelTag();
			tt_HideInit();
			lasteditIndex=index;
			////console.log("editing tag #"+index);
			setPanelStatus('manageProductAds',false,null);
			//setClickedData to getTagInfo(index).clickedData;
			selecttagtype(sharedEditorData.getTagInfo(index).clickedData.lastDataTagType);//lastDataTagType
			sharedEditorData.setClickedData(sharedEditorData.getTagInfo(index).clickedData);
			sharedEditorData.setPanelStatus('tagTypeSelection',true);
		}
		var removeTag = function(index)
		{
			if(sharedEditorData.getdeletingTagFlag()==false)
			{
				sharedEditorData.setdeletingTagFlag();
				lasteditIndex=-1;
				saveTagInformation = false;
				cancelTag();
				//console.log("removing tag #"+index);
				tt_HideInit();
				sharedEditorData.removeFromListOfTags(index);
				formTag();
			}
		}
		var restoreTagDefault = function()
		{
			var mycurrselected = $("#bottomaddtypeselectorholder .addtypeselectorSelectedtag");
			mycurrselected.removeClass("addtypeselectorSelectedtag");
			mycurrselected.addClass("addtypeselectorUnselectedtag");
			$('#pricetag').removeClass("addtypeselectorUnselectedtag");
			$('#pricetag').removeClass("addtypeselectorSelectedtag");
			$('#pricetag').addClass("addtypeselectorSelectedtag");
			//restore
		}
		var getTagTypes = function()
		{
			return sharedEditorData.getTagTypes();
		}
		// var showTagType = function()
		// {
		// 	sharedEditorData.getPanelStatus().tagTypeSelection;
		// }
		var panelAdSize = function(width,type,target)
		{
			return decisionMakersProcedures.panelAdSize(width,type,target);
		}
		var callonce_array=[];
        var callonce = function(type)
        {
        	//console.log("called callonce");
        	if(callonce_array.indexOf(type)==-1)
        	{
        		callonce_array.push(type);
        		switch(type)
        		{
        			case 'productListingHolder':
        				//console.log('setting productListingHolder');
        				$('#productListingHolder').on("scroll",checkScrollProducts);
        				break;
        			default:
        				break;
        		}
        	}        	
        }
        var getAllTags = function()
        {
        	////console.log("tag length:"+sharedEditorData.getAllTags().length);
        	// if((sharedEditorData.getAllTags()).length>0)
        	// {
        	// 	//console.log(sharedEditorData.getAllTags());
        	// }
        	return sharedEditorData.getAllTags();
        }
        var showManageProductEditor = function(id)
        {
        	//console.log('show '+id);
        	$("#"+id).show();
        }
        var hideManageProductEditor = function(id)
        {
        	//console.log('hide '+id);
        	$("#"+id).hide();
        }
        var getEmbedCode = function()
  		{
  			return sharedEditorData.getEmbedCode();
  		}
  		var showAdvert = function()
  		{
  			return show;
  		}
		$scope.exposed = {
            getResolutionList:getResolutionList,
            getSelectedRes:getSelectedRes,
            setSelectedRes:setSelectedRes,
            setAdvertName:setAdvertName,
            getCreateStatus:getCreateStatus,
            initCreate:initCreate,
            clearEverything:clearEverything,
            getManagePhotoStyx:getManagePhotoStyx,
            goBackToStart:goBackToStart,
            openFile:openFile,
            saveAdvert:saveAdvert,
            openFileChoose:openFileChoose,
            getColors:getColors,
            setColors:setColors,
            getPreviewCard:getPreviewCard,
            setPreviewCard:setPreviewCard,
            getPanelStatus:getPanelStatus,
            setPanelStatus:setPanelStatus,
            changeState:changeState,
            opacityMenu:opacityMenu,
            opacityTarget:opacityTarget,
            opacityThis:opacityThis,
            embedCodeMsg:embedCodeMsg,
            decideDoneInPage0:decideDoneInPage0,
            returnToPage0:returnToPage0,
            previouspageDecide:previouspageDecide,
            getborderstatus:getborderstatus,
            getshadowstatus:getshadowstatus,
            setSearchColor:setSearchColor,
            setSearchName:setSearchName,
			setSearchCat:setSearchCat,
            getSearchProd:getSearchProd,
            getDepartmentList:getDepartmentList,
            initiateSearchProducts:initiateSearchProducts,
            getProductList:getProductList,
            convertCurr:convertCurr,
            salePrice:salePrice,
            increasePage:increasePage,
            checkScrollProducts:checkScrollProducts,
            saveCurrentProduct:saveCurrentProduct,
            getCurrentProduct:getCurrentProduct,
            selecttagtype:selecttagtype,
            formTag:formTag,
            cancelTag:cancelTag,
            editTag:editTag,
            removeTag:removeTag,
            getTagTypes:getTagTypes,
            panelAdSize:panelAdSize,
            callonce:callonce,
            getAllTags:getAllTags,
            showManageProductEditor:showManageProductEditor,
            hideManageProductEditor:hideManageProductEditor,
            getEmbedCode:getEmbedCode,
            showAdvert:showAdvert
        };
        $scope.selectedRes = sharedEditorData.getResolutionSetIndex(0);
        $scope.color1selected = (sharedEditorData.getColors()).color1;
        $scope.color2selected = (sharedEditorData.getColors()).color2;
        $scope.color3selected = (sharedEditorData.getColors()).color3;
        $scope.shadowcolorselected = (sharedEditorData.getColors()).shadowcolor;
        $scope.bordercolorselected = (sharedEditorData.getColors()).bordercolor;
        $scope.currentDrag = [];
        $scope.sampletext = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam maximus ipsum et arcu sodales convallis. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Aliquam sollicitudin condimentum tortor, a bibendum nulla finibus a.";
        $scope.colorSelectorMenu=false;
        $scope.departmentSelector=false;

        
        if(show==false)
        {
        	productInfoProcedures.getAllCategories(sharedEditorData);
        	initiateSearchProducts();
        }
        
        globaldropEvent=dropEvent;

        $scope.$on("fileSelected", function (event, args) {
            $scope.$apply(function () {            
                //add the file object to the scope's files collection
                sharedEditorData.pushFiles(args.file);
            });
        });
        function preloadRequirements(id)
        {
        	productInfoProcedures.getEditData(id,sharedEditorData,decisionMakersProcedures,stringManipulationProcedures,$compile,$scope,$sce);
        }
        function checkURL()
        {
  			//alert(document.URL+"  ->  "+GlobalHost);
  			var urlpath = (document.URL).replace(GlobalHost,'');
  			//alert(urlpath);
  			if(urlpath.match(/photostyx\/edit/)!=null)
  			{
  				if((urlpath.match(/photostyx\/edit/)).length > 0)
	  			{
	  				var holder = (document.URL).split("/photostyx/edit/");
	  				preloadRequirements(holder[holder.length-1]);
	  			}
  			}
  			if(urlpath.match(/show\/photostyx/)!=null)
  			{
  				if((urlpath.match(/show\/photostyx/)).length > 0)
	  			{
	  				var holder = (document.URL).split("/show/photostyx/");
	  				preloadRequirements(holder[holder.length-1]);
	  			}
  			}
  			// if(urlpath.match(/show\/photostyx/)!=null)
  			// {
  			// 	if((urlpath.match(/show\/photostyx/)).length > 0)
	  		// 	{
	  		// 		var holder = (document.URL).split("/show/photostyx/");
	  		// 		preloadRequirements(holder[holder.length-1]);
	  		// 	}
  			// } 			
  			
        }
        $document.ready(checkURL);

	}]);
	PhotoStyx.filter('limitStyxText', function () {
        return function (value, wordwise, max, tail) {
            if (!value) return '';

            max = parseInt(max, 10);
            if (!max) return value;
            if (value.length <= max) return value;

            value = value.substr(0, max);
            if (wordwise) {
                var lastspace = value.lastIndexOf(' ');
                if (lastspace != -1) {
                    value = value.substr(0, lastspace);
                }
            }

            return value + (tail || ' …');
        };
    });
})();
function editTagAjax(value)
{
	var scope = angular.element(document.getElementById("mainbody")).scope();
	scope.$apply(function () {
		scope.exposed.editTag(value);
	});
}

function removeTagAjax(value)
{
	var scope = angular.element(document.getElementById("mainbody")).scope();
	scope.$apply(function () {
		scope.exposed.removeTag(value);
	});
}