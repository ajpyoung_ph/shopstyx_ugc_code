var videoStyxCreate = function(dataStruct)
{
	console.log('VideoStyxStart');
	var showme = false;
	try{
		if(typeof(dataStruct.req.params.show)!='undefined')
		{
			showme = dataStruct.req.params.show;
		}
	}catch(err){
		//do nothing
	}
	
	var payload = {
		GlobalHost:dataStruct.req.protocol+"://"+dataStruct.req.get('host'),//GLOBAL.http,
		user_id:dataStruct.user_id,
		store_id:dataStruct.req.params.store_id,
		NODE_ENV:process.env.NODE_ENV,
		token:dataStruct.req.params.token,
		show:showme,
		video:true
	};
	console.log("payload");
	console.log(payload);
	dataStruct.res.render('photoStyxStartPage',payload);
};

var photoStyxCreate = function(dataStruct)
{
	console.log('PhotoStyxStart');
	var showme = false;
	try{
		if(typeof(dataStruct.req.params.show)=='undefined')
		{
			dataStruct.req.params.show = showme;
		}
	}catch(err){
		//do nothing
	}
	if(dataStruct.req.params.token=='bypass')
	{
		var query = "SELECT token FROM cs_tokens WHERE user_id="+dataStruct.user_id+" LIMIT 1;";
		GLOBAL.db_cs_common.query(query,function(err,rows, fields){
			dataStruct.req.params.token = rows[0]['token'];
			startPhotostyx(dataStruct);
		});
	}else{
		startPhotostyx(dataStruct);
	}
	
};

function startPhotostyx(dataStruct)
{
	var payload = {
		GlobalHost:dataStruct.req.protocol+"://"+dataStruct.req.get('host'),//GLOBAL.http,
		user_id:dataStruct.user_id,
		store_id:dataStruct.req.params.store_id,
		NODE_ENV:process.env.NODE_ENV,
		token:dataStruct.req.params.token,
		show:dataStruct.req.params.show,
		video:false
	};
	console.log("payload");
	console.log(payload);
	dataStruct.res.render('photoStyxStartPage',payload);
}
module.exports = {
	videoStyxCreate:videoStyxCreate,
	photoStyxCreate:photoStyxCreate
};