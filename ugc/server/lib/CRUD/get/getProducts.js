var mongojs = require('mongojs');

var getList = function(dataStruct)
{
	try{

        var query = 'SELECT * FROM cs_stores a, cs_products b, cs_product_images c, cs_departments d, cs_users_collections_products e, cs_users_collections f WHERE c.mark_as_deleted =  \'N\' AND ((a.store_id = b.store_id AND a.user_id= b.user_id AND a.claimed_by = '+dataStruct.user_id+' AND a.claimed_by != 0) AND (c.product_id = b.product_id AND c.store_id = b.store_id AND c.user_id = b.user_id AND c.store_id = b.store_id) AND (d.id = b.department_id) AND (b.publish_type = 1 AND b.mark_as_deleted = \'N\') AND c.image_large IS NOT NULL) AND (a.store_id = e.store_id AND a.user_id = e.user_id AND b.product_id = e.product_id AND e.store_id = e.storeID AND e.user_id = e.store_user_id) AND (e.collection_id=f.collection_id AND e.user_id=f.user_id AND e.store_id=f.store_id) ';

        if(typeof(dataStruct.filter)!='undefined')
        {
            query = query+dataStruct.filter;
        }

        query = query + ' GROUP BY b.product_id ORDER BY c.sort_order ASC ';//insert end of query
        var skip = parseInt(dataStruct.req.params.page_number) * parseInt(dataStruct.req.params.number_of_records);
        query = query+' LIMIT '+skip+','+parseInt(dataStruct.req.params.number_of_records)+';';
        //console.log(query);
		GLOBAL.db_cs_common.query(query,function(err,rows,fields){
				if(err==null)
				{
					var holder = [];
					rows.forEach(function(data){
                        // console.log("data.image_large");
                        // console.log(data.image_large);
						var image_path = data.image_large.split("/");//str.split(" ")
						var filename = image_path[image_path.length-1];
						var filenameHolder = filename.split(".");
						var extension = filenameHolder[1];
						filename = filenameHolder[0]+'_small300';
						var newPath = '';
						for(var x = 0;x<image_path.length;x++)
						{
							if(x!=image_path.length-1)
							{
								newPath = newPath+image_path[x]+'/';
							}
						}
						newPath = newPath+filename+'.'+extension;
						if(data.type==4)
						{
							newPath = GLOBAL.imagepathexternal + '/' +newPath;
						}else{
							newPath = GLOBAL.imagepathinternal + '/' +newPath;
						}
						var dataStore = {
							external_product_url:data.external_url,
							external_tags:data.external_tags,
							product_name:data.product_name,
							product_desc:data.product_desc,
							currency:data.currency,
                            color:data.color,
                            department_name:data.department_name,
							base_price:data.base_price,
							sale_price:data.sale_price,
							image:newPath,
                            collection_name:data.collection_name
						};
                        holder.push(dataStore);
					});
                    var msg = {
                        status:"Success",
                        message:"[getList]Data Found",
                        data:holder
                    };
                    GLOBAL.sendError(msg,dataStruct.res);
				}else{
					var msg = {
						status:"Error",
						message:"[getList]Error in MySQL",
						desc:err,
                        query:query
					};
					GLOBAL.sendError(msg,dataStruct.res);
				}
		});
	}catch(err){
		var msg = {
			status:"Error",
			message:"[getList]Error Processing Data",
			desc:err
		};
		GLOBAL.sendError(msg,dataStruct.res);
	}
}

var getProductWithFilters = function(dataStruct)
{
    try{
        ///:token/:user_id/:store_id/get/product_list/:colortype/:department_name/:product_name/:number_of_records/:page_number/
        var query='';
        if((dataStruct.req.params.colortype).toLowerCase()!='*')
        {
            query = query + ' AND b.color="'+dataStruct.req.params.colortype+'"';
        }
        if((dataStruct.req.params.department_name).toLowerCase()!='*')
        {
            query = query + ' AND d.department_name="'+dataStruct.req.params.department_name+'"';
        }
        if((dataStruct.req.params.product_name).toLowerCase()!='*')
        {
            var prods = (dataStruct.req.params.product_name).split(" ");
            for(var x = 0;x<prods.length;x++)
            {
                query = query + ' AND b.product_name LIKE "%'+prods[x]+'%"';
            }
        }
        dataStruct['filter']=[];
        dataStruct['filter']=query;
        getList(dataStruct);
    }catch(err){
        var msg = {
            status:"Error",
            message:"[getProductWithFilters]Error Processing Data",
            desc:err
        };
        GLOBAL.sendError(msg,dataStruct.res);
    }
}

var getAllDepartments = function(dataStruct)
{
    try{
        var query = "SELECT  `department_name` FROM  `cs_departments` ORDER BY id";
        GLOBAL.db_cs_common.query(query,function(err,rows,fields){
            if(err==null)
            {
                var msg = {
                    status:"Success",
                    message:"[getAllDepartments]Data Found",
                    data:rows
                };
                GLOBAL.sendError(msg,dataStruct.res);
            }else{
               var msg = {
                    status:"Error",
                    message:"[getAllDepartments]MySQL Error",
                    desc:err
                };
                GLOBAL.sendError(msg,dataStruct.res); 
            }
        });
    }catch(err){
        var msg = {
            status:"Error",
            message:"[getAllDepartments]Error Processing Data",
            desc:err
        };
        GLOBAL.sendError(msg,dataStruct.res);
    }
}
var getCountStickied = function(dataStruct)
{
	//query to get the current number of people who stickied this and the latest person who stickied this
	
}

var getAdvertisement = function(dataStruct)
{
    var query = {
        "_id":mongojs.ObjectId(dataStruct.req.params.id)
    };
    GLOBAL.mongodb.ugcPhotostyx.find(query,function(err,docs){
        if(err==null)
        {
            var msg = {
                status:"Success",
                message:"[getAdvertisement]Data Found",
                data:docs[0]
            };
           
        }else{
            var msg = {
                status:"Error",
                message:"[getAdvertisement]Error Processing Data",
                desc:err
            };
            
        }
         GLOBAL.sendError(msg,dataStruct.res);
    });
}

module.exports = {
	getList:getList,
    getProductWithFilters:getProductWithFilters,
    getAllDepartments:getAllDepartments,
    getCountStickied:getCountStickied,
    getAdvertisement:getAdvertisement
};

/*
SELECT * FROM cs_stores a, cs_products b, cs_product_images c, cs_departments d WHERE (a.store_id = b.store_id AND a.user_id= b.user_id AND a.claimed_by = 57 AND a.claimed_by != 0) AND (c.store_id = b.store_id AND c.user_id = b.user_id AND c.store_id = b.store_id AND c.default_image = 1) AND (d.id = b.department_id) AND (b.publish_type = 1 AND b.mark_as_deleted = 'N')

cs_product_images = use product_id, store_id, user_id from taas and default_image = 1


return array('original' => $dir.$filename_noext.$ext,
					'webstore' => $dir.$filename_noext.'_webstore960'.$ext,
					'medium' => $dir.$filename_noext.'_medium650'.$ext,
					'small' => $dir.$filename_noext.'_small300'.$ext,
					'xsmall' => $dir.$filename_noext.'_xsmall100'.$ext);

{ store_id: 2,
    is_external: '1',
    external_url: 'http://lovethesign.com',
    added_by: 1316,
    claimed_by: 57,
    store_name: 'lovethesign',
    store_description: null,
    user_id: 0,
    collection_id: 0,
    single_id: 0,
    type: 4,
    unique_key: '',
    store_url: 'lovethesign',
    date_created: Thu Aug 07 2014 05:44:53 GMT+0800 (PHT),
    store_type: 0,
    social_account: '',
    store_logo: '20140922-1411432456-lovethesign.jpg',
    store_image: '',
    store_logo_type: 2,
    webstore_settings: '',
    current_step: 1,
    total_internal_product: 0,
    total_collection: 2,
    total_product: 52,
    total_sticked_product: 0,
    total_follower: 4,
    total_following: 0,
    total_contributors: 3,
    total_comment: 0,
    total_view: 0,
    total_ordered: 0,
    date_last_followed: Wed Nov 26 2014 00:00:00 GMT+0800 (PHT),
    date_last_added_product: '0000-00-00',
    product_id: 93,
    category_id: 0,
    department_id: 4,
    external_product_url: 'http://www.lovethesign.com/eu/products/lighting/pendants/studio-zappriani/fairytale-pendant-%C3%B8-60-cm',
    external_tags: 'Pendants,Lighting,Products,Studio Zappriani',
    external_lastupdated: Thu Aug 07 2014 05:44:53 GMT+0800 (PHT),
    shop_id: 0,
    product_name: 'Fairytale pendant Ø 60 cm',
    product_desc: 'This drum lamp by Zappriani impresses with its simplicity and rich purple tone, which gives your room the cosy and comfort you are looking for.It suits equally well living rooms, dining rooms, lounges, offices or restaurants.\nStudio Zappriani specialises in creating and manufacturing high-quality lampshades for a multitude of rooms. Each product is 100% handmade with care in each detail.\n\nMeasurements H 25 cm / Ø 60 cm\nMaterials 100% Cotton',
    overview: '',
    color: '#683788',
    ribbon: '',
    occasion_id: 0,
    recipient_id: 0,
    sku: '',
    currency: 'USD',
    base_price: 177,
    sale_price: 0,
    price: 177,
    currency_price: 0,
    is_taxable: 0,
    weight: 0,
    weight_id: 0,
    dimension_length: 0,
    dimension_width: 0,
    dimension_height: 0,
    length_id: 0,
    shipping_meta_type: 'G',
    shipping_meta: '',
    shipping_type: 0,
    shipping_fee: 0,
    inventory: 'I',
    quantity: 0,
    allow_purchase: 0,
    payout_type: 0,
    payout_rate: 0,
    total_earning: 0,
    minimum_order: 0,
    seo_title: 'Fairytale pendant Ø 60 cm',
    seo_url: 'fairytale-pendant-60-cm',
    seo_metadata: '',
    date_updated: Wed Sep 24 2014 13:17:26 GMT+0800 (PHT),
    publish_type: 1,
    mark_as_deleted: 'N',
    in_collection: 1,
    duplicate_counter: 0,
    last_user_id: 0,
    last_date_added: Mon Sep 22 2014 22:31:16 GMT+0800 (PHT),
    last_store_id: 2,
    last_collection_id: 1,
    total_sticked: 0,
    total_sticked_byuser: 0,
    total_like: 0,
    total_cliked_buynow: 0,
    total_ordered_shipped: 0,
    total_ordered_cancelled: 0,
    date_lastview: '0000-00-00 00:00:00',
    date_lastlike: '0000-00-00 00:00:00',
    date_lastcomment: '0000-00-00 00:00:00',
    date_lastbuynow: '0000-00-00 00:00:00',
    sort_order: 1,
    image_small: '',
    image_large: '20140828/93/140928988354000e9b724e5878861614.jpg',
    default_image: 1,
    alt: '',
    id: 4,
    department_name: 'Home' }



*/