var startPageEditor = require(__dirname+'/lib/CRUD/get/startPageEditor');
var mySQLStuff = require('./lib/mysql_connects/mysql_convertions');
var verifyAccess = require('./securityChecks');
var getProducts = require('./lib/CRUD/get/getProducts');
var saveAdvertisement = require('./lib/CRUD/post/saveAdvertisement');
module.exports = function(router){

	//photostyx routes
	router.route('/:token/:user_id/:store_id/photostyx/create/')
		.get(function(req,res){
			validate(req,res,startPageEditor.photoStyxCreate);
		});
	router.route('/:token/:user_id/:store_id/photostyx/edit/:id')
		.get(function(req,res){
			validate(req,res,startPageEditor.photoStyxCreate);
		});
	router.route('/:token/:user_id/get/advertisement/:id')
		.get(function(req,res){
			validate(req,res,getProducts.getAdvertisement);
		});
	router.route('/:user_id/show/advertisement/:id')
		.get(function(req,res){
			req.params.token = "bypass";
			validate(req,res,getProducts.getAdvertisement);
		});
	//photostyx images
	router.route('/cdn/uploads/advertisement/photostyx/:user_id/:store_id/:filename')
		.get(function(req,res){
			// if(process.env.NODE_ENV!='production')
			// {
				var rootPath = __dirname;
				var holder = rootPath.split('server');
				rootPath = holder[0];
				res.sendFile(req.params.filename,{root:rootPath+'/cdn/uploads/advertisement/photostyx/'+req.params.user_id+'/'+req.params.store_id});
			// }else{
			// 	//get image from rackspace CDN
			// }
		});
	//show photostyx
	//http://192.168.56.101:8765/show/photostyx/5564285bc7ea3ce371175b29
	router.route('/:user_id/:store_id/show/photostyx/:id')
		.get(function(req,res){
			req.params.show = true;
			req.params.token = "bypass";
			validate(req,res,startPageEditor.photoStyxCreate);
		});
	//videostyx routes
	router.route('/:token/:user_id/:store_id/videostyx/create/')
		.get(function(req,res){
			validate(req,res,startPageEditor.videoStyxCreate);
		});

	//get your products
	router.route('/:token/:user_id/:store_id/get/product_list/:colortype/:department_name/:product_name/:number_of_records/:page_number/')
		.get(function(req,res){
			validate(req,res,getProducts.getProductWithFilters);
		});
	router.route('/get/count/stickied/:product_id/:store_id/:user_id')//no token and userid because this will be used to reconstruct the view
		.get(function(req,res){
			validate(req,res,getProducts.getCountStickied);
		});
	//get categories
	router.route('/:token/:user_id/get/all/departments/')
		.get(function(req,res){
			validate(req,res,getProducts.getAllDepartments);
		});
	//global read routers
	//image file readers
	router.route('/images/:image')
		.get(function(req,res){
			res.sendFile(__dirname + '/images/'+req.params.image);
		});
	router.route('/images/:dir1/:dir2/:image')
		.get(function(req,res){
			res.sendFile(__dirname + '/images/'+req.params.dir1+'/'+req.params.dir2+'/'+req.params.image);
		});
	//save photostyx advert
	router.route('/:token/:user_id/:store_id/save/photostyx/')
		.post(function(req,res){
			validate(req,res,saveAdvertisement.savePhotoStyx);
		});

	//404
	router.route('*')
		.get(function(req,res){
			msg = {
				status:"Error",
				message:"400 page not found",
				description: ""
			};
			GLOBAL.sendError(msg,res);
		})
		.post(function(req,res){
			msg = {
				status:"Error",
				message:"400 page not found",
				description: ""
			};
			GLOBAL.sendError(msg,res);
		})
		.put(function(req,res){
			msg = {
				status:"Error",
				message:"400 page not found",
				description: ""
			};
			GLOBAL.sendError(msg,res);
		})
		.delete(function(req,res){
			msg = {
				status:"Error",
				message:"400 page not found",
				description: ""
			};
			GLOBAL.sendError(msg,res);
		});
}

function validate(req,res,callback)
{
	var dataStruct = {
		"user_id":parseInt(req.params.user_id),
		"token":req.params.token,
		"req": req,
		"res": res,
		"newSimpleDate":'',
		"newUnixDate":'',
		"timezone":"America/Los_Angeles"
	};
	var newSimpleDate = mySQLStuff.MySQLDateTimeNOW(dataStruct);
	dataStruct.newSimpleDate = newSimpleDate;
	//currently we have store_id and user_id fixed
	if(process.env.NODE_ENV=='development' || dataStruct.token == 'bypass')// && parseInt(req.params.token) == 0)
	{
		callback(dataStruct);
	}else{
		verifyAccess.checkToken(dataStruct,callback);
	}
}