/*
 * LoginDialog
 */
function LoginDialog(){
	var url = root + "signin-dialog.html";
	modalbox.get(url, {title:'Login', hideCloseBtn:false},{width:658, position: ['center',100]});
	$('.ui-dialog-titlebar').before('<div id="modalbox-colors-logo"><div class="one"></div><div class="two"></div><div class="three"></div>');
	$('#ui-dialog-title-dialog-box').append(' <span class="ajaxloader"></span>');
}

/*
 * RegisterDialog
 */
function RegisterDialog($this){
	var url = root + "register-dialog.html";
	modalbox.get(url , {title:'', hideCloseBtn:false},{width:720, position: ['center',40]});
	$('.ui-dialog-titlebar').before('<div id="modal-register-box-colors-logo"><div class="one"></div><div class="two"></div><div class="three"></div>');
	$('.ui-dialog .ui-dialog-titlebar').css({'height':'115px'});
	$('.ui-dialog-titlebar').html( '<div class="custom-titlebox custom-titlebox-b">\
										<div class="social-register-box social-register-box-b">\
											<span class="login-register">Create an account with <span class="ajaxloader"></span></span> \
											<span class="fb_logo" onclick="javascript: popUp(\''+ root +'fb_login.html\');" ></span>\
											<span class="y_logo" onclick="javascript: popUp(\''+ root +'user/openid_login/yahoo?login\');" ></span>\
											<span class="g_logo" onclick="javascript: popUp(\''+ root +'user/openid_login/google?login\');" ></span>\
										</div>\
										<p class="register-link register_already gray_c align-right">Already on ShopStyx?</span> <a id="login" class="login_btn_style" href="javascript:;">LOG IN</a></p>\
									</div> <input class="register_type" type="hidden" name="register_type" value="' + ($this).attr('data') +'" /> ' );
}

/*
 * PageSettingsDialog
 */
function PageSettingsDialog($status){
	var status = $status.val();
	$status.val('');
	modalbox.get(root + 'page-settings-dialog.html/' + status, {title:''},{width:720, position: ['center',40]});
	$('.ui-dialog-titlebar').before('<div id="top-bar"><div class="one"></div><div class="two"></div><div class="six"></div><div class="seven"></div><div class="eight"></div></div>');
}

/*
 * LostPasswordDialog
 */
function LostPasswordDialog(){
	var url = root + "lost-password-form.html";
	modalbox.get(url, {title:'Change Password', hideCloseBtn:false},{width:600, position: ['center',100]});
	$('#ui-dialog-title-dialog-box').append(' <span class="ajaxloader"></span>');
	$('.ui-dialog-titlebar').before('<div id="modalbox-colors-logo"><div class="one"></div><div class="two"></div><div class="three"></div></div>');
}

/*
 * LostPasswordDialog
 */
function ChangePasswordDialog(){
	var url = root + "change-password-form-modal.html";
	modalbox.get(url, {title:'Reset your password', hideCloseBtn:true},{width:600, position: ['center',100]});
	$('.ui-dialog-titlebar').before('<div id="modalbox-colors-logo"><div class="one"></div><div class="two"></div><div class="three"></div><div class="four"></div><div class="five"></div></div>');
}

/*
 * LoginForm
 */
function LoginForm($form){
	$('.loader').remove();
	$('.ajaxloader').html('<img class="loader" src="'+cloud_url+'images/ajax-loader.gif" />');
	var tz = jstz.determine();
	
	url = $form.attr( 'action' );
	var form_data = {};

	form_data.email = $form.find( 'input[name="email"]' ).val();
	form_data.password = $form.find( 'input[name="password"]' ).val();
	form_data.remember= $form.find('input[name="remember"]').is(':checked')?1:0;
	form_data.url = $form.attr( 'action' );
	form_data.temp_sessionid = vid;
	form_data.token = vid;
	form_data.tz = tz.name();
	
	store.set('remember', form_data.remember);
	
	var timestamp = new Date().getTime();
	store.set('session_date', timestamp);
		

	store.set('temp_sessionid', vid);
	store.set('token', vid);
	
	$('.message').html('');
	
	$.ajax({
		type: "POST",
		url: url,
		data: form_data,
		cache: false,
		success: function(data){
			var _data = $.parseJSON(data);
			if(_data.result == 'failed'){
				$('.message').html(_data.message);
			}else{
				store.set('display_name', _data.display_name);
				store.set('user_id', _data.user_id);
				$('.main-preloader').show();
				modalbox.close();
				location.href = root;
			}
			$('.loader').remove();
		}
	});
	return false;
}

/*
 * RegisterForm
 */
function RegisterForm($form){
	$('.loader').remove();
	
	if($('#agree').attr('checked')){
		$("#registerbtn").attr("disabled", "disabled");
		$('.ajaxloader').html('<img class="loader" src="'+cloud_url+'images/ajax-loader.gif" />');
		url = $form.attr( 'action' );
		var form_data = {};

		form_data.firstname = $form.find( 'input[name="firstname"]' ).val();
		form_data.lastname = $form.find( 'input[name="lastname"]' ).val();
		form_data.email = $form.find( 'input[name="email"]' ).val();
		form_data.password = $form.find( 'input[name="password"]' ).val();
		if($('.register_type').val() != "undefined"){
			form_data.user_type = $('.register_type').val();
		}
		form_data.temp_sessionid = vid;
		form_data.token = vid;
		
		store.set('temp_sessionid', vid);
		store.set('token', vid);
		
		$('.message').html('');
		
		$.ajax({
			type: "POST",
			url: url,
			data: form_data,
			cache: false,
			success: function(data){//alert(data);
				var _data = $.parseJSON(data);
				
				if(_data.register_result == false){
					$('.error2').html('');
					
					if(_data.error_class == '.gen_error'){
						$(_data.error_class).html('<span style="width:616px;" class="fberrorbox">' + _data.error_msg + '</span>');
					}else{
						$(_data.error_class).html(_data.error_msg);
					}
					$("#registerbtn").removeAttr("disabled");
				}else{
					
					csConfirm( 'Success!', 
						'<div id="success-message-box">\
						 <p>You should receive an email shortly.</p>\
						 <p>If you still need assistance, contact <strong>ShopStyx Support</strong></p>\
						 </div>\
						',
						{Ok: null}, null, null,
						{ width:520, position:['center',40] }
					); 
					$('.ui-dialog-titlebar').before('<div id="modalbox-colors-logo"><div class="one"></div><div class="two"></div><div class="three"></div><div class="four"></div><div class="five"></div></div>');

				}
				$('.loader').remove();
				
			}
		});
		return false;
	}else{
		alert('You must agree to the terms and conditions');
		return false;
	}

}

/*
 * RegisterForm Home
 */
function MainRegisterForm($form){
	$('.loader').remove();
	$("#sign-up-button").attr("disabled", "disabled");
	$('.create-fields #popup-preloader-wrapper').show();
	url = $form.attr( 'action' );
	var form_data = {};

	form_data.firstname = $form.find( 'input[name="firstname"]' ).val();
	form_data.lastname = $form.find( 'input[name="lastname"]' ).val();
	form_data.email = $form.find( 'input[name="email"]' ).val();
	form_data.password = $form.find( 'input[name="password"]' ).val();
	form_data.temp_sessionid = vid;
	form_data.token = vid;
	
	store.set('temp_sessionid', vid);
	store.set('token', vid);
	
	$('.message').html('');
	
	$.ajax({
		type: "POST",
		url: url,
		data: form_data,
		cache: false,
		success: function(data){//alert(data);
			var _data = $.parseJSON(data);
			
			if(_data.register_result == false){
				$('.error2').html('');
				
				if(_data.error_class == '.gen_error'){
					$('.gen_error').html('<span style="width:88%;" class="fberrorbox">' + _data.error_msg + '</span>');
				}else{
					$('.gen_error').html('<span style="width:88%;" class="fberrorbox">' + _data.error_msg + '</span>');
				}
				$("#sign-up-button").removeAttr("disabled");
			}else{
				
				csConfirm( 'Success!', 
					'<div id="success-message-box">\
					 <p>You should receive an email shortly.</p>\
					 <p>If you still need assistance, contact <strong>ShopStyx Support</strong></p>\
					 </div>\
					',
					{Ok: null}, null, null,
					{ width:520, position:['center',40] }
				); 
				$('.ui-dialog-titlebar').before('<div id="modalbox-colors-logo"><div class="one"></div><div class="two"></div><div class="three"></div><div class="four"></div><div class="five"></div></div>');

			}
			$('.create-fields #popup-preloader-wrapper').hide();
			
		}
	});
	return false;
	

}

/*
 * ForgotPasswordForm
 */
function ForgotPasswordForm($form){
	$('.loader').remove();
	 
	$('.ajaxloader').html('<img class="loader" src="'+cloud_url+'images/ajax-loader.gif" />');
	$('.error2').html('');
	
	url = $form.attr( 'action' );
		var form_data = {};
		form_data.email = $form.find( 'input[name="email"]' ).val();
		
	$.ajax({
		type: "POST",
		url: url,
		data: form_data,
		cache: false,
		success: function(data){
			var _data = $.parseJSON(data); 
			if(_data.request_result == false){
				$(_data.error_class).html(_data.error_msg);
			}else{
				csConfirm( 'Success!', 
						'<div id="success-message-box">\
						 <p>You should receive an email shortly.</p>\
						 <p>If you still need assistance, contact <strong>ShopStyx Support</strong></p>\
						 </div>\
						',
						{Ok: null}, null, null,
						{ width:520, position:['center',40] }
					); 
					$('.ui-dialog-titlebar').before('<div id="modalbox-colors-logo"><div class="one"></div><div class="two"></div><div class="three"></div><div class="four"></div><div class="five"></div></div>');

			}
			$('.loader').remove();
		}
	});
	return false;
}

/*
 * ChangePasswordForm
 */
function ChangePasswordForm($form){
	$('.loader').remove();
	
	$('.ajaxloader').html('<img class="loader" src="'+cloud_url+'images/ajax-loader.gif" />');
	$('.error2').html('');
	
	url = $form.attr( 'action' );
		var form_data = {};
		form_data.password = $form.find( 'input[name="password"]' ).val();
		form_data.reset_password_key = $form.find( 'input[name="reset_password_key"]' ).val();
		form_data.temp_sessionid = vid;
		form_data.token = vid;
		
		store.set('temp_sessionid', vid);
		store.set('token', vid);
		
	$.ajax({
		type: "POST",
		url: url,
		data: form_data,
		cache: false,
		success: function(data){
			var _data = $.parseJSON(data); 
			if( _data.result ){
				store.set('display_name', _data.display_name);
				store.set('user_id', _data.user_id);
				csConfirm( 'Success!', 
					'<div id="success-message-box">\
					 <p>Your password has been changed. <a href="' + root + '"><strong>Click here to continue</strong></a></p>\
					 </div>\
					',
					{Ok: null}, null, null,
					{ width:520, position:['center',40] }
				); 
				$('.ui-dialog-titlebar').before('<div id="modalbox-colors-logo"><div class="one"></div><div class="two"></div><div class="three"></div><div class="four"></div><div class="five"></div></div>');

			}
		}
	});
	return false;
}


/*
 * Logout
 */
function Logout(){
	var url = root + 'signout.html';
	var form_data = {};
		form_data.post_dta = 1;
		form_data.temp_sessionid = store.get('temp_sessionid');
		form_data.token = store.get('token');
		
	$.ajax({
		type: "POST",
		url: url,
		data: form_data,
		cache: false,
		success: function(data){
			if(data == 'success'){
				store.clear();
				store.set('logout_status', 1);
				location.href = root;
			}
		}
	});
	return false;
}

/*
 * Subscription Successful
 */
function SubscriptionSucess(){
	var is_subscribe = store.get('is_subscribe');
	if(is_subscribe != "Y") { return false; }
	
	var form_data = {};
	form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
	form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
	
	$.post(root + 'account.html?action=subscription_success', form_data,function(content){ 
		if(!content || content == null || $.isEmptyObject(content)) {
			return false;
		}
		$('.premium-vendor').trigger('click');
		//$('.premium-vendor-wrapper #popup-preloader-wrapper').show();
		$('.premium-vendor-wrapper .box-content').html(content);
		$('.premium-vendor-wrapper #popup-preloader-wrapper').hide();
		$('.package-wrapper .scrollbar').jScrollPane();
		store.remove('is_subscribe');
	});
}



/* 
 * below are functions for dialog UI * 
 * 
 * Load dialog and set content to image loader
 * 
 * showDialogLoader
 */
function showDialogLoader(args){
	var defOptions = {width:400, height:'auto', title:'', resizable:false, show:'fade', modal:true, closeOnEscape:true, position:['center','center']};
	args = typeof(args)=='object'?args:{};
	
	var options = $.extend({}, defOptions, args);
	
	//remove existing dialog
	removeDialogUI();
    
	$(msPopupId).html('<div id="dialog-box"><div style="width: 100%; text-align: center;"><img src="' + cloud_url + 'images/dialog-ajax-loader.gif" /></div></div>');
	
	$(msDialogId).dialog({
        width: options.width,
        height: options.height,
        title: options.title,
        resizable: options.resizable,
        show: options.show,
        modal: options.modal,
        position: options.position,
        closeOnEscape: options.closeOnEscape
    });
}
/*
 * removeDialogUI 
 */
function removeDialogUI(){
	$(msDialogId).remove();
    $('.ui-dialog').remove(); // dont know what is this, but lets keep it
}
/*
 * setDialogHTMl 
 */
function setDialogHTMl(html){
	$(msDialogId).html(html);
}

/**
 * csConfirm mimics traditional confirm method and trigger a chained function "ok" or "cancel"
 */

function csConfirm( title, question, callback, init, on_before_close, settings )
{
	var modal = typeof modalbox !== "undefined" ? modalbox : new ModalBox( );
	
	modal.dialog
	( {
		title:	title,
		html:	''
	}, settings || {
		width:	350
	} );
	
	var buttons = callback || {
		"Ok":	null,
		"No":	null
	};
	modal.dialogbox.append( question );
	$( '<br />' ).css( {clear: 'both', display: 'block'} ).appendTo( modal.dialogbox );
	var actions = $( '<p>' ).addClass( 'opt-btn-wrapper' ).appendTo( modal.dialogbox );
	
	$.each( buttons, function( x, y )
	{
		var button = $( '<input>' )
			.attr( {
				type:		'button',
				value:		x
			} )
			.addClass( 'opt-btn' )
			.css( {marginBottom: '10px', marginTop: '10px', marginRight: '10px'} );
		
		button.appendTo( actions );
	} );
	
	actions.find( 'input[type=button]' ).click( function( e )
	{
		if( typeof on_before_close == 'function' )
			if( on_before_close( ) === false )
				return;
		modal.close( );
		var response = $( this ).val( ),
			call = typeof callback[ response ] ? callback[ response ] : new Function( );

		call( );
	} );
	
	typeof init == 'function' && init( modal, modal.dialogbox );
};

function msError( error )
{
	var modal = typeof modalbox !== "undefined" ? modalbox : new ModalBox( );
	
	modal.dialog
	( {
		title:	'Error',
		html:	'<p class="notify error"><span>&nbsp;</span>' + error + '</p>'
	}, {
		width:	350
	} );
};