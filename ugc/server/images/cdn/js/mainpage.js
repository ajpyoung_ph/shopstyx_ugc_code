$(document).ready(function(){
	$(document).on('focus', '.h_email', function(){
		if($(this).val() == "Email"){
			$(this).val('');
		}
		
	});
	
	$(document).on('blur', '.h_email', function(){
		if($(this).val() == ""){
			$(this).val('Email');
		}
		
	});
	
	$(document).on('focus', '.h_store_name', function(){
		if($(this).val() == "Store Name"){
			$(this).val('');
		}
		
	});
	
	$(document).on('blur', '.h_store_name', function(){
		if($(this).val() == ""){
			$(this).val('Store Name');
		}
		
	});
	
	$(document).on('focus', '.password_e', function(){
		$(this).replaceWith('<input class="h_password password_f" type="password" name="h_password" value="" />');
		$('#h_pass_detect').val(1);
	});
	
	$(document).on('blur', '.password_f', function(){
		if($(this).val() == ""){
			$(this).replaceWith('<input class="h_password password_e" type="text" name="h_password" value="Password" />');
		}
	});
	
	setInterval(function () {
		if($("#h_pass_detect").val() == 1){
			$('#h_pass_detect').val(0);
			$('.h_password').focus();
		}
	}, 10);
});