(function($){	
	$.fn.getNextHighestZindex = function(){
		var highestIndex = 0;
		var currentIndex = 0;
		var elArray = Array();
		this.each(function(){
			$this = this || document;
			elArray = $this.getElementsByTagName('*');
			for(var i=0; i < elArray.length; i++){
				if (elArray[i].currentStyle){
				currentIndex = parseFloat(elArray[i].currentStyle['zIndex']);
				}else if(window.getComputedStyle){
					currentIndex = parseFloat(document.defaultView.getComputedStyle(elArray[i],null).getPropertyValue('z-index'));
				}
				if(!isNaN(currentIndex) && currentIndex > highestIndex){ highestIndex = currentIndex; }
			}
		});
		return (highestIndex+1);
	};

	// auto font resizing
	$.fn.textfill = function(options) {
        var fontSize = options.maxFontPixels;
        var ourText = $('span:visible:first', this);
        var maxHeight = $(this).height();
        var maxWidth = $(this).width();
        var textHeight;
        var textWidth;
        do {
            ourText.css('font-size', fontSize);
            textHeight = ourText.height();
            textWidth = ourText.width();
            fontSize = fontSize - 1;
        } while ((textHeight > maxHeight || textWidth > maxWidth) && fontSize > 3);
        return this;
    };
})(jQuery);


var jSpane_api_tab_options;

function productentry_statusloader(obj) {
	if(obj.show) {
		$('#popup_wrapper.productentry-wrapper form').addClass('opacity');
		$('.form-submit-status').fadeIn( 200, function(){
			$(this).find(".status_info_wrapper").html('<span class="status_info">'+ obj.message +'</span>');
		});
		return;		
	}
	else {
		$('.form-submit-status').find(".status_info").fadeOut(300, function(){
			$(this).remove();
		});
		$('.form-submit-status').find(".status_info_wrapper").fadeIn( 300, function(){
			$(this).html('<span class="status_info">'+ obj.message +'</span>');
		});
		$('.form-submit-status').delay(3600).fadeOut(500, function(){
			$(this).remove();
			var callbacks = $.Callbacks();
			callbacks.add( obj.callback );
			callbacks.fire();
			$('.form-submit-status').stop().fadeOut( 400 ); // prevent from calling this effect twice
		});
	}
	
}

function _load_customized_select() {
	$('.customized_select select').each(function(){
		if($(this).parent().find('div').length == 0) {
			title = $('option:selected',this).text();
			$(this)
				.css({'z-index':10,'opacity':0,'-khtml-appearance':'none'})
				.after('<div>' + title + '</div>')
				.change(function(){
					val = $('option:selected',this).text();
					$(this).next().text(val);
				})
		}
		
	});
}

function _check_productentry_errors() {
	var error = false;
	var section = new Array();
	section['details'] = false;
	section['attributes'] = false;
	section['optionsinventory'] = false;
	section['categories'] = false;
	section['images'] = false;
	section['shipping'] = false;
	section['seo'] = false;
	
	var details = attributes = images = categories = optionsinventory = shipping = seo = 0;
	$("#fileupload").validationEngine();
	
	if($.trim($('#product-entry-product_name').val()) == '') {
		error = true;
		$('#product-entry-product_name').validationEngine('showPrompt', 'Please provide a product title.', 'error', 'bottomLeft', true);
		details++;
		
	}
	else {
		$('#product-entry-product_name').validationEngine('hide');
	}
	
	
	if($.trim($('#product-entry-product_desc').val()) == '') {
		error = true;
		$('#product-entry-product_desc').validationEngine('showPrompt', 'Please provide a details.', 'error', 'bottomLeft', true);
		details++;
	}
	else {
		$('#product-entry-product_desc').validationEngine('hide');
	}
		
	if(details > 0) {
		section['details'] = true;
	}
	
	
	if($.trim($('#product-entry-sku').val()) == '') {
		error = true;
		$('#product-entry-sku').validationEngine('showPrompt', 'Please provide a SKU.', 'error', 'bottomLeft', true);
		attributes++;
	}
	else {
		$('#product-entry-sku').validationEngine('hide');
	}
	
	if($.trim($('#product-entry-base_price').val()) == '' || parseFloat($('#product-entry-base_price').val()) <= 0) {
		error = true;
		$('#product-entry-base_price').validationEngine('showPrompt', 'Please provide a price.', 'error', 'bottomLeft', true);
		attributes++;
	}
	else {
		$('#product-entry-base_price').validationEngine('hide');
	}
	
	if($.trim($('#product-entry-payout_rate').val()) == '' || parseFloat($('#product-entry-payout_rate').val()) <= 0) {
		error = true;
		$('#payoutrate-wrapper').validationEngine('showPrompt', 'Please provide a payout rate.', 'error', 'bottomLeft', true);
		attributes++;
	}
	else {
		$('#payoutrate-wrapper').validationEngine('hide');
	}
	
	if($.trim($('#product-entry-weight').val()) == '' || parseFloat($('#product-entry-weight').val()) <= 0) {
		error = true;
		$('#product-entry-weight').validationEngine('showPrompt', 'Please provide a weight.', 'error', 'bottomRight', true);
		attributes++;
	}
	else {
		$('#product-entry-weight').validationEngine('hide');
	}
	
	var psize = (parseFloat($('#product-dimension-length').val()) + 
				parseFloat($('#product-dimension-width').val()) + 
				parseFloat($('#product-dimension-height').val()));
	
	if(parseFloat($('#product-dimension-length').val()) == 0 
	|| isNaN(parseFloat($('#product-dimension-length').val()))
	|| parseFloat($('#product-dimension-width').val()) == 0 
	|| isNaN(parseFloat($('#product-dimension-width').val()))
	|| parseFloat($('#product-dimension-height').val()) == 0
	|| isNaN(parseFloat($('#product-dimension-height').val()))) {
		error = true;
		$('#product-size-entry-container').validationEngine('showPrompt', 'Please provide a payout rate.', 'error', 'bottomRight', true);
		attributes++;
	}
	else {
		$('#product-size-entry-container').validationEngine('hide');
	}
	
	if(attributes > 0) {
		section['attributes'] = true;
	}
	
	/*
	if(get_productID() == 0) {
		if($('input[name="temp_instock"]').length > 0 && $('input[name="temp_instock"]').eq(1).is(':checked')) {
			var quantity = parseInt($('input#quantity').val());
			quantity = isNaN(quantity) ? 0 : quantity;
			
			if(quantity == 0) {
				error = true;
				$('.productentry-box-content #tab-options .stockinventory-wrapper input.quantity').validationEngine('showPrompt', 'Please specify a quantity.', 'error', 'bottomLeft', true);
				section['optionsinventory'] = true;
			}
			else {
				$('.productentry-box-content #tab-options .stockinventory-wrapper input.quantity').validationEngine('hide');
			}
		}
		else {
			error = true;
			$('.productentry-box-content #tab-options .stockinventory-wrapper input.quantity').validationEngine('showPrompt', 'Please specify a quantity.', 'error', 'bottomLeft', true);
			section['optionsinventory'] = true;
		}
	}
	else {

	}
	*/
	var photos_queud = (typeof (qq_fileCount) == "undefined") ? 0 : qq_fileCount;
	var current_photos = $('.thumb-photos-wrapper .thumb-row .thumb').length;
	var existing_photos = ($.trim($('.productentry-wrapper').data('photos')) == '') ? 0 : $('.productentry-wrapper').data('photos');
	var count_photos = 0;
	
	if($('.mphoto-wrapper-sub .thumb-photos-wrapper').length == 0) {
		count_photos = 	existing_photos;
	}
	else {
		count_photos = (current_photos + photos_queud);
	}
	
	if(count_photos == 0) {
		error = true;
		$('.productentry-box-content #tab-images .mphoto-wrapper').validationEngine('showPrompt', 'Please upload at least 1 photo.', 'error', 'topRight', true);
		images++;
	}
	else {
		$('.productentry-box-content #tab-images .mphoto-wrapper').validationEngine('hide');
	}
	
	if(images > 0) {
		section['images'] = true;
	}
	
	if($.trim($('.productentry-wrapper').data('product_categories')) == '') {
		error = true;
		$('.productentry-box-content #tab-categories #cat-list-wrapper').validationEngine('showPrompt', 'Please select at least one category.', 'error', 'topLeft', true);
		categories++;
	}
	else {
		$('.productentry-box-content #tab-categories #cat-list-wrapper').validationEngine('hide');
	}
	
	if($.trim($('.productentry-wrapper').data('department_id')) == '' || $('.productentry-wrapper').data('department_id') == 0) {
		error = true;
		$('.productentry-box-content #tab-categories #main-department').validationEngine('showPrompt', 'Please select shopstyx department.', 'error', 'centerTop', true);
		categories++;
	}
	else {
		$('.productentry-box-content #tab-categories #main-department').validationEngine('hide');
	}
	
	if(categories > 0) {
		section['categories'] = true;
	}
	// shipping
	if($.trim($('.productentry-wrapper').data('shipping')) == 'false' || $.trim($('.productentry-wrapper').data('shipping')) == '' || 
	($.trim($('.productentry-wrapper').data('shipping')) == "" && $('#shipping_global .sr').length == 0) ) {
		error = true;
		$('.productentry-box-content #tab-shipping #shipping-setings-wrapper').validationEngine('showPrompt', 'Please add at least one shipping destination.', 'error', 'topLeft', true);
		shipping++;
	}
	else {
		$('.productentry-box-content #tab-shipping #main-department').validationEngine('hide');
	}
	
	if(shipping > 0) {
		section['shipping'] = true;
	}
	
	// seo
	if($.trim($('#product-entry-seo_url').val()) == '') {
		error = true;
		$('#product-entry-seo_url').validationEngine('showPrompt', 'Please provide a SEO URL.', 'error', 'bottomLeft', true);
		seo++;
		
	}
	else {
		$('#product-entry-seo_url').validationEngine('hide');
	}
	
	if(seo > 0) {
		section['seo'] = true;
	}
	
	

	if(section['details'] == true) {
		$('.productentry-wrapper').find('#tabs a').eq(0).addClass('errortab');	
		$('.productentry-wrapper').find('#tabs a').eq(0).focus();
	}
	else {
		$('.productentry-wrapper').find('#tabs a').eq(0).removeClass('errortab');	
	}
	
	if(section['attributes'] == true) {
		$('.productentry-wrapper').find('#tabs a').eq(1).addClass('errortab');	
		$('.productentry-wrapper').find('#tabs a').eq(1).focus();
	}
	else {
		$('.productentry-wrapper').find('#tabs a').eq(1).removeClass('errortab');	
	}
	
	if(section['images'] == true) {
		$('.productentry-wrapper').find('#tabs a').eq(2).addClass('errortab');	
		$('.productentry-wrapper').find('#tabs a').eq(2).focus();
	}
	else {
		$('.productentry-wrapper').find('#tabs a').eq(2).removeClass('errortab');	
	}
	
	if(section['optionsinventory'] == true) {
		$('.productentry-wrapper').find('#tabs a').eq(3).addClass('errortab');	
		$('.productentry-wrapper').find('#tabs a').eq(3).focus();
	}
	else {
		$('.productentry-wrapper').find('#tabs a').eq(3).removeClass('errortab');	
	}
	
	if(section['categories'] == true) {
		$('.productentry-wrapper').find('#tabs a').eq(4).addClass('errortab');
		$('.productentry-wrapper').find('#tabs a').eq(4).focus();	
	}
	else {
		$('.productentry-wrapper').find('#tabs a').eq(4).removeClass('errortab');	
	}
	
	if(section['shipping'] == true) {
		$('.productentry-wrapper').find('#tabs a').eq(5).addClass('errortab');
		$('.productentry-wrapper').find('#tabs a').eq(5).focus();	
	}
	else {
		$('.productentry-wrapper').find('#tabs a').eq(5).removeClass('errortab');	
	}
	
	if(section['seo'] == true) {
		$('.productentry-wrapper').find('#tabs a').eq(7).addClass('errortab');
		$('.productentry-wrapper').find('#tabs a').eq(7).focus();	
	}
	else {
		$('.productentry-wrapper').find('#tabs a').eq(7).removeClass('errortab');	
	}
	
	if(error) {
		return true;	
	}
	else {
		return false;	
	}
}

function set_default_product_photo(dataObj) {
	var form_data = {'token': store.get('token'), 'user_id': store.get('user_id'), 'store_id': store.get('store_id')};
	form_data.sort_order = dataObj.sort_order;
	form_data.product_id = dataObj.product_id;
	
	$.ajax({
		type: "POST",
		url: '/store_product/set_default_photo',
		data: form_data,
		success: function(data){}
	});
}

function delete_product_photo(dataObj) {
	var form_data = {'token': store.get('token'), 'user_id': store.get('user_id'), 'store_id': store.get('store_id')};
	form_data.sort_order = dataObj.sort_order;
	form_data.product_id = dataObj.product_id;


	if(confirm('Are you sure you want to delete this image?')) {
		$('#thumb-product-image-'+dataObj.sort_order).remove();
		qq_fileCount--;
		
		//
		var i = 1;
		var style = '';
		$('.thumb-photos-wrapper .thumb-row .thumb').each(function(){
			$(this).removeClass('nomarginright');
			style = (i%4 == 0) ? 'nomarginright' : '';
			$(this).addClass(style);
			i++;	
		});
		
		if(jSpane_api_tab_images) jSpane_api_tab_images.reinitialise();
		if(typeof qq_fileCount != 'undefined') $('.thumb-photos-wrapper .thumb-row .thumb').length;
		
		if($('.thumb-photos-wrapper .thumb-row .thumb').length == 0) {
			$('.first-upload-btn').show(); 
			$('.addmore-upload-btn').hide();	
			$('#manual-fine-uploader').removeClass('more');
		}
				
		$.ajax({
			type: "POST",
			url: '/store_product/delete_photo',
			data: form_data,
			success: function(data){
				/*
				var _data = $.parseJSON(data);
				if(_data.success) {
						
				}
				*/
			}
		});
	}
}

function _search_products_inrelated(objData) {
	objData = (typeof (objData) === 'undefined')? { } : objData;
	objData.search_text = $('#tab-related .product-related-search-wrapper input.searchtext').val();

	if(objData.search_text == '') {
		alert('Please enter a text to search.');
		return;
	}

	form_data = $.extend({token:store.get('token'),user_id:store.get('user_id'),store_id:store.get('store_id')},objData);
	
	$('#tab-related .search-results-wrapper').html('<div class="ajaxloader" align="center"><img class="loader" src="/cdn/images/dialog-ajax-loader.gif" /></div>');	
	$.ajax({
		type: "POST",
		url: '/store_product/search_products',
		data: form_data,
		success: function(data){
			$('#tab-related .search-results-wrapper').html(data);
			var jSpane_tab_related_search = $('#tab-related .search-results-wrapper .product-listing-wrapper');
			jSpane_tab_related_search.jScrollPane();
			var jSpane_api_tab_related_search = jSpane_tab_related_search.data('jsp');	

			
			//if(typeof (jSpane_tab_related_search) == 'object') {
				// re-initialize content for the scrollbars (shows only scrollbards depending on the content)
				//jSpane_api_tab_related_search.reinitialise();
				//console.log(jSpane_api_tab_related_search);
				
			//}
		}
	});
}

function addto_relatedproduct(e) {
	var parent = $(e).parent();
	var data = $.parseJSON(parent.attr('data'));
	var html = '';
	
	html += '<li class="list"><a class="delete-btn" href="javascript: ;"></a><input type="hidden" name="products_related[]" value="'+data.user_id+'|'+data.product_id+'" />';
	html += '<div class="hoverborder">';
	html += '<div class="thumb"><img src="'+parent.find('img').attr('src')+'" /></div>';
	html += '<div class="details">';
	html += '<div class="prodname">'+parent.find('.prodname').html()+'</div>';
	html += '<div class="desc">'+parent.find('.desc').html()+'</div>';
	html += '</div>';
	html += '<br clear="all" /></div></li>';
	$('#tab-related .selected-product-related-wrapper .product-listing-wrapper ul').prepend(html);
	jSpane_api_tab_related_list.reinitialise();
}

$.urlParam = function(url,name){
    var results = new RegExp('[\\?&amp;]' + name + '=([^&amp;#]*)').exec(url);
    return results[1] || 0;
}

function product_reload_category_list(objData) {
	form_data = $.extend({token:store.get('token'),user_id:store.get('user_id'),store_id:store.get('store_id')},objData);
	
	$('#tab-categories').html('<div class="ajaxloader" align="center"><img class="loader" src="'+cloud_url+'images/dialog-ajax-loader.gif" /></div>');
	
	$.ajax({
		type: "POST",
		url: '/store_product/categories_form',
		data: form_data,
		success: function(data){
			$('#tab-categories').html(data);
			// re-initialize content for the scrollbars (shows only scrollbards depending on the content)
			jSpane_api.reinitialise();
		}
	});
}

function get_productID() {
	return $('.productentry-wrapper').data('product_id');	
}

// options
function _loadproduct_option(objData) {
	$('.productentry-wrapper #tab-options-modal').html('<div class="ajaxloader" align="center"><img class="loader" src="'+cloud_url+'images/dialog-ajax-loader.gif" /></div>');
	$('#tab-options-modal').show();
	form_data = $.extend({token:store.get('token'),user_id:store.get('user_id'),store_id:store.get('store_id'),'product_id':get_productID()},objData);

	$.post('/store_product/load_option', form_data, function(data) {
		$('.productentry-wrapper #tab-options-modal').html(data);

		var jSpane_tab_options = $('#tab-options-modal table .optionvalue span.scrollbar');
		jSpane_tab_options.jScrollPane();
		jSpane_api_tab_options = jSpane_tab_options.data('jsp');
	});
}

// options
function _save_product_option(form) {
	//var form_data = form.serialize();
	form = $('.productentry-wrapper #tab-options-modal form');
	var form_data = form.serialize();
	form_data = 'token=' + store.get('token') + '&user_id=' + store.get('user_id') + '&store_id=' + store.get('store_id') + '&product_id='+ get_productID() + '&' + form_data;

	var option_id = form.find('input[name="option_id"]').val();

	$('#tab-options').html('<div align="center" class="ajaxloader"><img class="loader" src="'+cloud_url+'images/dialog-ajax-loader.gif" /></div>');
	$('#tab-options-modal').hide();
	$.ajax({
		type: "POST",
		url: '/store_product/save_option',
		data: form_data,
		success: function(data){
			$('.productentry-wrapper #tab-options-modal').html('');
			
			// reload tab option menu
			_reload_product_optionlist();
			//container.html(data);
			// re-initialize content for the scrollbars (shows only scrollbards depending on the content)
			//jSpane_api.reinitialise();
		}
	});
}

function _reload_product_optionlist() {
	$('#tab-options').html('<div align="center" class="ajaxloader"><img class="loader" src="'+cloud_url+'images/dialog-ajax-loader.gif" /></div>');
	form_data = {token:store.get('token'),user_id:store.get('user_id'),store_id:store.get('store_id'),product_id:get_productID()}
	//console.log(form_data);
	$.ajax({
		type: "POST",
		url: '/store_product/option_form',
		data: form_data,
		success: function(data){
			$('#tab-options').html(data);
			// re-initialize content for the scrollbars (shows only scrollbards depending on the content)
			jSpane_api.reinitialise();
			$('.loader').remove();
		}
	});
}

function _delete_product_option_value(objData) {
	if(typeof (objData) === 'undefined') {
		return;	
	}
	var form_data = $.extend({token:store.get('token'),user_id:store.get('user_id'),store_id:store.get('store_id'),product_id:get_productID()},objData);
	$.post('/store_product/delete_option_value', form_data, function(data) {
		_reload_product_optionlist();
	});
	
	return false;
}

function _delete_product_option(objData) {
	if(typeof (objData) === 'undefined') {
		return;	
	}
	
	var form_data = $.extend({token:store.get('token'),user_id:store.get('user_id'),store_id:store.get('store_id'),product_id:get_productID()},objData);
	$.post('/store_product/delete_option', form_data, function(data) {
		var _data = $.parseJSON(data);

		if(_data.status == "success") {
			_reload_product_optionlist();
		} else {
			alert("You still have an existing product variants.\nPlease delete it first before you can delete any of the product options.");	
		}
	});
}

function set_product_categories_selected(el) {
	var cat_id = parseInt(el.val());
	var cats = $('.productentry-wrapper').data('product_categories');
	cats = cats.split(',');

	
	if(el.is(':checked')) {
		if($.inArray(cat_id, cats) == -1) {
			//console.log(cats);
			//console.log(cat_id);
			cats.push(cat_id);
			$('.productentry-wrapper').data('product_categories',cats.join(','));
		}
	}
	else {
		if($.inArray(cat_id, cats) == -1) {
			//$('.productentry-wrapper').data('product_categories',cats.join(','));
			cats.splice( $.inArray(cat_id, cats), 1 );
			$('.productentry-wrapper').data('product_categories',cats.join(','));
		}
	}
}

function load_subdepartment(id) {
	$('#sub-department').remove();
	$.ajax({
		type: "POST",
		url: '/store_product/load_subdepartment',
		data: 'token='+store.get('token') + '&user_id=' + store.get('user_id') + '&product_id=' + get_productID() + '&category_id='+id ,
		success: function(data){
			$('.cat-list-wrapper.shopstyxcat').append('<div class="st-catmain-list sub" id="sub-department"></div>');
			$('#sub-department').html(data);
			$('#tab-categories #sub-department').jScrollPane();
		}
	});
}

function save_shipping_settings(type) {
	if(type == 'G') {
		form_data = $('#tab-shipping #shipping_global').find('input, select').serialize();
	}
	else {
		form_data = $('#tab-shipping #shipping_customize').find('input, select').serialize();
	}

	$('#tab-shipping .innercontainer').hide();
	$('#tab-shipping').append('<div class="ajaxloader" align="center"><img class="loader" src="/cdn/images/dialog-ajax-loader.gif" /></div>');
	$.ajax({
		type: "POST",
		url: '/store_product/save_shipping_settings?type='+type,
		data: 'token='+store.get('token') + '&user_id=' + store.get('user_id') + '&product_id=' + get_productID() + '&' + form_data + '&shipping_meta_type=' + type,
		success: function(data){
			/*
			var _data = $.parseJSON(data);
			if(_data.success) {
					
			}
			*/
			reload_tab_shipping();
		}
	});
	
}

function reload_tab_shipping() {
	form_data = {token:store.get('token'),
					user_id:store.get('user_id'),
					product_id:get_productID()
				};
	$.ajax({
		type: "POST",
		url: '/store_product/shipping_form',
		data: form_data,
		success: function(data){
			$('#tab-shipping').html(data);
		}
	});
}

// field focus and blur
$(document.body).delegate('input.decimal', 'focus', function(e) {
	$(this).parent().addClass('focus'); $(this).css('focus','none');
	
    return false;
});
$(document.body).delegate('input.decimal', 'blur', function(e) {
	$(this).parent().removeClass('focus'); 
    return false;
});

$(document).on('keyup', '.productentry-wrapper #product-entry-product_name', function(e) {
	var val = $.trim($(this).val());
	$('#product-entry-seo_title').val(val);
	var seo_url = val.toLowerCase().replace(/[^a-z0-9 -]/g,'').replace(/\s+/g,'-').replace(/-+/g, '-');
	$('#product-entry-seo_url').val(seo_url);
});

// Images

$(document).on('mouseover mouseout', '.thumb-photos-wrapper .thumb-row .thumb', function(e) {
	if(e.type == "mouseover") {
		$(this).find('div').show();
	}
	else {
		$(this).find('div').hide();
		$(this).find('em').not('.defaulted').hide();
		$(this).parent().find('em.defaulted').html('Default Image');
	}

});
$(document).on('mouseover mouseout', '.thumb-photos-wrapper .thumb-row .thumb div span', function(e) {
	if(e.type == "mouseover") {
		$(this).addClass('hover');
		if($(this).hasClass('default')) {
			$(this).parent().parent().find('em').html('Default Image').show();
		}
		else {
			$(this).parent().parent().find('em').html('Delete Image').show();
		}
	}
	else {
		$(this).parent().find('em').not('.defaulted').hide();
		$(this).removeClass('hover');
	}
})
$(document).on('click', '.thumb-photos-wrapper .thumb-row .thumb div span', function(e) {
	e.stopImmediatePropagation();
	var el = $(this);
	var data = $.parseJSON(el.parent().attr('data'));
	var data = {product_id:data.pid,sort_order:data.sort_order};
	if(el.hasClass('default')) {
		set_default_product_photo(data);
		$('.thumb-photos-wrapper .thumb-row .thumb div span.default').removeClass('selected');
		$('.thumb-photos-wrapper .thumb-row .thumb em').removeClass('defaulted').hide();
		el.addClass('selected');
		el.parent().parent().find('em').addClass('defaulted').show();
	}
	else if(el.hasClass('delete'))  {
		delete_product_photo(data)
	}
});


// Options
$(document.body).delegate('.productentry-wrapper #tab-options-modal #close-btn', 'click', function(e) {
	e.stopImmediatePropagation();
	$('#tab-options-modal').hide();
});

$(document.body).delegate('.stockinventory-wrapper input', 'click', function(e) {
    e.stopImmediatePropagation();

	if($(this).attr('id') == 'temp_inventoryQ' ) {
		$('#quantity').removeClass('disabled');
		$('#quantity').removeAttr('disabled');		
		$('#quantity').focus();
	}
	else {
		$('#quantity').addClass('disabled');
		$('#quantity').attr('disabled','disabled');	
	}
	
	if($(this).attr('id') != 'temp_inventoryO' ) {
		$('.variant-qty').hide();
		$('.variant-noqty').show();
	}
	else {
		$('.variant-qty').show();
		$('.variant-noqty').hide();
	}
});

$(document.body).delegate('#tab-options .options-wrapper .option-list-wrapper.clickable', 'click', function(e) {
	e.stopImmediatePropagation();
	
	var form_data = {token:store.get('token'),user_id:store.get('user_id'),store_id:store.get('store_id'),'product_id':get_productID()};
	
	$.post('/store_product/check_product_variants', form_data, function(data) {
		var data = $.parseJSON(data);
		if(data.total_variants > 0) {
			alert("You still have an existing product variants.\nPlease delete it first before you can add a product options.");	
		}
		else {
			_loadproduct_option({product_id:get_productID()});
		}
	});
	
	return false;
});
$(document.body).delegate('#tab-options-modal table td.optionvalue div.opt span', 'click', function(e) {
	e.stopImmediatePropagation();
	$(this).parent().remove();
	jSpane_api_tab_options.reinitialise();
});

$(document.body).delegate('#btn-add-option-value', 'click', function(e) {
	e.stopImmediatePropagation();
	var html = '<div class="opt">\
					<input type="text" value="" name="option_value[]" />\
					<span>delete</span>\
					<span class="clearfix"></span>\
				</div>';
	$('#tab-options-modal table .optionvalue .option-value-content').append(html);
	jSpane_api_tab_options.reinitialise();
});

$(document.body).delegate('#tab-options-modal form', 'submit', function(e) {
	e.preventDefault(); 
	e.stopImmediatePropagation();
	var form = $(this);
	if(get_productID() == 0) {
		var form_data = {token:store.get('token'),user_id:store.get('user_id'),store_id:store.get('store_id')};
		
		$.post('/store_product/generate_product_id', form_data, function(data) {
			var data = $.parseJSON(data);
			$('.productentry-wrapper').data('product_id',data.id);
			_save_product_option(form);
		});
	}
	else {
		_save_product_option(form);
	}
});
// end: options

// Shipping section
$(document.body).delegate('#btn-addnew-variant', 'click', function(e) {
	e.stopImmediatePropagation();
	var inventory_html;
	
	var alt = parseInt($('#tab-options .option-variants-wrapper table tr.results').length)%2;
	alt = alt ? 'alt' : '';	
	
	var td='', s;
	for(var i = 0; i < p_option_values.length; i++) {
		s = '<select name="variant_value_ids" option_id=""><option>-</option>';
		for(a = 0; a < p_option_values[i].values.length; a++) {
			s += '<option value="'+p_option_values[i].values[a].value_id+'">'+p_option_values[i].values[a].value_name+'</option>';
		}
		s += '</select>';
		td += '<td class="option">'+s+'<div style="display:none"></div></td>';	
	}
	
	var html = '<tr class="results '+alt+'">'+td+'\
					<td class="variant_sku"><input type="text" name="variant_sku" /><div style="display:none"></div></td>\
					<td class="variant_price"><input type="text" name="variant_addon_price" class="decimal" /><div style="display:none"></div></td>\
					<td class="variant_qty"><input type="text" name="variant_quantity" class="variant-qty numeric" style="display:none" /><div class="variant-noqty" style="display:none">&Omega;</div></td>\
					<td><span class="editdel"><span class="btn-variant-save" variant_id="">Save</span> &nbsp;|&nbsp;  <span class="btn-variant-delete">Delete</span></span></td>\
				  </tr> ';
	var sss = $('#tab-options .option-variants-wrapper table').append(html);
	jSpane_api_tab_option_variants.reinitialise();
	$("input.numeric").numeric({ decimal: false, negative: false }, function() { this.value = ""; this.focus(); });
	$("input.decimal").numeric({ decimal: ".", negative: true }, function() { this.value = ""; this.focus(); });
	$("input.decimal").floatnumber('.',2);
	
	if($('.stockinventory-wrapper input:checked').attr('id') == "temp_inventoryO") {
		$('.variant-qty').last().show();
		$('.variant-noqty').last().hide();
	}
	else {
		$('.variant-qty').last().hide();
		$('.variant-noqty').last().show();
	}
});

$(document).on('click', '.btn-variant-save', function(e) {
	e.stopImmediatePropagation();
	var thisBtn = $(this);
	var inventory = $('.stockinventory-wrapper input:checked').attr('value');
	var form_data = {'token': store.get('token'), 'user_id': store.get('user_id'), 'store_id': store.get('store_id')};
	form_data.product_id = get_productID();
	variant_id = parseInt(thisBtn.attr('variant_id'));
	variant_id = isNaN(variant_id) ? 0 : variant_id;
	form_data.variant_id = variant_id;
	form_data.sku = thisBtn.parents('tr.results').find('input[name="variant_sku"]').val();
	form_data.addon_price = thisBtn.parents('tr.results').find('input[name="variant_addon_price"]').val();
	form_data.quantity = thisBtn.parents('tr.results').find('input[name="variant_quantity"]').val();
	form_data.variants = {};
	var variants = new Array();
	var sel = thisBtn.parents('tr.results').find('select');
	var sel_length = sel.length;
	for(var i = 0; i < sel_length; i++) {
		form_data.variants[i] = $(sel[i]).find('option:selected').val();	
	}
	
	$.post('/store_product/save_variant', form_data, function(data) {
		var data = $.parseJSON(data);
		if(data.status != false) {
			thisBtn.parents('tr').find('div').show();
			thisBtn.parents('tr').find('select, input').hide();
			thisBtn.parents('tr').find('.btn-variant-save').addClass('btn-variant-edit').removeClass('btn-variant-save').html('Edit');
			
			thisBtn.parents('tr').find('td.option').eq(0).find('div').html(thisBtn.parents('tr').find('td.option').eq(0).find('select option:selected').html());
			thisBtn.parents('tr').find('td.option').eq(1).find('div').html(thisBtn.parents('tr').find('td.option').eq(1).find('select option:selected').html());
			thisBtn.parents('tr').find('td.option').eq(2).find('div').html(thisBtn.parents('tr').find('td.option').eq(2).find('select option:selected').html());
			
			thisBtn.parents('tr').find('td.variant_sku').find('div').html(thisBtn.parents('tr').find('td.variant_sku').find('input').val());
			thisBtn.parents('tr').find('td.variant_price').find('div').html(thisBtn.parents('tr').find('td.variant_price').find('input').val());
			thisBtn.parents('tr').find('td.variant_qty').find('div').html(thisBtn.parents('tr').find('td.variant_qty').find('input').val());
			
			thisBtn.parents('tr').find('.btn-variant-edit').attr('variant_id',data.variant_id);
			thisBtn.parents('tr').find('.btn-variant-delete').attr('variant_id',data.variant_id);
		}
		else {
			alert(data.error);	
		}
	});
});

$(document).on('click', '.btn-variant-edit', function(e) {
	e.stopImmediatePropagation();
	
	$(this).parents('tr').find('div').hide();
	$(this).parents('tr').find('select, input').show();
	$(this).parents('tr').find('.btn-variant-edit').addClass('btn-variant-save').removeClass('btn-variant-edit').html('Save');
});

$(document.body).delegate('.btn-variant-delete', 'click', function(e) {
	e.stopImmediatePropagation();
	
	var thisBtn = $(this);
	var inventory = $('.stockinventory-wrapper input:checked').attr('value');
	var form_data = {'token': store.get('token'), 'user_id': store.get('user_id'), 'store_id': store.get('store_id')};
	form_data.product_id = get_productID();
	variant_id = parseInt(thisBtn.attr('variant_id'));
	variant_id = isNaN(variant_id) ? 0 : variant_id;
	form_data.variant_id = variant_id;
	$.post('/store_product/delete_variant', form_data, function(data) {
		var data = $.parseJSON(data);
		thisBtn.parents('tr').remove();
	});
});

$(document).on('click', '#shipping_global_txt button', function(e) {
	e.stopImmediatePropagation();
	$('#shipping_global_txt').hide();
	$('#tab-shipping .tbl-lbl').show();
	$('#tab-shipping .shippingdest-wrapper').show();
	$('#tab-shipping #shipping_global').show();
	$('#tab-shipping .scrollbars').jScrollPane();
	$('#tab-shipping .tbl-lbl div span').show();
})

$(document).on('click', '#tab-shipping input[name="shipping_meta_type"]', function(e) {
	e.stopImmediatePropagation();
	$('#tab-shipping input[name="shipping_meta_type"]').parent().removeClass('selected');
	$('#tab-shipping input[name="shipping_meta_type"]:checked').parent().addClass('selected');
	
	
	if($(this).val() == "G") {
		var meta = $(this).attr('shipping_meta_global');

		if(meta == "") {
			$('#tab-shipping .tbl-lbl').show();
			$('#tab-shipping .shippingdest-wrapper').show();
			$('#tab-shipping #shipping_global').show();
			$('#tab-shipping #shipping_customize').hide();
			$('#shipping_global_txt').show();
		}
		else {
			$('#tab-shipping .tbl-lbl').hide();
			$('#tab-shipping .shippingdest-wrapper').hide();
			$('#tab-shipping #shipping_global').hide();
			$('#tab-shipping #shipping_customize').hide();
			$('#shipping_global_txt').show();
		}
	}
	else {
		var meta = $(this).attr('shipping_meta_customize');
		$('#shipping_global_txt').hide();
		$('#tab-shipping #shipping_global').hide();
		$('#tab-shipping .tbl-lbl').show();
		$('#tab-shipping .shippingdest-wrapper').show();
		$('#tab-shipping #shipping_customize').show()
		$('#tab-shipping .tbl-lbl div span').hide();
	}
	
	$('#tab-shipping .scrollbars').jScrollPane();
});

$(document).on('click', '#tab-shipping tr.addbtn td button', function(e) {
	e.stopImmediatePropagation();
	var index = parseInt($(this).parents('table').find('tr').length) - 2;
	var countries = $('#tmp_shipping_countries').html();
	var stype = '', carrier = '';
	var data = $.parseJSON($('#productentry-shipfromcountry').attr('data'));
	
	if($('#productentry-shipfromcountry').attr('country_code') == 'US') {
		//stype = '<option value="0">Calculated</option>';
	}
	
	if(data.premium_type > 0) {
		carrier = '<select name="shipping_global_meta[][carrier]"> \
								<option value="">Select carrier</option> \
								<option value="UPS">UPS</option> \
								<option value="USPS">USPS</option> \
								<option value="FedEx">FedEx</option> \
							</select>';
		stype = '<option value="0">Calculated</option>';
	}
	else {
		carrier = '';
	}
	
	var html = '<tr class="sr"> \
					<td width="185" class="country"> \
						<div> \
						'+countries+' \
						</div> \
					</td> \
					<td width="180" class="rates"> \
						<div class="borderright"> \
						<select class="rates" name="shipping_global_meta[][shipping_type]"> \
							<option value="">Select shipping rates</option> \
							'+ stype +' \
							<option value="1">Flat rate</option> \
							<option value="2">Free Shipping</option> \
						</select> \
						<div class="freeshipping" style="display:none;">$0.00 = Free Shipping</div> \
						</div> \
					</td> \
					<td class="types"> \
						<div class="main" style="display:none;"> \
							'+ carrier + '\
							<div class="shipping_types"> \
								<div class="standard"> \
									<input type="checkbox" name="shipping_global_meta[][carrier][options][standard]" /> \
									<span>Standard</span> \
									<div class="price disabled"> \
										<span>$</span> \
										<input type="text" value="0.00" disabled="disabled" name="shipping_global_meta[][rate][standard]" /> \
									</div> \
								</div> \
								<div class="express"> \
									<input type="checkbox" name="shipping_global_meta[][carrier][options][express]" /> \
									<span>Express</span> \
									<div class="price disabled"> \
										<span>$</span> \
										<input type="text" value="0.00" disabled="disabled" name="shipping_global_meta[][rate][express]" /> \
									</div> \
								</div> \
								<div class="nextday"> \
									<input type="checkbox" name="shipping_global_meta[][carrier][options][next_day]" /> \
									<span>Next Day</span> \
									<div class="price nomarginright disabled"> \
										<span>$</span> \
										<input type="text" value="0.00" disabled="disabled" name="shipping_global_meta[][rate][standard]" /> \
									</div> \
								</div> \
							</div> \
							<div class="freeshipping_types" style="display:none;">\
								<select name="shipping_global_meta[][freeshipping_option]" style="display:none;">\
									<option value="">Select service levels</option>\
									<option value="standard">Standard</option>\
									<option value="express">Express</option>\
									<option value="next_day">Next Day</option>\
								</select>\
							</div>\
							<span class="clearfix"></span> \
						</div> \
						<a></a> \
					</td> \
				</tr>';
	html = html.replace(/[\r\n]/g, "");	
	if($(this).parents('table').find('tr').length == 1) {
		$(this).parents('table').prepend(html);
	}
	else {
		$($(this).parents('table').find('tr')[index]).after(html);		
	}
	$('#tab-shipping .scrollbars').jScrollPane();
});

$(document).on('change', '#tab-shipping td.country select', function(e) {
	e.stopImmediatePropagation();
	
	var code = $(this).val();
	var row = $(this).parents('tr');
	
	row.find('td.rates select').attr('name','shipping_global_meta['+code+'][shipping_type]');
	row.find('td.rates select').attr('country_code',code);
	row.find('td.types select').attr('name','shipping_global_meta['+code+'][carrier]');
	row.find('td.types div.standard input[type="checkbox"]').attr('name','shipping_global_meta['+code+'][options][standard]');
	row.find('td.types div.standard input[type="text"]').attr('name','shipping_global_meta['+code+'][rate][standard]');
	row.find('td.types div.express input[type="checkbox"]').attr('name','shipping_global_meta['+code+'][options][express]');
	row.find('td.types div.express input[type="text"]').attr('name','shipping_global_meta['+code+'][rate][express]');
	row.find('td.types div.nextday input[type="checkbox"]').attr('name','shipping_global_meta['+code+'][options][next_day]');
	row.find('td.types div.nextday input[type="text"]').attr('name','shipping_global_meta['+code+'][rate][next_day]');
	row.find('td.types div.freeshipping_types select').attr('name','shipping_global_meta['+code+'][freeshipping_option]');
	
	row.find('td.types a').attr('code',code);
	
	if($('#productentry-shipfromcountry').attr('country_code') == code) {
		row.find('td.types div.nextday input[type="checkbox"]').removeAttr('disabled');
	}
	else {
		row.find('td.types div.nextday input[type="checkbox"]').attr('disabled','disabled');
		row.find('td.types div.nextday input[type="checkbox"]').removeAttr('checked');
	}
});

$(document).on('change', '#tab-shipping td.rates select', function(e) {
	e.stopImmediatePropagation();
	var row = $(this).parents('tr');
	
	row.find('.main').show();
	row.find('.price').hide();
	row.find('.types select').hide();
	row.find('.freeshipping').hide();
	row.find('.freeshipping_types').hide();
	row.find('.shipping_types').show();
	row.find('td.types input[type="checkbox"]').removeAttr('disabled');
	
	var shiptocountry = $(this).attr('country_code');
	
	if($(this).val() == 0 && $(this).val() != '') {
		row.find('.shipping_types').css('margin-left','23px');
		row.find('.types select').show();
		row.find('.freeshipping_types select').hide();
	}
	else if($(this).val() == 1) {
		row.find('.shipping_types').css('margin-left',0);
		row.find('.price').show();
	}
	else {
		row.find('.shipping_types').hide();
		row.find('.freeshipping_types').show();
		row.find('.freeshipping_types select').show();
		row.find('.freeshipping').show();
	}
	
	if($(this).val() == 0) {
		if($('#productentry-shipfromcountry').attr('country_code') == shiptocountry) {
			console.log(row.find('td.types div.nextday input[type="checkbox"]'));
			row.find('td.types div.nextday input[type="checkbox"]').removeAttr('disabled');
		}
		else {
			row.find('td.types div.standard input[type="checkbox"]').attr('disabled','disabled');
			row.find('td.types div.standard input[type="checkbox"]').removeAttr('checked');
			row.find('td.types div.nextday input[type="checkbox"]').attr('disabled','disabled');
			row.find('td.types div.nextday input[type="checkbox"]').removeAttr('checked');
		}
	}
	
	$('#tab-shipping .scrollbars').jScrollPane();
	
	/*
	var country = $(this).attr('country_code');
	if(country == 'US') {
		row.find('td.types .nextday input[type="checkbox"]').removeAttr('disabled');
	}
	else {
		if($(this).val() == 0) {
			row.find('td.types .nextday input[type="checkbox"]').attr('disabled','disabled');
		}
		else {
			row.find('td.types .nextday input[type="checkbox"]').removeAttr('disabled');
		}
	}
	*/
	

});

$(document).on('change', '#tab-shipping td.types input[type="checkbox"]', function(e) {
	e.stopImmediatePropagation();
	var row = $(this).parent();
	
	if($(this).attr('checked') == "checked") {
		row.find('.price').removeClass('disabled');
		row.find('.price input').removeAttr('disabled');
	}
	else {
		row.find('.price').addClass('disabled');
		row.find('.price input').attr('disabled','disabled');
	}
});

$(document).on('click', '#tab-shipping table.shipping tr.sr td.types a', function(e) {
	e.stopImmediatePropagation();
	var row = $(this).parents('tr');
	var code = $(this).attr('code');
	
	$('#tab-shipping select.country option[value="'+code+'"]').show();
	row.remove();
});

$(document).on('click', '#tab-shipping .tbl-lbl div.lbl2 button', function(e) {
	e.stopImmediatePropagation();
	
	var meta_type = $('input[name="shipping_meta_type"]:checked').val();
	meta_type = (meta_type == "G") ? "G" : "C";
	save_shipping_settings(meta_type);
});

// SEO
$(document).on('keyup', '.productentry-wrapper #product-entry-seo_url', function(e) {
	var val = $.trim($(this).val());
	var seo_url = val.toLowerCase().replace(/[^a-z0-9 -]/g,'').replace(/\s+/g,'-').replace(/-+/g, '-');
	$('#product-entry-seo_url').val(seo_url);
});

$(document).on('blur', '.productentry-wrapper #product-entry-seo_url', function(e) {
	var val = $.trim($(this).val());
	var seo_url = val.toLowerCase().replace(/[^a-z0-9 -]/g,'').replace(/\s+/g,'-').replace(/-+/g, '-');

	if (seo_url.charAt(seo_url.length - 1) == '-') {
		seo_url = seo_url.substr(0, seo_url.length - 1);
	}
	$('#product-entry-seo_url').val(seo_url);
});



	


	
