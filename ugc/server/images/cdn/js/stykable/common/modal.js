$.extend({
	parseJSON: function( data ) {
		if ( typeof data !== "string" || !data ) {
			return null;
		}
		data = jQuery.trim( data );
		if ( /^[\],:{}\s]*$/.test(data.replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g, "@")
			.replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, "]")
			.replace(/(?:^|:|,)(?:\s*\[)+/g, "")) ) {
			return window.JSON && window.JSON.parse ?
			window.JSON.parse( data ) :
			(new Function("return " + data))();
		}
	}
});
	
var msPopupId = '#popup', 
	msDialogId = '#dialog-box', 
	getAjaxCache = {},
	modalbox,
	vid = CS.vid,
	cloud_url = CS.cloudURL || location.protocol + '//' + location.host + '/';
	main_url = CS.siteURL || location.protocol + '//' + location.host + '/';
	root = CS.siteURLMain || location.protocol + '//' + location.host + '/';
	store_uri = CS.storeURI;
	
typeof jQuery !== "undefined" && jQuery( function( )
{

	modalbox = new ModalBox(); 
	
	
	$.getScript(cloud_url+'js/stykable/common/function/modalFunction.js', 
	function(data, textStatus){
	
		$(document.body).on('touchstart click', '#login', function(){ LoginDialog();});
		
		$(document.body).delegate('#loginform', 'submit', function(event){ event.preventDefault(); LoginForm($(this));});
		
		$(document.body).delegate('#logout', 'click', function(){ Logout();});
		
		$(document.body).delegate('.btn_cancel', 'click', function(){ modalbox.close(); });
		
		$(document.body).delegate('#register', 'click', function(){ RegisterDialog();});
		
		$(document.body).delegate('#lostpassword', 'click', function(){ LostPasswordDialog();});
		
		$(document.body).delegate('#changepassword', 'click', function(){ ChangePasswordDialog();});
		
		$(document.body).delegate('#registerform', 'submit', function(event){ event.preventDefault(); RegisterForm($(this));});
		
		$(document.body).delegate('.btn_cancel', 'click', function(){ modalbox.close(); });
		
		$(document.body).delegate('#forgotpasswordform', 'submit', function(event){ event.preventDefault(); ForgotPasswordForm($(this));});
		
		$(document.body).delegate('#changepasswordform', 'submit', function(event){ event.preventDefault(); ChangePasswordForm($(this));});
		
		$(document.body).delegate('#registerbtn', 'click', function(){
			if($('#display_name').val() == 'Display Name'){
				$('#display_name').val('');
			}
			if($('#email').val() == 'Your email'){
				$('#email').val('');
			}
			if($('#password').val() == 'Password'){
				$('#password').val('');
			}
			$('.error2').html('');
			$(this).parents("form").trigger("submit");
			return false;
		});
	});


function ModalBox(settings){
		
		_self = this;
		this.dialogbox;
		this.messagebox;
		this.loadingImg = cloud_url + 'images/dialog-ajax-loader.gif';
		this.settings = {};
		this.params = {};
		this.getCache = {};
		this.defSettings = {width:400, height:'auto', resizable:false, show:'fade', modal:true, closeOnEscape:true, position:['center','center'], draggable:true,
			open: function (event, ui) { window.setTimeout(function () {
				jQuery(document).unbind('mousedown.dialog-overlay').unbind('mouseup.dialog-overlay'); }, 100);
			}
		
		};
		this.defparams = {html:'', title:'', hideCloseBtn:false, boxType:'normal', message:'', cache:true, displayGetContent:true, buttonLabel:'none'};
		
		this.close = function(){
			$(_self.dialogbox).html('').remove();
			return this;
		}
		
		function toggleCloseBtn(hide){
			if(hide)
				$('.ui-dialog-titlebar-close').hide();
			else
				$('.ui-dialog-titlebar-close').show();
		}
		
		this.dialog = function(params, settings){
			
			_self.close();
			
			var html = '', message, boxType, hideCloseBtn, buttonLabel;
			settings = settings || {};
			params = params || {};
			
			_self.params = $.extend({}, _self.defparams, params);
			_self.settings = $.extend({}, _self.defSettings, settings, {title:_self.params['title']});
			
			boxType = _self.params['boxType'];
			message = _self.params['message'];
			html = _self.params['html'];
			hideCloseBtn = _self.params['hideCloseBtn'];
			buttonLabel = _self.params['buttonLabel'];
	
			if(boxType != 'normal' && message != '')
				$(_self.messagebox).removeClass().addClass('sbMessage ' + boxType).html('<span class="sbStatIco"></span>'+message).appendTo(_self.dialogbox);
			
			$(_self.dialogbox).append('<div id="dialogbox-html">'+html+'</div>').appendTo('body').dialog(_self.settings);
			
			if(buttonLabel != 'none')
				$(_self.dialogbox).append('<div><input class="dialog-green-btn" id="modalbox-button" type="button" value="'+buttonLabel+'"></div>');
			
			if(hideCloseBtn == true)
				toggleCloseBtn(hideCloseBtn);		
			
			$('.ui-dialog-titlebar-close, #modalbox-button').bind('click', _self.close);
			
			return _self;
		}
		
		this.loading = function(params, settings){
			settings = $.extend({},{closeOnEscape:false},settings || {});
			params = $.extend({},{hideCloseBtn:true},params || {})
			params['html'] = '<div id="dialogbox-html"><div style="width: 100%; text-align: center;"><img class="img-loader"  src="' + _self.loadingImg +'" /></div></div>';
			
			_self.dialog(params, settings);
			
			return _self;
		}
		
		this.done = function(html, title){
			html = html || '';
			if(title != 'undefined')
				$('.ui-dialog-title').html(title);
				
			$('#dialogbox-html').html(html);
			toggleCloseBtn(false);
			
			return _self;
		}
		
		this.get = function(url, params, settings, callback, customParam){
			settings = settings || {};
			params = params || {};
			customParam = customParam || {}
			
				_self.loading(params, settings);
				$.get(url, function(data){
					
					
					if(_self.params['displayGetContent'] == true)	
						_self.done(data);
	
					if(typeof callback == 'string' && callback != '')
						eval(callback+'(data, customParam)');
					else if(typeof callback == 'function')
						callback(data, customParam);
						 
				});
			
			return _self;
		}
		
		function init(){
			settings = settings || {};
			_self.settings = $.extend({}, _self.defSettings, settings);		
			_self.dialogbox = $('<div id="dialog-box"></div>');
			_self.messagebox = $('<div id="sbMessage"></div>');
			$(document).ready(function(){
				$('<img class="img-loader" src="'+_self.loadingImg+'">').appendTo('body').hide();
			});
			return _self;	
		}
	
		return init();
	}

modalbox = new ModalBox(); 		
});

function csConfirm( title, question, callback, init, on_before_close, settings )
{
	var modal = typeof modalbox !== "undefined" ? modalbox : new ModalBox( );
	
	modal.dialog
	( {
		title:	title,
		html:	''
	}, settings || {
		width:	350
	} );
	
	var buttons = callback || {
		"Ok":	null,
		"No":	null
	};
	modal.dialogbox.append( question );
	$( '<br />' ).css( {clear: 'both', display: 'block'} ).appendTo( modal.dialogbox );
	var actions = $( '<p>' ).addClass( 'opt-btn-wrapper' ).appendTo( modal.dialogbox );
	
	$.each( buttons, function( x, y )
	{
		var button = $( '<input>' )
			.attr( {
				type:		'button',
				value:		x
			} )
			.addClass( 'opt-btn' )
			.css( {marginBottom: '10px', marginTop: '10px', marginRight: '10px'} );
		
		button.appendTo( actions );
	} );
	
	actions.find( 'input[type=button]' ).click( function( e )
	{
		if( typeof on_before_close == 'function' )
			if( on_before_close( ) === false )
				return;
		modal.close( );
		var response = $( this ).val( ),
			call = typeof callback[ response ] ? callback[ response ] : new Function( );

		call( );
	} );
	
	typeof init == 'function' && init( modal, modal.dialogbox );
};

function Loader(){
	preLoader($('#user_status'));
	
	// Load cart items
	var form_data = {user_id:store.get('user_id'),
					token:store.get('token'),
					cart_sess: store.get('cart_sess'), 
					cart: get_cart_data(),
					storeURI:CS.storeURI};
	$.ajax({
		type: "POST",
		url: main_url + "ajax.html?content=cartitems",
		data: form_data,
		crossDomain: true,
		cache: false,
		success: function(response){
			var _data = $.parseJSON(response);
			var total_item = parseInt(_data.total_item);
			var total_price = parseFloat(_data.total_price);
			
			if(total_item > 1) {
				total_item += " items";	
			}
			else if(total_item == 1) {
				total_item += " item";	
			}
			else {
				total_item = "no item";	
			}

			$('#shopping-text').show();
			$('#shopping-count').html(total_item + ' - $' + number_format(total_price, 2, '.', ','));
			$('#shopping-count').show();
			$('#cart_items').find('#preloader-wrapper').hide();
		}
	})
	
	var user_id = store.get('user_id');
	if(typeof user_id !== 'undefined'){
		var token = store.get('token');
		var form_data = {
			postdata: 1,
			token: token,
			user_id: user_id
		};
		$.post(root + "checkuser.html",form_data,function(data){
			var _data = $.parseJSON(data);
			if( _data.result ){
				store.set('display_name', _data.display_name);
				store.set('user_id', _data.user_id);
				store.set('user_login_email', _data.user_login_email);
				
				$('#e-email').val(_data.user_login_email);
				$('#e-email').attr('disabled', 'disabled');
				$('#user_status').find('#preloader-wrapper').hide();
				$('#account-name').html(store.get('display_name'));
				$('.login-logout').html('Sign Out');
				$('.login-logout').removeAttr('id');
				$('.login-logout').attr('id', 'logout');
				$('.login-logout').addClass('login-logout-hover');
				$('.ms-loggedin').addClass('hide');
				$('.ms-notloggedin').removeClass('hide');
				
				if(_data.has_store == 0){ $('.u-shop').remove(); $('.u-sales').remove(); }
				if(_data.has_store_products == 0){ $('.u-catalog').remove(); }
				if(_data.has_styx == 0){ $('.u-styx').remove(); }
				if(_data.userType == 'P'){ $('.earn-or-sale').html('Earnings') }
				if(_data.userType == 'V'){ $('.earn-or-sale').html('Sales') }
				if(_data.user_picture != ""){
					$('#account-icon-image').css('background-image', 'url('+cloud_url+'uploads/users_picture/' + encodeURIComponent(_data.user_picture) + ')'); 
					$('#account-icon-image').css('background-size', 'cover'); 
				}
			}
			else {
				$('#user_status').find('#preloader-wrapper').hide();
			}
		});
	}else{
		$('#user_status').find('#preloader-wrapper').hide();
		$('#account-name').html('Your Account');
		$('.login-logout').html('Sign In Here');
		$('.login-logout').removeAttr('id');
		$('.login-logout').attr('id', 'signin');
		$('.login-logout').removeClass('login-logout-hover');
		$('.ms-loggedin').removeClass('hide');
		$('.ms-notloggedin').addClass('hide');
	}
	// preLoader($('#cart_items'));
}


var last_stamp = store.get('session_date');
var new_timestamp = new Date().getTime();

if(( typeof store.get('token') !== 'undefined' || store.get('token') != '' ) && (last_stamp !== 'undefined' || last_stamp != '')  && store.get('remember') == 0 ){
	if (new_timestamp - last_stamp > 120000) {
		autoLogout();
	}
}

if(( typeof store.get('token') !== 'undefined' || store.get('token') != '' ) && (last_stamp !== 'undefined' || last_stamp != '')  && store.get('remember') == 1 ){
	if (new_timestamp - last_stamp > 604800000) {
		autoLogout();
	}
}

var timestamp = new Date().getTime();
store.set('session_date', timestamp);

setInterval(function() {
	if( ( typeof store.get('token') !== 'undefined' || store.get('token') != '' ) && ( store.get('remember') == 0 || store.get('remember') == 1 ) ){
			 var timestamp = new Date().getTime();
			 store.set('session_date', timestamp);
	}
	 
}, 6000);

function autoLogout(){
	var url = root + 'signout.html';
	var form_data = {};
	form_data.post_dta = 1;
	form_data.temp_sessionid = store.get('temp_sessionid');
	form_data.token = store.get('token');
		
	$.ajax({
		type: "POST",
		url: url,
		data: form_data,
		cache: false,
		success: function(data){
			if(data == 'success'){
				store.clear();
				location.href = location.href;
			}
		}
	});
	return false;
}

function close_modal(modal_id){
	$("#store_modal").fadeOut(200);
	modal_id.css({"display":"none"})
}

function loadLoader(id_or_class){
	$('.loading_image').trigger('click');
	//$(id_or_class).html('<div id="dialog-box"><div style="width: 100%; text-align: center; margin: 80px 0;"><img class="img-loader" src="' + cloud_url + 'images/dialog-ajax-loader.gif" /></div></div>');
}

function closeLoader(){
	close_modal($('.store_loader'));
	$('#store_modal_loader').removeAttr('style');
}

function preLoader(mcontainer) {
	var w = parseInt(mcontainer.width());
	var h = parseInt(mcontainer.height());
	
	mcontainer.find('#preloader-wrapper').css({width:w,height:h});
	mcontainer.find('#preloader-wrapper').show();
	var t = (h-parseInt(mcontainer.find('#preloader-center').height())) / 2;
	mcontainer.find('#preloader-center').css({'top':t});
	return;
}

function mainpreLoader(mcontainer) {
	// Centerize
	var dWidth = parseInt($(document).width());
	var dHeight = parseInt($(document).height());
	$('#mainpreloader-wrapper').css({width:dWidth, height:dHeight});
	
	var sWidth = $(window).width(); 
	var sHeight = $(window).height(); 
	var left = $(document).scrollLeft();
	var top = $(document).scrollTop();
	
	var modal = $('#mainpreloader-wrapper').find('#mainpreloader-center');
	var top = ((sHeight - modal.height()) / 2) + (top-50);
	//var top = ((sHeight - 131) / 2);
	//var top = 200;
	var left = (sWidth - modal.width()) / 2;
	modal.css({top:top});
	$('#mainpreloader-wrapper').show();
	
	return;
}