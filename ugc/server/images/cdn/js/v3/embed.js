/*
	Developed by Robert Nyman, http://www.robertnyman.com
	Code/licensing: http://code.google.com/p/getelementsbyclassname/
*/	
var getElementsByClassName=function(e,t,n){if(document.getElementsByClassName){getElementsByClassName=function(e,t,n){n=n||document;var r=n.getElementsByClassName(e),i=t?new RegExp("\\b"+t+"\\b","i"):null,s=[],o;for(var u=0,a=r.length;u<a;u+=1){o=r[u];if(!i||i.test(o.nodeName)){s.push(o)}}return s}}else if(document.evaluate){getElementsByClassName=function(e,t,n){t=t||"*";n=n||document;var r=e.split(" "),i="",s="http://www.w3.org/1999/xhtml",o=document.documentElement.namespaceURI===s?s:null,u=[],a,f;for(var l=0,c=r.length;l<c;l+=1){i+="[contains(concat(' ', @class, ' '), ' "+r[l]+" ')]"}try{a=document.evaluate(".//"+t+i,n,o,0,null)}catch(h){a=document.evaluate(".//"+t+i,n,null,0,null)}while(f=a.iterateNext()){u.push(f)}return u}}else{getElementsByClassName=function(e,t,n){t=t||"*";n=n||document;var r=e.split(" "),i=[],s=t==="*"&&n.all?n.all:n.getElementsByTagName(t),o,u=[],a;for(var f=0,l=r.length;f<l;f+=1){i.push(new RegExp("(^|\\s)"+r[f]+"(\\s|$)"))}for(var c=0,h=s.length;c<h;c+=1){o=s[c];a=false;for(var p=0,d=i.length;p<d;p+=1){a=i[p].test(o.className);if(!a){break}}if(a){u.push(o)}}return u}}return getElementsByClassName(e,t,n)};
/*!
 * contentloaded.js
 *
 * Author: Diego Perini (diego.perini at gmail.com)
 * Summary: cross-browser wrapper for DOMContentLoaded
 * Updated: 20101020
 * License: MIT
 * Version: 1.2
 *
 * URL:
 * http://javascript.nwbox.com/ContentLoaded/
 * http://javascript.nwbox.com/ContentLoaded/MIT-LICENSE
 *
 */
function Shopstyx_contentLoaded(e,t){var n=false,r=true,i=e.document,s=i.documentElement,o=i.addEventListener,u=o?"addEventListener":"attachEvent",a=o?"removeEventListener":"detachEvent",f=o?"":"on",l=function(r){if(r.type=="readystatechange"&&i.readyState!="complete")return;(r.type=="load"?e:i)[a](f+r.type,l,false);if(!n&&(n=true))t.call(e,r.type||r)},c=function(){try{s.doScroll("left")}catch(e){setTimeout(c,50);return}l("poll")};if(i.readyState=="complete")t.call(e,"lazy");else{if(!o&&s.doScroll){try{r=!e.frameElement}catch(h){}if(r)c()}i[u](f+"DOMContentLoaded",l,false);i[u](f+"readystatechange",l,false);e[u](f+"load",l,false)}};

Shopstyx_contentLoaded(window,function() {
	var widget_link, iframe, i, widget_links, hostname = "shopstyx.com";
    widget_links = getElementsByClassName('data-shopstyx-collection');
    for (i = 0; i < widget_links.length; i++) {
      widget_link = widget_links[i];
      iframe = document.createElement('iframe');
      iframe.setAttribute('src', '//'+hostname+'/embed?u=' + escape(widget_link.getAttribute('data-url')));
      iframe.setAttribute('width', widget_link.getAttribute('data-width'));
      iframe.setAttribute('height', widget_link.getAttribute('data-height'));
      iframe.setAttribute('frameborder', '0');
      iframe.setAttribute('scrolling', 'no');
      iframe.setAttribute('title', 'Shopstyx Embed Collection');
      widget_link.parentNode.replaceChild(iframe, widget_link);
    }

    widget_links = getElementsByClassName('data-shopstyx-product');
    for (i = 0; i < widget_links.length; i++) {
      widget_link = widget_links[i];
      iframe = document.createElement('iframe');
      iframe.setAttribute('src', '//'+hostname+'/embed?u=' + escape(widget_link.getAttribute('data-url')));
      iframe.setAttribute('width', widget_link.getAttribute('data-width'));
      iframe.setAttribute('height', widget_link.getAttribute('data-height'));
      iframe.setAttribute('frameborder', '0');
      iframe.setAttribute('scrolling', 'no');
      iframe.setAttribute('title', 'Shopstyx Embed Product');
      widget_link.parentNode.replaceChild(iframe, widget_link);
    }
})