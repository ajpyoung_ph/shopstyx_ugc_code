function pp(user_id,token,endpoint){
	this.token = token;
	this.user_id = user_id;
	this.resultstring = "";
	this.statuscode = "";
	this.pp_key = "";
	this.pp_endpoint = endpoint; //Endpoint = https://api.paypal.com for live, https://api.sandbox.paypal.com for sandbox
	this.pp_resultstring = "";
	
	// we shall make a request
	$.ajax({
				type: "POST",
				url: "https://shopstyx.com/api/v1/pp/get_token", 
				data: { user_id: user_id, token: token }, 
				dataType: "json",
				crossDomain: true,
				async: false,
				success: function(data){
					pp_key = data.results.key;
				}
			});
	
	this.pp_post = function(url,mydata){
		$.ajax({
			   		type: "POST",
					url: this.pp_endpoint+url,
					headers: {'Authorization':'Bearer '+pp_key,'Accept':'application/json','Content-Type':'application/json'},
					async: false,
				    crossDomain: true,
					dataType: 'json',
					data: mydata,
					success: function(data){
						pp_resultstring = data;
					}
			   });
	}
	
	this.pp_get = function(url){
		$.ajax({
			   		type: "GET",
					url: this.pp_endpoint+url,
					headers: {'Authorization':'Bearer '+pp_key,'Accept':'application/json','Content-Type':'application/json'},
					async: false,
				    crossDomain: true,
					dataType: 'json',
					success: function(data){
						pp_resultstring = data;
					}
			   });
	}
	
	this.pp_get_key = function (){
		return pp_key;
	}
	
	this.pp_make_payment = function(mydata){
		url = "/v1/payments/payment";
		this.pp_post(url,mydata);
	}
	
	this.pp_save_card = function(payer_id,number,type,expire_month,expire_year,first_name,last_name){
		mydata = {
			payer_id: payer_id,
			number: number,
			type: type,
			expire_month: expire_month,
			expire_year: expire_year,
			first_name: first_name,
			last_name: last_name
		}
		url = "/v1/vault/credit-card";
		this.pp_post(url,mydata);
	}
	
	this.pp_get_result = function(){
		return pp_resultstring;
	}
	
	this.pp_get_card = function(cardID){
		url = "/v1/vault/credit-card/"+cardID;
		this.pp_get(url);
	}
}