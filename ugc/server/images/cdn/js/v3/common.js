var vid = CS.vid,
	cloud_url = CS.cloudURL || location.protocol + '//' + location.host + '/',
	root = CS.siteURL || location.protocol + '//' + location.host + '/',
	main_url = CS.main_url,
	current_url = location.href,
	loadMPhone = true,
	trending_load_status = false;

function getScripts(js) {
	if(typeof js == "object" && js.length > 0){
		for(var i = 0; i < js.length; i++){
			$.getScript(js[i]);
		}
	}
	return true;
}

var SSAjax = function( options ) {
		var form_data = {
				token: store.get('token'),
				user_id: store.get('user_id'),
				_Ssfp_ID: store.get('_Ssfp_ID')
			};

        // Default options.
        var settings = $.extend(true,{
            // These are the defaults.
			loadCSS: [],
            type: "POST",
            data: form_data,
            url: "",
            success: function(response) {
            	//console.log(response);
            },
            loadJS: []
            
        }, options );

        // loading external css and js
        var jsLib = "", cssLib = "", loadJS = new Array(), loadCSS = new Array();
        if(settings.hasOwnProperty('loadJS') && settings.loadJS.length > 0) {

        	if(typeof $('html').data('loadJS') != "object") {
        		$('html').data('loadJS',[]);
        	}

    		for(var i = 0; i < settings.loadJS.length; i++) {
    			if($.inArray(settings.loadJS[i], $('html').data('loadJS')) == -1) {
    				loadJS.push(settings.loadJS[i]);
					jsLib += ",$.getScript('"+settings.loadJS[i]+"')";
    			}
    		}
        }

        if(settings.hasOwnProperty('loadCSS') && settings.loadCSS.length > 0) {
        	if(typeof $('html').data('loadCSS') != "object") {
        		$('html').data('loadCSS',[]);
        	}

        	for(var i = 0; i < settings.loadCSS.length; i++) {
    			if($.inArray(settings.loadCSS[i], $('html').data('loadCSS')) == -1) {
    				loadCSS.push(settings.loadCSS[i]);
    			}
    		}
			/*
    		if(loadCSS.length > 0) {	
    			for(var i = 0; i < loadCSS.length; i++) {
					$.get(loadCSS[i], function(contents){
						$("<style type=\"text/css\">" + contents + "</style>").appendTo(document.head);
						// $('<link>')
							// .appendTo($('head'))
							// .attr({type : 'text/css', rel : 'stylesheet'})
							// .attr('href', loadCSS[i]);
					});
				}
			}
			*/
        }
       	
		var success = settings.success;
		settings.success = function(response, textStatus, xmlHttp) {
			
			if(response == "" || response == false) {
				// invalid toke and/or user_id, so logout
				//return;
			}
			success(response);	 
			//console.log(xmlHttp.getAllResponseHeaders())  
			//console.log(xmlHttp.getResponseHeader("Date")) 		
		};

		var token = store.get('token'), 
			last_stamp = store.get('session_date'),
			remember = store.get('remember');

		// check here if logged In session has already been expired
		var session = check_login_session({
							"token": token,
							"last_stamp": last_stamp,
							"remember": remember
						});

		// if session is expired, make a pop-up here
		if(!session) {
			//alert("Your session has been expired due to inactivity!");
			//autoLogout();
			//return;
		}


		var login_data = {post_data:1,
							token:token,
							user_id:settings.data.user_id,
							shopstyx_utz:store.get('shopstyx-utz'),
						};

		if(typeof store.get('_Ssfp_ID') == "undefined" && (typeof token == "undefined" || typeof settings.data.user_id == "undefined")) {
			login_data._Ssfp = $.fingerprint();
		}
		else {
			login_data._Ssfp_ID = store.get('_Ssfp_ID');
		}

		// check here if user is logged In
		$.ajax({url:"/checkuser.html",
				type:'post',
				data: login_data,
				success: function(data){
						store.remove('shopstyx-utz');

						var _data = $.parseJSON(data);
						if(typeof _data._Ssfp_ID != "undefined" && !$.isEmptyObject(_data._Ssfp_ID) && _data._Ssfp_ID != 0) {
							store.set('_Ssfp_ID',_data._Ssfp_ID);
							settings.data._Ssfp_ID = _data._Ssfp_ID;
						}

						if( _data.result){
							store.set('display_name', _data.display_name);
							store.set('user_id', _data.user_id);

				        	if(loadJS.length > 0 || loadCSS.length > 0) {
								if(loadCSS.length > 0) {
									$.get(loadCSS[0], function(contents){
										$("<style type=\"text/css\">" + contents + "</style>").appendTo(document.head);
										$('html').data('loadCSS',$.merge($('html').data('loadCSS'),loadCSS));
										
										if(loadJS.length > 0) {
							        		$.when(eval(jsLib.substr(1)))//getScripts(loadJS))
							        		 .then(
							        		 	function() {
								        		 	$('html').data('loadJS',$.merge($('html').data('loadJS'),loadJS));
													// load the specific page
													$.ajax(settings);
							        		 	},
							        		 	function() {
							        		 		console.log('Unable to load the js external files. ' + settings.loadJS);
							        		 	}
							        		 );
										}
										else {
											setTimeout(function(){$.ajax(settings)},100);
										}
									});
								}
								else {
									$.when(eval(jsLib.substr(1)))//getScripts(loadJS))
									 .then(
										function() {
											$('html').data('loadJS',$.merge($('html').data('loadJS'),loadJS));
											// load the specific page
											$.ajax(settings);
										},
										function() {
											console.log('Unable to load the js external files. ' + settings.loadJS);
										}
									 );
								}
							}
							else {
								//console.log(settings);
								$.ajax(settings);
							}
						}
						else {
							store.remove('token');
							store.remove('user_id');
							store.remove('display_name');
							store.remove('remember');
							//return;
							// redirect user if not logged in
							//alert("Sorry! You are not logged in.");
							//location.href = "/";
							$.ajax(settings);
						}
				}
			});		
	};


var check_login_session = function(options) {
		var new_timestamp = new Date().getTime();
		// milliseconds
		var minutes_to_expire = 120000; // 2 minutes 

		if(( typeof options.token !== 'undefined' || options.token != '' ) 
		&& (options.last_stamp !== 'undefined' || options.last_stamp != '')  
		&& options.remember == 0 ){
			minutes_to_expire = 600000; // 10 minutes
			if ((new_timestamp - options.last_stamp) > 120000) {
				return false;
			}
		}

		if(( typeof options.token !== 'undefined' || options.token != '' ) 
		&& (options.last_stamp !== 'undefined' || options.last_stamp != '')  
		&& options.remember == 1 ){
			minutes_to_expire = 604800000; // 7 days
			if (new_timestamp - options.last_stamp > 604800000) {
				return false;
			}
		}

		// renew the session date
		/*
		if( ( typeof store.get('token') !== 'undefined' || store.get('token') != '' ) 
		&& ( store.get('remember') == 0 || store.get('remember') == 1 ) ){

		}
		*/
		store.set('session_date', new_timestamp);
		return true;
	};

// check every 6 seconds if session has been expired	
/*
setInterval(function() {
	var token = store.get('token'), 
			last_stamp = store.get('session_date'),
			remember = store.get('remember');

	// check here if logged In session has already been expired
	var session = check_login_session({
						"token": token,
						"last_stamp": last_stamp,
						"remember": remember
					});

	// if session is expired, make a pop-up here
	if(!session) {
		//alert("Your session has been expired due to inactivity!");
		//autoLogout();
		return;
	}

}, 6000);	
*/

function autoLogout(){
	var url = root + 'signout.html';
	var form_data = {};
	form_data.post_dta = 1;
	form_data.temp_sessionid = store.get('temp_sessionid');
	form_data.token = store.get('token');
		
	$.ajax({
		type: "POST",
		url: url,
		data: form_data,
		cache: false,
		success: function(data){
			if(data == 'success'){
				//store.clear();
				store.remove('user_id');
				store.remove('cart');
				store.remove('token');

				var currentUrl = location.href.replace('#','');
				var pos = root.indexOf("?");
				var querystring = (pos == -1) ? "?referer="+ escape(currentUrl) : '&referer=' + pos;
				location.href = root + querystring;
			}
		}
	});
	return false;
}

function hash_querystring(hash){
	var query = {};
	if(typeof hash == "undefined" || hash == "") return query;

	var hash = hash.split('&');
	var q;

	for(var i = 0; i < hash.length; i++) {
		if(hash[i] != "") {
			q = hash[i].split('=');
			if(typeof(q[1]) != "undefined" && q[1] != "") {
				query[q[0]] = q[1].toString();
			}
		}
	}
	return query;
}		

function get_hash_querystring_value(hash,name) {
	var hash = hash_querystring(hash);
	if(hash.hasOwnProperty(name)) {
		for(i in hash) {
			if(i == name) {
				return hash[i];
			}
		}
	}

	return "";
}

function load_page(param,pagetype) {
	var url = $(location).attr('href').split('#');
	var mainURL = url[0], hash = url[1];
	
	hash = typeof hash == "undefined" ? "" : hash;
	//var container = get_hash_querystring_value(hash,'c');

	var param = (typeof param == "undefined") ? {} : param; 
	var container = "#wrapper";
	var page_title;
	var user_id = store.get('user_id');
	var token = store.get('token');
	var form_data = {
		isAjax:true,
		token: token,
		user_id: user_id
	};
	var browserNav = false;
	
	if(param.hasOwnProperty('container')) {
		container = param.container;
	}

	if(param.hasOwnProperty('data')) {
		form_data = $.extend(true,form_data, param.data);
	}

	form_data.shipto_country = store.get('shipto_country');

	if(param.hasOwnProperty('page_tile')) {
		page_title = param.page_title;
	}
	else {
		page_title = $(document).find("title").text();
	}

	if(param.hasOwnProperty('url')) {
		mainURL = param.url;
		if(mainURL.indexOf(CS.siteURL) == -1) {
			mainURL = CS.siteURL + mainURL.substr(1);
		}
	}	

	if(param.hasOwnProperty('browserNav')) {
		browserNav = (param.browserNav) ? true : false;
	}

	if ( History.enabled) {
		History.pushState(null, page_title, mainURL);
	}

	//if(mainURL == History.getState().url && browserNav) {

	if(browserNav) {
		return;
	}

	if(container == "#subcontent-wrapper") {
		toggle_preloader_maincontent();
	}
	/*
	var controller = mainURL.replace(CS.siteURL,'');
	//alert(mainURL)
	var cssfile = CS.cloudURL + 'css/v2/main.css';
	alert(controller)
	if(controller != ""){
		controller = controller.split('/');
		controller = controller[1];
		controller = controller.split('?')[0];
		controller = controller.toLowerCase().replace(/[^a-z0-9]/g,'');
		var cssfile = CS.cloudURL + 'css/v2/' + controller + '.css';
	}
	var loadCSS = new Array();

	if($.inArray(cssfile, $('html').data('loadCSS')) == -1) {
		loadCSS.push(cssfile);
	}

	if(typeof $('html').data('loadCSS') != "object") {
		$('html').data('loadCSS',[]);
	}
	if(loadCSS.length > 0) {
		$.get(cssfile, function(contents){
			$("<style type=\"text/css\">" + contents + "</style>").appendTo(document.head);
		})
		.always(function(){
			$('html').data('loadCSS',$.merge($('html').data('loadCSS'),loadCSS));

			SSAjax({
				data: form_data,
				url: mainURL,
				success:function(response) {
					$(container).html(response);
					if(container == "#subcontent-wrapper") {
						toggle_preloader_maincontent();
					}
					else if(container == "#wrapper") {
						toggleScroll_leftmenu();
					}
				}
			});
		});

		return;
	}
	*/
	SSAjax({
		data: form_data,
		url: mainURL,
		success:function(response) {
			if(pagetype == "checkout") {
				try {
					var data = $.parseJSON(response);
					if(!$.isEmptyObject(data) && typeof data.redirect_code != "undefined") {
						/*
						switch(data.redirect_code) {
							case 100:
								location.href = data.url;
								break;
						}
						*/
						location.href = data.url;
						
						return;
					}
				} catch(e) {

				}

			}

			$(container).html(response).removeClass('hide');
			if(container == "#subcontent-wrapper") {
				toggle_preloader_maincontent();
			}
			else if(container == "#wrapper") {
				toggleScroll_leftmenu();
			}

			hideLoader();
			/*
			if(hash != "") {
				var order_id = get_hash_querystring_value(hash,"vpo");
				if(order_id != "") {
					view_purchase_order_details(order_id);
				}
			}
			*/			
		},
		error: function(jqXHR, textStatus, errorThrown ) {
			if(jqXHR.status == 404) {
				location.href = "/";
			}
		}
	});
}

function toggle_preloader_maincontent() {
	if($('#body-wrapper .shopstyxloader').eq(0).hasClass('hide')) {
		$('#subcontent-wrapper').eq(0).addClass('hide');
		$('#body-wrapper .shopstyxloader').eq(0).removeClass('hide');
		$('#body-wrapper .overlay').eq(0).removeClass('hide');	
	}
	else {
		$('#body-wrapper .shopstyxloader').eq(0).addClass('hide');
		$('#body-wrapper .overlay').eq(0).addClass('hide');
		$('#subcontent-wrapper').eq(0).removeClass('hide');
	}
}


/* 	===================================
 *	Events Section
 *	===================================
 */

$(document).on('click touchstart', '#header .userNav .logout', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	autoLogout();
});

$(document).on('change', '#header #change-photo input, #change-profile-photo', function(e){
	_upload_userpic($(this));
});

$(document).on('click', '.profile-menu .uload-logo, .profile-menu .ui-logo', function(e){
	_update_pic_display($(this));
});

$(document).on('touchstart click', '#header .userNav .navLink.messages .bg', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	doNewMessage();
});

$(document).on('mouseenter', '#header .userNav .navLink.messages', function(e){
	//e.preventDefault(); e.stopImmediatePropagation();
	close_panels();
	chatprops.msghovd = true;
	nummsg = 0;
	$(this).find('.messages-panel.npops').removeClass('hide');
});

$(document).on('mouseleave', '#header .userNav .navLink.messages', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	chatprops.msghovd = false;
	close_messages();
});


$(document).on('mouseenter', '#sub-wrapper #header .userNav .navLink.cart, #small-header-wrapper .userNav .navLink.cart', function(){
	close_panels();
	if($(this).hasClass('active')) {
		$(this).find('.cart-panel.npops').removeClass('hide');
	}
	else {
		load_cart();
	}
});

$(document).on('mouseleave', '#sub-wrapper #header .userNav .navLink.cart, #small-header-wrapper .userNav .navLink.cart', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	close_cart();
});

$(document).on('mouseenter', '#sub-wrapper #header .userNav .navLink.wallet, #small-header-wrapper .userNav .navLink.wallet', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	close_panels();
	if($(this).hasClass('active')) {
		$(this).find('.wallet-panel.npops').removeClass('hide');
	}
	else {
		load_wallet();
	}
});

$(document).on('mouseleave', '#sub-wrapper #header .userNav .navLink.wallet, #small-header-wrapper .userNav .navLink.wallet', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	close_wallet();
});


// $(document).on('mouseenter', '.wallet-circbtn', function(e){
	// e.preventDefault(); e.stopImmediatePropagation();
	// //close_panels();
	// if($(this).hasClass('active')) {
		// $(this).find('.pop-wallet-panel.npops').removeClass('hide');
	// }
	// else {
		// load_wallet2();
	// }
// });

// $(document).on('mouseleave', '.wallet-circbtn', function(e){
	// e.preventDefault(); e.stopImmediatePropagation();
	// close_wallet2();
// });

$(document).on('mouseenter', '#sub-wrapper #header .userNav .navLink.notif, #small-header-wrapper .userNav .navLink.notif', function(e){
	//e.preventDefault(); e.stopImmediatePropagation();
	close_panels();
	//chatprops.notifhovd = true;
	var cntr = $('.navLink.notif .bg span.total-counter');
	cntr.addClass('hide');
	
	SSAjax({
		url: '/notifications/mark_read_notifs',
		data: {user_id: store.get('user_id'), 
			   token: store.get('token') }
	});
	numnotifs=0;
				
	$(this).find('.notification-panel.npops').removeClass('hide');
});

$(document).on('mouseleave', '#sub-wrapper #header .userNav .navLink.notif, #small-header-wrapper .userNav .navLink.notif', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	//chatprops.notifhovd = false;
	//reset_scroll_data_opts('fornotif');
	close_panels();
});


/*
$(document).on('click', '#header .userNav .navLink.cart .cart-panel .products-wrapper .delete-item', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	delete_cart_item($(this));
});


$(document).on('click', '#header .userNav .navLink.wallet .bgColor, #header .userNav .navLink.wallet .bg, #claim-wallet-form .success-claimed-wallet a', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	load_wallet($(this));
});

$(document).on('click', '#header .userNav .wallet-panel .close-btn', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	close_wallet();
});
*/

$(document).on('click', '#header .userNav .wallet-panel .claim-money-btn:not(".disabled")', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	var type = ($(this).hasClass('C')) ? "C" : "S";
	claim_walet_popup(type);
});

$(document).on('click', '.wallet-modal #claim-wallet-form .paypal-email-txt a, .wallet-modal #claim-wallet-form .no-paypal-email a', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	var type = $('.wallet-modal #claim-wallet-form').attr('wtype');
	wallet_paypalemail_popup(type);
});

$(document).on('click', '#wallet-pop .header-wrapper .right.back-btn, #wallet-pop .header-wrapper .claim-wallet-backbtn', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	var type = $(this).attr('wtype');
	claim_walet_popup(type);
});

/*
$(document).on('click', '#claim-wallet-form .paypal-email-txt a, #claim-wallet-form .no-paypal-email a.vendor', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	History.pushState(null, "Shopstyx", "/admin/shop-settings/getpaid");
	$('.shopsettings .paypalemail-wrapper input[name="paypal_email"]').focus();
});
*/
$(document).on('closed', '#wallet-pop', function () {
	if(typeof dispute_countdownTimer != "undefined") {
		clearInterval(dispute_countdownTimer);
	}
	var loader = $(this).find('.loadcont');
	loader.addClass('hide');	
	$(this).find('.loader').addClass('hide');	
	$(this).find('.loader-overlay').addClass('hide');
	$('#main-wrapper').show();
	
	if(typeof scrollopts != "undefined") {
		delete scrollopts['for-purchasehistory'];
	}
});

$(document).on('click', '.wallet-modal .cont .pent #save-paypal-email-btn', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	var wtype = $(this).attr('wtype');
	save_promoter_paypalemail(wtype);
});

$(document).on('click', '.wallet-panel .history-btn.walletpurchases, #wallet-pop .header-wrapper .purchase-history-backbtn', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	load_wallet_purchase_history();
});

$(document).on('click', '.wallet-panel .history-btn.walletsales', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	load_wallet_sales_history();
});

$(document).on('click', '#wallet-pop .header-wrapper .viewhistory-btn', function(e){
	var wtype = $(this).attr('wtype');
	e.preventDefault(); e.stopImmediatePropagation();
	load_wallet_claimed_history(wtype);
});

/* Claim Money on wallet */
$(document).on('keyup ', '.wallet-modal #claim-wallet-form #claim-wallet-amount', function(e) {
	e.stopImmediatePropagation();
	val = parseFloat($(this).val());
	val = isNaN(val) ? 0 : val;
	paypal_fee = (val*0.02);
	paypal_fee = Math.round(paypal_fee*100) / 100; 
	paypal_fee = paypal_fee.toFixed(2);
	claim_amount = val - paypal_fee;
	claim_amount = Math.round(claim_amount*100) / 100; 
	//claim_amount = claim_amount.toFixed(2);
	claim_amount = claim_amount.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");
	$('#claim-wallet-form .claim-form-wrapper .field input#claim-wallet-paypal-fee').val(paypal_fee);
	$('#claim-wallet-form .claim-form-wrapper .claim-wallet-btn').html('Claim $'+claim_amount);
});

$(document).on('click', '.wallet-modal #claim-wallet-form .claim-form-wrapper .claim-wallet-btn:not(".disabled")', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	claim_wallet($(this),'authenticate');
});

$(document).on('click', '#wallet-pop .cont .claimcofirmation-wrapper button', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	claim_wallet($(this),'claim');
});

$(document).on('click', '#wallet-pop .cont .my-tbl.purchasehistory .tbl-cell.ordertotal .od-btns-wrapper', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	var id = $(this).attr('order_id');
	view_purchase_order_details(id);
});

$(document).on('click', '#wallet-pop .cont .order-details-wrapper .confirm-order-btn.enabled', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	var id = $(this).attr('order_id');
	confirm_transaction_order(id);
});

$(document).on('click', '#wallet-pop .cont .order-details-wrapper .open-dispute-product-btn .distpute-type-wrapper .dlink', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	var el = $(this).parent(),
		id = el.attr('order_id'),
		pid = el.attr('pid'),
		variant_id = el.attr('variant_id')
		dtype = $(this).attr('dtype');
	open_dispute_order_product(dtype,id,pid,variant_id);
});

$(document).on('click', '#wallet-pop .cont .order-details-wrapper .order-dispute-btn', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	var el = $(this),
		id = el.attr('order_id'),
		pid = 0,
		variant_id = 0
		dtype = 1;
	open_dispute_order_product(dtype,id,pid,variant_id);
});


$(document).on('click', '#wallet-pop .cont .open-dispute-modal .closedispute-btn', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	$('#wallet-pop .open-dispute-modal').addClass('hide').html('');
	$('#wallet-pop .cont .order-details-wrapper').removeClass('hide');
});

$(document).on('change','.open-dispute-modal .uploadfile-btn input[name="dispute_images"]', function(e) {
	var input = this;
	var limit_reached = false;
	var form = $('#wallet-pop .open-dispute-modal form');
	var upload_btn = $('.open-dispute-modal .uploadfile-btn');
	var ul = $('.open-dispute-modal .dispute-form-wrapper .dispute-images ul');
	var max_images = 3;
    if (input.files && input.files[0]) {
        for(var i = 0, f; f = this.files[i]; i++) {
            // Only process image files.
            var file_mime = f.type;
            var fname = f.name.split('.');
            var flength = fname.length-1;
            var fext = fname[flength];


            var is_allowed = ($.inArray(file_mime,["images/jpeg","images/jpg","images/png"]) == -1 &&
								$.inArray(file_mime,["jpeg","jpg","png"]) == -1) ? false : true;

            // Only process image files.
            if (!file_mime.match('image.*') && !is_allowed) {
                continue;
            }
            else {
                if(limit_reached) {
                	upload_btn.addClass('hide');
                    continue;
                } else {
                    if(typeof form.data('image_files') != "undefined" && form.data('image_files').length >= max_images) {
                        //alert("You have reached the maximum photos that can be uploaded.");
                        limit_reached = true;
                        upload_btn.addClass('hide');
                        continue;
                    }
                    else {
                    	if(typeof form.data('image_files') == "undefined") {
                    		form.data('image_files',[]);
                    	}
                    	form.data('image_files').push(f);

                    	var li = document.createElement("li");
						li.innerHTML = f.name + " <a>delete</a>";
						ul.append(li);

						if(typeof form.data('image_files') != "undefined" && form.data('image_files').length >= max_images) {
							limit_reached = true;
	                        upload_btn.addClass('hide');
						}
                    }
                }
            }
        }
    }
}); 

$(document).on('click', '.open-dispute-modal .dispute-form-wrapper .dispute-images ul li a', function(e) {
	e.preventDefault(); e.stopImmediatePropagation();
	var form = $('#wallet-pop .open-dispute-modal form');
	var index = $('.open-dispute-modal .dispute-form-wrapper .dispute-images ul li').index($(this).parent());
	var upload_btn = $('.open-dispute-modal .uploadfile-btn'),
		max_images = 3;

	form.data('image_files').splice(index,1);
	$(this).parent().remove();

	if(form.data('image_files').length == 0) {
		form.data('image_files',[]);
	}

	if(form.data('image_files').length < max_images) {
		upload_btn.removeClass('hide');
	}
});

$(document).on('submit', '#wallet-pop .open-dispute-modal form', function(e) {
	e.preventDefault(); e.stopImmediatePropagation();
	submit_order_dispute($(this));
});

$(document).on("click", "#resolve-transaction-order-product,#resolve-transaction-order", function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	resolve_transaction_order($(this).attr('order_id'),$(this).attr('pid'),$(this).attr('variant_id'));
});

$(document).on('click', '.main-modal .mmodal-header .action-buttons .close-btn', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	var cpo = new customer_purchase_order();
	cpo.close();
});

$(document).on('click', '#header .userNav .navLink.messages', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	load_message($(this));
});

$(document).on('click', '#header .userNav .message-panel .close-btn', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	close_message();
});

$(document).on('click', '.view-purchase-order', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	var order_id = $(this).attr('order_id');
	view_purchase_order_details(order_id);
});

$(document).on('mouseover','.dd-select .outer', function(e) {
	$('.dd-select .opts div').removeClass('selected');
});

$(document).on('mouseleave','.dd-select', function(e) {
	var el = $(this);
	var val = el.attr('value');

	el.find('.opts div[value="'+val+'"]').addClass('selected');
});

$(document).on('click','.dd-select .opts div', function(e) {
	var val = $(this).attr('value');
	var text = $(this).html();
	var col2 = $(this).parents('.dd-select');

	col2.attr('value',val);
	col2.find('em').html(text);
	col2.find('.outer').hide();
	$(this).addClass('selected');

	setTimeout(function(){ col2.find('.outer').attr('style','') }, 400);
});

$(document).on('mouseover','.header-caption-action-btn .search-btn', function() {
	var el = $(this);
	var select = el.parent().find('.dd-select');
	var input = el.parent().find('.search-input');

	select.addClass('hide');
	input.removeClass('hide');

	input.focus();
});

$(document).on('blur','.header-caption-action-btn .search-input', function() {
	var el = $(this);
	var select = el.parent().find('.dd-select');
	var input = el.parent().find('.search-input');

	if(input.val() != "") { return; }
	input.addClass('hide');
	select.removeClass('hide');
});

$(document).on('mouseover','.header-caption-action-btn .add-btn', function() {
	var el = $(this);
	var select = el.parent().find('.dd-select');
	var input = el.parent().find('.search-input');

	if(input.val() != "") { return; }
	input.addClass('hide');
	select.removeClass('hide');
});

$(document).on('click','.header-caption-action-btn .add-btn', function(e) {
	e.preventDefault(); e.stopImmediatePropagation();

	addproduct({
				data:{product_id:0, formType:'basic'},
				onSaved: function(data) {
							console.log(data);
						},
				beforeClose: function(){
					console.log('closing the window');
				}			
						
			});
});

$(document).on('change','#subcontent-wrapper select', function() {
	if($(this).hasClass('js-notenabled')) { return false; }
	if($(this).val() != "") {
		$(this).removeClass('color1').addClass('color2 selected');
	}
	else {
		$(this).removeClass('color2').addClass('color1').removeClass('selected');
	}
});	

$(document).on('touchstart click','#addproduct-btn', function(e) {
	e.preventDefault(); e.stopImmediatePropagation();
	var parent_catid = $('.main-category-hidden').val(),
		modal_id = '#category-product-modal',
		level = $('.level').val();
	addproduct({
		//loadType: "slideRight",
		data:{category_id: $(this).attr('data-cid'), product_id:0, formType:'basic'},
		onSaved: function(data) {
			$('body').css('overflow','hidden');
			reloadPage({link:'/admin/catalog_actions?action=reload_catalog', wrapper: '.catagory-list', preloader: '.mcatalogloader'});
			reloadPage({category_id: parent_catid, link:'/admin/catalog_actions?action=category_products', wrapper: modal_id + ' .mod-wrapper', preloader: '.mod-loader-wrapper', level:level});
		},
		beforeClose: function(){
			$('body').css('overflow','hidden');
		}
	});
});

$(document).on('click', '.mod-close', function(){
	closeModal2();
});
$(document).on('click', '.blog-mod-close', function(){
	$('.blog-modal2 .mod-wrapper').html('');
	closeModal3();
});

$(document).on('click', '.vproduct', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	$('.product-info-modal .mod-wrapper').html('');
	openModal();
	var $form = $(this),
		form_data = {};
	form_data.isAjax = true;
	if($(this).hasClass('product_featured')){
		form_data.featured = 1;
	}else{
		form_data.featured = 0;
	}
	var settings = $.parseJSON($(this).attr('data'));
	$.extend(form_data,settings);
	showModalLoader();
	SSAjax({
		data: form_data,
		url: "/admin/catalog?page=product-details",
		success: function(response){
			$('.product-info-modal .mod-wrapper').html(response);
			var height = $('.modal-header').height(),
				window_height = $(window).height();
			window_height = window_height - height;
			// $('.product-info-modal .mod-wrapper-content').slimScroll({
				// height: window_height,
				// width: '100%',
				// distance: '6px'
			// });
			hideModalLoader();
		}
	});
});

$(document).on('click', '#add-category-btn', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	var modal_id = '#category-modal';
	$(modal_id + ' .mod-wrapper').html('');
	openCustomModal(modal_id);
	var $form = $(this),
		form_data = {};
	form_data.isAjax = true;
	showModalLoader();
	SSAjax({
		data: form_data,
		url: "/admin/catalog_actions?action=manage_category",
		loadJS: [cloud_url+'js/jquery.form.js'],
		success: function(response){
			$(modal_id + ' .mod-wrapper').html(response);
			hideModalLoader();
		}
	});
});

$(document).on('click', '#edit-category', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	var modal_id = '#category-modal2';
	$(modal_id + ' .mod-wrapper').html('');
	openCustomModal(modal_id);
	var $form = $(this),
		form_data = {};
	form_data.isAjax = true;
	form_data.order_id = $(this).attr('data-cid');
	form_data.parent_order_id = $(this).attr('data-pcid');
	$('.mod-category-loader-wrapper').removeClass('hide');
	SSAjax({
		data: form_data,
		url: "/admin/catalog_actions?action=edit_category",
		loadJS: [cloud_url+'js/jquery.form.js'],
		success: function(response){
			$(modal_id + ' .mod-wrapper').html(response);
			$('.mod-category-loader-wrapper').addClass('hide');
		}
	});
});

$(document).on('click', '#delete-category', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	var $form = $(this),
		form_data = {},
		parent_catid = $('.parent-cid').val(),
		modal_id = '#category-product-modal',
		category_id = parseInt($('.product-list .sub-nav .active').attr('data-cid'));
		if(category_id == 0){
			category_id = $('.main-category-hidden').val();
		}
		
	form_data.isAjax = true;
	form_data.order_id = $(this).attr('data-cid');
	form_data.parent_order_id = $(this).attr('data-pcid');
	if(parseInt(form_data.parent_order_id) > 0){
		//$(modal_id + ' .mod-wrapper').html('');
	}
	$('.mod-loader-wrapper').show();
	SSAjax({
		data: form_data,
		url: "/admin/catalog_actions?action=delete_category",
		success: function(response){
			var _datas = $.parseJSON(response);
			if( _datas.status == "has products"){
				$('.mod-loader-wrapper').hide();
				alert(_datas.msg);
				return false;
			}
			if( _datas.status == "failed"){
				$('.mod-loader-wrapper').hide();
				alert('Delete first the sub category before deleting the main category...');
				return false;
			}
			reloadPage({link:'/admin/catalog_actions?action=reload_catalog', wrapper: '.catagory-list', preloader: '.mcatalogloader'});
			if(parseInt(form_data.parent_order_id) > 0){
				reloadPage({category_id: parent_catid, link:'/admin/catalog_actions?action=category_products', wrapper: modal_id + ' .mod-wrapper', preloader: '.mod-loader-wrapper'});
			}
			if(parseInt(form_data.parent_order_id) == 0){ $('.cp-modal-close').trigger('click'); }
			
		}
	});
});

$(document).on('touchstart click', '.category-list-box', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	var modal_id = '#category-product-modal';
	$(modal_id + ' .mod-wrapper').html('');
	openCustomModal(modal_id);
	reloadPage({category_id:$(this).attr('data-cid'), link:'/admin/catalog_actions?action=category_products', wrapper: modal_id + ' .mod-wrapper', preloader: '.mod-loader-wrapper', level: $(this).attr('data-level')});
});

$(document).on('touchstart click', '._search .search-btn', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	var category_id = parseInt($('.product-list .sub-nav .active').attr('data-cid')),
		level = $('.level').val();
	if(! $('dl').hasClass('sub-nav') || category_id == 0){
		category_id = $('.main-category-hidden').val();
	}
	var searchtext = $.trim($('._search input').val());
	if( searchtext == ''){
		return false;
	}
	
	var modal_id = '#category-product-modal';
	$(modal_id + ' .mod-wrapper').html('');
	openCustomModal(modal_id);
	reloadPage({searchtext: searchtext, category_id: category_id, link:'/admin/catalog_actions?action=category_products', wrapper: modal_id + ' .mod-wrapper', preloader: '.mod-loader-wrapper', level:level});
});

$(document).on('keypress', '._search input', function(e){
	e.stopImmediatePropagation();
	if (e.which == 13) {
		var category_id = parseInt($('.product-list .sub-nav .active').attr('data-cid')),
			level = $('.level').val();
		if(! $('dl').hasClass('sub-nav') || category_id == 0){
			category_id = $('.main-category-hidden').val();
		}
		var searchtext = $.trim($(this).val());
		if( searchtext == ''){
			return false;
		}
		
		var modal_id = '#category-product-modal';
		$(modal_id + ' .mod-wrapper').html('');
		openCustomModal(modal_id);
		reloadPage({searchtext: searchtext, category_id: category_id, link:'/admin/catalog_actions?action=category_products', wrapper: modal_id + ' .mod-wrapper', preloader: '.mod-loader-wrapper', level:level});
	}
});



$(document).on('touchstart click','#edit-product-btn', function(e) {
	e.preventDefault(); e.stopImmediatePropagation();
	var modal_id = '#category-product-modal',
		category_id = $(this).attr('data-cid'),
		load_cat = $(this).attr('data-load-cat'),
		level = $('.level').val();
	addproduct({
		data:{product_id: $(this).attr('data-pid')},
		onSaved: function(data) {
			$('body').css('overflow','hidden');
			reloadPage({category_id:load_cat, link:'/admin/catalog_actions?action=category_products', wrapper: modal_id + ' .mod-wrapper', preloader: '.mod-loader-wrapper', level:level});
		},
		beforeClose: function(){
			$('body').css('overflow','hidden');
		}
	});
});

$(document).on('touchstart click','#product-edit-btn', function(e) {
	e.preventDefault(); e.stopImmediatePropagation();
	var modal_id = '#category-product-modal',
		load_cat = parseInt($('.product-list .sub-nav .active').attr('data-cid'));
		if(load_cat == 0){
			load_cat = $('.main-category-hidden').val();
		}
	addproduct({
		data:{product_id: $(this).attr('data-pid')},
		onSaved: function(data) {
				$('body').css('overflow','hidden');
				$('.reload-product').trigger('click');
				reloadPage({category_id:load_cat, link:'/admin/catalog_actions?action=category_products', wrapper: modal_id + ' .mod-wrapper', preloader: '.mod-loader-wrapper'});
				reloadPage({link:'/admin/catalog_actions?action=reload_catalog', wrapper: '.catagory-list', preloader: '.mcatalogloader'});
			},
		beforeClose: function(){
			$('body').css('overflow','hidden');
		}
	});
});

$(document).on('touchstart click', '#delete-product-btn', function(e){
	e.stopImmediatePropagation();
	var level = $('.level').val();
	var product_name = $(this).parents('.p-list-box').find('.product-name').html();

	if (! confirm("Are you sure you want to delete " + product_name + " ?")) {
		return false;
	}
	
	var modal_id = '#category-product-modal';
	var form_data = {},
		load_cat = $(this).attr('data-load-cat');
	form_data.isAjax = true;
	form_data.store_category_id = $(this).attr('data-cid');
	form_data.product_id = $(this).attr('data-pid');
	$('.mod-loader-wrapper .shopstyxloader div').html('DELETING...');
	$('.mod-loader-wrapper').show();
	SSAjax({
		data: form_data,
		url: "/admin/catalog_actions?action=remove_category_product",
		success: function(response){
			reloadPage({category_id:load_cat, link:'/admin/catalog_actions?action=category_products', wrapper: modal_id + ' .mod-wrapper', preloader: '.mod-loader-wrapper', level:level});
			reloadPage({link:'/admin/catalog_actions?action=reload_catalog', wrapper: '.catagory-list', preloader: '.mcatalogloader'});
		}
	});
});

$(document).on('touchstart click', '#product-delete-btn', function(e){
	e.stopImmediatePropagation();
	var product_name = $(this).parents('.prod-title-wrapper').find('.prod-title h2').html();

	if (! confirm("Are you sure you want to delete " + product_name + " ?")) {
		return false;
	}
	
	var modal_id = '#category-product-modal';
	var form_data = {},
		load_cat = parseInt($('.product-list .sub-nav .active').attr('data-cid'));
		if(load_cat == 0){
			load_cat = $('.main-category-hidden').val();
		}
	form_data.isAjax = true;
	form_data.store_category_id = $(this).attr('data-cid');
	form_data.product_id = $(this).attr('data-pid');
	$('.mod-loader-wrapper .shopstyxloader div').html('DELETING...');
	$('.mod-loader-wrapper').show();
	SSAjax({
		data: form_data,
		url: "/admin/catalog_actions?action=remove_category_product",
		success: function(response){
			var _datas = $.parseJSON(response);
			if(_datas.has_product){ 
				reloadPage({category_id:load_cat, link:'/admin/catalog_actions?action=category_products', wrapper: modal_id + ' .mod-wrapper', preloader: '.mod-loader-wrapper'});
			}else{
				$('.cp-modal-close').trigger('click'); 
			}
			reloadPage({link:'/admin/catalog_actions?action=reload_catalog', wrapper: '.catagory-list', preloader: '.mcatalogloader'});
			$('.prod-title-wrapper .mod-close').trigger('click');
		}
	});
});

$(document).on('touchstart click', '.feature-product-btn', function(e){
	e.stopImmediatePropagation();
	$('.category-product-modal .mod-loader-wrapper').show();
	var form_data = {};

	if($(this).hasClass('featured')){
		$(this).removeClass('featured');
		$('.vproduct').removeClass('product_featured');
		form_data.featured = 0;
	}else{
		$(this).addClass('featured');
		$('.vproduct').addClass('product_featured');
		form_data.featured = 1;
	}
	form_data.isAjax = true;
	var featured_val = $.parseJSON($(this).attr('data-featured'));
	$.extend(form_data,featured_val);
	SSAjax({
		data: form_data,
		url: "/admin/catalog_actions?action=feature_product",
		success: function(response){
			$('.category-product-modal .mod-loader-wrapper').hide();
		}
	});
	
});

$(document).on('touchstart click', '.feature-this-product', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	$('.' + $(this).attr('data-feat')).trigger('click');
	if($(this).hasClass('featured-active')){
		$(this).removeClass('featured-active');
	}else{
		$(this).addClass('featured-active');
	}
});

$(document).on('touchstart click', '#shop-preview-btn, .back-shop-btn', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	loadMPhone = true;
	$('.preview-shop-modal').css({'width':'904px'});
	var modal_id = '#preview-shop-modal';
	$(modal_id + ' .mod-wrapper').html('');
	openCustomModal(modal_id);
	reloadPage({link:'/admin/build?action=shop-design-preview', wrapper: modal_id + ' .mod-wrapper', preloader: '.mod-loader-wrapper'});
});

$(document).on('touchstart click', '#tablet-mod-btn', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	var modal_id = '#preview-shop-modal';
	$(modal_id + ' .mod-wrapper').html('');
	openCustomModal(modal_id);
	reloadPage({link:'/admin/build?action=shop-tablet-preview', wrapper: modal_id + ' .mod-wrapper', preloader: '.mod-loader-wrapper'});
});

$(document).on('click', '.tabphone-btn2', function(){
	$('.tabphone-btn2').removeClass('tabphone-active');
	$(this).addClass('tabphone-active');
	$('.tabphone-wrapper').addClass('hide');
	$('.' + $(this).attr('data-class')).removeClass('hide');
});
$(document).on('click', '.tablet-btn2', function(){
	$('.preview-shop-modal').css({'width':'904px'});
	$('.mod-other-option label').show();
	$('.edit-shop').html('Edit');
});

$(document).on('click', '.phone-btn2', function(e){
	$('.preview-shop-modal').css({'width':'514px'});
	$('.mod-other-option label').hide();
	$('.edit-shop').html('Edit Design');
	e.preventDefault(); e.stopImmediatePropagation();
	if(loadMPhone){
		var modal_id = '#preview-shop-modal';
		openCustomModal(modal_id);
		loadMPhone = false;
		reloadPage({link:'/admin/build?action=shop-phone-preview', wrapper: modal_id + ' .phone-frame', preloader: ''});
	}
});

$(document).on('touchstart click', '#fb-mod-btn', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	var modal_id = '#preview-shop-modal';
	$(modal_id + ' .mod-wrapper').html('');
	openCustomModal(modal_id);
	reloadPage({link:'/admin/build?action=shop-facebook-preview', wrapper: modal_id + ' .mod-wrapper', preloader: '.mod-loader-wrapper'});
});

$(document).on('touchstart click', '#minishop-mod-btn', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	var modal_id = '#preview-shop-modal';
	$(modal_id + ' .mod-wrapper').html('');
	openCustomModal(modal_id);
	reloadPage({link:'/admin/build?action=shop-minishop-preview', wrapper: modal_id + ' .mod-wrapper', preloader: '.mod-loader-wrapper'});
});

$(document).on('click touchstart', '.edit-shop-btn', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	if($(this).hasClass('data-tablet-phone')){
		store.set('manage_design', 'tablet-phone');
		if($('.tablet-btn2').hasClass('tabphone-active')){ store.set('manage_design_active', 'tablet'); }
		if($('.phone-btn2').hasClass('tabphone-active')){ store.set('manage_design_active', 'phone'); }
	}
	if($(this).hasClass('data-facebook')){
		store.set('manage_design', 'facebook');
	}
	if($(this).hasClass('data-minishop')){
		store.set('manage_design', 'minishop');
	}
	var el = $(this);
	var url  = el.attr('data-link');
	$('#inner-leftmenu').find('li').removeClass('selected');
	$('.sm-design').addClass('selected');
	
	$('body').css({'overflow':'inherit'});

	load_page({
		browserNav: true,
		url: url,
		container: "#subcontent-wrapper",
		data: {body_wrapper:true,subcontent_wrapper:true}
	});
});

$(document).on('click touchstart', '#manage-shop-btn', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	var el = $(this);
	var url  = el.attr('href');
	$('#inner-leftmenu').find('li').removeClass('selected');
	$('.sm-catalog').addClass('selected');
	
	$('body').css({'overflow':'inherit'});

	load_page({
		browserNav: true,
		url: url,
		container: "#subcontent-wrapper",
		data: {body_wrapper:true,subcontent_wrapper:true}
	});
});

$(document).on('click', '.view-blog-btn2', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	openModal3();
	reloadPage({link:'/admin/build?action=blog-template', wrapper: '.blog-modal2 .mod-wrapper', preloader: '.blog-modal2 .mod-loader-wrapper'});
});
$(document).on('click', '.back-details-btn', function(e){
	e.stopImmediatePropagation();
	$('.reload-product').trigger('click');
});

$(document).on('touchstart click', '.stickit-btn, .promte-btn, .stykable-size-btn', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	$('.product-info-modal .mod-wrapper').html('');
	openModal();
	var $form = $(this),
		form_data = {};
	form_data.isAjax = true;
	
	var settings = $.parseJSON($(this).attr('data'));
	$.extend(form_data,settings);
	showModalLoader();
	SSAjax({
		data: form_data,
		url: "/admin/stykable_actions?action=stykable",
		success: function(response){
			$('.product-info-modal .mod-wrapper').html(response);
			hideModalLoader();
		}
	});
});

$(document).on('click', '.category-modal-close', function(){
	closeCustomModal('#category-modal');
});
$(document).on('click', '.category-modal2-close', function(){
	closeCustomModal2('#category-modal2');
});
$(document).on('click', '.cp-modal-close', function(){
	closeCustomModal('#category-product-modal');
	$('.category-product-modal .modal-content').html('');
});
$(document).on('click', '.shop-preview-close', function(){
	loadMPhone = true;
	closeCustomModal('#preview-shop-modal');
});


$(document).on('click', '.view_pdetails', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	view_proddetails($(this));
});


$(document).on('click', '.product-details-view', function () {
	$('#productViewModal').foundation('reveal', 'close');
});

$(document).on('click', '.product-details-content, .sticked-more-collection, .like-products', function (e) {
	e.stopImmediatePropagation();
});

$(document).on('open', '#productViewModal', function () {
	$(this).centerized();
});
$(document).on('opened', '#productViewModal', function () {
	$('.reveal-modal-bg').addClass('reveal-modal-bg-color');
	var top = parseInt($('.product-details-view').position().top) - 88;
	$('.product-details-view').css({'top': $('.product-details-view').position().top});
});

$(document).on('close', '#productViewModal, #cartpops', function () {
	History.pushState(null, 'Shopstyx', CS.current_url);
	$('body').removeClass('hidescroll');
	$('.product-details-view .product-info-wrapper').html('');
});

$(document).on('open', '#wallet-pop', function () {
	$(this).centerized();
	$('#main-wrapper').hide();
});
$(document).on('opened', '#wallet-pop', function () {
	$('.reveal-modal-bg').addClass('reveal-modal-bg-color');
	//var top = parseInt($('.product-details-view').position().top) - 88;
	//$('.product-details-view').css({'top': $('.product-details-view').position().top});
});



$(document).on('open', '#productViewModal', function () {
	$(this).centerized();
});
$(document).on('touchstart click', '.close-product-view', function(){
	$('#productViewModal').foundation('reveal', 'close');
	$('.reveal-modal-bg').remove();
	History.pushState(null, 'Shopstyx', CS.current_url);
});

$(document).on('click', '.reveal-modal-promoteproduct-bg', function(e) {
	e.preventDefault(); e.stopImmediatePropagation();
	$(this).hide();
	$('#promoteProductModal').hide().html('').removeClass('noloader').removeClass('collection');

	if($(this).hasClass('ispopup')) {
		$(this).removeClass('ispopup');
	}
	else {
		$('body').removeClass('hidescroll');
	}
	
	if($('#productViewModal').css('display') == 'block'){
		$('body').addClass('hidescroll');
	}
});

$(document).on('click', '.promote-this-product', function(e){
	e.preventDefault(); e.stopImmediatePropagation();

	if(store.get('user_id') == 0 || typeof(store.get('user_id')) == "undefined" || typeof(store.get('token')) == "undefined") {
		signin_modal();
		if($(this).attr('ispopup')) {
			History.pushState(null, 'Shopstyx', '/');
		}
		return;
	}

	$('.reveal-modal-promoteproduct-bg').show();
	if($(this).attr('ispopup')) {
		$('.reveal-modal-promoteproduct-bg').addClass('ispopup')
	}

	$('body').addClass('hidescroll');
	$('#promoteProductModal').show().centerized();

	SSAjax({
		data: {isAjax:true,action:'ajax',action_type:'embedform'},
		url: $(this).attr('url'),
		success: function(response) {
			$('#promoteProductModal').html(response);
			$('#promoteProductModal').addClass('noloader');
		}
	});

	var data = $(this).attr('data');
	var mdata = $.parseJSON(data);
	$('#promoteProductModal').data('data',data);
});

$(document).on('click', '.promote-this', function (e) {
	e.preventDefault(); e.stopImmediatePropagation();

	if(store.get('user_id') == 0 || typeof(store.get('user_id')) == "undefined" || typeof(store.get('token')) == "undefined") {
		signin_modal();
		return;
	}

	$('.reveal-modal-promoteproduct-bg').show();
	$('body').addClass('hidescroll');
	$('#promoteProductModal').addClass('collection').show().centerized();

	SSAjax({
		data: {isAjax:true,action:'ajax',action_type:'embedform'},
		url: $(this).attr('url'),
		success: function(response) {
			$('#promoteProductModal').html(response);
			$('#promoteProductModal').addClass('noloader');
		}
	});

	var data = $(this).attr('data');
	//var mdata = $.parseJSON(data);
	//console.log(data)
	$('#promoteProductModal').data('data',data);
});

$(document).on('click', '.promote-product-view .p-right .share-wrapper .left-share .share-common', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	var data = {type:$(this).attr('type')};
	var mdata = decodeURIComponent($('#promoteProductModal').data('data').replace(/\+/g, ' '));
	mdata = $.parseJSON(mdata);
	data = $.extend(data,mdata);
	//console.log(data)
	promote_product(data);
});

$(document).on('click', '.promote-product-view .p-right .share-wrapper .share-mail', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	var data = {type:'mail'};
	console.log()
	var mdata = decodeURIComponent($('#promoteProductModal').data('data').replace(/\+/g, ' '));
	mdata = $.parseJSON(mdata);
	data = $.extend(data,mdata);
	//console.log(data)
	promote_product(data);
});

$(document).on('click', '.collection-edit-btn', function (e) {
	e.stopPropagation();
	$('.collection-info').addClass('hide');
	$('.collection-edit').removeClass('hide');
	$("html, body").animate({ scrollTop: 0 }, "fast");
});

$(document).on('click', '.cancel-edit-btn', function (e) {
	e.stopPropagation();
	$('.collection-edit').addClass('hide');
	$('.collection-info').removeClass('hide');
});

$(document).on('click', '.save-collection-data', function (e) {
	e.preventDefault(); e.stopImmediatePropagation();
	$(this).parents('form').submit();
});

$(document).on('submit', '#saveCollectionForm', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	var $form = $(this),
		url = $form.attr( 'action' );
		form_data = {};
	form_data.collection_name = $.trim($form.find( 'input[name="collection_name"]' ).val());
	form_data.collection_id = $.trim($form.find( 'input[name="collection_id"]' ).val());
	form_data.store_id = $.trim($form.find( 'input[name="store_id"]' ).val());
	form_data.description = $.trim($form.find( 'textarea[name="description"]' ).val());
	form_data.isAjax = true;
	$('.save-collection').html('Saving...');
	$('.save-collection').removeClass('bgcolor1');
	$('.save-collection').addClass('bgcolor4');
	$('.save-collection').addClass('.save-collection-btn-disabled');
	$('.save-collection').removeClass('.save-collection-data');
	SSAjax({
		data: form_data,
		url: url,
		success: function(response){
			var _data = $.parseJSON(response);
			if( _data.no_error){
				location.href = _data.url;
			}
		}
	});
	
	return false;
});

$(document).on('click', '.follow-this-collection', function (e) {
	e.preventDefault(); e.stopImmediatePropagation();
	var $form = $(this),
		url = root + 'collections';
		form_data = {};
	form_data.owner_id = $(this).attr('data-uid');
	form_data.store_id = $(this).attr('data-sid');
	form_data.collection_id = $(this).attr('data-cid');
	form_data.isAjax = true;
	form_data.action = 'follow';
	SSAjax({
		data: form_data,
		url: url,
		success: function(response){
			var _data = $.parseJSON(response);
			if( _data.no_error){
				location.href = location.href;
			}
		}
	});
});



$(window).resize(function() {
	$('#productViewModal').centerized();
	$('#productView').centerized();
	$('#promoteProductModal').centerized();
	$('#seeMoreModal').centerized();
	$('#startWizardModal').centerized();
	prod_resize();
});

/* Shop Settings */
$(document).on('click', '.save-global-shipping-btn', function (e) {
	e.preventDefault(); e.stopImmediatePropagation();
	$('.shopsettings .save-btn').trigger('click');
});

$(document).on('submit valid', '.shopsettings form', function (e) {
	e.preventDefault(); e.stopImmediatePropagation();
	save_shopsettings($(this));
}).on('invalid', function() {
	//alert('a');
	$('.form-submission-message').removeClass('ss-success').stop();
	$('.form-submission-message')
		.addClass('ss-error')
		.fadeIn(500);						
});

/* Follow/Unfollow user */
$(document).on('click', '.follow-wrapper li .top-wrapper .btn-follow, .product-listing.storepage-contributors .details .btn-follow', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	if(store.get('user_id') == 0 || typeof(store.get('user_id')) == "undefined") {
		signin_modal();
		return;
	}
					
	var username = $(this).attr('username');
	var type = $(this).attr('type');
	var uid = $(this).attr('uid');
	var acts = "u";
	if(type == "following") {
		$('#followingID-'+uid).remove();
	}
	else {
		if($(this).hasClass('unfollow')) {
			$(this).removeClass('unfollow').addClass('follow').html('follow');
		}
		else {
			acts = "f";
			$(this).removeClass('follow').addClass('unfollow').html('followed<span>unfollow</span>');
		}
	}
	//alert('asdasd: '+acts);

	SSAjax({
		data: {action:'ajax',action_type:'follow_user',followtype:type,uid:uid,cstate:acts},
		url: "/"+username,
		success: function(response){
			var data = $.parseJSON(response);
			if(data == "" || $.isEmptyObject(data)) return;

			var follower = (data.follower == 1) ? data.total_follower + ' follower' : data.total_follower + ' followers';

			$('.profile-header .inner .follow-link').eq(0).html(data.total_following + ' following');
			$('.profile-header .inner .follow-link').eq(1).html(follower);

			if(type == "following") {
				var text = (data.total_following == 1) ? data.total_following + ' person' : data.total_following + ' people';
				$('.list-collections .caption .total-people').html(text);
			}
		}
	});
});

$(document).on('click', '.profile-header .profile-message-btn', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	if(store.get('user_id') == 0 || typeof(store.get('user_id')) == "undefined") {
		signin_modal();
		return;
	}
	var uid = parseInt($(this).attr('uid'));
	openThisThread(uid,'chat',null);
});

$(document).on('click', '.u-prod-info-cont .bottom .add-this a', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	location.href = $(this).attr('href');
});

$(document).on('click', '.see-more-desc, .inside-tab-content', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	$('.more-desc').removeClass('hide');
});

$(document).on('click', 'html, .close-see-more', function(){
	$('.more-desc').addClass('hide');
});

// follow/unfollow shop
$(document).on('click', '.visit-follow-wrapper .follow-btn', function(e){
	e.preventDefault(); e.stopImmediatePropagation();

	var $this = $(this);
	var storeURL = $(this).attr('storeURL');
	var sid = parseInt($(this).attr('sid'));
	var suid = parseInt($(this).attr('suid'));
	if(store.get('user_id') == 0 || typeof(store.get('user_id')) == "undefined") {
		signin_modal();
		return;
	}

	if($(this).hasClass('follow')) {
		$(this).removeClass('follow').addClass('unfollow').html('followed<span>unfollow</span>');
		if($this.hasClass('follow-on-home')){
			var num = parseInt($(this).parents('.follow-wrapper').find('.followers-num .num').html());
			var follower = (num > 1)?'Followers':'Follower';
			num++;
		}
	}
	else {
		$(this).removeClass('unfollow').addClass('follow').html('follow<span>unfollow</span>');
		if($this.hasClass('follow-on-home')){
			var num = parseInt($(this).parents('.follow-wrapper').find('.followers-num .num').html());
			var follower = (num > 1)?'Followers':'Follower';
			if(num > 0){
				num--;
			}
		}
	}
	$('.followers-num').html('<span class="num">' + num + '</span> ' + follower );
	
	SSAjax({
		data: {action:'ajax',action_type:'follow','sid':sid,'suid':suid},
		url: "/"+storeURL,
		success: function(response){
			var data = $.parseJSON(response);
			if(data == "" || $.isEmptyObject(data)) return;
			// if($this.hasClass('follow-on-home')){
				// var follower = (parseInt(data.total_user_followed) > 1)?'Followers':'Follower';
				// $('.followers-num').html('<span class="num">' + data.total_user_followed + '</span> ' + follower );
			// }
		}
	});
});

// follow/unfollow collection
$(document).on('click', '.trend-box .follow-btn, .btn.btn-follow-collection, .sticked-more-collection .follow-box', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	if(store.get('user_id') == 0 || typeof(store.get('user_id')) == "undefined") {
		signin_modal();
		return;
	}
	var el = $(this);
	var data = $.parseJSON(el.attr('data'));
	var username = el.attr('username');

	// follow
	if(el.hasClass('follow')) {
		el.removeClass('follow');
		if(el.hasClass('btn-follow-collection')) {
			el.addClass('unfollow').html('followed' + '<span class="bgcolor1 color2">unfollow</span>');
		}
		else {
			el.addClass('unfollow').html('followed' + '<span>unfollow</span>');
		}
	}
	// unfollow
	else {
		el.removeClass('unfollow');
		if(el.hasClass('btn-follow-collection')) {
			el.addClass('follow').html('follow' + '<span class="bgcolor1 color2">unfollow</span>');
		}
		else {
			el.addClass('follow').html('follow' + '<span>unfollow</span>');
		}
	}

	$.extend(data,{action:'ajax',action_type:'follow_collection'});
	SSAjax({
		data: data,
		url: "/"+username,
		success: function(response){
			var data = $.parseJSON(response);
			if(data == "" || $.isEmptyObject(data)) return;
		}
	});
});


$(document).on('click', '.remove-prod-btn', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	
	var url = root + 'collections';
		form_data = {};
	form_data.uid = parseInt($(this).attr('uid'));
	form_data.csid = parseInt($(this).attr('csid'));
	form_data.cid = parseInt($(this).attr('cid'));
	form_data.pid = parseInt($(this).attr('pid'));
	form_data.isAjax = true;
	form_data.action = 'remove_product';
	$(this).removeClass('bgcolor1');
	$(this).addClass('bgcolor6');
	$(this).html('Removing...');
	SSAjax({
		data: form_data,
		url: url,
		success: function(response){
			//var _data = $.parseJSON(response);
			$('.prod-' + form_data.uid + form_data.pid ).parents('.plist-four').remove();
		}
	});
});

$(document).on('click', '.trend-box .follow-btn.edit, .collection-desc-wrapper .btn.edit', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	if(store.get('user_id') == 0 || typeof(store.get('user_id')) == "undefined") {
		signin_modal();
		return;
	}
	else {
		return location.href=$(this).attr('href');
	}
});
$(document).on('click', '.collection-details-link', function(e) {
	e.preventDefault(); e.stopImmediatePropagation();
	location.href = $(this).attr('data-url');
});

$(document).on('click', '.list-collections .two-list li a, .u-product a:not(.prodname)', function(e) {
	e.preventDefault(); e.stopImmediatePropagation();
	location.href = $(this).attr('href');
});
$(document).on('click', '.open-details', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	location.href = $(this).attr('data-url');
});

$(document).on('click', '.asq-btn', function(){
	$('.inline-q-chat').removeClass('hide');
});

$(document).on('click', '.inline-q-chat .send-msg', function(){
	
	if ($('#pinqmsg').val() != ''){

		var datalod = gatherReq();
		$.extend(datalod,{'product_id': $(this).attr('pid'),
						'store_id': $(this).attr('sid'),
						'store_user_id': $(this).attr('uid'),
						'collection_id': 0,
						'variant_id': 0});

		$.extend(datalod,{'target': $(this).attr('aid'),
						'message': $('#pinqmsg').val(),
						'ctype': 'product-chat'});

		//disabled all
		$('#pinqmsg').attr('disabled','disabled');
		$('.inline-q-chat .btns button').attr('disabled','disabled');
		$('.inline-q-chat').addClass('disablemode');
		$('.inline-q-chat .btns .shopstyxloader').empty().append($('<span>'));

		$.ajax({
			url: CS.main_url+'notifications/send_chat',
			type: 'POST',
			data: datalod,
			success: function(resp){
				$('.inline-q-chat .btns .shopstyxloader').empty().text('Done!');
				setTimeout(function(){
					$('#pinqmsg').val('');
					$('.inline-q-chat').removeClass('disablemode');
					$('#pinqmsg').removeAttr('disabled');
					$('.inline-q-chat .btns button').removeAttr('disabled');
					$('.inline-q-chat').addClass('hide');
				},3000);
			}
		});
	}
	
	return false;
});



$(document).on('click', '.q-cancel-btn', function(){
	$('.inline-q-chat').addClass('hide');
});

$(document).on('click', '.panel-tab', function(){
	if(!$(this).hasClass('details-tab')){
		$('.details-tab').removeClass('hide');
	}else{
		$(this).addClass('hide');
	}
});

$(document).on('click', '.see-product', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	var $this = $(this);
	var page = $this.attr('data-url');
	if($this.hasClass('active')){
		return false;
	}
	$('.see-product').removeClass('active');
	
	$('.loading-prod-img').css('backgroundImage','url(' + $(this).attr('data-image') + ')');
	$('.loading-prod-title').html($(this).attr('data-title'));
	$('.loading-prod-price').html('$' + $(this).attr('data-price'));
	
	$this.addClass('active');
	$('.see-product-details').html('');
	$('.pdetails-loadbox').removeClass('hide');
	$('.more-products-display .loadbox').removeClass('hide');
	$('.more-products-display .more-display-content').html('');
	History.pushState(null, 'Shopstyx', page);
	$.ajax({
		type: "POST",
		url: page,
		data: {single_product:true, user_id:store.get('user_id'), token: store.get('token')},
		cache: false,
		success: function(response){
			$('.see-product-details').html(response);
			$('.pdetails-loadbox').addClass('hide');
			$.ajax({
				type: "POST",
				url: page,
				data: {more_products:true, user_id:store.get('user_id'), token: store.get('token')},
				cache: false,
				success: function(datas){
					$('.more-products-display .more-display-content').html(datas);
					$('.more-products-display .loadbox').addClass('hide');
				}
			});
		}
	});
});

$(document).on('click', '.seemore', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	if(store.get('user_id') == 0 || typeof(store.get('user_id')) == "undefined" || typeof(store.get('token')) == "undefined") {
		signin_modal();
		return;
	}
	var $this = $(this);
	var show_img = $(this).attr('data-showing');
	$('#seeMoreModal').foundation('reveal', 'open', {
		animation_speed: 0
	});
	$('.product-seemore-modal .stick-wrapper').html('');
	$('.product-seemore-modal .stick-loader').removeClass('hide');
	$.ajax({
		type: "POST",
		url: $(this).attr('data-url'),
		data: {seemore:true, user_id:store.get('user_id'), token: store.get('token')},
		cache: false,
		success: function(response){
			$('.product-seemore-modal .stick-wrapper').html(response);
			$('.onpopup' + show_img).trigger('click');
			$('.tab-title').removeClass('active');
			$('.tcontent').removeClass("active");
			if($this.attr('data-tab') == 'info'){
				$('.tab-title:nth-child(1)').addClass("active");
				$('.tcontent:nth-child(1)').addClass("active");
			}
			if($this.attr('data-tab') == 'shipping'){
				$('.tab-title:nth-child(2)').addClass("active");
				$('.tcontent:nth-child(2)').addClass("active");
			}
			if($this.attr('data-tab') == 'policy'){
				$('.tab-title:nth-child(3)').addClass("active");
				$('.tcontent:nth-child(3)').addClass("active");
			}
			
			$('.product-seemore-modal .stick-loader').addClass('hide');
		}
	});
});



$(document).on('open', '#seeMoreModal, #startWizardModal', function (e) {
	$(this).centerized();
});

$(document).on('close', '#seeMoreModal, #startWizardModal', function () {
	$('body').removeClass('hidescroll');
	$('.product-seemore-modal .stick-wrapper').html('');
});

$(document).on("mouseenter click",".prod-left .nav span, .pcontent-details .nav span", function(e) {
	e.preventDefault(); e.stopImmediatePropagation();
	var $this = $(this);
	if($this.hasClass('active')){ return false; }
	var slide = $this.attr('data-id');
	
	if($this.hasClass('onpopup')){
		$('.preview-image .active-slide').fadeOut(100, function() {
			$(this).removeClass('active-slide');
			$('.show' + slide).fadeIn(50, function(){
				$(this).addClass('active-slide');
				$('.prod-left .nav span').removeClass('active');
				$this.addClass('active');
			});
		});
	}
	if(!$this.hasClass('onpopup')){
		$('.preview-image-wrapper .active-slide').fadeOut(100, function() {
			$(this).removeClass('active-slide');
			$('.show-img' + slide).fadeIn(50, function(){
				$(this).addClass('active-slide');
				$('.pcontent-details .nav span').removeClass('active');
				$this.addClass('active');
			});
		});
	}
	
});

$(document).on('click', '.prev-img-wrapper .next, .prev-img-wrapper .prev', function(e){
	if($('.onpopup').length > 0){
		var img_count = parseInt($('.prev-out-wrapper .nav .onpopup').length);
	}
	if($('.ondetails').length > 0){
		var img_count = parseInt($('.prev-out-wrapper .nav .ondetails').length);
	}
	var on_img = parseInt($('.prev-out-wrapper .nav .active').attr('data-id'));
	var pop;
	if($(this).hasClass('next')){
		pop = on_img + 1;
		if(on_img == img_count){ pop = 1; }
	}
	if($(this).hasClass('prev')){
		pop = on_img - 1;
		if(on_img <= 1){ pop = img_count; }
	}
	if($('.onpopup').length > 0){
		$('.onpopup' + pop).trigger('click');
	}
	if($('.ondetails').length > 0){
		$('.ondetails' + pop).trigger('click');
	}
});

$(document).on('click', '.shipping-btn', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	$('.more-desc').removeClass('hide');
	$('.tabs li').removeClass('active');
	$('.tabs-content section.content').removeClass('active');
	$('.shipping-content').addClass('active');
	$('.shipping-tab').addClass('active');
});

$(document).on('click', '.policy-btn', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	$('.more-desc').removeClass('hide');
	$('.tabs li').removeClass('active');
	$('.tabs-content section.content').removeClass('active');
	$('.policy-content').addClass('active');
	$('.policy-tab').addClass('active');
});

$(document).on('click', '.add-plus.circbtn', function(){
	$('#openShop').foundation('reveal', 'open', {
		animation_speed: 0
	});
});

/* wizard */
$(document).on('open', '#openShop', function () {
	$(this).centerized();
});
$(document).on('open', '#openShopExisting', function () {
	$(this).centerized();
});
$(document).on('open', '#addNewProdModal', function () {
	$(this).centerized();
});

/* hover card */

$(document).on('click', '.hover-card .message-btn', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	if(store.get('user_id') == 0 || typeof(store.get('user_id')) == "undefined") {
		signin_modal();
		return;
	}
	var uid = parseInt($(this).attr('uid'));
	openThisThread(uid,'chat',null);
});

$(document).on('click', '.hovercard-follow-btn', function(e){
	e.preventDefault(); e.stopImmediatePropagation();

	var uid = parseInt($(this).attr('uid'));
	var username = $(this).attr('username');
	if(store.get('user_id') == 0 || typeof(store.get('user_id')) == "undefined") {
		signin_modal();
		return;
	}
	
	if($(this).hasClass('follow')){
		$(this).parents('.hover-card').find('.followers-minus').val($(this).parents('.hover-card').find('.following').html());
		$(this).parents('.hover-card').find('.following').html($(this).parents('.hover-card').find('.followers-plus').val());
		$(this).html('- Unfollow').addClass('unfollow').removeClass('follow');
	}else{
		if($(this).hasClass('unfollow')){
			$(this).parents('.hover-card').find('.followers-plus').val($(this).parents('.hover-card').find('.following').html());
			$(this).parents('.hover-card').find('.following').html($(this).parents('.hover-card').find('.followers-minus').val());
			$(this).html('Follow').addClass('follow').removeClass('unfollow');
		}
	}
	
	SSAjax({
		data: {action:'ajax',action_type:'follow_user',followtype:'follower',uid:uid},
		url: "/"+username,
		success: function(response){ return; }
	});
});

$(document).on('click', '.hovercard-list-follow-btn', function(e){
	e.preventDefault(); e.stopImmediatePropagation();

	var uid = parseInt($(this).attr('uid'));
	var username = $(this).attr('username');
	if(store.get('user_id') == 0 || typeof(store.get('user_id')) == "undefined") {
		signin_modal();
		return;
	}
	$(this).parents('.hover-card-view').html('<div class="hover-card hover-card-pos-on-details hover-card-loader"><div class="arrow-down arrow-down-pos1"></div></div>');
	$('.load' + uid + ' .hover-card-view').html('<div class="hover-card hover-card-pos-on-details hover-card-loader"><div class="arrow-down arrow-down-pos1"></div></div>');
	
	SSAjax({
		data: {action:'ajax',action_type:'follow_user',followtype:'follower',uid:uid},
		url: "/"+username,
		success: function(response){
			SSAjax({
				data: {action:'ajax',action_type:'loadhovercard',uid:uid,arrow_position:'bottom'},
				url: "/"+username,
				success: function(response){
					$('.load' + uid + ' .hover-card-view').html(response);
				}
			});
		}
	});
});

$(document).on('mouseenter', '.u-sticked-pic-wrapper', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	var $this = $(this),
		uid = parseInt($this.attr('uid')),
		username = $this.attr('username');
	if(parseInt($this.attr('data')) == 1){
		return;
	}
	if(parseInt($this.attr('data')) == 0){
		$('.load'+uid).attr('data', '1');
	}
	SSAjax({
		data: {action:'ajax',action_type:'loadhovercard',uid:uid},
		url: "/"+username,
		success: function(response){
			$('.load' + uid + ' .hover-card-view').html(response);
		}
	});
});

/* wizard */

$(document).on('click', '.open-shop-wizard', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	if(store.get('user_id') == 0 || typeof(store.get('user_id')) == "undefined" || typeof(store.get('token')) == "undefined") {
		signin_modal();
		return;
	}
	
	$('#starterModal').hide();
	$('.welcome-box').hide();
	
	$('#startWizardModal').foundation('reveal', 'open', {
		animation_speed: 0
	});
	
	var $this = $(this),
		page = '/store_wizard/shop_info',
		step = parseInt($this.attr('data-step')),
		form_data = {};
	form_data.store_url = $this.attr('data-url');
	if($this.hasClass('new-shop')){
		form_data.new_shop = true;
	}
	$('.start-wizard-modal .wizard-wrapper').html('');
	$('.start-wizard-modal .wizard-loader').removeClass('hide');
	
	if( step == 2){
		page = '/store_wizard/basic_seller';
	}
	SSAjax({
		data: form_data,
		url: page,
		success: function(response){
			$('.start-wizard-modal .wizard-wrapper').html(response);
			$('.start-wizard-modal .wizard-loader').addClass('hide');
		}
	});
});

$(document).on("click",".shopinfo-btn", function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	$('.shopinfoform-btn').trigger('click');
});


$(document).on("submit",".wiz-content .ship-info-form form", function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	$('.shop-name-field .error2').html('');
	$('.shop-url-field .error2').html('');
	if(store.get('user_id') == 0 || typeof(store.get('user_id')) == "undefined") {
		signin_modal();
		return;
	}

	var el = $(this),
		store_name = $.trim(el.find('.store-name').val()),
		store_url = $.trim(el.find('.store-url').val());
		action = $.trim(el.find('.shopinfoform-action').val());
		current_domain = $.trim(el.find('.current-domain').val());
	$('#startWizardModal .wizard-loader').removeClass('hide');
	SSAjax({
		url: el.attr('action'),
		data: {store_name:store_name,store_url:store_url,current_domain:current_domain,action:action},
		success: function(response) {
			response = $.parseJSON(response);
			if($.isEmptyObject(response)) {
				signin_modal();
				return;
			}
			if(response.status == 'fail'){
				$('.shop-name-field .error2').html(response.errors.store_name);
				$('.shop-url-field .error2').html(response.errors.store_url);
				$('#startWizardModal .wizard-loader').addClass('hide');
			}
			if(response.status == 'success'){
					SSAjax({
					url: '/store_wizard/basic_seller',
					data: {},
					success: function(response) {
						$('.start-wizard-modal .wizard-wrapper').html(response);
						$('.start-wizard-modal .wizard-loader').addClass('hide');
					},
					error: function( jqXHR, textStatus, errorThrown) {
						if(textStatus) {
							signin_modal();
						}
					}
				});
			}
		},
		error: function( jqXHR, textStatus, errorThrown) {
			if(textStatus) {
				signin_modal();
			}
		}
	});
});

$(document).on('click', '.back-top-shop-info', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	if(store.get('user_id') == 0 || typeof(store.get('user_id')) == "undefined" || typeof(store.get('token')) == "undefined") {
		signin_modal();
		return;
	}

	var $this = $(this),
		page = '/store_wizard/shop_info',
		form_data = {};
		
	$('.start-wizard-modal .wizard-wrapper').html('');
	$('.start-wizard-modal .wizard-loader').removeClass('hide');

	SSAjax({
		data: form_data,
		url: page,
		success: function(response){
			$('.start-wizard-modal .wizard-wrapper').html(response);
			$('.start-wizard-modal .wizard-loader').addClass('hide');
		}
	});
});


$(document).on("click",".seller-info-btn", function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	$('.sellerinfoform-btn').trigger('click');
});


$(document).on("submit",".wiz-content .seller-info-form form", function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	$('.seller-state .error2').html('');
	$('.vendor-info-wrapper .error3').html('');
	if(store.get('user_id') == 0 || typeof(store.get('user_id')) == "undefined") {
		signin_modal();
		return;
	}

	var el = $(this),
		company_name = $.trim(el.find('.company-name').val()),
		country_code = $.trim(el.find('.country-code').val());
		city = $.trim(el.find('.city-town').val());
		address = $.trim(el.find('.street-address').val());
		state_name = $.trim(el.find('.state-name').val());
		state_code = $.trim(el.find('.state-code').val());
		zip_code = $.trim(el.find('.zip-code').val());
		action = $.trim(el.find('.basicsellerform-action').val());
	$('#startWizardModal .wizard-loader').removeClass('hide');
	SSAjax({
		url: el.attr('action'),
		data: {company_name:company_name,address:address,country_code:country_code,state_code:state_code,state_name:state_name,city:city,zip_code:zip_code,action:action},
		success: function(response) {
			response = $.parseJSON(response);
			if($.isEmptyObject(response)) {
				signin_modal();
				return;
			}
			if(response.status == 'fail'){
				
				if(country_code == "US" && state_code == "") {
                    $('.seller-state .error2').html(response.errors.state_code);
                } else if(country_code != "US" && state_name == "") {
                   $('.seller-state .error2').html(response.errors.state_name);
                }
				
				if(typeof response.errors.statecityzip != 'undefined'){
					$('.vendor-info-wrapper .error3').html(response.errors.statecityzip);
				}
				
				$('#startWizardModal .wizard-loader').addClass('hide');
				return false;
			}
			
			if(response.status == 'success'){
					SSAjax({
					url: '/store_wizard/display_shop_info',
					data: {},
					success: function(response) {
						$('.start-wizard-modal .wizard-wrapper').html(response);
						$('#startWizardModal .wizard-loader').addClass('hide');
					},
					error: function( jqXHR, textStatus, errorThrown) {
						if(textStatus) {
							signin_modal();
						}
					}
				});
			}
		},
		error: function( jqXHR, textStatus, errorThrown) {
			if(textStatus) {
				signin_modal();
			}
		}
	});
});

$(document).on("keyup",".wiz-content .ship-info-form input", function(e){
	$('.wiz-content .ship-info-form .error2').html('');
});

$(document).on("keyup",".wiz-content .vendor-info-wrapper input", function(e){
	$('.wiz-content .vendor-info-wrapper .error2').html('');
	$('.wiz-content .vendor-info-wrapper .error3').html('');
});

$(document).on("keyup keypress",".shop-name-field input", function(e){
	var val = $.trim($(this).val());
	var seo_url = val.toLowerCase().replace(/[^a-z0-9 -]/g,'').replace(/\s+/g,'-').replace(/-+/g, '-');
	$('.shop-url-field input').val(seo_url);
});

$(document).on("click", ".conversation-btn", function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	var data = $.parseJSON($(this).attr('data'));
	openThisThread(data.to,'order-chat',{order_id:data.order_id,
										cust_order_id:data.transaction_number,
										scust_order_id:data.order_number
									});
});

/* claim shop */
$(document).on('click', '#claim-shop', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	if(store.get('user_id') == 0 || typeof(store.get('user_id')) == "undefined" || typeof(store.get('token')) == "undefined") {
		signin_modal();
		if($(this).attr('ispopup')) {
			History.pushState(null, 'Shopstyx', '/');
		}
		return;
	}
	
	var $form = $(this),
		url = main_url + 'store_wizard/claim_shop_form',
		form_data = {};
	form_data.isAjax = true;
	form_data.store_id = $(this).attr('data-store');
	
	var data_attr = $.parseJSON($(this).attr('data'));
	$.extend(form_data,data_attr);
	
	$('#claimShopModal').removeClass('hide');
	$('#claimShopModal .stick-loader').removeClass('hide');
	$('#claimShopModal .claim-shop-form-wrapper').html('');
	SSAjax({
		data: form_data,
		url: url,
		success: function(data){
			$('#claimShopModal .claim-shop-form-wrapper').html(data);
			$('#claimShopModal .stick-loader').addClass('hide');
		}
	});
});

$(document).on('click', '.claim-now', function(e){
	e.preventDefault();
	$('.claim-btn').trigger('click');
});

$(document).on('click', '.claim-btn', function (e) {
	e.preventDefault(); e.stopImmediatePropagation();
	$(this).parents('form').submit();
});

$(document).on('keyup', '.claim-content input', function(){
	$('.error2').html('');
});

$(document).on('submit', '#claimShopForm', function(event){
	event.preventDefault();
	event.stopImmediatePropagation();
	$('.upload-error').html('');
	$('.error2').html('');
	$('#display_message').html('');
	var options = {
		target:        '#display_message',
		success:       claimResponse
	};
	
	
	var files = $('#upload-userfile')[0].files;
	var f = files[0];
	
	if(typeof(f) == 'undefined'){
		$('.upload-error').html('You did not select a file to upload.');
		return false;
	}
	
	var ext = f.name.split('.').pop().toLowerCase();
	
	if ($.inArray(ext, ['jpg', 'jpeg', 'png', 'doc', 'pdf']) == -1) {
		$('.upload-error').html('The filetype you are attempting to upload is not allowed.');
		return false;
	}
	
	if(parseInt(f.size) > 1000000){
		$('.upload-error').html('The file you are attempting to upload is larger than the permitted size.');
		return false;
	}
	
	$('.claim-shop-modal .stick-loader .shopstyxloader div').html('PROCESSING...');
	$('.claim-shop-modal .stick-loader').removeClass('hide');
	$(this).ajaxSubmit(options); 
	return false; 
});

/* account settings */

$(document).on('click', '.save-profile-btn', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	$(this).parents('form').submit();
	return false;
});

$(document).on('submit', '#editProfileForm', function(e) {
	e.preventDefault(); e.stopImmediatePropagation();
	if(store.get('user_id') == 0 || typeof(store.get('user_id')) == "undefined" || typeof(store.get('token')) == "undefined") {
		signin_modal();
		return;
	}
	
	var $form = $(this),
		form_data = {};
	form_data.isAjax = true;
	form_data.firstname = $.trim($form.find( 'input[name="profile_firstname"]' ).val());
	form_data.lastname = $.trim($form.find( 'input[name="profile_lastname"]' ).val());
	form_data.username = $.trim($form.find( 'input[name="profile_username"]' ).val());
	form_data.current_username = $.trim($form.find( 'input[name="current_username"]' ).val());
	form_data.email = $.trim($form.find( 'input[name="profile_email"]' ).val());
	form_data.current_email = $.trim($form.find( 'input[name="current_email"]' ).val());
	form_data.password = $.trim($form.find( 'input[name="profile_password"]' ).val());
	form_data.country = $.trim($form.find( 'select[name="profile_country_code"]' ).val());
	form_data.saveform = true;
	form_data.url = $form.attr( 'action' );
	$('.error2').html('');
	$('.accout-settings-wrapper .overlay').removeClass('hide');
	$('.accout-settings-wrapper .shopstyxloader div').html('SAVING...');
	$('.accout-settings-wrapper .shopstyxloader').removeClass('hide');
	SSAjax({
		data: form_data,
		url: form_data.url,
		success: function(response){
			var _data = $.parseJSON(response);
			if($.isEmptyObject(_data)) {
				$('.accout-settings-wrapper .overlay').addClass('hide');
				$('.accout-settings-wrapper .shopstyxloader').addClass('hide');
				$('.accout-settings-wrapper .shopstyxloader div').html('LOADING...');
				return;
			}
			if(_data.success){
				if(_data.new_username){
					location.href = root + _data.new_username + '/settings';
					return false;
				}
			}
			if(_data.firstname){
				$('.fname-error').html(_data.firstname);
			}
			if(_data.lastname){
				$('.lname-error').html(_data.lastname);
			}
			if(_data.username){
				$('.uname2-error').html(_data.username);
			}
			if(_data.email){
				$('.email-error').html(_data.email);
			}
			if(_data.password){
				$('.pass-error').html(_data.password);
			}
			
			
			$('.accout-settings-wrapper .overlay').addClass('hide');
			$('.accout-settings-wrapper .shopstyxloader').addClass('hide');
			$('.accout-settings-wrapper .shopstyxloader div').html('LOADING...');
		}
	});
	return false;
});

$(document).on('keyup', '.profile-username', function(){
	$(this).val(convertToSlug($(this).val()));
});
			
/* 	===================================
 *	Functions Section
 *	===================================
 */
 
function prod_resize(){
	var windowsize = $(window).width();
	if (windowsize <= 1000) {
		$('.pro-listing ul li').each(function(){
			$('.plist-three').removeClass("small-4 large-4");
			$('.plist-four').removeClass('small-6 large-3');
			$('.pro-listing ul li').removeClass('plist-three');
			$('.pro-listing ul li').removeClass('plist-four');
			$('.li-g-one').addClass('plist-three small-6 large-6');
			$('.li-g-two').addClass('plist-four small-4 large-4');
		});
	}
	if (windowsize > 1000) {
		$('.pro-listing ul li').each(function(){
			$('.li-g-one').removeClass('plist-three small-6 large-6');
			$('.li-g-two').removeClass('plist-four small-4 large-4');
			$('.li-g1').addClass('plist-three small-4 large-4');
			$('.li-g2').addClass('plist-four small-6 large-3');
		});
	}
}

function claimResponse(responseText, statusText, xhr, $form)  {
	$('.claim-shop-modal .stick-loader').addClass('hide');
	$('.claim-shop-modal .stick-loader .shopstyxloader div').html('LOADING...');
	var _data = $.parseJSON(responseText);
	if(!$.isEmptyObject(_data) && typeof _data.status != "undefined") {
		if(_data.status == 'fail'){
			if(_data.error_msg != ''){
				$('.upload-error').html(_data.error_msg);
			}
			if(typeof _data.errors != "undefined"){
				if(typeof _data.errors.state_name != "undefined" && _data.errors.state_name != ""){
					$('.state-error').html(_data.errors.state_name);
				}
				if(typeof _data.errors.zip_code != "undefined" && _data.errors.zip_code != ""){
					$('.zip-error').html(_data.errors.zip_code);
				}
				if(typeof _data.errors.statecityzip != "undefined" && _data.errors.statecityzip != ""){
					$('.city-error').html(_data.errors.statecityzip);
				}
			}
		}
		if(_data.status == 'success'){
			$('#claimShopForm').remove();
			$('.claim-header .claim-now').remove();
			$('.claim-content .done').removeClass('hide');
		}
		
	}
	
	return false;
}

function _claim_shop(input) {
	
	
	var $this = $(this),
		page = main_url + 'store_wizard/claim_shop_form',
		//file = input[0].files,
		file = $('.upload-userfile')[0].files,
		form_data = new FormData();
		//form_data = {};

	$.each(file, function(key, value)
	{
		form_data.append('userfile', value);
	});
	
	// $.ajax({
		// type: 'POST',
		// data: form_data,
		// cache: false,
		// processData: false,
		// contentType: false, 
		// url: "/admin/account?action=upload&isAjax=true&token="+store.get('token')+"&user_id="+store.get('user_id'),
		// success: function(response){
			// var results = $.parseJSON(response);

			// if(results.status == "success") {
				// $.get(results.file, function(contents){
					// $('.claim-shop-modal .stick-loader').addClass('hide');
				// });
			// }
		// }
	// });
	
	form_data.saveform = true;
	$('.claim-shop-modal .stick-loader').removeClass('hide');
	
	SSAjax({
		data: form_data,
		url: page,
		success: function(response){
			$('.claim-shop-modal .stick-loader').addClass('hide');
		}
	});
}

function _upload_userpic(input) {
	$('.upic-' + store.get('user_id') + ' .upic-loader-overlay').removeClass('hide');
	$('.upic-' + store.get('user_id') + ' .upic-loader').removeClass('hide');

	var file = input[0].files;
	var form_data = new FormData();

	$.each(file, function(key, value)
	{
		form_data.append('userfile', value);
	});
	
	$.ajax({
		type: 'POST',
		data: form_data,
		cache: false,
		processData: false,
		contentType: false, 
		url: "/admin/account?action=upload&isAjax=true&token="+store.get('token')+"&user_id="+store.get('user_id'),
		success: function(response){
			var results = $.parseJSON(response);

			if(results.status == "success") {
				$.get(results.file, function(contents){
					$('.upic-' + store.get('user_id') + ' .ipic-thumb').addClass('hide');
					$('.upic-' + store.get('user_id')).css('background-image','url('+results.file+')');
					$('#header .profile-img img').attr('src',results.file);
					$('.profile-menu .uload-logo .icon').css('background-image','url('+results.file+')');
					$('.userNav .navLink .profile-img').css('background-image','url('+results.file+')');
					$('.upic-' + store.get('user_id')).removeClass('image-hide');
					if($('.u-sticked-pic-wrapper').length > 0){
						if($('.u-image').length > 0){
							$('.load' + store.get('user_id') + ' .u-image').css('background-image','url('+results.file+')');
						}
						if($('.u-sticked-pic').length > 0){
							$('.load' + store.get('user_id') + ' .u-sticked-pic').css('background-image','url('+results.file+')');
							$('.load' + store.get('user_id') + ' .u-sticked-pic').removeClass('image-hide');
						}
					}
				});
			}
			$('.upic-' + store.get('user_id') + ' .upic-loader-overlay').addClass('hide');
			$('.upic-' + store.get('user_id') + ' .upic-loader').addClass('hide');
		}
	});
}

function _update_pic_display($this) {
	$('.upic-loader-overlay').removeClass('hide');
	$('.upic-loader').removeClass('hide');
	var form_data = {};
		form_data.user_picture_display = parseInt($this.attr('data'));
	
	$.ajax({
		url: "/admin/account?action=update_picture_display&isAjax=true&token="+store.get('token')+"&user_id="+store.get('user_id'),
		type:'post',
		data: form_data,
		success: function(response){
			var results = $.parseJSON(response);
			if(results.status == "success") {
				if(form_data.user_picture_display == 0){
					$('.ipic-thumb').removeClass('hide');
					$('.upic-' + store.get('user_id')).addClass('image-hide');
					$('.load' + store.get('user_id') + ' .u-sticked-pic').addClass('image-hide');
				}
				if(form_data.user_picture_display == 1){
					$('.ipic-thumb').addClass('hide');
					$('.upic-' + store.get('user_id')).removeClass('image-hide');
					$('.load' + store.get('user_id') + ' .u-sticked-pic').removeClass('image-hide');
				}
			}
			$('.upic-loader-overlay').addClass('hide');
			$('.upic-loader').addClass('hide');
		}
	});
}

function close_panels() {
	var el = $('#header .userNav');
		cart = el.find('.cart-panel'),
		wallet = el.find('.wallet-panel'),
		notification_panel = el.find('.notification-panel');

	el.find('.navLink').removeClass('selected');
	cart.find('.loader-overlay').addClass('hide');
	cart.find('.loader').addClass('hide');
	cart.addClass('hide');
	wallet.addClass('hide');
	notification_panel.addClass('hide');
	
	var el2 = $('#small-header-wrapper .userNav');
		cart2 = el2.find('.cart-panel'),
		wallet2 = el2.find('.wallet-panel'),
		notification_panel2 = el2.find('.notification-panel');

	el2.find('.navLink').removeClass('selected');
	cart2.find('.loader-overlay').addClass('hide');
	cart2.find('.loader').addClass('hide');
	cart2.addClass('hide');
	wallet2.addClass('hide');
	notification_panel2.addClass('hide');
}

function load_popup_cart_data(data) {
	SSAjax({
		url: '/admin/ajax?action=cart_popup',
		data: {cart_sess:store.get('cart_sess'), forpopup_cart: true, data: data},
		success: function(response){
			$('#addToCartPopup .atc-wrapper').html(response);
			$('.global-cart-loadbox').addClass('hide');
			
			var element = $('#sub-wrapper #header .userNav .navLink.cart'),
				container = element.find('.cart-panel.npops'),
				loader = container.find('.loader'),
				overlay = container.find('.loader-overlay'),	
				ajax_container = container.find('.inner-scroller');


			element.removeClass('active');
			overlay.addClass('hide');
			loader.addClass('hide');
			container.addClass('hide');
			
			var element2 = $('#small-header-wrapper .userNav .navLink.cart'),
				container2 = element2.find('.cart-panel.npops'),
				loader2 = container2.find('.loader'),
				overlay2 = container2.find('.loader-overlay'),	
				ajax_container2 = container2.find('.inner-scroller');
				
			element2.removeClass('active');
			overlay2.addClass('hide');
			loader2.addClass('hide');
			container2.addClass('hide');
		}
	});
}

function load_cart() {
	close_panels();
	
	var element = $('#sub-wrapper #header .userNav .navLink.cart'),
		container = element.find('.cart-panel.npops'),
		loader = container.find('.loader'),
		overlay = container.find('.loader-overlay'),	
		ajax_container = container.find('.inner-scroller');


	element.addClass('active');
	overlay.removeClass('hide');
	loader.removeClass('hide');
	container.removeClass('hide');
	
	var element2 = $('#small-header-wrapper .userNav .navLink.cart'),
		container2 = element2.find('.cart-panel.npops'),
		loader2 = container2.find('.loader'),
		overlay2 = container2.find('.loader-overlay'),	
		ajax_container2 = container2.find('.inner-scroller');
		
	element2.addClass('active');
	overlay2.removeClass('hide');
	loader2.removeClass('hide');
	container2.removeClass('hide');
	
	SSAjax({
		url: '/admin/ajax?action=cart',
		data: {cart_sess:store.get('cart_sess')},
		success: function(response){
			overlay.addClass('hide');
			loader.addClass('hide');
			
			overlay2.addClass('hide');
			loader2.addClass('hide');

			ajax_container.html(response);
			ajax_container2.html(response);
		}
	});
}

function close_cart() {

	var element = $('#sub-wrapper #header .userNav .navLink.cart'),
		container = element.find('.cart-panel.npops');
	
	var element2 = $('#small-header-wrapper .userNav .navLink.cart'),
		container2 = element2.find('.cart-panel.npops');

	container.addClass('hide');
	container2.addClass('hide');
}

function load_header_cart(type) {
	if(type == "loading") {
		var element = $('#sub-wrapper #header .userNav .navLink.cart'),
			container = element.find('.cart-panel.npops'),
			loader = container.find('.loader'),
			overlay = container.find('.loader-overlay');	

		element.addClass('active');
		overlay.removeClass('hide');
		loader.removeClass('hide');
		container.removeClass('hide');
	}
	else {
		load_cart();
	}
}

function get_cart_total_items() {
	var form_data = {};
	form_data.user_id = store.get('user_id');
	form_data.token = store.get('token');
	form_data.isAjax = true;
	form_data._Ssfp_ID = store.get('_Ssfp_ID');
	
	$.ajax({
		url: root + 'admin/ajax?action=get_cart_total_items',
		type:'post',
		data: form_data,
		success: function(data){
			var _data = $.parseJSON(data);
			if(_data.cart_total_item > 0) {
				$('#header .userNav .navLink.cart .bg .total-counter').html(_data.cart_total_item).removeClass('hide');
			}
		}
	});
}

function delete_cart_item(el) {
	var input = el.parent().find('input');

	var quantity = input.val();
	var m = input.attr('m');
	var p = input.attr('p');
	var variant_id = input.attr('variant_id');
	var su = input.attr('su');
	var s = input.attr('s');
	var has_options = 0;
	var product_options = input.attr('options');
	
	if(typeof(variant_id) != 'undefined' && variant_id > 0) {
		has_options = 1;
	}
	var data = {product_user_id:m,
				product_id:p, 
				store_id: s,
				store_user_id: su,
				quantity: quantity,
				has_options: has_options,
				variant_id: variant_id,
				product_options:product_options,
				cart_sess: store.get('cart_sess')
				};


	SSAjax({
			data: data,
			url: '/admin/ajax?action=cart&loadtype=updatecart&update=delete_item',
			success: function(response){
				//var response = $.parseJSON(response);
				load_cart();
			}
	});						
}

function load_wallet() {
	close_panels();
	
	var element = $('#sub-wrapper #header .userNav .navLink.wallet'),
		container = element.find('.wallet-panel.npops'),
		loader = container.find('.loader'),
		overlay = container.find('.loader-overlay'),	
		ajax_container = container.find('.inner-scroller');
		
	element.addClass('active');
	overlay.removeClass('hide');
	loader.removeClass('hide');
	container.removeClass('hide');
	
	var element2 = $('#small-header-wrapper .userNav .navLink.wallet'),
		container2 = element2.find('.wallet-panel.npops'),
		loader2 = container2.find('.loader'),
		overlay2 = container2.find('.loader-overlay'),	
		ajax_container2 = container2.find('.inner-scroller');
		
	element2.addClass('active');
	overlay2.removeClass('hide');
	loader2.removeClass('hide');
	container2.removeClass('hide');

	SSAjax({
		url: '/admin/ajax?action=wallet',
		data: {},
		success: function(response){
			overlay.addClass('hide');
			loader.addClass('hide');
			
			overlay2.addClass('hide');
			loader2.addClass('hide');

			ajax_container2.html(response);
			ajax_container.html(response);
		}
	});
}

function close_wallet() {
	var element = $('#sub-wrapper #header .userNav'),
		container = element.find('.wallet-panel.npops');

	var element2 = $('#small-header-wrapper .userNav'),
		container2 = element2.find('.wallet-panel.npops');

	container.addClass('hide');
	container2.addClass('hide');
}


// function load_wallet2(element) {
	// close_panels();

	// var element = $('.wallet-circbtn'),
		// container = element.find('.pop-wallet-panel.npops'),
		// loader = container.find('.loader'),
		// overlay = container.find('.loader-overlay'),	
		// ajax_container = container.find('.inner-scroller');


	// element.addClass('active');
	// overlay.removeClass('hide');
	// loader.removeClass('hide');
	// container.removeClass('hide');

	// SSAjax({
		// url: '/admin/ajax?action=wallet',
		// data: {},
		// success: function(response){
			// overlay.addClass('hide');
			// loader.addClass('hide');

			// ajax_container.html(response);
			// //updateWalletSizeContainer();
		// }
	// });
// }

// function close_wallet2() {
	// var container = element.find('.wallet-panel.npops');
	// container.addClass('hide');
// }



function load_message(element) {
	close_panels();

	var container = element.find('.message-panel'),
		loader = container.find('.loader'),
		overlay = container.find('.loader-overlay'),	
		closebtn = container.find('.close-btn'),
		h3 = element.find('h3'),
		ajax_container = container.find('.cart-panel-inner-scoller');

	element.addClass('selected');
	overlay.removeClass('hide');
	loader.removeClass('hide');

	overlay.addClass('hide');
	loader.addClass('hide');
	container.removeClass('hide');		
}

function updateCartSizeContainer(){
	class_or_id = '#sub-wrapper #header .userNav .cart-panel.npops .inner-scroller';
	parent_class_or_id = '#sub-wrapper  #header .userNav .cart-panel.npops .top-wrapper'; 
	var window_height = $('#sub-wrapper  #header .userNav .cart-panel.npops').height();
	window_height = 385;
	$(class_or_id).removeAttr('style');
	$(class_or_id + ' .slimScrollBar, ' + class_or_id +' .slimScrollRail').remove();
	if($(parent_class_or_id + ' .slimScrollDiv').length > 0) {
		$(class_or_id).parent().replaceWith($(class_or_id));
	}
	
	$(class_or_id).slimScroll({
		height: window_height,
		width: '100%',
		distance: '6px'
	});
}

function updateWalletSizeContainer() {
	class_or_id = '#header .userNav .wallet-panel .panel-inner-scroller';
	parent_class_or_id = '#header .userNav .wallet-panel'; 
	//var window_height = $('#header .userNav .wallet-panel').height();
	var window_height = $(window).height() - 60;

	$(class_or_id).removeAttr('style');
	$(class_or_id + ' .slimScrollBar, ' + class_or_id +' .slimScrollRail').remove();
	if($(parent_class_or_id + ' .slimScrollDiv').length > 0) {
		$(class_or_id).parent().replaceWith($(class_or_id));
	}

	$(class_or_id).slimScroll({
		height: window_height,
		width: 'auto',
		distance: '4px'
	});
}
function resizeProductDetails(){
	var wH = $(window).height(),
		mH = wH - 68;
		mH2 = wH - 60;
	$('.prod-details-wrapper').css({'height': mH + 'px'});
	$('.modal-content').css({'height': mH + 'px'});
	$('.blog-content').css({'height': mH + 'px'});
}
function updateSizeContainer(class_or_id, parent_class_or_id){
	var height = $('.modal-header').height(),
		window_height = $(window).height();
	window_height = window_height - height;
	
	$(class_or_id).removeAttr('style');
	$(class_or_id + ' .slimScrollBar, ' + class_or_id +' .slimScrollRail').remove();
	if($(parent_class_or_id + ' .slimScrollDiv').length > 0) {
		$(class_or_id).parent().replaceWith($(class_or_id));
	}
	
	$(class_or_id).slimScroll({
		height: window_height,
		width: '100%',
		distance: '6px'
	});
	$('.prod-details-wrapper').css({'height': window_height + 'px'});
}
 
function toggleScroll_leftmenu() {
	$("#leftmenu-sc").removeAttr('style');
	$('#leftmenu-sc #slimScrollBar, #leftmenu-sc #slimScrollRail').remove();
	if($('.left-submenu .slimScrollDiv').length > 0) {
		$("#leftmenu-sc").parent().replaceWith($("#leftmenu-sc"));
	}

	var lm_height = $('.left-submenu').height();

	if(lm_height-95 < $('#leftmenu-sc').height()) {
		$('#leftmenu-sc').slimScroll({
			height: lm_height-1
		});
	}

	return;
} 

function view_proddetails(element) {
	$('body').addClass('hidescroll');
	//$('#productViewModal').foundation('reveal', 'open');
	
	$('#productViewModal').foundation('reveal', 'open', {
		animation_speed: 0
	});
	
	var page = element.attr('data-url');
	$('.product-details-view .stick-loader').removeClass('hide');
	$('.product-details-view .product-info-wrapper').html('');
	var form_data = {};
	form_data.display_details = true;

	History.pushState(null, 'Shopstyx', page);
	SSAjax({
		data: form_data,
		url: page,
		success: function(response){
			$('.product-details-view .product-info-wrapper').html(response);
			$('.product-details-view .stick-loader').addClass('hide');
		}
	});
}

function addproduct(options) {
	var settings = $.extend(true,{
			"url": "/admin/addproduct?action=basic",
			"data": {product_id:0},
			"loadJS": [cloud_url+'js/v2/addproduct.js'
						
					],
			"loadCSS": [cloud_url+'css/v2/addproduct.css'],
			"loadType": "default"
			//"onSaved": function(response) {},
			//"beforeClose": funcion() { }
	},options);

	var onSaved, beforeClose;
	if(settings.hasOwnProperty('onSaved')) {
		onSaved = settings.onSaved;
	}

	if(settings.hasOwnProperty('beforeClose')) {
		beforeClose = settings.beforeClose;
	}

	if(settings.loadType == "default") {
		if(settings.data.product_id > 0 || store.get('addproduct_modal') == "advanced") {
			settings.url = "/admin/addproduct?action=advanced";
			$('#addproductModal').removeClass('basic').addClass('advanced');
		}
		else {
			settings.url = "/admin/addproduct?action=basic";
			$('#addproductModal').removeClass('advanced').addClass('basic');
		}

		$('#addproductModal .shopstyxloader').removeClass('hide');
		$('#addproductModal #modal-content').html("").hide();
		$('#wrapper').hide();
		if(settings.data.product_id > 0 || store.get('addproduct_modal') == "advanced") {
			var wH = $(window).height();
			//$('#addproductModal').css('height',wH);
			$('#addproductModal').css('left',0);
		}
		else {
			$('#addproductModal').centerized({});
		}
		$('#addproductModal').foundation('reveal', 'open');

	} else {
		$('#addproductModal-basic').show('slide', {direction: 'right'}, 400);
	}

	$('body').css('overflow','inherit');
	settings.success = function(response){

		if(settings.loadType == "default") {
			$('#addproductModal .overlay').addClass('hide');
			$('#addproductModal .shopstyxloader').addClass('hide');
			$('#addproductModal #modal-content').html(response).show();
			$('#wrapper').css('height',$('#addproductModal').height()+2);
			
			resize_containers();

		}
		else {
			$('#addproductModal-basic .overlay').addClass('hide');
			$('#addproductModal-basic .shopstyxloader').addClass('hide');
			$('#addproductModal-basic #modal-content').html(response).show();
		}	

		$('#addproductModal form').data('image_files',[]);
		$('#addproductModal form').data('callback','');
		$('#addproductModal form').data('beforeClose','');

		if(typeof onSaved == "function") {
			$('#addproductModal form').data('callback', onSaved);			
		}

		if(typeof beforeClose == "function") {
			$('#addproductModal form').data('beforeClose', beforeClose);			
		}
	}

	SSAjax(settings);
}

function stepRedirect(current_step,referer){
	referer = typeof referer == "undefined" ? false : referer;
	if(store.get('user_type')){
		var dpromote = store.get('promoter_data');
		if(typeof dpromote !== "undefined"){
			SSAjax({
				url: '/admin/build?action=save-products',
				data: {isAjax:true,isContent:true,promote_data:dpromote},
				success: function(response){
					var _data = $.parseJSON(response);
					if( _data.no_error){
						store.remove('promoter_data');
					}
				}
			});
		}
	}
	
	if(current_step == 1){
		History.pushState(null, 'shop_info', '/admin/build?page=shop_info');
		mainURL = '/admin/build?page=shop_info';
	}
	if(current_step == 2){
		History.pushState(null, 'catalog', '/admin/build?page=catalog');
		mainURL = '/admin/build?page=catalog';
	}
	if(current_step == 3){
		History.pushState(null, 'get_paid', '/admin/build?page=get_paid');
		mainURL = '/admin/build?page=get_paid';
	}
	if(current_step == 4){
		History.pushState(null, 'shop_preview', '/admin/build?page=shop_preview');
		mainURL = '/admin/build?page=shop_preview';
	}


	if(current_step > 4){
		if(referer) {
			History.pushState(null, 'Shopstyx', referer);
			mainURL = referer;
			location.href = referer;
			return false;
		}
		else {
			History.pushState(null, 'shop_info', '/admin/dashboard');
			mainURL = '/admin/dashboard';
			location.href = root + 'admin/dashboard';
			return false;
		}
	}
	SSAjax({
		url: mainURL,
		data: {isAjax:true,isContent:true},
		loadJS: [cloud_url+'js/v2/build.js'],
		loadCSS: [cloud_url+'css/v2/build.css'],
		success: function(response){
			$('#wrapper').html(response);
			hideLoader();
			return false;
		}
	});
}

function showLoader(){
	$('#wrapper').addClass('hide');
	$('#loadbox-wrapper').removeClass('hide');
}

function hideLoader(){
	$('#loadbox-wrapper').addClass('hide');
	$('#wrapper').removeClass('hide');
}

function showModalLoader(){
	$('.mod-loader-wrapper').show();
}

function hideModalLoader(){
	$('.mod-loader-wrapper').hide();
}

function convertToSlug(Text)
{
	return Text
        .toLowerCase()
        .replace(/ /g,'-')
        .replace(/[^\w-]+/g,'');
}

function loadDepartments(dataObj){
	var form_data = {};
	form_data.isAjax = true;
	$.extend(form_data,dataObj);
	if(typeof form_data.loadProducts !== 'undefined' ){ $('.product-loader').removeClass('hide'); }
	SSAjax({
		data: form_data,
		url: "/admin/departments?action=load",
		success: function(response){
			if(form_data.department_level == 'main'){
				$('#department-menus').html(response);
				$('#department-sub-menus').addClass('hide');
				$('#department-menus').removeClass('hide');
			}
			if(form_data.department_level == 'sub'){
				$('#department-sub-menus').html(response);
				$('#department-menus').addClass('hide');
				$('#department-sub-menus').removeClass('hide');
				$('.dept-title').html(form_data.department_title);
				$('.all-dept-lbl').addClass('hide');
				//$('.dept-spacing').removeClass('hide');
				$('#back-departments').attr('data-id', form_data.parent_id);
				$('#back-departments').attr('data-catname', form_data.department_title);
			}
			$('.department-loading').remove();
			if(typeof form_data.loadProducts !== 'undefined' ){
				loadDepartmentProducts({department_id: form_data.department_id, level: form_data.level});
			}
		}
	});
}

function loadDepartmentProducts(dataObj){
	var form_data = {},
		link;
	form_data.isAjax = true;
	$.extend(form_data,dataObj);
	if(typeof form_data.more !== 'undefined'){
		link = '/admin/departments?action=load-products&page=' + form_data.page_num;
	}else{
		$('.product-loader').removeClass('hide');
		link = '/admin/departments?action=load-products';
	}
	SSAjax({
		data: form_data,
		url: link,
		success: function(response){
			if(typeof form_data.more !== 'undefined'){
				$('.more-product').before(response);
				$('.loader-image').addClass('hide');
			}else{
				$('.shop-product-wrapper').html(response);
				$('.shop-product-wrapper').append('<p class="more-product bgcolor3"><span class="more-button color4"><span class="loader-image hide"></span>Load more...</span></p>');
				var total_page = $('#h-total_pages').val(),
					current_page = $('#h-current_page').val();
				if(total_page > current_page){ 
					$('.more-product').removeClass('hide'); 
				}else{
					$('.more-product').addClass('hide'); 
				}
				$('.product-loader').addClass('hide');
			}
			$('.shop-left').addClass('hide2');
		}
	});
}

function loadProducts(dataObj){
	var form_data = {},
		link;
	form_data.isAjax = true;
	$.extend(form_data,dataObj);
	SSAjax({
		data: form_data,
		url: '/admin/build?page=catalog&page_num=' + form_data.page_num,
		success: function(response){ 
			//console.log('response');
			$('.promoter-products').append(response);
			var total_page = $('#c-total_pages').val(),
				current_page = $('#c-current_page').val();
			if(total_page > current_page){ 
				$('.more-promoter-products').removeClass('hide'); 
			}else{
				$('.more-promoter-products').addClass('hide'); 
			}
			$('.loader-image').addClass('hide');
		}
	});
}

var customer_purchase_order = function() {
	var height = $(window).height();
	var modal = $('.main-modal.modal-order-details');
	var overlay = $('.main-modal .overlay');
	var loader = $('.main-modal .loader');

	this.toggle_loader = function() {
		if(loader.hasClass('hide')) {
			loader.removeClass('hide');
		}
		else {
			loader.addClass('hide');
		}
	}

	this.open = function() {
		//modal.find('.mmodal-inner').html('');
		//modal.css({'height': height});
		$('body').css({'overflow':'hidden'});
		modal.show('slide', {direction: 'right'}, 400);
	}

	this.close = function() {
		modal.find('.mmodal-inner').html('');
		$('body').css({'overflow':'inherit'});
		modal.hide('slide', {direction: 'right'}, 200);
	}

	this.resizeModal = function() {
		class_or_id = '.main-modal.modal-order-details .mmodal-inner .content .inner-scoller';
		parent_class_or_id = '.main-modal.modal-order-details .mmodal-inner .content'; 
		var window_height = modal.height() - 68;
		
		$(class_or_id).removeAttr('style');
		$(class_or_id + ' .slimScrollBar, ' + class_or_id +' .slimScrollRail').remove();
		if($(parent_class_or_id + ' .slimScrollDiv').length > 0) {
			$(class_or_id).parent().replaceWith($(class_or_id));
		}
		
		$(class_or_id).slimScroll({
			height: window_height,
			width: '100%',
			distance: '6px'
		});

		$(parent_class_or_id).height(window_height);

	}
};

function openModal(){
	var height = $(window).height();
	$('.dashboard-modal .mod-wrapper').css({'height': height});
	$('body').css({'overflow':'hidden'});
	$('.dashboard-modal').show('slide', {direction: 'right'}, 400);
}
function closeModal(){
	$('body').css({'overflow':'inherit'});
	$('.dashboard-modal').hide('slide', {direction: 'right'}, 200);
}

function openModal2(){
	$('.dashboard-modal').show('slide', {direction: 'right'}, 400);
}
function openModal3(){
	$('.blog-modal2').show('slide', {direction: 'right'}, 400);
}
function closeModal2(){
	$('.dashboard-modal').hide('slide', {direction: 'right'}, 200);
}
function closeModal3(){
	$('.blog-modal2').hide('slide', {direction: 'right'}, 200);
}

function openCustomModal(id_or_class){
	var height = $(window).height();
	$(id_or_class + ' .mod-wrapper').css({'height': height});
	$('body').css({'overflow':'hidden'});
	$(id_or_class).show('slide', {direction: 'right'}, 400);
}
function closeCustomModal(id_or_class){
	$('body').css({'overflow':'inherit'});
	$(id_or_class).hide('slide', {direction: 'right'}, 200);
}
function closeCustomModal2(id_or_class){
	$(id_or_class).hide('slide', {direction: 'right'}, 200);
}
/* dot pre-loader function 
	sample:
	<span id="text-pre-loader" class="text-pre-loader-style bgcolor1 color3">Saving<span class="dots"></span></span>
*/
var dots = 0;
$(document).ready(function()
{
    setInterval (type, 600);
});

function type()
{
    if(dots < 3)
    {
        $('.dots').append('.');
        dots++;
    }
    else
    {
        $('.dots').html('');
        dots = 0;
    }
}

function checkdate(value){
	var validformat=/^\d{4}\/\d{2}\/\d{2}$/;

	if (!validformat.test(value)) {
		return false;
	}
	else{ 
		var month = value.split("/")[0], day = value.split("/")[1], year = value.split("/")[2];
		var myDate = new Date(year, month, day);

		if ((myDate.getMonth()+1!=month) || 
			(myDate.getDate()!=dayfield) ||
			(myDate.getFullYear()!=yearfield)) {
			return false;
		}
		else {
			return true;
		}
	}
}

function shopsetting_form_params(field) {
	var radioCheck = /radio|checkbox/i;
	var data = {},
				current;
	var keyBreaker = /[^\[\]]+/g;			

	$(field).each(function(){
		var el = this,
			type = el.type && el.type.toLowerCase();
		var key = el.name;
		var parts = key.match(keyBreaker),
			value = $.data(el, "value") || $.fn.val.call([el]),
			isRadioCheck = radioCheck.test(el.type),
			write = !isRadioCheck || !! $(el).attr('checked'),
			lastPart;

		//console.log(parts);
		current = data;
		for ( var i = 0; i < parts.length - 1; i++ ) {
			if (!current[parts[i]] ) {
				current[parts[i]] = {};
			}
			current = current[parts[i]];

			//console.log(current);
		}

		lastPart = parts[parts.length - 1];
		if ( lastPart in current && type === "checkbox" ) {
			if (!$.isArray(current[lastPart]) ) {
				current[lastPart] = current[lastPart] === undefined ? [] : [current[lastPart]];
			}
			if ( write ) {
				current[lastPart].push(value);
			}
		} else if ( write || !current[lastPart] ) {
			current[lastPart] = write ? value : undefined;
		}
	});

	return data;
}

function shopsetting_shipping_form_params() {
	var radioCheck = /radio|checkbox/i;
	var data = {},
				current;
	var keyBreaker = /[^\[\]]+/g;			

	$('.shopsettings input[name^="shipping_global_meta"]').each(function(){
		var el = this,
			type = el.type && el.type.toLowerCase();
		var key = el.name;
		var parts = key.match(keyBreaker),
			value = $.data(el, "value") || $.fn.val.call([el]),
			isRadioCheck = radioCheck.test(el.type),
			write = !isRadioCheck || !! $(el).attr('checked'),
			lastPart;

		//console.log(parts);
		current = data;
		for ( var i = 0; i < parts.length - 1; i++ ) {
			if (!current[parts[i]] ) {
				current[parts[i]] = {};
			}
			current = current[parts[i]];

			//console.log(current);
		}

		lastPart = parts[parts.length - 1];
		if ( lastPart in current && type === "checkbox" ) {
			if (!$.isArray(current[lastPart]) ) {
				current[lastPart] = current[lastPart] === undefined ? [] : [current[lastPart]];
			}
			if ( write ) {
				current[lastPart].push(value);
			}
		} else if ( write || !current[lastPart] ) {
			current[lastPart] = write ? value : undefined;
		}
	});

	return data;
}

function save_shopsettings(form) {
	if($('.shopsettings .save-btn').html() == "Saving...") {
		return;
	}

	toggle_preloader_maincontent();
	$('#body-wrapper .overlay').eq(0).addClass('hide');
	$('#subcontent-wrapper').eq(0).removeClass('hide');

	$('.shopsettings .save-btn').html('Saving...');
	$('.shopsettings .save-global-shipping-btn').html('Saving...');
	$('.form-submission-message').stop().hide();

	var form_data = {action:'saveform',isAjax:true,body_wrapper:true,subcontent_wrapper:true};
	if(form.attr('name') != "shipping-settings-form") {
		form.serializeArray().map(function(x){
			form_data[x.name] = x.value;
		});
	}
	// for shipping form only
	else {
		$('.shopsettings table tr.add-country .error-shipping').addClass('hide');
		var shipping_data = shopsetting_shipping_form_params();
		$.extend(form_data,shipping_data);

		if($.isEmptyObject(shipping_data)) {
			$('.shopsettings tr.error-shipping').removeClass('hide');
			toggle_preloader_maincontent();
			$('.shopsettings .save-btn').html('Save Changes');
			$('.shopsettings .save-global-shipping-btn').html('Save Changes');


			$('.form-submission-message').removeClass('ss-success');
			$('.form-submission-message')
				.addClass('ss-error')
				.fadeIn(500, function() {
					$('.shopsettings .save-btn').html('Save Changes');
				});
			return;
		}
		else {
			$('.shopsettings tr.error-shipping').addClass('hide');
		}			
	}

	if(form.attr('name') == "tax-settings-form") {
		var tax_addedstate_length = $('.shopsettings .tax-wrapper li').not('.addstate-wrapper').find('.added-cityzip-wrapper div').length;

		if(tax_addedstate_length == 0) {
			/*
			toggle_preloader_maincontent();
			$('.form-submission-message').removeClass('ss-success');
			$('.shopsettings .tax-wrapper li.addstate-wrapper .common-field-wrapper').addClass('error').find('.error').html('Please add at least one state.');
			$('.form-submission-message')
				.addClass('ss-error')
				.fadeIn(500, function() {
					$('.shopsettings .save-btn').html('Saving Changes');
				});
			return;
			*/
		}
	}
	//$('.shopsettings form').attr('disabled','disabled').find('button.save-btn').attr('disabled','disabled');

	switch(form.attr('name')) {
		case 'general-settings-form': 
		case 'shipping-settings-form':
		case 'policy-settings-form': 
		case 'tax-settings-form':
			SSAjax({
				data: form_data,
				url: form.attr('action'),
				success:function(response) {
					var response = $.parseJSON(response);

					if(response.status != "success") {
						if(form.attr('name') != "shipping-settings-form") {
							for(e in response.errors) {
								if(e == "address") {
									form.find('textarea[name="'+e+'"]').parent().addClass('error').find('.error').html(response.errors[e]);
								} else {
									form.find('input[name="'+e+'"]').parent().addClass('error').find('.error').html(response.errors[e]);
								}
							}
						}
						
						$('.form-submission-message').removeClass('ss-success');
						$('.form-submission-message')
							.addClass('ss-error')
							.fadeIn(500, function() {
								$('.shopsettings .save-btn').html('Save Changes');;
							});
						
					}
					else {
						$('.form-submission-message').removeClass('ss-error');
						$('.form-submission-message')
							.addClass('ss-success')
							.fadeIn(500, function() {
								$('.shopsettings .save-btn').html('Save Changes');
							})
							.fadeOut(15000);

						if($('#header .userNav #claim-wallet-form').hasClass('hide') == false) {
							claim_walet_popup();
						}
					}

					toggle_preloader_maincontent();
					$('#body-wrapper .overlay').eq(0).addClass('hide');
					$('#subcontent-wrapper').eq(0).removeClass('hide');
				}
			});
			break;	
	}
}

function reloadPage(dataObj){
	var form_data = {};
	form_data.isAjax = true;
	$.extend(form_data,dataObj);
	$(form_data.preloader).show();
	SSAjax({
		data: form_data,
		url: form_data.link,
		success: function(response){
			$(form_data.wrapper).html(response);
			$(form_data.preloader).hide();
			$(form_data.preloader + ' .shopstyxloader div').html('LOADING...');
		}
	});
}

function toggle_statezip_autocomplete(opts) {
	opts = typeof opts == "undefined" ? {} : opts;
	var defaults = {enabled:true,
					state_code: '',
					state: 'select#state_code',
					city: 'input#city',
					zip: 'input#zip',
					};
	opts = $.extend(true,defaults,opts);
	if(opts.state_code == "") {
		if($(opts.state).val() == "" || $(opts.state).length == 0) {
			return;
		}
		else {
			opts.state_code = $(opts.state).val();
		}
	}

	if(!opts.enabled) {
		//$(opts.city).autocomplete( "option", "disabled", true );
		$(opts.zip).autocomplete( "option", "disabled", true );
	}
	else {
		//$(opts.city).autocomplete("enabled");
		$(opts.city).autocomplete({
			//define callback to format results  
			source: function(req, add){  
				//pass request to server  
				$.getJSON("/autocomplete/US_city.html?state_code="+opts.state_code+"&callback=?", req, function(data) {  
						//create array for response objects  
						var suggestions = [];  
						//process response  
						$.each(data, function(i, val){  
						suggestions.push(val.name);  
					});  

					//pass array to callback  
					add(suggestions);  
				});  
			}, 
			select: function(e, ui) {  
				//create formatted friend  
				var city = ui.item.value; 
				$.get("/autocomplete/US_zip.html?state_code="+opts.state_code+"&city="+city+"&callback=?", function(data) {
					data = data.replace('?','').replace('(','').replace(')','');
					data = $.parseJSON(data);
					//console.log(data);
					//console.log(data.length);
					if(data.length == 1) {
						$(opts.zip).val(data[0].name);	
					}
					else {
						$(opts.zip).val('');	
					}
				});
			},  
			//define select handler  
			change: function() {  
			}  
		}); 
		
		//$(opts.zip).autocomplete("enable");
		$(opts.zip).autocomplete({  
			//define callback to format results  
			source: function(req, add){  
				//pass request to server  
				$.getJSON("/autocomplete/US_zip.html?state_code="+opts.state_code+"&city="+$(opts.city).val()+"&callback=?", req, function(data) {  
						//create array for response objects  
						var suggestions = [];  
						//process response  
						$.each(data, function(i, val){  
						suggestions.push(val.name);  
					});  
					//pass array to callback  
					add(suggestions);  
				});  
			},  
			select: function(e, ui) {  
				//create formatted friend  
				var zip = ui.item.value; 
				$.get("/autocomplete/US_city.html?state_code="+opts.state_code+"&zip="+zip+"&callback=?", function(data) {
					data = data.replace('?','').replace('(','').replace(')','');
					data = $.parseJSON(data);
					//console.log(data);
					//console.log(data.length);
					if(data.length == 1) {
						$(opts.city).val(data[0].name);	
					}
					else {
						$(opts.city).val('');	
					}
				});
			}
		}); 
		
	}
}

function HomeLoader(){
	var user_id = store.get('user_id'),
		token = store.get('token'),
		login_data = {post_data:1,
						token:token,
						user_id:user_id
					};
	

	// check here if user is logged In
	$.post(root + "checkuser.html",login_data,function(data){
		var _data = $.parseJSON(data);
		if( _data.result){
			store.set('display_name', _data.display_name);
			store.set('user_id', _data.user_id);
		}
		else {
			store.remove('token');
			store.remove('user_id');
			store.remove('display_name');
			store.remove('remember');
			$('.sign-join-ontop').removeClass('hide');
			$('#wrapper').removeClass('hide');
			$('#loadbox-wrapper').addClass('hide');
		}


		$('#wrapper').html('');
		$('#loadbox-wrapper').removeClass('hide');
		var form_data = {};
		form_data.token = token;
		form_data.user_id = user_id;
		form_data.isAjax = true;
		form_data.isWrapper = true;
		$.post(location.href, form_data,function(response){
			$('#wrapper').html(response);
			$('.out-header').addClass('hide');
			$('#loadbox-wrapper').addClass('hide');
			$('#wrapper').removeClass('hide');
			$('#loadbox-wrapper').addClass('hide');
		});
	});	
}

var t = this;
var defdataopts, scrollopts, dataopts;

function init_scroll_opts(){
	if (!dataopts) dataopts = {};
	if (!scrollopts) scrollopts = {};
	defdataopts = {	user_id: store.get('user_id'),
		token: store.get('token'),
		isAjax: true,
		body_wrapper: false,
		subcontent_wrapper: false
		};
}

function load_scroll_opts(vartouse){
	dataopts[vartouse] = {};
	scrollopts[vartouse] = {
		page: 1,
		url: ''
	};
	reset_scroll_data_opts(vartouse);
}

function reset_scroll_page(vartouse){
	if(typeof scrollopts[vartouse] != "undefined") {
		scrollopts[vartouse].page = 1;	
	}
}

function reset_scroll_data_opts(vartouse){
	dataopts[vartouse] = defdataopts;
}

function extend_scroll_opts(dat,vartouse){
	$.extend(dataopts[vartouse], dat, true);
}

function infinite_scroll(scrolltarget, vstorage, params, urlrequest, whereinsert, iswindow){
	init_scroll_opts();
	load_scroll_opts(vstorage);
	scrollopts[vstorage].url = urlrequest;
	scrollopts[vstorage].iscontent_loaded = false;
	var hascontent = true;
	var iscontent_loaded = false;
	extend_scroll_opts(params,vstorage);
	var loadMoreContent = function() {
		var url;
		scrollopts[vstorage].page++;
		if(scrollopts[vstorage].url.indexOf("?") == -1) {
			url = scrollopts[vstorage].url+'?page='+scrollopts[vstorage].page;
		}
		else {
			url = scrollopts[vstorage].url+'&page='+scrollopts[vstorage].page
		}
		
		SSAjax({
			data: dataopts[vstorage],
			url: url,
			success: function(response){
				scrollopts[vstorage].iscontent_loaded = false;
				whereinsert = $(whereinsert.selector);
				/*
				if(typeof whereinsert.selector.indexOf('#') != "undefined" && whereinsert.selector.indexOf('#') != -1) {
					whereinsert = $(whereinsert.selector);
				}
				else {
					var ss = whereinsert.attr('class').split(' ');
					ss = "." + ss.join('.');
					whereinsert = $(ss);	
				}
				*/

							
				if ($.trim(response) != '') {
				  	whereinsert.append(response);
				}else{
					hascontent = false;
				}	
				whereinsert.parent().find('.infinite-scroll-loader').addClass('hide');
			}
		});
	}

	var trigger_scroll = function(){
		if (hascontent && typeof(scrollopts[vstorage]) != "undefined" && !scrollopts[vstorage].iscontent_loaded){
			whereinsert.parent().find('.infinite-scroll-loader').removeClass('hide');
			var stargheight = (iswindow)?$(document).height():$(scrolltarget)[0].scrollHeight;
	  		if (scrolltarget.scrollTop() + 300  >= stargheight - scrolltarget.height()) {
	  			//console.log('toload '+page);
	  			scrollopts[vstorage].iscontent_loaded = true;
	    		loadMoreContent();
	    		
	  		}
	  	}
	}
	
	scrolltarget.scroll(trigger_scroll);
}

function trending_search(){
	var form_data = {};
	form_data.user_id = store.get('user_id');
	form_data.token = store.get('token');
	form_data.isAjax = true;
	form_data._Ssfp_ID = store.get('_Ssfp_ID');
	
	$.ajax({
		url: root + 'stick_products?action=trending_search',
		type:'post',
		data: form_data,
		success: function(data){
			var _data = $.parseJSON(data);
			var limit = 11, start = 0;
			$('.trend-search-wrapper').css({'display':'block'});
			if(_data.recent_key_counter == 0 && _data.trending_prod_counter == 0){
				$('.trend-search-wrapper').css({'display':'none'});
			}
			$.each(_data.keywords, function(i,item){
				$('.r-search-txt').append('<li><a class="color1" href="#">'+item+'</a></li>');
			});
			
			if(_data.recent_key_counter > 0){ limit =  5; }
			if(_data.recent_key_counter == 0){ $('.recent-search').addClass('hide'); }
			
			$.each(_data.trending_prod, function(i,item){
				if(start <= limit){
					$('.trend-keys').append('<li class="small-3 large-2"> \
												<span class="trend-img" style="background-image: url(\''+ item.image +'\')"></span> \
												<span class="trend-title-box"> \
													<label class="color1">'+ item.keyword +'</label> \
												</span> \
											</li>');
				}
				start++;
			});
		}
	});
}

function popUp(URL, data) {
	var day = new Date();
	var id = day.getTime();
	data = "_social_sharing_";
	eval("page" + id + " = window.open(URL, '" + id + "', 'directories=0,toolbar=0,scrollbars=0,location=0,statusbar=0,menubar=0,resizable=0,width=1000,height=430,left=200,top=50');");
}

/*
 *
 */
function promote_product(data) {
	var opts = {type:'fb',
				url:'',
				text:'',
				desc:'',
				image:'',
				current_url:''
				};

	data = (typeof data == "undefined" || $.isEmptyObject(data)) ? {} : data;
	opts = $.extend(opts,data);
	if($.isEmptyObject(opts.desc) || opts.desc == "") {
		opts.desc = opts.text;
	}

	opts.url = encodeURIComponent(opts.url);
	opts.text = encodeURIComponent(opts.text);
	opts.image = encodeURIComponent(opts.image);
	opts.desc = encodeURIComponent(opts.desc);

	switch(opts.type) {
		default:
		case 'fb':
			url = 'https://www.facebook.com/dialog/share?app_id=142470092447374&display=popup&href='+opts.url+'&redirect_uri='+encodeURIComponent(main_url + "go/gotourl?r="+opts.current_url);
			break;
		case 'tw':
			url = 'http://twitter.com/share?count=false&original_referer='+opts.current_url+'&via=shopstyx.com&text='+opts.text+'&url='+opts.url;
			break;
		case 'gplus':
			url = 'https://plus.google.com/share?url='+opts.current_url;
			break;
		case 'linkedin':
			url = 'http://www.linkedin.com/shareArticle?mini=true&url='+opts.url+'&title='+opts.text+'&summary='+opts.desc+'&source=shopstyx.com';
			break;
		case 'pt':
			url = 'http://pinterest.com/pin/create/button/?url='+opts.url+'&media='+opts.image+'&description='+opts.desc;
			break;
		case 'mail':
			url = 'mailto:?subject='+opts.text+' @ Shopstyx&body=Check out this cool product '+opts.current_url;
			break;
	}
	popUp("/go?r="+escape(url)+'&u='+opts.uid+'&pu='+opts.pu+'&s='+opts.s+'&c='+opts.c+'&p='+opts.p);
}

function check_allowed_image_filetype(file_mime,file_ext) {
	file_ext = file_ext.toLowerCase();
	if($.inArray(file_mime,["images/jpeg","images/jpg","images/png","images/gif"]) == -1 &&
		$.inArray(file_mime,["jpeg","jpg","png","gif"]) == -1
		) {
		return false;
	}

	return true;
}

function _support_html5_uploadform(form, inputfile) {
	var files = false;
	if (inputfile.length > 0 && inputfile[0].files) {
		files = inputfile[0].files;
	} 

	if (window.FormData && files && files.length > 0) {
		var form_data = new FormData();
		$.each(files, function(key, value) {
			form_data.append('userfile', value);
		});

		return form_data;
	}

	return false;
}

function check_store(){
	var form_data = {};
	form_data.user_id = store.get('user_id');
	form_data.token = store.get('token');
	
	$.ajax({
		url: root + 'manage_claimed_store/check_store',
		type:'post',
		data: form_data,
		success: function(response){
			var _data = $.parseJSON(response)
			if(_data.success){
				if(_data.manage_shop == 0){
					$('#openShop .right.shop-box-option').addClass('open-shop-wizard').addClass('new-shop').html('<div class="bg"></div><label class="color1">Open a shop</label><div class="box"><span class="existing"></span></div>');
				}
				if(_data.manage_shop == 1){
					var currentUrl = location.href.replace('#','');
					$('#openShop .right.shop-box-option').html('<a href="' + root + 'manage_claimed_store?store=' + _data.store_name + '&referer=' + escape(currentUrl) + '"><div class="bg"></div><label class="color1">Upload new</label><div class="box"><span class="existing"></span></div></a>');
				}
				if(_data.manage_shop == 2){
					$('#openShop .right.shop-box-option').html('<a href="' + root + 'admin/dashboard"><div class="bg"></div><label class="color1">Upload new</label><div class="box"><span class="existing"></span></div></a>');
				}
			}
		}
	});
}

function show_add_prod_guide(){
	$('.guide-prod').css('opacity',0).removeClass('hide').animate({opacity: 1}, 300);
}

function hide_add_prod_guide(){
	$('.guide-prod').animate({opacity: 0}, 300, function(){
		$('.guide-prod').addClass('hide');
	});
}

function show_open_shop_guide(){
	$('.guide-openshop').css('opacity',0).removeClass('hide').animate({opacity: 1}, 300);
}

function hide_open_shop_guide(){
	$('.guide-openshop').animate({opacity: 0}, 300, function(){
		$('.guide-openshop').addClass('hide');
	});
}

