function dispute_timer_expiration() {
    var days        = Math.floor(dispute_remaining_seconds/24/60/60);
    var hoursLeft   = Math.floor((dispute_remaining_seconds) - (days*86400));
    var hours       = Math.floor(hoursLeft/3600);
    var minutesLeft = Math.floor((hoursLeft) - (hours*3600));
    var minutes     = Math.floor(minutesLeft/60);
    var remainingSeconds = dispute_remaining_seconds % 60;
        
    remainingSeconds = (remainingSeconds < 10) ? "0" + remainingSeconds : remainingSeconds; 
    minutes = (minutes < 10) ? "0" + minutes : minutes; 
    //console.log(days + ":" + hours + ":" + minutes + ":" + remainingSeconds);
    
    $('.dispute-timer ul li span.days').html(days);
    $('.dispute-timer ul li span.hrs').html(hours);
    $('.dispute-timer ul li span.mins').html(minutes);
    $('.dispute-timer ul li span.secs').html(remainingSeconds);

    if (dispute_remaining_seconds == 0) {
        clearInterval(dispute_countdownTimer);
        setTimeout(function(){view_purchase_order_details(shopstyx_transaction_id);},1000)
    } else {
        dispute_remaining_seconds--;
    }
}

function claim_walet_popup(type){
    $('#wallet-pop').removeClass('extens').foundation('reveal','open',{
                                                        animation_speed: 0,
                                                        close_on_background_click: false
                                                    });
    var ajax_container = $('#wallet-pop .cont'),
    loader = $('#wallet-pop .loadcont');
    loader.removeClass('hide'); 
    loader.find('.loader').removeClass('hide'); 
    ajax_container.html('').removeClass('hide');
    
    SSAjax({
        url: CS.main_url + 'admin/ajax?action=claim_wallet_form&type='+type,
        success: function(response){
            loader.addClass('hide');
            ajax_container.html(response);
        }
    });     
}

function wallet_paypalemail_popup(type) { 
    $('#wallet-pop').removeClass('extens');

    var ajax_container = $('#wallet-pop .cont'),
    loader = $('#wallet-pop .loadcont');
    loader.removeClass('hide'); 
    loader.find('.loader').removeClass('hide'); 
    ajax_container.html('').removeClass('hide');
    
    SSAjax({
        url: CS.main_url + '/admin/ajax?action=wallet_paypalemail_form&type='+type,
        success: function(response){
            loader.addClass('hide');
            ajax_container.html(response);
        }
    }); 
}

function save_promoter_paypalemail(type) {
    var email_field = $('.wallet-modal .paypal-form-wrapper .field input');
    var country_field = $('.wallet-modal .paypal-form-wrapper .field select');
    var email = $.trim(email_field.val());
    var country = country_field.val();
    var error_wrapper = $('.wallet-modal .paypal-form-wrapper .field div.error-paypal-email');
    var overlay = $('#wallet-pop .loader-overlay'),
        loader = $('#wallet-pop .loader');

    email_field.val(email);
    error_wrapper.addClass('hide').html('');

    if(email == "") {
        error_wrapper.removeClass('hide').html('Please provide a valid email.');
        email_field.focus();
        return;
    }

    if(country == "") {
        error_wrapper.removeClass('hide').html('Please select a country.');
        country_field.focus();
        return;
    }
    overlay.removeClass('hide');
    loader.removeClass('hide'); 

    SSAjax({
            data: {paypal_email:email, country:country},
            url: CS.main_url + '/admin/ajax?action=wallet_paypalemail_form&save_paypalemail=true&type='+type,
            success: function(response){
                var response = $.parseJSON(response);

                overlay.addClass('hide');
                loader.addClass('hide');

                if(response.status == "success") {
                    claim_walet_popup(type);
                }
                else {
                    error_wrapper.removeClass('hide').html(response.error);
                }
            }
        });


}

function claim_wallet(element,action_type) {
    var errorfield = $('#wallet-pop .cont .claim-form-wrapper .field-holder .error-wallet');
    var moneyonwallet = parseFloat(element.attr('moneyonwallet'));
    var min_amount = parseFloat(element.attr('min-amount'));
    var amount = parseFloat($('#claim-wallet-form .claim-form-wrapper input#claim-wallet-amount').val());
    var wtype = $('.wallet-modal #claim-wallet-form').attr('wtype');
    var password = $('#wallet-pop .cont .claimcofirmation-wrapper input').val(),
        errorpassword = $('#wallet-pop .cont .claimcofirmation-wrapper .error-password');
    //var loader = $('#wallet-pop .loadcont');
    var loader_overlay = $('#wallet-pop .loader-overlay'),
        loader = $('#wallet-pop .loader');


    errorfield.removeClass('hide');
    errorpassword.addClass('hide');

    //alert(amount + ' == ' + min_amount)

    if(amount < min_amount) {
        errorfield.html('Please enter an amount greater than or equal to ' + min_amount + '.');
        return;
    }
    else if(amount > moneyonwallet) {
        errorfield.html('The amount is greater than the remaining money on your wallet.');
        return;
    }

    errorfield.addClass('hide').html('');

    //loader.removeClass('hide');   
    //loader.find('.loader').removeClass('hide');
        
    loader_overlay.removeClass('hide'); 
    loader.removeClass('hide');

    SSAjax({
            data: {moneytoclaim:amount,type:wtype,action_type:action_type,password:password},
            url: CS.main_url + 'admin/ajax?action=claim_wallet',
            success: function(response){

                try{
                    var res = $.parseJSON(response);

                    if(action_type != "authenticate" && res.status == "success") {
                        $('.wallet-modal .claimcofirmation-wrapper').remove();
                        $('.claim-form-wrapper.claimconfirmation .claim-text-default').remove();
                        $('.claim-form-wrapper.claimconfirmation .claim-text-success').removeClass('hide');
                    }
                    else {
                        errorfield.html(res.message).removeClass('hide');
                        errorpassword.removeClass('hide');
                    }
                } catch(err) {
                    if(action_type == "authenticate") {
                        $('#wallet-pop .cont').html(response);
                        //loader.addClass('hide');  
                    }
                }

                //loader.addClass('hide');  
                loader_overlay.addClass('hide');    
                loader.addClass('hide');

                load_subscription_button();
            }
        });
    
}

function load_wallet_claimed_history(wtype) {
    $('#wallet-pop').removeClass('extens').foundation('reveal','open',{
                                                        animation_speed: 0,
                                                        close_on_background_click: false
                                                    });
    
    var ajax_container = $('#wallet-pop .cont'),
        loader_overlay = $('#wallet-pop .loader-overlay'),
        loader = $('#wallet-pop .loader');
        loader_overlay.removeClass('hide'); 
        loader.removeClass('hide'); 
        ajax_container.html('').removeClass('hide');

    SSAjax({
        url: CS.main_url + 'admin/ajax?action=wallet_claimed_history&wtype='+wtype,
        success: function(response){
            loader_overlay.addClass('hide');
            loader.addClass('hide');

            ajax_container.html(response);
            //updateWalletSizeContainer();
        }
    }); 
}

function load_wallet_purchase_history() {
    
    $('#wallet-pop').addClass('extens').foundation('reveal','open',{
                                                        animation_speed: 0,
                                                        close_on_background_click: false
                                                    });
    var ajax_container = $('#wallet-pop .cont'),
        loader = $('#wallet-pop .loadcont');
        loader.removeClass('hide'); 
        loader.find('.loader').removeClass('hide'); 
        ajax_container.html('').removeClass('hide');

    SSAjax({
        url: CS.main_url + 'admin/ajax?action=purchase_history',
        success: function(response){
            loader.addClass('hide');

            ajax_container.html(response);
            //updateWalletSizeContainer();
        }
    }); 


        return;
        
        if($.trim(ajax_container.html()) == "") {
            SSAjax({
                url: CS.main_url + 'admin/ajax?action=purchase_history',
                success: function(response){
                    loader.addClass('hide');

                    ajax_container.html(response);
                    //updateWalletSizeContainer();
                }
            });     
        }
        else {
            overlay.addClass('hide');
            loader.addClass('hide');

            container.removeClass('hide');
            updateWalletSizeContainer();
        }
}

function load_wallet_sales_history() {
    $('#wallet-pop').addClass('extens')
    .foundation('reveal','open',{
        animation_speed: 0,
        close_on_background_click: false
    });
    var ajax_container = $('#wallet-pop .cont'),
        loader = $('#wallet-pop .loadcont');
        loader.removeClass('hide'); 
        loader.find('.loader').removeClass('hide'); 
        ajax_container.html('').removeClass('hide');
        
        if($.trim(ajax_container.html()) == "") {
            SSAjax({
                url: CS.main_url + 'admin/ajax?action=sales_history',
                success: function(response){
                    loader.addClass('hide');
                    ajax_container.find('.loader').addClass('hide');
                    ajax_container.find('.loader-overlay').addClass('hide');

                    ajax_container.html(response);
                    //updateWalletSizeContainer();
                }
            });     
        }
        else {
            overlay.addClass('hide');
            loader.addClass('hide');

            container.removeClass('hide');
            updateWalletSizeContainer();
        }
}

function view_purchase_order_details(id){
    $('#wallet-pop').addClass('extens').foundation('reveal','open',{
                                                        animation_speed: 0,
                                                        close_on_background_click: false
                                                    });
    var ajax_container = $('#wallet-pop .cont'),
    loader = $('#wallet-pop .loadcont');
    loader.removeClass('hide'); 
    loader.find('.loader').removeClass('hide'); 
    ajax_container.html('').removeClass('hide');

    var cpos = $('#wallet-pop').offset();
    $(document).scrollTop(cpos.top);


    SSAjax({
        url: CS.main_url + 'admin/ajax?action=order_details',
        data: {order_id:id},
        success: function(response){
            loader.addClass('hide');
            ajax_container.html(response);
        },
        error: function(jqXHR, textStatus, errorThrown ) {
            if(jqXHR.status == 404) {
                $('#wallet-pop').removeClass('extens').foundation('reveal','close',{
                                                        animation_speed: 0
                                                    });
            }
        }
    });     
}

function confirm_transaction_order(id) {
    var loader = $('#wallet-pop .loadcont');
        loader.removeClass('hide'); 
        loader.find('.loader').removeClass('hide'); 
    
    SSAjax({
        url: CS.main_url + 'admin/ajax?action=confirm_order',
        data: {order_id:id},
        success: function(response){
            view_purchase_order_details(id);
            load_wallet();
        },
        error: function(jqXHR, textStatus, errorThrown ) {
            if(jqXHR.status == 404) {
                $('#wallet-pop').removeClass('extens').foundation('reveal','close',{
                                                        animation_speed: 0
                                                    });
            }
        }
    }); 
}

function open_dispute_order_product(dtype,id,pid,variant_id) {
    $('#wallet-pop').addClass('extens').foundation('reveal','open',{
                                                        animation_speed: 0,
                                                        close_on_background_click: false
                                                    });
    var ajax_container = $('#wallet-pop .cont .order-details-wrapper'),
    loader = $('#wallet-pop .loadcont');
    loader.removeClass('hide'); 
    loader.find('.loader').removeClass('hide'); 
    ajax_container.addClass('hide');

    var cpos = $('#wallet-pop.extens').offset();
    //$(document).scrollTop(cpos.top);

    SSAjax({
        url: CS.main_url + 'admin/ajax?action=open_order_dispute',
        data: {dtype:dtype,order_id:id,pid:pid,variant_id:variant_id},
        success: function(response){
            loader.addClass('hide');
            $('#wallet-pop .open-dispute-modal').html(response).removeClass('hide');
        },
        error: function(jqXHR, textStatus, errorThrown ) {
            if(jqXHR.status == 404) {
                $('#wallet-pop').removeClass('extens').foundation('reveal','close',{
                                                        animation_speed: 0
                                                    });
            }
        }
    }); 
}

function submit_order_dispute(form) {
    //#wallet-pop .open-dispute-modal form
    var refund_type = form.find('input[name="refund_type"]:checked').val(),
        payshipping = form.find('input[name="payshipping"]:checked').val(),
        details = form.find('textarea[name="dispute-details"]').val(),
        dispute_images = form.find('input[name="dispute_images"]'),
        form_data = {dispute_type:form.find('input[name="dispute_type"]').val(),
                    order_id:form.find('input[name="order_id"]').val(),
                    pid:form.find('input[name="pid"]').val(),
                    variant_id:form.find('input[name="variant_id"]').val(),
                    is_product:form.find('input[name="is_product"]').val(),
                    refund_type:refund_type,
                    payshipping:payshipping,
                    details:details
                    },
        max_upload = 3,
        limit_reached = false,
        error = new Array(), top = 0;

    form.find('.error-wapper').addClass('hide');

    if(typeof refund_type == "undefined" || refund_type == "") {
        error.push("How would you like to receive your refund?");
    } 

    if(typeof payshipping == "undefined" || payshipping == "") {
        error.push("Do you want to ship the goods back?");
    }

    if(typeof details == "undefined" || $.trim(details) == "") {
        error.push("Details of your request.");
    }

    if(typeof form.data('image_files') == "undefined" || form.data('image_files').length == 0) {
        error.push("Images of your evidence.");
    }


    if(error.length > 0) {
        error = error.join("<br />");
        form.find('.error-wapper .error-texts').html(error);
        form.find('.error-wapper').removeClass('hide');

        top = parseInt($('.open-dispute-modal .error-wapper').offset().top) - 50;
        $(document).scrollTop(top);
        return;
    }

    form.find('.error-wapper').addClass('hide');

    top = 0;
    $(document).scrollTop(top);


    var ovarlay = $('#wallet-pop .loader-overlay'),
        loader = $('#wallet-pop .loader');

    loader.html('submitting dispute request...').removeClass('hide'); 
    ovarlay.removeClass('hide');

    SSAjax({
        url: "/admin/ajax?action=save_dispute",
        data: form_data,
        success: function(response){
            var data = $.parseJSON(response);   
            var callback = form.data('callback');
            
            if(data.hasOwnProperty('success')) {
                if(data.success) {
                    upload_dispute_photos(form_data);
                    return;
                }
            }

            loader.addClass('hide').html('loading content...');
            ovarlay.addClass('hide');
        },
        error: function(jqXHR, textStatus, errorThrown ) {
            if(jqXHR.status == 404) {
                $('#wallet-pop').removeClass('extens').foundation('reveal','close',{
                                                        animation_speed: 0
                                                    });
            }
        }
    });

}

function upload_dispute_photos(data) {
    var form = $('#wallet-pop .open-dispute-modal form'),
        dispute_images = form.data('image_files'),
        max_upload = 3,
        limit_reached = false,
        error = new Array(),
        token, uid;

    var ovarlay = $('#wallet-pop .loader-overlay'),
        loader = $('#wallet-pop .loader');

    if(typeof CS.is_cookies == "undefined" || !CS.is_cookies) {
        uid = store.get('user_id');
        token = store.get('token');
    }
    else {
        uid = $.cookies.get("uid");
        token = $.cookies.get("token");    
    }    

    var url_query = "";
    for(var key in data){
        url_query += "&"+key+"="+data[key];
    }

    $.ajax({
        type: 'POST',
        dataType: 'json',
        cache: false,
        data: dispute_images[0],
        processData: false,
        contentType: "application/octet-stream", 
        headers: {
            "X-File-Name": encodeURIComponent(dispute_images[0].name),
            "X-Mime-Type" : dispute_images[0].type
        },
        url: "/admin/ajax?action=upload_dispute_photos&filename="+dispute_images[0].name+"&token="+token+"&user_id="+uid+url_query,
        complete: function(response) {
            //var results = $.parseJSON(response);
            dispute_images.shift();
            if(dispute_images.length == 0) {
                loader.addClass('hide').html('loading content...');
                ovarlay.addClass('hide');
                view_purchase_order_details(data.order_id);
            }
            else {
                upload_dispute_photos(data);
            }
        }
    });

}

function resolve_transaction_order(order_id,pid,variant_id) {
    var loader = $('#wallet-pop .loadcont');
        loader.removeClass('hide'); 
        loader.find('.loader').removeClass('hide'); 

    var form_data = {order_id:order_id,product_id:pid,variant_id:variant_id};

    SSAjax({
        data: form_data,
        url: "/admin/ajax?action=resolve",
        success: function(response){
            if(!response || response == null || $.isEmptyObject(response)) {
                //location.href = "/";
                return false;
            }   

            response = $.parseJSON(response);   
            if(!response.hasOwnProperty('status')){
                //location.href = "/";
                return false;
            }

            if(response.status == "Error") {
                //loader.addClass('hide');

                switch(response.error_code) {
                    case 100: // Braintree error transaction (payment)
                        alert(response.data.message);
                        break;
                    case 101: // No Products found
                        alert(response.data.message);
                        break;
                    case 102: // Critical error: did not update status to mongoDB ("PaymentSuccessful" or "Shipped")
                        alert(response.error_message);
                        break;  

                    default:    
                    case 1:
                        alert(response.error_message);
                        break;  
                }

                /*
                $('.mod-loader-wrapper').hide();
                $('.mod-loader-wrapper .shopstyxloader div').html('');
                */  

                view_purchase_order_details(order_id);
                load_wallet();
            }   
            else {
                view_purchase_order_details(order_id);
                load_wallet();
            }
        },
        error: function(jqXHR, textStatus, errorThrown ) {
            //location.href = "/";
        }
    });
}

