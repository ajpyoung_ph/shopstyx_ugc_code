var shopstyx_JS = new Array();

$.extend({
	parseJSON: function( data ) {
		if ( typeof data !== "string" || !data ) {
			return null;
		}
		data = jQuery.trim( data );
		if ( /^[\],:{}\s]*$/.test(data.replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g, "@")
			.replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, "]")
			.replace(/(?:^|:|,)(?:\s*\[)+/g, "")) ) {
			return window.JSON && window.JSON.parse ?
			window.JSON.parse( data ) :
			(new Function("return " + data))();
		}
	}
});

var productLoadStatus = 0,
	categoriesLoadStatus = 0,
	storecolorsLoadStatus = 0,
	storelogoLoadStatus = 0,
	webstorebgLoadStatus = 0,
	homepageLoadStatus = 0,
	otherpageLoadStatus = 0,
	shopStepCss = 0,
	loadShopCatalog = 0;
var msPopupId = '#popup', 
	msDialogId = '#dialog-box', 
	getAjaxCache = {},
	modalbox,
	vid = CS.vid,
	cloud_url = CS.cloudURL || location.protocol + '//' + location.host + '/';
	root = CS.siteURL || location.protocol + '//' + location.host + '/';


typeof jQuery !== "undefined" && jQuery( function( )
{

	modalbox = new ModalBox(); 
	
	
	$.getScript(cloud_url+'js/function/modalFunction.js', 
	function(data, textStatus){

		var site_URL = window.location.href,
			hash_action = site_URL.split('#')[1];
		
		if(hash_action == "uLogin") {
			LoginDialog();	
		}
		else if(hash_action == "uRegister") {
			RegisterDialog($('#register'));	
		}
		else if(hash_action == "subscription-sucess") {
			SubscriptionSucess();
		}
		else if(hash_action == "subscription-cancel") {
		}
	
		$(document.body).delegate('#login', 'click', function(event){ event.stopImmediatePropagation(); LoginDialog();});
		
		$(document.body).delegate('#loginform', 'submit', function(event){ event.preventDefault(); event.stopImmediatePropagation(); LoginForm($(this));});
		
		$(document.body).delegate('#register', 'click', function(event){ event.stopImmediatePropagation(); RegisterDialog($(this));});
		
		$(document.body).delegate('#lostpassword', 'click', function(event){ event.stopImmediatePropagation(); LostPasswordDialog();});
		
		$(document.body).delegate('#changepassword', 'click', function(event){ event.stopImmediatePropagation(); ChangePasswordDialog();});
		
		$(document.body).delegate('#registerform', 'submit', function(event){ event.preventDefault(); event.stopImmediatePropagation(); RegisterForm($(this));});
		
		$(document.body).delegate('#mainRegisterForm', 'submit', function(event){ event.preventDefault(); event.stopImmediatePropagation(); MainRegisterForm($(this));});
		
		$(document.body).delegate('#logout', 'click', function(event){ event.stopImmediatePropagation(); Logout();});
		
		$(document.body).delegate('.btn_cancel', 'click', function(){ modalbox.close(); });
		
		$(document.body).delegate('#forgotpasswordform', 'submit', function(event){ event.preventDefault(); event.stopImmediatePropagation(); ForgotPasswordForm($(this));});
		
		$(document.body).delegate('#changepasswordform', 'submit', function(event){ event.preventDefault(); event.stopImmediatePropagation(); ChangePasswordForm($(this));});
		
		$(document.body).delegate('#registerbtn', 'click', function(event){
			event.stopImmediatePropagation();
			if($('#r-firstname').val() == 'First Name'){
				$('#r-firstname').val('');
			}
			if($('#r-lastname').val() == 'Last Name'){
				$('#r-lastname').val('');
			}
			if($('#email').val() == 'Email'){
				$('#email').val('');
			}
			if($('#password').val() == 'Password'){
				$('#password').val('');
			}
			$('.error2').html('');
			$(this).parents("form").trigger("submit");
			return false;
		});
		
		$(document.body).delegate('#sign-up-button', 'click', function(event){
			event.stopImmediatePropagation();
			if($('#your-fname').val() == 'First Name'){
				$('#your-fname').val('');
			}
			if($('#your-lname').val() == 'Last Name'){
				$('#your-lname').val('');
			}
			if($('#your-email').val() == 'Email'){
				$('#your-email').val('');
			}
			if($('#your-password').val() == 'Password'){
				$('#your-password').val('');
			}
			$('.error2').html('');
			$(this).parents("form").trigger("submit");
			return false;
		});
	
	});


function ModalBox(settings){
		
		_self = this;
		this.dialogbox;
		this.messagebox;
		this.loadingImg = cloud_url + 'images/dialog-ajax-loader.gif';
		this.settings = {};
		this.params = {};
		this.getCache = {};
		this.defSettings = {width:400, height:'auto', resizable:false, show:'fade', modal:true, closeOnEscape:true, position:['center','center'], draggable:true,
			open: function (event, ui) { window.setTimeout(function () {
				jQuery(document).unbind('mousedown.dialog-overlay').unbind('mouseup.dialog-overlay'); }, 100);
			}
		
		};
		this.defparams = {html:'', title:'', hideCloseBtn:false, boxType:'normal', message:'', cache:true, displayGetContent:true, buttonLabel:'none'};
		
		this.close = function(){
			$(_self.dialogbox).html('').remove();
			return this;
		}
		
		function toggleCloseBtn(hide){
			if(hide)
				$('.ui-dialog-titlebar-close').hide();
			else
				$('.ui-dialog-titlebar-close').show();
		}
		
		this.dialog = function(params, settings){
			
			_self.close();
			
			var html = '', message, boxType, hideCloseBtn, buttonLabel;
			settings = settings || {};
			params = params || {};
			
			_self.params = $.extend({}, _self.defparams, params);
			_self.settings = $.extend({}, _self.defSettings, settings, {title:_self.params['title']});
			
			boxType = _self.params['boxType'];
			message = _self.params['message'];
			html = _self.params['html'];
			hideCloseBtn = _self.params['hideCloseBtn'];
			buttonLabel = _self.params['buttonLabel'];
	
			if(boxType != 'normal' && message != '')
				$(_self.messagebox).removeClass().addClass('sbMessage ' + boxType).html('<span class="sbStatIco"></span>'+message).appendTo(_self.dialogbox);
			
			$(_self.dialogbox).append('<div id="dialogbox-html">'+html+'</div>').appendTo('body').dialog(_self.settings);
			
			if(buttonLabel != 'none')
				$(_self.dialogbox).append('<div><input class="dialog-green-btn" id="modalbox-button" type="button" value="'+buttonLabel+'"></div>');
			
			if(hideCloseBtn == true)
				toggleCloseBtn(hideCloseBtn);		
			
			$('.ui-dialog-titlebar-close, #modalbox-button').bind('click', _self.close);
			
			return _self;
		}
		
		this.loading = function(params, settings){
			settings = $.extend({},{closeOnEscape:false},settings || {});
			params = $.extend({},{hideCloseBtn:true},params || {})
			params['html'] = '<div id="dialogbox-html"><div style="width: 100%; text-align: center;"><img class="img-loader"  src="' + _self.loadingImg +'" /></div></div>';
			
			_self.dialog(params, settings);
			
			return _self;
		}
		
		this.done = function(html, title){
			html = html || '';
			if(title != 'undefined')
				$('.ui-dialog-title').html(title);
				
			$('#dialogbox-html').html(html);
			toggleCloseBtn(false);
			
			return _self;
		}
		
		this.get = function(url, params, settings, callback, customParam){
			settings = settings || {};
			params = params || {};
			customParam = customParam || {}
			
				_self.loading(params, settings);
				$.get(url, function(data){
					
					
					if(_self.params['displayGetContent'] == true)	
						_self.done(data);
	
					if(typeof callback == 'string' && callback != '')
						eval(callback+'(data, customParam)');
					else if(typeof callback == 'function')
						callback(data, customParam);
						 
				});
			
			return _self;
		}
		
		function init(){
			settings = settings || {};
			_self.settings = $.extend({}, _self.defSettings, settings);		
			_self.dialogbox = $('<div id="dialog-box"></div>');
			_self.messagebox = $('<div id="sbMessage"></div>');
			$(document).ready(function(){
				$('<img class="img-loader" src="'+_self.loadingImg+'">').appendTo('body').hide();
			});
			return _self;	
		}
	
		return init();
	}

modalbox = new ModalBox(); 		
});

function csConfirm( title, question, callback, init, on_before_close, settings )
{
	var modal = typeof modalbox !== "undefined" ? modalbox : new ModalBox( );
	
	modal.dialog
	( {
		title:	title,
		html:	''
	}, settings || {
		width:	350
	} );
	
	var buttons = callback || {
		"Ok":	null,
		"No":	null
	};
	modal.dialogbox.append( question );
	$( '<br />' ).css( {clear: 'both', display: 'block'} ).appendTo( modal.dialogbox );
	var actions = $( '<p>' ).addClass( 'opt-btn-wrapper' ).appendTo( modal.dialogbox );
	
	$.each( buttons, function( x, y )
	{
		var button = $( '<input>' )
			.attr( {
				type:		'button',
				value:		x
			} )
			.addClass( 'opt-btn' )
			.css( {marginBottom: '10px', marginTop: '10px', marginRight: '10px'} );
		
		button.appendTo( actions );
	} );
	
	actions.find( 'input[type=button]' ).click( function( e )
	{
		if( typeof on_before_close == 'function' )
			if( on_before_close( ) === false )
				return;
		modal.close( );
		var response = $( this ).val( ),
			call = typeof callback[ response ] ? callback[ response ] : new Function( );

		call( );
	} );
	
	typeof init == 'function' && init( modal, modal.dialogbox );
};

function Loader(){
	$('.main-preloader').show();
	var user_id = store.get('user_id');
	if(typeof user_id !== 'undefined'){
		var token = store.get('token');
		var form_data = {
			postdata: 1,
			token: token,
			user_id: user_id
		};

		$.post(root + "checkuser.html",form_data,function(data){
			var _data = $.parseJSON(data);
			if( _data.result ){
				store.set('display_name', _data.firstname);
				store.set('user_id', _data.user_id);
				var form_login_data = {};
				form_login_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
				form_login_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
				
				$.post(root + "login-data.html", form_login_data, function(content){
					$('#mainpage-menutab').html(content);
				});
				
				$.post(root + "check-user-type.html", form_login_data, function(user_data){
					var user_data = $.parseJSON(user_data);
					if( user_data.user_type == ""){ $('.main-preloader').hide(); load_landing_page({page_link: root + 'no-store.html'});}
					if( user_data.user_type != ""){						
						$.post(root + "check-store-exist.html", form_login_data, function(store_data){
							if(user_data.user_type == "V") {
								if(! store_data){
									load_landing_page({page_link: root + 'no-store.html'});
									if(typeof store.get('loadShopOrStyx') === 'undefined'){
										store.set('loadShopOrStyx', 1);
										$('.your-store').trigger('click');
									}
								}else{
									store.set('store_id', store_data);
									load_landing_page({page_link: root + 'landing-page.html', home_type: 'vendor'});
								}
								
								check_premium_vendor();
							}
							if(user_data.user_type == "P") {
								if(! store_data){
									load_landing_page({page_link: root + 'no-store.html'});
									if(typeof store.get('loadShopOrStyx') === 'undefined'){
										store.set('loadShopOrStyx', 1);
										$('.your-store').trigger('click');
									}
								}else{
									store.set('store_id', store_data);
									load_landing_page({page_link: root + 'landing-page.html', home_type: 'promoter'});
								}
							}
						});
					}
				});
				

			}else{
				$.get(root + "logout-data.html",function(content){
					$('#mainpage-menutab').html(content);
					$('.main-preloader').hide();
				});
				$('#profilepage-wrapper').show();
			}
		});
	}else{
		$.get(root + "logout-data.html",function(content){
			$('#mainpage-menutab').html(content);
			$('.main-preloader').hide();
		});
		$('#profilepage-wrapper').show();
	}
}


var last_stamp = store.get('session_date');
var new_timestamp = new Date().getTime();

if(( typeof store.get('token') !== 'undefined' || store.get('token') != '' ) && (last_stamp !== 'undefined' || last_stamp != '')  && store.get('remember') == 0 ){
	if (new_timestamp - last_stamp > 120000) {
		autoLogout();
	}
}

if(( typeof store.get('token') !== 'undefined' || store.get('token') != '' ) && (last_stamp !== 'undefined' || last_stamp != '')  && store.get('remember') == 1 ){
	if (new_timestamp - last_stamp > 604800000) {
		autoLogout();
	}
}

var timestamp = new Date().getTime();
store.set('session_date', timestamp);

setInterval(function() {
	if( ( typeof store.get('token') !== 'undefined' || store.get('token') != '' ) && ( store.get('remember') == 0 || store.get('remember') == 1 ) ){
			 var timestamp = new Date().getTime();
			 store.set('session_date', timestamp);
	}
	 
}, 6000);

function autoLogout(){
	var url = root + 'signout.html';
	var form_data = {};
	form_data.post_dta = 1;
	form_data.temp_sessionid = store.get('temp_sessionid');
	form_data.token = store.get('token');
		
	$.ajax({
		type: "POST",
		url: url,
		data: form_data,
		cache: false,
		success: function(data){
			if(data == 'success'){
				store.clear();
				location.href = location.href;
			}
		}
	});
	return false;
}

function close_modal(modal_id){
	$("#store_modal").fadeOut(200);
	modal_id.css({"display":"none"})
}

function loadLoader(id_or_class){
	$('.loading_image').trigger('click');
	//$(id_or_class).html('<div id="dialog-box"><div style="width: 100%; text-align: center; margin: 80px 0;"><img class="img-loader" src="' + cloud_url + 'images/dialog-ajax-loader.gif" /></div></div>');
}

function closeLoader(){
	close_modal($('.store_loader'));
	$('#store_modal_loader').removeAttr('style');
}

function load_vendor_store_content(dataObj) {
	$('.main-preloader').show();
	var form_login_data = {};
	form_login_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
	form_login_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
	form_login_data.store_id = store.get('store_id');
	if(typeof dataObj.user_id !== "undefined" && typeof dataObj.store_id !== "undefined" ){
		form_login_data.user_id = dataObj.user_id;
		form_login_data.store_id = dataObj.store_id;
		form_login_data.page_tab = dataObj.page_tab;
		store.set('user_id', dataObj.user_id);
		store.set('store_id', dataObj.store_id);
	}
	$.post(root + "vendor.html", form_login_data, function(content){
		$('#profilepage-wrapper').html(content);
		$('#profilepage-wrapper').show();
		$('.main-preloader').hide();
	});
}

function load_promoter_store_content(dataObj) {
	$('.main-preloader').show();
	var form_login_data = {};
	form_login_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
	form_login_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
	form_login_data.store_id = store.get('store_id');
	if(typeof dataObj.user_id !== "undefined" && typeof dataObj.store_id !== "undefined" ){
		form_login_data.user_id = dataObj.user_id;
		form_login_data.store_id = dataObj.store_id;
		form_login_data.page_tab = dataObj.page_tab;
		store.set('user_id', dataObj.user_id);
		store.set('store_id', dataObj.store_id);
	}
	$.post(root + "promoter.html", form_login_data, function(content){
		$('#profilepage-wrapper').html(content);
		$('#profilepage-wrapper').show();
		$('.main-preloader').hide();
	});
}

function load_landing_page(dataObj) {
	loadCSS( cloud_url + "css/landingpagecss/nostorecss.css");
	var page; var wrapper; var loader;
	page = root + "no-store.html";
	loader = $('.main-preloader');
	wrapper = $('#profilepage-wrapper');
	loader.show();
	var form_data = {};
	form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
	form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
	form_data.store_id = store.get('store_id');
	$.extend(form_data,dataObj);
	
	if(typeof form_data.page_link !== "undefined"){
		page = form_data.page_link;
	}
	
	$.post(page, form_data,function(content){
		wrapper.html(content);
		wrapper.show();
		loader.hide();
	});
}


function checkFbStatus(){
	var form_data = {};
	var page; var wrapper; var loader;
	page = root + "check-fb-status.html";
	form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
	form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
	$.post(page, form_data,function(data){
		if(data != ''){
			$('.view_facebook_shop').trigger('click');
			$('.view_facebook_shop_wrapper .box-content').html(data);
			$.post(root + 'update-fb-status.html', form_data,function(result){ null; });
		}
	});
}

function check_premium_vendor() {
	var form_data = {};
	form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
	form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
	$.post('account.html?action=check_vendor', form_data,function(data){
		var _data = $.parseJSON(data);
		if(_data.status != ''){
			$.extend(form_data,_data);
			// If there are no API credentials available for subscribe merchant 
			// OR if the API provided by the merchant is not valid anymore
			if((_data.status == 'A' && _data.isfield_empty != false) || 
				(_data.status=='A' && _data.isfield_empty == false && _data.api_status == 'failure')) {
				$.post(root + 'account.html?action=api_form', form_data,function(content){ 
					$('.premium-vendor-wrapper .box-content').html(content);
					$('.premium-vendor').trigger('click');
				});
			}
			else if(_data.status == 'E') {
				
			}
		}
	});
}

loadCSS = function(href) {

  var cssLink = $("<link>");
  $("head").append(cssLink); //IE hack: append before setting href

  cssLink.attr({
    rel:  "stylesheet",
    type: "text/css",
    href: href
  });

};

// sample: loadPreloader('#profilepage-wrapper');
loadPreloader = function(id_or_class){
	var loader = '<div id="popup-preloader-wrapper" class="preloader-wrapper border-radius4" > \
						<div class="popup-preloader-holder border-radius4"> \
							<div id="popup-preloader-center" class="border-radius4"> \
								<div class="popup-preloader-storename color1"><strong>L o a d i n g</strong></div> \
								<div id="popup-fountainG"> \
									<div id="popup-fountainG_1" class="popup-fountainG"></div> <div id="popup-fountainG_2" class="popup-fountainG"> </div> <div id="popup-fountainG_3" class="popup-fountainG"> </div> <div id="popup-fountainG_4" class="popup-fountainG"> </div> <div id="popup-fountainG_5" class="popup-fountainG"> </div> <div id="popup-fountainG_6" class="popup-fountainG"> </div> <div id="popup-fountainG_7" class="popup-fountainG"> </div> <div id="popup-fountainG_8" class="popup-fountainG"> </div> \
								</div> \
							</div> \
						</div> \
					</div>';
	$(id_or_class).append(loader);
}
// sample: removePreloader();
removePreloader = function(){
	$('.preloader-wrapper').remove();
}