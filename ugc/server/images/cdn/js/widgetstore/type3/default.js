var	cloud_url = CS.cloudURL || location.protocol + '//' + location.host + '/';
var	root = CS.siteURL || location.protocol + '//' + location.host + '/';

$(document).ready(function(){
	$('.wdgt-logo').textfill({innerTag:'label'});
	
	$(document).on('click', '.wdgt-shop-now-btn, .shop-now-wrapper', function(){
		$('#wdgt-splash-wrapper').addClass('hide');
		//$('#wdgt-splash-category-display').parent('.slimScrollDiv').addClass('hide');
		//$('#wdgt-splash-category-display').addClass('hide');
		$('#wdgt-normal-category-display').removeClass('hide');
		$('#wdgt-home').removeClass('hide');
		$('#wdgt-splash-category-display').removeClass('hide');
		$('#wdgt-splash-category-display').parent('.slimScrollDiv').removeClass('hide');
		$('.wdgt-logo label').addClass('backtomain');
		// $('#wdgt-main-left').slimScroll({
			// height: '227px',
			// width: '91px',
			// start: 'top',
			// wheelStep : 2,
			// disableFadeOut: true
		// });
		if($('#enable_back_categories').val() == 0){
			$('.show_product').trigger('click');
		}
		if($('#enable_back_categories').val() == 1){
			$('#wdgt-splash-category-display').slimScroll({
				height: '227px',
				width: '336px',
				start: 'center',
				wheelStep : 2,
				disableFadeOut: true
			});
		}
	});
	
	$(document).on('click', '.show_subcat', function(){
		$('.' + $(this).attr('maincat')).removeClass('hide');
	});
	
	$(document).on('click', '.close-popup-btn', function(){
		$(this).parents('.sub-category-wrapper').addClass('hide');
	});
	
	$(document).on('click', '.backtomain', function(){
		$('#wdgt-splash-category-display').parent('.slimScrollDiv').removeClass('hide');
		$('#wdgt-splash-category-display').removeClass('hide');
		$('#wdgt-normal-category-display').addClass('hide');
	});
	
	$(document).on('click', '.show_product, .wdgt_prod_page, .show_subcat_product', function(){
		$('.sub-category-wrapper').addClass('hide');
		$('#wdgt-splash-category-display').parent('.slimScrollDiv').addClass('hide');
		$('#wdgt-splash-category-display').addClass('hide');
		$('#wdgt-normal-category-display').removeClass('hide');
		
		if($(this).attr('id') == "splashcat"){
			$('#wdgt-main-left').slimScroll({
				height: '227px',
				width: '91px',
				start: $('#start_' + $(this).attr('cid')),
				wheelStep : 2,
				disableFadeOut: true
			});
		}
		
		$('#preloader-wrapper').hide();
		$('.wdgt-main-wrapper').addClass('hide');
		$('.wdgt-show-product').removeClass('hide');
		var page = $(this).attr('page');
		var form_data = {};
		form_data.key = $('#key').val();
		form_data.user_id = $('#ms_uid').val();
		form_data.store_id = $('#ms_sid').val();
		form_data.store_category_id = $(this).attr('cid');
		form_data.widget_type = $('#widget_type').val();
		form_data.home_type = $('#home_type').val();
		form_data.enable_back_categories = $('#enable_back_categories').val();
		$('#mcat_' + $(this).attr('cid')).addClass('active');
		
		var category_name = $(this).attr('catname');
		if(typeof category_name !== "undefined"){
			form_data.category_name = category_name;
		}else{
			form_data.category_name = $('#category_name').val();
		}
		if(typeof $(this).attr('parent') !== "undefined"){
			form_data.parent_category_name = $(this).attr('parent');
		}
		var page;
		if($(this).attr('id') == "page_link"){
			page = $(this).attr('href');
			form_data.store_category_id = $('#store_category_id').val();
		}else{
			page = root + "widget-store-product-listing.html";
		}
		preLoader($('#ms-wrapper'));
		$.post(page, form_data,function(data){ 
			$('.wdgt-show-product').html(data);
			$('#preloader-wrapper').hide();
		});
		return false;
	});
	
	
	$(document).on('click', '.main_cat_menu', function(){
		$('.main_cat_menu').removeClass('active');
		$(this).addClass('active');
	});
	
	$(document).on('click', '.wdgt-search-btn', function(){
		if(! $('.wdgt-search-wrapper').hasClass('present')){
		$('.wdgt-cart-wrapper').after('<div class="wdgt-search-wrapper present bg_color2 body_color">'
									+	'<span class="wdgt-search-lbl body_color">Search</span>'
									+	'<input type="text" class="wdgt-search border-color1 body_background color2" name="search" value="" />'
									+	'<button class="wdt-search-button bg_color1"></button>'
									+	'<a href="javascript:;" class="wdgt-search-cancel-link color1">Cancel</a>'
									+ '</div>');
		}
	});
	$(document).on('click', '.wdgt-search-cancel-link', function(){
		$('.wdgt-search-wrapper').remove();
	});
	
	$(document).on('click', '.wdgt-more-info-btn, .wdgt-more-info, .wdgt-related-btn, .wdgt-prev-next', function(){
		$('#wdgt-popup').css({'z-index': 20});
		var form_data = {};
		form_data.key = $('#key').val();
		form_data.cat_user_id = $('#ms_uid').val();
		form_data.store_id = $('#ms_sid').val();
		form_data.store_category_id = $('#store_category_id').val();
		form_data.product_id = $(this).attr('pid');
		form_data.user_id = $(this).attr('puid');
		form_data.widget_type = $('#widget_type').val();
		form_data.home_type = $('#home_type').val();
		preLoader($('#ms-wrapper'));
		$.post(root + "widget-store/product-details.html", form_data,function(data){ 
			$('#wdgt-slideshow-wrapper').addClass('hide');
			$('#wdgt-home').removeClass('hide');
			$('.mainfooter').css({'z-index': 0});
			$('#wdgt-popup').html(data);
			get_cart_items({ key: $('#key').val(),
					user_id: $('#ms_uid').val(),
					store_id: $('#ms_sid').val(),
					widget_type: $('#widget_type').val()});
			$('#preloader-wrapper').hide();
		});
	});
	
	$(document).on('click', '.wdgt-entire-popup', function(){
		$('#wdgt-popup').html('');
		$('#wdgt-popup').css({'z-index': 0});
	});
	
	$(document).on('click', '.wdgt-slideshow-popup', function(){
		$('#wdgt-slideshow-wrapper').removeClass('hide');
		$('#wdgt-home').addClass('hide');
		$('.mainfooter').css({'z-index': 99});
	});
	
	$(document).on('click', '.wdgt-pmenu', function(){
		$('#wdgt-popup').removeClass('wdgt-video-wrapper');
		$('.show-details').removeClass('hide');
		$('.wdgt-popup-right-content').addClass('hide');
		$('.wdgt-new-tab').removeClass('hide');
		if($(this).attr('id') == 'show-related'){
			$('.wdgt-new-tab').addClass('hide');
		}
		if($(this).attr('id') == 'show-video'){
			$('.show-details').addClass('hide');
			$('#wdgt-popup').addClass('wdgt-level2-wrapper');
			$('.show-video').removeClass('hide');
			return false;
		}
		
		if($(this).attr('id') == 'show-images'){
			$('.show-details').addClass('hide');
			$('#wdgt-popup').addClass('wdgt-level2-wrapper');
			$('.show-images-zoom').removeClass('hide');
			$('#slider1').tinycarousel({ pager: true });
			return false;
		}
		$('.wdgt-show').removeClass('active');
		$(this).parent('li').addClass('active');
		$('.' + $(this).attr('id')).removeClass('hide');
		$('#re_open').val($(this).attr('id'));
	});

	$(document).on('click', '.wdgt-level2-popup-close', function(){
		$('.show-video').addClass('hide');
		$('.show-images-zoom').addClass('hide');
		$('.show-details').removeClass('hide');
		var re_open = $('#re_open').val();
		if(re_open != ""){
			$('.' + re_open).removeClass('hide');
			if(re_open == 'show-related'){
				//$('.wdgt-buynow-wrapper').addClass('hide');
			}
		}
		$('#wdgt-popup').removeClass('wdgt-level2-wrapper');
	});
	
	$(document).on('keypress', '.wdgt-search', function(e){
		if (e.which == 13) {
			$('#preloader-wrapper').hide();
			$('.wdgt-entire-popup').trigger('click');
			$('.wdgt-main-wrapper').addClass('hide');
			$('.wdgt-show-product').removeClass('hide');
			$('.main_cat_menu').removeClass('active');
			
			$(this).removeClass('input_error');
			var error_status = 0;
			var form_data = {};
				form_data.key = $('#key').val();
				form_data.user_id = $('#ms_uid').val();
				form_data.store_id = $('#ms_sid').val();
				form_data.wdgt_search_text = trimText($(this).val());
			if(form_data.wdgt_search_text == ""){
				$(this).addClass('input_error');
				error_status++;
				return false;
			}
			
			$('#wdgt-splash-wrapper').addClass('hide');
			$('#wdgt-splash-category-display').addClass('hide');
			$('#wdgt-splash-category-display').parent('.slimScrollDiv').addClass('hide');
			$('#wdgt-normal-category-display').removeClass('hide');
			$('#wdgt-home').removeClass('hide');
			form_data.widget_type = $('#widget_type').val();
			form_data.home_type = $('#home_type').val();
			store.set('wdgt_search_text', form_data.wdgt_search_text);
			
			preLoader($('#ms-wrapper'));
			$.post(root + 'widget-store-search.html', form_data,function(data){ 
				$('.wdgt-show-product').html(data);
				$('.wdgt-search-wrapper').remove();
				$('#preloader-wrapper').hide();
			});
			
			return false;
		}
	});
	
	$(document).on('click', '.wdt-search-button', function(){
			$('#preloader-wrapper').hide();
			$('.wdgt-search').removeClass('input_error');
			var error_status = 0;
			var form_data = {};
				form_data.key = $('#key').val();
				form_data.user_id = $('#ms_uid').val();
				form_data.store_id = $('#ms_sid').val();
				form_data.wdgt_search_text = trimText($('.wdgt-search').val());
				form_data.widget_type = $('#widget_type').val();
				form_data.home_type = $('#home_type').val();
			if(form_data.wdgt_search_text == ""){
				$('.wdgt-search').addClass('input_error');
				error_status++;
				return false;
			}
			$('#wdgt-splash-wrapper').addClass('hide');
			$('#wdgt-splash-category-display').addClass('hide');
			$('#wdgt-splash-category-display').parent('.slimScrollDiv').addClass('hide');
			$('#wdgt-normal-category-display').removeClass('hide');
			$('#wdgt-home').removeClass('hide');
			$('.wdgt-entire-popup').trigger('click');
			$('.wdgt-main-wrapper').addClass('hide');
			$('.wdgt-show-product').removeClass('hide');
			$('.main_cat_menu').removeClass('active');
			
			store.set('wdgt_search_text', form_data.wdgt_search_text);
			
			preLoader($('#ms-wrapper'));
			$.post(root + 'widget-store-search.html', form_data,function(data){ 
				$('.wdgt-show-product').html(data);
				$('.wdgt-search-wrapper').remove();
				$('#preloader-wrapper').hide();
			});
			
			return false;
		
	});
	
	
	
	$(document).on('click', '.wdgt_search_page', function(){
			$('#preloader-wrapper').hide();
			$(this).removeClass('input_error');
			var error_status = 0;
			var form_data = {};
				form_data.key = $('#key').val();
				form_data.user_id = $('#ms_uid').val();
				form_data.store_id = $('#ms_sid').val();
				form_data.wdgt_search_text = $('#search_text').val();
				form_data.widget_type = $('#widget_type').val();
				form_data.home_type = $('#home_type').val();
			
			var page;
			if($(this).attr('id') == "page_link"){
				page = $(this).attr('href');
			}else{
				page = root + "widget-store-search.html";
			}
			preLoader($('#ms-wrapper'));
			$.post( page, form_data,function(data){
				$('.wdgt-show-product').html(data);
				$('#preloader-wrapper').hide();
			});
			return false;
		
	});


	$('.box').mouseenter(function() {
		$(this).animate({rotate: '10deg'}, {queue: false, duration: 200});
	});
	$('.box').mouseleave(function() {
		$(this).animate({rotate: '0deg'}, {queue: false, duration: 200});
	});
	
	
	$(document).on('click', '.wdgt-popup-gift-close', function(){
		$('#wdgt-giftbox').hide(50);
	});
	
	$(document).on('mouseenter', '.#slideshow-each-title-div-top #slideshow-title-top label', function(){
		$('#slideshow-title label').css({'opacity':1, 'filter': 'Alpha(opacity=100)'});
	});
	$(document).on('mouseleave', '.#slideshow-each-title-div-top #slideshow-title-top label', function(){
		$('#slideshow-title label').css({'opacity':0.8, 'filter': 'Alpha(opacity=80)'});
	});
	
	// Cart Section
	//	
	$(document).on('click', '.wdgt-cart', function(){
		var form_data = {};
			form_data.key = $('#key').val();
			form_data.user_id = $('#ms_uid').val();
			form_data.store_id = $('#ms_sid').val();
			form_data.widget_type = $('#widget_type').val();
			form_data.home_type = $('#home_type').val();
			
		preLoader($('#ms-wrapper'));
		load_cart(form_data);
	});
	$(document).on('click', '.wdgt-cart-close', function(){
		$('#wdgt-cart-popup').hide();
	});
	$(document).on('click', '.wdgt-popup-cart-close', function(){
		$('#wdgt-cart-popup').hide();
	});
	$(document).on('click', '.wgt-item-list-lastcolum a', function() {
		preLoader($('#ms-wrapper'));
		$('#addtocart-wrapper').hide();
		var quantity = $(this).parent().find('input').val();
		var m = $(this).parent().find('input').attr('m');
		var p = $(this).parent().find('input').attr('p');
		var variant_id = parseInt($(this).parent().find('input').attr('variant_id'));
		var su = $(this).parent().find('input').attr('su');
		var s = $(this).parent().find('input').attr('s');
		var has_options = 0;
		
		if(typeof(variant_id) != 'undefined' && variant_id > 0) {
			has_options = 1;
		}
		
		var data = {product_user_id:m,
					product_id:p, 
					product_store_id: s,
					product_store_url: CS.parent_embed_url,
					store_id: s,
					store_user_id: su,
					quantity: quantity,
					has_options: has_options,
					variant_id: variant_id};
		update_cart_quantity(data);
	});
	$(document).on('click', '#wdgt-cart-popup .wgt-item-list-cancel', function() {
		preLoader($('#ms-wrapper'));
		$('#wdgt-cart-popup').hide();
		var quantity = $(this).parent().find('input').val();
		var m = $(this).parent().find('input').attr('m');
		var p = $(this).parent().find('input').attr('p');
		var variant_id = $(this).parent().find('input').attr('variant_id');
		var su = $(this).parent().find('input').attr('su');
		var s = $(this).parent().find('input').attr('s');
		var has_options = 0;
		
		if(typeof(variant_id) != 'undefined' && variant_id > 0) {
			has_options = 1;
		}
		var data = {product_user_id:m,
					product_id:p, 
					store_id: s,
					store_user_id: su,
					quantity: quantity,
					has_options: has_options,
					variant_id: variant_id};
		delete_cart_product(data);
	});
	
	get_cart_items({ key: $('#key').val(),
					user_id: $('#ms_uid').val(),
					store_id: $('#ms_sid').val(),
					widget_type: $('#widget_type').val()});
					
	$(document).on("click", ".wdgt-bynow-btn", function(){
		preLoader($('#ms-wrapper'));
		var el = $(this);
		var qty = parseInt(el.parent().find('input[name="qty"]').val());
		var data = $.parseJSON(el.attr('data'));
		var data = {product_user_id:data.p_uid,
					product_id:data.p_id, 
					product_store_id: data.store_id,
					product_store_url: CS.parent_embed_url,
					store_id:data.store_id,
					store_user_id:data.store_uid ,
					quantity: parseInt(qty),
					has_options: data.has_options };
		if(parseInt(data.has_options) > 0) {
			show_selectoptions(data);
		}
		else {
			addtocart(data);	
		}
	});
});


function trimText (str) {
	str = str.replace(/^\s+/, '');
	for (var i = str.length - 1; i >= 0; i--) {
		if (/\S/.test(str.charAt(i))) {
			str = str.substring(0, i + 1);
			break;
		}
	}
	return str;
}

function preLoader(mcontainer) {
	var w = parseInt(mcontainer.width());
	var h = parseInt(mcontainer.height());
	
	$('#preloader-wrapper').css({width:w,height:h});
	$('#preloader-wrapper').show();
	var t = (h-parseInt($('#preloader-center').height())) / 2;
	$('#preloader-center').css({'top':t});
}