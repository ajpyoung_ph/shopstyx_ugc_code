var	cloud_url = CS.cloudURL || location.protocol + '//' + location.host + '/';
var	root = CS.siteURL || location.protocol + '//' + location.host + '/';

$(document).ready(function(){
	$('.header-top').textfill({innerTag:'label'});
	
	$(document).on('click', '#shopnow-button, .shop-now-wrapper', function(){
		$('#type4-splash-center').addClass('hide');
		$('#category-list-wrapper').removeClass('hide');
		$('#wdgt-splash-category-display').parent('.slimScrollDiv').removeClass('hide');
		$('#wdgt-splash-category-display').removeClass('hide');
		$('#wdgt-listing-display').addClass('hide');
		$('#wdgt-listing-main').addClass('hide');
		if($('#enable_back_categories').val() == 0){
			$('.show_product').trigger('click');
		}
		if($('#enable_back_categories').val() == 1){
			$('.category-ul-list').slimScroll({
				height: '531px',
				width: '160px',
				start: 'top',
				wheelStep : 2,
				disableFadeOut: true
			});
			
			$('.content-listing').slimScroll({
				height: '107px',
				width: '154px',
				start: 'top',
				wheelStep : 2,
				position: 'left',
				disableFadeOut: true
			});
		}
	});
	
	$(document).on('click', '.show_subcat', function(){
		// $('.wdgt-main-wrapper').addClass('hide');
		// $('.wdgt-show-product').addClass('hide');
		$('.' + $(this).attr('maincat')).removeClass('hide');
		// $('.wdgt-show-product').css({'background-color':'#FFFFFF'});
		// $('.wdgt-show-product').html('');
	});
	
	$(document).on('click', '.close-popup-btn', function(){
		$(this).parents('.sub-category-wrapper').addClass('hide');
	});
	
	$(document).on('click', '.show_product, .wdgt_prod_page, .show_subcat_product', function(){
		$('.sub-category-wrapper').addClass('hide');
		$('#type4-splash-center').addClass('hide');
		$('#category-list-wrapper').removeClass('hide');
		$('#wdgt-splash-category-display').parent('.slimScrollDiv').addClass('hide');
		$('#wdgt-splash-category-display').addClass('hide');
		$('#wdgt-listing-display').removeClass('hide');
		$('#wdgt-listing-main').removeClass('hide');
		
		$('#preloader-wrapper').hide();
		$('.wdgt-main-wrapper').addClass('hide');
		$('.wdgt-show-product').removeClass('hide');
		var page = $(this).attr('page');
		var form_data = {};
		form_data.key = $('#key').val();
		form_data.user_id = $('#ms_uid').val();
		form_data.store_id = $('#ms_sid').val();
		form_data.store_category_id = $(this).attr('cid');
		form_data.widget_type = $('#widget_type').val();
		form_data.home_type = $('#home_type').val();
		form_data.enable_back_categories = $('#enable_back_categories').val();
		$('#mcat_' + $(this).attr('cid')).addClass('active');
		
		var category_name = $(this).attr('catname');
		if(typeof category_name !== "undefined"){
			form_data.category_name = category_name;
		}else{
			form_data.category_name = $('#category_name').val();
		}
		
		var page;
		if($(this).attr('id') == "page_link"){
			page = $(this).attr('href');
			form_data.store_category_id = $('#store_category_id').val();
		}else{
			page = root + "widget-store-product-listing.html";
		}
		preLoader($('#ms-wrapper'));
		$.post(page, form_data,function(data){ 
			$('#wdgt-listing-main').html(data);
			$('#preloader-wrapper').hide();
		});
		return false;
	});
	
	
	$(document).on('click', '.main_cat_menu', function(){
		$('.main_cat_menu').removeClass('active');
		$(this).addClass('active');
	});
	
	$(document).on('click', '#top-search', function(){
		if(! $('.wdgt-search-wrapper').hasClass('present')){
		$(this).parents('#wdgt-main-top').after('<div class="wdgt-search-wrapper present bg-color2 font-color1">'
									+	'<span class="wdgt-search-lbl font-color1">Search</span>'
									+	'<input type="text" class="wdgt-search border-color1 bg-color2 font-color1" name="search" value="" />'
									+	'<button class="wdt-search-button bg-color1"></button>'
									+	'<a href="javascript:;" class="wdgt-search-cancel-link font-color1">Cancel</a>'
									+ '</div>');
		}
	});
	$(document).on('click', '.wdgt-search-cancel-link', function(){
		$('.wdgt-search-wrapper').remove();
	});
	
	$(document).on('click', '.wdgt-more-info-btn, .wdgt-more-info, .wdgt-related-btn, .wdgt-prev-next', function(){
		hideShareAndWish();
		$('#wdgt-popup').css({'z-index': 999});
		var form_data = {};
		form_data.key = $('#key').val();
		form_data.cat_user_id = $('#ms_uid').val();
		form_data.store_id = $('#ms_sid').val();
		form_data.store_category_id = $('#store_category_id').val();
		form_data.product_id = $(this).attr('pid');
		form_data.user_id = $(this).attr('puid');
		form_data.widget_type = $('#widget_type').val();
		form_data.home_type = $('#home_type').val();
		
		var category_name = $('#category_name').val();
		form_data.category_name = category_name;
		if(typeof category_name === "undefined"){
			form_data.category_name = '';
		}
		var fromSlideShow = 0;
		if($(this).hasClass('fromslideshow')){
			fromSlideShow = 1;
		}
		
		preLoader($('#ms-wrapper'));
		$.post(root + "widget-store/product-details.html", form_data,function(data){ 
			$('.home-slideshow').addClass('hide');
			$('.home-splash').addClass('hide');
			$('.home-splash-category').removeClass('hide');
			$('#wdgt-splash-category-display').addClass('hide');
			$('#wdgt-listing-display').removeClass('hide');
			$('#wdgt-listing-main').removeClass('hide');
			$('#wdgt-listing-main').html(data);
			$('#preloader-wrapper').hide();
		});
	});
	
	$(document).on('click', '.back-to-categories', function(){
		$('#type4-splash-center').addClass('hide');
		$('#category-list-wrapper').removeClass('hide');
		$('#wdgt-splash-category-display').parent('.slimScrollDiv').removeClass('hide');
		$('#wdgt-splash-category-display').removeClass('hide');
		$('#wdgt-listing-display').addClass('hide');
		$('#wdgt-listing-main').addClass('hide');
	});
	
});
function trimText (str) {
	str = str.replace(/^\s+/, '');
	for (var i = str.length - 1; i >= 0; i--) {
		if (/\S/.test(str.charAt(i))) {
			str = str.substring(0, i + 1);
			break;
		}
	}
	return str;
}

function preLoader(mcontainer) {
	var w = parseInt(mcontainer.width());
	var h = parseInt(mcontainer.height());
	
	$('#preloader-wrapper').css({width:w,height:h});
	$('#preloader-wrapper').show();
	var t = (h-parseInt($('#preloader-center').height())) / 2;
	$('#preloader-center').css({'top':t});
}