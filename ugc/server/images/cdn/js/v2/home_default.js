var vid = CS.vid,
	cloud_url = CS.cloudURL || location.protocol + '//' + location.host + '/',
	root = CS.siteURL || location.protocol + '//' + location.host + '/',
	current_url = location.href;

$(document).foundation();
var mySwiper = new Swiper('.swiper-container',{
	pagination: '.pagination',
	paginationClickable: true
});
var tabsSwiper = new Swiper('.swiper-container2',{
	onlyExternal : true,
	speed:500
});

function Loader(){
	var user_id = store.get('user_id'),
		token = store.get('token'),
		login_data = {post_data:1,
						token:token,
						user_id:user_id
					};

	// check here if user is logged In
	$.post(root + "checkuser.html",login_data,function(data){
		var _data = $.parseJSON(data);
		if( _data.result){
			store.set('display_name', _data.display_name);
			store.set('user_id', _data.user_id);
			
			$('#wrapper').html('');
			$('#loadbox-wrapper').removeClass('hide');
			var form_data = {};
			form_data.token = token;
			form_data.user_id = user_id;
			form_data.isAjax = true;
			$.post(location.href, form_data,function(response){
				$('#wrapper').html(response);
				$('#loadbox-wrapper').addClass('hide');
				$('#wrapper').removeClass('hide');
				$('#loadbox-wrapper').addClass('hide');
			});
			
		}
		else {
			store.remove('token');
			store.remove('user_id');
			store.remove('display_name');
			store.remove('remember');
			$('#wrapper').removeClass('hide');
			$('#loadbox-wrapper').addClass('hide');
		}
	});	
}
$.getScript(cloud_url+'js/v2/promote-product-fn.js');
$(document).ready(function(){
	//Loader();
	
	$('.arrow-left').on('click', function(e){
		e.preventDefault()
		mySwiper.swipePrev()
	});
	$('.arrow-right').on('click', function(e){
		e.preventDefault()
		mySwiper.swipeNext()
	});

	
	$(document).on('click', '.build-now-btn', function(e){
		e.preventDefault();
		Foundation.libs.abide.settings.patterns.password = /^[\$!\w]{6,}$/;
		$('#select-your-color').addClass('hide');
		$('.u-color').removeClass('hide');
		$('.tab2').trigger('click');
	});
	$(document).on('click', '.back-btn', function(e){
		e.preventDefault();
		dataService.resetform();
		$('.tab1').trigger('click');
	});
	$('.tabs a').on('touchstart mousedown click',function(e){
		e.preventDefault()
		tabsSwiper.swipeTo( $(this).index() )
	});

	$("a[href='#next']").click(function() {
		$("html, body").animate({ scrollTop: $(".pg2")[0].scrollHeight }, "slow");
		return false;
	});

	$('#start-build').click(function(e) {
		e.preventDefault();
		Foundation.libs.abide.settings.patterns.password = /^[\$!\w]{6,}$/;
		$('.tab2').trigger('click');
		$('.u-color').addClass('hide');
		$('#select-your-color').removeClass('hide');
		$("html, body").animate({ scrollTop: 0 }, "slow");
		return false;
	});

	$(document).on('touchstart click', '.colors-opt span', function(){
		$('#select-your-color span').removeClass('selected');
		$('#select-your-color span.' + $(this).attr('class')).addClass('selected');
		$('.colors-opt span').removeClass('selected');
		$(this).addClass('selected');
		$('.s-color').css({'background-color': $(this).attr('data')}); console.log($(this).attr('data-color'));
		$('.s-color').attr('data-color', $(this).attr('data-color'));
	});

	$(document).on('touchstart click', '#select-promoter-color span', function(){
		$('#select-promoter-color span').removeClass('selected');
		$(this).addClass('selected');
	});
	
	$(document).on('touchstart click', '#select-your-color span', function(){
		$('.colors-opt span').removeClass('selected');
		$('.colors-opt span.' + $(this).attr('class')).addClass('selected');
		$('#select-your-color span').removeClass('selected');
		$(this).addClass('selected');
		$('.s-color').css({'background-color': $(this).attr('data')});
		$('.s-color').attr('data-color', $(this).attr('data-color')); console.log($(this).attr('data-color'));
		$('#select-your-color').addClass('hide');
		$('.u-color').removeClass('hide');
	});

	$(document).on('click', '#sel-product', function(){
		$('.slide-product').removeClass('selected');
		$(this).parents('.slide-product-wrapper').find('.slide-product').addClass('selected');
	}); 

	$(document).on('click', '.common-cancel-btn', function(){
		dataService.resetform();
	});

	$(document).on('click', '.c-modal', function(){
		dataService.resetform();
	});

	$(document).on('mouseover', '.slide-product-wrapper', function(){
		$('.slider-wrapper .arrow-right').css({'z-index':0});
	});
	$(document).on('mouseout', '.slide-product-wrapper', function(){
		$('.slider-wrapper .arrow-right').css({'z-index':2});
	});
	
	

	
	$(document).on('touchstart click', '.shop-back-btn', function(){
		if($('.category-list').length <= 0){
			$('.build-product-wrapper').animate({ marginLeft: '0' }, 300);
			$('.tips-and-cmd h2').addClass('hide');
		}
		if($('.category-list').length > 0){
			$('.build-product-wrapper').animate({ marginLeft: '0' }, 300);
			$('.tips-and-cmd h2').addClass('hide');
			$('#continue-build-btn').fadeIn(300, function(){ $(this).removeClass('hide');});
		}
		dataService.resetform();
	});
	$(document).on('touchstart click', '#continue-build-btn', function(){
		Foundation.libs.abide.settings.patterns.password = /^[\$!\w]{6,}$/;
		dataService.resetform();
		$(this).addClass('hide');
		$('.tips-and-cmd h2').removeClass('hide');
		$('.build-product-wrapper').animate({ marginLeft: '-716px' }, 300);
	});
});