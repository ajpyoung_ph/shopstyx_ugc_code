$(document).foundation();
$(document).ready(function(){
	var count = 0;
	var winH = $(window).height();
	var loadPhone = true;
	var loadTablet = false;
	
	$('INPUT.minicolors').minicolors({
		textfield: false,
		hide: function() {
			$('.save-colors-btn').removeClass('hide');
			$('.upload-image-logo').css({'background-color':$('.pcolor1').val(), 'color':$('.pcolor2').val(), 'border-color':$('.pcolor3').val() });
			$('.text-logo-box').css({'background-color':$('.pcolor1').val(), 'color':$('.pcolor2').val(), 'border-color':$('.pcolor3').val() });
			$('.tmpcolor1').val( $('.pcolor1').val() );
			$('.tmpcolor2').val( $('.pcolor2').val() );
			$('.tmpcolor3').val( $('.pcolor3').val() );
			$('.tmpcolor4').val( $('.pbody_background').val() );
		}
	});
	
	$(document).on('click', '.save-colors-btn', function(){
		$(this).css({'opacity':0.8}).html('Saving...');
		
		if($('.colors-opts-box input:radio[name="color_opt"]:checked').val() == 1){
			saveColors({color1:$('.color-options .selected .color1').attr('data-color'),color2:$('.color-options .selected .color2').attr('data-color'),color3:$('.color-options .selected .color3').attr('data-color'),body_background:$('.color-options .selected .color4').attr('data-color'),preset_colors_id:$('.color-options .selected .preset-id').val()});
		}
		if($('.colors-opts-box input:radio[name="color_opt"]:checked').val() == 2){
			saveColors({color1:$('.pcolor1').val(),color2:$('.pcolor2').val(),color3:$('.pcolor3').val(),body_background:$('.pbody_background').val(),preset_colors_id:0});
		}
	});
	
	$(document).on('keyup', '.text-logo', function(){
		$('.save-txt-logo-btn').removeClass('hide');
	});
	
	$(document).on('change', '.txt-logo-type', function(){
		var store_logo_type;
		$('.upload-image-logo').addClass('hide');
		$('.text-logo-box').removeClass('hide');
		saveLogoType({store_logo_type: 1});
	});
	
	$(document).on('change', '.img-logo-type', function(){
		var store_logo_type;
		$('.text-logo-box').addClass('hide');
		$('.upload-image-logo').removeClass('hide');
		saveLogoType({store_logo_type: 2});
	});
	
	$(document).on('change', '.preset-color-opt', function(){
		$('.custom-color-box').addClass('hide');
		$('.preset-color-box').removeClass('hide');
		if(typeof mySwiper == 'undefined'){
			var mySwiper = new Swiper('.swiper-container',{
				slidesPerView: 3
			});
			console.log(mySwiper);
			$('.arrow-left').on('click', function(e){
				e.preventDefault();
				mySwiper.swipePrev();
			});
			$('.arrow-right').on('click', function(e){
				e.preventDefault();
				mySwiper.swipeNext();
			});
		}
	});
	
	$(document).on('click', '.color-options .swiper-slide', function(){
		$('.color-options .swiper-slide').removeClass('selected');
		$(this).addClass('selected');
		$('.save-colors-btn').removeClass('hide');
		
		$('.upload-image-logo').css({'background-color':$(this).find('.color1').attr('data-color'), 'color':$(this).find('.color2').attr('data-color'), 'border-color':$(this).find('.color3').attr('data-color') });
		$('.text-logo-box').css({'background-color':$(this).find('.color1').attr('data-color'), 'color':$(this).find('.color2').attr('data-color'), 'border-color':$(this).find('.color3').attr('data-color') });
		$('.pcolor1' ).minicolors('value', $(this).find('.color1').attr('data-color'));
		$('.pcolor2' ).minicolors('value', $(this).find('.color2').attr('data-color'));
		$('.pcolor3' ).minicolors('value', $(this).find('.color3').attr('data-color'));
		$('.pbody_background' ).minicolors('value', $(this).find('.color4').attr('data-color'));
	});
	
	$(document).on('change', '.custom-color-opt', function(){
		$('.preset-color-box').addClass('hide');
		$('.custom-color-box').removeClass('hide');
		$('.save-colors-btn').removeClass('hide');
	});
	
	$(document).on('keypress change', '.tmpcolor', function(){
		var $class = $(this).attr('data');
		$('.' + $class ).minicolors('value', $(this).val());
		$('.save-colors-btn').removeClass('hide');
		
		$('.upload-image-logo').css({'background-color':$('.pcolor1').val(), 'color':$('.pcolor2').val(), 'border-color':$('.pcolor3').val() });
		$('.text-logo-box').css({'background-color':$('.pcolor1').val(), 'color':$('.pcolor2').val(), 'border-color':$('.pcolor3').val() });
	});
	
	$(document).on('click', '.save-txt-logo-btn', function(){
		$('.text-logo').css({'border': '1px solid #E0E0E0'});
		var store_name = $.trim($('.text-logo').val());
		if(store_name == ''){
			$('.text-logo').css({'border': '1px solid #FF0000'});
			return false;
		}
		$(this).css({'opacity':0.8}).html('Saving...');
		saveTextLogo({store_name: $('.text-logo').val()});
	});
	
	$('#color-choices').removeClass('hide');

	// $(document).on('open', '#desktopModal, #stykableDesktopModal, #tabletphoneModal, #facebookModal, #miniStoreModal', function (e) {
		// $(this).centerized();
		// e.preventDefault(); e.stopImmediatePropagation();
		// var wH = $(window).height();
			// mwH = $('#body-wrapper').height() + 100,
			// page = '',
			// modal_class = '';
		// $('.shop-settings-modal').css({'height': mwH + 'px'});
		// $('.reveal-modal-bg').css({'background-color':'#F2F5F6'});
		
		// if($(this).hasClass('desktopModal')){
			// modal_class = '.desktopModal';
			// page = '/admin/build?page=desktop';
			// $(modal_class + ' .settings-wrapper').html('');
			// $(modal_class + ' .settings-loader').removeClass('hide');
			
		// }
		// if($(this).hasClass('tabletphoneModal')){
			// modal_class = '.tabletphoneModal';
			// page = '/admin/build?page=tablet_phone';
			// $(modal_class + ' .settings-wrapper').html('');
			// $(modal_class + ' .settings-loader').removeClass('hide');
		// }
		// if($(this).hasClass('facebookModal')){
			// modal_class = '.facebookModal';
			// page = '/admin/build?page=facebook';
			// $(modal_class + ' .settings-wrapper').html('');
			// $(modal_class + ' .settings-loader').removeClass('hide');
		// }
		// if($(this).hasClass('miniStoreModal')){
			// modal_class = '.miniStoreModal';
			// page = '/admin/build?page=ministore';
			// $(modal_class + ' .settings-wrapper').html('');
			// $(modal_class + ' .settings-loader').removeClass('hide');
		// }
		// if($(this).hasClass('stykableDesktopModal')){
			// modal_class = '.stykableDesktopModal';
			// page = '/admin/build?page=stykable_desktop';
			// $(modal_class + ' .settings-wrapper').html('');
			// $(modal_class + ' .settings-loader').removeClass('hide');
		// }
		// var form_data = {};
		// form_data.isAjax = true;
		// SSAjax({
			// data: form_data,
			// url: page,
			// success: function(response){
				// $(modal_class + ' .settings-wrapper').html(response);
				// $(modal_class + ' .settings-loader').addClass('hide');
				// if(modal_class == '.tabletphoneModal'){
					// if(typeof store.get('manage_design_active') !== 'undefined' && store.get('manage_design_active') == 'phone'){
						// $('.phone-btn1').trigger('click');
						// loadTablet = true;
						// store.remove('manage_design_active');
						// return false;
					// }
					// store.remove('manage_design_active');
					// previewPage({page: 'tablet', color1:$('.tabletphone-pcolor1').val(),color2:$('.tabletphone-pcolor2').val(),body_background:$('.tabletphone-pbody_background').val(), home_type: $('.tabletphone-home-type').val()});
				// }
			// }
		// });
	// });
	
	// $(document).on('close', '#desktopModal, #stykableDesktopModal, #tabletphoneModal, #facebookModal, #miniStoreModal', function (e) {
		// e.preventDefault(); e.stopImmediatePropagation();
		// $('.reveal-modal-bg').css({'background-color':'#FFFFFF'});
		// loadPhone = true;
	// });

	$(document).on('opened', '#desktopModal, #stykableDesktopModal, #tabletphoneModal, #facebookModal, #miniStoreModal', function (e) {
		e.preventDefault(); e.stopImmediatePropagation();
		$('.reveal-modal-bg').css({'background-color':'#F2F5F6'});
	});

	$( window ).resize(function(e) {
		e.preventDefault(); e.stopImmediatePropagation();
		$('#desktopModal').centerized();
		
		var wH = winH = $(window).height();
			mwH = $('#body-wrapper').height() + 100;
			mH = wH - 60;
		$('.shop-settings-modal').css({'height': mwH + 'px'});
		$('.settings-left').css({'height': mH + 'px'});
		
		if(winH <= 720){
			if(count == 0){
				var mySwiper = new Swiper('.swiper-container', {
					scrollContainer:true,
					mousewheelControl : true,
					mode:'vertical',
					//Enable Scrollbar
					scrollbar: {
						container :'.swiper-scrollbar',
						hide: true,
						draggable: true  
					}
				});
			}
			count++;
		}
	});

	$(document).on('click', '.home-type-box', function(){
		if($(this).hasClass('active')){ return false; }
		$('.change-msg').removeClass('hide');
		$('.home-type-box').removeClass('active');
		$(this).addClass('active');
		$('.display-type').addClass('hide');
		$('.display' + $(this).attr('data')).removeClass('hide');
		$('.desktop-home-type').val($(this).attr('data-home-type'));
		$('.tabletphone-home-type').val($(this).attr('data-home-type'));
		var page = $(this).attr('data-page');
		if(page == 'desktop'){
			previewPage({page: page, color1:$('.desktop-pcolor1').val(),color2:$('.desktop-pcolor2').val(),body_background:$('.desktop-pbody_background').val(), home_type: $('.desktop-home-type').val()});
		}
		if(page == 'stykable_desktop'){
			previewPage({page: page, color1:$('.stykable-desktop-pcolor1').val(),color2:$('.stykable-desktop-pcolor2').val(),body_background:$('.stykable-desktop-pbody_background').val(), home_type: $('.stykable-desktop-home-type').val()});
		}
		if(page == 'tabletphone'){
			if($('.tabphone-active').attr('data-class') == 'tablet-wrapper'){
				previewPage({page: 'tablet', color1:$('.tabletphone-pcolor1').val(),color2:$('.tabletphone-pcolor2').val(),body_background:$('.tabletphone-pbody_background').val(), home_type: $('.tabletphone-home-type').val()});
			}
			if($('.tabphone-active').attr('data-class') == 'phone-wrapper'){
				previewPage({page: 'phone', color1:$('.tabletphone-pcolor1').val(),color2:$('.tabletphone-pcolor2').val(),body_background:$('.tabletphone-pbody_background').val(), home_type: $('.tabletphone-home-type').val()});
			}
		}
	});
	
	$(document).on('click', '.phone-btn1', function(){
		if(loadPhone){
			loadPhone = false;
			previewPage({page: 'phone', color1:$('.tabletphone-pcolor1').val(),color2:$('.tabletphone-pcolor2').val(),body_background:$('.tabletphone-pbody_background').val(), home_type: $('.tabletphone-home-type').val()});
		}
	});
	
	$(document).on('touchstart click', '.tablet-btn1', function(){
		if(loadTablet){
			loadTablet = false;
			previewPage({page: 'tablet', color1:$('.tabletphone-pcolor1').val(),color2:$('.tabletphone-pcolor2').val(),body_background:$('.tabletphone-pbody_background').val(), home_type: $('.tabletphone-home-type').val()});
		}
	});
	
	$(document).on('click', '.page-type-box', function(){
		$('.page-type').val($(this).attr('data-page-type'));
		if($(this).hasClass('active')){ return false; }
		$('.change-msg').removeClass('hide');
		$('.page-type-box').removeClass('active');
		$(this).addClass('active');
		$('.display-type').addClass('hide');
		$('.display' + $(this).attr('data')).removeClass('hide');
		var page = $(this).attr('data-page');
		previewPage({page: page, color1:$('.fb-pcolor1').val(),color2:$('.fb-pcolor2').val(),body_background:$('.fb-pbody_background').val(), page_type: $('.page-type').val()});
	});
	
	$(document).on('click', '.utheme', function(){
		$('.utheme').removeClass('active');
		$(this).addClass('active');
		$('.stheme').addClass('hide');
		$('.theme' + $(this).attr('data')).removeClass('hide');
	});

	$(document).on('mouseover', '.slide-inner', function(){
		$('.swiper-wrapper').css({'z-index': 1});
		if(winH <= 720){ $('.swiper-scrollbar').css({'opacity': 1 }); }
	});

	$(document).on('mouseover', '.menu-home-page', function(){
		if(winH <= 720){ $('.swiper-scrollbar').css({'opacity': 0 }); }
	});

	$(document).on('click', '.slide-inner', function(){
		$('.swiper-scrollbar').css({'opacity': 0 });
	});

	$(document).on('mouseover', '.swiper-scrollbar', function(){
		if(winH <= 720){ $(this).css({'opacity': 1 }); }
	});

	$(document).on('mouseout', '.slide-inner', function(){
		if(winH <= 720){ $('.swiper-scrollbar').css({'opacity': 0 }); }
	});

	$(document).on('click', '.view-blog-btn', function(e){
		e.preventDefault(); e.stopImmediatePropagation();
		openModal2();
	});
	
	// $(document).on('click', '.save-desktop-settings-btn', function(e){
		// e.preventDefault(); e.stopImmediatePropagation();
		// $(this).css({'opacity':0.8}).html('Saving...');
		// $('.settings-mini-view').addClass('hide');
		// $('.settings-mini-view').removeClass('unhide');
		// if($('.store-type').val() == 'desktop'){
			// //saveDesktopSettings({color1:$('.desktop-pcolor1').val(),color2:$('.desktop-pcolor2').val(),color3:$('.desktop-pcolor3').val(),body_background:$('.desktop-pbody_background').val(), home_type: $('.desktop-home-type').val(), slideshow: $('.desktop-slideshow').val(), listing: $('.desktop-listing').val(), slideshow_items: $('.desktop-slideshow-item').val(), listing_items: $('.desktop-listing-item').val()});
			// saveDesktopSettings({color1:$('.desktop-pcolor1').val(),color2:$('.desktop-pcolor2').val(),color3:$('.desktop-pcolor3').val(),body_background:$('.desktop-pbody_background').val(), preset_colors_id: 0});
			// if($('.desktop-home-type').val() ==  'slideshow'){
				// $('.for-slideshow-listing').addClass('unhide');
				// $('.for-slideshow-listing').removeClass('hide');
			// }
			// if($('.desktop-home-type').val() ==  'category'){
				// $('.for-categories').addClass('unhide');
				// $('.for-categories').removeClass('hide');
			// }
			// if($('.desktop-home-type').val() ==  'slideshow_only'){
				// $('.for-slideshow-only').addClass('unhide');
				// $('.for-slideshow-only').removeClass('hide');
			// }
		// }
		// if($('.store-type').val() == 'stykable_desktop'){
			// saveDesktopSettings({color1:$('.stykable-desktop-pcolor1').val(),color2:$('.stykable-desktop-pcolor2').val(),body_background:$('.stykable-desktop-pbody_background').val(), home_type: $('.stykable-desktop-home-type').val(), slideshow: $('.stykable-desktop-slideshow').val(), listing: $('.stykable-desktop-listing').val(), slideshow_items: $('.stykable-desktop-slideshow-item').val(), listing_items: $('.stykable-desktop-listing-item').val()});
			// if($('.stykable-desktop-home-type').val() ==  'slideshow'){
				// $('.for-slideshow-listing').addClass('unhide');
				// $('.for-slideshow-listing').removeClass('hide');
			// }
			// if($('.stykable-desktop-home-type').val() ==  'category'){
				// $('.for-categories').addClass('unhide');
				// $('.for-categories').removeClass('hide');
			// }
			// if($('.stykable-desktop-home-type').val() ==  'slideshow_only'){
				// $('.for-slideshow-only').addClass('unhide');
				// $('.for-slideshow-only').removeClass('hide');
			// }
		// }
		// if($('.store-type').val() == 'tabletphone'){
			// saveDesktopSettings({color1:$('.tabletphone-pcolor1').val(),color2:$('.tabletphone-pcolor2').val(),body_background:$('.tabletphone-pbody_background').val(), home_type: $('.tabletphone-home-type').val(), slideshow: $('.tabletphone-slideshow').val(), listing: $('.tabletphone-listing').val(), slideshow_items: $('.tabletphone-slideshow-item').val(), listing_items: $('.tabletphone-listing-item').val()});
			// if($('.tabletphone-home-type').val() ==  'slideshow'){
				// $('.for-slideshow-listing').addClass('unhide');
				// $('.for-slideshow-listing').removeClass('hide');
			// }
			// if($('.tabletphone-home-type').val() ==  'category'){
				// $('.for-categories').addClass('unhide');
				// $('.for-categories').removeClass('hide');
			// }
			// if($('.tabletphone-home-type').val() ==  'slideshow_only'){
				// $('.for-slideshow-only').addClass('unhide');
				// $('.for-slideshow-only').removeClass('hide');
			// }
		// }
	// });
	
	$(document).on('closed', '.reveal-modal', function () {
		//var modal = $(this); alert('tablet');
		modal_class = '.reveal-modal';
		$(modal_class + ' .settings-wrapper').html('');
	});
	
	$(document).on('click', '.save-shop-settings-btn', function(e){
		e.preventDefault(); e.stopImmediatePropagation();
		$(this).css({'opacity':0.8}).html('Saving...');
		
		if($('.store-type').val() == 'fbstore'){
			saveShopSettings({color1:$('.fb-pcolor1').val(),color2:$('.fb-pcolor2').val(),body_background:$('.fb-pbody_background').val(), store_type: $('.store-type').val(), page_type: $('.page-type').val(), slideshow: $('.fb-slideshow').val(), listing: $('.fb-listing').val(), slideshow_items: $('.fb-slideshow-item').val(), listing_items: $('.fb-listing-item').val()});
		}
		if($('.store-type').val() == 'ministore'){
			saveShopSettings({color1:$('.ministore-pcolor1').val(),color2:$('.ministore-pcolor2').val(),body_background:$('.ministore-pbody_background').val(), store_type: $('.store-type').val(), page_type: $('.page-type').val(), slideshow: $('.ministore-slideshow').val(), listing: $('.ministore-listing').val(), slideshow_items: $('.ministore-slideshow-item').val(), listing_items: $('.ministore-listing-item').val()});
		}
	});
	
	$(document).on('change', '.home-page-box select', function(){
		$('.change-msg').removeClass('hide');
	});
	
	$(document).on('click', '#store_logo_btn', function(event){
		event.stopImmediatePropagation();
		$("#store_logo_file").click();
		return false;
	});
	
	
	$(document).on('change', '#store_logo_file', function(event){
		event.stopImmediatePropagation();
		$(".logo_filename").html($(this)[0].files[0].name);
		$(".file_exist").val(1);
		$('#storelogoform').trigger('submit');
		//$(this).parents('.store-addcat-img').removeAttr('style');
	});
	
	
	$(document).on('submit', '#storelogoform', function(event){
		event.preventDefault();
		event.stopImmediatePropagation();
		
		var options = {
			target:        '#store_logo_message',
			success:       uploadLogoResponse
		};
		$('.upload-loader').removeClass('hide');
        $(this).ajaxSubmit(options); 
        return false; 
    });
	
	$(document).on('click', '.tabphone-btn', function(){
		$('.tabphone-btn').removeClass('tabphone-active');
		$(this).addClass('tabphone-active');
		$('.tabphone-wrapper').addClass('hide');
		$('.' + $(this).attr('data-class')).removeClass('hide');
		if($(this).attr('data-class') == 'tablet-wrapper'){
			$('.tabphone-tab').val('tablet');
		}
		if($(this).attr('data-class') == 'phone-wrapper'){
			$('.tabphone-tab').val('phone');
		}
		
	});
	
	if(typeof store.get('manage_design') !== 'undefined' && store.get('manage_design') == 'tablet-phone'){
		store.remove('manage_design');
		$('.clickable-tablet').trigger('click');
	}
	if(typeof store.get('manage_design') !== 'undefined' && store.get('manage_design') == 'facebook'){
		store.remove('manage_design');
		$('.clickable-fb').trigger('click');
	}
	if(typeof store.get('manage_design') !== 'undefined' && store.get('manage_design') == 'minishop'){
		store.remove('manage_design');
		$('.clickable-ministore').trigger('click');
	}
	
	
});

// function saveDesktopSettings(dataObj){
	// var form_data = {};
	// form_data.isAjax = true;
	// $.extend(form_data,dataObj);
	// SSAjax({
		// data: form_data,
		// url: '/admin/build?action=save-desktop-settings',
		// success: function(response){
			// $('.change-msg').addClass('hide');
			// $('.save-colors-btn').css({'opacity':1}).addClass('hide').html('Save Changes');
			// $('.save-desktop-settings-btn').addClass('hide');
			// $('.save-desktop-settings-btn').css({'opacity':1}).html('Save Settings');
			// $('.pcolor1').minicolors('value', $('.desktop-pcolor1').val());
			// $('.pcolor2').minicolors('value', $('.desktop-pcolor2').val());
			// $('.pcolor3').minicolors('value', $('.desktop-pcolor3').val());
			// $('.pbody_background').minicolors('value', $('.desktop-pbody_background').val());
			// $('.upload-image-logo').css({'background-color':$('.desktop-pcolor1').val(), 'color':$('.desktop-pbody_background').val() });
			// $('.text-logo').css({'background-color':$('.desktop-pcolor1').val(), 'color':$('.desktop-pbody_background').val() });
			
			// if($('.store-type').val() == 'desktop'){
				// $('.pcolor1').minicolors('value', $('.desktop-pcolor1').val());
				// $('.pcolor2').minicolors('value', $('.desktop-pcolor2').val());
				// $('.pcolor3').minicolors('value', $('.desktop-pcolor3').val());
				// $('.pbody_background').minicolors('value', $('.desktop-pbody_background').val());
				// $('.tmpcolor1').val( $('.desktop-pcolor1').val() );
				// $('.tmpcolor2').val( $('.desktop-pcolor2').val() );
				// $('.tmpcolor3').val( $('.desktop-pcolor3').val() );
				// $('.tmpcolor4').val( $('.desktop-pbody_background').val() );
				// $('.upload-image-logo').css({'background-color':$('.desktop-pcolor1').val(), 'color':$('.desktop-pcolor2').val(), 'border-color':$('.desktop-pcolor3').val() });
				// $('.text-logo-box').css({'background-color':$('.desktop-pcolor1').val(), 'color':$('.desktop-pcolor2').val(), 'border-color':$('.desktop-pcolor3').val() });
			// }
			// if($('.store-type').val() == 'stykable_desktop'){
				// $('.pcolor1').minicolors('value', $('.stykable-desktop-pcolor1').val());
				// $('.pcolor2').minicolors('value', $('.stykable-desktop-pcolor2').val());
				// $('.pbody_background').minicolors('value', $('.stykable-desktop-pbody_background').val());
				// $('.upload-image-logo').css({'background-color':$('.stykable-desktop-pcolor1').val(), 'color':$('.stykable-desktop-pbody_background').val() });
				// $('.text-logo').css({'background-color':$('.stykable-desktop-pcolor1').val(), 'color':$('.stykable-desktop-pbody_background').val() });
			// }
			// if($('.store-type').val() == 'tabletphone'){
				// $('.pcolor1').minicolors('value', $('.tabletphone-pcolor1').val());
				// $('.pcolor2').minicolors('value', $('.tabletphone-pcolor2').val());
				// $('.pbody_background').minicolors('value', $('.tabletphone-pbody_background').val());
				// $('.upload-image-logo').css({'background-color':$('.tabletphone-pcolor1').val(), 'color':$('.tabletphone-pbody_background').val() });
				// $('.text-logo').css({'background-color':$('.tabletphone-pcolor1').val(), 'color':$('.tabletphone-pbody_background').val() });
			// }
		// }
	// });
// }
function saveShopSettings(dataObj){
	var form_data = {};
	form_data.isAjax = true;
	$.extend(form_data,dataObj);
	SSAjax({
		data: form_data,
		url: '/admin/build?action=save-shop-settings',
		success: function(response){
			$('.change-msg').addClass('hide');
			$('.save-colors-btn').css({'opacity':1}).addClass('hide').html('Save Changes');
			$('.save-shop-settings-btn').css({'opacity':1}).html('Save Settings');
		}
	});
}

function saveColors(dataObj){
	var form_data = {};
	form_data.isAjax = true;
	$.extend(form_data,dataObj);
	SSAjax({
		data: form_data,
		url: '/admin/build?action=save-colors',
		success: function(response){
			$('.save-colors-btn').css({'opacity':1}).addClass('hide').html('Save Changes');
		}
	});
}

function saveLogoType(dataObj){
	var form_data = {};
	form_data.isAjax = true;
	$.extend(form_data,dataObj);
	SSAjax({
		data: form_data,
		url: '/admin/build?action=save-logo-type',
		success: function(response){ return; }
	});
}

function saveTextLogo(dataObj){
	var form_data = {};
	form_data.isAjax = true;
	$.extend(form_data,dataObj);
	SSAjax({
		data: form_data,
		url: '/admin/build?action=save-text-logo',
		success: function(response){
			$('.save-txt-logo-btn').css({'opacity':1}).addClass('hide').html('Save Changes');
		}
	});
}



function uploadLogoResponse(responseText, statusText, xhr, $form)  {
	var _data = $.parseJSON(responseText);
	if(_data.result == 'failed'){
		$('.upload-msg').html(_data.msg);
		$('.upload-loader').addClass('hide');
	}
	if(_data.result == 'success'){
		$('.upload-msg').html('');
		location.href = location.href;
	}
	
	return false;
}