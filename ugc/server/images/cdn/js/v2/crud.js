var drpOptions = {
	hoverClass: "drop-hover",
	drop: function( event, ui ) {
		var $data = ui.draggable;
		if(! $data.hasClass('slimScrollBar') && ! $data.hasClass('prod-list-added')){
			var $drop_img = $data.find('.product-img').attr('data-bg'),
				$drop_plist_id = $data.find('.product-img').attr('data-prodlistid'),
				$pid = $data.find('.product-img').attr('data-pid'),
				$puid = $data.find('.product-img').attr('data-puid');
			
			if($('.tmp-plistid').val() != $drop_plist_id){
				var $this = $(this),
					$category_id = $this.find('.product-images').attr('data-scid');
				
				
				var form_data = {};
					form_data.product_id = $pid;
					form_data.product_user_id = $puid;
					form_data.category_id = $category_id;
					form_data.isAjax = true;
				
				$this.prepend('<span id="text-pre-loader" class="text-pre-loader-style bgcolor1 color3">Adding<span class="dots"></span></span>');
				SSAjax({
					data: form_data,
					url: "/admin/build?action=add-product",
					success: function(response){
						$this.find( '.product-images' ).prepend( '<span data-scid="' + $category_id + '" data-pid="' + $pid + '" data-puid="' + $puid + '" class="p-image prodlistid' + $drop_plist_id + '" style="' + $drop_img + '"></span>' );
						var img_length = $this.find('.p-image').length;
						if(img_length > 0){
							if($this.hasClass('sub-cat-list')){
								$this.find('.category-del2').remove();
							}else{
								$this.find('.category-del').remove();
							}
						}
						if(img_length == 1){
							$this.addClass('cat-link'); 
						}
						if(img_length > 1){
							if($this.hasClass('sub-cat-list')){
								$this.parents('.sub-category-list').addClass('more');
							}else{
								$this.addClass('more');
							}
						}
						if(img_length > 5){
							var more = img_length - 5;
							$this.find('.more-lbl').html('(+' + more + ' more)'); 
						}
						
						var plist = '<div class="category-products prodlistid' + $drop_plist_id + '" style="' + $drop_img + '"> \
										<span class="product-del" data-prodlistid="' + $drop_plist_id + '">&#215;</span> \
										<span class="earn-bottom"></span> \
										<span class="earn-top color3">Earn $180</span> \
									</div>';
						var clist_id = $this.find('.cat-title').attr('data-cat_listid'); 
						$('.oncat' + clist_id).find('.category-products-list').prepend( plist );
						$('.b-prod-l' + $drop_plist_id).addClass('prod-list-added');
						$('.text-pre-loader-style').remove()
					}
				})
				
				
			}
		}
	},
	tolerance: "pointer"
};

var drpOptions2 = {
	drop: function( event, ui ) {
		var $data = ui.draggable;
		if(! $data.hasClass('slimScrollBar') && ! $data.hasClass('prod-list-added')){
			var $drop_img = $data.find('.product-img').attr('data-bg'),
				$drop_plist_id = $data.find('.product-img').attr('data-prodlistid'),
				$pid = $data.find('.product-img').attr('data-pid'),
				$puid = $data.find('.product-img').attr('data-puid');
			
			if($('.tmp-plistid').val() != $drop_plist_id){
				var plist = '<div class="category-products prodlistid' + $drop_plist_id + '" style="' + $drop_img + '"> \
								<span class="product-del" data-prodlistid="' + $drop_plist_id + '">&#215;</span> \
								<span class="earn-bottom"></span> \
								<span class="earn-top color3">Earn $180</span> \
							</div>';
				$(this).prepend( plist );
				var clist_id = $(this).parents('.category-products-wrapper').attr('data-cat_listid');
				var $category_id = $('.category-list' + clist_id).find('.product-images').attr('data-scid');
				$('.category-list' + clist_id)
				.find( '.product-images' )
				.prepend( '<span data-scid="' + $category_id + '" data-pid="' + $pid + '" data-puid="' + $puid + '" class="p-image prodlistid' + $drop_plist_id + '" style="' + $drop_img + '"></span>' );
				
				var $sub_category_id = $('.sub-category-list' + clist_id).find('.product-images').attr('data-scid');
				$('.sub-category-list' + clist_id)
				.find( '.product-images' )
				.prepend( '<span data-scid="' + $sub_category_id + '" data-pid="' + $pid + '" data-puid="' + $puid + '" class="p-image prodlistid' + $drop_plist_id + '" style="' + $drop_img + '"></span>' );
				
				var form_data = {};
					form_data.product_id = $pid;
					form_data.product_user_id = $puid;
					form_data.category_id = (typeof $category_id !== 'undefined')? $category_id : $sub_category_id;
					form_data.isAjax = true;
				
				$('.prodlistid' + $drop_plist_id).prepend('<span id="text-pre-loader" class="text-pre-loader-style bgcolor1 color3">Saving<span class="dots"></span></span>');
				SSAjax({
					data: form_data,
					url: "/admin/build?action=add-product",
					success: function(response){
						var img_length = $('.category-list' + clist_id).find('.p-image').length;
						if(img_length > 0){ 
							$('.category-list' + clist_id).find('.category-del').remove(); 
						}
						if(img_length == 1){ 
							$('.category-list' + clist_id).addClass('cat-link'); 
						}
						if(img_length > 1){ 
							$('.category-list' + clist_id).addClass('more');
						}
						if(img_length > 5){
							var more = img_length - 5;
							$('.category-list' + clist_id).find('.more-lbl').html('(+' + more + ' more)');
						}
						
						var sub_img_length = $('.sub-category-list' + clist_id).find('.p-image').length;
						if(sub_img_length > 0){ 
							$('.sub-category-list' + clist_id).find('.category-del2').remove(); 
						}
						if(sub_img_length == 1){ 
							$('.sub-category-list' + clist_id).addClass('cat-link'); 
						}
						if(sub_img_length > 1){ 
							$('.sub-category-list' + clist_id).parents('.sub-category-list').addClass('more');
						}
						if(sub_img_length > 5){
							var more = sub_img_length - 5;
							$('.sub-category-list' + clist_id).find('.more-lbl').html('(+' + more + ' more)'); 
						}
						
						$('.b-prod-l' + $drop_plist_id).addClass('prod-list-added');
						$('.text-pre-loader-style').remove();
					}
				});
			}
		}
		$(this).parents('.b-product-listing-wrapper').removeClass('drop-hover');
	},
	tolerance: "pointer",
	over: function( event, ui ) {
		$(this).parents('.b-product-listing-wrapper').addClass('drop-hover');
	},
	out: function( event, ui ) {
		$(this).parents('.b-product-listing-wrapper').removeClass('drop-hover');
	}
};
var drgOptions = {
		appendTo: ".category-panel",
		helper: "clone",
		cursor: "move",
		opacity: 0.6,
		zIndex: 10
	};
$(document).ready(function(){
	
	$( ".shop-product-wrapper li" ).draggable( drgOptions );
	//http://api.jqueryui.com/droppable/
	$( ".add-new-cat-btn" ).droppable({
		hoverClass: "drop-hover",
		drop: function( event, ui ) {
			var $data = ui.draggable;
			if(! $data.hasClass('prod-list-added')){
				$( this )
				.parent( ".b-droppable-category" )
				.find( ".add-new-cat-btn" )
				.addClass( "hide" )
				.end()
				.find( ".add-new-cat-box" )
				.removeClass( "hide" );
				$('.add-new-cat-box input').focus();
				var $drop_img = $data.find('.product-img').attr('data-bg'),
					$drop_plist_id = $data.find('.product-img').attr('data-prodlistid');
					$drop_pid = $data.find('.product-img').attr('data-pid');
					$drop_puid = $data.find('.product-img').attr('data-puid');
				$('.tmp-img').html($drop_img);
				$('.tmp-plistid').val($drop_plist_id);
				$('.tmp-pid').val($drop_pid);
				$('.tmp-puid').val($drop_puid);
				
				var option_data = '<option value="0">No parent category</option>';
				$('.main-cat-name').each(function(){
					option_data = option_data + '<option value="' + $(this).attr('data-main_catid') + '">' + $.trim($(this).html()) + '</option>';
				});
				$('#parent_cat').html(option_data);
			}
		}
	});
	$(document).on('click', '.cancel-btn', function(){
		$('.add-new-cat-box').addClass('hide');
		$('.add-new-cat-btn').removeClass('hide');
		$('.common_input_wrapp').removeClass('error');
		$("#addfile").html('<span class="upload-bg"></span><span class="upload-lbl">Upload Image</span>');
		$('#display_message p').html('');
		resetCategoryForm();
	});
	
	
	$( ".category-list" ).droppable( drpOptions );
	$( ".sub-cat-list" ).droppable( drpOptions );
	$( ".category-products-list" ).droppable( drpOptions2 );
	
		
	$(document).on('click', '.product-del', function(){
		var form_data = {},
			cat_prod_wrapper_id = $(this).parents('.category-products-wrapper').attr('data-cat_listid'),
			plist_wrapper = $(this).parents('.category-products-list'),
			plist_id = $(this).attr('data-prodlistid');
			
			
		form_data.product_id = $('.prodlistid' + plist_id).attr('data-pid');
		form_data.product_user_id = $('.prodlistid' + plist_id).attr('data-puid');
		form_data.category_id = $('.prodlistid' + plist_id).attr('data-scid');
		form_data.isAjax = true;
		
		$(this).before('<span id="text-pre-loader" class="text-pre-loader-style bgcolor1 color3">Deleting<span class="dots"></span></span>');
		SSAjax({
			data: form_data,
			url: "/admin/build?action=delete-product",
			success: function(response){
				$('.prodlistid' + plist_id).remove();
				$('.b-prod-l' + plist_id).removeClass('prod-list-added');
				var plist = parseInt($('.oncat' + cat_prod_wrapper_id + ' .category-products').length);
				console.log(plist);
				if(plist <= 0){
					$('.category-list' + cat_prod_wrapper_id).prepend('<span class="category-del" data-catid="' + form_data.category_id + '">&#215;</span>');
					$('.category-list' + cat_prod_wrapper_id).removeClass('cat-link');
					$('.sub-category-list' + cat_prod_wrapper_id).prepend('<span class="category-del2" data-catid="' + form_data.category_id + '">&#215;</span>');
					$('.sub-category-list' + cat_prod_wrapper_id).removeClass('cat-link');
				}
				if(plist <= 1){
					$('.category-list' + cat_prod_wrapper_id).removeClass('more');
				}
				if(plist > 5){
					var more = plist - 5;
					$('.category-list' + cat_prod_wrapper_id).find('.more-lbl').html('(+' + more + ' more)'); 
					$('.sub-category-list' + cat_prod_wrapper_id).find('.more-lbl').html('(+' + more + ' more)'); 
				}else{
					$('.category-list' + cat_prod_wrapper_id).find('.more-lbl').html(''); 
					$('.sub-category-list' + cat_prod_wrapper_id).find('.more-lbl').html(''); 
				}
				
				var sub_cat_count = $('.sub-category-list' + cat_prod_wrapper_id).parents('#cat-list-wrapper').find('.sub-cat-list').length;
				var sub_cat_prod_count = $('.sub-category-list' + cat_prod_wrapper_id).parents('#cat-list-wrapper').find('.p-image').length;
				if(sub_cat_count <= 1 && sub_cat_prod_count <= 1){
					$('.sub-category-list' + cat_prod_wrapper_id).parents('#cat-list-wrapper').find('.sub-category-list').removeClass('more');
				}
			}
		});
			
		
		
		
	});
	$(document).on('click', '.category-del', function(e){
		e.preventDefault();
		e.stopImmediatePropagation();
		var $this = $(this),
			form_data = {};
		form_data.category_id = $(this).attr('data-catid');
		form_data.isAjax = true;
		
		$this.before('<span id="text-pre-loader" class="text-pre-loader-style bgcolor1 color3">Deleting<span class="dots"></span></span>');
		SSAjax({
			data: form_data,
			url: "/admin/build?action=delete-category",
			success: function(response){
				$this.parents('#cat-list-wrapper').remove();
			}
		});
	});
	
	$(document).on('click', '.category-del2', function(e){
		e.preventDefault();
		e.stopImmediatePropagation();
		var $this = $(this),
			form_data = {},
			cat_list_wrapper_id = $this.parents('.sub-category-wrapper').find('.main-cat-name').attr('data-main_catid'),
			catname = $this.parents('.sub-category-wrapper').find('.main-cat-name').html(),
			cat_prod_id =  $this.parent('.sub-cat-list').attr('data-cat_listid');
		
		form_data.category_id = $(this).attr('data-catid');
		form_data.isAjax = true;
		
		$this.before('<span id="text-pre-loader" class="text-pre-loader-style bgcolor1 color3">Deleting<span class="dots"></span></span>');
		SSAjax({
			data: form_data,
			url: "/admin/build?action=delete-category",
			success: function(response){
				$this.parent('.sub-cat-list').remove();
				$('oncat' + cat_prod_id).remove();
				
				var sub_cat_count = $('.cat-list-wrapper-' + cat_list_wrapper_id).find('.sub-cat-list').length,
					sub_cat_prod_count = $('.cat-list-wrapper-' + cat_list_wrapper_id).find('.p-image').length;
				if(sub_cat_count <= 1 && sub_cat_prod_count <= 1){
					$('.cat-list-wrapper-' + cat_list_wrapper_id + ' .sub-category-list').removeClass('more');
				}
				
				if(parseInt($('.cat-list-wrapper-' + cat_list_wrapper_id + ' .sub-cat-list').length) == 0 ){
					var catlist = '<div class="category-list ui-droppable main-cat-list-'+ cat_list_wrapper_id +' category-list' + cat_listid + '" data-cat_listid="' + cat_listid + '"> \
										<span class="category-del" data-catid="' + cat_list_wrapper_id + '">×</span> \
										<div class="b-cat-box1"></div> \
										<div class="b-cat-box2"></div> \
										<div class="b-cat-box3"></div> \
										<div class="b-cat-box4"> \
											<div class="main-category-wrapper main-cat-'+ cat_list_wrapper_id +'">\
												<span class="cat-title main-cat-name color1" data-main_catid="' + cat_list_wrapper_id + '" data-cat_listid="' + cat_listid + '">' + catname + '</span> \
												<div class="product-images"></div> \
												<span class="color1 more-lbl" data-cat_listid="' + cat_listid + '"></span> \
											</div> \
										</div> \
									</div>';
					$( '.cat-list-wrapper-' + cat_list_wrapper_id ).html(catlist);
					$( '.main-cat-list-'+ cat_list_wrapper_id ).droppable( drpOptions );
					var prodlist = '<div class="category-products-wrapper hide oncat' + cat_listid + '" data-cat_listid="' + cat_listid + '"> \
									<div class="category-name-wrapper"> \
										<div> \
											<span class="bgcolor6"></span> \
											<span class="bgcolor6"></span> \
											<span class="bgcolor6"></span> \
											<span class="bgcolor6"></span> \
										</div> \
										<h2 class="color6">' + catname + '</h2> \
									</div> \
									<div class="category-products-list"></div> \
									<span class="backto-categories color6" href="javascript:;" data-cat_listid="' + cat_listid + '"><span class="back-icon-img"></span> Back</span> \
								</div>';
					$('.b-product-listing-wrapper').prepend( prodlist );
					$( ".oncat" + cat_listid + " .category-products-list" ).droppable( drpOptions2 );
					cat_listid++;
				}
			}
		});
		
		
	});
	
	$(document).on('click', '.cat-link', function(){
		var clist_id = $(this).attr('data-cat_listid');
		$('.b-category-wrapper').addClass('hide');
		$('.b-product-listing-wrapper').removeClass('hide');
		$('.oncat' + clist_id).removeClass('hide');
		$('.cat-lbl').addClass('hide');
	});
	$(document).on('click', '.backto-categories', function(){
		var clist_id = $(this).attr('data-cat_listid');
		$('.oncat' + clist_id).addClass('hide');
		$('.b-product-listing-wrapper').addClass('hide');
		$('.b-category-wrapper').removeClass('hide');
		$('.cat-lbl').removeClass('hide');
	});
	
	$('.b-contain-left-data').slimScroll({
		height: '474px',
		width: '716px',
		start: 'top',
		wheelStep : 2,
		disableFadeOut: true
	});
	
	$('.added-category-wrapper').slimScroll({
		height: '410px',
		width: '263px',
		start: 'top',
		wheelStep : 2,
		disableFadeOut: true
	});
	
	$(document).on('touchstart mouseover', '.category-products-list', function(){
		if(! $(this).parent('div').hasClass('slimScrollDiv')){
			$(this).slimScroll({
				height: '356px',
				width: '232px',
				start: 'top',
				wheelStep : 2,
				disableFadeOut: true
			});
		}
	});
	
	$(document).on('keypress', '#search_product', function(e){
		e.stopImmediatePropagation();
		if (e.which == 13) {
			$(this).removeClass('input_error');
			var search_text = $.trim($(this).val());
			
			if(search_text == ""){
				$(this).addClass('input_error');
				return false;
			}
			
			var $form = $(this),
				form_data = {};
				
			form_data.search_text = search_text;
			form_data.isAjax = true;
			//showLoader();
			SSAjax({
				data: form_data,
				url: "/admin/build?action=search",
				success: function(response){
					$('.b-contain-left-data ul').html(response);
					$('.category-products').each(function(){
						var prodlist_id = $(this).find('.product-del').attr('data-prodlistid');
						$('.b-prod-l' + prodlist_id).addClass('prod-list-added');
					});
					$( ".b-contain-left li" ).draggable( drgOptions );
					var total_page = $('#h-total_pages').val(),
						current_page = $('#h-current_page').val();
					if(total_page > current_page){ 
						$('.more-product').removeClass('hide'); 
					}else{
						$('.more-product').addClass('hide'); 
					}
					$('.loader-wrapper').hide();
				}
			});
			return false;
		}
	});
	
	$(document).on('touchstart click', '.search-btn', function(e){
		e.stopImmediatePropagation();
		$('.more-product').addClass('hide'); 
		$('#search_product').removeClass('input_error');
		var search_text = $.trim($('#search_product').val());
		
		if(search_text == ""){
			$('#search_product').addClass('input_error');
			return false;
		}
		
		var page = root;
			form_data = {},
			form_data.search_text = search_text;
		$('#h-search_text').remove();
		$('#h-total_pages').remove();
		$('#h-current_page').remove();
		$('.loader-wrapper').show();
		$.post(page, form_data,function(data){
			$('.b-contain-left-data ul').html(data);
			$('.category-products').each(function(){
				var prodlist_id = $(this).find('.product-del').attr('data-prodlistid');
				$('.b-prod-l' + prodlist_id).addClass('prod-list-added');
			});
			$( ".b-contain-left li" ).draggable( drgOptions );
			var total_page = $('#h-total_pages').val(),
				current_page = $('#h-current_page').val();
			if(total_page > current_page){ 
				$('.more-product').removeClass('hide'); 
			}else{
				$('.more-product').addClass('hide'); 
			}
			$('.loader-wrapper').hide();
		});
	
		return false;
	});
	
	$(document).on('touchstart click', '.more-btn', function(e){
		e.preventDefault();
		e.stopImmediatePropagation();
		$('.loader-image').removeClass('hide');
		var page_num = parseInt($('#h-current_page').val()) + 1;
		var search_text = $.trim($('#h-search_text').val());
		$('#h-search_text').remove();
		$('#h-total_pages').remove();
		$('#h-current_page').remove();
		var page = root + '?page=' + page_num;
			form_data = {},
			form_data.search_text = search_text;
			form_data.more = true;

		$.post(page, form_data,function(data){
			$('.b-contain-left-data ul').append(data);
			$('.category-products').each(function(){
				var prodlist_id = $(this).find('.product-del').attr('data-prodlistid');
				$('.b-prod-l' + prodlist_id).addClass('prod-list-added');
			})
			$( ".b-contain-left li" ).draggable( drgOptions );
			var tpage = $('#tpage').val(),
				current_page = $('#h-current_page').val();
			if(tpage > current_page){
				$('.more-product').removeClass('hide'); 
			}else{
				$('.more-product').addClass('hide'); 
			}
			$('.loader-image').addClass('hide');
		});
	
		return false;
	});
	
	$(document).on('touchstart click', '.more-button', function(e){
		e.preventDefault();
		e.stopImmediatePropagation();
		$('.loader-image').removeClass('hide');
		var page_num = parseInt($('#h-current_page').val()) + 1;
			search_text = $.trim($('#h-search_text').val()),
			page = '/admin/departments?action=load-products&page=' + page_num,
			department_id = $('#search-dept-id').val(),
			level = $('#search-dept-level').val(),
			more = true,
			isAjax = true;
		loadDepartmentProducts({department_id: department_id, level: level, search_text: search_text, more: more, page_num: page_num});
		return false;
	});
	
	$(document).on('keypress', '#search-products', function(e){
		e.stopImmediatePropagation();
		if (e.which == 13) {
			$(this).removeClass('input_error');
			var search_text = $.trim($(this).val());
			
			if(search_text == ""){
				$(this).addClass('input_error');
				return false;
			}
			
			var department_id = $('#search-dept-id').val(),
				level = $('#search-dept-level').val(),
				search_text = search_text,
				isAjax = true;
			loadDepartmentProducts({department_id: department_id, level: level, search_text: search_text});
			return false;
		}
	});
	
	$(document).on('touchstart click', '#search-products-btn', function(e){
		e.stopImmediatePropagation();
		console.log('search');
		$('#search-products').removeClass('input_error');
		var search_text = $.trim($('#search-products').val());
		
		if(search_text == ""){
			$('#search-products').addClass('input_error');
			return false;
		}
		
		var department_id = $('#search-dept-id').val(),
			level = $('#search-dept-level').val(),
			search_text = search_text,
			isAjax = true;
		loadDepartmentProducts({department_id: department_id, level: level, search_text: search_text});
		return false;
	});
});

function catResponse(responseText, statusText, xhr, $form){
	$('.text-pre-loader-style').remove();
	$('.cmd-wrapper').removeClass('hide');
	var _data = $.parseJSON(responseText); 
	if(_data.status == 'failed'){
		$('#display_message').html(_data.error_msg);
		return false;
	}
	
	
	var category_id = _data.category_id;
	var catname = $.trim($('#category_name').val());
	var parent = $('#parent_cat').val();
	var parent_name = $( "#parent_cat option:selected" ).text();
	if(catname != ''){
		var tmp_img = $('.tmp-img').html(),
			tmp_plistid = $('.tmp-plistid').val(),
			tmp_pid = $('.tmp-pid').val(),
			tmp_puid = $('.tmp-puid').val(),
			catlist_wrapper = '<div id="cat-list-wrapper" class="cat-list-wrapper-'+ category_id +'"></div>',
			catlist = '<div class="category-list ui-droppable cat-link main-cat-list-'+ category_id +' category-list' + cat_listid + '" data-cat_listid="' + cat_listid + '"> \
							<div class="b-cat-box1"></div> \
							<div class="b-cat-box2"></div> \
							<div class="b-cat-box3"></div> \
							<div class="b-cat-box4"> \
								<div class="main-category-wrapper main-cat-'+ category_id +'">\
									<span class="cat-title main-cat-name color1" data-main_catid="' + category_id + '" data-cat_listid="' + cat_listid + '">' + catname + '</span> \
									<div class="product-images" data-scid="' + category_id + '"> \
										<span data-scid="' + category_id + '" data-pid="' + tmp_pid + '" data-puid="' + tmp_puid + '" class="p-image prodlistid' + tmp_plistid + '" style="' + tmp_img + '"></span> \
									</div> \
									<span class="color1 more-lbl" data-cat_listid="' + cat_listid + '"></span> \
								</div> \
							</div> \
						</div>',
			subcatlist_wrapper = '<div class="sub-category-list"> \
							<div class="b-cat-box1"></div> \
							<div class="b-cat-box2"></div> \
							<div class="b-cat-box3"></div> \
							<div class="b-cat-box4"> \
							<div class="sub-category-wrapper"> \
								<span class="main-cat-title main-cat-name color1" data-main_catid="' + parent + '">' + parent_name + '</span> \
							</div> \
						</div>',
			subcatlist = '<div class="sub-cat-list bgcolor3 cat-link sub-category-list' + cat_listid + '" data-cat_listid="' + cat_listid + '"> \
									<span class="cat-title color1" data-cat_listid="' + cat_listid + '">' + catname + '</span> \
										<div class="product-images" data-scid="' + category_id + '"> \
											<span data-scid="' + category_id + '" data-pid="' + tmp_pid + '" data-puid="' + tmp_puid + '" class="p-image prodlistid' + tmp_plistid + '" style="' + tmp_img + '"></span> \
										</div> \
									<span class="color1 more-lbl" data-cat_listid="' + cat_listid + '"></span> \
								</div>';
		$('#add-new-category-inpt').val('');
		if(parent == 0){
			$('.added-category-wrapper').prepend(catlist_wrapper);
			$( '.cat-list-wrapper-' + category_id ).html(catlist);
			$( '.main-cat-list-'+ category_id ).droppable( drpOptions );
		}
		
		var transfer_status = false;
		if(parent > 0){
			if($('.main-category-wrapper').hasClass('main-cat-' + parent)){
				if(parseInt($('.main-cat-' + parent + ' .p-image').length) > 0){
					if(parseInt($('.cat-list-wrapper-' + parent + ' .sub-category-wrapper .sub-cat-list').length) > 0){
						$( subcatlist ).droppable( drpOptions ).appendTo( '.cat-list-wrapper-' + parent + ' .sub-category-wrapper' );
					}else{
						transfer_status = true;
						var main_product = $('.main-cat-' + parent + ' .product-images').html();
						var old_list_id = $('.main-cat-' + parent + ' .main-cat-name').attr('data-cat_listid');
						$('.cat-list-wrapper-' + parent).html(subcatlist_wrapper);
						$( subcatlist ).droppable( drpOptions ).appendTo( '.cat-list-wrapper-' + parent + ' .sub-category-wrapper' );
						$( '.cat-list-wrapper-' + parent + ' .product-images' ).append(main_product);
						$( '.cat-list-wrapper-' + parent + ' .product-images' ).find('.p-image').attr('data-scid', category_id);
					}
				}else{
					$('.cat-list-wrapper-' + parent).html(subcatlist_wrapper);
					$( subcatlist ).droppable( drpOptions ).appendTo( '.cat-list-wrapper-' + parent + ' .sub-category-wrapper' );
				}
			}else{
				if($('.cat-list-wrapper-' + parent + ' .sub-category-wrapper .sub-cat-list').length > 0){
					$( subcatlist ).droppable( drpOptions ).appendTo( '.cat-list-wrapper-' + parent + ' .sub-category-wrapper' );
				}else{
					$('.cat-list-wrapper-' + parent).html(subcatlist_wrapper);
					$( subcatlist ).droppable( drpOptions ).appendTo( '.cat-list-wrapper-' + parent + ' .sub-category-wrapper' );
				}
			}
			
			var sub_img_length = $('.sub-category-list' + cat_listid).find('.p-image').length;
			if(sub_img_length > 1){ 
				$('.sub-category-list' + cat_listid).parents('.sub-category-list').addClass('more');
			}
			if(parseInt($('.sub-category-list' + cat_listid).parents('#cat-list-wrapper').find('.sub-cat-list').length) > 1){
				$('.sub-category-list' + cat_listid).parents('.sub-category-list').addClass('more');
			}
		}
		$('.add-new-cat-box').addClass('hide');
		$('.add-new-cat-btn').removeClass('hide');
		var prodlist = '<div class="category-products-wrapper hide oncat' + cat_listid + '" data-cat_listid="' + cat_listid + '"> \
							<div class="category-name-wrapper"> \
								<div> \
									<span class="bgcolor6"></span> \
									<span class="bgcolor6"></span> \
									<span class="bgcolor6"></span> \
									<span class="bgcolor6"></span> \
								</div> \
								<h2 class="color6">' + catname + '</h2> \
							</div> \
							<div class="category-products-list"> \
								<div class="category-products prodlistid' + tmp_plistid + '" style="' + tmp_img + '"> \
									<span class="product-del" data-prodlistid="' + tmp_plistid + '">&#215;</span> \
									<span class="earn-bottom"></span> \
									<span class="earn-top color3">Earn $180</span> \
								</div> \
							</div> \
							<span class="backto-categories color6" href="javascript:;" data-cat_listid="' + cat_listid + '"><span class="back-icon-img"></span> Back</span> \
						</div>';
		$('.b-product-listing-wrapper').prepend( prodlist );
		if(transfer_status){
			var product_lists = $('.oncat' + old_list_id + ' .category-products-list').html();
			$('.oncat' + cat_listid + ' .category-products-list').append(product_lists);
		}
		$('.b-prod-l' + tmp_plistid).addClass('prod-list-added');
		$( ".oncat" + cat_listid + " .category-products-list" ).droppable( drpOptions2 );
		cat_listid++;
		if($('.category-list').length > 0){
			$('.build-promoter').removeClass('hide');
			$('.build-promoter2').removeClass('hide');
			$('.upload-for-vendor').slideUp(500);
			$('.tips-box').addClass('hide');
		}
		$("#addfile").html('<span class="upload-bg"></span><span class="upload-lbl">Upload Image</span>');
		resetCategoryForm();
	}
	return false;
}

function resetCategoryForm(){
	$('.tmp-plistid').val('');
	$('.tmp-pid').val('');
	$('.tmp-puid').val('');
	$('#category_name').val('');
	$('#description').val('');
	$('#add_myfile').val('');
	$('.file_exist').val('');
	$('.filename').html('');
	$('.more-cat-details').addClass('hide');
	$('.upload-img-btn').removeClass('hide');
}