$(document).foundation();
$(document).ready(function(){
	$(document).on('focus', 'input', function(){
		$(this).parent('.common_input_wrapp').find('.dynamic_error').html('');
	});
	$(document).on('keyup', '#store-name', function(){
		$('#store-url').val(convertToSlug($(this).val()));
	});
	$(document).on('click', '#proceed-to-catalog', function(){
		$('#save-shop-info').trigger('click');
	});
	$(document).on('click', '#save-shop-info', function(){
		$(this).parents("form").trigger("submit");
		return false;
	});
	$(document).on('submit', '#buildform', function(e){
		e.preventDefault(); e.stopImmediatePropagation();
		
		var $form = $(this),
			form_data = {};
			
		form_data.store_name = $form.find( 'input[name="store_name"]' ).val();
		form_data.store_description = $form.find( 'textarea[name="store_description"]' ).val();
		form_data.store_colors = $form.find('.shop-colors .selected').attr('data-color');
		form_data.store_url = $form.find( 'input[name="store_url"]' ).val();
		form_data.country_code = $form.find( 'select[name="country_code"]' ).val();
		form_data.user_type = $form.find( 'input[name="user_type"]' ).val();
		form_data.isAjax = true;
		showLoader();
		SSAjax({
			data: form_data,
			url: "/admin/build?action=save-shop-info",
			success: function(response){
				var _data = $.parseJSON(response);
				if( _data.errors){
					if(_data.errors.store_url !== "undefined"){
						$('.url_error').html(_data.errors.store_url);
					}
					if(_data.errors.store_name !== "undefined"){
						$('.store_name_error').html(_data.errors.store_name);
					}
					hideLoader();
					return false;
				}
				if(! _data.errors){
					stepRedirect(2);
				}
			}
		});
		
	});
	
	$(document).on('open', '#shopCatalogModal', function (e) {
		$(this).centerized();
		e.preventDefault(); e.stopImmediatePropagation();
		$('.search-box input').attr('placeholder', 'Search in All Department Products...');
		$('#search-dept-id').val('all');
		loadDepartments({parent_id:0, department_level: 'main', loadProducts: true, department_id: 'all'});
	});
	
	$(document).on('click', '#department-main li', function (e) {
		e.preventDefault(); e.stopImmediatePropagation();
		$('#department-main li').removeClass('active');
		$(this).addClass('active');
		$(this).append('<span class="department-loading bgcolor3">Loading...</span>');
		var department_level = $(this).attr('data-target_level');
		$('#department-name').html($(this).find('.dname label').html());
		var placeholder = $(this).find('.dname label').html();
		placeholder = placeholder.replace("&amp;", "&");
		$('.search-box input').attr('placeholder', 'Search in ' + placeholder + '...');
		$('#search-dept-id').val($(this).attr('data-id'));
		$('#search-dept-level').val('main');
		loadDepartments({parent_id:$(this).attr('data-id'), department_level: department_level, level: 'main', department_title:$(this).find('.main-dept-title').html(), loadProducts: true, department_id: $(this).attr('data-id')});
	});
	
	$(document).on('click', '#back-departments', function (e) {
		e.preventDefault(); e.stopImmediatePropagation();
		$('#department-sub-menus').addClass('hide');
		$('#department-menus').removeClass('hide');
		$('.all-dept-lbl').removeClass('hide');
		//$('.dept-spacing').addClass('hide');
		$('#department-name').html($(this).attr('data-catname'));
		var placeholder = $(this).attr('data-catname');
		placeholder = placeholder.replace("&amp;", "&");
		$('.search-box input').attr('placeholder', 'Search in ' + placeholder + '...');
		$('#search-dept-id').val($(this).attr('data-id'));
		$('#search-dept-level').val('main');
		loadDepartmentProducts({department_id:$(this).attr('data-id'), level: 'main'});
	});
	
	$(document).on('click', '#department-sub li', function (e) {
		e.preventDefault(); e.stopImmediatePropagation();
		if(!$(this).hasClass('active')){
			$('#department-sub li').removeClass('active');
			$(this).addClass('active');
			$('#department-name').html($(this).find('.dname label').html());
			var placeholder = $(this).find('.dname label').html();
			placeholder = placeholder.replace("&amp;", "&");
			$('.search-box input').attr('placeholder', 'Search in ' + placeholder + '...');
			$('#search-dept-id').val($(this).attr('data-id'));
			$('#search-dept-level').val('sub');
			loadDepartmentProducts({department_id: $(this).attr('data-id'), level: 'sub'});
		}
	});
	
	$(document).on('touchstart click', '#department-name', function(){
		$('.shop-left').removeClass('hide2');
	});
	
	$(document).on('touchstart click', '.upload-img-btn', function(){
		$(this).addClass('hide');
		$('.more-cat-details').removeClass('hide');
	});
	
	$(document).on('click', '#addfile', function(e){
		e.stopImmediatePropagation();
		$("#add_myfile").click();
		return false;
	});
	
	$(document).on('change', '#add_myfile', function(e){
		e.stopImmediatePropagation();
		$(".file_exist").val(1);
		
		var input = this;
		if (input.files && input.files[0]) {

			var ftype = input.files[0].type;
			var fname = input.files[0].name.split('.');
			var flength = fname.length-1;
			var fext = fname[flength];
			if (!ftype.match('image.*') && !check_allowed_image_filetype(ftype,fext)) {
				return;
			}

			var reader = new FileReader();

			reader.onload = function (e) {
				$('#addfile').html('<img src="'+e.target.result+'" />');
			};

			reader.readAsDataURL(input.files[0]);
		}
	});
	
	$(document).on('click', '.create-btn', function(e){
		e.preventDefault();
		e.stopImmediatePropagation();
		$(this).parents("form").trigger("submit");
        return false;
	});
	
	$(document).on('submit', '#categoryForm', function(e){
		e.preventDefault();
		e.stopImmediatePropagation();
		var options = {
			target:        '#display_message',
			success:       catResponse
		};
		$('.action-wrapper').append('<span id="text-pre-loader" class="text-pre-loader-style bgcolor1 color3">Saving<span class="dots"></span></span>');
		$('.cmd-wrapper').addClass('hide');
		$('#display_message').html('');
        $(this).ajaxSubmit(options);
        return false; 
    });
	
	$( window ).resize(function() {
		$('#shopCatalogModal').centerized();
	});
	
	$(document).on('click', '#back-btn1', function(){
		showLoader();
		stepRedirect(1);
	});
	$(document).on('click', '#back-btn2', function(){
		showLoader();
		stepRedirect(2);
	});
	$(document).on('click', '#back-btn3', function(){
		showLoader();
		stepRedirect(3);
	});
	$(document).on('touchstart click', '.more-promoter-button', function(e){
		e.preventDefault();
		e.stopImmediatePropagation();
		$('.loader-image').removeClass('hide');
		var page_num = parseInt($('#c-current_page').val()) + 1;
			more = true,
			isAjax = true;
		$('#c-search_text').remove();
		$('#c-total_pages').remove();
		$('#c-current_page').remove();
		loadProducts({more: more, page_num: page_num});
	});
	
	$(document).on('touchstart click', '#build-catalog-btn', function(e){
		e.preventDefault(); e.stopImmediatePropagation();
		var form_data = {};
		form_data.isAjax = true;
		showLoader();
		getPromoterData();
		SSAjax({
			data: form_data,
			url: "/admin/build?action=save-promoter-catalog",
			success: function(response){
				var dpromote = store.get('promoter_data');
				if(typeof dpromote !== "undefined"){
					SSAjax({
						url: '/admin/build?action=save-products',
						data: {isAjax:true,isContent:true,promote_data:dpromote},
						success: function(response){
							var _data = $.parseJSON(response);
							if( _data.no_error){
								store.remove('promoter_data');
								stepRedirect(2);
							}
						}
					});
				}
			}
		});
		
	});
	
	$(document).on('click','#addproduct', function(e){
		e.preventDefault(); e.stopImmediatePropagation();
		
		addproduct({
			data:{product_id:0, formType:'basic'},
			onSaved: function(data) {
					var form_data = {};
					form_data.isAjax = true;
					SSAjax({
						data: form_data,
						url: "/admin/build?action=save-user-type",
						success: function(response){ 
							$('.reveal-modal-bg').css({'display': 'none'}); 
							stepRedirect(2); 
						}
					});
				}
			// beforeClose: function(){
				// console.log('closing the window');
			// }			
					
		});
	});
	$(document).on('click','#add-product', function(e){
		e.preventDefault(); e.stopImmediatePropagation();
		
		addproduct({
			data:{product_id:0, formType:'basic'},
			onSaved: function(data) {
				stepRedirect(2);
			}
		});
	});
	
	/* shipping */
	$(document).on('click','.wz-ap-content-shipping table td.col3 label', function(e) {
		e.preventDefault(); e.stopImmediatePropagation();

		var checkbox = $(this).find('.checkbox-wrapper');
		var radio = $(this).find('.radio-wrapper');

		var input = (checkbox.length > 0) ? checkbox : radio;
		var type = (checkbox.length > 0) ? 'checkbox' : 'radio';

		if(input.parents('.shipping-option-wrapper').hasClass('disabled') 
		|| input.hasClass('readonly')) {
			input.find('input').removeAttr('checked');
			return;
		}

		if(type == 'checkbox') {
			if(input.hasClass('checked')) {
				input.removeClass('checked');
				input.find('input').removeAttr('checked');

				input.parents('.shipping-option-wrapper')
					.find('.input-wrapper')
					.addClass('disabled')
					.find('input')
					.attr('disabled','disabled');
			}
			else {
				input.addClass('checked');
				input.find('input').attr('checked','checked');

				input.parents('.shipping-option-wrapper')
					.find('.input-wrapper')
					.removeClass('disabled')
					.removeAttr('disabled')
					.find('input').removeAttr('disabled').focus();

			}
		}
		else {
			input.parents('.col3').find('.radio-wrapper').removeClass('checked')
				.find('input')
				.removeAttr('checked');

			input.addClass('checked');
			input.find('input').attr('checked','checked');
		}
	});

	$(document).on('click','.wz-ap-content-shipping table td.col3 a', function(e) {
		e.preventDefault(); e.stopImmediatePropagation();

		$(this).parents('tr').remove();

		if($('#global-shipping-settings tr.data:not(.hide)').length == 0) {
			$('.wz-ap-content-shipping tr.error-shipping').removeClass('hide');

			var country_code = $('.wz-ap-content-shipping .shipsfrom select option:selected').val();
			$('.wz-ap-content-shipping #addnewshipping-country').val(country_code);
		}
	});

	$(document).on('click','.wz-ap-content-shipping #add-new-shipping', function(e) {
		e.preventDefault(); e.stopImmediatePropagation();

		var country = $('.wz-ap-content-shipping #addnewshipping-country');
		var country_code = country.val();
		var country_name = country.find('option:selected').text();
		var shipping_rate = $('.wz-ap-content-shipping #addnewshipping-shippingrates');
		var sp_name = shipping_rate.find('option:selected').text();
		var sp_value = shipping_rate.val();
		var ships_from = $('.shipsfrom select').val(); 
		
		country.parent().addClass('error').find('small').html("");
		
		if(country_code == "") {
			country.parent().addClass('error').find('small').html("Please select a country.");
			return;
		}
		
		var countries = $('.wz-ap-content-shipping #global-shipping-settings tr.data').map(function() {
						return $(this).attr('country_code');
					}).get();

		if($.inArray( country_code, countries ) != -1) {
			country.parent().addClass('error').find('small').html("Country already exists.");
			return;
		}
		country.parent().removeClass('error');

		var tr = $('.wz-ap-content-shipping #global-shipping-settings tr.data.hide').clone();

		if(sp_value == 0) {
			sp_name = "FEDEX";
			tr.find('.col3 .ac-shipping-txtwrapper').removeClass('hide').find('.carrier-txt').html(sp_name);
		}	
		tr.removeClass('hide').attr('country_code',country_code);
		tr.find('.col1 span').html(country_name);
		tr.find('.col1').find('input.shipping-type-input').attr('name','shipping_global_meta['+country_code+'][shipping_type]').val(sp_value);
		tr.find('.col1').find('input.shipping-carrier-input').attr('name','shipping_global_meta['+country_code+'][carrier]').val('FEDEX');
		tr.find('em').html(sp_name);
		tr.find('.col3').find('.selections').eq(0).find('input').attr('name','shipping_global_meta['+country_code+'][options][standard]');
		tr.find('.col3').find('.selections').eq(1).find('input').attr('name','shipping_global_meta['+country_code+'][options][express]');
		tr.find('.col3').find('.selections').eq(2).find('input').attr('name','shipping_global_meta['+country_code+'][options][next_day]');
		tr.find('.col3').find('.input-wrapper input').eq(0).attr('name','shipping_global_meta['+country_code+'][rate][standard]').val('0.00');
		tr.find('.col3').find('.input-wrapper input').eq(1).attr('name','shipping_global_meta['+country_code+'][rate][express]').val('0.00');
		tr.find('.col3').find('.input-wrapper input').eq(2).attr('name','shipping_global_meta['+country_code+'][rate][next_day]').val('0.00');
		
		if(sp_value != 2) {
			tr.find('.col3')
				.find('.selections')
				.addClass('checkbox-wrapper')
				.find('input')
				.attr('type','checkbox');
		} else {
			tr.find('.col3')
				.find('.selections')
				.addClass('radio-wrapper')
				.find('input')
				.attr('type','radio');
			tr.find('.col3').find('.selections').eq(0).addClass('checked')
				.find('input').attr('checked','checked');
		}

		tr.find('.selections input').removeAttr('disabled');
		$('.wz-ap-content-shipping #global-shipping-settings tr.data').removeClass('noborder');

		if(sp_value != 1) {
			tr.find('.input-wrapper').addClass('hide');
		}

		if(ships_from == "US" && country_code == "US" && sp_value == 0) {
			//tr.find('.input-wrapper:last')
		}

		$('.wz-ap-content-shipping tr.error-shipping').addClass('hide');
		$('.wz-ap-content-shipping #global-shipping-settings tr.data:last').after(tr);
	});

	$(document).on('click','.wz-ap-content-shipping .select.shippingrates .opts div', function(e) {
		e.preventDefault(); e.stopImmediatePropagation();
		
		var val = $(this).attr('value');
		var carrier = $(this).attr('carrier');
		var text = $(this).html();
		var col2 = $(this).parents('tr').find('.col1');
		var col3 = $(this).parents('tr').find('.col3');

		if(val == "" || typeof val == "undefined") {
			if(!$(this).hasClass('auto')) {
				// Call Subscription Modal Here (to be able to upgrade a plan)

				col2.find('.select.shippingrates .outer').hide();
				setTimeout(function(){ col2.find('.select .outer').attr('style','') }, 400);

			}
			return;
		}

		$(this).addClass('selected');
		if(val == 0) {
			text = "Carrier Calculated";
			text = carrier;

			col3.find('.ac-shipping-txtwrapper').removeClass('hide');
			col3.find('.ac-shipping-txtwrapper .carrier-txt').html(text);
		}
		else {
			carrier = "";
			col3.find('.ac-shipping-txtwrapper').addClass('hide');
		}

		col2.find('.select.shippingrates em').html(text);
		col2.find('.select.shippingrates .outer').hide();
		col2.find('input.shipping-type-input').val(val);
		col2.find('input.shipping-carrier-input').val(carrier);
		setTimeout(function(){ col2.find('.select .outer').attr('style','') }, 400);

		if(val !=2) {
			col3.find('.selections')
				.addClass('checkbox-wrapper')
				.removeClass('radio-wrapper')
				.find('input')
				.attr('type','checkbox');
		}
		else {
			col3.find('.selections')
				.addClass('radio-wrapper')
				.removeClass('checkbox-wrapper')
				.find('input')
				.attr('type','radio');
			col3.find('.selections').eq(0).addClass('checked')
				.find('input').attr('checked','checked');
		}
		col3.find('.selections input').removeAttr('disabled');

		// If from flat rate or auto calculated to free shipping have more than 1 checked button, 
		// make the Standard as the only free shipping
		if(val == 2 && col3.find('.selections.checked').length > 1) {
			var el = col3.find('.selections.checked').eq(0);

			col3.find('.selections').removeClass('checked')
				.find('input').removeAttr('checked');
			col3.find('.input-wrapper input').val('0.00').attr('readonly','readonly');
			el.addClass('checked')
				.find('input').attr('checked','checked');
		} 
		else {
			col3.find('.selections').each(function() {
				if(!$(this).hasClass('checked')) {
					$(this).find('input').attr('readonly','readonly')
						.parents('.shipping-option-wrapper')
						.find('.input-wrapper').addClass('disabled')
						.find('input').val('0.00').attr('disabled','disabled');
				}
				else {
					$(this).find('input').removeAttr('readonly')
						.parents('.shipping-option-wrapper')
						.find('.input-wrapper').removeClass('disabled')
						.find('input').removeAttr('disabled').removeAttr('readonly');
				}
			});
		}

		if(val != 1) {
			col3.find('.input-wrapper').addClass('hide');
		}
		else {
			col3.find('.input-wrapper').removeClass('hide');
		}
	});

	$(document).on('mouseover','.wz-ap-content-shipping .select .outer', function(e) {
		$('.wz-ap-content-shipping .select.shippingrates .opts div').removeClass('selected');
	});

	$(document).on('mouseleave','.wz-ap-content-shipping .select', function(e) {
		var row = $(this).parents('tr');
		var val = row.find('.col1 input.shipping-type-input').val();
		var carrier = row.find('.col1 input.shipping-carrier-input').val();

		if(val != 0) {
			row.find('.col1 .select .opts div[value="'+val+'"]').addClass('selected');
		}
		else {
			row.find('.col1 .select .opts div[carrier="'+carrier+'"]').addClass('selected');
		}
	});
	
	$(document).on('click', '#payment-settings-btn', function(){
		$('.footer-message').removeClass('footer-message-display');
		$('.footer-message').html('');
		showLoader();
		var shipping_data = wz_shipping_form_params();
		if($.isEmptyObject(shipping_data)) {
			$('.wz-ap-content-shipping tr.error-shipping').removeClass('hide');
			hideLoader();
		}
		else {
			$.extend(shipping_data, {isAjax:true});
			SSAjax({
				url: "/admin/build?action=save-shipping",
				data: shipping_data,
				success: function(response){
					var data = $.parseJSON(response);
					if(data.hasOwnProperty('error')) {
						$('.footer-message').addClass('footer-message-display');
						$('.footer-message').html(data.error);
						hideLoader();
					} else {
						stepRedirect(3);
					}
				}
			});
			$('.wz-ap-content-shipping tr.error-shipping').addClass('hide');
		}
	});
	
	$(document).on('click', '#proceed-to-next-step', function(e){
		e.preventDefault(); e.stopImmediatePropagation();
		$('.footer-message-display').remove();
		showLoader();
		SSAjax({
			url: "/admin/build?action=check-product",
			data: {isAjax:true},
			success: function(response){
				var data = $.parseJSON(response);
				if(data.has_product){
					SSAjax({
						url: "/admin/build?action=prepare-to-next-step",
						data: {step: 3, isAjax:true},
						success: function(response){
							stepRedirect(3);
						}
					});
				}else{
					$('.footer-message').html('<p class="footer-message-display">Please upload products</p>');
					hideLoader();
				}
				
			}
		});
	});
	
	$(document).on('click', '#get-paid-settings-btn', function(){
		$('.get-paid-btn').trigger('click');
	});
	$(document).on('click', '.get-paid-btn', function(){
		$(this).parents("form").trigger("submit");
		return false;
	});
	$(document).on('submit', '#getPaidForm', function(event) {
		event.preventDefault(); 
		event.stopImmediatePropagation();
		
		var form = $(this);
		var form_data = $(this).serialize();
		var token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
		var user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
		var isAjax = true;
		showLoader();
		$.ajax({
			type: "POST",
			url: '/admin/build?action=save-getpaid',
			data: form_data + '&token='+token+'&user_id='+user_id+'&isAjax='+isAjax,
			success: function(data){
				var _data = $.parseJSON(data);
				if(_data.errors == false) {
					stepRedirect(4);
				}
				else {
					for(var prop in _data.errors) {
						if (_data.errors.hasOwnProperty(prop)) {
							console.log(prop);
							
							if(prop != "statecityzip") {
								// handle prop as required
								form.find('.' + prop).html(_data.errors[prop]);
								form.find('.' + prop).parent('.common_input_wrapp').addClass('error');
							}
							else {
								form.find('.shipping_city').html(_data.errors[prop]);
								form.find('.shipping_city').parent('.common_input_wrapp').addClass('error');
							}
						}
					}
				}
			}
		});
		return false;
	});
	
	$(document).on('click', '#proceed-btn', function(e){
		e.preventDefault(); e.stopImmediatePropagation();
		showLoader();
		SSAjax({
			url: "/admin/build?action=prepair-to-dashboard",
			data: {isAjax:true},
			success: function(response){
				var data = $.parseJSON(response);
				if(data.errors){
					$('.more-preview-lbl').html('Please confirm your email address to proceed to your dashboard.');
					hideLoader();
					return false;
				}else{
					stepRedirect(data.step);
				}
			}
		});
	});
	
	$(document).on('mouseover', '.common_input_wrapp input, .common_input_wrapp textarea, #shipping_country_code, #shipping_state_code', function(){
		$(this).parents('.common_input_wrapp').removeClass('error');
		$(this).parents('.customized_select').removeClass('error');
	});
	
	$(document).on('change', 'select#shipping_country_code', function(){
		var c = $(this).val();
		$('input#shipping_city').val('');
		$('input#shipping_zip').val('');
		$('.state-province-region').removeClass('error');
		if(c == 'US') {
			$('#shipping_state_name').remove();
			$('.shipping-state-code select#shipping_state_code').remove();
			$('.shipping-state-code').append($('.shipping-state-code-tmp').html());
			
			
			if($('select#shipping_state_code').val() != '') {
				toggle_disable_shippingcityzip('enable');
				toggle_disable_shipping_autocomplete('enable');
			}
			else {
				toggle_disable_shippingcityzip();
				toggle_disable_shipping_autocomplete();
			}
		}
		else {
			$('input#shipping_city'.target).find('input').autocomplete( "option", "disabled", true );
			$('input#shipping_zip'.target).find('input').autocomplete( "option", "disabled", true );
			$('#shipping_state_name').remove();
			$('.state-province-region').append('<input type="text" required class="comm-inpt-height" value="" id="shipping_state_name" name="state_name" required>');
			$('.shipping-state-code select#shipping_state_code').remove();
			toggle_disable_shippingcityzip('enable');
			toggle_disable_shipping_autocomplete(); // disable auto complete for Non-US state				
		}
	});
	
	$(document).on('change', 'select#shipping_state_code', function(){
		$('input#shipping_city').val('');
		$('input#shipping_zip').val('');
		
		
		$(this).parents('.common_input_wrapp').removeClass('error');
		$(this).parent('.customized_select').removeClass('error');
		
		if( $(this).val() == '') {
			toggle_disable_shippingcityzip();
			toggle_disable_shipping_autocomplete();
		}
		else {
			console.log('not empty');
			$('input#shipping_city').removeAttr('disabled');
			$('input#shipping_zip').removeAttr('disabled');
			$('input#shipping_city').parent('.common_input_wrapp').removeClass('error');
			$('input#shipping_zip').parent('.common_input_wrapp').removeClass('error');
			$('input#shipping_city').parent('.common_input_wrapp').find('small.error').html('required.');
			$('input#shipping_zip').parent('.common_input_wrapp').find('small.error').html('required.');
			
			$('input#shipping_city'.target).find('input').autocomplete("enable");
			toggle_disable_shippingcityzip('enable');
			toggle_disable_shipping_autocomplete('enable');
		}
		
	});
			
});

function toggle_disable_shippingcityzip(type) {
	type = (typeof type == 'undefined') ? 'disabled' : type;
	if(type == 'disabled') {
		$('input#shipping_city').attr('disabled','disabled');
		$('input#shipping_zip').attr('disabled','disabled');
		$('input#shipping_city').parent('.common_input_wrapp').find('small.error').html('');
		$('input#shipping_zip').parent('.common_input_wrapp').find('small.error').html('');
	}
	else {
		$('input#shipping_city').removeAttr('disabled');
		$('input#shipping_zip').removeAttr('disabled');
		$('input#shipping_city').parent('.common_input_wrapp').removeClass('error');
		$('input#shipping_zip').parent('.common_input_wrapp').removeClass('error');
		$('input#shipping_city').parent('.common_input_wrapp').find('small.error').html('required.');
		$('input#shipping_zip').parent('.common_input_wrapp').find('small.error').html('required.');
	}
	
}
function toggle_disable_shipping_autocomplete(type) {
	type = (typeof type == 'undefined') ? 'disabled' : type;
	if(type == 'disabled') {
		$('input#shipping_city'.target).find('input').autocomplete( "option", "disabled", true );
		$('input#shipping_zip'.target).find('input').autocomplete( "option", "disabled", true );
	}
	else {
		$('input#shipping_city'.target).find('input').autocomplete("enable");
		$('input#shipping_city').autocomplete({
			//define callback to format results  
			source: function(req, add){  
				//pass request to server  
				$.getJSON("/autocomplete/US_city.html?state_code="+$('select#shipping_state_code').val()+"&callback=?", req, function(data) {  
						//create array for response objects  
						var suggestions = [];  
						//process response  
						$.each(data, function(i, val){  
						suggestions.push(val.name);  
					});  

					//pass array to callback  
					add(suggestions);  
				});  
			}, 
			 select: function(e, ui) {  
				//create formatted friend  
				var city = ui.item.value; 
					$.get("/autocomplete/US_zip.html?state_code="+$('select#shipping_state_code').val()+"&city="+city+"&callback=?", function(data) {
						data = data.replace('?','').replace('(','').replace(')','');
						data = $.parseJSON(data);
						//console.log(data);
						//console.log(data.length);
						if(data.length == 1) {
							$('input#shipping_zip').val(data[0].name);	
						}
						else {
							$('input#shipping_zip').val('');	
						}
					});
				},  
				//define select handler  
				change: function() {  
				}  
		}); 
		
		$('input#shipping_zip'.target).find('input').autocomplete("enable");
		$('input#shipping_zip').autocomplete({  
			//define callback to format results  
			source: function(req, add){  
				//pass request to server  
				$.getJSON("/autocomplete/US_zip.html?state_code="+$('select#shipping_state_code').val()+"&city="+$('input#shipping_city').val()+"&callback=?", req, function(data) {  
						//create array for response objects  
						var suggestions = [];  
						//process response  
						$.each(data, function(i, val){  
						suggestions.push(val.name);  
					});  
					//pass array to callback  
					add(suggestions);  
				});  
			},  
		}); 
		
	}
}

function check_allowed_image_filetype(file_mime,file_ext) {
	file_ext = file_ext.toLowerCase();
	if($.inArray(file_mime,["images/jpeg","images/jpg","images/png","images/gif"]) == -1 &&
		$.inArray(file_mime,["jpeg","jpg","png","gif"]) == -1
		) {
		return false;
	}

	return true;
}


function wz_shipping_form_params() {
	var radioCheck = /radio|checkbox/i;
	var data = {},
				current;
	var keyBreaker = /[^\[\]]+/g;			

	$('.wz-ap-content-shipping input[name^="shipping_global_meta"]').each(function(){
		var el = this,
			type = el.type && el.type.toLowerCase();
		var key = el.name;
		var parts = key.match(keyBreaker),
			value = $.data(el, "value") || $.fn.val.call([el]),
			isRadioCheck = radioCheck.test(el.type),
			write = !isRadioCheck || !! $(el).attr('checked'),
			lastPart;

		//console.log(parts);
		current = data;
		for ( var i = 0; i < parts.length - 1; i++ ) {
			if (!current[parts[i]] ) {
				current[parts[i]] = {};
			}
			current = current[parts[i]];

			//console.log(current);
		}

		lastPart = parts[parts.length - 1];
		if ( lastPart in current && type === "checkbox" ) {
			if (!$.isArray(current[lastPart]) ) {
				current[lastPart] = current[lastPart] === undefined ? [] : [current[lastPart]];
			}
			if ( write ) {
				current[lastPart].push(value);
			}
		} else if ( write || !current[lastPart] ) {
			current[lastPart] = write ? value : undefined;
		}
	});

	return data;
}