var	cloud_url = CS.cloudURL || location.protocol + '//' + location.host + '/';
var	root = CS.siteURL || location.protocol + '//' + location.host + '/';
var vid = CS.vid;
var doNotLoad = false;
var ms_store_key = CS.store_key;
var ms_store_user_id = 0;
var ms_store_id = 0;

$(document).ready(function(){
	$(document).on('click','#stor-name', function() {
		ministore_getMainPage(1);
	});
	
	$(document).on('click','.pinfo-thumb', function() {
		$('.pagination li:nth-child(' + $(this).attr('slideshow_id') + ') a').trigger('click');
	});
	
	$(document).on('click', '.ms-category-name, #ms-cat-lists .ms-category', function(e){
		e.stopImmediatePropagation();
		var data = {};
			data.catname = $(this).find('.ms-cname').html();
			data.total_products = $(this).find('.ms-cat-quantity').html() == null ? $(this).parents('.ms-category').find('.ms-cat-quantity').html() : $(this).find('.ms-cat-quantity').html();
			data.store_category_id = $(this).attr('cid');
		ministore_gotoProductLists(data, 1);
	});
	
	$(document).on('click', '#fbmain-menu .menu-but', function(e){
		e.stopImmediatePropagation();
		var settings = $.parseJSON($(this).attr('datas'));
		ministore_gotoProductLists(settings, 1);
	});
	
	$(document).on('click', '.ms-sub-cname', function(e){
		e.stopImmediatePropagation();
		var data = {};
			data.catname = $(this).find('.subcat_N').html();
			data.total_products = $(this).find('.subcat_total').val();
			data.store_category_id = $(this).attr('cid');
		ministore_gotoProductLists(data, 1);
	});
	
	$(document).on('mouseenter', '.ms-category-slide', function(e){
		e.preventDefault();
		var $this = $(this);
		$(this).find('.ms-cat-top').slideUp(400, function(){
			$this.find('.ms-subcat-list').fadeIn(100);
		});
	});
	
	$(document).on('mouseenter', '.ms-subcat-list', function(e){
		$(this).parents('.ms-category-slide').find('.ms-cat-top').slideUp("fast");
	});
	
	// $(document).on('click', '.ms-subcat-list', function(e){
		// $(this).find('li:nth-child(1)').fadeOut("slow");
	// });
			  
	$(document).on('mouseleave', '.ms-category-slide', function(e){
		e.preventDefault();
		var $this = $(this);
		$(this).find('.ms-cat-top').slideDown("fast", function(){
			$this.find('.ms-subcat-list').fadeOut('fast');
		});
		$(this).find('.ms-subcat-list').fadeOut('fast', function(){
			$this.find('.ms-cat-top').slideDown('fast');
		});
	});
	
	$(document).on('mouseenter', '#ms-footer', function(){
		$('.ms-subcat-list').fadeOut('fast');
	});
	
	
	$(document).on('click', '.ms-back', function(){
		var page = $('#catpage').val();
		ministore_getMainPage(page);
	});
	
	$(document).on('click', '.ms-more-details, .sidebar-product-details', function(){
		ministore_getProductDetails($(this));
	});
	
	$(document).on('click', '.table .td .details .pname, .table .td .thumb', function(){
		ministore_getProductDetails($(this));
	});
	
	$(document).on('click', '.ms-back-to-list, .continueshopping', function(e){
		e.stopImmediatePropagation();
		preLoader($('#fbmain-wrapper'));
		
		if ($('#searchtext').val() != "") {
			var error_status = 0;
			var form_data = {};
				form_data.catname = $('#catname').val();
				form_data.total_products = $('#total_products').val();
				form_data.store_category_id = $('#ms_cid').val();
				form_data.catpage = $('#catpage').val();
				form_data.ms_search_text = $('#searchtext').val();
				form_data.ms_search_products_option = $('#products_option').val();
				
			$.extend(form_data,ms_getQueryParams());
				
			var searchpage = 0;
			if($('#searchpage').val() != ""){
				searchpage = $('#searchpage').val();
			}
			$('#ms-wrapper').data('data',form_data);
			$.post(root + 'fb-store/search.html?page=' + searchpage, form_data,function(data){ 
				$('#ms-wrapper').html(data);
				$('#preloader-wrapper').hide();
			});
			
			return false;
		}else if($('#catname').val() == '') {
			var page = $('#catpage').val();
			ministore_getMainPage(page);
		}else{
			var page = $('#prodpage').val();
			var data = {};
				data.catname = $('#catname').val();
				data.total_products = $('#total_products').val();
				data.store_category_id = $('#ms_cid').val();
			ministore_gotoProductLists(data, page);
		}
	});
	
	$(document).on('click', '.ms-cancel-link', function(){
		$('#ms-search-container').remove();
	});
	$(document).on('click', '.c-thumb-image', function(){
		$('#ms-search-container').remove();
	});
	
	$(document.body).on('click', '.photo-btn', function(){
		$('#video-photos-btn-wrapper span').removeClass('selected');
		$(this).addClass('selected');
		$('#video-wrapper').addClass('hide');
		$('#details-slideshow-wrapper').removeClass('hide');
	});
	
	$(document.body).on('click', '.video-btn', function(){
		$('#video-photos-btn-wrapper span').removeClass('selected');
		$(this).addClass('selected');
		$('#details-slideshow-wrapper').addClass('hide');
		$('#video-wrapper').removeClass('hide');
	});
	
	$(document).on('click', '#wdgt-wishlistbox ul li', function(){
		if(typeof store.get('user_id') === 'undefined' || store.get('user_id') <= 0){
			$('#ms-header-login').trigger('click');
			return false;
		}
		url = root + 'add-to-wishlist.html';
		$this = $(this);
		var form_data = {};
		form_data.wishlist_val = $(this).find('#wishtype').val();
		form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
		form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
		$this.parents('#wdgt-wishlistbox').find('#preloader-wrapper').show();
		$.ajax({
			type: "POST",
			url: url,
			data: form_data,
			cache: false,
			success: function(data){
				if(!data){ $this.parents('#wdgt-wishlistbox').find('#preloader-wrapper').hide(); return false; }
				var _data = $.parseJSON(data);
				$.each(_data, function(i, item) {
					$('.wishlist_'+_data[i].product_id+'_'+_data[i].store_id+'_1').removeClass('active');
					$('.wishlist_'+_data[i].product_id+'_'+_data[i].store_id+'_2').removeClass('active');
					$('.wishlist_'+_data[i].product_id+'_'+_data[i].store_id+'_3').removeClass('active');
					$('.wishlist_'+_data[i].product_id+'_'+_data[i].store_id+'_'+_data[i].status).addClass('active');
					$this.parents('#wdgt-wishlistbox').find('#preloader-wrapper').hide();
				});
			}
		});
		return false;
	});
	
	$(document).on('click', '.ms_cat_page', function(){
		var form_data = {};
			form_data.key = $('#key').val();
			form_data.catname = $(this).find('.ms-cname').html();
			form_data.total_products = $(this).find('.ms-cat-quantity').html() == null ? $(this).parents('.ms-category').find('.ms-cat-quantity').html() : $(this).find('.ms-cat-quantity').html();
			form_data.store_category_id = $(this).attr('cid');
			form_data.store_name = $('#ms_storename').val();
			form_data.store_tag = $('#ms_storetag').val();
			form_data.user_id = $('#ms_uid').val();
			form_data.store_id = $('#ms_sid').val();
			
		$.extend(form_data,ms_getQueryParams());
		preLoader($('#fbmain-wrapper'));	
		$('#ms-wrapper').data('data',form_data);
		var page;
		if($(this).attr('id') == "page_link"){
			page = $(this).attr('href');
		}else{
			page = root + "fb-store/main.html";
		}
		
		$.post( page, form_data,function(data){
			$('#ms-wrapper').html(data);
			$('#preloader-wrapper').hide();
		});
		
		return false;
	});
	
	$(document).on('click', '.ms_prod_page', function(event){
		event.stopImmediatePropagation();
		var form_data = {};
			form_data.key = $('#key').val();
			form_data.catname = $('#catname').val();
			form_data.total_products = $('#total_products').val();
			form_data.store_category_id = $('#ms_cid').val();
			form_data.store_name = $('#ms_storename').val();
			form_data.store_tag = $('#ms_storetag').val();
			form_data.user_id = $('#ms_uid').val();
			form_data.store_id = $('#ms_sid').val();
			form_data.catpage = $('#catpage').val();
			
		$.extend(form_data,ms_getQueryParams());
		preLoader($('#fbmain-wrapper'));
		$('#ms-wrapper').data('data',form_data);
			
		var page;
		if($(this).attr('id') == "page_link"){
			page = $(this).attr('href');
		}else{
			page = root + "fb-store/product-listing.html";
		}
		$.post( page, form_data,function(data){
			$('#ms-wrapper').html(data);
			$('#preloader-wrapper').hide();
		});
		return false;
	});
	
	$(document).on('keypress', '.ms-search', function(e){
		if (e.which == 13) {
			$(this).removeClass('input_error');
			var error_status = 0;
			var form_data = {};
				form_data.catname = $('#catname').val();
				form_data.total_products = $('#total_products').val();
				form_data.store_category_id = $('#ms_cid').val();
				form_data.catpage = $('#catpage').val();
				form_data.ms_search_text = $.trim($(this).val());
				form_data.ms_search_products_option = $('.search_products_option').val();
			
			$.extend(form_data,ms_getQueryParams());
			preLoader($('#fbmain-wrapper'));
			$('#ms-wrapper').data('data',form_data);
				
			if(form_data.ms_search_text == ""){
				$(this).addClass('input_error');
				error_status++;
				return false;
			}
			
			store.set('ms_search_text', form_data.ms_search_text);
			store.set('ms_search_products_option', form_data.ms_search_products_option);
			
			$.post(root + 'fb-store/search.html', form_data,function(data){ 
				$('#ms-wrapper').html(data);
				$('#preloader-wrapper').hide();
			});
			
			return false;
		}
	});
	
	$(document).on('keypress', '.search-product', function(e){
		if (e.which == 13) {
			$(this).removeClass('input_error');
			var error_status = 0;
			var form_data = {};
				form_data.catname = $('#catname').val();
				form_data.total_products = $('#total_products').val();
				form_data.store_category_id = $('#ms_cid').val();
				form_data.catpage = $('#catpage').val();
				form_data.ms_search_text = $.trim($(this).val());
				form_data.ms_search_products_option = $('.search_products_option').val();
			
			$.extend(form_data,ms_getQueryParams());
			
			$('#ms-wrapper').data('data',form_data);
				
			if(form_data.ms_search_text == ""){
				$(this).addClass('input_error');
				error_status++;
				return false;
			}
			preLoader($('#fbmain-wrapper'));
			store.set('ms_search_text', form_data.ms_search_text);
			store.set('ms_search_products_option', form_data.ms_search_products_option);
			
			$.post(root + 'fb-store/search.html', form_data,function(data){ 
				$('#ms-wrapper').html(data);
				$('#preloader-wrapper').hide();
			});
			
			return false;
		}
	});
	
	$(document).on('click', '#search-icon', function(e){
		$('.search-product').removeClass('input_error');
		var error_status = 0;
		var form_data = {};
			form_data.catname = $('#catname').val();
			form_data.total_products = $('#total_products').val();
			form_data.store_category_id = $('#ms_cid').val();
			form_data.catpage = $('#catpage').val();
			form_data.ms_search_text = trimText($('.search-product').val());
			form_data.ms_search_products_option = $('.search_products_option').val();
		$.extend(form_data,ms_getQueryParams());
		
		$('#ms-wrapper').data('data',form_data);
			
		if(form_data.ms_search_text == ""){
			$('.search-product').addClass('input_error');
			error_status++;
			return false;
		}
		preLoader($('#fbmain-wrapper'));
		store.set('ms_search_text', form_data.ms_search_text);
		store.set('ms_search_products_option', form_data.ms_search_products_option);
		
		$.post(root + 'fb-store/search.html', form_data,function(data){ 
			$('#ms-wrapper').html(data);
			$('#preloader-wrapper').hide();
		});
		
		return false;
	});
	
	$(document).on('click', '.ms-search-btn', function(e){
		$('.ms-search').removeClass('input_error');
		var error_status = 0;
		var form_data = {};
			form_data.catname = $('#catname').val();
			form_data.total_products = $('#total_products').val();
			form_data.store_category_id = $('#ms_cid').val();
			form_data.catpage = $('#catpage').val();
			form_data.ms_search_text = trimText($('.ms-search').val());
			form_data.ms_search_products_option = $('.search_products_option').val();
		$.extend(form_data,ms_getQueryParams());
		preLoader($('#fbmain-wrapper'));
		$('#ms-wrapper').data('data',form_data);
			
		if(form_data.ms_search_text == ""){
			$('.ms-search').addClass('input_error');
			error_status++;
			return false;
		}
		
		store.set('ms_search_text', form_data.ms_search_text);
		store.set('ms_search_products_option', form_data.ms_search_products_option);
		
		$.post(root + 'fb-store/search.html', form_data,function(data){ 
			$('#ms-wrapper').html(data);
			$('#preloader-wrapper').hide();
		});
		
		return false;
	});
	
	$(document).on('click', '.ms_search_page', function(e){
		
		$(this).removeClass('input_error');
		var error_status = 0;
		var form_data = {};
			form_data.key = $('#key').val();
			form_data.catname = $('#catname').val();
			form_data.total_products = $('#total_products').val();
			form_data.store_category_id = $('#ms_cid').val();
			form_data.store_name = $('#ms_storename').val();
			form_data.store_tag = $('#ms_storetag').val();
			form_data.user_id = $('#ms_uid').val();
			form_data.store_id = $('#ms_sid').val();
			form_data.catpage = $('#catpage').val();
			form_data.ms_search_text = $('#searchtext').val();
			form_data.ms_search_products_option = $('#products_option').val();
		
		$.extend(form_data,ms_getQueryParams());
		preLoader($('#fbmain-wrapper'));
		$('#ms-wrapper').data('data',form_data);
		
		var page;
		if($(this).attr('id') == "page_link"){
			page = $(this).attr('href');
		}else{
			page = root + "fb-store/search.html";
		}

		$.post( page, form_data,function(data){
			$('#ms-wrapper').html(data);
			$('#preloader-wrapper').hide();
		});
		return false;
		
	});
	
	// add to cart
	$(document).on('click','.addtocart-btn', function(e) {
		e.stopImmediatePropagation();	
		var el = $(this);
		var qty = parseInt(el.parent().find('input[name="qty"]').val());
		var data = $.parseJSON(el.attr('data'));
		var data = {product_user_id:data.p_uid,
					product_id:data.p_id, 
					product_store_id: data.store_id,
					product_store_url: CS.parent_embed_url,
					store_id:data.store_id,
					store_user_id:data.store_uid ,
					quantity: parseInt(qty),
					has_options: data.has_options };
					
		if(parseInt(data.has_options) > 0) {
			$.extend(data,ms_getQueryParams());
			show_modal({ overlay: true,
						 overlay_class: '',
						 modal: '#ms-modal-window',
						 modal_class: 'option',
						 ajaxURL: true,
						 data: data,
						 onBeforeLoad: function() {
							preLoader($('#fbmain-wrapper'));
						 },
						 onAfterLoad: function() {
							var jSpane_modal_window = $('#ms-modal-window .scroller');
							jSpane_modal_window.jScrollPane();
							jSpane_api_modal_window = jSpane_modal_window.data('jsp');
							$('#preloader-wrapper').hide();
						 }
						});
		}
		else {
			addtocart(data);	
		}
		
	});
	
	// product options (pop-up)
	$(document).on('click', '#ms-modal-window .btn-close', hide_modal);
	$(document).on('click','#ms-overlay', hide_modal);
	$(document).on('change','.ms-mainoption-value-wrapper .customized_select select, .details-option-value-wrapper .customized_select select', function() {
		var parent_container = ($(this).parent().parent().hasClass('details-option-value-wrapper')) ? '.details-option-value-wrapper': '.ms-mainoption-value-wrapper';
		var variants_values = $.parseJSON($('#unique_variants').attr('variants_values'));
		var unique_variants = $.parseJSON($('#unique_variants').val());
		var index = parseInt($(parent_container + ' .customized_select select').index(this));
		var value = $(this).val();
		var next_option = (index + 1);

		if(index == 0) {
			$(parent_container + ' .customized_select').eq(1).addClass('disabled');	
			$(parent_container + ' .customized_select').eq(2).addClass('disabled');	
			$(parent_container + ' .customized_select select').eq(1).val('');
			$(parent_container + ' .customized_select select').eq(2).val('');
			$(parent_container + ' .customized_select select').eq(1).attr('disabled','disabled');	
			$(parent_container + ' .customized_select select').eq(2).attr('disabled','disabled');	
			$(parent_container + ' .customized_select div').eq(1).html('Select Item');
			$(parent_container + ' .customized_select div').eq(2).html('Select Item');
			
			$(this).parent().removeClass('disabled');
			$(this).removeAttr('disabled');
		}

		if(typeof $(parent_container + ' .customized_select select').eq(next_option) != "undefined" && value != "") {
			$(parent_container + ' .customized_select select').eq(next_option).find('option').hide();
			$(parent_container + ' .customized_select select').eq(next_option).find('option').eq(0).show();
			$(parent_container + ' .customized_select select').eq(next_option).removeAttr('disabled');
			$(parent_container + ' .customized_select select').eq(next_option).parent().removeClass('disabled');
			$(parent_container + ' .customized_select select').eq(next_option).val('');
			$(parent_container + ' .customized_select div').eq(next_option).html('Select Item');
			//console.log(variants_values);
			for(i = 0; i < variants_values.length; i++) {
				if(variants_values[i][index] == value) {
					$(parent_container + ' .customized_select select').eq(next_option).find('option[value='+variants_values[i][next_option]+']').show();	
				}
			}
			
		}
		else {
			if(index > 0 && value == '') {
				$(parent_container + ' .customized_select select').eq(next_option).parent().addClass('disabled');
				$(parent_container + ' .customized_select select').eq(next_option).attr('disabled','disabled');
				$(parent_container + ' .customized_select select').eq(next_option).find('option').hide();
				$(parent_container + ' .customized_select select').eq(next_option).val('');
				$(parent_container + ' .customized_select select').eq(next_option).find('option').eq(0).show();
				$(parent_container + ' .customized_select div').eq(next_option).html('Select Item');
			}
		}
	});

	// end product options (pop-up)
	
	// Cart
	$(document).on('click', '.table .td .qty a', function() {
		var quantity = $(this).prev().val();
		var m = $(this).prev().attr('m');
		var p = $(this).prev().attr('p');
		var variant_id = $(this).parent().find('input').attr('variant_id');
		var su = $(this).prev().attr('su');
		var s = $(this).prev().attr('s');
		var has_options = 0;
		var product_options = $(this).parent().find('input').attr('options');
		
		if(typeof(variant_id) != 'undefined' && variant_id > 0) {
			has_options = 1;
		}
		
		var data = {product_user_id:m,
					product_id:p, 
					store_id: s,
					store_user_id: su,
					quantity: quantity,
					has_options: has_options,
					variant_id: variant_id,
					product_options:product_options };
		
		update_cart_quantity(data);
	});
	$(document).on('click', '.table .td .qty span', function() {
		var m = $(this).attr('m');
		var p = $(this).attr('p');
		var variant_id = $(this).parent().find('input').attr('variant_id');
		var su = $(this).attr('su');
		var s = $(this).attr('s');
		var has_options = 0;
		var product_options = $(this).parent().find('input').attr('options');
		
		if(typeof(variant_id) != 'undefined' && variant_id > 0) {
			has_options = 1;
		}

		var data = {product_user_id:m,
					product_id:p, 
					store_id: s,
					store_user_id: su,
					has_options: has_options,
					variant_id: variant_id,
					product_options:product_options };
		
		delete_cart_product(data);
	});
	$(document).on('mouseup', "textarea", function() {
		jSpane_api_cartlisting.reinitialise();
	});
	$(document).on('keyup', "textarea", function() {
		jSpane_api_cartlisting.reinitialise();
	});
	$(document).on('change', '#shipto_country', function(e){
		if($(this).val() != '') {
			
			var merchant_note = {};
			$('.ms-merchant-wrapper textarea').each(function() {
				var val = $(this).val();
				var id = $(this).attr('name').replace(/merchant_note_/,'');
				merchant_note[id] = val;
				//merchant_note.push($(this).val());	
			});
			$(document).data('co_note',merchant_note);
			
			load_cart({shipto_country:$(this).val()});
		}
	});
	
	// Checkout button 
	$(document).on('click', '#checkout-process', function(){
		
		/*
		var merchant_note = {};
		$('.ms-merchant-wrapper textarea').each(function() {
			var val = $(this).val();
			var id = $(this).attr('name').replace(/merchant_note_/,'');
			merchant_note[id] = val;
		});
		$(document).data('co_note',merchant_note);
		
		ministore_checkout('shipping');
		*/
		
		var newtab = window.open();
		store.set('shipto_country',$('#shipto_country').val());
		newtab.location = $(this).attr('url') + '#&shipto_country='+$('#shipto_country').val();
	});
	$(document).on('click', '.ms-checkout-header #close', function() {
		$('#ms-checkout-inner-wrapper').html('');
		$('#ms-checkout-wrapper').hide();
		preLoader($('#fbmain-wrapper'));
		$('#preloader-wrapper').hide();
	});
	
	// Main header
	$(document).on('click', '#ms-header-cart', load_cart);
	$(document).on('click', '#ms-header-login', ministore_csLogin);
	$(document).on('click', '#ms-header-logout', ministore_csLogout);
	$(document).on('click', '#ms-header-account .submenu .socials.fb', ministore_fbLogin);
	$(document).on('click', '#ms-header-account .submenu .socials.yahoo', ministore_yahooLogin);
	$(document).on('click', '#ms-header-account .submenu .socials.gplus', ministore_gplusLogin);
	$(document).on("hover", "#ms-header-account", function(e) {
		if(e.type == 'mouseenter') { $(this).find('.submenu').show();}
		else { $(this).find('.submenu').hide(); }
	});
});


function loadWishlist(){
	if(typeof store.get('user_id') === 'undefined'){
		return false;
	}
	url = root + 'load-wishlist.html';
	var form_data = {};
	form_data.wishlist_val = $(this).find('#wishtype').val();
	form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
	form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
	
	$.ajax({
		type: "POST",
		url: url,
		data: form_data,
		cache: false,
		success: function(data){
			var _data = $.parseJSON(data);
			$.each(_data, function(i, item) {
				$('.wishlist_'+_data[i].product_id+'_'+_data[i].store_id+'_'+_data[i].status).addClass('active');
			});
		}
	});
	return false;
}
	
	
// Textarea auto expand height
(function($){$.fn.TextAreaExpander=function(minHeight,maxHeight){var hCheck=!($.browser.msie||$.browser.opera);function ResizeTextarea(e){e=e.target||e;var vlen=e.value.length,ewidth=e.offsetWidth;if(vlen!=e.valLength||ewidth!=e.boxWidth){if(hCheck&&(vlen<e.valLength||ewidth!=e.boxWidth))e.style.height="0px";var h=Math.max(e.expandMin,Math.min(e.scrollHeight,e.expandMax));e.style.overflow=(e.scrollHeight>h?"auto":"hidden");e.style.height=h+"px";e.valLength=vlen;e.boxWidth=ewidth}return true};this.each(function(){if(this.nodeName.toLowerCase()!="textarea")return;var p=this.className.match(/expand(\d+)\-*(\d+)*/i);this.expandMin=minHeight||(p?parseInt('0'+p[1],10):0);this.expandMax=maxHeight||(p?parseInt('0'+p[2],10):99999);ResizeTextarea(this);if(!this.Initialized){this.Initialized=true;$(this).css("padding-top",0).css("padding-bottom",0);$(this).bind("keyup",ResizeTextarea).bind("focus",ResizeTextarea)}});return this}})(jQuery);
function trimText (str) {str = str.replace(/^\s+/, '');for (var i = str.length - 1; i >= 0; i--){if (/\S/.test(str.charAt(i))){str = str.substring(0, i + 1);break;}}return str;}

function customized_select() {
	$('.customized_select select').each(function(){
		if($(this).parent().find('div').length == 0) {
			title = $('option:selected',this).text();
			$(this)
				.css({'z-index':10,'opacity':0,'-khtml-appearance':'none'})
				.after('<div>' + title + '</div>')
				.change(function(){
					val = $('option:selected',this).text();
					$(this).next().text(val);
				})
		}
	});
}

function ministore_csConfirm(msg) {
	doNotLoad == true;
	$("#ms-modal-message-bar").html(msg);
	$("#ms-overlay").show();
	$("#ms-modal-notify").show();
}
// Login Window
function ministore_csLogin() {
	ministore_popUp('login.html?' + 'returnUrl=' + encodeURIComponent(location.href) + '&type=ministore&id=1234567890');
	//newwindow = window.open(url,'CSLoginPage',features);
}
function ministore_fbLogin() { ministore_popUp('ms_fb_login.html'); }
function ministore_yahooLogin() { ministore_popUp('user/openid_login/yahoo?login&type=ministore'); }
function ministore_gplusLogin() { ministore_popUp('user/openid_login/google?login&type=ministore'); }
function ministore_popUp(url) {
	var  screenX    = typeof window.screenX != 'undefined' ? window.screenX : window.screenLeft,
	screenY    = typeof window.screenY != 'undefined' ? window.screenY : window.screenTop,
	outerWidth = typeof window.outerWidth != 'undefined' ? window.outerWidth : document.body.clientWidth,
	outerHeight = typeof window.outerHeight != 'undefined' ? window.outerHeight : (document.body.clientHeight - 22),
	width    = 500,
	height   = 270,
	left     = parseInt(screenX + ((outerWidth - width) / 2), 10),
	top      = parseInt(screenY + ((outerHeight - height) / 2.5), 10),
	features = (
			'width=' + width +
			',height=' + height +
			',left=' + left +
			',top=' + top
	);
	var url = root + url;
	newwindow = window.open(url,'CSLoginPage',features);
	
	if (window.focus) {newwindow.focus()}
	return false;
}

// logout		
function ministore_csLogout(){
	preLoader($('#fbmain-wrapper'));
	var url = root + 'signout.html';
	var form_data = {};
		form_data.post_dta = 1;
		form_data.temp_sessionid = store.get('temp_sessionid');
		form_data.token = store.get('token');
		
	$.ajax({
		type: "POST",
		url: url,
		data: form_data,
		cache: false,
		success: function(data){
			if(data == 'success'){
				store.clear();
				//$(document).data('shipping_address_id',0);
				//$(document).data('billing_address_id',0);
				//$(document).data('co_selected_SABAPD',{});
				//$(document).data('co_note',{});				
				ministore_getMainPage(1);
				//location.href = location.href;
			}
		}
	});
	return false;
}
/* Returns values (in json or url querystring format):
 * 		Logged user: user_id and token
 *		Store info: store_key (which identifies store owner and store_id
 */
function ms_getQueryParams(type) {
	type = typeof(type) == 'undefined' ? 0 : 1;
	var params = eval( {login_user_id:store.get('user_id'),token:store.get('token'),store_key:ms_store_key,cart_sess:store.get('cart_sess'),cart:store.get('cart'),m_note:$(document).data('co_note')} );
	var data = '';
	var value;
	
	// Json format
	if(!type) {
		data = params;		
	}
	// Serialize format
	else {
		for(var d in params) {
			if(params.hasOwnProperty(d)) {
				value = typeof(params[d]) == 'undefined' ? '' : params[d];
				data += '&' + d + '=' + value;
			}
		}
	}
	
	return data;
}
	
// reload certain ajax content
function ministore_csReload(pageToReload) {
	preLoader($('#fbmain-wrapper'));	
	//ministore_getMainPage(1);
	$.ajax({
		type: "POST",
		url: $('#ms-wrapper').data('current_url'),
		data: $.extend($('#ms-wrapper').data('data'),ms_getQueryParams()),
		cache: false,
		success: function(res){
			$('#ms-wrapper').html(res);
			
			if(typeof jSpane_api_cartlisting != 'undefined') {
				jSpane_api_cartlisting.reinitialise();	
			}
			
			$('#preloader-wrapper').hide();
		}
	});
	
	//location.href = location.href;
}

function preLoader(mcontainer) {
	var w = parseInt(mcontainer.width());
	var h = parseInt(mcontainer.height());
	
	$('#preloader-wrapper').css({width:w,height:h});
	$('#preloader-wrapper').show();
	var t = (h-parseInt($('#preloader-center').height())) / 2;
	$('#preloader-center').css({'top':t});
}

// Page Contents Here
// ================================
// Main Page (Home)
function ministore_getMainPage(page){
	var isOK = checkBrowser('ministore');
	if(!isOK) return false;
	
	form_data = ms_getQueryParams();
	preLoader($('#fbmain-wrapper'));
	$('#ms-wrapper').data('data',form_data);

	var queryparam = window.location.search.substring(1);

	$.post(root + 'fb-store/main.html?page=' + page + '&' + queryparam, form_data,function(data){ $('#ms-wrapper').html(data); $('#preloader-wrapper').hide(); });
}
// Product Listing
function ministore_gotoProductLists(data, page){
	var isOK = checkBrowser('ministore');
	if(!isOK) return false;
	
	var form_data = {};
		form_data.catname = data.catname;
		form_data.total_products = data.total_products;
		form_data.store_category_id = data.store_category_id;
		form_data.catpage = $('#catpage').val();

	$.extend(form_data,ms_getQueryParams());
	preLoader($('#fbmain-wrapper'));
	$('#ms-wrapper').data('data',form_data);	

	$.post(root + 'fb-store/product-listing.html?page=' + page, form_data,function(data){ 
		$('#ms-wrapper').html(data);
		$('#preloader-wrapper').hide();
	});
}

function ministore_getProductDetails(el) {
	var isOK = checkBrowser('ministore');
	if(!isOK) return false;
	
		var form_data = {};
		
		form_data.catname = $('#catname').val();
		form_data.total_products = $('#total_products').val();
		form_data.store_category_id = $('#ms_cid').val();
		form_data.product_id = el.attr('pid');
		form_data.product_user_id = $(el).attr('uid');
		form_data.catpage = $('#catpage').val();
		form_data.prodpage = $('#prodpage').val();
		form_data.searchtext = $('#searchtext').val();
		form_data.searchpage = $('#searchpage').val();
		form_data.products_option = $('#products_option').val();
		
		$.extend(form_data,ms_getQueryParams());
		preLoader($('#fbmain-wrapper'));
		$('#ms-wrapper').data('data',form_data);
		
		$.post(root + 'fb-store/details.html', form_data,function(data){ 
			$('#ms-wrapper').html(data);
			$('#preloader-wrapper').hide();
		});
}
// Product option content (pop-up modal)
function show_modal(options) { 
	var opts = $.extend({overlay: true,
						 overlay_class: '',
						 modal: '#ms-modal-window',
						 modal_class: 'option',
						 ajaxURL: false,
						 data: {},
						 content: '',
						 onBeforeLoad: false,
						 onAfterLoad: false
						}, options);
	
	var top, left, modal = $(opts.modal);					
	var cb_before, cb_after;					

	if(opts.overlay != false) { $('#ms-overlay').show(); }
	if(opts.overlay_class != '') { $('.ms-overlay').addClass('overlay_class'); }
	if($(opts.modal).length == 0) { modal = $('#ms-modal-window'); }
	if(opts.modal_class != '') { modal.addClass(opts.modal_class); }
	
	modal.css({height:modal.height(), width:modal.width()} );
	
	if(opts.onBeforeLoad != false) {
		cb_before = $.Callbacks();
		cb_before.add( opts.onBeforeLoad );
		cb_before.fire();	
	}
	
	//opts.modal.addClass('option');
	top = (560 - modal.height()) / 2;
	left = (810 - modal.width()) / 2;
	modal.css({top:top, left:left});
	
	if(opts.ajaxURL) {
		$.post( '/fb-store/options.html?type=listing', opts.data, function(data){
			$('#ms-modal-window').html(data);
			$('#ms-modal-window').show();
			if(opts.onAfterLoad != false) {
				cb_after = $.Callbacks();
				cb_after.add( opts.onAfterLoad );
				cb_after.fire();	
			}
		});
	}
	else {
		$('#ms-modal-window').show();
		if(opts.onAfterLoad != false) {
			cb_after = $.Callbacks();
			cb_after.add( opts.onAfterLoad );
			cb_after.fire();	
		}
	}
}

function hide_modal() {
	$('#ms-modal-window').hide();
	$('#ms-modal-window').html('');
	$('#ms-overlay').hide();
}

function get_cart_data() {
	var cart_data = JSON.stringify(store.get('cart'));
	return cart_data = eval( '(' + cart_data + ')' );
}

function load_cart(data) {
	data = typeof(data) == 'undefined' ? {} : data;
	var form_data = {};
		form_data.catname = $('#catname').val();
		form_data.total_products = $('#total_products').val();
		form_data.store_category_id = $('#ms_cid').val();
		form_data.catpage = $('#catpage').val();
		form_data.ms_search_text = $('#searchtext').val();
		form_data.ms_search_products_option = $('#products_option').val();

	if(data.hasOwnProperty('shipto_country')) {
		form_data.shipto_country = data.shipto_country;
	}
	else {
		form_data.shipto_country = store.get('shipto_country');
	}
	
	form_data.cart = store.get('cart');
	preLoader($('#fbmain-wrapper'));
	form_data = $.extend(form_data,ms_getQueryParams());
	$('#ms-wrapper').data('data',form_data);
	$.post( '/fb-store/cart.html?type=listing', form_data,function(data){
		$('#ms-wrapper').html(data);
		jSpane_api_cartlisting.reinitialise();
		$('.fb-shoppingcart-inside-wrapper').html($('#ms-header-cartitems').html());
		$('#preloader-wrapper').hide();
	});
}

function addtocart(data) {
	if(typeof(data) != 'object') return;
	
	var product_user_id = data.product_user_id; // product owner or merchant
	var product_id = data.product_id;
	var store_id = data.store_id;
	var store_user_id = data.store_user_id; // endorser or store owner
	var quantity = data.quantity;
	var has_options = data.has_options;
	data.product_options = {};
	
	// get existing cart values
	var cart = store.get('cart');
	cart = (typeof(cart) != 'object' || cart == null) ? {} : cart;

	//console.log(cart.merchants[user_id].products.hasOwnProperty(1));
	//console.log(cart);
	// retreive existing 
	var item, option = '';
	var error_option = false;
	var variants_values = new Array();
	
	// Option Validations
	$('.ms-mainoption-value-wrapper').each(function() {
		if($(this).find('select').val() == '') {
			error_option = true;	
		}
		
		data.product_options[$(this).attr('option_id')] = $(this).find('select').val();
		variants_values.push($(this).find('select').val());
	});
	
	var options_string = variants_values.join(',');
	var variants = $.parseJSON($('#unique_variants').attr('variants'));
	var variant_id = 0;
	for (var key in variants) {
		if (variants.hasOwnProperty(key)) {
			if(variants[key] == options_string) {
				variant_id = key;	
			}
		}
	}
	
	if(error_option || (has_options > 0 && variant_id == 0)) {
		alert('Please select an option.');
		return;	
	}
	data.variant_id = variant_id;
	data.cart = cart;
	
	hide_modal();
	
	var query_store = ms_getQueryParams();
	delete query_store['cart'];
	$.extend(data,query_store);
	
	//console.log(data); return;
	//preLoader($('#fbmain-wrapper'));
	$.ajax({
		type: "POST",
		url: '/fb-store/update-cart-data.html',
		data: data,
		cache: false,
		success: function(res){
			data = $.parseJSON(res);
			store.set('cart',data.cart);
			store.set('cart_sess',data.cart_sess);
			load_cart();
		}
	});
}

function update_cart_quantity(data) {
	if(typeof(data) != 'object') return;
	
	// get existing cart values
	var cart = store.get('cart');
	cart = (typeof(cart) != 'object' || cart == null) ? {} : cart;
	
	data.cart = cart;
	$.extend(data,ms_getQueryParams());
	preLoader($('#fbmain-wrapper'));
	$.ajax({
		type: "POST",
		url: '/fb-store/update-cart-data.html?update=new_quantity',
		data: data,
		cache: false,
		success: function(res){
			data = $.parseJSON(res);
			store.set('cart',data.cart);
			store.set('cart_sess',data.cart_sess);
			load_cart();
		}
	});
}

function delete_cart_product(data) {
	if(typeof(data) != 'object') return;
	
	// get existing cart values
	var cart = store.get('cart');
	cart = (typeof(cart) != 'object' || cart == null) ? {} : cart;
	
	data.cart = cart;
	$.extend(data,ms_getQueryParams());
	preLoader($('#fbmain-wrapper'));
	$.ajax({
		type: "POST",
		url: '/fb-store/update-cart-data.html?update=delete_item',
		data: data,
		cache: false,
		success: function(res){
			data = $.parseJSON(res);
			store.set('cart',data.cart);
			store.set('cart_sess',data.cart_sess);
			load_cart();
		}
	});
}

function destroySlimscroll(objectId) { $("#"+objectId).parent().replaceWith($("#"+objectId)); }