var	cloud_url = CS.cloudURL || location.protocol + '//' + location.host + '/';
var	root = CS.siteURL || location.protocol + '//' + location.host + '/';

$(document).ready(function(){
	$('.wdgt-logo').textfill({innerTag:'label'});
	
	$(document).on('click', '.wdgt-shop-now-btn', function(){
		$('#wdgt-splash-wrapper').addClass('hide');
		$('#wdgt-home').removeClass('hide');
	});
	
	$(document).on('click', '.show_subcat', function(){
		$('.wdgt-main-wrapper').addClass('hide');
		$('.wdgt-show-product').addClass('hide');
		$('.' + $(this).attr('maincat')).removeClass('hide');
		$('.wdgt-show-product').css({'background-color':'#FFFFFF'});
		$('.wdgt-show-product').html('');
	});
	
	$(document).on('click', '.show_product, .wdgt_prod_page', function(){
		$('#dialog-box').remove();
		$('.wdgt-main-wrapper').addClass('hide');
		$('.wdgt-show-product').removeClass('hide');
		var page = $(this).attr('page');
		var form_data = {};
		form_data.key = $('#key').val();
		form_data.user_id = $('#ms_uid').val();
		form_data.store_id = $('#ms_sid').val();
		form_data.store_category_id = $(this).attr('cid');
		var category_name = $(this).attr('catname')
		if(typeof category_name !== "undefined"){
			form_data.category_name = $(this).html();
		}else{
			form_data.category_name = $('#category_name').val();
		}
		
		var page;
		if($(this).attr('id') == "page_link"){
			page = $(this).attr('href');
			form_data.store_category_id = $('#store_category_id').val();
		}else{
			page = root + "widget-store-product-listing.html";
		}
		$('#wdgt-wrapper').prepend('<div id="dialog-box"><div class="preloader_wrapper" ><img class="img-loader" src="' + cloud_url + 'images/ministore_images/ministore-loader.gif" /></div></div>');
		$.post(page, form_data,function(data){ 
			$('.wdgt-show-product').html(data);
			$('#dialog-box').remove();
		});
		return false;
	});
	
	
	$(document).on('click', '.main_cat_menu', function(){
		$('.main_cat_menu').removeClass('active');
		$(this).addClass('active');
	});
	
	$(document).on('click', '.wdgt-search-btn', function(){
		$('.wdgt-cart-wrapper').after('<div class="wdgt-search-wrapper bg_color2 body_color">'
									+	'<span class="wdgt-search-lbl body_color">Search</span>'
									+	'<input type="text" class="wdgt-search body_background color2" name="search" value="" />'
									+	'<button class="wdt-search-button bg_color1"></button>'
									+	'<a href="javascript:;" class="wdgt-search-cancel-link color1">Cancel</a>'
									+ '</div>');
	});
	$(document).on('click', '.wdgt-search-cancel-link', function(){
		$('.wdgt-search-wrapper').remove();
	});
	
	$(document).on('click', '.wdgt-more-info-btn, .wdgt-related-btn, .wdgt-prev-next', function(){
		$('#wdgt-popup').css({'z-index': 20});
		var form_data = {};
		form_data.key = $('#key').val();
		form_data.cat_user_id = $('#ms_uid').val();
		form_data.store_id = $('#ms_sid').val();
		form_data.store_category_id = $('#store_category_id').val();
		form_data.product_id = $(this).attr('pid');
		form_data.user_id = $(this).attr('puid');
		$('#wdgt-wrapper').prepend('<div id="dialog-box"><div class="preloader_wrapper" ><img class="img-loader" src="' + cloud_url + 'images/ministore_images/ministore-loader.gif" /></div></div>');
		$.post(root + "widget-store/product-details.html", form_data,function(data){ 
			$('#wdgt-popup').html(data);
			$('#dialog-box').remove();
		});
	});
	
	$(document).on('click', '.wdgt-entire-popup', function(){
		$('#wdgt-popup').html('');
		$('#wdgt-popup').css({'z-index': 0});
	});
	
	$(document).on('click', '.wdgt-pmenu', function(){
		$('#wdgt-popup').removeClass('wdgt-video-wrapper');
		$('.show-details').removeClass('hide');
		$('.wdgt-popup-right-content').addClass('hide');
		$('.wdgt-buynow-wrapper').removeClass('hide');
		if($(this).attr('id') == 'show-related'){
			$('.wdgt-buynow-wrapper').addClass('hide');
		}
		if($(this).attr('id') == 'show-video'){
			$('.show-details').addClass('hide');
			$('#wdgt-popup').addClass('wdgt-level2-wrapper');
			$('.show-video').removeClass('hide');
			return false;
		}
		
		if($(this).attr('id') == 'show-images'){
			$('.show-details').addClass('hide');
			$('#wdgt-popup').addClass('wdgt-level2-wrapper');
			$('.show-images-zoom').removeClass('hide');
			$('#slider1').tinycarousel();
			return false;
		}
		$('.wdgt-show').removeClass('active');
		$(this).parent('li').addClass('active');
		$('.' + $(this).attr('id')).removeClass('hide');
		$('#re_open').val($(this).attr('id'));
	});

	$(document).on('click', '.wdgt-level2-popup-close', function(){
		$('.show-video').addClass('hide');
		$('.show-images-zoom').addClass('hide');
		$('.show-details').removeClass('hide');
		var re_open = $('#re_open').val();
		if(re_open != ""){
			$('.' + re_open).removeClass('hide');
			if(re_open == 'show-related'){
				$('.wdgt-buynow-wrapper').addClass('hide');
			}
		}
		$('#wdgt-popup').removeClass('wdgt-level2-wrapper');
	});
	
	$(document).on('keypress', '.wdgt-search', function(e){
		if (e.which == 13) {
			$('#dialog-box').remove();
			$('.wdgt-entire-popup').trigger('click');
			$('.wdgt-main-wrapper').addClass('hide');
			$('.wdgt-show-product').removeClass('hide');
			$('.main_cat_menu').removeClass('active');
			
			$(this).removeClass('input_error');
			var error_status = 0;
			var form_data = {};
				form_data.key = $('#key').val();
				form_data.user_id = $('#ms_uid').val();
				form_data.store_id = $('#ms_sid').val();
				form_data.wdgt_search_text = trimText($(this).val());
			if(form_data.wdgt_search_text == ""){
				$(this).addClass('input_error');
				error_status++;
				return false;
			}
			$('#wdgt-splash-wrapper').addClass('hide');
			$('#wdgt-home').removeClass('hide');
			store.set('wdgt_search_text', form_data.wdgt_search_text);
			
			$('#wdgt-wrapper').append('<div id="dialog-box"><div class="preloader_wrapper" ><img class="img-loader" src="' + cloud_url + 'images/ministore_images/ministore-loader.gif" /></div></div>');
			$.post(root + 'widget-store-search.html', form_data,function(data){ 
				$('.wdgt-show-product').html(data);
				$('#dialog-box').remove();
			});
			
			return false;
		}
	});
	
	$(document).on('click', '.wdt-search-button', function(){
			$('#dialog-box').remove();
			$('.wdgt-search').removeClass('input_error');
			var error_status = 0;
			var form_data = {};
				form_data.key = $('#key').val();
				form_data.user_id = $('#ms_uid').val();
				form_data.store_id = $('#ms_sid').val();
				form_data.wdgt_search_text = trimText($('.wdgt-search').val());
			if(form_data.wdgt_search_text == ""){
				$('.wdgt-search').addClass('input_error');
				error_status++;
				return false;
			}
			$('#wdgt-splash-wrapper').addClass('hide');
			$('#wdgt-home').removeClass('hide');
			$('.wdgt-entire-popup').trigger('click');
			$('.wdgt-main-wrapper').addClass('hide');
			$('.wdgt-show-product').removeClass('hide');
			$('.main_cat_menu').removeClass('active');
			
			store.set('wdgt_search_text', form_data.wdgt_search_text);
			
			$('#wdgt-wrapper').append('<div id="dialog-box"><div class="preloader_wrapper" ><img class="img-loader" src="' + cloud_url + 'images/ministore_images/ministore-loader.gif" /></div></div>');
			$.post(root + 'widget-store-search.html', form_data,function(data){ 
				$('.wdgt-show-product').html(data);
				$('#dialog-box').remove();
			});
			
			return false;
		
	});
	
	
	
	$(document).on('click', '.wdgt_search_page', function(){
			$('#dialog-box').remove();
			$(this).removeClass('input_error');
			var error_status = 0;
			var form_data = {};
				form_data.key = $('#key').val();
				form_data.user_id = $('#ms_uid').val();
				form_data.store_id = $('#ms_sid').val();
				form_data.wdgt_search_text = $('#search_text').val();
			
			var page;
			if($(this).attr('id') == "page_link"){
				page = $(this).attr('href');
			}else{
				page = root + "widget-store-search.html";
			}
			$('#wdgt-wrapper').prepend('<div id="dialog-box"><div class="preloader_wrapper" ><img class="img-loader" src="' + cloud_url + 'images/ministore_images/ministore-loader.gif" /></div></div>');
			$.post( page, form_data,function(data){
				$('.wdgt-show-product').html(data);
				$('#dialog-box').remove();
			});
			return false;
		
	});
	
	
	$(document).on('click', '.wdgt-cart', function(){
		$('#wdgt-cart-popup').show();
	});
	
	$(document).on('click', '.wdgt-cart-close', function(){
		$('#wdgt-cart-popup').hide();
	});
	
});


function trimText (str) {
	str = str.replace(/^\s+/, '');
	for (var i = str.length - 1; i >= 0; i--) {
		if (/\S/.test(str.charAt(i))) {
			str = str.substring(0, i + 1);
			break;
		}
	}
	return str;
}