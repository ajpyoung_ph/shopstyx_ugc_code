$(document).ready(function(){
	$("div[rel*=create_account_and_store]").CSModal({ top : 100, closeButton: ".modal_close" });
	
	$(document).on('click', '.create-new-store, .i-new-store', function(){
		$('.create_account_and_store_wrapper').trigger('click');
		var form_data = {};
		form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
		form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
		$('.create_account_and_store #popup-preloader-wrapper').show();
		$.post(root + "create-store-new-user-form.html", form_data,function(content){
			$('.create_account_and_store').hide();
			$('.create_account_and_store .box-content').html(content);
			$('.create_account_and_store').fadeIn(300);
			if(typeof $('#onpage_create').val() !== "undefined"){
				$('#i_store_name').val($('.h_store_name').val());
				$('#i_email').val($('.h_email').val());
				if($('.h_password').val() == 'Password'){
					$('.password_d').replaceWith('<input id="i_password" class="cas-input cas-password required password_c color-DEB32E border-radius2 border-color-DEB32E inner-glow-F2E1AC" type="password" name="password" value="Password" />');
					$('#pass_detect').val(0);
				}else{
					$('.password_c').replaceWith('<input id="i_password" class="cas-input cas-password required password_d color-DEB32E border-radius2 border-color-DEB32E inner-glow-F2E1AC" type="password" name="password" value="" />');
					$('#pass_detect').val(1);
					$('#i_password').val($('.h_password').val());
				}
			}
			$('.create_account_and_store #popup-preloader-wrapper').hide();
			$('.main-preloader').hide();
		});
	});
	
	$(document).on('click', '.cas-next-btn', function(){
		if($('.password_c').val() == 'Password'){
			$('.password_c').val('');
		}
		if($('#i_email').val() == 'Email'){
			$('#i_email').val('');
		}
		if($('#i_store_name').val() == 'Store Name'){
			$('#i_store_name').val('');
		}
		if($('#i_firstname').val() == 'First Name'){
			$('#i_firstname').val('');
		}
		if($('#i_lastname').val() == 'Last Name'){
			$('#i_lastname').val('');
		}
		if($('#i_baseurl').val() == 'Your Store URL'){
			$('#i_baseurl').val('');
		}
		$('#user_type').val($(this).attr('id'));
		$(this).parents("form").trigger("submit");
		return false;
	});
	
	$(document).on('click', '.cs-create-btn', function(){
		if($('#i_store_name').val() == 'Store Name'){
			$('#i_store_name').val('');
		}
		if($('#i_baseurl').val() == 'Your Store URL'){
			$('#i_baseurl').val('');
		}
		$('#user_type').val($(this).attr('id'));
		$(this).parents("form").trigger("submit");
		return false;
	});
	
	$(document.body).on('submit', '#create_store_and_account', function(event){ event.preventDefault(); createStoreAndAccount($(this));});
	$(document.body).on('submit', '#create_store_registered', function(event){ event.preventDefault(); createStoreRegistered($(this));});
	
	$(document.body).on('click', '.cas-ok-btn', function(){
		close_modal($('.create_account_and_store'));
	});
	
	$(document.body).on('click', '.cas-continue-btn', function(){
		close_modal($('.create_account_and_store'));
	});
	
});

function createStoreAndAccount($form){
	$('.create_account_and_store #popup-preloader-wrapper').show();
	url = $form.attr( 'action' );
	var form_data = {};

	form_data.store_name = $form.find( 'input[name="store_name"]' ).val();
	form_data.email = $form.find( 'input[name="email"]' ).val();
	form_data.password = $form.find( 'input[name="password"]' ).val();
	form_data.first_name = $form.find( 'input[name="first_name"]' ).val();
	form_data.last_name = $form.find( 'input[name="last_name"]' ).val();
	form_data.store_url = $form.find( 'input[name="store_url"]' ).val();
	form_data.updateinfo = $form.find( 'input[name="updateinfo"]' ).val();
	form_data.user_id = $form.find( 'input[name="user_id"]' ).val();
	form_data.user_type = $('#user_type').val();
	form_data.temp_sessionid = vid;
	form_data.token = vid;
	
	$.ajax({
		type: "POST",
		url: url,
		data: form_data,
		cache: false,
		success: function(data){
			var _data = $.parseJSON(data);
			
			if(_data.register_result == false){
				$('.error2').html('');
				if(_data.error_class == '.gen_error'){
					$(_data.error_class).html('<span style="min-width:346px; text-align: left;" class="fberrorbox">' + _data.error_msg + '</span>');
				}else{
					$(_data.error_class).html(_data.error_msg);
				}
				$('.create_account_and_store #popup-preloader-wrapper').hide();
			}else{
				if(_data.to_verify == 'yes'){
					$('.create_account_and_store #popup-preloader-wrapper').show();
					$.get(root + "create_store_step2.html", function(content){
						$('.create_account_and_store #popup-preloader-wrapper').hide();
						$('.create_account_and_store').hide();
						$('.create_account_and_store .box-content').html(content);
						$('.create_account_and_store').fadeIn(300);
					});
				}
				if(_data.to_verify == 'no'){
					if(_data.user_type == 'P'){
						$('.create_account_and_store #popup-preloader-wrapper').show();
						$.get(root + "store-created.html", function(content){
							$('.create_account_and_store #popup-preloader-wrapper').hide();
							$('.create_account_and_store').hide();
							$('.create_account_and_store .box-content').html(content);
							$('.cas-continue-btn').addClass('cas-ok-btn');
							$('.cas-ok-btn').removeClass('cas-continue-btn');
							$('.cas-ok-btn').html('OK');
							$('.create_account_and_store').fadeIn(300);
						});
					}
					if(_data.user_type == 'V'){
						$('.create_account_and_store #popup-preloader-wrapper').show();
						$.get(root + "store-created.html", function(content){
							$('.create_account_and_store #popup-preloader-wrapper').hide();
							$('.create_account_and_store').hide();
							$('.create_account_and_store .box-content').html(content);
							$('.create_account_and_store').fadeIn(300);
						});
					}
				}
			}
		}
	});
	return false;
}

function createStoreRegistered($form){
	$('.create_account_and_store #popup-preloader-wrapper').show();
	url = $form.attr( 'action' );
	var form_data = {};

	form_data.store_name = $form.find( 'input[name="store_name"]' ).val();
	form_data.store_url = $form.find( 'input[name="store_url"]' ).val();
	form_data.user_type = 'V';
	form_data.user_id = store.get('user_id');
	
	$.ajax({
		type: "POST",
		url: url,
		data: form_data,
		cache: false,
		success: function(data){
			var _data = $.parseJSON(data);
			
			if(_data.register_result == false){
				$('.error2').html('');
				if(_data.error_class == '.gen_error'){
					$(_data.error_class).html('<span style="min-width:346px; text-align: left;" class="fberrorbox">' + _data.error_msg + '</span>');
				}else{
					$(_data.error_class).html(_data.error_msg);
				}
				$('.create_account_and_store #popup-preloader-wrapper').hide();
			}else{
				$('.create_account_and_store #popup-preloader-wrapper').show();
				$.get(root + "store-created.html", function(content){
					$('.create_account_and_store #popup-preloader-wrapper').hide();
					$('.create_account_and_store').hide();
					$('.create_account_and_store .box-content').html(content);
					$('.create_account_and_store').fadeIn(300);
				});
			}
		}
	});
	return false;
}

function verifiedEmail(){
	if( typeof store.get('registered') !== 'undefined' && store.get('registered') == 1){
		$('.create_account_and_store_wrapper').trigger('click');
		$('.create_account_and_store #popup-preloader-wrapper').show();
		$.get(root + "verify-email.html", function(content){
			$('.create_account_and_store #popup-preloader-wrapper').hide();
			$('.create_account_and_store').hide();
			$('.create_account_and_store .box-content').html(content);
			$('.create_account_and_store').fadeIn(300);
			store.set('registered', '');
		});
	}
}

function popup_preLoader(mcontainer) {
	var w = parseInt(mcontainer.width());
	var h = parseInt(mcontainer.height());
	$('#popup-preloader-wrapper').css({width:w,height:h});
	$('#popup-preloader-wrapper').show();
	var t = (h-parseInt($('#preloader-center').height())) / 2;
	$('#popup-preloader-center').css({'top':t});
}