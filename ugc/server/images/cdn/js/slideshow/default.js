function getHeight(width) {
	var height = 360;
	if(width <= 224 ) { height = 360; }
	else if(width <= 249 ) { height = 370; }
	else if(width <= 274 ) { height = 380; }
	else if(width <= 299 ) { height = 390; }
	else if(width <= 324 ) { height = 400; }
	else if(width <= 349 ) { height = 410; }
	else if(width <= 374 ) { height = 420; }
	else if(width <= 399 ) { height = 430; }
	else if(width <= 424 ) { height = 440; }
	else if(width <= 449 ) { height = 450; }
	else if(width <= 474 ) { height = 460; }
	else if(width <= 499 ) { height = 470; }
	else if(width <= 524 ) { height = 480; }
	else if(width <= 549 ) { height = 490; }
	else if(width <= 574 ) { height = 500; }
	else if(width <= 599 ) { height = 510; }
	else if(width <= 624 ) { height = 520; }
	else if(width <= 649 ) { height = 530; }
	else if(width <= 674 ) { height = 540; }
	else if(width <= 699 ) { height = 550; }
	else if(width <= 724 ) { height = 560; }
	else if(width <= 749 ) { height = 570; }
	else if(width <= 774 ) { height = 580; }
	else if(width <= 799 ) { height = 590; }
	else if(width <= 824 ) { height = 600; }
	else if(width <= 849 ) { height = 610; }
	else if(width <= 874 ) { height = 620; }
	else if(width <= 899 ) { height = 630; }
	else if(width <= 924 ) { height = 640; }
	else if(width <= 949 ) { height = 650; }
	else if(width <= 974 ) { height = 660; }
	else if(width <= 999 ) { height = 670; }
	else if(width === 1000) { height = 680; }
	
	return height;
}

function setSlideHeaderDesign(width) {

	height = 109;
	if(width <= 299 ) { height = 109; }
	else if(width <= 324 ) { height = 120; }
	else if(width <= 424 ) { height = 120; }
	else { height = 140; }
	
	fs_pn = 22;
	if(width <= 299 ) { fs_pn = 22; fs_price = 16; }
	else if(width <= 399 ) { fs_pn = 28; fs_price = 20; }
	else if(width <= 499 ) { fs_pn = 35; fs_price = 24; }
	else if(width <= 699 ) { fs_pn = 38; fs_price = 26; }
	else { fs_pn = 40; fs_price = 28; }
	
	
	$('.prodname-wrapper').height(height);
	$('.prodname').css({'font-size':fs_pn+'px'});
	$('.price').css({'font-size':fs_price+'px'});
	/*
	$('.prodname').each(function() { 
		$(this).css({'font-size':fs_pn+'px'});	
		var height, width, fs = fs_pn;
		do {
			height = $(this).height();
			width = $(this).width();
			fs--;
		} while(height > 109 && fs >= 20)
		fs++;
		$(this).css({'font-size':fs+'px'});
	});
	
	$('.price').each(function() { 
		$(this).css({'font-size':fs_price+'px'});	
		var height, width, fs = fs_price;
		do {
			height = $(this).height();
			width = $(this).width();
			fs--;
		} while(height > 105 && fs >= 16)
		fs++;
		$(this).css({'font-size':fs+'px'});
	});
	*/
}

function adjust_design(firstload) {
	var mw_height = 360, mw_width;
	mw_width = $('#main-wrapper').width();
	mw_height = getHeight(mw_width);
	swcontainer_h = 321;
	$('#main-wrapper').css({height:mw_height});
	
	setSlideHeaderDesign(mw_width);
	
	// footer
	if(mw_width < 600) {
		footer_h = 39;
		swcontainer_h = mw_height - footer_h;
		$('#cart-wrapper').css({'margin-top':'7px'});
		$('.pby-wrapper .pby-shopstyx').css({'font-size':11+'px'});
		cart_shopstyx_width = 32 + 71;
		
		$('.swiper-container .btn-wrapper .icon-wrapper div.ss-icons').css({width:38, height:40});
		$('.swiper-container .btn-buy').css({height:34});
		$('.swiper-container .btn-buy div').css({height:32});
	}
	else {
		footer_h = 37;
		swcontainer_h = mw_height - footer_h;
		$('#cart-wrapper').css({'margin-top':'5px'});
		$('.pby-wrapper .pby-shopstyx').css({'font-size':14+'px'});
		cart_shopstyx_width = 32 + 88;
		
		$('.swiper-container .btn-wrapper .icon-wrapper div.ss-icons').css({width:48, height:50});
		$('.swiper-container .btn-buy').css({height:38});
		$('.swiper-container .btn-buy div').css({height:36});
	}
	
	$('.swiper-container').css('height',swcontainer_h);
	
	// fix photo slider height
	var slider_height = $('.swiper-container').height();
	$('.swiper-slide').each(function() {
		$(this).find('.photo-wrapper').css({height:(slider_height-$(this).find('.prodname-wrapper').height())});
	});

	if(mw_width < 209 || mw_width > 600) { 
		$('.btn-wrapper .btn-buy').css({'margin-top':'7px'});
	}
	else {
		$('.btn-wrapper .btn-buy').css({'margin-top':'4px'});
	}
	
	$('#footer').css({'height':footer_h+'px', 'line-height':footer_h+'px'});
	$('.pby-wrapper').css({'height':footer_h});
	
	if(firstload) return;
	
	var fs, sn_margin, sn_maxwidth, sn_lineheight;
	if(mw_width <= 299 ) { sn_fs = 12; sn_margin = 14; sn_lineheight = 11; }
	else if(mw_width <= 399 ) { sn_fs = 14; sn_margin = 16;  sn_lineheight = 12;  }
	else if(mw_width <= 499 ) { sn_fs = 16; sn_margin = 18;  sn_lineheight = 13;  }
	else if(mw_width <= 699 ) { sn_fs = 17; sn_margin = 22;  sn_lineheight = 14;  }
	else { sn_fs = 18; sn_margin = 26;  sn_lineheight = 15;  }
	
	sn_maxwidth = mw_width - sn_margin - cart_shopstyx_width
	$('#storename').css({'font-size':sn_fs+'px','line-height':footer_h+'px','padding-left':sn_margin+'px','max-width':sn_maxwidth}).addClass('floatleft');
	if($('#storename').height() > footer_h) {
		//sn_fs -= 1;
		$('#storename').css({'font-size':sn_fs,'line-height':sn_lineheight+'px'}).removeClass('floatleft');
	}
}

function adjust_detail_design() {
	if($.trim($('#detail-wrapper').html()) == "") {
		return;
	}
	
	mw_width = $('#main-wrapper').width();
	mw_height = getHeight(mw_width);
	
	var menu_height, menu_lineheight, menu_border, menutxt_fs, menutxt_padding,
		footer_height,footer_ssicon_width, footer_ssicon_height, btn_height, btn_mTop, btnDiv_height,
		cart_mTop, cart_mRight, detail_fs, thumb_WH,
		borderTop_top, borderBottom_bottom;
		
	if(mw_width <= 299 ) { 
		menu_height = 38; menu_border = 8; menutxt_fs = 11; menutxt_padding = '0 5px'; 
		footer_height = 59; footer_ssicon_width = 38; footer_ssicon_height = 40,
		btn_height = 34, btn_mTop = 4, btnDiv_height = 32, cart_mTop = 6, cart_mRight = 10,
		detail_fs = 12, thumb_WH = 18, borderTop_top = -4, borderBottom_bottom = -5;
	}
	else if(mw_width <= 399 ) { 
		menu_height = 41; menu_border = 10; menutxt_fs = 13; menutxt_padding = '0 8px'; 
		footer_height = 64; footer_ssicon_width = 41; footer_ssicon_height = 43;
		btn_height = 35, btn_mTop = 4, btnDiv_height = 33, cart_mTop = 7, cart_mRight = 12,
		detail_fs = 12, thumb_WH = 23, borderTop_top = -2, borderBottom_bottom = 0;
	}
	else if(mw_width <= 499 ) {
		menu_height = 44; menu_border = 12; menutxt_fs = 14; menutxt_padding = '0 10px'; 
		footer_height = 69; footer_ssicon_width = 44; footer_ssicon_height = 46;
		btn_height = 36, btn_mTop = 5, btnDiv_height = 34, cart_mTop = 8, cart_mRight = 15,
		detail_fs = 13, thumb_WH = 29, borderTop_top = -2, borderBottom_bottom = -2;
	}
	else if(mw_width <= 699 ) {
		menu_height = 47; menu_border = 14; menutxt_fs = 15; menutxt_padding = '0 14px'; 
		footer_height = 74; footer_ssicon_width = 47; footer_ssicon_height = 49;
		btn_height = 37, btn_mTop = 6, btnDiv_height = 35, cart_mTop = 9, cart_mRight = 18,
		detail_fs = 14, thumb_WH = 35, borderTop_top = -1, borderBottom_bottom = -1;
	}
	else { 
		menu_height = 50; menu_border = 15; menutxt_fs = 16; menutxt_padding = '0 19px'; 
		footer_height = 77; footer_ssicon_width = 48; footer_ssicon_height = 50;
		btn_height = 38, btn_mTop = 7, btnDiv_height = 36, cart_mTop = 10, cart_mRight = 21,
		detail_fs = 15, thumb_WH = 40, borderTop_top = 0, borderBottom_bottom = 0;
	}
	
	$('#detail-wrapper #menu').css({'height':menu_height+'px', 'line-height':menu_height+'px'});
	$('#detail-wrapper #menu div.border').css({'top':menu_border+'px'});
	$('#detail-wrapper #menu div.txt').css({'font-size':menutxt_fs+'px', 'padding':menutxt_padding});
	
	$('#detail-footer').css({height:footer_height});
	$('#detail-wrapper .btn-wrapper .icon-wrapper div.ss-icons').css({width:footer_ssicon_width, height:footer_ssicon_width});
	$('#detail-wrapper .btn-buy').css({height:btn_height,'margin-top':btn_mTop+'px'});
	$('#detail-wrapper .btn-buy div').css({height:btnDiv_height});
	$('#detail-footer #cart-wrapper').css({'margin-top':cart_mTop+'px','margin-right':cart_mRight+'px'});
	
	var dc_height = mw_height - (menu_height + $('#detail-footer').outerHeight());
	$('#detail-content').css({height:dc_height,'font-size':detail_fs+'px'});
	$('.thumb').css({width:thumb_WH, height:thumb_WH});
	
	$('.bordershadows').css({top:borderTop_top});
	$('.bordershadows-bottom').css({bottom:borderBottom_bottom});
}

function adjust_cart_design() {
	if($.trim($('#wdgt-cart-popup').html()) == "") {
		return;
	}
	
	$('.wgt-item-list-detials').removeAttr('style');
	
	var mw_width = $('#main-wrapper').width();
	var mw_height = $('#main-wrapper').height();
	
	$('#wdgt-added-price').removeAttr('style');
	$('#checkout').removeAttr('style');
	
	// subtotal and checkout button
	if(mw_width < 294) {
		$('#checkout').css({width:130,'font-size':'9px','margin':'8px 4px 0 0','text-indent':'6px'});
		if(mw_width < 256) {
			$('#wdgt-added-price').css({width:50,'font-size':'10px','line-height':'11px','margin-top':'7px'});
		}
	}
	
	$('#wdgt-cart-main-center-wrapper').height(mw_height-64-12);
	/*
	$('#wdgt-cart-main-center-wrapper').jScrollPane();
	
	var $stylesheet = $('<style type="text/css" media="screen">\
                    .scrollVertical{height:'+(mw_height-64-14)+'px !important;}\
                    </style>');

	$('body').append($stylesheet);
	$('#wdgt-cart-popup .jspVerticalBar').addClass('scrollVertical');
	$('#wdgt-cart-popup .jspTrack').addClass('scrollVertical');
	*/
	var w = $('#wdgt-list li').eq(0).outerWidth(true);	
	$('.wgt-item-list-detials').css({width:w-114-9});
	$('#wdgt-list li').find('.item-list-picture-wrapper').removeAttr('style');
	$('#wdgt-list li').each(function(i,element){
		var h = $(element).height();	
		$(element).find('.item-list-picture-wrapper').css({height:h});
	});
}

function centerized_option() {
	if($.trim($('#option-wrapper').html()) == "") {
		return;
	}
	
	var mw_w= $('#main-wrapper').width();
	var mw_h = $('#main-wrapper').height();
	var w = $('#option-wrapper #option-content').outerWidth();
	var h = $('#option-wrapper #option-content').outerHeight();
	
	var l = (mw_w - w) / 2;
	var t = (mw_h - h) / 2;
	l = (l < 0) ? 0 : l;

	$('#option-wrapper #option-content').css({left:l,top:t});
}

function show_selectoptions(data) {
	//preLoader($('#ms-wrapper'));
	$('#loader').show();
	
	var form_data = {};
		/*
		form_data.key = $('#key').val();
		form_data.user_id = $('#ms_uid').val();
		form_data.store_id = $('#ms_sid').val();
		form_data.widget_type = $('#widget_type').val();
		form_data.home_type = $('#home_type').val();
		*/
	$.extend(form_data,data);	
	$.post( '/stykable-product-slideshow/'+stykable_url+'/option.html', data, function(data){
		//$('#preloader-wrapper').hide();
		$('#loader').hide();
		$('#option-wrapper').html(data).show();
		centerized_option();
		//jSpane_api_product_option.reinitialise();
		customized_select();
	});
}

function customized_select() {
	$('.customized_select select').each(function(){
		if($(this).parent().find('div').length == 0) {
			title = $('option:selected',this).text();
			$(this)
				.css({'z-index':10,'opacity':0,'-khtml-appearance':'none'})
				.after('<div>' + title + '</div>')
				.change(function(){
					val = $('option:selected',this).text();
					$(this).next().text(val);
				})
		}
	});
}



function addtocart(data) {
	if(typeof(data) != 'object') return;
	
	var product_user_id = data.product_user_id; // product owner or merchant
	var product_id = data.product_id;
	var store_id = data.store_id;
	var store_user_id = data.store_user_id; // endorser or store owner
	var quantity = data.quantity;
	var has_options = data.has_options;
	data.product_options = {};
	
	// get existing cart values
	var cart = store.get('cart');
	cart = (typeof(cart) != 'object' || cart == null) ? {} : cart;

	//console.log(cart.merchants[user_id].products.hasOwnProperty(1));
	//console.log(cart);
	// retreive existing 
	var item, option = '';
	var error_option = false;
	var variants_values = new Array();
	
	/*
	for (var key in cart) {
		if (cart.hasOwnProperty(key)) {
			total_merchant++;
			new_cart[key] = cart[key]
		}
	}
	*/
	
	// Option Validations
	$('.option-value-wrapper').each(function() {
		if($(this).find('select').val() == '') {
			error_option = true;	
		}
		
		data.product_options[$(this).attr('option_id')] = $(this).find('select').val();
		variants_values.push($(this).find('select').val());
	});
	
	var options_string = variants_values.join(',');
	if($('#unique_variants').length > 0) {
		var variants = $.parseJSON($('#unique_variants').attr('variants'));
		var variant_id = 0;
		for (var key in variants) {
			if (variants.hasOwnProperty(key)) {
				if(variants[key] == options_string) {
					variant_id = key;	
				}
			}
		}
	}
	
	if(error_option || (has_options > 0 && variant_id == 0)) {
		alert('Please select an option.');
		return;	
	}

	data.variant_id = variant_id;
	data.cart = cart;
	data.user_id = store.get('user_id');
	data.token = store.get('token');
	
	//$.extend(data,ms_getQueryParams());
	//preLoader($('#ms-wrapper'));
	$('#loader').show();
	$('#option-wrapper').html('').hide(); 
	$.ajax({
		type: "POST",
		url: '/stykable-product-slideshow/'+stykable_url+'/update-cart.html',
		data: data,
		cache: false,
		success: function(res){
			cart = $.parseJSON(res);
			store.set('cart',cart);
				
			//preLoader($('#ms-wrapper'));
			load_cart({});
		}
	});
}

function load_cart_total() {
	var form_data = {cart: store.get('cart'),
					user_id: store.get('user_id'),
					token: store.get('token'),
					cart_sess: store.get('cart_sess')
					};
	$.post( '/stykable-product-slideshow/'+stykable_url+'/cart-items.html', form_data,function(data){
		data = $.parseJSON(data);
			$('#cart-icon div').html(data.total_item);
			$('#cart-wrapper').fadeIn(300);
	});
}

function load_cart(data) {
	if(typeof(data) == 'undefined') {
		return;	
	}
	var form_data = {cart: store.get('cart'),
					user_id: store.get('user_id'),
					token: store.get('token'),
					cart_sess: store.get('cart_sess')
					};
	$.extend(form_data,data);
	
	$.post( '/stykable-product-slideshow/'+stykable_url+'/cart.html', form_data,function(data){
		$('#wdgt-cart-popup').html(data).show();
		//$('#preloader-wrapper').hide();
		adjust_cart_design();
		load_cart_total();
		$('#loader').hide();
	});
}

function update_cart_quantity(data) {
	if(typeof(data) != 'object') return;
	/*
	var product_user_id = data.product_user_id; // product owner or merchant
	var product_id = data.product_id;
	var store_id = data.store_id;
	var store_user_id = data.store_user_id; // endorser or store owner
	var quantity = data.quantity;
	var has_options = data.has_options;
	*/
	var variant_id = data.variant_id;
	data.product_options = {};
	// get existing cart values
	var cart = store.get('cart');
	cart = (typeof(cart) != 'object' || cart == null) ? {} : cart;
	
	data.variant_id = variant_id;
		
	data.cart = cart;
	data.user_id = store.get('user_id');
	data.token = store.get('token');
	data.cart_sess = store.get('cart_sess');

	//preLoader($('#ms-wrapper'));
	$('#loader').show();
	$.ajax({
		type: "POST",
		url: '/stykable-product-slideshow/'+stykable_url+'/update-cart.html?update=new_quantity',
		data: data,
		cache: false,
		success: function(res){
			cart = $.parseJSON(res);
			store.set('cart',cart);
			load_cart({});
		}
	});
}

function delete_cart_product(data) {
	if(typeof(data) != 'object') return;
	var variant_id = data.variant_id;
	data.product_options = {};
	// get existing cart values
	var cart = store.get('cart');
	cart = (typeof(cart) != 'object' || cart == null) ? {} : cart;
	
	data.variant_id = variant_id;
		
	data.cart = cart;
	data.user_id = store.get('user_id');
	data.token = store.get('token');
	data.cart_sess = store.get('cart_sess');

	//preLoader($('#ms-wrapper'));
	$('#loader').show();
	$.ajax({
		type: "POST",
		url: '/stykable-product-slideshow/'+stykable_url+'/update-cart.html?update=delete_item',
		data: data,
		cache: false,
		success: function(res){
			cart = $.parseJSON(res);
			store.set('cart',cart);
			load_cart({});
		}
	});
}


$(window).load(function() {
	
	if(is_mobile == 0) {
		$(window).bind( 'orientationchange, resize', function(e){
		//$(document).on(	'orientationchange, resize', function(){
			adjust_design();
			adjust_detail_design();
			centerized_option();
			adjust_cart_design();
			/*
			if ($.event.special.orientationchange.orientation() == "portrait") {
				//Do whatever in portrait mode
			} else {
				//Do Whatever in landscape mode
			}
			*/
		});
	}
	adjust_design(true);
	
	if(total_products > 1) {
		mySwiperOptions = {mode:'horizontal',
							loop: true,
							keyboardControl:true,
							speed: 300
							}
		if(!ispreview) {
			mySwiperOptions.autoplay = 2000;
		}
		mySwiper = new Swiper('.swiper-container', mySwiperOptions);
	}
	
	adjust_design();
	
	$(document).on('click', '.arrow-left', function(e) {
		e.preventDefault(); 
		e.stopImmediatePropagation();
		if(typeof(mySwiper) != "undefined") {
			mySwiper.swipePrev();
		}
	});
	
	$(document).on('click', '.arrow-right', function(e) {
		e.preventDefault(); 
		e.stopImmediatePropagation();
		if(typeof(mySwiper) != "undefined") {
			mySwiper.swipeNext();
		}
	});
	
	$(document).on('click', '.photo-wrapper .info, .prodname-wrapper .prodname', function(e) {
		e.preventDefault(); 
		e.stopImmediatePropagation();
		$('#loader').show();
		if(typeof(mySwiper) != "undefined") {
			setTimeout(function(){
	        	mySwiper.stopAutoplay();
	    	}, 1000);
		}
		
		form_data = {uid:$(this).attr('uid'),pid:$(this).attr('pid'),cart:store.get('cart')};
		$.ajax({
			type: "POST",
			url: '/stykable-product-slideshow/'+stykable_url+'/details.html',
			data: form_data,
			success: function(data){
				$('#detail-wrapper').html(data);
				adjust_detail_design();
				$('#loader').hide();
				$('.detail-content').hide();
				$('#tab-info').show();
				$('#detail-wrapper').show();
			}
		});
	});
	
	$(document).on('click', '#btn-close', function(e) {
		e.preventDefault(); 
		e.stopImmediatePropagation();
		$('#detail-wrapper').html('').hide();
	});
	
	$('.ss-icons.share').hover(
		function() {
			$('.arrow-left').hide();
			$('.arrow-right').hide();
			$(this).find('#sharebox').show();
		},
		function() {
			$('.arrow-left').show();
			$('.arrow-right').show();
			$(this).find('#sharebox').hide();
		}
	);
	
	if(typeof(mySwiper) != "undefined") {
		$('.swiper-container').hover(
			function() {
				mySwiper.stopAutoplay();
			},
			function() {
				mySwiper.startAutoplay();
			}
		);
	}
	
	$(document).on('click', '#cart-icon', function(e) {
		e.preventDefault(); 
		e.stopImmediatePropagation();
		$('#loader').show();
		load_cart({});
	});
	
	$(document).on('click', '#menu .txt', function(e) {
		e.preventDefault(); 
		e.stopImmediatePropagation();
		
		$('#menu .txt').removeClass('selected');
		$(this).addClass('selected');
		
		$('.detail-content').hide();
		$('#tab-'+$(this).attr('content')).show();
	});
	
	$(document).on('click', '.thumb', function(e) {
		e.preventDefault(); 
		e.stopImmediatePropagation();
		$('#tab-photos').css({'background-image':'url("'+$(this).attr('img')+'")'});

		$('.thumb.selected').css({'background': 'url("'+$('.thumb.selected').attr('img')+'") no-repeat center center',
									'background-size': 'cover'});
		$('.thumb.selected').removeClass('bordercolor1 selected').addClass('bordercolor2');
		
		$(this).removeClass('bordercolor2').addClass('bordercolor1 selected').removeAttr('style');
		adjust_detail_design();
	});
	
	
	$(document).on('click','#close-btn', function(e){
		e.preventDefault(); 
		e.stopImmediatePropagation();
		$('#option-wrapper').hide().html(''); 
		if(typeof(mySwiper) != "undefined") {
			mySwiper.startAutoplay();
		}
	});
	
	$(document).on('click','.btn-buy', function(e){
		e.preventDefault(); 
		e.stopImmediatePropagation();

		if(typeof(mySwiper) != "undefined") {
			setTimeout(function(){
	        	mySwiper.stopAutoplay();
	    	}, 100);
		}

		var el = $(this);
		var qty = 1;
		var data = $.parseJSON(el.attr('data'));
		var data = {product_user_id:data.p_uid,
					product_id:data.p_id, 
					product_store_id: data.store_id,
					product_store_url: parent_embed_url,
					store_id:data.store_id,
					store_user_id:data.store_uid ,
					quantity: parseInt(qty),
					has_options: data.has_options };
					
		if(parseInt(data.has_options) > 0) {
			show_selectoptions(data);
		}
		else {
			addtocart(data);	
		}
	});
	
	$(document).on('change','.option-value-wrapper .customized_select select', function() {
		var variants_values = $.parseJSON($('#unique_variants').attr('variants_values'));
		var unique_variants = $.parseJSON($('#unique_variants').val());
		var index = parseInt($('.option-value-wrapper .customized_select select').index(this));
		var value = $(this).val();
		var next_option = (index + 1);

		if(index == 0) {
			$('.option-value-wrapper .customized_select').eq(1).addClass('disabled');	
			$('.option-value-wrapper .customized_select').eq(2).addClass('disabled');	
			$('.option-value-wrapper .customized_select select').eq(1).val('');
			$('.option-value-wrapper .customized_select select').eq(2).val('');
			$('.option-value-wrapper .customized_select select').eq(1).attr('disabled','disabled');	
			$('.option-value-wrapper .customized_select select').eq(2).attr('disabled','disabled');	
			$('.option-value-wrapper .customized_select div').eq(1).html('Select Item');
			$('.option-value-wrapper .customized_select div').eq(2).html('Select Item');
			
			$(this).parent().removeClass('disabled');
			$(this).removeAttr('disabled');
		}

		if(typeof $('.option-value-wrapper .customized_select select').eq(next_option) != "undefined" && value != "") {
			$('.option-value-wrapper .customized_select select').eq(next_option).find('option').hide();
			$('.option-value-wrapper .customized_select select').eq(next_option).find('option').eq(0).show();
			$('.option-value-wrapper .customized_select select').eq(next_option).removeAttr('disabled');
			$('.option-value-wrapper .customized_select select').eq(next_option).parent().removeClass('disabled');
			$('.option-value-wrapper .customized_select select').eq(next_option).val('');
			$('.option-value-wrapper .customized_select div').eq(next_option).html('Select Item');
			for(i = 0; i < variants_values.length; i++) {
				if(variants_values[i][index] == value) {
					$('.option-value-wrapper .customized_select select').eq(next_option).find('option[value='+variants_values[i][next_option]+']').show();	
				}
			}
		}
	});
	
	$(document).on('click','.wdgt-popup-close', function(e){
		e.preventDefault(); 
		e.stopImmediatePropagation();
		$('#wdgt-cart-popup').html('').hide();
		
		/*
		if(typeof(mySwiper) != "undefined") {
			setTimeout(function(){
	        	mySwiper.startAutoplay();
	    	}, 100);
		}
		*/
	});
	
	$(document).on('click', '#cart-content .wgt-item-list-lastcolum a', function() {
		//preLoader($('#ms-wrapper'));
		$('#loader').show();
		var quantity = $(this).parent().find('input').val();
		var m = $(this).parent().find('input').attr('m');
		var p = $(this).parent().find('input').attr('p');
		var variant_id = parseInt($(this).parent().find('input').attr('variant_id'));
		var su = $(this).parent().find('input').attr('su');
		var s = $(this).parent().find('input').attr('s');
		var has_options = 0;
		
		if(typeof(variant_id) != 'undefined' && variant_id > 0) {
			has_options = 1;
		}
		
		var data = {product_user_id:m,
					product_id:p, 
					product_store_id: s,
					product_store_url: parent_embed_url,
					store_id: s,
					store_user_id: su,
					quantity: quantity,
					has_options: has_options,
					variant_id: variant_id};
		update_cart_quantity(data);
	});
	
	$(document).on('click', '#cart-content .wgt-item-list-cancel', function() {
		//preLoader($('#ms-wrapper'));
		$('#loader').show();
		var quantity = $(this).parent().find('input').val();
		var m = $(this).parent().find('input').attr('m');
		var p = $(this).parent().find('input').attr('p');
		var variant_id = $(this).parent().find('input').attr('variant_id');
		var su = $(this).parent().find('input').attr('su');
		var s = $(this).parent().find('input').attr('s');
		var has_options = 0;
		
		if(typeof(variant_id) != 'undefined' && variant_id > 0) {
			has_options = 1;
		}
		var data = {product_user_id:m,
					product_id:p, 
					store_id: s,
					store_user_id: su,
					quantity: quantity,
					has_options: has_options,
					variant_id: variant_id};
		delete_cart_product(data);
	});
	
	$('#loader').hide();
	
	//
	setTimeout(load_cart_total, 100);
});
