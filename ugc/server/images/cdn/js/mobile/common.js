var vid = CS.vid,
	cloud_url = CS.cloudURL || location.protocol + '//' + location.host + '/',
	root = CS.siteURL || location.protocol + '//' + location.host + '/',
	main_url = CS.main_url,
	current_url = location.href,
	loadMPhone = true,
	trending_load_status = false;

var SSAjax = function( options ) {
		var form_data = {
				token: store.get('token'),
				user_id: store.get('user_id'),
				_Ssfp_ID: store.get('_Ssfp_ID')
			};

        // Default options.
        var settings = $.extend(true,{
            // These are the defaults.
			loadCSS: [],
            type: "POST",
            data: form_data,
            url: "",
            success: function(response) {
            	//console.log(response);
            },
            loadJS: []
            
        }, options );

        // loading external css and js
        var jsLib = "", cssLib = "", loadJS = new Array(), loadCSS = new Array();
       	
		var success = settings.success;
		settings.success = function(response, textStatus, xmlHttp) {
			
			if(response == "" || response == false) {
				// invalid toke and/or user_id, so logout
			}
			success(response);	 
		};

		var token = store.get('token'), 
			last_stamp = store.get('session_date'),
			remember = store.get('remember');

		// check here if logged In session has already been expired
		var session = check_login_session({
							"token": token,
							"last_stamp": last_stamp,
							"remember": remember
						});

		// if session is expired, make a pop-up here
		if(!session) {
			//alert("Your session has been expired due to inactivity!");
			//autoLogout();
			//return;
		}


		var login_data = {post_data:1,
							token:token,
							user_id:settings.data.user_id,
							shopstyx_utz:store.get('shopstyx-utz'),
						};

		if(typeof store.get('_Ssfp_ID') == "undefined" && (typeof token == "undefined" || typeof settings.data.user_id == "undefined")) {
			login_data._Ssfp = $.fingerprint();
		}
		else {
			login_data._Ssfp_ID = store.get('_Ssfp_ID');
		}

		// check here if user is logged In
		$.ajax({url:"/checkuser.html",
				type:'post',
				data: login_data,
				success: function(data){
						store.remove('shopstyx-utz');

						var _data = $.parseJSON(data);
						if(typeof _data._Ssfp_ID != "undefined" && !$.isEmptyObject(_data._Ssfp_ID) && _data._Ssfp_ID != 0) {
							store.set('_Ssfp_ID',_data._Ssfp_ID);
							settings.data._Ssfp_ID = _data._Ssfp_ID;
						}

						if( _data.result){
							store.set('display_name', _data.display_name);
							store.set('user_id', _data.user_id);
							//console.log(settings);
						}
						else {
							/*
							store.remove('token');
							store.remove('user_id');
							store.remove('display_name');
							store.remove('remember');
							*/
						}

						$.ajax(settings);
				}
			});		
	};


var check_login_session = function(options) {
	var new_timestamp = new Date().getTime();
	// milliseconds
	var minutes_to_expire = 120000; // 2 minutes 

	if(( typeof options.token !== 'undefined' || options.token != '' ) 
	&& (options.last_stamp !== 'undefined' || options.last_stamp != '')  
	&& options.remember == 0 ){
		minutes_to_expire = 600000; // 10 minutes
		if ((new_timestamp - options.last_stamp) > 120000) {
			return false;
		}
	}

	if(( typeof options.token !== 'undefined' || options.token != '' ) 
	&& (options.last_stamp !== 'undefined' || options.last_stamp != '')  
	&& options.remember == 1 ){
		minutes_to_expire = 604800000; // 7 days
		if (new_timestamp - options.last_stamp > 604800000) {
			return false;
		}
	}

	// renew the session date
	/*
	if( ( typeof store.get('token') !== 'undefined' || store.get('token') != '' ) 
	&& ( store.get('remember') == 0 || store.get('remember') == 1 ) ){

	}
	*/
	store.set('session_date', new_timestamp);
	return true;
};

// check every 6 seconds if session has been expired	
/*
setInterval(function() {
	var token = store.get('token'), 
			last_stamp = store.get('session_date'),
			remember = store.get('remember');

	// check here if logged In session has already been expired
	var session = check_login_session({
						"token": token,
						"last_stamp": last_stamp,
						"remember": remember
					});

	// if session is expired, make a pop-up here
	if(!session) {
		//alert("Your session has been expired due to inactivity!");
		//autoLogout();
		return;
	}

}, 6000);	
*/

function autoLogout(){
	var url = root + 'signout.html';
	var form_data = {};
	form_data.post_dta = 1;
	form_data.temp_sessionid = store.get('temp_sessionid');
	form_data.token = store.get('token');

		
	$.ajax({
		type: "POST",
		url: url,
		data: form_data,
		cache: false,
		success: function(data){
			if(data == 'success'){
				//store.clear();
				store.remove('user_id');
				store.remove('cart');
				store.remove('token');

				var currentUrl = location.href.replace('#','');
				var pos = root.indexOf("?");
				var querystring = (pos == -1) ? "?referer="+ escape(currentUrl) : '&referer=';
				if(root != currentUrl) {
					location.href = root + querystring;
				}
				else {
					location.href = root;
				}
			}
		}
	});
	return false;
}

function hash_querystring(hash){
	var query = {};
	if(typeof hash == "undefined" || hash == "") return query;

	var hash = hash.split('&');
	var q;

	for(var i = 0; i < hash.length; i++) {
		if(hash[i] != "") {
			q = hash[i].split('=');
			if(typeof(q[1]) != "undefined" && q[1] != "") {
				query[q[0]] = q[1].toString();
			}
		}
	}
	return query;
}		

function get_hash_querystring_value(hash,name) {
	var hash = hash_querystring(hash);
	if(hash.hasOwnProperty(name)) {
		for(i in hash) {
			if(i == name) {
				return hash[i];
			}
		}
	}

	return "";
}

function load_page(param,pagetype) {
	var url = $(location).attr('href').split('#');
	var mainURL = url[0], hash = url[1];
	
	hash = typeof hash == "undefined" ? "" : hash;
	//var container = get_hash_querystring_value(hash,'c');

	var param = (typeof param == "undefined") ? {} : param; 
	var container = "#wrapper";
	var page_title;
	var user_id = store.get('user_id');
	var token = store.get('token');
	var form_data = {
		isAjax:true,
		token: token,
		user_id: user_id
	};
	var browserNav = false;
	
	if(param.hasOwnProperty('container')) {
		container = param.container;
	}

	if(param.hasOwnProperty('data')) {
		form_data = $.extend(true,form_data, param.data);
	}

	form_data.shipto_country = store.get('shipto_country');

	if(param.hasOwnProperty('page_tile')) {
		page_title = param.page_title;
	}
	else {
		page_title = $(document).find("title").text();
	}

	if(param.hasOwnProperty('url')) {
		mainURL = param.url;
		if(mainURL.indexOf(CS.siteURL) == -1) {
			mainURL = CS.siteURL + mainURL.substr(1);
		}
	}	

	if(param.hasOwnProperty('browserNav')) {
		browserNav = (param.browserNav) ? true : false;
	}

	if(browserNav) {
		return;
	}

	if(container == "#subcontent-wrapper") {
		toggle_preloader_maincontent();
	}

	SSAjax({
		data: form_data,
		url: mainURL,
		success:function(response) {
			if(pagetype == "checkout") {
				try {
					var data = $.parseJSON(response);
					if(!$.isEmptyObject(data) && typeof data.redirect_code != "undefined") {
						/*
						switch(data.redirect_code) {
							case 100:
								location.href = data.url;
								break;
						}
						*/
						location.href = data.url;
						
						return;
					}
				} catch(e) {

				}

			}

			$(container).html(response).removeClass('hide');
			hideLoader();
		},
		error: function(jqXHR, textStatus, errorThrown ) {
			if(jqXHR.status == 404) {
				location.href = "/";
			}
		}
	});
}

function showLoader(text){
	if(typeof text != "undefined" && $.trim(text) != "") {
		$('#loadbox-wrapper div').html(text);
	}
	$('#loadbox-wrapper').removeClass('hide');
}

function hideLoader(){
	$('#loadbox-wrapper').addClass('hide').find('div').html('loading...');
}

function open_addproduct_menu_modal() {
	$('body').addClass('hidescroll');
	$('.overlay').show();
	$('#add-product-button').hide();
	$('#addproduct_menu_modal').show()
}

function close_addproduct_menu_modal() {
	$('body').removeClass('hidescroll');
	$('.overlay').hide();
	$('#add-product-button').show();
	$('#addproduct_menu_modal').hide()
}

function truncate_string(data) {
	if(typeof data.height == "undefined") {
		data.height = null;
	}

	$(data.container).dotdotdot({
		/*	The text to add as ellipsis. */
		ellipsis	: '... ',

		/*	How to cut off the text/html: 'word'/'letter'/'children' */
		wrap		: 'word',

		/*	Wrap-option fallback to 'letter' for long words */
		fallbackToLetter: true,

		/*	jQuery-selector for the element to keep and put after the ellipsis. */
		after		: null,

		/*	Whether to update the ellipsis: true/'window' */
		watch		: false,

		/*	Optionally set a max-height, if null, the height will be measured. */
		height		: data.height,

		/*	Deviation for the height-option. */
		tolerance	: 0,

		/*	Callback function that is fired after the ellipsis is added,
			receives two parameters: isTruncated(boolean), orgContent(string). */
		callback	: function( isTruncated, orgContent ) {},

		lastCharacter	: {

			/*	Remove these characters from the end of the truncated text. */
			remove		: [ ' ', ',', ';', '.', '!', '?' ],

			/*	Don't add an ellipsis if this array contains 
				the last character of the truncated text. */
			noEllipsis	: []
		}
	});
}

/* 
 * Events 
 */
$(document).on('click', '.nav-profile .clickable-thumb', function(e){ 
	e.preventDefault(); e.stopImmediatePropagation(); 
});

$('body').on('doubletap', '.nav-profile .clickable-thumb', function() {
	location.href = $(this).attr('href');
});

$(document).on('click touchstart MSPointerDown', '.logout', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	autoLogout();
});

$(document).on('click touchstart MSPointerDown', '#add-product-button', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	open_addproduct_menu_modal();
});

$(document).on('click touchstart MSPointerDown', '.overlay, #addproduct_menu_modal, #addproduct_menu_modal .inner-wrapper', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	close_addproduct_menu_modal();
});
$(document).on('click touchstart MSPointerDown', '#addproduct_menu_modal .inner-wrapper .menu-btn', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	if($(this)[0].nodeName.toLowerCase() == "a") {
		location.href = $(this).attr('href');
	}
	else {
	}
});
$(document).on('click touchstart MSPointerDown', '.filter-wrapper .color-filter, .filter-wrapper .pricetab, .filter-wrapper .departmenttab', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	$('.filter-values-wrapper').hide();
	var t = $(this).attr('filtertab');

	if($(this).hasClass('color-filter') || $(this).hasClass('pricetab') || $(this).hasClass('departmenttab')) {
		$('.' + t + '-filter-values').show();
		$('.filter-inner-wrapper').focus();
	}
});
$(document).on('blur', '.filter-inner-wrapper', function(e){
	e.preventDefault(); e.stopImmediatePropagation();
	$('.filter-values-wrapper').hide();
});


