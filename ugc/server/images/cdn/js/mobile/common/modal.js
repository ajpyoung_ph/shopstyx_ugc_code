$.extend({
	parseJSON: function( data ) {
		if ( typeof data !== "string" || !data ) {
			return null;
		}
		data = jQuery.trim( data );
		if ( /^[\],:{}\s]*$/.test(data.replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g, "@")
			.replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, "]")
			.replace(/(?:^|:|,)(?:\s*\[)+/g, "")) ) {
			return window.JSON && window.JSON.parse ?
			window.JSON.parse( data ) :
			(new Function("return " + data))();
		}
	}
});
	
var msPopupId = '#popup', 
	msDialogId = '#dialog-box', 
	getAjaxCache = {},
	vid = CS.vid,
	cloud_url = CS.cloudURL || location.protocol + '//' + location.host + '/';
	main_url = CS.siteURL || location.protocol + '//' + location.host + '/';
	root = CS.rootURL || location.protocol + '//' + location.host + '/';
	store_uri = CS.storeURI;
	
typeof jQuery !== "undefined" && jQuery( function( )
{
	$.getScript(cloud_url + 'js/mobile/common/function/modalFunction.js', 
	function(data, textStatus){
	
		$(document.body).delegate('#loginform', 'submit', function(event){ event.preventDefault(); LoginForm($(this));});
		
		$(document.body).delegate('#logout', 'click', function(){ Logout();});
		
		$(document.body).delegate('#registerform', 'submit', function(event){ event.preventDefault(); RegisterForm($(this));});
	});
});

function Loader(){
	//Load cart items
	var form_data = {user_id:store.get('user_id'),
					token:store.get('token'),
					cart_sess: store.get('cart_sess'), 
					cart: get_cart_data(),
					storeURI:CS.storeURI};
	$.ajax({
		type: "POST",
		url: main_url + "ajax.html?content=cartitems",
		data: form_data,
		crossDomain: true,
		cache: false,
		success: function(response){
			var _data = $.parseJSON(response);
			var total_item = parseInt(_data.total_item);
			var total_price = parseFloat(_data.total_price);
			if(total_item > 0) {
				$('#cartCountValue').html(total_item).show();
			}
		}
	});
	
	var user_id = store.get('user_id');
	if(typeof user_id !== 'undefined'){
		var token = store.get('token');
		var form_data = {
			postdata: 1,
			token: token,
			user_id: user_id
		};
		$.post(root + "checkuser.html",form_data,function(data){
			var _data = $.parseJSON(data);
			if( _data.result ){
				store.set('display_name', _data.display_name);
				store.set('user_id', _data.user_id);
				store.set('user_login_email', _data.user_login_email);
				if(_data.user_picture != ""){
					$('.account-icon-image').css('background-image', 'url('+cloud_url+'uploads/users_picture/' + encodeURIComponent(_data.user_picture) + ')'); 
					$('.account-icon-image').css('background-size', 'cover'); 
				}
				$('#out-header').addClass('hide');
				$('#in-header').removeClass('hide');
				$('.account-name').html('Howdy, ' + store.get('display_name') + '!');
				$('.settigs-class').removeClass('hide');
				return;
			}
			else {
				$('#in-header').addClass('hide');
				$('#out-header').removeClass('hide');
			}
		});
		
		return;
	}else{
		$('#in-header').addClass('hide');
		$('#out-header').removeClass('hide');
	}
	
	store.remove('token');
	store.remove('display_name');
	store.remove('user_id');
	store.remove('user_login_email');
}


var last_stamp = store.get('session_date');
var new_timestamp = new Date().getTime();

if(( typeof store.get('token') !== 'undefined' || store.get('token') != '' ) && (last_stamp !== 'undefined' || last_stamp != '')  && store.get('remember') == 0 ){
	if (new_timestamp - last_stamp > 120000) {
		autoLogout();
	}
}

if(( typeof store.get('token') !== 'undefined' || store.get('token') != '' ) && (last_stamp !== 'undefined' || last_stamp != '')  && store.get('remember') == 1 ){
	if (new_timestamp - last_stamp > 604800000) {
		autoLogout();
	}
}

var timestamp = new Date().getTime();
store.set('session_date', timestamp);

setInterval(function() {
	if( ( typeof store.get('token') !== 'undefined' || store.get('token') != '' ) && ( store.get('remember') == 0 || store.get('remember') == 1 ) ){
			 var timestamp = new Date().getTime();
			 store.set('session_date', timestamp);
	}
	 
}, 6000);

function autoLogout(){
	var url = root + 'signout.html';
	var form_data = {};
	form_data.post_dta = 1;
	form_data.temp_sessionid = store.get('temp_sessionid');
	form_data.token = store.get('token');
		
	$.ajax({
		type: "POST",
		url: url,
		data: form_data,
		cache: false,
		success: function(data){
			if(data == 'success'){
				store.clear();
				location.href = location.href;
			}
		}
	});
	return false;
}

