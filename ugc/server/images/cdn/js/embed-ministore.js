var _csMs_EventListener=function(obj,type,fn){if(obj.addEventListener){obj.addEventListener(type,fn,false)}else if(obj.attachEvent){obj['e'+type+fn]=fn;obj[type+fn]=function(){obj['e'+type+fn](window.event)};obj.attachEvent('on'+type,obj[type+fn])}};
var _csMs_getsupportedprop=function(proparray){var root=document.documentElement;for(var i=0;i<proparray.length;i++){if(typeof root.style[proparray[i]]=="string"){return proparray[i]}}};
var _csMs_IframeSetup=function(){
	var _csMS_storeUrl;
	if(typeof(_csMS_environment) == 'undefined' || _csMS_environment == '' || _csMS_environment == 'production') {
		_csMS_storeUrl = "http://stagedev.vwala.com/mini-store/";
	}
	else if(_csMS_environment == 'development') {
		_csMS_storeUrl = "http://stagedev.vwala.com/mini-store/";
	}
	else {
		_csMS_storeUrl = _csMS_environment;
	}
	
	
	if(typeof(_csMS_storeKey)=='string'){
		var ms = new Date().getTime().toString();
		var _csMS_ElementId='ministore_'+_csMS_storeKey;
		
		if(typeof(_csMS_storeBorder) != 'boolean') {
			_csMS_storeBorder = false;	
		}
		
		if(!document.getElementById(_csMS_ElementId) || !window.frames[_csMS_ElementId]) {
			return;	
		}
		
		/*
		// This for future use, creating iframe and inserting it to where the current script tag is.
		var sc = document.getElementsByTagName('script');
		var sc_length = sc.length;
		for(var i=0; i < sc_length; i++) {
			console
			//console.log(sc[i].parentNode.childNodes);//getAttribute('src'));	
		}
		
		return;
		var i = document.createElement("iframe");
		i.setAttribute("src", _csMS_storeUrl + _csMS_storeKey);
		i.setAttribute("frameborder", 0);
		i.setAttribute("scrolling", "no");
		i.setAttribute("id", _csMS_ElementId);
		i.setAttribute("name", _csMS_ElementId);
		i.style.width = 600+"px";
		i.style.height = 560+"px";
		document.body.appendChild(i);
		*/		
		if(_csMS_storeBorder) {
			var boxshadowprop=_csMs_getsupportedprop(['boxShadow','MozBoxShadow','WebkitBoxShadow']);
			document.getElementById(_csMS_ElementId).style[boxshadowprop]="0px 0px 3px 1px #CCCCCC";
			document.getElementById(_csMS_ElementId).style.border='1px solid #ffffff;';
		}
		window.frames[_csMS_ElementId].location=_csMS_storeUrl+_csMS_storeKey
	}
};
_csMs_EventListener(window, 'load', _csMs_IframeSetup);