$(document).ready(function(){
	
	$(document).on('click', '.account-settings', function(event){
		event.stopImmediatePropagation();
		clearActiveMenu();
		remove_styx_header_style();
		load_account_page();
	});

	$(document).on('touchstart click', '.account-menu, .account-menu-hidden', function(event){
		event.stopImmediatePropagation();
		clearActiveMenu();
		remove_styx_header_style();
		var settings = $.parseJSON($(this).attr('data'));
		$('#name-wrapper .name-login ul').addClass('hide');
		load_account_page(settings);
	});
	
	$(document).on('mouseenter', '.drop-menu', function(event){
		$('#name-wrapper .name-login ul').removeClass('hide');
	});
	
	$(document).on('click', '#store-create-btn', function(){
		$(this).parents("form").trigger("submit");
		return false;
	});
	
	$(document).on('submit', '#createstore', function(event){
		event.preventDefault();
		if(typeof store.get('user_id') === 'undefined'){ ifnotlogin(); return false; }
	
		$form = $(this);
		$('.loader').remove();
		$('.ajaxloader').html('<img class="loader" src="'+cloud_url+'images/ajax-loader.gif" />');
		
		var error_status = 0;
		$('#store_name').removeClass('input_error');
		
		url = $form.attr( 'action' );
		var form_data = {};

		form_data.store_name = trimText($form.find( 'input[name="store_name"]' ).val());
		form_data.store_description = trimText($form.find( 'textarea[name="description"]' ).val());
		form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
		form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
		form_data.unique_key = $form.find( 'input[name="unique_key"]' ).val();
		if(form_data.store_name == "" || form_data.store_name == "Example: eBags, Electronic Surplus, Boutique, etc."){
			$('#store_name').addClass('input_error');
			error_status++;
		}
		if(error_status > 0){ $('.loader').remove(); return false; }
		$.ajax({
			type: "POST",
			url: url,
			data: form_data,
			cache: false,
			success: function(data){
				$('.loader').remove();
				close_modal($('.create_store'));
				var form_datas = {};
				form_datas.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
				form_datas.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
				form_datas.store_id = data;
				store.set('store_id', data);
				$.post(root + "view-store-content.html", form_datas,function(content){
					$('#store-content').html(content);
				});
			}
		});
		return false;
	});
	
	$(document).on('click', '.modal_close', function(){
		$(this).parent('#popup_wrapper').children('box-content').html('');
		$('#store_modal').removeAttr('style');
		$('#store_modal_loader').removeAttr('style');
	});
	
	$(document).on('click', '.addprod_close_btn', function(){
		$('#setopacity').val('');
		$('#addtoval').val('');
		$('#cid').val('');
		$('#scid').val('');
		$('#ctype').val('');
		$('#reload_content').val('');
	});
	
	$(document).on('click', '.add_store', function(){
		if(typeof store.get('user_id') !== 'undefined'){
			$('.create_store .box-content').html('<div id="dialog-box"><div style="width: 100%; text-align: center; margin: 80px 0;"><img class="img-loader" src="' + cloud_url + 'images/dialog-ajax-loader.gif" /></div></div>');

			var form_data = {};
			form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
			form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
			form_data.store_id = store.get('store_id');
			
			$.post(root + "store-type-form.html", form_data,function(data){
				$('.create_store .box-content').html(data);
			});
			return false;
			
		}else{
			close_modal($('.create_store'));
			csConfirm( 'Oops!', 
				'<div id="success-message-box">\
				 <p>Your session has expired, please login again.</p>\
				 </div>\
				',
				{Ok: null}, null, null,
				{ width:520, position:['center',120] }
			);
			$('.ui-dialog-titlebar').before('<div id="modalbox-colors-logo"><div class="one"></div><div class="two"></div><div class="three"></div><div class="four"></div><div class="five"></div></div>');
			
		}
	});
	
	
	$("div[rel*=store_loader]").CSModalLoader({ top : 290});
	$(document.body).delegate('#store_list', 'change', function(){
		$('.loading_image').trigger('click');
		var form_datas = {};
		form_datas.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
		form_datas.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
		form_datas.store_id = $(this).val();
		store.set('store_id', $(this).val());
		$.post(root + "view-store-content.html", form_datas,function(content){
			$('#store-content').html(content);
			close_modal($('.store_loader'));
			$('#store_modal_loader').removeAttr('style');
		});
	});
	
	$(document.body).on('click', '.is_subcat', function(){
		if($(this).is(':checked')){
			$('.subcat_enabled').val(1);
			$('.cat-parent').removeClass('hide');
		}else{
			$('.subcat_enabled').val(0);
			$('.cat-parent').addClass('hide');
		}
	});
	
	$(document.body).on('focus', '#category_name', function(){
		if($(this).val() == "Example: T-Shirt, Shoes & Footwear, Jeans, etc."){
			$(this).val('');
			$(this).css({'font-style':'normal', 'color':'#666666'});
		}
		
	});
	
	$(document.body).on('blur', '#category_name', function(){
		if($(this).val() == ""){
			$(this).val('Example: T-Shirt, Shoes & Footwear, Jeans, etc.');
			$(this).css({'font-style':'italic', 'color':'#D6D6D6'});
		}
		
	});

	$(document).on('click', '.cancel_btn', function(){
		close_modal($('.confirm_addcategory_wrapper'));
		$('#store_modal_level4').removeAttr('style');
		$('#store_modal').css({"display":"block","opacity":"0.5"});
	});
	
	$(document).on('click', '.product_edit', function(e){
		e.stopImmediatePropagation();
		productLoadStatus = 2;
		$('#reload_content').val($(this).attr('ctype'));
		var cat_id = $(this).attr('cid');
		load_productentry({product_id:$(this).attr('id'),category_id:cat_id});
	});
	
	$('.addproducts_wrapper').find('.standard-products').click(function(e) {
		e.stopImmediatePropagation();
		var cat_id = $('#cid').val();
		close_modal($('.addproducts_wrapper'));
		$('#store_modal_level2').removeAttr('style');
		load_productentry({category_id:cat_id});
		$('#cid').val('');
	});
	
	$(document).on('click', '#add-shop-product', function(e){
		e.stopImmediatePropagation();
		
		if(parseInt($(this).attr('total_products')) >= parseInt($(this).attr('max_products'))) {
			switch(parseInt($(this).attr('premium_type'))) {
				default:
				case 0:	plan = "Starter"; break;
				case 1:	plan = "Basic"; break;
				case 2:	plan = "Premium"; break;
				case 3:	plan = "Professional"; break;
			}
			
			$('.status_info_wrapper').show();
			$('.productentry-wrapper').css({width:600});
			$('.productentry-wrapper .productentry').css({width:600,height:200,'background-color':'#FFFFFF'});
			$('.productentry-wrapper  .box-content').html('<h1 style="text-align: center; font-size: 18px; font-weight: bold; margin-top: 78px;">\
																You have exceeded '+parseInt($(this).attr('max_products'))+' products limit for your '+plan+' Plan.<br>\
																You want to add more prodcuts? <a href="#" class="link-ugrade-subscription">Upgrade your account</a>.\
																</h1>');
			$('.store_productentry').trigger('click');
			$('.productentry-wrapper').css({position:'fixed',top:100});
			$('.status_info_wrapper').hide();
			$('.productentry .link-ugrade-subscription').click(function(e) {
				e.preventDefault();
				e.stopImmediatePropagation();
				upgrade_subscription_plan();	
			});
			return;
		}
		else {
			$('.productentry-wrapper').css({width:892});
			$('.productentry-wrapper .productentry').css({width:892,height:484});
		}
		
		if($(this).hasClass('add-shop-product')){
			load_productentry({category_id:''});
			return false;
		}
		var form_data = {};
		form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
		form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
		form_data.store_id = store.get('store_id');
		$('.status_info_wrapper').show();
		$.post(root + 'check-allowed-products.html', form_data,function(data){
			var _data = $.parseJSON(data);
			if(_data.status == 'exceed'){
				alert(_data.msg);
			}else{
				productLoadStatus = 2;
				loadShopCatalog = 1;
				load_productentry({category_id:''});
			}
			$('.status_info_wrapper').hide();
		});
	});
	
	$(document).on('click', '.view_my_products', function(){
		$('#reload_content').val('prod');
		$('#show_store_product').val($(this).attr('show_store_product'));
	});
	
	$(document).on('click', '.unp', function(){
		productLoadStatus = 2;
		var unp_val = $(this).attr('unp_val');
		if(unp_val == ""){
			$('.addprodhead').html('<strong>ADD Products</strong>');
		}
		if(unp_val != ""){
			$('.addprodhead').html('<strong>ADD Products to</strong> <span class="italic">'+unp_val+'</span>');
		}
	});
	
	$(document).on('click', '.paging', function(){
		return false;
	});
	
	//temporary
	$(document).on('click', '#store-wrapper .livepreview-wrapper .lbltxt:eq(0)', function(e) {
		e.stopImmediatePropagation();
		
		var data = {token:store.get('token'),user_id:store.get('user_id')};
		$('.add_vendor').trigger('click');
		$.ajax({
			type: "POST",
			url: '/create-vendor.html',
			data: data,
			success: function(data){
				var _data = $.parseJSON(data);
				$('.createvendor-wrapper .box-content').html(data);
			}
		});
	});
	
/* begin version 5 script */
	$(document).on('click', '.vendor-products-menu-wrapper', function(event){
		event.stopImmediatePropagation();
		if(productLoadStatus <= 0){
			reloadProductListing();
			return false;
		}
	});
	
	$(document).on('click', '.promoter-products-menu-wrapper', function(event){
		event.stopImmediatePropagation();
		if(productLoadStatus <= 0){
			reloadProductListing();
			return false;
		}
	});
	
	$(document).on('click', '.vendor-styx-product-menu-wrapper', function(event){
		event.stopImmediatePropagation();
		if(productLoadStatus <= 0){
			reloadProductListing({styx_id:store.get('styx_id'), page_link: root + 'styx-product-listing.html'});
			return false;
		}
	});
	
	$(document).on('click', '.shop-category-menu li', function(event){
		event.stopImmediatePropagation();
		var element = $('.shop-category-menu').jScrollPane({});
		var api = element.data('jsp');
		api.destroy();
		
		var category_level = '';
		$('.shop-category-menu li').removeClass('selected');
		$(this).addClass('selected');
		$('.shop-category-menu li label').removeClass('active');
		$(this).children('label').addClass('active');
		if($(this).hasClass('main') && typeof $(this).find('ul').css('display') === 'undefined'){
			$('.shop-category-menu li ul').slideUp(20);
		}
		if(! $(this).hasClass('sub') && ($(this).hasClass('main') && $(this).find('ul').css('display') == 'none')){
			$('.shop-category-menu li ul').slideUp(20);
			$(this).find('ul').slideDown(20);
		}
		if($(this).hasClass('main')){ category_level = 'main'; }
		if($(this).hasClass('sub')){ category_level = 'sub'; }
		reloadProductListing({category_level:category_level,category_id:$(this).attr('data'),publish_type:$('#filter_product_display').val(),products_only:true});
	});
	
	$(document).on('change', '#promoter_cat_listing', function(event){
		event.stopImmediatePropagation();
		reloadProductListing({user_type:'promoter',category_id:$(this).val(),publish_type:$('#promoter_filter_product_display').val()});
	});
	
	$(document).on('change', '#filter_product_display', function(event){
		event.stopImmediatePropagation();
		reloadProductListing({publish_type:$(this).val(),category_id:$('.shop-category-menu li.selected').attr('data'),products_only:true});
	});
	
	$(document).on('change', '#promoter_filter_product_display', function(event){
		event.stopImmediatePropagation();
		reloadProductListing({user_type:'promoter',publish_type:$(this).val(),category_id:$('#promoter_cat_listing').val()});
	});
	
	$(document).on('click', '#product-listing-wrapper .product_listing', function(event){
		event.stopImmediatePropagation();
		var page = $(this).attr('href');
		reloadProductListing({page_link:page,category_id:$('#cat_listing').val(),publish_type:$('#filter_product_display').val()});
		return false;
	});
	$(document).on('click', '.promoter_product_listing', function(event){
		event.stopImmediatePropagation();
		var page = $(this).attr('href');
		
		if($('#store-menu-wrapper').hasClass('promoter-wrapper')){ 
			reloadProductListing({page_link:page, category_id:$('.shop-category-menu li.selected').attr('data'), publish_type:$('#filter_product_display').val(),products_only:true}); 
		}else{
			reloadProductListing({user_type:'promoter',page_link:page,category_id:$('#promoter_cat_listing').val(),publish_type:$('#promoter_filter_product_display').val()});
		}
		return false;
	});
	$(document).on('click', '.manage-link', function(event){
		event.stopImmediatePropagation();
		$('.menu3').trigger('click');
	});
	
	$(document).on('click', '.promote_product_lists', function(event){
		event.stopImmediatePropagation();
		var page = $(this).attr('href');
		loadPromoteProductLists({page_link:page});
		return false;
	});
	
	$(document).on('click', '.search_promote_product_lists', function(event){
		event.stopImmediatePropagation();
		var page = $(this).attr('href');
		loadPromoteProductLists({search_text:store.get('entire_search_text'),page_link:page});
		return false;
	});
	
	$(document).on('change', '#select-occassions', function(event){
		event.stopImmediatePropagation();
		var occassions = $(this).val();
		loadPromoteProductLists({occassions:occassions,recipients:$('#select-recipients').val()});
		return false;
	});
	
	$(document).on('change', '#select-recipients', function(event){
		event.stopImmediatePropagation();
		var recipients = $(this).val();
		loadPromoteProductLists({recipients:recipients,occassions:$('#select-occassions').val()});
		return false;
	});
	
	/* begin more shop products */
	$(document).on('click', '#more-shop-products', function(event){
		event.stopImmediatePropagation();
		$('.more_shop_products').trigger('click');
		loadSelectMoreProducts();
		return false;
	});
	
	$(document).on('change', '#more-shop-catalog-wrapper #choose-occassions', function(event){
		event.stopImmediatePropagation();
		var occassions = $(this).val();
		loadSelectMoreProducts({department_level:$('.department-left span.selected').parent('li').attr('class'),department_id:$('.department-left span.selected').parent('li').attr('val'), occassions:occassions,recipients:$('#choose-recipients').val(),product_sorting:$('#product-sorting').val(),load:'products'});
		return false;
	});
	
	$(document).on('change', '#more-shop-catalog-wrapper #choose-recipients', function(event){
		event.stopImmediatePropagation();
		var recipients = $(this).val();
		loadSelectMoreProducts({department_level:$('.department-left span.selected').parent('li').attr('class'),department_id:$('.department-left span.selected').parent('li').attr('val'), recipients:recipients,occassions:$('#choose-occassions').val(),product_sorting:$('#product-sorting').val(),load:'products'});
		return false;
	});
	
	$(document).on('change', '#more-shop-catalog-wrapper #product-sorting', function(event){
		event.stopImmediatePropagation();
		var product_sorting = $(this).val();
		if($('.search-shop-product-box').hasClass('for-search-page')){
			loadSelectMoreProducts({product_sorting:product_sorting,search_text:store.get('search_text')});
		}else{
			loadSelectMoreProducts({department_level:$('.department-left span.selected').parent('li').attr('class'),department_id:$('.department-left span.selected').parent('li').attr('val'), product_sorting:product_sorting,occassions:$('#choose-occassions').val(),recipients:$('#choose-recipients').val(),load:'products'});
		}
		return false;
	});
	
	$(document).on('click', '#more-shop-catalog-wrapper .load_shop_product_list', function(event){
		event.stopImmediatePropagation();
		var page = $(this).attr('href');
		loadSelectMoreProducts({department_level:$('.department-left span.selected').parent('li').attr('class'),department_id:$('.department-left span.selected').parent('li').attr('val'), page_link:page,product_sorting:$('#product-sorting').val(),occassions:$('#choose-occassions').val(),recipients:$('#choose-recipients').val(),load:'products'});
		return false;
	});
	
	$(document).on('click', '#more-shop-catalog-wrapper .load_shop_product_search_list', function(event){
		event.stopImmediatePropagation();
		var page = $(this).attr('href');
		loadSelectMoreProducts({page_link:page, product_sorting:$('#product-sorting').val(), search_text:store.get('search_text')});
		return false;
	});
	
	$(document).on('focus', '#more-shop-catalog-wrapper #seach-online-products', function(event){
		event.stopImmediatePropagation();
		if($(this).val() == "Search entire shopstyx"){
			$(this).val('');
		}
	});
	
	$(document).on('blur', '#more-shop-catalog-wrapper #seach-online-products', function(event){
		event.stopImmediatePropagation();
		if($(this).val() == ""){
			$(this).val('Search entire shopstyx');
		}
	});
	
	$(document).on('keypress', '#more-shop-catalog-wrapper #seach-online-products', function(e){
		e.stopImmediatePropagation();
		if (e.which == 13) {
			$(this).removeClass('input_error');
			var search_text = trimText($(this).val());
			
			if(search_text == ""){
				$(this).addClass('input_error');
				return false;
			}
			
			store.set('search_text', search_text);
			loadSelectMoreProducts({search_text:search_text});
			return false;
		}
	});
	
	$(document).on('click', '#more-shop-catalog-wrapper #back-to-product-list', function(event){
		event.stopImmediatePropagation();
		loadSelectMoreProducts();
	});
	
	$(document).on('click', '#more-shop-catalog-wrapper .department-left ul li.main span.main-department-menu', function(event){
		event.stopImmediatePropagation();
		var element = $('.shop-department-list-wrapper').jScrollPane({});
		var api = element.data('jsp');
		api.destroy();

		var slide = false;
		if($(this).hasClass('selected')){ return false; }
		if($(this).parents('.main').find('.sub-department-menu').hasClass('selected')){ slide = true; }
		$('.department-left ul li span').removeClass('selected');
		$(this).addClass('selected');
		loadSelectMoreProducts({department_level:'main',department_id:$(this).parents('.main').attr('val'), load:'products'});
		if(slide){ return false; }
		$('.department-left ul li.main ul').slideUp(50);
		$(this).parent('li').find('ul').slideDown(100);
	});
	
	$(document).on('click', '#more-shop-catalog-wrapper .department-left ul li ul li span', function(event){
		event.stopImmediatePropagation();
		var element = $('.shop-department-list-wrapper').jScrollPane({});
		var api = element.data('jsp');
		api.destroy();
		$('.department-left ul li span').removeClass('selected');
		$(this).addClass('selected');
		loadSelectMoreProducts({department_level:'sub',department_id:$(this).parents('.sub').attr('val'), load:'products'});
	});
	
	$(document).on('click', '#more-shop-catalog-wrapper #add-more-departments', function(event){
		event.stopImmediatePropagation();
		loadPromoterDepartments();
	});
	
	$(document).on('click', '#done-departments-btn', function(event){
		event.stopImmediatePropagation();
		if(! $('.department-list-wrapper ul li').hasClass('selected')){
			alert('Please pick at least 1 shopstyx department.');
			return false;
		}
		$('.your-store').trigger('click');
	});
	
	/* end more shop products */
	
	/* begin shop departments */
	
	$(document).on('click', '.promoter-departments .department-list-wrapper ul li', function(event){
		event.stopImmediatePropagation();
		if(parseInt($(this).attr('subtotal')) > 0){
			$('.promoter_sub_departments').trigger('click');
			loadPromoterSubDepartments({parent_id:$(this).attr('val'),department_name:$(this).find('.department_name').html()});
		}else{;
			var dpt_id = $(this).attr('val');
			var status = '';
			if($(this).hasClass('selected')){
				$(this).removeClass('selected');
				status = 'not selected';
			}else{
				$(this).addClass('selected');
				status = 'selected';
			}
			
			var form_data = {};
			form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
			form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
			form_data.department_id = $(this).attr('val');
			form_data.status = status;
			$.post(root + "save-departments.html", form_data,function(data){});
		}
	});
	
	$(document).on('click', '.promoter_sub_departments_wrapper .sub-department-list-wrapper ul li', function(event){
		event.stopImmediatePropagation();
		var subdpt_id = $(this).attr('val');
		if($(this).hasClass('selected')){
			$(this).removeClass('selected');
			$('.subdpt' + subdpt_id).remove();
		}else{
			$(this).addClass('selected');
			$('#sub-department-storage').append('<input type="hidden" class="subdpt'+ subdpt_id +'" name="sub_department_id[]" value="'+ subdpt_id +'" />');
		}
	});
	
	$(document).on('click', '.promoter_sub_departments_wrapper #sub-department-cancel', function(event){
		event.stopImmediatePropagation();
		$('.sub-department-close-btn').trigger('click');
	});
	
	$(document).on('click', '.promoter_sub_departments_wrapper #submit-departments', function(event){
		event.stopImmediatePropagation();
		if(! $('.department-list-wrapper ul li').hasClass('selected')){
			alert('Please pick at least 1 shopstyx department.');
			return false;
		}
		loadShopProducts();
	});
	
	$(document).on('click', '.promoter_sub_departments_wrapper #sub-department-save', function(event){
		event.stopImmediatePropagation();
		var form_data = {};
		var page; var wrapper; var loader;
		var page = root + "save-departments.html";
		var loader = $('.sub_departments_wrapper #popup-preloader-wrapper');
		loader.show();
		form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
		form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
		form_data.sub_department_id = $( 'input[name="sub_department_id[]"]' ).map(function() { return $(this).val() }).get().join(",");
		form_data.parent_department_id = $( '#department-parent-id' ).val();
		$.post(page, form_data,function(data){
			if(jQuery.trim($('#sub-department-storage').html()) == ""){
				$('#main_dpt'+$('#department-parent-id').val()).removeClass('selected');
			}
			if(jQuery.trim($('#sub-department-storage').html()) != ""){
				$('#main_dpt'+$('#department-parent-id').val()).addClass('selected');
			}
			loader.hide();
			$('.sub-department-close-btn').trigger('click');
		});
	});
	
	/* end shop departments */
	
	/* begin product details popup */
	$(document).on('click', '#product-info', function(event){
			event.stopImmediatePropagation();
			if($(this).hasClass('promoter-page')){
				$('.product_details').css({'margin-top':'280px'});
			}
			$('.view_pdetails').trigger('click');
			$('.product_details #popup-preloader-wrapper').show();
			var url = root + 'load-product-details.html';
			var form_data = {};
			form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
			form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
			form_data.store_id = typeof store.get('store_id') !== "undefined" ? store.get('store_id') : 0;
			var settings = $.parseJSON($(this).attr('data'));
			$.extend(form_data,settings);
			if(typeof form_data.view_as !== 'undefined'){
				form_data.store_id = typeof store.get('styx_id') !== "undefined" ? store.get('styx_id') : 0;
			}
			$.post(url, form_data,function(data){
				$('.product_details .box-content').html(data);
				$('.product_details #popup-preloader-wrapper').hide();
			});
			return false;
	});
	/* end product details popup */
	/* begin feature product */
	$(document.body).on('click', '#feature-product', function(event){
		event.stopImmediatePropagation();
		$('.sp-product-main-wrapper #popup-preloader-wrapper').show();
		var $this = $(this);
		var form_data = {};
		form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
		form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";

		if($(this).hasClass('featured-shop-product-active')){
			$(this).removeClass('featured-shop-product-active');
			form_data.featured = 0;
		}else{
			$(this).addClass('featured-shop-product-active');
			form_data.featured = 1;
		}

		var featured_val = $.parseJSON($(this).val());
		$.extend(form_data,featured_val);
		
		$.post(root + "feature-product.html", form_data,function(data){
			homepageLoadStatus = 0;
			if(featured_val.user_type == "promoter"){
				if($this.hasClass('featured-onsearch')){
					publish_type = $('#filter_product_display-onsearch').val();
				}else{
					publish_type = $('#filter_product_display').val();
				}
				reloadProductListing({user_type:'promoter',publish_type:publish_type,category_id:$('.shop-category-menu li.selected').attr('data'),products_only:true});
			}else{
				reloadProductListing({publish_type:$('#filter_product_display').val(),category_id:$('.shop-category-menu li.selected').attr('data'),products_only:true});
				productSearching({search_text:store.get('search_text'),publish_type:$('#filter_product_display-onsearch').val()});
			}
			if(data == 'slideshow'){
				reloadFrame();
			}
			$('.sp-product-main-wrapper #popup-preloader-wrapper').hide();
		});
		return false;
		
	});
	
	/* end feature product */
	
	/* begin transfer product */
	$(document).on('click', '.transfer, #transfer-product', function(event){
		event.stopImmediatePropagation();
		$('.transfer_product').trigger('click');
		$('.transfer_category #popup-preloader-wrapper').show();
		var url = root + 'transfer-product.html';
		var form_data = {};
		form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
		form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
		var transfer_val = $.parseJSON($(this).val());
		$.extend(form_data,transfer_val);
		
		$.post(url, form_data,function(data){
			$('.transfer_category .box-content').html(data);
			$('.transfer_category #popup-preloader-wrapper').hide();
		});
		return false;
	});
	
	$(document).on('click', '.transferproductnow', function(event){
		event.stopImmediatePropagation();
		$(this).parents("form").trigger("submit");
		return false;
	});
	
	$(document).on('submit', '#transferProduct', function(event){
		event.preventDefault();
		event.stopImmediatePropagation();
		$('.transfer_category #popup-preloader-wrapper').show();
		
		var error_status = 0;
		$('.new_category_id').removeClass('input_error');
		if($('.new_category_id').val() == 0){
			$('.category_parent_name').addClass('input_error');
			error_status++;
		}
		if(error_status > 0){ $('.transfer_category #popup-preloader-wrapper').hide(); return false; }
		
		$form = $(this);
		url = $form.attr( 'action' );
		var form_data = {};
		form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
		form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
		form_data.new_category_id = trimText($form.find( 'select[name="new_category_id"]' ).val());
		var product_val = $.parseJSON($form.find( 'input[name="product_val"]' ).val());
		$.extend(form_data,product_val);
		
		$('.transferproductnow').attr("disabled","disabled");
		
		$.post(url, form_data,function(data){
			close_modal($('.transfer_category'));
			$('#store_modal_level2').removeAttr('style');
			if($('#create_shop_step').val() == 3){
				loadShopCatalog();
				return false;
			}
			$('.transfer_category #popup-preloader-wrapper').hide();
			categoriesLoadStatus = 0;
			reloadProductListing({publish_type:$('#filter_product_display').val(),category_id:$('.shop-category-menu li.selected').attr('data'),products_only:true});
			productSearching({search_text:store.get('search_text'),publish_type:$('#filter_product_display-onsearch').val()});
			reloadFrame();
			return false;
		});
		return false;
	});
	
	/* end transfer product */
	
	/* start uncategorize */
	$(document).on('click', '#create_uncategorize', function(event){
		event.stopImmediatePropagation();
		$('.subcat_enabled').val(2);
		close_modal($('.confirm_addcategory_wrapper'));
		$('#modal_close_level4').removeAttr('style');
	
	});
	/* end uncategorize */
	
	/* begin existing products */
	$(document).on('click', '.exist-products', function(event){
		event.stopImmediatePropagation();
		loadExistingProduct();
		close_modal($('.addproducts_wrapper'));
		$('#store_modal_level2').removeAttr('style');
	});
	
	/* end existing products */
	
	/* begin search my products */
	
	$(document).on('focus', '.search-user-products', function(event){
		event.stopImmediatePropagation();
		if($(this).val() == "Search my products"){
			$(this).val('');
		}
	});
	
	$(document).on('blur', '.search-user-products', function(event){
		event.stopImmediatePropagation();
		if($(this).val() == ""){
			$(this).val('Search my products');
		}
	});
	
	$(document).on('focus', '.search-entire-catalog', function(event){
		event.stopImmediatePropagation();
		if($(this).val() == "Search entire shopstyx catalog"){
			$(this).val('');
		}
	});
	
	$(document).on('blur', '.search-entire-catalog', function(event){
		event.stopImmediatePropagation();
		if($(this).val() == ""){
			$(this).val('Search entire shopstyx catalog');
		}
	});
	
	$(document).on('keypress', '.search-entire-catalog', function(e){
		e.stopImmediatePropagation();
		if (e.which == 13) {
			$(this).removeClass('input_error');
			var search_text = trimText($(this).val());
			
			if(search_text == ""){
				$(this).addClass('input_error');
				return false;
			}
			
			store.set('entire_search_text', search_text);
			$('#search_box').trigger('click');
			loadPromoteProductLists({search_text:search_text});
			return false;
		}
	});
	
	$(document).on('keypress', '.search-user-products', function(e){
		e.stopImmediatePropagation();
		if (e.which == 13) {
			$(this).removeClass('input_error');
			var search_text = trimText($(this).val());
			
			if(search_text == ""){
				$(this).addClass('input_error');
				return false;
			}
			
			store.set('search_text', search_text);
			$('#search_box').trigger('click');
			productSearching({search_text:search_text});
			return false;
		}
	});
	
	$(document).on('click', '.promote-products-btn, .promote-products-button', function(e){
		e.stopImmediatePropagation();
		$('#search_box').trigger('click');
		loadPromoteProductLists();
		return false;
	});
	
	$(document).on('click', '.product_searching', function(event){
		event.stopImmediatePropagation();
		var page = $(this).attr('href');
		productSearching({search_text:store.get('search_text'), page_link:page});
		return false;
	});
	
	$(document).on('change', '#category_list_on_search', function(event){
		event.stopImmediatePropagation();
		productSearching({search_text:store.get('search_text'),category_id:$(this).val(),publish_type:$('#filter_product_display-onsearch').val()});
	});
	
	$(document).on('change', '#filter_product_display-onsearch', function(event){
		event.stopImmediatePropagation();
		productSearching({search_text:store.get('search_text'),publish_type:$(this).val()});
	});
	
	/* end search my products */
	
	/* begin category listing */
	$(document).on('click', '.show_categories', function(event){
		event.stopImmediatePropagation();
		reloadCategoryListing();
		return false;
	});
	/* end category listing */
	
	/* begin subcategory listing */
	$(document).on('click', '.show_sub_categories', function(event){
		event.stopImmediatePropagation();
		reloadSubCategoryListing({order_id:$(this).attr('id'),category_name:$(this).attr('catname')});
		return false;
	});
	/* end subcategory listing */
	
	/* begin add edit category */
	
	$(document).on('click', '.add-new-catbox', function(event){
		event.stopImmediatePropagation();
		$('.addnewcategory #popup-preloader-wrapper').show();
		$('.store_addcategory').trigger('click');
		
		var url = root + 'add-category-form.html';
		var form_data = {};
		form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
		form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
		form_data.store_id = typeof store.get('store_id') !== "undefined" ? store.get('store_id') : 1;
		form_data.cattype = $(this).attr('cattype');
		form_data.order_id = $(this).attr('poid');
		form_data.main_category = true;
		form_data.reload = '';
		if($(this).hasClass('from-add-standard-products')){
			form_data.reload = 'product category list';
		}
		$.post(url, form_data,function(data){
			$('.addnewcategory .box-content').html(data);
			$('.addnewcategory #popup-preloader-wrapper').hide();
		});
		return false;
	});
	
	$(document).on('click', '.edit-catbox', function(event){
		event.stopImmediatePropagation();
		$('.editcategory #popup-preloader-wrapper').show();
		$('.store_editcategory').trigger('click');
		
		var url = root + 'edit-category-form.html';
		var form_data = {};
		form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
		form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
		form_data.store_id = store.get('store_id');
		form_data.order_id = $(this).attr('id');
		form_data.parent_order_id = $(this).attr('parent');
		form_data.cattype = $(this).attr('cattype');
		
		$.post(url, form_data,function(data){
			$('.editcategory .box-content').html(data);
			$('.editcategory #popup-preloader-wrapper').hide();
		});
		return false;
	});
	
	$(document).on('click', '.add_edit_category', function(event){
		event.stopImmediatePropagation();
        $(this).parents("form").trigger("submit");
        return false;
    });
	
	$('.add_edit_category_btn_one, .add_edit_category_btn_two').one('click', function(event){
		event.preventDefault();
		event.stopImmediatePropagation();
		$(this).parents("form").trigger("submit");
        return false;
	});
	
	$('.edit_category_btn_one, .edit_category_btn_two').one('click', function(event){
		event.preventDefault();
		event.stopImmediatePropagation();
		$(this).parents("form").trigger("submit");
        return false;
	});
	

	$(document).on('submit', '#addeditcategoryform', function(event){
		event.preventDefault();
		event.stopImmediatePropagation();
		var options = {
			target:        '#display_message',
			success:       showResponse
		};
		
		$form = $(this);
		$('.loader').remove();
		$('.ajaxloader').html('<img class="loader" src="'+cloud_url+'images/ajax-loader.gif" />');
		
		var error_status = 0;
		$('#category_name').removeClass('input_error');
		$('.category_parent_name2').removeClass('input_error');
		
		var form_data = {};
		form_data.category_name = $form.find( 'input[name="category_name"]' ).val();
		if(trimText(form_data.category_name) == "" || form_data.category_name == "Example: T-Shirt, Shoes & Footwear, Jeans, etc."){
			$('#category_name').addClass('input_error');
			error_status++;
		}
		
		if($('.subcat_enabled').val() == 1){
			if($('.category_parent_name2').val() == 0){
				$('.category_parent_name2').addClass('input_error');
				error_status++;
			}
		}
		
		if(error_status > 0){ $('.loader').remove(); return false; }
		
		if($('.subcat_enabled').val() == 1){
			var main_cat = $('.category_parent_name2').val();
			if(main_cat > 0){
				if($('#cat_id_' + main_cat).val() > 0){
					$('.confirm_addcategory').trigger('click');
					$('.loader').remove();
					return false;
				}
			}
		}
		
		$('.add_edit_category_btn_one, .add_edit_category_btn_two').attr("disabled","disabled");
		
        $(this).ajaxSubmit(options); 
        return false; 
    });
	
	$(document).on('click', '#add_choose', function(event){
		event.stopImmediatePropagation();
		$("#add_myfile").click();
		return false;
	});
	
	$(document).on('change', '#add_myfile', function(event){
		event.stopImmediatePropagation();
		$(".filename").html($(this)[0].files[0].name);
		$(".file_exist").val(1);
		$("#add_choose").html('Change Category Picture');
		$(this).parents('.store-addcat-img').removeAttr('style');
	});
	
	$(document).on('click', '#edit_choose', function(event){
		event.stopImmediatePropagation();
		$("#edit_myfile").click();
		return false;
	});
	
	$(document).on('change', '#edit_myfile', function(event){
		event.stopImmediatePropagation();
		$(".filename").html($(this)[0].files[0].name);
		$(".file_exist").val(1);
		$("#edit_choose").html('Change Category Picture');
		$(this).parents('.store-addcat-img').removeAttr('style');
	});
	
	/* end add edit category */
	
	/* begin category delete */
	
	$(document).on('click', '.catdelete', function(event){
		event.stopImmediatePropagation();
		var $this = $(this);
		var category_name = $(this).parents('.ui-enabled').find('.cat-name').html();
		if($('#create_shop_step').val() == 3){
			var category_name = $(this).parents('.shop-category').find('.shop-category-title').html();
		}
		if($(this).hasClass('cat-delete')){
			var category_name = $(this).parents('li').find('.cat-title').html();
		}
		if (! confirm("Are you sure you want to delete " + category_name + " ?")) {
			return false;
		}
		if( $(this).parents('.shop-category').find('#stp').val() > 0 ){
			alert('Delete first all product on "' + category_name + '" category.');
			return false;
		}
		$('#category-listing-wrapper #popup-preloader-wrapper').show();
		var form_datas = {};
		form_datas.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
		form_datas.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
		form_datas.store_id = typeof store.get('store_id') !== "undefined" ? store.get('store_id') : 0;
		form_datas.order_id = $(this).attr('id');
		form_datas.cattype = $(this).attr('cattype');
		form_datas.parent_order_id = $(this).attr('poid');
		var parent_order_id = $(this).attr('poid');
		productLoadStatus = 1;
		$.post(root + "delete-category.html", form_datas,function(datas){
			var _datas = $.parseJSON(datas);
			if( _datas.status == "has products"){
				alert(_datas.msg);
				$('#category-listing-wrapper #popup-preloader-wrapper').hide();
				return false;
			}
			if( _datas.status == "failed"){
				alert('Delete first the sub category before deleting the main category...');
				$('#category-listing-wrapper #popup-preloader-wrapper').hide();
				return false;
			}
			if($('#create_shop_step').val() == 3){
				if( typeof loadShopCatalog == 'function' ){
					loadShopCatalog();
					$('#store_modal_level6').removeAttr('style');
					return false;
				}
			}
			if($this.hasClass('delete-this-category')){
				loadPromoteProductForm({"product_id":$('#ur_product_id').val(),"product_user_id":$('#ur_product_user_id').val()});
			}
			if( _datas.result == 'category' ){
				reloadCategoryListing();
			}
			if( _datas.result == 'subcategory' ){
				reloadSubCategoryListing({order_id:parent_order_id});
			}
			reloadProductListing();
			reloadFrame();
			$('#category-listing-wrapper #popup-preloader-wrapper').hide();
			$('#product-listing-wrapper #popup-preloader-wrapper').hide();
			return false;
		});
	});
	
	/* end category delete */
	
	/* begin product delete */
	
	$(document).on('click', '.proddelete', function(event){
		event.stopImmediatePropagation();
		var category_name = $(this).parents('.shop-product-wrapper').find('.sptitle').html();
		if($(this).attr('ids') == "search_popup"){
			var category_name = $(this).parents('.box-top-right').find('h1').html();
		}
		if (! confirm("Are you sure you want to delete " + category_name + " ?")) {
			return false;
		}
		$('#product-listing-wrapper #popup-preloader-wrapper').show();
		var form_datas = {};
		form_datas.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
		form_datas.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
		form_datas.store_id = typeof store.get('store_id') !== "undefined" ? store.get('store_id') : 0;
		form_datas.store_category_id = $(this).attr('cid');
		form_datas.product_id = $(this).attr('pid');
		form_datas.order_id = $(this).attr('cid');
		form_datas.category_name = $(this).attr('cname');
		$.post(root + "remove-category-product.html", form_datas,function(datas){
			if($('#create_shop_step').val() == 3){
				loadShopCatalog();
				return false;
			}
			reloadProductListing({publish_type:$('#filter_product_display').val(),category_id:$('.shop-category-menu li.selected').attr('data'),products_only:true});
			productSearching({search_text:store.get('search_text'),publish_type:$('#filter_product_display-onsearch').val()});
			close_modal($('.product_details'));
			$('#store_modal_level4').removeAttr('style');
			reloadFrame();
			return false;
		});
	});
	
	/* end product delete */
	
	/* begin add to store */
	$(document.body).delegate('.p_promote_product', 'click', function(event){
		event.stopImmediatePropagation();
		var product_data = $.parseJSON($(this).val());
		$('.category_box').trigger('click');
		loadPromoteProductForm(product_data)
		return false;
	});
	
	$(document.body).delegate('.remove-btn, .remove-button', 'click', function(event){
		event.stopImmediatePropagation();
		if (! confirm("Are you sure you want to remove " + $(this).parents('.shop-product-wrapper').find('.sptitle').html() + " ?")) {
			return false;
		}
		
		var $this = $(this);
		var form_data = {};
			form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
			form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
			form_data.store_id = typeof store.get('store_id') !== "undefined" ? store.get('store_id') : 0;
			form_data.ref_user_id = form_data.user_id;
			var product_data = $.parseJSON($(this).val());
			$.extend(form_data,product_data);

			var url = root + 'remove-promote-product.html';
			$.post(url, form_data,function(data){
				if(typeof $('#from_dialog').val() !== "undefined"){ reloadProductListing(); }
				if($this.hasClass('forpromoter')){ reloadProductListing({category_id:$('.shop-category-menu li.selected').attr('data'), publish_type:$('#filter_product_display').val(),products_only:true}); }
				categoriesLoadStatus = 0;
				$($this).addClass('plus-btn');
				$($this).html('+');
				$($this).addClass('p_promote_product');
				$($this).removeClass('remove-button');
				reloadProductListing({category_id:$('.shop-category-menu li.selected').attr('data'),publish_type:$('#filter_product_display').val(), products_only:true});
				reloadFrame();
				return false;
			});
	});
	
	$(document.body).delegate('.prod-remove', 'click', function(event){
		event.stopImmediatePropagation();
		if (! confirm("Are you sure you want to delete " + $(this).parents('.regular-product').find('.iproduct-title').html() + " ?")) {
			return false;
		}
		var $this = $(this);
		var form_data = {};
			form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
			form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
			form_data.store_id = typeof store.get('store_id') !== "undefined" ? store.get('store_id') : 0;
			form_data.ref_user_id = form_data.user_id;
			var product_data = $.parseJSON($(this).val());
			$.extend(form_data,product_data);

			var url = root + 'remove-promote-product.html';
			$.post(url, form_data,function(data){
				reloadProductListing();
				categoriesLoadStatus = 0;
				return false;
			});
	});
	
	$(document).on('click', '.addtostore', function(event){
		event.stopImmediatePropagation();
		$(this).parents("form").trigger("submit");
		return false;
	});
	
	$(document).on('submit', '#addToStoreForm', function(event){
		event.preventDefault();
		event.stopImmediatePropagation();
		
		$form = $(this);
		$('.cat_required').html('');
		
		var error_status = 0;
		$('.new_category_id').removeClass('input_error');
		
		url = $form.attr( 'action' );
		var form_data = {};
		
		form_data.store_id = trimText($form.find( 'input[name="store_id"]' ).val());
		form_data.product_id = trimText($form.find( 'input[name="product_id"]' ).val());
		form_data.product_user_id = trimText($form.find( 'input[name="product_user_id"]' ).val());
		form_data.ref_user_id = trimText($form.find( 'input[name="ref_user_id"]' ).val());
		
		form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
		form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
		form_data.store_category_id = $form.find( 'input:checkbox[name=categories[]]:checked' ).val();
		form_data.categories = $( 'input:checkbox[name=categories[]]:checked' ).map(function() { return $(this).val() }).get().join(",");
		var upid = trimText($form.find( 'input[name="up_id"]' ).val());

		if(typeof form_data.store_category_id === "undefined"){
			$('.cat_required').html('Please select category');
			$('.loader').remove(); return false;
		}
		$('.addtostore').attr("disabled","disabled");
		$('.category_box_wrapper #popup-preloader-wrapper').show();
		$.post(url, form_data,function(data){
			if($('.shop_page').val() == 'shop page step'){
				if( typeof loadShopProducts == 'function' ){
					var page = root + 'load-shop-products.html?page=' + $('.page-numbers .pages .selected').html(),
						department_level = $('.department-left span.selected').parent('li').attr('class'),
						department_id = $('.department-left span.selected').parent('li').attr('val'),
						product_sorting = $('#product-sorting').val(),
						recipients = $('#choose-recipients').val(),
						occassions = $('#choose-occassions').val();
					loadShopProducts({page_link:page, product_sorting:product_sorting, department_level:department_level, department_id:department_id, recipients:recipients, occassions:occassions, load:'products'});
				}
			}
			if($('.shop_page').val() == 'shop search page step'){
				if( typeof loadShopProducts == 'function' ){
					var page = root + 'load-shop-products.html?page=' + $('.page-numbers .pages .selected').html();
					loadShopProducts({page_link:page, product_sorting:$('#product-sorting').val(), search_text:store.get('search_text')});
				}
			}
			categoriesLoadStatus = 0;
			$('.addtostore').removeAttr("disabled");
			$('.category_box_wrapper #popup-preloader-wrapper').hide();
			close_modal($('.category_box_wrapper'));
			$('#store_modal_level3').removeAttr('style');
			$('.' + upid +' .plus-btn').addClass('remove-button');
			$('.' + upid +' .plus-btn').html('Remove');
			$('.' + upid +' .plus-btn').removeClass('p_promote_product');
			$('.' + upid +' .remove-button').removeClass('plus-btn');
			reloadProductListing({category_id:$('.shop-category-menu li.selected').attr('data'),publish_type:$('#filter_product_display').val(), products_only:true});
			reloadFrame();
		});
		
		return false;
		
	});
	/* end add to store */
	
	/* begin store colors */
	$(document).on('click', '.store_menu', function(event){
		event.stopImmediatePropagation();
		if(storecolorsLoadStatus <= 0){
			reloadColors();
			return false;
		}
	});
	
	$(document).on('click', '.save_colors_btn', function(event){
		event.stopImmediatePropagation();
		url = root + 'save-store-colors.html';
		var form_data = {};
		form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
		form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
		form_data.store_id = store.get('store_id');
		form_data.color1 = $('#custom_color1').val();
		form_data.color2 = $('#custom_color2').val();
		form_data.body_background = $('#custom_body_background').val();
		
		if($('#mini-color-wrapper').hasClass('min-colors')){
			$('.pcolor1').minicolors('value', $('#custom_color1').val());
			$('.pcolor2').minicolors('value', $('#custom_color2').val());
			$('.pbody_background').minicolors('value', $('#custom_body_background').val());
		}
		
		$.post(url, form_data,function(data){
			reloadColors();
			reloadFrame();
		});
		
		return false;
	});
	
	$(document).on('change', '#select_saved_colors', function(event){
		event.stopImmediatePropagation();
		if($(this).val() != 0){
			var color_data = $.parseJSON($(this).val());
			$('.color1').minicolors('value', color_data.color1);
			$('.color2').minicolors('value', color_data.color2);
			$('.body_background').minicolors('value', color_data.body_background);
			$('.color_set').removeClass('preset-selected');
			save_colors({customize_colors_id:color_data.customize_colors_id});
			reloadFrame();
		}
	});
	
	$(document).on('click', '.preset-settings-box', function(event){
		event.stopImmediatePropagation();
		$('.preset-settings-box h3').removeClass('active');
		$('.preset-settings-box .preset-color-wrapper').addClass('hide');
		$(this).find('h3').addClass('active');
		$(this).find('.preset-color-wrapper').removeClass('hide');
	});
	
	$(document).on('click', '.color_set', function(event){
		event.stopImmediatePropagation();
		$('.color_set').removeClass('preset-selected');
		$(this).addClass('preset-selected');
		$("#select_saved_colors").find('option:selected').removeAttr("selected");
		$("div.saved_colors").html('Select from saved color');
		
		if($('#mini-color-wrapper').hasClass('min-colors')){
			$('.pcolor1').minicolors('value', $(this).find('.tcolor1').attr('color_val'));
			$('.pcolor2').minicolors('value', $(this).find('.tcolor2').attr('color_val'));
			$('.pbody_background').minicolors('value', $(this).find('.tcolor4').attr('color_val'));
		}
		
		save_colors({preset_colors_id:$(this).find('#color_id').val()});
		reloadFrame();
	});
	
	/* end store colors */
	
	/* begin theme  */
	
	$(document).on('click', '.templates li', function(event){
		event.stopImmediatePropagation();
		$('.templates li.active').append('<span class="magnify"></span>');
		$('.templates li').removeClass('active');
		$('.templates li').addClass('notactive');
		
		$(this).removeClass('notactive');
		$(this).addClass('active');
		$(this).find('.magnify').remove();
	});
	
	/* end theme */
	
	/* begin store logo  */
	
	$(document).on('click', '.store_logo_menu', function(event){
		event.stopImmediatePropagation();
		if(storelogoLoadStatus <= 0){
			reloadStoreLogo();
			return false;
		}
	});
	
	$(document).on('click', '.use_store_name', function(event){
		event.stopImmediatePropagation();
		$('#logo-option-wrapper').addClass('hide');
		$('#text-option-wrapper').removeClass('hide');
		$('.text_logo').removeClass('input_error');
		var form_data = {};
		form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
		form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
		form_data.store_id = store.get('store_id');
		form_data.store_name = trimText($('.text_logo').val());
		form_data.store_logo_type = 1;
		if(form_data.store_name == ""){
			$('.text_logo').addClass('input_error');
			return false;
		}
		$.post(root + 'save-store-logo.html', form_data,function(data){
			reloadFrame();
		});
	});
	
	$(document).on('click', '.use_logo', function(event){
		event.stopImmediatePropagation();
		$('#text-option-wrapper').addClass('hide');
		$('#logo-option-wrapper').removeClass('hide');
		if($('#store_logo_image_status').val() == 1){
			var form_data = {};
			form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
			form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
			form_data.store_id = store.get('store_id');
			form_data.store_logo_type_only = 1;
			$.post(root + 'save-store-logo.html', form_data,function(data){ reloadFrame(); });
		}
	});
	
	$(document).on('click', '#store_logo_btn', function(event){
		event.stopImmediatePropagation();
		$("#store_logo_file").click();
		return false;
	});
	
	
	$(document).on('change', '#store_logo_file', function(event){
		event.stopImmediatePropagation();
		$(".logo_filename").html($(this)[0].files[0].name);
		$(".file_exist").val(1);
		$('#storelogoform').trigger('submit');
		//$(this).parents('.store-addcat-img').removeAttr('style');
	});
	
	
	$(document).on('submit', '#storelogoform', function(event){
		event.preventDefault();
		event.stopImmediatePropagation();
		
		var options = {
			target:        '#store_logo_message',
			success:       uploadLogoResponse
		};
		$('#store_logo_wrapper #popup-preloader-wrapper').show();
        $(this).ajaxSubmit(options); 
        return false; 
    });
	
	$(document).on('click', '.save_logo_text_btn', function(event){
		event.stopImmediatePropagation();
		$('.text_logo').removeClass('input_error');
		var form_data = {};
		form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
		form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
		form_data.store_id = store.get('store_id');
		form_data.store_name = trimText($('.text_logo').val());
		form_data.store_logo_type = 1;
		if(form_data.store_name == ""){
			$('.text_logo').addClass('input_error');
			return false;
		}
		$.post(root + 'save-store-logo.html', form_data,function(data){
			reloadStoreLogo();
			reloadFrame();
		});
	});
	
	/* end store logo */
	
	/* begin background webstore */
	$(document).on('click', '.webstore_menu', function(event){
		event.stopImmediatePropagation();
		if(webstorebgLoadStatus <= 0){
			reloadWebStoreBG();
			return false;
		}
	});
	$(document).on('click', '#webstore_bg_btn', function(event){
		event.stopImmediatePropagation();
		$("#webstore_bg_file").click();
		return false;
	});
	$(document).on('change', '#webstore_bg_file', function(event){
		event.stopImmediatePropagation();
		$(".filename").html($(this)[0].files[0].name);
		$(".file_exist").val(1);
		$('#webstorebgform').trigger('submit');
	});
	
	$(document).on('click', '#save_background_btn', function(event){
		event.stopImmediatePropagation();
		$('#webstorebgform').trigger('submit');
	});
	
	$(document).on('click', '#delete_background_image_btn', function(event){
		event.stopImmediatePropagation();
		$('#dstatus').val('yes');
		$('#webstorebgform').trigger('submit');
	});
	
	$(document).on('submit', '#webstorebgform', function(event){
		event.preventDefault();
		event.stopImmediatePropagation();
		var options = {
			target:		'#display_message',
			success:	uploadResponse
		};
		
		$('#webstore_bg_wrapper #popup-preloader-wrapper').show();
        $(this).ajaxSubmit(options); 
        return false; 
    });
	
	$(document).on('click', '.option-box', function(event){
		event.stopImmediatePropagation();
		$('.webstore_photo').addClass('hide');
		$('.option-enabled').addClass('hide');
		$('.production-option-wrapper').addClass('hide');
		$('.infor-wrapper').addClass('hide');
		$(".option-box").removeClass('selected-box');
		$(this).addClass('selected-box');
		$(this).find('.option-enabled').removeClass('hide');
		$(this).find('.production-option-wrapper').removeClass('hide');
		$(this).find('.infor-wrapper').removeClass('hide');
		if($(this).find('.home_settings').val() == '1a' || $(this).find('.home_settings').val() == '1b'){
			if(parseInt($(this).find('.product_slideshow_used:checked').val()) == 1 && $('input[name=showlatest]').is(':checked')){
				$('.webstore-template-image1b').removeClass('hide');
			}else{
				$('.webstore-template-image1a').removeClass('hide');
			}
		}else{
			$('.webstore-template-image' + $(this).find('.home_settings').val()).removeClass('hide');
		}
		
		
		url = root + 'save-homepage-settings.html';
		var form_data = {};
		form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
		form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
		form_data.store_id = store.get('store_id');
		form_data.homepage_settings = $(this).find('.home_settings').val();
		if(form_data.homepage_setting == '1a' || form_data.homepage_setting == '1b'){
			form_data.homepage_setting = 1;
		}
		form_data.show_latest = typeof $(this).find('.showlatest:checked').val() !== "undefined" ? $(this).find('.showlatest:checked').val() : 0;
		form_data.used_slideshow = typeof $(this).find('.product_slideshow_used:checked').val() !== "undefined" ? $(this).find('.product_slideshow_used:checked').val() : 0;
		if(form_data.used_slideshow == 0){ $('.show-latest-prod-wrapper').addClass('hide'); }
		if(form_data.used_slideshow == 1){ $('.show-latest-prod-wrapper').removeClass('hide'); }
		$.post(url, form_data,function(data){ reloadFrame(); });
	});
	/* end background webstore */
	
	/* begin manage other pages */
	$(document).on('click', '#other_page_tab', function(event){
		event.stopImmediatePropagation();
		if(otherpageLoadStatus <= 0 || typeof otherpageLoadStatus === 'undefined'){
			reloadOtherPages();
			return false;
		}
	});
	
	$(document).on('click', '.add-new-page', function(event){
		event.stopImmediatePropagation();
		$('.add_edit_page_form').trigger('click');
		reloadAddEditPage();
	});
	
	$(document).on('click', '.add-new-sub-page', function(event){
		event.stopImmediatePropagation();
		$('.add_edit_page_form').trigger('click');
		var settings = $.parseJSON($(this).val());
		reloadAddEditPage(settings);
	});
	
	$(document).on('click', '.activate_page', function(event){
		event.stopImmediatePropagation();
		url = root + 'activate-page.html';
		var form_data = {};
		form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
		form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
		form_data.store_id = store.get('store_id');
		var settings = $.parseJSON($(this).val());
		$.extend(form_data,settings);
		$.post(url, form_data,function(data){ 
			if(settings.show == "sub pages"){
				reloadOtherPages({show:'sub pages',page_id:settings.parent_id});
			}else{
				reloadOtherPages();
			}
			reloadFrame(); 
		});
		
	});
	
	$(document).on('click', '.edit_page', function(event){
		event.stopImmediatePropagation();
		//url = root + 'edit-page-settings.html';
		var settings = $.parseJSON($(this).val());
		$('.add_edit_page_form').trigger('click');
		reloadAddEditPage(settings);
	});
	
	$(document).on('click', '.delete_page', function(event){
		event.stopImmediatePropagation();
		var settings = $.parseJSON($(this).val());
		if (! confirm("Are you sure you want to delete " + settings.title + " ?")) {
			return false;
		}
		
		if(settings.total_subpage > 0){
			alert("Delete first the sub-pages before deleting the main page...");
			return false;
		}
		
		url = root + 'delete-page.html';
		var form_data = {};
		form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
		form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
		form_data.store_id = store.get('store_id');
		var settings = $.parseJSON($(this).val());
		$.extend(form_data,settings);
		$.post(url, form_data,function(data){ 
			if(settings.show == "sub pages"){
				reloadOtherPages({show:'sub pages',page_id:settings.parent_id});
			}else{
				reloadOtherPages();
			}
			reloadFrame(); 
		});
	});
	
	$(document).on('click', '.show_sub_pages', function(event){
		event.stopImmediatePropagation();
		reloadOtherPages({show:'sub pages',page_id:$(this).attr('page_id')});
		return false;
	});
	
	$(document).on('click', '.backtomainpage', function(event){
		event.stopImmediatePropagation();
		reloadOtherPages();
		return false;
	});
	
	$(document).on('click', '.save_page_btn', function(event){
		event.stopImmediatePropagation();
		$(this).parents("form").trigger("submit");
		return false;
	});
	
	$(document).on('submit', '#savePageSettings', function(event){
		event.preventDefault();
		event.stopImmediatePropagation();
		tinyMCE.triggerSave();
        $form = $(this);
		$('.add_edit_page #popup-preloader-wrapper').show();
		
		var error_status = 0;
		$('#store_name').removeClass('input_error');
		
		url = $form.attr( 'action' );
		var form_data = {};
		
		form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
		form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
		form_data.store_id = store.get('store_id');
		form_data.id = trimText($form.find( 'input[name="id"]' ).val());
		form_data.title = trimText($form.find( 'input[name="title"]' ).val());
		form_data.original_title = trimText($form.find( 'input[name="original_title"]' ).val());
		form_data.system_generated = trimText($form.find( 'input[name="system_generated"]' ).val());
		form_data.content = trimText($form.find( 'textarea[name="content"]' ).val());
		form_data.parent_id = $form.find( 'select[name="parent_page"]' ).val();
		form_data.allow_comments = $form.find( 'input:checkbox[name=allow_comments]:checked' ).val();
		form_data.enable_share = $form.find( 'input:checkbox[name=enable_share]:checked' ).val();
		form_data.show_email = $form.find( 'input:checkbox[name=show_email]:checked' ).val();
		var settings = $.parseJSON($('.thepage').val());
		form_data.parent_page_id = settings.parent_id;
		
		if(form_data.title == ""){
			$('#store_name').addClass('input_error');
			$('.add_edit_page #popup-preloader-wrapper').hide();
			return false;
		}
		
		$.post(url, form_data,function(data){
			$('.add_edit_page #popup-preloader-wrapper').hide();
			close_modal($('.add_edit_page'));
			if(form_data.parent_id > 0){
				reloadOtherPages({show:'sub pages',page_id:form_data.parent_id});
			}else{
				reloadOtherPages();
			}
			reloadFrame(); 
		});
		
		return false; 
    });
	
	$(document).on('click', '.store_page_nav', function(event){
		event.stopImmediatePropagation();
		
		var loader, wrapper;
		
		if($(this).attr('id') == "ministore_page"){
			$('.mini_store_popup').trigger('click');
			loader = $('.mini_store_popup_wrapper #popup-preloader-wrapper');
			wrapper = $('.mini_store_popup_wrapper .box-content');
		}
		if($(this).attr('id') == "facebook_page"){
			$('.fb_store_popup').trigger('click');
			loader = $('.fb_store_popup_wrapper #popup-preloader-wrapper');
			wrapper = $('.fb_store_popup_wrapper .box-content');
		}
		
		
		loader.show();
		url = root + 'view-store-page.html';
		var form_data = {};
		
		form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
		form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
		form_data.store_id = store.get('store_id');
		form_data.page = $(this).attr('id');
		$.post(url, form_data,function(data){
			wrapper.html(data);
			loader.hide();
		});
	});
	
	/* end manage other pages */
	
	/* begin styx */
	$(document).on('touchstart click', '.vstyx', function(event){
		clearActiveMenu();
		event.stopImmediatePropagation();
		var form_data = {};
		form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
		form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
		$('.main-preloader').show();
		$.post(root + "check-user-type.html", form_data, function(user_data){
			var user_data = $.parseJSON(user_data);
			if(user_data.user_type != ""){
				$('.vstyx').addClass('active');
				add_styx_header_style();
				load_v_styx_main_page();
			}else{
				clearActiveMenu();
				$('.main-preloader').hide();
				$('.styx_user_type_wrapper').trigger('click');
			}
		});
	});
	$(document).on('touchstart click', '.styka-view-all', function(event){
		$('.vstyx').trigger('click');
	});
	
	$(document).on('click', '.i-am-styx-btn', function(event){
		event.stopImmediatePropagation();
		var form_data = {};
		form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
		form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
		form_data.user_type = $(this).attr('id');
		$('.styx_user_type #popup-preloader-wrapper').show();
		$.post(root + "save-user-type.html", form_data, function(data){
			$('.styx_user_type #popup-preloader-wrapper').hide();
			clearActiveMenu();
			$('.vstyx').addClass('active');
			add_styx_header_style();
			load_v_styx_main_page();
			close_modal($('.styx_user_type'));
		});
		return false;
	});
	
	$(document).on('click', '.i-am-shop-btn', function(event){
		event.stopImmediatePropagation();
		var form_data = {};
		form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
		form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
		form_data.user_type = $(this).attr('id');

		$.post(root + "save-user-type.html", form_data, function(data){
			clearActiveMenu();
			$('.your-store').addClass('active');
			$('.your-store').trigger('click');
			close_modal($('.styx_user_type'));
		});
		return false;
	});
	
	$(document).on('touchstart click', '.your-store', function(event){
		clearActiveMenu();
		event.stopImmediatePropagation();
		var store_data = $.parseJSON($(this).attr('data'));
		if(typeof store_data.page_tab !== "undefined"){
			$('.your-store').addClass('active');
		}else{
			$(this).addClass('active');
		}
		remove_styx_header_style();
		resetSettings();
		
		var form_data = {};
		form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
		form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
		if(typeof store_data.user_type !== "undefined"){
			form_data.user_type = store_data.user_type;
			store.set('loadShopOrStyx', 1);
		}
		$('.main-preloader').show();
		$.post(root + "check-user-type.html", form_data, function(user_data){
			var user_data = $.parseJSON(user_data);
			if(user_data.user_type == ""){
				$('.shop_user_type_wrapper').trigger('click');
				clearActiveMenu();
				$('.main-preloader').hide();
				return false;
			}
			$.post(root + "check-current-step.html", form_data, function(step){
				if(step <= 3){
					if(shopStepCss == 0){
						loadCSS( cloud_url + "css/steps.css");
						shopStepCss++;
					}
					loadCreateShop();
				}
				if(step > 3){
					$('.shoplabel').html('Your Shop');
					$('.se-a-wrapper').removeClass('hide');
					$('.main-catalog-menu').removeClass('hide');
					if(user_data.user_type == "V"){
						$('.se-label').html('Sales');
						load_vendor_store_content(store_data);
					}
					if(user_data.user_type == "P"){
						$('.se-label').html('Earnings');
						$('.pw-label').html('Wallet');
						load_promoter_store_content(store_data);
					}
				}
			});
			
			
		});
		
	});
	
	$(document).on('click', '.create_new_styx_btn, .styx-new, .ecommerce-create', function(event){
		event.stopImmediatePropagation();
		store.remove('store_id');
		clearActiveMenu();
		$('.vstyx').addClass('active');
		var settings = $.parseJSON($(this).attr('data'));
		loadCreateStyx(settings);
	});
	
	$(document).on('click', '.create_styx_btn', function(event){
		event.stopImmediatePropagation();
		load_v_styx_content();
	});
	
	$(document).on('click', '.styx-edit, .ecommerce-edit', function(event){
		event.stopImmediatePropagation();
		clearActiveMenu();
		$('.vstyx').addClass('active');
		var settings = $.parseJSON($(this).val());
		store.set('store_id', settings.store_id);
		loadCreateStyx(settings);
		//load_v_styx_content();
	});
	
	$(document).on('click', '.edit-styx-name', function(event){
		event.stopImmediatePropagation();
		$('.view_styx_name_wrapper').trigger('click');
		$('.styx_name_input').val($('.user-styx-name').html());
	});
	
	$(document).on('click', '.styx-delete', function(event){
		event.stopImmediatePropagation();
		var settings = $.parseJSON($(this).val());
		if (! confirm("Are you sure you want to delete " + settings.store_name + " ?")) {
			return false;
		}
		
		url = root + 'delete-styx.html';
		var form_data = {};
		
		form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
		form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
		form_data.store_id = settings.store_id;
		
		$.post(url, form_data,function(data){
			if(data == "" || parseInt(data) < 1){
				$('.vstyx').html('Create Styx <span class="white-overlap"></span>');
			}
			load_v_styx_main_page();
		});
	});
	
	$(document).on('click', '.main-styx-delete', function(event){
		event.stopImmediatePropagation();
		var settings = $.parseJSON($(this).val());
		if (! confirm("Are you sure you want to delete " + settings.store_name + " ?")) {
			return false;
		}
		$('.main-preloader').show();
		
		url = root + 'delete-styx.html';
		var form_data = {};
		
		form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
		form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
		form_data.store_id = settings.store_id;
		
		$.post(url, form_data,function(data){
			$.post(root + "check-user-type.html", form_data, function(user_data){
				var user_data = $.parseJSON(user_data);
				if(user_data.user_type == "V"){
					load_landing_page({page_link: root + 'landing-page.html', home_type: 'vendor'});
				}
				if(user_data.user_type == "P"){
					load_landing_page({page_link: root + 'landing-page.html', home_type: 'promoter'});
				}
				$('.main-preloader').hide();
			});
		});
	});
	
	$(document).on('click', '.main-styx-delete, .ecommerce-delete', function(event){
		event.stopImmediatePropagation();
		var settings = $.parseJSON($(this).val());
		if (! confirm("Are you sure you want to delete " + settings.store_name + " ?")) {
			return false;
		}
		$('.main-preloader').show();
		
		url = root + 'delete-styx.html';
		var form_data = {};
		
		form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
		form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
		form_data.store_id = settings.store_id;
		
		$.post(url, form_data,function(data){
			location.href = root;
		});
	});
	
	$(document).delegate('.remove-user-styx-product', 'click', function(event){
		event.stopImmediatePropagation();
		if (! confirm("Are you sure you want to remove " + $(this).parents('.shop-product-wrapper').find('.sptitle').html() + " ?")) {
			return false;
		}
		
		var $this = $(this);
		var form_data = {};
			form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
			form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
			form_data.store_id = typeof store.get('store_id') !== "undefined" ? store.get('store_id') : "";
			form_data.stykable_type = typeof store.get('stykable_type') !== "undefined" ? store.get('stykable_type') : "";
			var product_data = $.parseJSON($(this).val());
			$.extend(form_data,product_data);

			$.post(root + 'remove-styx-product.html', form_data,function(data){
				$('.buttons-wrapper').hide();
				var no_products = parseInt($('.products-num').html()) - 1;
				$('.products-num').html(no_products);
				if($this.hasClass('on-search-page')){
					loadStyxProducts({search_text:store.get('search_text'),publish_type:$('#filter_styx_search_display').val()});
				}else{
					loadStyxProducts();
				}
				return false;
			});
	});
	
	$(document).on('click', '#save_styx_name_btn', function(event){
		event.stopImmediatePropagation();
		$(this).parents("form").trigger("submit");
		return false;
	});
	
	$(document).on('submit', '#styxNameForm', function(event){
		event.preventDefault();
		event.stopImmediatePropagation();
		$form = $(this);
		var error_status = 0;
		$('.styx_name_input').removeClass('input_error');
		
		url = $form.attr( 'action' );
		var form_data = {};

		form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
		form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
		form_data.store_id = typeof store.get('styx_id') !== "undefined" ? store.get('styx_id') : "";
		form_data.stykable_type = typeof store.get('stykable_type') !== "undefined" ? store.get('stykable_type') : "";
		form_data.styx_name = trimText($form.find( 'input[name="styx_name"]' ).val());
		form_data.store_type = 1;
		form_data.update_styx = true;

		if(form_data.styx_name == ""){
			$('.styx_name_input').addClass('input_error');
			return false;
		}
		$('.styx_name_wrapper #popup-preloader-wrapper').show();
		$.post(url, form_data,function(data){
			$('.styx_name_wrapper #popup-preloader-wrapper').hide();
			close_modal($('.styx_name_wrapper'));
			loadCreateStyx(form_data);
		});
		
	});
	
	$(document).on('change', '#styx_cat_listing', function(event){
		event.stopImmediatePropagation();
		loadStyxProducts({category_id:$(this).val()});
	});
	
	$(document).on('touchstart click', '#color-design-button', function(event){
		event.preventDefault();
		event.stopImmediatePropagation();
		$('.h-products-btn').removeClass('h-products-btn-selected');
		$(this).addClass('h-color-design-btn-selected');
		loadStyxColorDesign({display_save_btn: true});
		$('.buttons-wrapper').show();
	});
	
	$(document).on('click', '.stykbale-save-btn', function(e){
		e.stopImmediatePropagation();
		var form_data = {};
		form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
		form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
		form_data.store_id = typeof store.get('styx_id') !== "undefined" ? store.get('styx_id') : "";
		form_data.stykable_type = typeof store.get('stykable_type') !== "undefined" ? store.get('stykable_type') : "";
		form_data.color1 = $('#stykable-color1').val();
		form_data.color2 = $('#stykable-color2').val();
		form_data.color3 = $('#stykable-color3').val();
		form_data.theme = $('#select_theme').val();
		form_data.width = $('#width_val').val() + 'px';
		form_data.height = $('#height_val').val() + 'px';
		form_data.font_size = $('#font_size').val() + 'px';
		form_data.mobile_width = $( '#mobile_width:checked' ).val();
		$('#styx-page-wrapper #popup-preloader-wrapper').show();
		$.post(root + 'save-stykable-cad.html', form_data,function(data){
			loadStyxColorDesign({display_save_btn:true});
		});
	});
	
	$(document).on('click', '#select-more-styx-products', function(event){
		event.preventDefault();
		event.stopImmediatePropagation();
		loadStyxProductsSelection({display:true});
	});
	
	$(document).on('click', '#done-select-products', function(event){
		event.preventDefault();
		event.stopImmediatePropagation();
		loadStyxProducts();
	});
	
	$(document).on('click', '#main-manage-styx-wrapper .shop-category-menu-for-styx li', function(event){
		event.stopImmediatePropagation();
		var element = $('#main-manage-styx-wrapper .shop-category-menu-for-styx').jScrollPane({});
		var api = element.data('jsp');
		api.destroy();
		
		var category_level = '';
		$('#main-manage-styx-wrapper .shop-category-menu-for-styx li').removeClass('selected');
		$(this).addClass('selected');
		$('#main-manage-styx-wrapper .shop-category-menu-for-styx li label').removeClass('active');
		$(this).children('label').addClass('active');
		if($(this).hasClass('main') && typeof $(this).find('ul').css('display') === 'undefined'){
			$('#main-manage-styx-wrapper .shop-category-menu-for-styx li ul').slideUp(20);
		}
		if(! $(this).hasClass('sub') && ($(this).hasClass('main') && $(this).find('ul').css('display') == 'none')){
			$('#main-manage-styx-wrapper .shop-category-menu-for-styx li ul').slideUp(20);
			$(this).find('ul').slideDown(20);
		}
		if($(this).hasClass('main')){ category_level = 'main'; }
		if($(this).hasClass('sub')){ category_level = 'sub'; }
		loadStyxProductsSelection({category_level:category_level,category_id:$(this).attr('data'),publish_type:$('#styx_filter_product_display').val(),products_only:true});
	});
	
	$(document).on('change', '#main-manage-styx-wrapper #styx_filter_product_display', function(event){
		event.stopImmediatePropagation();
		loadStyxProductsSelection({publish_type:$(this).val(),category_id:$('.shop-category-menu-for-styx li.selected').attr('data'),products_only:true});
	});
	
	$(document).on('click', '#main-manage-styx-wrapper .v_product_listing', function(event){
		event.stopImmediatePropagation();
		var page = $(this).attr('href');
		loadStyxProductsSelection({page_link:page,category_id:$('.shop-category-menu-for-styx li.selected').attr('data'),publish_type:$('#styx_filter_product_display').val(),products_only:true});
		return false;
	});
	
	$(document).on('click', '#main-manage-styx-wrapper .v_search_product_listing', function(event){
		event.stopImmediatePropagation();
		var page = $(this).attr('href');
		loadStyxProductsSelection({search_text: store.get('search_text'), page_link:page});
		return false;
	});
	
	$(document).on('click', '#main-manage-styx-wrapper .addtostyx', function(event){
		event.preventDefault();
		event.stopImmediatePropagation();
		var $this = $(this);
		var form_data = {};
		form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
		form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
		form_data.store_id = typeof store.get('styx_id') !== "undefined" ? store.get('styx_id') : "";
		form_data.stykable_type = typeof store.get('stykable_type') !== "undefined" ? store.get('stykable_type') : "";
		var product_data = $.parseJSON($(this).val());

		$.extend(form_data, product_data);
		$('.sp-product-main-wrapper #popup-preloader-wrapper').show();
		$.post(root + 'add-product-to-styx.html', form_data,function(data){
			if(parseInt(data) >= 10){
				alert('Maximum of 15 products per slideshow allowed');
				$('.sp-product-main-wrapper #popup-preloader-wrapper').hide();
				return false;
			}
			var no_products = parseInt($('.products-num').html()) + 1;
			$('.products-num').html(no_products);
			$this.addClass('remove-styx-product');
			$this.html('Remove');
			$this.removeClass('addtostyx');
			$this.removeClass('plus-btn');
			$('.sp-product-main-wrapper #popup-preloader-wrapper').hide();
			return false;
		});
	});
	
	$(document).delegate('#main-manage-styx-wrapper .remove-styx-product', 'click', function(event){
		event.stopImmediatePropagation();
		if (! confirm("Are you sure you want to remove " + $(this).parents('.shop-product-wrapper').find('.sptitle').html() + " ?")) {
			return false;
		}
		
		var $this = $(this);
		var form_data = {};
			form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
			form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
			form_data.store_id = typeof store.get('styx_id') !== "undefined" ? store.get('styx_id') : "";
			form_data.stykable_type = typeof store.get('stykable_type') !== "undefined" ? store.get('stykable_type') : "";
			var product_data = $.parseJSON($(this).val());
			$.extend(form_data,product_data);

			$('.sp-product-main-wrapper #popup-preloader-wrapper').show();
			$.post(root + 'remove-styx-product.html', form_data,function(data){
				var no_products = parseInt($('.products-num').html()) - 1;
				$('.products-num').html(no_products);
				$($this).addClass('plus-btn');
				$($this).html('+');
				$($this).addClass('addtostyx');
				$($this).removeClass('remove-styx-product');
				$('.sp-product-main-wrapper #popup-preloader-wrapper').hide();
				return false;
			});
	});
	
	$(document).on('focus', '#main-manage-styx-wrapper .search-user-catalog', function(event){
		event.stopImmediatePropagation();
		if($(this).val() == "Search my catalog"){
			$(this).val('');
		}
	});
	
	$(document).on('blur', '#main-manage-styx-wrapper .search-user-catalog', function(event){
		event.stopImmediatePropagation();
		if($(this).val() == ""){
			$(this).val('Search my catalog');
		}
	});
	
	$(document).on('keypress', '#main-manage-styx-wrapper .search-user-catalog', function(e){
		e.stopImmediatePropagation();
		if (e.which == 13) {
			$(this).removeClass('input_error');
			var search_text = trimText($(this).val());
			
			if(search_text == ""){
				$(this).addClass('input_error');
				return false;
			}
			
			store.set('search_text', search_text);
			loadStyxProductsSelection({search_text:search_text, display:true});
			return false;
		}
	});
	
	$(document).on('click', '#main-manage-styx-wrapper #back-to-catalog-list', function(e){
		e.stopImmediatePropagation();
		loadStyxProductsSelection({display:true});
	});
	
	$(document).on('click', '.load_styx_products', function(e){
		e.stopImmediatePropagation();
		var page = $(this).attr('href');
		loadStyxProductsSelection({page_link:page});
		return false;
	});
	
	$(document).on('focus', '.search-styx-product', function(event){
		event.stopImmediatePropagation();
		if($(this).val() == "Search styx products"){
			$(this).val('');
		}
	});
	
	$(document).on('blur', '.search-styx-product', function(event){
		event.stopImmediatePropagation();
		if($(this).val() == ""){
			$(this).val('Search styx products');
		}
	});
	
	$(document).on('keypress', '.search-styx-product', function(e){
		e.stopImmediatePropagation();
		if (e.which == 13) {
			$(this).removeClass('input_error');
			var search_text = trimText($(this).val());
			
			if(search_text == ""){
				$(this).addClass('input_error');
				return false;
			}
			
			store.set('search_text', search_text);
			loadStyxProducts({search_text:search_text});
			return false;
		}
	});
	
	$(document).on('change', '#filter_styx_search_display', function(event){
		event.stopImmediatePropagation();
		loadStyxProducts({search_text:store.get('search_text'),publish_type:$(this).val()});
	});
	
	$(document).on('touchstart click', '#products-button', function(event){
		event.preventDefault();
		event.stopImmediatePropagation();
		$('.h-color-design-btn').removeClass('h-color-design-btn-selected');
		$(this).addClass('h-products-btn-selected');
		$('.buttons-wrapper').hide();
		loadStyxProducts();
	});
	
	$(document).on('change', '#select_widget_size', function(event){
		event.preventDefault();
		event.stopImmediatePropagation();
		if($(this).val() == 0){ return false; }
		saveWidget({widget_type:$(this).val(), page_type:$('.page_type_option').find('.selected').val()});
	});
	
	$(document).on('click', '.frame_btn', function(event){
		event.preventDefault();
		event.stopImmediatePropagation();
		if($('#select_widget_size').val() == 0){ return false; }
		
		$('.frame_btn').removeClass('selected');
		$(this).addClass('selected');
		if($(this).hasClass('splash')){
			$('#splash-details').removeClass('hide');
		}else{
			$('#splash-details').addClass('hide');
		}
		saveWidget({widget_type:$('#select_widget_size').val(), page_type:$(this).val()});
	});
	
	$(document).on('click', '.store-btn-fn', function(event){
		event.preventDefault();
		event.stopImmediatePropagation();
		if($(this).hasClass('fb-fn')){
			$('.fb-fn').removeClass('selected');
			$(this).addClass('selected');
		}
		if($(this).hasClass('ms-fn')){
			$('.ms-fn').removeClass('selected');
			$(this).addClass('selected');
		}
		var settings = $.parseJSON($(this).val());
		saveStore(settings);
	});
	
	$(document).on('click', '#upload-splash-btn, #image-input', function(event){
		event.stopImmediatePropagation();
		$("#splash_image_file").click();
		return false;
	});
	
	$(document).on('change', '#splash_image_file', function(event){
		event.stopImmediatePropagation();
		$("#image-input").val($(this)[0].files[0].name);
		$(".splash_image_exist").val(1);
		$('#splashImageForm').trigger('submit');
	});
	
	$(document).on('submit', '#splashImageForm', function(event){
		event.preventDefault();
		event.stopImmediatePropagation();
		
		var options = {
			target:        '#display_response',
			success:       splashImageResponse
		};
		$('#widget-wrapper #popup-preloader-wrapper').show();
        $(this).ajaxSubmit(options); 
        return false;
    });
	
	$(document).on('click', '#splash_action_btn', function(event){
		event.stopImmediatePropagation();
		
		url = 'save-splash-action.html';
		var form_data = {};

		form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
		form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
		form_data.store_id = store.get('styx_id');
		form_data.show_action = $(this).is(':checked')? 1 : 0;
		form_data.action_label = trimText($('#action-label').val());

		$('#widget-wrapper #popup-preloader-wrapper').show();
		$.post(url, form_data,function(data){
			loadWidget();
		});
	});
	
	$(document).on('click', '#delete-splash-btn', function(event){
		event.stopImmediatePropagation();
		
		url = 'delete-splash-image.html';
		var form_data = {};

		form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
		form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
		form_data.store_id = store.get('styx_id');

		$('#widget-wrapper #popup-preloader-wrapper').show();
		$.post(url, form_data,function(data){
			$('#image-input').val('');
			$('.outer-upload-wrapper').removeClass('hide');
			$('.splash-delete-wrapper').addClass('hide');
			loadWidget();
		});
	});
	
	/* end styx */
	
	/* begin sales */
	$(document).on('touchstart click', '.your-sales', function(event){
		clearActiveMenu();
		event.stopImmediatePropagation();
		$(this).addClass('active');
		remove_styx_header_style();
		load_sales_page();
	});
	
	$(document).on('click','.sales-wrapper .left-menu li', function(event) {
		event.stopImmediatePropagation();
		$('.sales-wrapper .left-menu li').removeClass('selected');
		$(this).addClass('selected');
		
		$('.sales-wrapper #popup-preloader-wrapper').show();
		load_sales_page({},$(this).attr('url'));
	});
	
	/* end sales */
	
	/* begin wallet */
	$(document).on('touchstart click', '.your-wallet', function(event){
		clearActiveMenu();
		event.stopImmediatePropagation();
		$('.pw-a').addClass('active');
		load_wallet_page();
	});
	/* end wallet */
	
	/* begin main catalog */
	$(document).on('click', '.main-catalog-menu', function(event){
		event.stopImmediatePropagation();
		clearActiveMenu();
		$(this).addClass('active');
		var datas = $.parseJSON($(this).attr('data'));
		var form_data = {};
		form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
		$.extend(form_data,datas);
		store.set('store_id', form_data.store_id);
		$('.main-preloader').show();
		$.post(root + "load-main-catalog.html", form_data, function(content){
			$('#profilepage-wrapper').html(content);
			$('.main-preloader').hide();
		});
	});
	
	/* end main catalog */
	
	/* begin wishlist */
	$(document).on('click', '.wishlist-menu', function(event){
		event.stopImmediatePropagation();
		clearActiveMenu();
		$('.wishlist-menu').addClass('active');
		var form_data = {};
		form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
		form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
		$('.main-preloader').show();
		$.post(root + "load-wishlist-page.html", form_data, function(content){
			$('#profilepage-wrapper').html(content);
			$('.main-preloader').hide();
		});
	});
	
	$(document).on('click', '#wishlist-wrapper .wishlist-tabs li', function(event){
		event.stopImmediatePropagation();
		$('.wishlist-tabs li').removeClass('active');
		$(this).addClass('active');
		var form_data = {};
		form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
		form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
		var datas = $.parseJSON($(this).attr('data'));
		$.extend(form_data,datas);
		$('.wishlist-content-wrapper #popup-preloader-wrapper').show();
		$.post(root + "load-wishlist-content.html", form_data,function(data){ 
			$('.wishlist-content').html(data);
			$('.wleft h2').html(datas.title);
			$('.wishlist-content-wrapper #popup-preloader-wrapper').hide();
		});
	});
	/* end wishlist */
	
	/* begin messages */
	$(document).on('click', '.message-menu', function(event){
		event.stopImmediatePropagation();
		clearActiveMenu();
		$('.message-menu').addClass('active');
		var form_data = {};
		form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
		form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
		$('.main-preloader').show();
		$.post(root + "load-message-page.html", form_data, function(content){
			$('#profilepage-wrapper').html(content);
			$('.main-preloader').hide();
		});
	});
	
	$(document).on('click', '.show-messages', function(event){
		event.stopImmediatePropagation();
		$('.show-messages').removeClass('selected');
		$(this).addClass('selected');
		$(this).find('.unread').remove();
		var datas = $.parseJSON($(this).attr('datas'));
		var loader = $('.mright #popup-preloader-wrapper');
		var wrapper = $('.messages-thread');
		var page = root + 'messages-thread.html';
		var form_data = {};
		form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
		form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
		$.extend(form_data,datas);
		loader.show();
		$.post(page, form_data,function(data){
			wrapper.html(data);
			loader.hide();
		});
	});
	
	$(document).on('click', '#send-message', function(e){
		e.preventDefault();
		e.stopImmediatePropagation();
		if(jQuery.trim($('.message-input-wrapper textarea').val()) == ""){
			return false;
		}
		var loader = $('.mright #popup-preloader-wrapper');
		var wrapper = $('.messages-thread');
		var page = root + 'send-message.html';
		var form_data = {};
		form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
		form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
		form_data.recipient_id = $('#recipient_id').val();
		form_data.recipient_name = $('#recipient_name').val();
		form_data.message = jQuery.trim($('.message-input-wrapper textarea').val());
		loader.show();
		$.post(page, form_data,function(data){
			wrapper.html(data);
			loader.hide();
		});
	});
	
	$(document).on('click', '#send-new-message', function(e){
		e.preventDefault();
		e.stopImmediatePropagation();
		if(jQuery.trim($('.message-input-wrapper textarea').val()) == ""){
			return false;
		}
		if($('#recipient_id').val() == ""){
			alert('Please type again the email or username and select one of the dropdown result(s)');
			return false;
		}
		var loader = $('.mright #popup-preloader-wrapper');
		var wrapper = $('.messages-thread');
		var page = root + 'send-message.html';
		var form_data = {};
		form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
		form_data.recipient_id = $('#recipient_id').val();
		form_data.message = jQuery.trim($('.message-input-wrapper textarea').val());
		loader.show();
		$.post(page, form_data,function(data){
			wrapper.html("Your message was successfully sent");
			loader.hide();
		});
	});
	
	$(document).on('click', '#create-message', function(e){
		e.preventDefault();
		e.stopImmediatePropagation();
		$('.show-messages').removeClass('selected');
		var loader = $('.mright #popup-preloader-wrapper');
		var wrapper = $('.messages-thread');
		var form_data = {};
		form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
		form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
		wrapper.html('');
		loader.show();
		$.post(root + 'create-message.html', form_data,function(data){
			wrapper.html(data);
			loader.hide();
		});
	});
	
	$(document).on('click', '.sendbtn', function(e){
		e.preventDefault();
		e.stopImmediatePropagation();
		$(this).parents('form').submit();
	});
	
	$(document).on('submit', '#emailForm', function(e){
		e.preventDefault(); e.stopImmediatePropagation();
		var $form = $(this);
		var url = $form.attr( 'action' );
		var form_data = {};
		form_data.subject = jQuery.trim($form.find( 'input[name="subject"]' ).val());
		form_data.sender_email = jQuery.trim($form.find( 'input[name="sender_email"]' ).val());
		form_data.sender = jQuery.trim($form.find( 'input[name="sender_id"]' ).val());
		if(form_data.sender == ""){
			form_data.sender = form_data.sender_email;
		}
		form_data.name = jQuery.trim($form.find( 'input[name="name"]' ).val());
		form_data.message = jQuery.trim($form.find( 'textarea[name="message"]' ).val());
		form_data.recipient = jQuery.trim($form.find( 'input[name="recipient_id"]' ).val());
		form_data.reference = 'cars page';
		$('.sendbtn').attr('disabled', 'disabled');
		$.post(url, form_data,function(data){
			var _data = $.parseJSON(data);
			$('.sendbtn').removeAttr('disabled');
		});
		return false;
	});	
	/* end messages */
	
	/* begin stykable to domain */
	
	$(document).on('click', '.change-stykable', function(e){
		e.preventDefault(); e.stopImmediatePropagation();
		$('.domain_listing').trigger('click');
		load_inactive_domain({hostname_id:$(this).attr('hid')});
	});
	
	$(document).on('click', '.remove-stykable', function(e){
		e.preventDefault(); e.stopImmediatePropagation();
		loadPreloader('.common-domain-wrapper');
		var form_data = {};
		form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
		form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
		form_data.hostname_id = $(this).attr('hid');
		$.post(root + "remove-domain.html", form_data, function(data){
			load_active_domain();
		});
	});
	
	$(document).on('click', '.publish-stykable', function(e){
		e.preventDefault(); e.stopImmediatePropagation();
		loadPreloader('.domain_listing_wrapper');
		var form_data = {};
		form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
		form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
		var settings = $.parseJSON($(this).attr('pub_data'));
		$.extend(form_data,settings);
		$.post(root + "activate-domain.html", form_data, function(data){
			load_active_domain();
			$('.modal_close').trigger('click');
		});
	});
	
	$(document).on('click', '.show-shop-embed-code', function(e){
		$('.embed_code_modal').trigger('click');
		$('.shop-embed-code').val('');
		$('.shop-embed-code').val($(this).val());
	});
	
	$(document).on('click', '.upgrade_link', function(e){
		e.preventDefault();
		upgrade_subscription_plan();
	});
	
	/* end stykable to domain */
	
	/* begin stykable single product */
	$(document).on('click', '.embedSingleProduct', function(e){
		e.preventDefault(); e.stopImmediatePropagation();
		$('.embed_single_product_modal').trigger('click');
		loadPreloader('.s-product-content');
		url = root + 'stykable-single-product.html';
		var form_data = {};
		form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
		form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
		form_data.product_id = $(this).attr('pid');
		form_data.product_user_id = $(this).attr('puid');
		form_data.store_category_id = $(this).attr('cid');
		form_data.store_id = 1;
		
		$.post(url, form_data,function(data){
			$('.s-product-content').html(data);
			removePreloader();
		});
	});
	
	$(document).on('click', '.s-product-save-btn', function(e){
		e.stopImmediatePropagation();
		var form_data = {};
		form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
		form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
		form_data.ref_user_id = $('#s_ruid').val();
		form_data.product_id = $('#s_pid').val();
		form_data.product_user_id = $('#s_puid').val();
		form_data.store_category_id = $('#s_scid').val();
		form_data.stykable_id = $('#stykable_id').val();
		form_data.stykable_type = 2;
		form_data.color1 = $('#stykable-color1').val();
		form_data.color2 = $('#stykable-color2').val();
		form_data.color3 = $('#stykable-color3').val();
		form_data.theme = $('#select_theme').val();
		form_data.width = $('#width_val').val() + 'px';
		form_data.height = $('#height_val').val() + 'px';
		form_data.font_size = $('#font_size').val() + 'px';
		form_data.mobile_width = $( '#mobile_width:checked' ).val();
		loadPreloader('.s-product-content');
		$.post(root + 'save-stykable-single-product.html', form_data,function(data){
			var _data = $.parseJSON(data);
			$('#stykable_id').val(_data.stykable_id);
			$('.sp-embed-code').val(_data.embed_code);
			removePreloader();
		});
	});
	/* end stykable single product */
	
/* end version 5 script */
});

function load_inactive_domain(dataObj){
	url = root + 'load-inactive-domains.html';
	var form_data = {};
	form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
	form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
	$.extend(form_data,dataObj);
	loadPreloader('.domain_listing_wrapper');
	$.post(url, form_data,function(data){
		$('.domain_listing_wrapper .box-content').html(data);
		removePreloader();
	});
}

function load_active_domain(){
	url = root + 'load-active-domains.html';
	var form_data = {};
	form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
	form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
	loadPreloader('.common-domain-wrapper');
	$.post(url, form_data,function(data){
		$('.common-domain-wrapper .styk-upgrade-content-wrapper').html(data);
		removePreloader();
	});
}

function upgrade_subscription_plan() {
	$('.upgradepremium-vendor').trigger('click');
	loadPreloader('.upgradepremium-vendor-wrapper .box-content');
	var form_data = {};
	form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
	form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
	$.ajax({
		type: "POST",
		url: '/account.html?action=account_upgrade_content',
		data: form_data,
		cache: false,
		success: function(response){
			$('.upgradepremium-vendor-wrapper .box-content').html(response);
			var jSpane_pricing_plans = $('.package-wrapper .scrollbar');
			jSpane_pricing_plans.jScrollPane();
			jSpane_api_pricing_plans = jSpane_pricing_plans.data('jsp');
		}
	});
}

function save_colors(dataObj){
	url = root + 'save-selected-colors.html';
	var form_data = {};
	form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
	form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
	form_data.store_id = store.get('store_id');
	$.extend(form_data,dataObj);
	
	$.post(url, form_data,function(data){
		$('#store_colors_wrapper #popup-preloader-wrapper').hide();
		close_modal($('.v_store_colors'));
	});
	
}

function is_jsfile_already_loaded(jsfile) {
	var res = false;
	$(document).find('head script').each(function(){
		if(typeof $(this).attr('src') == 'string' && $(this).attr('src') != '') {	
			if($(this).attr('src').indexOf(jsfile) != -1) {
				res = true;	
			}
		}
	});
	
	return res;
}

function get_total_existing_products(){
	$.get(root + 'get-total-existing-products.html', function(data){ $('.total_existing_prod').html(data); });
}

function get_all_total_products(){
	var form_data = {};
	form_data.store_id = typeof store.get('store_id') !== "undefined" ? store.get('store_id') : "";
	form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
	$.post(root + 'get-all-total-products.html', form_data,function(data){ 
		$('.alltotalprod').html(data);
		if(parseInt(data) >= 1){
			$('#i_add_new_product').after('<button id="view-my-products" class="button store-bg3" >Manage Products</button>');
			$('#i_add_new_product').remove();
		}
		if(parseInt(data) <= 0){
			$('#view-my-products').after('<button unp_val="" show_store_product="all" id="i_add_new_product" addto="product" cid="0" ctype="" class="button store-bg3 addthisto view_my_products unp" rel="addproducts_wrapper" modal=".addproducts_wrapper" >Add New</button>');
			$('#view-my-products').remove();
		}
	});
}

function get_total_user_owned_products(){
	var form_data = {};
	form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
	form_data.store_id = typeof store.get('store_id') !== "undefined" ? store.get('store_id') : "";
	$.post(root + 'get-total-user_owned-products.html', form_data,function(data){ $('.totalownprod').html(data); });
}

function get_total_endorsed_product(){
	var form_data = {};
	form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
	form_data.store_id = typeof store.get('store_id') !== "undefined" ? store.get('store_id') : "";
	$.post(root + 'get-total-endorsed-products.html', form_data,function(data){ $('.totalendorsedprod').html(data); });
}

function get_total_main_categories(){
	var form_data = {};
	form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
	form_data.store_id = typeof store.get('store_id') !== "undefined" ? store.get('store_id') : "";
	$.post(root + 'get-total-main-categories.html', form_data,function(data){ 
		$('.scno').html(data);
		if(parseInt(data) >= 1){
			$('.addnewcatbtn').after('<button id="manage-categories" class="button store-bg3">Manage Categories</button>');
			$('.addnewcatbtn').remove();
		}
		if(parseInt(data) <= 0){
			$('#manage-categories').after('<button cattype="cat" class="button store-bg3 add-new-catbox addnewcatbtn">Add New</button>');
			$('#manage-categories').remove();
		}
		
	});
}

function reload_selector_style(){
	if (!$.browser.opera) {
		$('select.sortby').each(function(){
			var title = $(this).attr('title');
			if( $('option:selected', this).val() != ''  ) title = $('option:selected',this).text();
			$(this)
				.css({'z-index':10,'opacity':0,'-khtml-appearance':'none'})
				.after('<div class="sortby">' + title + '</div>')
				.change(function(){
					val = $('option:selected',this).text();
					$(this).next().text(val);
					})
		});
	};
}

function ifnotlogin(){
	close_modal($('.transfer_category'));
	csConfirm( 'Oops!', 
		'<div id="success-message-box">\
		 <p>Your session has expired, please login again.</p>\
		 </div>\
		',
		{Ok: null}, null, null,
		{ width:520, position:['center',120] }
	);
	$('.ui-dialog-titlebar').before('<div id="modalbox-colors-logo"><div class="one"></div><div class="two"></div><div class="three"></div><div class="four"></div><div class="five"></div></div>');
	
}

function process_start(msg,parent_container){
	if(typeof (parent_container) == 'undefined') { parent_container = '.str-wrapper'; }
	$(parent_container).find(".status_info_wrapper").fadeIn( 200, function(){
		$(this).html('<span class="status_info">'+ msg +'</span>');
	});
	return;
}
function process_done(msg,parent_container){
	if(typeof (parent_container) == 'undefined') { parent_container = '.str-wrapper'; }
	
	$(parent_container).find(".status_info").fadeOut(300, function(){
		$(this).remove();
	});
	$(parent_container).find(".status_info_wrapper").fadeIn( 300, function(){
		$(this).html('<span class="status_info">'+ msg +'</span>');
	});
	$(parent_container).find(".status_info").delay(3600).fadeOut(300, function(){
		$(this).remove();
	});
	return;
}

function showResponse(responseText, statusText, xhr, $form)  {
	var _data = $.parseJSON(responseText);
	$('.loader').remove();
	if(_data.status == 'failed'){
		$('#display_message').html(_data.error_msg);
		$('.add_edit_category_btn_one, .add_edit_category_btn_two').removeAttr("disabled");
		return false;
	}
	close_modal($('.addnewcategory'));
	close_modal($('.editcategory'));
	product_reload_category_list();
	if($('#create_shop_step').val() == 3){
		if( typeof loadShopCatalog == 'function' ){
			loadShopCatalog();
			$('#store_modal_level6').removeAttr('style');
			return false;
		}
	}
	$('.add_edit_category_btn_one, .add_edit_category_btn_two').removeAttr("disabled");
	if(typeof $('#ur_product_user_id').val() !== 'undefined'){
		loadPromoteProductForm({"product_id":$('#ur_product_id').val(),"product_user_id":$('#ur_product_user_id').val()});
	}
	reloadProductListing();
	if( _data.result == 'subcategory' ){
		reloadSubCategoryListing({order_id:_data.order_id});
	}else{
		reloadCategoryListing();
	}
	
	$('#product-listing-wrapper #popup-preloader-wrapper').hide();
	$('#store_modal_level6').removeAttr('style');
	reloadFrame();
	return false;
}

function uploadLogoResponse(responseText, statusText, xhr, $form)  {
	var _data = $.parseJSON(responseText);
	reloadStoreLogo();
	reloadFrame();
	return false;
}

function uploadResponse(responseText, statusText, xhr, $form)  {
	var _data = $.parseJSON(responseText);
	reloadWebStoreBG();
	reloadFrame();
	return false;
}

function splashImageResponse(responseText, statusText, xhr, $form){
	var _data = $.parseJSON(responseText);
	if(_data.result == 'success'){
		$('.outer-upload-wrapper').addClass('hide');
		$('.splash-delete-wrapper').removeClass('hide');
	}
	loadWidget();
	return false;
}

function reload_page(){
	if($('#create_shop_step').val() == 3){
		loadShopCatalog();
		return false;
	}
	
	if( typeof $('.on-main-catalog').val() !== 'undefined' ){
		var page = $('#main-catalog-wrapper .product_listing').attr('href');
		loadMainCatalog({page_link:page,category_id:$('.shop-category-menu li.selected').attr('data'),publish_type:$('#filter_product_display').val()});
		return false;
	}
	
	if(productLoadStatus == 2){
		if(loadShopCatalog == 1){
			reloadProductListing();
			loadShopCatalog = 0;
		}else{
			reloadProductListing({publish_type:$('#filter_product_display').val(),category_id:$('.shop-category-menu li.selected').attr('data'),category_level:$('.shop-category-menu li.selected').attr('class').split(' ')[0]});
		}
		productSearching({search_text:store.get('search_text'),publish_type:$('#filter_product_display-onsearch').val()});
		if( $('#product_info_settings').val() != ""){
			reloadProductInfo();
		}
		return false;
	}
}

function reloadProductListing(dataObj){
	var form_data = {};
	var page, wrapper, loader, loader2;
	page = root + "product-listing-page.html";
	loader = $('#product-listing-wrapper #popup-preloader-wrapper');
	wrapper = $('#product-listing-sub-wrapper');
	form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
	form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
	form_data.store_id = store.get('store_id');
	$.extend(form_data,dataObj);
	if(typeof form_data.products_only !== "undefined"){
		loader = $('.sp-product-main-wrapper #popup-preloader-wrapper');
		loader2 = $('#product-listing-wrapper #popup-preloader-wrapper');
		wrapper = $('.scp-wrapper');
	}
	loader.show();
	if(typeof form_data.page_link !== "undefined"){
		page = form_data.page_link;
	}

	$.post(page, form_data,function(data){
		wrapper.html(data);
		loader.fadeOut(50);
		if(typeof form_data.products_only !== "undefined"){
			loader2.fadeOut(50);
		}
		$('#category-listing-wrapper #popup-preloader-wrapper').hide();
	});
	productLoadStatus = 1;
	
}

function loadSelectMoreProducts(dataObj){
	$('#v-create-shop-content').addClass('hide');
	$('.shopform-wrapper').html('');
	$('#v-shop-catalog').removeClass('hide');
	
	var page = root + "more-shop-products.html",
		wrapper = $('.more_shop_products_wrapper .box-content'),
		loader = $('.more_shop_products_wrapper #popup-preloader-wrapper');
	var form_data = {};
		form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
		form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
		form_data.store_id = 1;
	$.extend(form_data,dataObj);
	if(typeof form_data.load !== "undefined"){
		wrapper = $('.shop-catalog-right');
		loader = $('.shop-catalog-right-wrapper #popup-preloader-wrapper');
	}
	loader.show();
	if(typeof form_data.page_link !== "undefined"){
		page = form_data.page_link;
	}
	$.post(page, form_data,function(data){
		wrapper.html(data);
		loader.hide();
		$('.shop-department-list-wrapper').jScrollPane();
	});
}

function productSearching(dataObj){
	var form_data = {};
	var page; var wrapper; var loader;
	page = root + "product-searching.html";
	wrapper = $('.search_popup .box-content');
	loader = $('.search_popup #popup-preloader-wrapper');
	loader.show();
	form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
	form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
	form_data.store_id = store.get('store_id');
	$.extend(form_data,dataObj);

	if(typeof form_data.page_link !== "undefined"){
		page = form_data.page_link;
	}
	
	$.post(page, form_data,function(data){
		wrapper.html(data);
		loader.fadeOut(50);
	});
}

function reloadCategoryListing(dataObj){
	var form_data = {};
	var page; var wrapper; var loader;
	page = root + "category-listing-page.html";
	loader = $('#category-listing-wrapper #popup-preloader-wrapper');
	wrapper = $('#category-listing-sub-wrapper');
	loader.show();
	form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
	form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
	form_data.store_id = store.get('store_id');
	$.extend(form_data,dataObj);

	if(typeof form_data.page_link !== "undefined"){
		page = form_data.page_link;
	}

	$.post(page, form_data,function(data){
		wrapper.html(data);
		loader.hide();
		$('#product-listing-wrapper #popup-preloader-wrapper').hide();
	});
	categoriesLoadStatus = 1;
	
}

function reloadSubCategoryListing(dataObj){
	var form_data = {};
	var page; var wrapper; var loader;
	page = root + "sub-category-listing-page.html";
	loader = $('#category-listing-wrapper #popup-preloader-wrapper');
	wrapper = $('#category-listing-sub-wrapper');
	loader.show();
	form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
	form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
	form_data.store_id = store.get('store_id');
	$.extend(form_data,dataObj);

	if(typeof form_data.page_link !== "undefined"){
		page = form_data.page_link;
	}

	$.post(page, form_data,function(data){
		wrapper.html(data);
		loader.fadeOut(50);
	});
	categoriesLoadStatus = 1;
	
}


function reloadColors(dataObj){
	var form_data = {};
	var page; var wrapper; var loader;
	page = root + "store-colors.html";
	loader = $('#store_colors_wrapper #popup-preloader-wrapper');
	wrapper = $('#store_colors_wrapper .box-content');
	loader.show();
	form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
	form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
	form_data.store_id = store.get('store_id');
	$.extend(form_data,dataObj);

	$.post(page, form_data,function(data){
		wrapper.html(data);
		loader.fadeOut(50);
	});
	storecolorsLoadStatus = 1;
}

function reloadStoreLogo(dataObj){
	var form_data = {};
	var page; var wrapper; var loader;
	page = root + "store-logo-text-image.html";
	loader = $('#store_logo_wrapper #popup-preloader-wrapper');
	wrapper = $('#store_logo_wrapper .box-content');
	loader.show();
	form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
	form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
	form_data.store_id = store.get('store_id');
	$.extend(form_data,dataObj);

	$.post(page, form_data,function(data){
		wrapper.html(data);
		loader.fadeOut(50);
	});
	storelogoLoadStatus = 1;
	
}

function reloadWebStoreBG(dataObj){
	var form_data = {};
	var page; var wrapper; var loader;
	page = root + "webstore-bg.html";
	loader = $('#webstore_bg_wrapper #popup-preloader-wrapper');
	wrapper = $('#webstore_bg_wrapper .box-content');
	loader.show();
	form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
	form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
	form_data.store_id = store.get('store_id');
	$.extend(form_data,dataObj);

	$.post(page, form_data,function(data){
		wrapper.html(data);
		loader.fadeOut(50);
	});
	webstorebgLoadStatus = 1;
	
}

function reloadHomepageSettings(dataObj){
	var form_data = {};
	var page; var wrapper; var loader;
	page = root + "homepage-settings.html";
	loader = $('.m-homepage #popup-preloader-wrapper');
	wrapper = $('#homepage_settings_wrapper');
	loader.show();
	form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
	form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
	form_data.store_id = store.get('store_id');
	$.extend(form_data,dataObj);

	$.post(page, form_data,function(data){
		wrapper.html(data);
		loader.fadeOut(50);
	});
	homepageLoadStatus = 1;
	
}

function reloadOtherPages(dataObj){
	var form_data = {};
	var page; var wrapper; var loader;
	page = root + "other-pages.html";
	loader = $('.m-otherpage #popup-preloader-wrapper');
	wrapper = $('#other_pages_wrapper');
	loader.show();
	form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
	form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
	form_data.store_id = store.get('store_id');
	$.extend(form_data,dataObj);

	$.post(page, form_data,function(data){
		wrapper.html(data);
		loader.fadeOut(50);
	});
	otherpageLoadStatus = 1;
}

function reloadProductInfo(){
	var form_data = {};
	var page; var wrapper; var loader;
	page = root + 'load-product-details.html';
	loader = $('.product_details #popup-preloader-wrapper');
	wrapper = $('.product_details .box-content');
	loader.show();
	form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
	form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
	form_data.store_id = typeof store.get('store_id') !== "undefined" ? store.get('store_id') : 0;
	var settings = $.parseJSON($('#product_info_settings').val());
	$.extend(form_data,settings);

	$.post(page, form_data,function(data){
		wrapper.html(data);
		loader.fadeOut(50);
	});
	return false;
}

function reloadFrame(){
	//document.getElementById('pageFrame').contentDocument.location.reload(true);
	

	$('.store_page_nav').removeClass('selected');
	$('#webstore_page').addClass('selected');
	$('.store-frame-wrapper').html('');
	$('#box-white #popup-preloader-wrapper').show();
	url = root + 'view-store-page.html';
	var form_data = {};
	
	form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
	form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
	form_data.store_id = store.get('store_id');
	form_data.page = 'webstore_page';
	$('#ministore-control-wrapper').addClass('hide');
	$('#fbstore-control-wrapper').addClass('hide');
	
	$.post(url, form_data,function(data){
		$('.store-frame-wrapper').html(data);
		$('#box-white #popup-preloader-wrapper').hide();
	});
}

function previewWebstoreFrame(dataObj){
	$('.store_page_nav').removeClass('selected');
	$('#webstore_page').addClass('selected');
	$('.store-frame-wrapper').html('');
	$('#box-white #popup-preloader-wrapper').show();
	url = root + 'view-store-page.html';
	var form_data = {};
	
	form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
	form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
	form_data.store_id = store.get('store_id');
	form_data.page = 'webstore_page';
	$.extend(form_data,dataObj);
	$('#ministore-control-wrapper').addClass('hide');
	$('#fbstore-control-wrapper').addClass('hide');
	
	$.post(url, form_data,function(data){
		$('.store-frame-wrapper').html(data);
		$('.save-colors-btn').show();
		$('#box-white #popup-preloader-wrapper').hide();
	});
}

function load_v_styx_main_page() {
	$('#profilepage-wrapper').html('');
	$('.main-preloader').show();
	var ts = Math.round((new Date()).getTime() / 1000);
	loadCSS( cloud_url + "css/styx-landing.css?"+ts);
	var form_login_data = {};
	form_login_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
	form_login_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
	form_login_data.store_id = store.get('store_id');

	$.post(root + "vendor-styx-home.html", form_login_data, function(content){
		$('#profilepage-wrapper').html(content);
		$('.main-preloader').hide();
	});
}

function load_v_styx_content() {
	$('#profilepage-wrapper').html('');
	$('.main-preloader').show();
	var form_login_data = {};
	form_login_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
	form_login_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
	form_login_data.store_id = store.get('store_id');

	$.post(root + "vendor-styx-create.html", form_login_data, function(content){
		//$.getScript(cloud_url + "js/store-form-fn.js");
		resetSettings();
		$('#profilepage-wrapper').html(content);
		$('#profilepage-wrapper').show();
		loadWidget();
		$('.main-preloader').hide();
	});
}

function loadManageStyx() {
	$('#profilepage-wrapper').html('');
	$('.main-preloader').show();
	var form_login_data = {};
	form_login_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
	form_login_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
	form_login_data.store_id = store.get('styx_id');
	form_login_data.stykable_type = typeof store.get('stykable_type') !== "undefined" ? store.get('stykable_type') : "";

	$.post(root + "manage-styx.html", form_login_data, function(content){
		resetSettings();
		$('#profilepage-wrapper').html(content);
		$('#profilepage-wrapper').show();
		if(form_login_data.stykable_type == 'ad'){
			loadWidget();
		}
		$('.main-preloader').hide();
	});
}

function loadPromoteProductForm(dataObj){
	var form_data = {};
		form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
		form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
		form_data.store_id = typeof store.get('store_id') !== "undefined" ? store.get('store_id') : 1;
		form_data.ref_user_id = form_data.user_id;
		$.extend(form_data,dataObj);
		var wrapper = $('.category_box_wrapper .box-content'),
			loader = $('.category_box_wrapper #popup-preloader-wrapper'),
			url = root + 'promote-product-form.html';
		loader.show();
		$.post(url, form_data,function(data){
			wrapper.html(data);
			loader.hide();
		});
}

function loadExistingProduct(dataObj){
	var form_data = {};
		form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
		form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
		form_data.store_id = typeof store.get('store_id') !== "undefined" ? store.get('store_id') : 0;
		$.extend(form_data,dataObj);
		$('.existing_product_popup').trigger('click');
		$('.existing_product_wrapper #popup-preloader-wrapper').show();
		var url = root + 'add-existing-products.html';

		$.post(url, form_data,function(data){
			$('.existing_product_wrapper .box-content').html(data);
			$('.existing_product_wrapper #popup-preloader-wrapper').hide();
		});
}

function loadPromoteProductLists(dataObj){
	var form_data = {};
	var page; var wrapper; var loader;
	page = root + "promote-product-lists.html";
	wrapper = $('.search_popup .box-content');
	loader = $('.search_popup #popup-preloader-wrapper');
	loader.show();
	form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
	form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
	form_data.store_id = store.get('store_id');
	$.extend(form_data,dataObj);

	if(typeof form_data.page_link !== "undefined"){
		page = form_data.page_link;
	}
	
	$.post(page, form_data,function(data){
		wrapper.html(data);
		loader.fadeOut(50);
	});
}

function loadPromoterDepartments(dataObj){
	var form_data = {};
	var page; var wrapper; var loader;
	var page = root + "load-promoter-departments.html";
	// var wrapper = $('.shopform-wrapper');
	// var loader = $('.shopform #popup-preloader-wrapper');
	wrapper = $('#profilepage-wrapper');
	loader = $('.main-preloader');
	loader.show();
	form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
	form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
	form_data.parent_id = 0;
	if(jQuery.trim($('#tmp_storage').html()) != ""){
		form_data.department_id = $( 'input[name="department_id[]"]' ).map(function() { return $(this).val() }).get().join(",");
	}
	$.extend(form_data,dataObj);

	if(typeof form_data.page_link !== "undefined"){
		page = form_data.page_link;
	}
	$('.more-products-close-btn').trigger('click');
	$.post(page, form_data,function(data){
		wrapper.html(data);
		loader.hide();
	});
}

function loadPromoterSubDepartments(dataObj){
	var form_data = {};
	var page; var wrapper; var loader;
	var page = root + "load-departments.html";
	var wrapper = $('.promoter_sub_departments_wrapper .box-content');
	var loader = $('.promoter_sub_departments_wrapper #popup-preloader-wrapper');
	loader.show();
	form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
	form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
	$.extend(form_data,dataObj);
	$.post(page, form_data,function(data){
		wrapper.html(data);
		loader.hide();
	});
}

function reloadAddEditPage(dataObj){
	var form_data = {};
	var page; var wrapper; var loader;
	page = root + "add-edit-page-form.html";
	loader = $('.add_edit_page #popup-preloader-wrapper');
	wrapper = $('.add_edit_page .box-content');
	loader.show();
	form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
	form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
	form_data.store_id = store.get('store_id');
	$.extend(form_data,dataObj);

	if(typeof form_data.page_link !== "undefined"){
		page = form_data.page_link;
	}

	$.post(page, form_data,function(data){
		wrapper.html(data);
		loader.fadeOut(50);
	});
	
}

function reloadStorePreview(dataObj){
	url = root + 'view-store-page.html';
	var loader, wrapper, form_data = {};
	if(dataObj.store_type == 'ministore'){
		loader = $('.stykable-box #popup-preloader-wrapper');
		wrapper = $('.stykable-box .stykable-box-content');
		
		// loader = $('.mini_store_popup_wrapper #popup-preloader-wrapper');
		// wrapper = $('.mini_store_popup_wrapper .box-content');
		form_data.page = 'ministore_page';
	}
	if(dataObj.store_type == 'fbstore'){
		loader = $('.fb_store_popup_wrapper #popup-preloader-wrapper');
		wrapper = $('.fb_store_popup_wrapper .box-content');
		form_data.page = 'facebook_page';
	}
	loader.show();
	form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
	form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
	form_data.store_id = 1;
	
	
	$.post(url, form_data,function(data){
		wrapper.html(data);
		loader.hide();
	});
}

function loadWidget(dataObj){
	var form_data = {};
	var page; var wrapper; var loader;
	$('#widget-display-inner').remove();
	page = root + "widget-view.html";
	loader = $('#widget-wrapper #popup-preloader-wrapper');
	wrapper = $('#widget-display');
	loader.show();
	form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
	form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
	form_data.store_id = typeof store.get('styx_id') !== "undefined" ? store.get('styx_id') : "";
	$.extend(form_data,dataObj);

	if(typeof form_data.page_link !== "undefined"){
		page = form_data.page_link;
	}

	$.post(page, form_data,function(data){
		wrapper.html(data);
		loader.fadeOut(50);
		$('#code-wrapper').val($('#embed_result').val());
	});
	
}

function saveWidget(dataObj){
	var form_data = {};
	var page; var wrapper; var loader;
	$('#widget-display-inner').remove();
	page = root + "save-widget-settings.html";
	loader = $('#widget-wrapper #popup-preloader-wrapper');
	loader.show();
	form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
	form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
	form_data.store_id = typeof store.get('styx_id') !== "undefined" ? store.get('styx_id') : "";
	$.extend(form_data,dataObj);
	$.post(page, form_data,function(data){
		loadWidget(dataObj);
	});
}

function saveStore(dataObj){
	var form_data = {};
	var page; var wrapper; var loader;
	$('#widget-display-inner').remove();
	page = root + "save-store-settings.html";

	form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
	form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
	form_data.store_id = 1;
	$.extend(form_data,dataObj);
	$.post(page, form_data,function(data){
		reloadStorePreview(dataObj);
	});
}

function reloadFrameStore(dataObj){
	$('#box-white #popup-preloader-wrapper').show();
	url = root + 'view-store-page.html';
	var form_data = {};
	form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
	form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
	form_data.store_id = store.get('store_id');
	form_data.page = $(this).attr('id');
	$.extend(form_data,dataObj);
	$.post(url, form_data,function(data){
		$('.store-frame-wrapper').html(data);
		$('#box-white #popup-preloader-wrapper').hide();
	});
}
	
function saveColors(dataObj){
	var form_data = {};
	var page; var wrapper; var loader;
	$('#widget-display-inner').remove();
	page = root + "save-store-colors.html";
	loader = $('#widget-wrapper #popup-preloader-wrapper');
	loader.show();
	form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
	form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
	form_data.store_id = store.get('store_id');
	$.extend(form_data,dataObj);
	if(typeof form_data.datafor !== 'undefined' && form_data.datafor == 'styx'){
		form_data.store_id = store.get('styx_id');
	}
	$.post(page, form_data,function(data){
		storecolorsLoadStatus = 0;
		loadWidget(dataObj)
	});
}

function saveStoreColors(dataObj){
	var form_data = {};
	var page; var wrapper; var loader;
	$('#widget-display-inner').remove();
	page = root + "save-store-colors.html";
	loader = $('#widget-wrapper #popup-preloader-wrapper');
	loader.show();
	form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
	form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
	form_data.store_id = store.get('store_id');
	$.extend(form_data,dataObj);
	$.post(page, form_data,function(data){
		reloadColors();
		$('#store_colors_wrapper #popup-preloader-wrapper').hide();
		reloadFrame();
	});
}

function resetSettings(){
	productLoadStatus = 0;
	categoriesLoadStatus = 0;
	storecolorsLoadStatus = 0;
	storelogoLoadStatus = 0;
	webstorebgLoadStatus = 0;
	homepageLoadStatus = 0;
	otherpageLoadStatus = 0;
}

function add_styx_header_style(){
	$('.main-logo-bg').addClass('styx-main-logo-bg');
	$('.main-menu-bg').addClass('styx-main-menu-bg');
	$('.main-menu-tab').addClass('styx-main-menu-tab');
	$('#status-tab').addClass('styx-status-tab');
	$('.font-color-main').addClass('styx-font-color-main');
}

function remove_styx_header_style(){
	$('.main-logo-bg').removeClass('styx-main-logo-bg');
	$('.main-menu-bg').removeClass('styx-main-menu-bg');
	$('.main-menu-tab').removeClass('styx-main-menu-tab');
	$('#status-tab').removeClass('styx-status-tab');
	$('.font-color-main').removeClass('styx-font-color-main');
}

function load_store_settings(dataObj) {
	var form_login_data = {};
	form_login_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
	form_login_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
	form_login_data.store_id = typeof store.get('store_id') !== "undefined" ? store.get('store_id') : 0;
	form_login_data.pagetype = dataObj.pagetype;
	$.extend(form_login_data,dataObj);
	$('.createvendor-wrapper').removeClass('get-paid-wrapper');
	
	if(dataObj.pagetype == 'get_paid') {
		$('.createvendor-wrapper').addClass('get-paid-wrapper');
	}
	
	$('.createvendor-wrapper .box-content').html('<div id="dialog-box"><div style="width: 100%; text-align: center; margin: 80px 0;"><img class="img-loader" src="' + cloud_url + 'images/dialog-ajax-loader.gif" /></div></div>');
	$('.add_vendor').trigger('click');
	$.ajax({
		type: "POST",
		url: '/create-vendor.html?type=store_settings&show=all',
		data: form_login_data,
		success: function(data){
			var _data = $.parseJSON(data);
			$('.createvendor-wrapper .box-content').html(data);
			$('.createvendor-wrapper .modal_close_level6').show();
		}
	});
}

function load_account_page(dataObj) {
	var form_login_data = {};
	form_login_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
	form_login_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
	form_login_data.store_id = store.get('store_id');
	$.extend(form_login_data,dataObj);
	
	var page = (typeof (page) == 'undefined') ? "account.html" : page;
	if(page == "account.html") {
		$('#profilepage-wrapper').html('');
		$('.main-preloader').show();
		//var ts = Math.round((new Date()).getTime() / 1000);
		//loadCSS( cloud_url + "css/account.css?"+ts);
	}
	
	$.post(root + page, form_login_data, function(content){
		if(page == "account.html") {
			$('#profilepage-wrapper').html(content);
			if(typeof form_login_data.openTab !== "undefined"){
				$(".active").removeClass("active selected");  
				$('.' + form_login_data.openTab).addClass("active selected");  
				$(".content-accnt").hide();  
				var content_show = $('.' + form_login_data.openTab).attr("title");  
				load_account_billingshipping(dataObj.url,content_show);
			}
			$('.main-preloader').hide();
		}
		else {
			$('.sales-wrapper #main-right-wrapper').html(content);
		}
	});
}

/* sales section */
function load_sales_page(dataObj,page) {
	var form_login_data = {};
	form_login_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
	form_login_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
	form_login_data.store_id = store.get('store_id');
	$.extend(form_login_data,dataObj);
	
	var page = (typeof (page) == 'undefined') ? "sales.html" : page;
	if(page == "sales.html") {
		$('#profilepage-wrapper').html('');
		$('.main-preloader').show();
		loadCSS( cloud_url + "css/sales.css");
	}
	
	$.post(root + page, form_login_data, function(content){
		
		if(page == "sales.html") {
			$('#profilepage-wrapper').html(content);
			$('.main-preloader').hide();
		}
		else {
			$('.sales-wrapper #main-right-wrapper').html(content);
		}
	});
}

/* wallet section */
function load_wallet_page(dataObj,content_type) {
	var form_login_data = {};
	form_login_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
	form_login_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
	form_login_data.store_id = store.get('store_id');
	$.extend(form_login_data,dataObj);
	
	var content_type = (typeof (content_type) == 'undefined' || content_type == "") ? "" : content_type;
	var page = "wallet.html?content="+content_type;

	if(content_type == "") {
		loadCSS( cloud_url + "css/wallet.css");
	}
	
	$('#profilepage-wrapper').html('');
	$('.main-preloader').show();
	
	$.post(root + page, form_login_data, function(content){
		
		if(content_type == "") {
			$('#profilepage-wrapper').html(content);
			$('.main-preloader').hide();
		}
	});
}

function load_account_billingshipping(url,el) {
	if(typeof(url) == "undefined") {
		$('#'+el).show();		
	}
	else {
		$('#account-preloader').show();
		var form_data = {};
		form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
		form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
		$.post(url, form_data,function(data){
			$('#account-preloader').hide();
			$('#'+el).html(data);
			$('#'+el).show();	
		});
	}
}

function loadCreateShop(dataObj){
	var form_data = {};
	var page; var wrapper; var loader;
	page = root + "create-shop.html";
	wrapper = $('#profilepage-wrapper');
	loader = $('.main-preloader');
	loader.show();
	form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
	form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
	$.extend(form_data,dataObj);
	$.post(page, form_data,function(data){
		wrapper.html(data);
		loader.hide();
	});
}

function loadCreateStyx(dataObj){
	if(shopStepCss == 0){
		loadCSS( cloud_url + "css/steps.css");
		shopStepCss++;
	}
	var form_data = {};
	var page; var wrapper; var loader;
	page = root + "create-styx.html";
	wrapper = $('#profilepage-wrapper');
	loader = $('.main-preloader');
	loader.show();
	form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
	form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
	$.extend(form_data,dataObj);
	store.remove('styx_id');
	$.post(page, form_data,function(data){
		store.set('stykable_type', form_data.stykable_type);
		if(typeof form_data.store_id !== 'undefined'){
			store.set('styx_id', form_data.store_id);
		}
		if(data == 'styx'){
			loadManageStyx();
		}else{
			wrapper.html(data);
			loader.hide();
		}
	});
}

function loadStyxColorDesign(dataObj){
	var form_data = {};
	var page = root + "colors-and-design.html",
		wrapper = $('#styx-page-content-wrapper'),
		loader = $('#styx-page-wrapper #popup-preloader-wrapper');
	form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
	form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
	form_data.stykable_type = typeof store.get('stykable_type') !== "undefined" ? store.get('stykable_type') : "";
	form_data.store_id = store.get('styx_id');
	form_data.display = true;
	$.extend(form_data,dataObj);
	loader.show();
	$.post(page, form_data,function(data){
		wrapper.html(data);
		loader.hide();
	});
}

function loadStyxProducts(dataObj){
	var form_data = {};
	var page = root + "load-styx-products.html",
		wrapper = $('#styx-page-content-wrapper'),
		loader = $('#styx-page-wrapper #popup-preloader-wrapper');
	form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
	form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
	form_data.stykable_type = typeof store.get('stykable_type') !== "undefined" ? store.get('stykable_type') : "";
	form_data.store_id = store.get('styx_id');
	$.extend(form_data,dataObj);
	loader.show();
	$.post(page, form_data,function(data){
		wrapper.html(data);
		loader.hide();
	});
}

function loadStyxProductsSelection(dataObj){
	var form_data = {};
	var page = root + "product-listing-selection.html",
		wrapper = $('#styx-page-content-wrapper'),
		loader = $('#styx-page-wrapper #popup-preloader-wrapper');
	form_data.token = typeof store.get('token') !== "undefined" ? store.get('token') : "";
	form_data.user_id = typeof store.get('user_id') !== "undefined" ? store.get('user_id') : "";
	form_data.store_id = typeof store.get('styx_id') !== "undefined" ? store.get('styx_id') : "";
	form_data.stykable_type = typeof store.get('stykable_type') !== "undefined" ? store.get('stykable_type') : "";
	$.extend(form_data,dataObj);
	if(typeof form_data.products_only !== "undefined"){
		loader = $('.sp-product-main-wrapper #popup-preloader-wrapper');
		loader2 = $('#styx-page-wrapper #popup-preloader-wrapper');
		wrapper = $('.scp-wrapper');
	}
	loader.show();
	if(typeof form_data.page_link !== "undefined"){
		page = form_data.page_link;
	}
	$.post(page, form_data,function(data){
		wrapper.html(data);
		loader.hide();
		if(typeof form_data.products_only !== "undefined"){
			loader2.hide();
		}
		$('#styx-page-wrapper #popup-preloader-wrapper').hide();
	});
}

function clearActiveMenu(){
	$('.main-menu-tab li a').removeClass('active');
	$('#pic-login').removeClass('active');
	$('.main-catalog-menu').removeClass('active');
	$('.message-menu').removeClass('active');
	$('.wishlist-menu').removeClass('active');
	$('.gift-menu').removeClass('active');
	$('.user-wrapper').removeClass('active');
}