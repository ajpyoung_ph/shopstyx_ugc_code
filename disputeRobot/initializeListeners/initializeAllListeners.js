var resReply = require('../email/resReply');
var phpAPI = require('../email/contactPHPEmailAPI');
var callAPI = require('../server/CRUD/Send/API_call_library');

var checkStatus = require('../server/backgroundProcess/checkDBData/collectDataStatuses');
var schedShip = require('../server/backgroundProcess/checkDBData/scheduleShippingCheck');
var cancelSched = require('../server/backgroundProcess/checkDBData/cancelSchedule');
var setBuyerProtection = require('../server/backgroundProcess/checkDBData/setBuyerProtection');
var setAutoResolve = require('../server/backgroundProcess/checkDBData/setAutoResolve');

var logInfo = require('../server/backgroundProcess/log_events');


var start = function()
{
	//email folder
	process.on('Shopstyx:error',resReply.sendError);
	process.on('Shopstyx:errormail',resReply.sendEmail);
	process.on('Shopstyx:sendGenericEmail',resReply.sendGenericEmail);
	
	//contact PHP API
	process.on('Shopstyx:callPHPAPI',phpAPI.sendRequest);

	//call remote API
	//process.on('Shopstyx:callPHPAPI',callAPI.callAPI);
	//log info in file
	process.on('Shopstyx:logme',logInfo.logInfo);
	//checkPending
	process.on('Shopstyx:checkPending',checkStatus.checkStatuses);
	process.on('Shopstyx:scheduleShippingCheck',schedShip.scheduleShippingCheck);
	process.on('Shopstyx:cancelSchedules',cancelSched.start);
	process.on('Shopstyx:setBuyerProtection',setBuyerProtection.start);
	process.on('Shopstyx:setAutoResolve',setAutoResolve.start);
};

module.exports={
	start:start
};