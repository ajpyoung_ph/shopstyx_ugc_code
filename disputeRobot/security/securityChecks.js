var mysqlStuff = require('../mysql/mysql_convertions');

var validate=function(req,res,emitString)
{
	try{
		var dataStruct = {
			"user_id":parseInt(req.params.user_id),
			"token":req.params.token,
			"req": req,
			"res": res,
			"new_unix_timestamp":'',
			"new_simple_timestamp":'',
			"timezone":"America/Los_Angeles"
		};
		dataStruct.new_simple_timestamp = mysqlStuff.MySQLDateTimeNOW2(dataStruct);
		//currently we have store_id and user_id fixed
		if(process.env.NODE_ENV=='development')// && parseInt(req.params.token) == 0)
		{
			process.emit(emitString,dataStruct);
		}else{
			checkToken(dataStruct,emitString);
		}
	}catch(err){
		var msg = {
			status:"Error",
			message:"[getOverall_transaction_status] Error Processing Data",
			description: err
		};
		GLOBAL.sendError(msg,dataStruct.res);
	}
}

function checkToken(dataStruct,emitString)
{
	if(isNaN(dataStruct.user_id)==false)
	{
		dataStruct.token = mysqlStuff.mysql_real_escape_string(dataStruct.token);
		var query = "SELECT * FROM cs_tokens WHERE user_id="+dataStruct.user_id+" AND token='"+dataStruct.token+"';";
		//console.log(query);
		GLOBAL.db_cs_common.query(query,function(err,rows, fields){
			var msg={};
			if(err==null)
			{
				if(rows.length>0)
				{
					process.emit(emitString,dataStruct);
				}else{
					msg = {
						status:"Error",
						message:"You are not permitted to use this service",
						description:"Credentials are not valid"
					};
					try{
						dataStruct.res.json(msg);
						console.log(msg);
					}catch(err){
						console.log("Error Sending Server response");
						console.log(err);
						console.log(dataStruct.res);
						//dataStruct.res.json(msg);
					}
				}
			}else{
				console.log("Error Accessing DB");
				console.log(err);
				msg = {
					status:"Error",
					message:"[checkTokenError Accessing DB",
					description:err,
					query:query
				};
				GLOBAL.db_cs_common.emit('error',err);
				dataStruct.res.json(msg);
				console.log(msg);
			}
		});
	}else{
		msg = {
			status:"Error",
			message:"You are not permitted to use this service",
			description:"Credentials are not valid"
		};
		try{
			dataStruct.res.json(msg);
			console.log(msg);
		}catch(err){
			console.log("Error Sending Server response");
			console.log(err);
			console.log(dataStruct.res);
			//dataStruct.res.json(msg);
		}
	}
	
}




module.exports = {
	validate:validate
}