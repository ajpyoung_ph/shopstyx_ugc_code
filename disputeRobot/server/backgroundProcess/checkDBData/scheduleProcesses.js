var schedule = require('node-schedule');
var start = function()
{
	//schedule email reminder a day before expiration
	//on schedule expiration, check for dispute status and job.cancel() 
	
	switch(process.env.NODE_ENV.toLowerCase())
	{
		case 'office':
		case 'development':
				console.log("Triggering Shipping Reminder");
		case 'staging':
		default:
				process.emit('Shopstyx:checkPending');
				console.log("running production settings for collectPending");
				var pendingStatusRule = new schedule.RecurrenceRule();
				pendingStatusRule.hour = 01;
				pendingStatusRule.minute = 00;
				console.log("calling Shipping Reminder every "+pendingStatusRule.hour+":"+pendingStatusRule.minute);
				var j = schedule.scheduleJob(pendingStatusRule, function(){
					process.emit('Shopstyx:logme',"Triggering Status Collection");
					process.emit('Shopstyx:checkPending');
				});

				break;
	}
}

module.exports = {
	start:start
};