var schedule = require('node-schedule');
var request = require('request');
var conv = require('../../../mysql/mysql_convertions');
//var time = require('time');
var emailReminder = require('../../../email/postEmail');

var start = function(rows)
{
	try{
		var query = "SELECT * FROM cs_settings WHERE `group`='order' AND `key`='order_refund_addtional_days'";
		GLOBAL.db_cs_common.query(query,function(err,datarows, fields){
			if(err==null)
			{
				var protection_days = parseInt(datarows[0].value);
				process.emit('Shopstyx:logme','protection_datys:'+protection_days);
				process.emit('Shopstyx:logme','rows.length: '+rows.length);
				if(rows.length>0)
				{
					for(var x=0;x<rows.length;x++)
					{
						// if(typeof(GLOBAL.scheduleShippingCheck[rows[x].shopstyx_transaction_id])!='undefined')
						// {
						// 	process.emit('Shopstyx:logme','setSchedule: '+rows[x].shopstyx_transaction_id);
						// 	setImmediate(setSchedule,rows[x],protection_days);
						// }else{
						// 	// process.emit('Shopstyx:logme','NO setSchedule: '+rows[x].shopstyx_transaction_id);
						// 	// process.emit('Shopstyx:logme','Order Details:');
						// 	// process.emit('Shopstyx:logme','purchase_date: '+rows[x].purchase_date);
						// 	// process.emit('Shopstyx:logme','purchase_date_simple: '+rows[x].purchase_date_simple);
						// 	// process.emit('Shopstyx:logme','extra_days: '+rows[x].extra_days);
						// 	// process.emit('Shopstyx:logme','item #: '+x);
						// 	setImmediate(setSchedule,rows[x],protection_days);
						// }
						setImmediate(setSchedule,rows[x],protection_days);
						setImmediate(checkScheduleForExpiryReminder,rows[x],protection_days);
					}
				}
			}
		});
		
	}catch(err){
		//no need to report error here
		console.log("Error in scheduleShippingCheck")
		console.log(err);
	}
};

function setSchedule(cs_users_orders,protection_days)
{
	//order_refund_addtional_days
	// try{
	// 	GLOBAL.scheduleShippingCheck[cs_users_orders.shopstyx_transaction_id].cancel();
	// 	delete GLOBAL.scheduleShippingCheck[cs_users_orders.shopstyx_transaction_id]; //purchase_date	
	// }catch(err){
	// 	//do nothing
	// }
	//get all shopstyx_transaction_id's for this specific shopstyx_order_id
	var query = "SELECT * FROM cs_users_orders WHERE (overall_transaction_status LIKE 'Shipped') AND user_id="+cs_users_orders.user_id+" AND shopstyx_order_id='"+cs_users_orders.shopstyx_order_id+"'  ORDER BY purchase_date;";
	//javascript date = new Date(unix_timestamp*1000);
	GLOBAL.db_cs_common.query(query,function(err,rows, fields){
		if(err==null)
		{
			for(var x=0;x<rows.length;x++)
			{
				//do this once we get the transactions per order id
				var extra_days = parseInt(rows[x].extra_days);
				var purchase_date = new Date(rows[x].purchase_date * 1000);
				var protection_date = new Date(rows[x].purchase_date * 1000);
				protection_date.setDate(purchase_date.getDate() + protection_days + extra_days);
				var now = new Date();
				//check if protection_date is still greater than now
				var protection_date_unix = Math.floor(protection_date.getTime() / 1000);
				var now_unix = Math.floor(now.getTime()/1000);
				if(protection_date_unix>now_unix)
				{
					process.emit('Shopstyx:logme','calling for setting Schedule for Protection: '+rows[x].shopstyx_transaction_id);
					if(typeof(GLOBAL.scheduleShippingCheck[rows[x].shopstyx_transaction_id])!='undefined')
					{
						GLOBAL.scheduleShippingCheck[rows[x].shopstyx_transaction_id].cancel();
						delete GLOBAL.scheduleShippingCheck[rows[x].shopstyx_transaction_id];
					}
					GLOBAL.scheduleShippingCheck[rows[x].shopstyx_transaction_id] = [];
					//process.emit('Shopstyx:logme',"setting job for buyer protection at "+conv.convertJSToMySQL(protection_date)+"for "+cs_users_orders.shopstyx_transaction_id);
					GLOBAL.scheduleShippingCheck[rows[x].shopstyx_transaction_id] = schedule.scheduleJob(protection_date,completeOrder.bind(null,rows[x].shopstyx_transaction_id));
				}else{
					//process.emit('Shopstyx:logme','calling for completeOrder');
					setImmediate(completeOrder,rows[x].shopstyx_transaction_id);
				}
			}
		}else{
			var msg = {
				"status":"Background Error",
				"desc":"[process_group_mail]Possible Error in MySQL",
				"err":err,
				"query":query
			}
			process.emit('Shopstyx:errormail',msg);
		}
	});
}

function completeOrder(shopstyx_transaction_id)
{
	try{
		//process.emit('Shopstyx:logme','trying to delete Global Array');
		if(typeof(GLOBAL.scheduleShippingCheck[shopstyx_transaction_id])!='undefined')
		{
			//process.emit('Shopstyx:logme','deleting Global Array');
			GLOBAL.scheduleShippingCheck[cs_users_orders.shopstyx_transaction_id].cancel();
			delete GLOBAL.scheduleShippingCheck[shopstyx_transaction_id];
		}else{
			//process.emit('Shopstyx:logme','Nothing to delete');
		}
	}catch(err){
		//do nothing
		//process.emit('Shopstyx:logme','NO DELETE, moving forward');
	}
	//check if status is still shipped
	var query = "SELECT * FROM cs_users_orders WHERE shopstyx_transaction_id='"+shopstyx_transaction_id+"';";
	//process.emit('Shopstyx:logme','query :'+query);
	GLOBAL.db_cs_common.query(query,function(err,rows, fields){
		if(err==null)
		{
			if(rows[0].overall_transaction_status.toLowerCase()=='shipped')
			{
				// var postConfig = {
				// 	"url":'',
				// 	"form":{
				// 		"shopstyx_transaction_id":shopstyx_transaction_id,
				// 		"overall_transaction_status":"Completed"
				// 	}
				// };
				// request.post(postConfig, function(err,httpResponse,body){ 
				// 	if(err==null)
				// 	{
				// 		//do nothing
				// 	}else{
				// 		var msg = {
				// 			"status":"Background Error",
				// 			"desc":"[completeOrder]Possible Error in POSTING request for update",
				// 			"err":err,
				// 			"postConfig":postConfig
				// 		}
				// 		process.emit('Shopstyx:errormail',msg);
				// 		//if error retry action after 10 minutes
				// 		setTimeout(completeOrder, 10 * 60 * 1000,shopstyx_transaction_id);
				// 	}
				// });
				//process.emit('Shopstyx:logme','calling PHP URL');
				process.emit('Shopstyx:logme',"triggering job for setBuyerProtection "+shopstyx_transaction_id);
				process.emit('Shopstyx:callPHPAPI',shopstyx_transaction_id);
			}
		}else{
			process.emit('Shopstyx:logme','err :');
			process.emit('Shopstyx:logme',err);
		}
	});
}

//buyer protection expiry reminder functions
function checkScheduleForExpiryReminder(cs_users_orders,protection_days)
{
	var extra_days = parseInt(cs_users_orders.extra_days);
	var purchase_date = new Date(cs_users_orders.purchase_date * 1000);
	var protection_date = new Date(cs_users_orders.purchase_date * 1000);
	protection_date.setDate(purchase_date.getDate() + protection_days + extra_days);
	var now = new Date();
	//check if protection_date is still greater than now
	var protection_date_unix = Math.floor(protection_date.getTime() / 1000);
	var now_unix = Math.floor(now.getTime()/1000);
	var target_mailing_date = new Date(cs_users_orders.purchase_date * 1000);
	target_mailing_date.setDate(purchase_date.getDate() + ((protection_days + extra_days)-4));
	if((now>target_mailing_date && now<protection_date))
	{
		getTargetInfo(cs_users_orders,protection_days)
	}
}

function getTargetInfo(cs_users_orders_data,protection_days)
{
	var query = "SELECT * FROM cs_users_orders WHERE (overall_transaction_status LIKE 'Shipped') AND user_id="+cs_users_orders_data.user_id+" AND shopstyx_order_id='"+cs_users_orders_data.shopstyx_order_id+"'  ORDER BY purchase_date;";
	//javascript date = new Date(unix_timestamp*1000);
	GLOBAL.db_cs_common.query(query,function(err,rows, fields){
		if(err==null)
		{
			var request_msg = {
				"target_information":{
					"firstname":'',
					"lastname":'',
					"username":'',
					"email":'',
					"user_id":''
				},
				"transaction_info":[]
			};
			for(var x=0;x<rows.length;x++)
			{
				//console.log("pushing data to transaction_info");
				request_msg.transaction_info.push(rows[x]);//.shopstyx_transaction_id);
			}
			var target_user_id=0;
			var target_user_id=0;
			// if(parseInt(cs_users_orders_data.store_user_id)==0)
			// {
			// 	target_user_id=parseInt(cs_users_orders_data.store_claimed_by);
			// }else{
			// 	target_user_id=parseInt(cs_users_orders_data.store_user_id);
			// }
			target_user_id=cs_users_orders_data.user_id;
			//setImmediate(getMerchantInfo,cs_users_orders_data,target_user_id,request_msg);
			//process.emit('Shopstyx:sendReminder',cs_users_orders_data,target_user_id,request_msg,GLOBAL.url+'webservices/sendmail/sendshippedreminder/',scheduleTimeTrigger);
			emailReminder.getTargetInfo(cs_users_orders_data,target_user_id,request_msg,GLOBAL.url+'webservices/sendmail/buyerprotectionexpiration/',scheduleTimeTrigger);
		}else{
			var msg = {
				"status":"Background Error",
				"desc":"[process_group_mail]Possible Error in MySQL",
				"err":err,
				"query":query
			}
			process.emit('Shopstyx:errormail',msg);
		}
	});
}
function scheduleTimeTrigger(cs_users_orders_data,postConfig,query)
{
	targetDate=new Date();
	//Date.setHours(hour,min,sec,millisec)
	targetDate.setHours(9,0,0,0);
	console.log('targetDate '+targetDate);
	emailReminder.postEmail(postConfig,query);

}
//end reminder functions

module.exports={
	start:start
};