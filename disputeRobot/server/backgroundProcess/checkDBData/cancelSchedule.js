
var start = function(rows)
{
	try{
		if(rows.length>0)
		{
			for(var x=0;x<rows.length;x++)
			{
				if(typeof(GLOBAL.scheduleShippingCheck[rows[x].shopstyx_transaction_id])!='undefined')
				{
					setImmediate(cancelSchedule,rows[x]);
				}
			}
		}
	}catch(err){
		//no need to report error here
		console.log("Error in scheduleShippingCheck")
		console.log(err);
	}
};

function cancelSchedule(cs_users_orders)
{
	GLOBAL.scheduleShippingCheck[cs_users_orders.shopstyx_transaction_id].cancel();
	process.emit('Shopstyx:logme',"cancelling job for "+cs_users_orders.shopstyx_transaction_id);
	delete GLOBAL.scheduleShippingCheck[cs_users_orders.shopstyx_transaction_id];
}

module.exports={
	start:start
};