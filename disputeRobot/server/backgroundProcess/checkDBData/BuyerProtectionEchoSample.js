var request = require('request');
var conv = require('../../../mysql/mysql_convertions');
var sqlConnect = require('../../../mysql/db_conf');
var	util = require('util');
var emailReminder = require('../../../email/postEmail');

var db_init = {
	host     : '104.239.220.252',
	user     : 'mymugzy',
	password : 'mymugzy!2009',
	database : ''
};
sqlConnect.mysqlConnect(db_init);

function step4()
{
	//check for Shipped
	query = "SELECT * FROM cs_users_orders WHERE overall_transaction_status LIKE 'Shipped' AND store_user_id = 8 GROUP BY shopstyx_order_id,user_id ORDER BY purchase_date ASC;";
	GLOBAL.db_cs_common.query(query,function(err,rows, fields){
		if(err==null)
		{
			start(rows);

		}else{
			var msg = {
				"status":"Background Error",
				"desc":"[checkBuyerProtection]Possible Error in MySQL",
				"err":err,
				"query":query
			}
			process.emit('Shopstyx:errormail',msg);
		}
	});
}

var start = function(rows)
{
	try{
		var query = "SELECT * FROM cs_settings WHERE `group`='order' AND `key`='order_refund_addtional_days'";
		GLOBAL.db_cs_common.query(query,function(err,datarows, fields){
			if(err==null)
			{
				var protection_days = parseInt(datarows[0].value);
				process.emit('Shopstyx:logme','protection_datys:'+protection_days);
				process.emit('Shopstyx:logme','rows.length: '+rows.length);
				if(rows.length>0)
				{
					for(var x=0;x<rows.length;x++)
					{
						// if(typeof(GLOBAL.scheduleShippingCheck[rows[x].shopstyx_transaction_id])!='undefined')
						// {
						// 	process.emit('Shopstyx:logme','setSchedule: '+rows[x].shopstyx_transaction_id);
						// 	setImmediate(setSchedule,rows[x],protection_days);
						// }else{
						// 	// process.emit('Shopstyx:logme','NO setSchedule: '+rows[x].shopstyx_transaction_id);
						// 	// process.emit('Shopstyx:logme','Order Details:');
						// 	// process.emit('Shopstyx:logme','purchase_date: '+rows[x].purchase_date);
						// 	// process.emit('Shopstyx:logme','purchase_date_simple: '+rows[x].purchase_date_simple);
						// 	// process.emit('Shopstyx:logme','extra_days: '+rows[x].extra_days);
						// 	// process.emit('Shopstyx:logme','item #: '+x);
						// 	setImmediate(setSchedule,rows[x],protection_days);
						// }
						//setImmediate(setSchedule,rows[x],protection_days);
						setImmediate(checkScheduleForExpiryReminder,rows[x],protection_days);
					}
				}
			}
		});
		
	}catch(err){
		//no need to report error here
		console.log("Error in scheduleShippingCheck")
		console.log(err);
	}
};

//buyer protection expiry reminder functions
function checkScheduleForExpiryReminder(cs_users_orders,protection_days)
{
	// var extra_days = parseInt(cs_users_orders.extra_days);
	// var purchase_date = new Date(cs_users_orders.purchase_date * 1000);
	// var protection_date = new Date(cs_users_orders.purchase_date * 1000);
	// protection_date.setDate(purchase_date.getDate() + protection_days + extra_days);
	// var now = new Date();
	// //check if protection_date is still greater than now
	// var protection_date_unix = Math.floor(protection_date.getTime() / 1000);
	// var now_unix = Math.floor(now.getTime()/1000);
	// var target_mailing_date = new Date(cs_users_orders.purchase_date * 1000);
	// target_mailing_date.setDate(purchase_date.getDate() + ((protection_days + extra_days)-4));
	// if((now>target_mailing_date && now<protection_date))
	// {
		getTargetInfo(cs_users_orders,protection_days)
	// }
}

function getTargetInfo(cs_users_orders_data,protection_days)
{
	var query = "SELECT * FROM cs_users_orders WHERE (overall_transaction_status LIKE 'Shipped') AND user_id="+cs_users_orders_data.user_id+" AND shopstyx_order_id='"+cs_users_orders_data.shopstyx_order_id+"'  ORDER BY purchase_date;";
	//javascript date = new Date(unix_timestamp*1000);
	GLOBAL.db_cs_common.query(query,function(err,rows, fields){
		if(err==null)
		{
			var request_msg = {
				"target_information":{
					"firstname":'',
					"lastname":'',
					"username":'',
					"email":'',
					"user_id":''
				},
				"transaction_info":[]
			};
			for(var x=0;x<rows.length;x++)
			{
				//console.log("pushing data to transaction_info");
				request_msg.transaction_info.push(rows[x]);//.shopstyx_transaction_id);
			}
			var target_user_id=0;
			// if(parseInt(cs_users_orders_data.store_user_id)==0)
			// {
			// 	target_user_id=parseInt(cs_users_orders_data.store_claimed_by);
			// }else{
			// 	target_user_id=parseInt(cs_users_orders_data.store_user_id);
			// }
			target_user_id=cs_users_orders_data.user_id;
			//setImmediate(getMerchantInfo,cs_users_orders_data,target_user_id,request_msg);
			//process.emit('Shopstyx:sendReminder',cs_users_orders_data,target_user_id,request_msg,GLOBAL.url+'webservices/sendmail/sendshippedreminder/',scheduleTimeTrigger);
			emailReminder.getTargetInfo(cs_users_orders_data,target_user_id,request_msg,GLOBAL.url+'webservices/sendmail/buyerprotectionexpiration/',scheduleTimeTrigger);
		}else{
			var msg = {
				"status":"Background Error",
				"desc":"[process_group_mail]Possible Error in MySQL",
				"err":err,
				"query":query
			}
			process.emit('Shopstyx:errormail',msg);
		}
	});
}
function scheduleTimeTrigger(cs_users_orders_data,postConfig,query)
{
	targetDate=new Date();
	//Date.setHours(hour,min,sec,millisec)
	targetDate.setHours(9,0,0,0);
	console.log('targetDate '+targetDate);
	emailReminder.postEmail(postConfig,query);

}
//end reminder functions

step4();