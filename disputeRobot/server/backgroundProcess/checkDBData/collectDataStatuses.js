//var time = require('time');
var request = require('request');
var conv = require('../../../mysql/mysql_convertions');
var os = require('os');
var mongojs = require('mongojs');
var schedule = require('node-schedule');
var emailReminder = require('../../../email/postEmail');

var checkStatuses = function()
{
	//check if interface information 
	var found = false;
	var interfaces = os.networkInterfaces();
	var addresses = [];
	if(typeof(GLOBAL.scheduleShippingCheck)=='undefined')
	{
		GLOBAL.scheduleShippingCheck = [];
	}
	if(typeof(GLOBAL.disputedItems)=='undefined')
	{
		GLOBAL.disputedItems = [];
	}
	if(typeof(GLOBAL.scheduleShippingReminder)=='undefined')
	{
		GLOBAL.scheduleShippingReminder = [];
	}
	
	for (var k in interfaces) {
	    for (var k2 in interfaces[k]) {
	        var address = interfaces[k][k2];
	        if (address.family === 'IPv4' && !address.internal) {
	            //addresses.push(address.address);
	            var min = 1;
				var max = 20;
				if(process.env.NODE_ENV.toLowerCase()=='development')
				{
					startvalidateToCheck(address.address);
				}else{
					var delay = Math.floor(Math.random() * (max - min + 1)) + min;
					setTimeout(startvalidateToCheck,delay*1000,address.address);
				}
				
	            found = true;
	            break;
	        }
	    }
	    if(found==true)
	    {
	    	break;
	    }
	}

};

function startvalidateToCheck(ipAddress){

	GLOBAL.mongodbTransaction.robotCoordination.find({}).limit(1,function(err,docs){
		if(err==null){
			//2015-08-05 22:45:26
			var date = new Date();
			date.setHours(0,0,0,0);
			var date_string = conv.convertJSToMySQL(date);
			var dateArray = date_string.split(" ");
			var dateData = (dateArray[0]).toString();

			var struct = {
				"ip":ipAddress,
				"date":dateData
			};
			// console.log("docs:");
			// console.log(docs);
			// process.emit('Shopstyx:logme',"robotCoordination:");
			// process.emit('Shopstyx:logme',docs);
			// process.emit('Shopstyx:logme',"--------- END ----------");
			if(docs.length<1)
			{
				// console.log('Inserting struct');
				process.emit('Shopstyx:logme',"inserting struct");
				GLOBAL.mongodbTransaction.robotCoordination.insert(struct);
				setImmediate(startCheck);
			}else{
				console.log('cheking date');
				if(dateData!=docs[0].date || docs[0].ip==ipAddress)
				{
					process.emit('Shopstyx:logme',"win by:");
					process.emit('Shopstyx:logme',"date: "+dateData+" === "+docs[0].date);
					process.emit('Shopstyx:logme',"address: "+ipAddress+" === "+docs[0].ip);
					setImmediate(daugKo,struct,docs);
				}else{
					//check if logged server is alive

					//setTimeout(pingServer, 60000,docs[0].ip);
					setImmediate(pingServer,docs[0].ip,struct,docs)
				}
			}
		}else{
			var min = 1;
			var max = 20;
			var delay = Math.floor(Math.random() * (max - min + 1)) + min;
			setTimeout(startvalidateToCheck,delay*1000,ipAddress);
		}
	});
}
function pingServer(ipAddress,struct,docs)
{
	var checkServerUrl = "http://"+ipAddress+":8769/api/disputerobot/alive";
	process.emit('Shopstyx:logme',"checking: "+checkServerUrl);
	request(checkServerUrl, function (error, response, body) {
		if (!error && response.statusCode == 200) 
		{
			console.log(body) // Show the HTML for the Google homepage.
			if(typeof(body)!='object')
			{
				body=JSON.parse(body);
			}
			if(body.alive!=true)
			{
				process.emit('Shopstyx:logme',"win by alive");
				process.emit('Shopstyx:logme',"dead "+checkServerUrl);
				//process.emit('Shopstyx:logme',"win by alive:" + res.alive);
				//process.emit('Shopstyx:logme',res);
				setImmediate(daugKo,struct,docs);
			}else{
				skipko(ipAddress,struct,docs);
			}
		}else{
			skipko(ipAddress,struct,docs);
		}
	});
}
function skipko(ipAddress,struct,docs)
{
	process.emit('Shopstyx:logme',">>>>>>>>>>>>SKIPPING Process at time "+conv.convertJSToMySQL(new Date())+" <<<<<<<<<<<<<<");
	deleteObjectSchedule(GLOBAL.scheduleShippingCheck);
	deleteObjectSchedule(GLOBAL.disputedItems);
	deleteObjectSchedule(GLOBAL.scheduleShippingReminder);
	//schedule to checkServer next Hour
	setTimeout(pingServer, 60000,ipAddress,struct,docs);
}
function daugKo(struct,docs)
{
	process.emit('Shopstyx:logme',"Updating struct");
	var updateQ = {
		"_id":mongojs.ObjectId(docs[0]._id.toString())
	};
	console.log(updateQ);
	GLOBAL.mongodbTransaction.robotCoordination.update(updateQ,struct,function(err,docs){
		if(err==null)
		{	
			//console.log("finished update");
			startCheck();
		}else{
			console.log(err);
		}
	});
}
function deleteObjectSchedule(objectSched)
{
	for(var key in objectSched)
	{
		try{
			objectSched.cancel();	
		}catch(err){
			//do nothing
			process.emit('Shopstyx:logme','Error cancelling job: '+key);
			process.emit('Shopstyx:logme',err);
		}
	}
	try{
		delete objectSched;
	}catch(err){

	}
}
function startCheck(){

	process.emit('Shopstyx:logme',">>>>>>>>>>>>Check Process at time "+conv.convertJSToMySQL(new Date())+" <<<<<<<<<<<<<<");
	
	// if(conv.checkTimeTarget(9)==true)
	// {
		setImmediate(step1);
	// }
	setImmediate(step2);
	setImmediate(step3);
	//setTimeout(setImmediate,5000,step3);
	setImmediate(step4);
	setImmediate(step5);	
}

function step1()
{
	//find shipped in overall_transaction_status
	//var query = "SELECT * FROM cs_users_orders WHERE overall_transaction_status LIKE 'Pending' ORDER BY purchase_date ASC";
	//find out purchase reminders
	//email reminders for shipping
	var query = "SELECT * FROM cs_users_orders WHERE overall_transaction_status LIKE 'pending' OR overall_transaction_status LIKE 'PaymentSuccessful' GROUP BY store_id,store_user_id,store_claimed_by ORDER BY purchase_date ASC;";
	//javascript date = new Date(unix_timestamp*1000);
	GLOBAL.db_cs_common.query(query,function(err,rows, fields){
		if(err==null)
		{
			for(var x=0;x<rows.length;x++)
			{
				// //get all shopstyx_transaction_id
				// setImmediate(process_transaction_id,row[x],setShippingReminderEmail);
				//get all store_id,store_user_id,store_claimed_by
				setImmediate(process_group_mail,rows[x]);

			}
			
		}else{
			var msg = {
				"status":"Background Error",
				"desc":"[checkStatuses]Possible Error in MySQL",
				"err":err,
				"query":query
			}
			process.emit('Shopstyx:errormail',msg);
		}
	});
}
function step2()
{
	//Check for pending or PaymentSuccessful
	query = "SELECT * FROM cs_users_orders WHERE overall_transaction_status LIKE 'pending' OR overall_transaction_status LIKE 'PaymentSuccessful' ORDER BY purchase_date ASC;";
	//javascript date = new Date(unix_timestamp*1000);
	GLOBAL.db_cs_common.query(query,function(err,rows, fields){
		if(err==null)
		{
			process.emit('Shopstyx:scheduleShippingCheck',rows);

		}else{
			var msg = {
				"status":"Background Error",
				"desc":"[checkStatuses]Possible Error in MySQL",
				"err":err,
				"query":query
			}
			process.emit('Shopstyx:errormail',msg);
		}
	});
}	
function step3()
{
	//check for PaymentFailed or Void to cancel Schedules
	query = "SELECT * FROM cs_users_orders WHERE overall_transaction_status LIKE 'PaymentFailed' OR overall_transaction_status LIKE 'Void' ORDER BY purchase_date ASC;";
	GLOBAL.db_cs_common.query(query,function(err,rows, fields){
		if(err==null)
		{
			process.emit('Shopstyx:cancelSchedules',rows);
			
		}else{
			var msg = {
				"status":"Background Error",
				"desc":"[checkStatuses]Possible Error in MySQL",
				"err":err,
				"query":query
			}
			process.emit('Shopstyx:errormail',msg);
		}
	});
}
function step4()
{
	//check for Shipped
	query = "SELECT * FROM cs_users_orders WHERE overall_transaction_status LIKE 'Shipped' GROUP BY shopstyx_order_id,user_id ORDER BY purchase_date ASC;";
	GLOBAL.db_cs_common.query(query,function(err,rows, fields){
		if(err==null)
		{
			process.emit('Shopstyx:logme','procesing shipped: '+rows.length+' item(s)');
			process.emit('Shopstyx:setBuyerProtection',rows);

		}else{
			var msg = {
				"status":"Background Error",
				"desc":"[checkBuyerProtection]Possible Error in MySQL",
				"err":err,
				"query":query
			}
			process.emit('Shopstyx:errormail',msg);
		}
	});
}
function step5()
{
	//check for Disputed
	query = "SELECT * FROM cs_users_orders WHERE overall_transaction_status LIKE 'Disputed' ORDER BY purchase_date ASC;";
	GLOBAL.db_cs_common.query(query,function(err,rows, fields){
		if(err==null)
		{
			process.emit('Shopstyx:setAutoResolve',rows);
			
		}else{
			var msg = {
				"status":"Background Error",
				"desc":"[checkBuyerProtection]Possible Error in MySQL",
				"err":err,
				"query":query
			}
			process.emit('Shopstyx:errormail',msg);
		}
	});
}

function process_group_mail(cs_users_orders_data)
{
	var query = "SELECT * FROM cs_users_orders WHERE (overall_transaction_status LIKE 'pending' OR overall_transaction_status LIKE 'PaymentSuccessful') AND store_id="+cs_users_orders_data.store_id+" AND store_user_id="+cs_users_orders_data.store_user_id+" AND store_claimed_by="+cs_users_orders_data.store_claimed_by+" ORDER BY purchase_date ASC LIMIT 4;";
	//javascript date = new Date(unix_timestamp*1000);
	GLOBAL.db_cs_common.query(query,function(err,rows, fields){
		if(err==null)
		{
			var request_msg = {
				"target_information":{
					"firstname":'',
					"lastname":'',
					"username":'',
					"email":'',
					"user_id":''
				},
				"transaction_info":[]
			};
			for(var x=0;x<rows.length;x++)
			{
				//console.log("pushing data to transaction_info");
				request_msg.transaction_info.push(rows[x]);//.shopstyx_transaction_id);
			}
			var target_user_id=0;
			if(parseInt(cs_users_orders_data.store_user_id)==0)
			{
				target_user_id=parseInt(cs_users_orders_data.store_claimed_by);
			}else{
				target_user_id=parseInt(cs_users_orders_data.store_user_id);
			}
			//setImmediate(getMerchantInfo,cs_users_orders_data,target_user_id,request_msg);
			//process.emit('Shopstyx:sendReminder',cs_users_orders_data,target_user_id,request_msg,GLOBAL.url+'webservices/sendmail/sendshippedreminder/',scheduleTimeTrigger);
			emailReminder.getTargetInfo(cs_users_orders_data,target_user_id,request_msg,GLOBAL.url+'webservices/sendmail/shippedreminder/',scheduleTimeTrigger);
		}else{
			var msg = {
				"status":"Background Error",
				"desc":"[process_group_mail]Possible Error in MySQL",
				"err":err,
				"query":query
			}
			process.emit('Shopstyx:errormail',msg);
		}
	});
}

function scheduleTimeTrigger(cs_users_orders_data,postConfig,query)
{
	targetDate=new Date();
	//Date.setHours(hour,min,sec,millisec)
	targetDate.setHours(9,0,0,0);
	if(typeof(GLOBAL.scheduleShippingReminder)=='undefined')
	{
		GLOBAL.scheduleShippingReminder = [];
	}
	GLOBAL.scheduleShippingReminder[cs_users_orders_data.shopstyx_transaction_id] = schedule.scheduleJob(targetDate,emailReminder.postEmail.bind(null,postConfig,query));
}

// function getMerchantInfo(cs_users_orders_data,target_user_id,request_msg)
// {
// 	var query = "SELECT user_id,username,firstname,lastname,email FROM cs_users WHERE user_id="+target_user_id+";";
// 	GLOBAL.db_cs_common.query(query,function(err,rows, fields){
// 		if(err==null)
// 		{
// 			request_msg.target_information.firstname=rows[0].firstname;
// 			request_msg.target_information.lastname=rows[0].lastname;
// 			request_msg.target_information.username=rows[0].username;
// 			request_msg.target_information.email=rows[0].email;
// 			request_msg.target_information.user_id=rows[0].user_id;

// 			var sigQuery = 'SELECT * FROM cs_settings WHERE `group` LIKE "api" AND `key` LIKE "webservice_signature"';
// 			GLOBAL.db_cs_common.query(sigQuery,function(errQ,rowsQ, fieldsQ){
// 				if(errQ==null)
// 				{
// 					var signature = rowsQ[0].value;
// 					//var url = GLOBAL.url+"webservices/order/updates/"+token+"/"+signature+"/"+user_id+"/"+shopstyx_transaction_id+"/";
					
// 					//send to Ferdie's API
// 					//http://shopstyx.local/webservices/sendmail/buyerprotectionexpiration
// 					var postConfig = {
// 						"url":GLOBAL.url+'webservices/sendmail/sendshippedreminder/'+signature,
// 						"form":{
// 							"data":request_msg
// 						}
// 					};
// 					//console.log("calling url: "+url);
// 					process.emit('Shopstyx:logme',"email Reminder for Shipment: at time "+conv.convertJSToMySQL(new Date()));
// 					process.emit('Shopstyx:logme',"target_information:");
// 					process.emit('Shopstyx:logme',request_msg.target_information);
					
// 					targetDate=new Date();
// 					//Date.setHours(hour,min,sec,millisec)
// 					targetDate.setHours(9,0,0,0);
// 					GLOBAL.scheduleShippingReminder[cs_users_orders_data.shopstyx_transaction_id] = schedule.scheduleJob(targetDate,postEmail.bind(null,postConfig,query));
// 				}else{
// 					setTimeout(sendRequest, 10 * 60 * 1000,shopstyx_transaction_id);
// 				}
// 			});
			

// 		}else{
// 			var msg = {
// 				"status":"Background Error",
// 				"desc":"[getMerchantInfo]Possible Error in MySQL",
// 				"err":err,
// 				"query":query
// 			}
// 			process.emit('Shopstyx:errormail',msg);
// 		}
// 	});
// }

// function postEmail(postConfig,query)
// {
// 	request.post(postConfig, function(err,httpResponse,body){ 
// 		if(err==null)
// 		{
// 			//do nothing
// 		}else{
// 			var msg = {
// 				"status":"Background Error",
// 				"desc":"[getMerchantInfo]Possible Error in POSTING request for email",
// 				"err":err,
// 				"query":query
// 			}
// 			process.emit('Shopstyx:errormail',msg);
// 		}
// 	});
// }
module.exports = {
	checkStatuses:checkStatuses
};