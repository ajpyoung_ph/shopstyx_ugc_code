var request = require('request');
var conv = require('../../../mysql/mysql_convertions');
var sqlConnect = require('../../../mysql/db_conf');
var	util = require('util');
var emailReminder = require('../../../email/postEmail');

var db_init = {
	host     : '104.239.220.252',
	user     : 'mymugzy',
	password : 'mymugzy!2009',
	database : ''
};
sqlConnect.mysqlConnect(db_init);

function step5()
{
	//check for Disputed
	query = "SELECT * FROM cs_users_orders WHERE overall_transaction_status LIKE 'Disputed' ORDER BY purchase_date ASC;";
	GLOBAL.db_cs_common.query(query,function(err,rows, fields){
		if(err==null)
		{
			start(rows);
			
		}else{
			var msg = {
				"status":"Background Error",
				"desc":"[checkBuyerProtection]Possible Error in MySQL",
				"err":err,
				"query":query
			}
			process.emit('Shopstyx:errormail',msg);
		}
	});
}

var start = function(rows)
{
    try{
    	var query = "SELECT * FROM cs_settings WHERE `group`='order' AND `key`='merchant_dispute_respond_days'";
		GLOBAL.db_cs_common.query(query,function(err,datarows, fields){
			if(err==null)
			{
				var reply_days = parseInt(datarows[0].value);
				if(rows.length>0)
		        {
		            for(var x=0;x<rows.length;x++)
		            {
		                //setImmediate(checkPerItemDispute,rows[x],reply_days);
		                setImmediate(checkDisputeExpiry,rows[x],reply_days);
		            }
		        }
			}
		});
        
    }catch(err){
        //no need to report error here
        console.log("Error in setAutoResolve")
        console.log(err);
    }
};

function checkPerItemDispute(cs_users_orders,reply_days)
{
    var query = {
        "transaction_info.bought_items.shopstyx_transaction_id" : cs_users_orders.shopstyx_transaction_id
    };
    GLOBAL.mongodbTransaction.transactionRecords.find(query,function(err,docs){
        if(err==null)
        {
        	if(docs.length>0)
        	{
        		 var bought_items = docs[0].transaction_info.bought_items;
	            for(var x = 0;x<bought_items.length;x++)
	            {
	                if(cs_users_orders.shopstyx_transaction_id==bought_items[x].shopstyx_transaction_id)
	                {
	                    setImmediate(check_item_purchased,bought_items[x],cs_users_orders,reply_days);
	                    break;
	                }
	            }
        	}else{
        		process.emit('Shopstyx:errormail',"ID does not exist\n"+query);
        	}
           
        }
    });
}

function check_item_purchased(bought_items,cs_users_orders,reply_days)
{
	for(var x = 0;x<bought_items.item_purchased.length;x++)
	{
		var item_purchased = bought_items.item_purchased[x];
		var unique_id = cs_users_orders.shopstyx_transaction_id+"-"+item_purchased.product_id+"-"+item_purchased.variant_id;
		if(item_purchased.order_status.status_state.toLowerCase() == 'disputed')
		{
			if(typeof(GLOBAL.disputedItems) == 'undefined')
			{
				GLOBAL.disputedItems=[];
			}
			if(typeof(GLOBAL.disputedItems[unique_id])=='undefined')
			{
				var dispute_date = new Date(item_purchased.order_status.order_disputed_date * 1000);
				var reply_date = new Date(item_purchased.order_status.order_disputed_date * 1000);
				reply_date.setDate(dispute_date.getDate() + reply_days);
				var now = new Date();
				
				//check if reply_date is still greater than now
				var reply_date_unix = Math.floor(reply_date.getTime() / 1000);
				var now_unix = Math.floor(now.getTime()/1000);
				if(reply_date_unix > now_unix)
				{
					GLOBAL.disputedItems[unique_id]=[];
					process.emit('Shopstyx:logme',"setting job for autoResolve at "+conv.convertJSToMySQL(reply_date)+"for "+cs_users_orders.shopstyx_transaction_id);
					GLOBAL.disputedItems[unique_id]=schedule.scheduleJob(reply_date,autoResolve.bind(null,cs_users_orders.shopstyx_transaction_id,unique_id,item_purchased.product_id,item_purchased.variant_id));
				}else{
					setImmediate(autoResolve,cs_users_orders.shopstyx_transaction_id,unique_id,item_purchased.product_id,item_purchased.variant_id)
				}
				
			}
		}
	}
}

function autoResolve(shopstyx_transaction_id,unique_id,product_id,variant_id)
{
	try{
		GLOBAL.disputedItems[unique_id].cancel();
		delete GLOBAL.disputedItems[unique_id];
	}catch(err){
		//do nothing
	}
	//check if status is still disputed
	var query = "SELECT * FROM cs_users_orders WHERE shopstyx_transaction_id='"+shopstyx_transaction_id+"';";
	GLOBAL.db_cs_common.query(query,function(err,rows, fields){
		if(err==null)
		{
			if(rows[0].overall_transaction_status.toLowerCase()=='disputed')
			{
				// var postConfig = {
				// 	"url":'',
				// 	"form":{
				// 		"shopstyx_transaction_id":shopstyx_transaction_id,
				// 		"overall_transaction_status":"Refunded",
				// 		"product_id":parseInt(product_id),
				// 		"variant_id":parseInt(variant_id)
				// 	}
				// };
				// request.post(postConfig, function(err,httpResponse,body){ 
				// 	if(err==null)
				// 	{
				// 		//do nothing
				// 	}else{
				// 		var msg = {
				// 			"status":"Background Error",
				// 			"desc":"[autoResolve]Possible Error in POSTING request for update",
				// 			"err":err,
				// 			"postConfig":postConfig
				// 		}
				// 		process.emit('Shopstyx:errormail',msg);
				// 		//if error retry action after 10 minutes
				// 		setTimeout(autoResolve, 10 * 60 * 1000,shopstyx_transaction_id,unique_id,product_id,variant_id);
				// 	}
				// });
				process.emit('Shopstyx:callPHPAPI',shopstyx_transaction_id);
			}
		}
	});	
}

//email functions start here
function checkDisputeExpiry(cs_users_orders,reply_days)
{
	//var extra_days = parseInt(cs_users_orders.extra_days);
	var purchase_date = new Date(cs_users_orders.purchase_date * 1000);
	var protection_date = new Date(cs_users_orders.purchase_date * 1000);
	protection_date.setDate(purchase_date.getDate() + reply_days);// + extra_days);
	var now = new Date();
	//check if protection_date is still greater than now
	var protection_date_unix = Math.floor(protection_date.getTime() / 1000);
	var now_unix = Math.floor(now.getTime()/1000);
	var target_mailing_date = new Date(cs_users_orders.purchase_date * 1000);
	target_mailing_date.setDate(purchase_date.getDate() + ((reply_days)-1));
	if((now>target_mailing_date && now<protection_date))
	{
		setImmediate(getTransactionInfo,cs_users_orders,reply_days)
	}
}

function getTransactionInfo(cs_users_orders_data,reply_days)
{

	var query = {
		"shopstyx_transaction_id":cs_users_orders_data.shopstyx_transaction_id
	};
	GLOBAL.mongodbTransaction.disputeRecords.findOne(query,function(err,doc){
		if(err==null)
		{
			if(doc!=null)
			{
				var request_msg = {
					"target_information":{
						"Seller":{
							"firstname":'',
							"lastname":'',
							"username":'',
							"email":'',
							"user_id":''
						},
						"Buyer":{
							"firstname":'',
							"lastname":'',
							"username":'',
							"email":'',
							"user_id":''
						}
					},
					"transaction_info":[]
				};
				request_msg.transaction_info.push(doc);
				var seller_id=doc.store_user_id;
				var buyer_id=doc.user_id;
				setImmediate(getTargetInfo,cs_users_orders_data,seller_id,buyer_id,request_msg);
			}
		}else{
			var msg = {
				"status":"Background Error",
				"desc":"[getTransactionInfo setAutoResolve]Possible Error in DB",
				"err":err,
				"query":query
			}
			process.emit('Shopstyx:errormail',msg);
		}
	});
}
function getTargetInfo(cs_users_orders_data,seller_id,buyer_id,request_msg)
{
	var querySeller = "SELECT user_id,username,firstname,lastname,email FROM cs_users WHERE user_id="+seller_id+";";
	GLOBAL.db_cs_common.query(querySeller,function(errSeller,rowsSeller, fieldsSeller){
		if(errSeller==null)
		{
			request_msg.target_information.Seller.firstname=rowsSeller[0].firstname;
			request_msg.target_information.Seller.lastname=rowsSeller[0].lastname;
			request_msg.target_information.Seller.username=rowsSeller[0].username;
			request_msg.target_information.Seller.email=rowsSeller[0].email;
			request_msg.target_information.Seller.user_id=rowsSeller[0].user_id;

			var queryBuyer = "SELECT user_id,username,firstname,lastname,email FROM cs_users WHERE user_id="+buyer_id+";";
			GLOBAL.db_cs_common.query(queryBuyer,function(errBuyer,rowsBuyer, fields){
				if(errBuyer==null)
				{
					request_msg.target_information.Buyer.firstname=rowsSeller[0].firstname;
					request_msg.target_information.Buyer.lastname=rowsSeller[0].lastname;
					request_msg.target_information.Buyer.username=rowsSeller[0].username;
					request_msg.target_information.Buyer.email=rowsSeller[0].email;
					request_msg.target_information.Buyer.user_id=rowsSeller[0].user_id;

					var sigQuery = 'SELECT * FROM cs_settings WHERE `group` LIKE "api" AND `key` LIKE "webservice_signature"';
					GLOBAL.db_cs_common.query(sigQuery,function(errQ,rowsQ, fieldsQ){
						if(errQ==null)
						{
							var signature = rowsQ[0].value;
							//var url = GLOBAL.url+"webservices/order/updates/"+token+"/"+signature+"/"+user_id+"/"+shopstyx_transaction_id+"/";
							
							//send to Ferdie's API
							//GLOBAL.url+'webservices/sendmail/buyerprotectionexpiration'
							//GLOBAL.url+'webservices/sendmail/disputeexpiration/'
							var url = GLOBAL.url+'webservices/sendmail/disputeexpiration/';
							var postConfig = {
								"url":url+signature,
								"form":{
									"data":request_msg
								}
							};
							//console.log("calling url: "+url);

							process.emit('Shopstyx:logme',"email Reminder for Shipment: at time "+conv.convertJSToMySQL(new Date()));
							process.emit('Shopstyx:logme',"target_information:");
							process.emit('Shopstyx:logme',request_msg.target_information);
							
							callback(cs_users_orders_data,postConfig,query);
							// targetDate=new Date();
							// //Date.setHours(hour,min,sec,millisec)
							// targetDate.setHours(9,0,0,0);
							// GLOBAL.scheduleShippingReminder[cs_users_orders_data.shopstyx_transaction_id] = schedule.scheduleJob(targetDate,postEmail.bind(null,postConfig,query));
						}else{
							setTimeout(sendRequest, 10 * 60 * 1000,shopstyx_transaction_id);
						}
					});
				}else{
					var msg = {
						"status":"Background Error",
						"desc":"[getTargetInfo setAutoResolve]Possible Error in MySQL",
						"err":errBuyer,
						"query":queryBuyer
					}
					process.emit('Shopstyx:errormail',msg);
				}
			});
		}else{
			var msg = {
				"status":"Background Error",
				"desc":"[getTargetInfo setAutoResolve]Possible Error in MySQL",
				"err":errSeller,
				"query":querySeller
			}
			process.emit('Shopstyx:errormail',msg);
		}
	});
}
function scheduleTimeTrigger(cs_users_orders_data,postConfig,query)
{
	targetDate=new Date();
	//Date.setHours(hour,min,sec,millisec)
	targetDate.setHours(9,0,0,0);
	console.log('targetDate '+targetDate);
	emailReminder.postEmail(postConfig,query);

}

step5();