var schedule = require('node-schedule');
var request = require('request');
var conv = require('../../../mysql/mysql_convertions');

var scheduleShippingCheck = function(rows)
{
	console.log("entering scheduleShippingCheck");
	try{
		if(rows.length>0)
		{
			if(typeof(GLOBAL.scheduleShippingCheck)=='undefined')
			{
				GLOBAL.scheduleShippingCheck = [];
			}
			for(var x=0;x<rows.length;x++)
			{
				if(typeof(GLOBAL.scheduleShippingCheck[rows[x].shopstyx_transaction_id])=='undefined')
				{
					setImmediate(setSchedule,rows[x]);
				}
			}
		}
	}catch(err){
		//no need to report error here
		console.log("Error in scheduleShippingCheck")
		console.log(err);
	}
};

function setSchedule(cs_users_orders)
{
	var query = {
		"transaction_info.bought_items.shopstyx_transaction_id":cs_users_orders.shopstyx_transaction_id
	};

	GLOBAL.mongodbTransaction.transactionRecords.find(query,function(err,docs){
		if(err==null)
		{
			try{
				if(docs.length>0)
				{
					var targetTransaction = docs[0];
					var purchase_date = new Date(parseInt(targetTransaction.transaction_info.purchase_date) * 1000);
					var bought_items='';
					var readytoship_days=0;
					for(var x = 0;x<targetTransaction.transaction_info.bought_items.length;x++)
					{
						bought_items=targetTransaction.transaction_info.bought_items[x];
						if(bought_items.shopstyx_transaction_id==cs_users_orders.shopstyx_transaction_id)
						{
							readytoship_days = bought_items.readytoship.readytoship_days;

							//calculate purchase date plus readytoship_days and schedule trigger for cancel
							var shipping_date = new Date(parseInt(targetTransaction.transaction_info.purchase_date) * 1000);
							shipping_date.setDate(purchase_date.getDate() + parseInt(readytoship_days));
							var now = new Date();
							//check if shipping_date is still greater than now

							var shipping_date_unix = Math.floor(shipping_date.getTime() / 1000);
							var now_unix = Math.floor(now.getTime()/1000);
							if(shipping_date_unix>now_unix)
							{
								if(typeof(GLOBAL.scheduleShippingCheck[bought_items.shopstyx_transaction_id])!='undefined')
								{
									GLOBAL.scheduleShippingCheck[bought_items.shopstyx_transaction_id].cancel();
									delete GLOBAL.scheduleShippingCheck[bought_items.shopstyx_transaction_id];
								}
								GLOBAL.scheduleShippingCheck[bought_items.shopstyx_transaction_id] = [];

								process.emit('Shopstyx:logme',"setting job for shipping action (refund/cancel order) at "+conv.convertJSToMySQL(shipping_date)+"for "+bought_items.shopstyx_transaction_id);
								process.emit('Shopstyx:logme','Order Details:');
								process.emit('Shopstyx:logme','purchase_date: '+targetTransaction.transaction_info.purchase_date);
								process.emit('Shopstyx:logme','purchase_date_simple: '+targetTransaction.transaction_info.purchase_date_simple);
								process.emit('Shopstyx:logme','readytoship_days: '+readytoship_days);
								GLOBAL.scheduleShippingCheck[bought_items.shopstyx_transaction_id] = schedule.scheduleJob(shipping_date,cancelOrder.bind(null,bought_items.shopstyx_transaction_id));
							}else{
								process.emit('Shopstyx:logme',"cancelling job for "+bought_items.shopstyx_transaction_id);
								setImmediate(cancelOrder,bought_items.shopstyx_transaction_id);
							}
							
						}
					}
				}else{
					process.emit('Shopstyx:errormail',"ID Does not Exist: "+query["transaction_info.bought_items.shopstyx_transaction_id"]);
				}
				
			}catch(err){
				console.log('Error!')
				console.log(err);
			}
			
		}
	});
}

function cancelOrder(shopstyx_transaction_id)
{
	try{
		delete GLOBAL.scheduleShippingCheck[shopstyx_transaction_id];	
	}catch(err){
		//do nothing
	}
	process.emit('Shopstyx:logme',"cancelling job for "+shopstyx_transaction_id);
	var query = "SELECT * FROM cs_users_orders WHERE shopstyx_transaction_id='"+shopstyx_transaction_id+"';";
	//javascript date = new Date(unix_timestamp*1000);
	GLOBAL.db_cs_common.query(query,function(err,rows, fields){
		if(err==null)
		{
			
			if(rows.length>0)
			{
				var status = rows[0].overall_transaction_status.toLowerCase();
				if(status=='pending' || status=='paymentsuccessful')
				{
					//call Ferdie
					// var postConfig = {
					// 	"url":'',
					// 	"form":{
					// 		"shopstyx_transaction_id":shopstyx_transaction_id
					// 	}
					// };
					// postConfig.form.overall_transaction_status=[];
					// if(status == 'pending')
					// {
					// 	postConfig.form.overall_transaction_status="Cancel";
					// }else{
					// 	postConfig.form.overall_transaction_status="Refunded";
					// }

					
					// request.post(postConfig, function(err,httpResponse,body){ 
					// 	if(err==null)
					// 	{
					// 		//do nothing
					// 	}else{
					// 		var msg = {
					// 			"status":"Background Error",
					// 			"desc":"[cancelOrder]Possible Error in POSTING request for email",
					// 			"err":err,
					// 			"postConfig":postConfig
					// 		}
					// 		process.emit('Shopstyx:errormail',msg);
					// 		//if error retry action after 10 minutes
					// 		setTimeout(cancelOrder, 10 * 60 * 1000,shopstyx_transaction_id);
					// 	}
					// });
					process.emit('Shopstyx:logme',"triggering job for either cancel/refund for "+shopstyx_transaction_id);
					process.emit('Shopstyx:callPHPAPI',shopstyx_transaction_id);
				}
			}
			
		}else{
			//if error in MySQL retry action after 10 minutes
			setTimeout(cancelOrder, 10 * 60 * 1000,shopstyx_transaction_id);
		}
	});
}
module.exports = {
	scheduleShippingCheck:scheduleShippingCheck
};