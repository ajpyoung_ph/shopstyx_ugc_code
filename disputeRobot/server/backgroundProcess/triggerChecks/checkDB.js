var schedule = require('node-schedule');
var start = function()
{
	switch(process.NODE_ENV.toLowerCase())
	{
		case 'development':
				console.log("Triggering Shipping Reminder")
				process.emit('Shopstyx:checkPending');
		case 'staging':
		default:
				console.log("running production settings for collectPending");
				var pendingStatusRule = new schedule.RecurrenceRule();
				pendingStatusRule.hour = 8;
				pendingStatusRule.minute = 45;
				console.log("calling Shipping Reminder every "+pendingStatusRule.hour+":"+pendingStatusRule.minute);
				var j = schedule.scheduleJob(rule, function(){
					console.log("Triggering Shipping Reminder")
					process.emit('Shopstyx:checkPending');
				});

				break;
	}
};

module.exports = {
	start:start
};