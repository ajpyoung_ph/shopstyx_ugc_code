var fs = require('fs');
var util = require('util');

var logInfo = function(info)
{
	var options = {
		mode : 511
	};
	info = util.inspect(info,{showHidden: false, depth: null});
	if(fs.existsSync(__dirname+"/schedule.log")!=true)
	{
		fs.writeFile(__dirname+"/schedule.log",info+"\n",options);
	}else{
		fs.appendFile(__dirname+"/schedule.log",info+"\n",options);
	}
}

module.exports={
	logInfo:logInfo
};