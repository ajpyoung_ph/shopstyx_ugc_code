var querystring = require('querystring');
var https = require('https');

function callAPI(api_url, method, data, callback) {
	var dataString = JSON.stringify(data);
	var headers = {};

	if (method == 'GET') {
		api_url += '?' + querystring.stringify(data);
	}
	else {
		headers = {
			'Content-Type': 'application/x-www-form-urlencoded',
			'Content-Length': dataString.length
		};
	}
	var options = {
		host: host,
		path: api_url,
		method: method,
		headers: headers
	};

	var req = https.request(options, function(res) {
		res.setEncoding('utf-8');

		var responseString = '';

		res.on('data', function(data) {
			responseString += data;
		});

		res.on('end', function() {
			console.log(responseString);
			var responseObject = JSON.parse(responseString);
			callback(responseObject);
		});
	});

	req.write(dataString);
	req.end();
}

module.exports={
	callAPI:callAPI
};