if(process.env.NODE_ENV!='development' && process.env.NODE_ENV!='localdev' && process.env.NODE_ENV!='staging')
{
    var time = require('time');
}

function twoDigits(d) {
    if(0 <= d && d < 10) return "0" + d.toString();
    if(-10 < d && d < 0) return "-0" + (-1*d).toString();
    return d.toString();
}

var MySQLDateTimeNOWDisplay = function(){
    if(process.env.NODE_ENV!='development' && process.env.NODE_ENV!='localdev' && process.env.NODE_ENV!='staging')
    {
        var d = new time.Date();
        d.setTimezone("America/Los_Angeles");
    }else{
        var d = new Date();
    }

  return d.getFullYear() + "-" + twoDigits(1 + d.getMonth()) + "-" + twoDigits(d.getDate()) + " " + twoDigits(d.getHours()) + ":" + twoDigits(d.getMinutes()) + ":" + twoDigits(d.getSeconds());
};

var MySQLDateTimeNOW = function(dataStruct){
    if(process.env.NODE_ENV!='development' && process.env.NODE_ENV!='localdev' && process.env.NODE_ENV!='staging')
    {
        var d = new time.Date();
        d.setTimezone(dataStruct.timezone);
    }else{
        var d = new Date();
    }
  dataStruct.newUnixDate = Math.floor((d).getTime() / 1000);
	return d.getFullYear() + "-" + twoDigits(1 + d.getMonth()) + "-" + twoDigits(d.getDate()) + " " + twoDigits(d.getHours()) + ":" + twoDigits(d.getMinutes()) + ":" + twoDigits(d.getSeconds());
};

var convertUnixToMySQL = function(unixDate)
{
  var mydate = unixDate * 1000;
  var d = new Date(mydate);
  return d.getFullYear() + "-" + twoDigits(1 + d.getMonth()) + "-" + twoDigits(d.getDate()) + " " + twoDigits(d.getHours()) + ":" + twoDigits(d.getMinutes()) + ":" + twoDigits(d.getSeconds());
}
var convertJSToMySQL = function(jsDate)
{
  //var mydate = unixDate * 1000;
  var d = jsDate;
  return d.getFullYear() + "-" + twoDigits(1 + d.getMonth()) + "-" + twoDigits(d.getDate()) + " " + twoDigits(d.getHours()) + ":" + twoDigits(d.getMinutes()) + ":" + twoDigits(d.getSeconds());
}

var stripslashes=function(str) 
{
  //       discuss at: http://phpjs.org/functions/stripslashes/
  //      original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  //      improved by: Ates Goral (http://magnetiq.com)
  //      improved by: marrtins
  //      improved by: rezna
  //         fixed by: Mick@el
  //      bugfixed by: Onno Marsman
  //      bugfixed by: Brett Zamir (http://brett-zamir.me)
  //         input by: Rick Waldron
  //         input by: Brant Messenger (http://www.brantmessenger.com/)
  // reimplemented by: Brett Zamir (http://brett-zamir.me)
  //        example 1: stripslashes('Kevin\'s code');
  //        returns 1: "Kevin's code"
  //        example 2: stripslashes('Kevin\\\'s code');
  //        returns 2: "Kevin\'s code"

  return (str + '')
    .replace(/\\(.?)/g, function(s, n1) {
      switch (n1) {
        case '\\':
          return '\\';
        case '0':
          return '\u0000';
        case '':
          return '';
        default:
          return n1;
      }
    });
};
var addslashes=function(str) 
{
  //  discuss at: http://phpjs.org/functions/addslashes/
  // original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // improved by: Ates Goral (http://magnetiq.com)
  // improved by: marrtins
  // improved by: Nate
  // improved by: Onno Marsman
  // improved by: Brett Zamir (http://brett-zamir.me)
  // improved by: Oskar Larsson Högfeldt (http://oskar-lh.name/)
  //    input by: Denny Wardhana
  //   example 1: addslashes("kevin's birthday");
  //   returns 1: "kevin\\'s birthday"

  return (str + '')
    .replace(/[\\"']/g, '\\$&')
    .replace(/\u0000/g, '\\0');
};

var mysql_real_escape_string=function(str) 
{
    return str.replace(/[\0\x08\x09\x1a\n\r"'\\\%]/g, function (char) {
        switch (char) {
            case "\0":
                return "\\0";
            case "\x08":
                return "\\b";
            case "\x09":
                return "\\t";
            case "\x1a":
                return "\\z";
            case "\n":
                return "\\n";
            case "\r":
                return "\\r";
            case "\"":
            case "'":
            case "\\":
            case "%":
                return "\\"+char; // prepends a backslash to backslash, percent,
                                  // and double/single quotes
        }
    });
};

var checkTimeTarget= function(MilitaryHour) //MilitaryHour is floored integer between 0-23
{
  var nowDate = new Date();
  if(nowDate.getHours()==parseInt(MilitaryHour))
  {
    return true;
  }
  return false;
}

module.exports = {
  MySQLDateTimeNOWDisplay:MySQLDateTimeNOWDisplay,
	MySQLDateTimeNOW:MySQLDateTimeNOW,
	stripslashes:stripslashes,
	addslashes:addslashes,
	mysql_real_escape_string:mysql_real_escape_string,
  convertUnixToMySQL:convertUnixToMySQL,
  convertJSToMySQL:convertJSToMySQL,
  checkTimeTarget:checkTimeTarget
};