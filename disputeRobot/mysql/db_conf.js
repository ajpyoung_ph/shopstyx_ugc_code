var pmx = require('pmx');
var mysql = require('mysql');
var mytime = require(__dirname+'/mysql_convertions');
var probe = pmx.probe();
 
var db_cs_common_disconnects = probe.counter({
  name : 'GLOBAL.db_cs_common disconnects'
});

var PROTOCOL_CONNECTION_LOST_counter = probe.counter({
  name : 'MySQL PROTOCOL_CONNECTION_LOST'
});
var mysqlConnect = function(db_init){
	// db_init.database = 'cs_search_results';
	// GLOBAL.db_cs_search_results = mysql.createConnection(db_init);
	try{
		GLOBAL.db_cs_common.end();
		console.log("ending MySQL connection");
		db_cs_common_disconnects.inc();
	}catch(err){
		console.log("No MySQL Connection to Close");
	}
	console.log("creating new mysql connection");
	console.log(mytime.MySQLDateTimeNOWDisplay());
	db_init.database = 'cs_common';
	//GLOBAL.db_cs_common = mysql.createConnection(db_init);
	db_init['connectionLimit'] = 100;
	GLOBAL.db_cs_common = mysql.createPool(db_init);


	// GLOBAL.db_cs_common.connect(function(err) {              // The server is either down
 //    if(err) {                                     // or restarting (takes a while sometimes).
	//       console.log('error when connecting to db:', err);
	//       setTimeout(mysqlConnect, 2000, db_init); // We introduce a delay before attempting to reconnect,
	//     }                                     // to avoid a hot loop, and to allow our node script to
	//   });                                     // process asynchronous requests in the meantime.
	//                                           // If you're also serving http, display a 503 error.
	// GLOBAL.db_cs_common.on('error', function(err) {
	// 	console.log('db error', err);
	// 	if(err.code === 'PROTOCOL_CONNECTION_LOST') { // Connection to the MySQL server is usually
	// 		PROTOCOL_CONNECTION_LOST_counter.inc();
	// 		mysqlConnect(db_init);                         // lost due to either server restart, or a
	// 	} else {                                      // connnection idle timeout (the wait_timeout
	// 	  throw err;                                  // server variable configures this)
	// 	}
	// });
}

module.exports = {
	mysqlConnect:mysqlConnect
}