var mongojs = require('mongojs');
var bodyParser = require('body-parser');
var logger = require('morgan'); //logger

var envConf = function(dataStruct)
{
	dataStruct.app.use(dataStruct.express.static(__dirname + '/public'));
	var trailingArg = '';
	switch(process.env.NODE_ENV.toLowerCase())
	{
		case 'office':
		case 'development':
			console.log('setting up '+process.env.NODE_ENV.toLowerCase());
			dataStruct.locationString = "localhost";
			dataStruct.app.use(logger('combined')); 						// log every request to the console
			dataStruct.app.use(bodyParser.json()); 							// parse application/json
			dataStruct.app.use(bodyParser.urlencoded({extended:true}));		// parse application/x-www-form-urlencoded
			dataStruct.db_init = {
				host     : 'localhost',
				user     : 'root',
				password : '',
				database : ''
			};
			if(process.env.NODE_ENV.toLowerCase()=='development')
			{
				GLOBAL.url = "https://localhost/";
			}else{
				GLOBAL.url = "https://shopstyx.office/";
			}
			
			break;
		case 'staging':
			console.log('setting up staging');
			dataStruct.locationString = "localhost";
			dataStruct.app.use(logger('combined')); 								// log every request to the console - default settings
			dataStruct.app.use(bodyParser.json()); 							// parse application/json
			dataStruct.app.use(bodyParser.urlencoded({extended:true}));		// parse application/x-www-form-urlencoded
			dataStruct.db_init = {
				host     : 'localhost',
				user     : 'mymugzy',
				password : 'mymugzy2009',
				database : ''
			};
			GLOBAL.url = "http://vwala.com/";
			break;
		case 'production':
			console.log('setting up production');
			dataStruct.locationString = "104.130.206.240:27017,104.130.162.34:27017,104.239.135.106:27017";
			dataStruct.app.use(logger('combined')); 						// log every request to the console
			dataStruct.app.use(bodyParser.json()); 							// parse application/json
			dataStruct.app.use(bodyParser.urlencoded({extended:true}));		// parse application/x-www-form-urlencoded
			dataStruct.db_init = {
				host     : '104.239.220.252',
				user     : 'mymugzy',
				password : 'mymugzy!2009',
				database : ''
			};
			GLOBAL.url = "https://shopstyx.com/";
			trailingArg='?replicaSet=styx01';
			break;
	}
	dataStruct.port = process.env.PORT || 8769;
	dataStruct.http = "http://"+dataStruct.locationString+":"+dataStruct.port;
	//GLOBAL.mongodbDispute = mongojs('disputeRobotUser:s#0pst!cks@'+dataStruct.locationString+'/shopstyx_disputeRobot'+trailingArg, ['disputeRobotRecords','emailReminderStatus']);
	GLOBAL.mongodbTransaction = mongojs('transactionUser:s#0pst!cks@'+dataStruct.locationString+'/shopstyx_transactions', ['transactionRecords','disputeRecords','robotCoordination','missingTransactionIDLogs'], {authMechanism: 'ScramSHA1'}); 
	require('../routes/node_route')(dataStruct);
	dataStruct.app.all('*', function(req, res, next) {
		// res.header("Access-Control-Allow-Origin", "*");
		// res.header("Access-Control-Allow-Headers", "X-Requested-With");
		// next();
		res.header('Access-Control-Allow-Origin', '*');
	    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
	    res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');

	    // intercept OPTIONS method
	    if ('OPTIONS' == req.method) {
	      res.send(200);
	    }
	    else {
	      next();
	    }
	});
	
	return dataStruct;
}


module.exports = {
	envConf:envConf
};