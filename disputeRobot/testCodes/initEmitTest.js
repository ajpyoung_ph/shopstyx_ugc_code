var testCodeSource = require('./testCodeSource');

var initEmitTest = function()
{
	process.on('Shopstyx:testConsoleLog',testCodeSource.ConsoleLog);
	process.on('Shopstyx:testReq',testCodeSource.replyRes);
}

module.exports = {
	initEmitTest:initEmitTest
}