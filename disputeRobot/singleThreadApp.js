// inital setup
var pmx = require('pmx');
//Global Exception Logging
pmx.init();
var express = require('express');

var exitProt = require('./server/exitProtocol');

var envConfig = require(__dirname + '/config/environmentConfig');
var sqlConnect = require(__dirname + '/mysql/db_conf');
var startMe = require(__dirname + '/server/backgroundProcess/checkDBData/scheduleProcesses');
var conv = require(__dirname + '/mysql/mysql_convertions');

var probe = pmx.probe();

//var testCodes = require('./testCodes/initEmitTest');
var initListeners = require(__dirname + '/initializeListeners/initializeAllListeners');

initListeners.start(); //initialize all known listeners for the system


var shutdown_counter = probe.counter({
  name : 'transaction shutdown'
});

var MainDataStruct = {
	app:express(),
	express:express,
	locationString:'',
	db_init:{
		host     : '',
		user     : '',
		password : '',
		database : ''
	},
	port:process.env.PORT,
	router:express.Router(),
	http:'',
	rootPath:__dirname
}


MainDataStruct = envConfig.envConf(MainDataStruct);
sqlConnect.mysqlConnect(MainDataStruct.db_init);
exitProt.initExitProtocols(MainDataStruct);

// REGISTER OUR ROUTES -------------------------------
// all of our routes will be from root /
if(process.env.NODE_ENV.toLowerCase()=='development')
{
	MainDataStruct.app.use('/', MainDataStruct.router);
}else{
	MainDataStruct.app.use('/api/disputerobot/', MainDataStruct.router);
}

// Add the error middleware at the end (after route declaration)
MainDataStruct.app.use(pmx.expressErrorHandler());

GLOBAL.server = MainDataStruct.app.listen(MainDataStruct.port);
console.log(MainDataStruct.http);
console.log(process.env.NODE_ENV+' mode running');
process.emit('Shopstyx:logme',"New Log Entry ==== "+conv.convertUnixToMySQL(Math.floor(new Date().getTime()/1000)));
startMe.start();