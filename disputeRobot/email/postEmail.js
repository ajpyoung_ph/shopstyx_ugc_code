var conv = require('../mysql/mysql_convertions');
var request = require('request');
var	util = require('util');

var getTargetInfo = function(cs_users_orders_data,target_user_id,request_msg,url,callback)
{
	var query = "SELECT user_id,username,firstname,lastname,email FROM cs_users WHERE user_id="+target_user_id+";";
	GLOBAL.db_cs_common.query(query,function(err,rows, fields){
		if(err==null)
		{
			request_msg.target_information.firstname=rows[0].firstname;
			request_msg.target_information.lastname=rows[0].lastname;
			request_msg.target_information.username=rows[0].username;
			request_msg.target_information.email=rows[0].email;
			request_msg.target_information.user_id=rows[0].user_id;

			var sigQuery = 'SELECT * FROM cs_settings WHERE `group` LIKE "api" AND `key` LIKE "webservice_signature"';
			GLOBAL.db_cs_common.query(sigQuery,function(errQ,rowsQ, fieldsQ){
				if(errQ==null)
				{
					var signature = rowsQ[0].value;
					//var url = GLOBAL.url+"webservices/order/updates/"+token+"/"+signature+"/"+user_id+"/"+shopstyx_transaction_id+"/";
					
					//send to Ferdie's API
					//http://shopstyx.local/webservices/sendmail/buyerprotectionexpiration
					var postConfig = {
						"url":url+signature,
						"form":{
							"data":request_msg
						}
					};
					//console.log("calling url: "+url);
					process.emit('Shopstyx:logme',"email Reminder for Shipment: at time "+conv.convertJSToMySQL(new Date()));
					process.emit('Shopstyx:logme',"target_information:");
					process.emit('Shopstyx:logme',request_msg.target_information);
					
					callback(cs_users_orders_data,postConfig,query);
					// targetDate=new Date();
					// //Date.setHours(hour,min,sec,millisec)
					// targetDate.setHours(9,0,0,0);
					// GLOBAL.scheduleShippingReminder[cs_users_orders_data.shopstyx_transaction_id] = schedule.scheduleJob(targetDate,postEmail.bind(null,postConfig,query));
				}else{
					setTimeout(sendRequest, 10 * 60 * 1000,shopstyx_transaction_id);
				}
			});
			

		}else{
			var msg = {
				"status":"Background Error",
				"desc":"[getMerchantInfo]Possible Error in MySQL",
				"err":err,
				"query":query
			}
			process.emit('Shopstyx:errormail',msg);
		}
	});
}

var postEmail = function(postConfig,query)
{

	console.log('postConfig');
	console.log(util.inspect(postConfig,{showHidden: false, depth: null}));
	// console.log("query:");
	// console.log(util.inspect(query,{showHidden: false, depth: null}));
	request.post(postConfig, function(err,httpResponse,body){ 
		if(err==null)
		{
			//do nothing
			console.log("Sent Email");
			console.log("reply is:");
			console.log(util.inspect(body,{showHidden: false, depth: null}));
			// console.log("httpResponse");
			// console.log(util.inspect(httpResponse,{showHidden: false, depth: null}));
		}else{
			var msg = {
				"status":"Background Error",
				"desc":"[getMerchantInfo]Possible Error in POSTING request for email",
				"err":err,
				"query":query
			}
			process.emit('Shopstyx:errormail',msg);
		}
	});
}

module.exports = {
	getTargetInfo:getTargetInfo,
	postEmail:postEmail
};