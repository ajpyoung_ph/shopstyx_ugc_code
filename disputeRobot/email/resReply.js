var util = require('util');
var nodemailer = require('nodemailer');
// var transporter = nodemailer.createTransport();
var smtpTransport = require('nodemailer-smtp-transport');
var mySQLStuff = require('../mysql/mysql_convertions');

var transporter = nodemailer.createTransport(smtpTransport({
    host: 'smtp.mailgun.org',
    port: 587,
    auth: {
        user: 'postmaster@shopstyx.com',
        pass: '0fkpo68eh3t4'
    }
}));
var pmx = require('pmx');

var sendEmail = function(msg)
{
	var message = msg;
	console.log(util.inspect(msg,{showHidden: false, depth: null}));
	var dataStruct = {
		"newSimpleDate":'',
		"newUnixDate":''
	};
	dataStruct.newSimpleDate = mySQLStuff.MySQLDateTimeNOW(dataStruct);
	if(typeof(msg)=='object')
	{
		msg['date']=[];
		msg['date_simple']=[];
		msg.date=dataStruct.newUnixDate;
		msg.date_simple=dataStruct.newSimpleDate;
		var err = new Error(JSON.stringify(msg));
			pmx.notify(err);
		message = JSON.stringify(msg);
	}
	var mailOptions = {
	    from: 'nodeservices@shopstyx.com', // sender address
	    to: 'andrewjohnyoung@gmail.com', // list of receivers
	    subject: 'Status Error', // Subject line
	    text: message // plaintext body
	    //html: '<b>Hello world ✔</b>' // html body
	};
	if(process.env.NODE_ENV.toLowerCase()!='production')
	{
		mailOptions.subject="["+process.env.NODE_ENV.toLowerCase()+"]"+mailOptions.subject;
	}
	// send mail with defined transport object
	transporter.sendMail(mailOptions, function(error, info){
	    if(error){
	        console.log(error);
	    }else{
	        console.log('Message sent: ' + info.response);
	    }
	});
};

var sendError = function(msg,res)
{
	if(msg.status=="Error" && msg.message!="400 page not found")
	{
		setImmediate(sendEmail,msg);
	}else{
		console.log(util.inspect(msg,{showHidden: false, depth: null}));
	}
	try{
		var err = new Error(JSON.stringify(msg));
		pmx.notify(err);
		res.json(msg);	
	}catch(err){
		//do nothing
		console.log("[sendError]");
		console.log(err);
	}
	
};

var sendGenericEmail = function(mailOptions)
{
	if(process.env.NODE_ENV.toLowerCase()!='production')
	{
		mailOptions.subject="["+process.env.NODE_ENV.toLowerCase()+"]"+mailOptions.subject;
	}
	transporter.sendMail(mailOptions, function(error, info){
	    if(error){
	        console.log(error);
	    }else{
	        console.log('Message sent: ' + info.response);
	    }
	});
}
module.exports = {
	sendEmail:sendEmail,
	sendError:sendError,
	sendGenericEmail:sendGenericEmail
};