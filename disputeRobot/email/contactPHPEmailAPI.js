
var request = require('request');
var conv = require('../mysql/mysql_convertions');
var sendRequest = function(shopstyx_transaction_id)
{
	//get user_id
	var mongoQuery = {
		"transaction_info.bought_items.shopstyx_transaction_id":shopstyx_transaction_id
	};
	//console.log(mongoQuery);
	GLOBAL.mongodbTransaction.transactionRecords.find(mongoQuery).limit(1,function(err,doc){

		if(err==null)
		{
			//console.log(doc);
			if(doc.length>0)
			{
				var user_id = doc[0].user_id;
				//get cs_token 
				var query = 'SELECT * FROM cs_tokens WHERE user_id='+user_id+' ORDER BY last_login DESC LIMIT 1;';
				GLOBAL.db_cs_common.query(query,function(err,rows, fields){
					if(err==null)
					{
						var user_id = rows[0].user_id;
						var token = rows[0].token;

						var sigQuery = 'SELECT * FROM cs_settings WHERE `group` LIKE "api" AND `key` LIKE "webservice_signature"';
						GLOBAL.db_cs_common.query(sigQuery,function(errQ,rowsQ, fieldsQ){
							if(errQ==null)
							{
								var signature = rowsQ[0].value;
								var url = GLOBAL.url+"webservices/order/updates/"+token+"/"+signature+"/"+user_id+"/"+shopstyx_transaction_id+"/";
								console.log("calling url: "+url);
								process.emit('Shopstyx:logme',"calling url: "+url+" at time "+conv.convertJSToMySQL(new Date()));
								request(url, function (error, response, body) {
									if (!error && response.statusCode == 200) 
									{
										// if(typeof(body)!='object')
										// {
										// 	body=JSON.parse(body);
										// }
										// if(String(body.status).toLowerCase()!="success")
										// {
										// 	setTimeout(sendRequest, 10 * 60 * 1000,shopstyx_transaction_id);
										// }
									}else{
										console.log("ERROR CALLING API");
										console.log(error);
										setTimeout(sendRequest, 10 * 60 * 1000,shopstyx_transaction_id);
									}
								});
							}else{
								setTimeout(sendRequest, 10 * 60 * 1000,shopstyx_transaction_id);
							}
						});
					}else{
						// if error retry action after 10 minutes
						setTimeout(sendRequest, 10 * 60 * 1000,shopstyx_transaction_id);
					}
				});
			}else{
				process.emit('Shopstyx:errormail',"ID does not exist: "+shopstyx_transaction_id);
			}
			
		}else{
			console.log("ERROR CALLING API");
			console.log(err);
			setTimeout(sendRequest, 10 * 60 * 1000,shopstyx_transaction_id);
		}

	});
};

module.exports={
	sendRequest:sendRequest
}