var callTrigger1 = require('../email/contactPHPEmailAPI');

module.exports = function(dataStruct){

	// router.route('/:token/:user_id/buyer/protection/log/start/')
	// 	.post(function(req,res){
	// 		validate(req,res,saveRecord.start);
	// 	});
	// router.route('/:token/:user_id/buyer/protection/log/stop/')
	// 	.post(function(req,res){
	// 		validate(req,res,saveRecord.start);
	// 	});
	router = dataStruct.router;
	router.route('/test/console/log/:msg/:type')
		.get(function(req,res){
			process.emit('Shopstyx:testConsoleLog',req.params.msg,req.params.type,res);
		});
		
	router.route('/test/res/:msg')
		.get(function(req,res){
			process.emit('Shopstyx:testReq',res,req.params.msg);
		});
	router.route('/logs/')
		.get(function(req,res){
			res.sendFile('schedule.log',{root:dataStruct.rootPath+'/server/backgroundProcess'});
		});
	router.route('/alive/')
		.get(function(req,res){
			var reply = {
				"alive":true
			};
			res.json(reply);
		});
	//for testing
	router.route('/testCall/:shopstyx_transaction_id')
		.get(function(req,res){
			var shopstyx_transaction_id = req.params.shopstyx_transaction_id;
			callTrigger1.sendRequest(shopstyx_transaction_id);
		});
	router.route('*')
		.get(function(req,res){
			msg = {
				status:"Error",
				message:"400 page not found",
				description: ""
			};
			process.emit('Shopstyx:error',msg,res);
		});

}