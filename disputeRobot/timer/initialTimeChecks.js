var collectPendingData = require('../server/backgroundProcess/checkDBData/collectPendingData');

var initializeCheck = function()
{
	process.on('Shopstyx:sheduledCheckDBPending',collectPendingData.checkPending);
	process.on('Shopstyx:errormail',resReply.sendEmail);
	process.on('Shopstyx:sendGenericEmail',resReply.sendGenericEmail);
}

module.exports = {
	initializeCheck:initializeCheck
}